TestNode is a one-off project to demonstrate GD92Component.

The project was created in Visual Studio 2010 using the Windows Forms Application template with minimum modification:
	Change Build target from x86 to AnyCPU to avoid Registry access problem
	Add App.config
	Add code copied (modified) from TestGateway

It should be possible to substitute this for the test MDG - messages going through the node are handled within the component.

For an MDT or IPSE you will need to add message and reply handling. This should be more or less as documented in the user guide but let me know if you get stuck. Comments and ideas for improvement would be very welcome.

You will probably find you need to run Visual Studio as administrator in order to get write access to the Registry (needed when sending and receiving messages).
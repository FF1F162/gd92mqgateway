﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Thales.KentFire.EventLib;
using Thales.KentFire.GD92;
using Thales.KentFire.LogLib;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private readonly string logName;
        private readonly string assemblyInformation;
        private readonly GD92Component gd92Component;
        private GD92EventHandler gd92Handler;

        public Form1()
        {
            InitializeComponent();

            // register event handler to close connections and tidy up if form is closed
            this.FormClosed += new FormClosedEventHandler(this.Form1_FormClosed);

            this.gd92Component = GD92Component.Instance;
            Assembly assembly = Assembly.GetExecutingAssembly();
            this.assemblyInformation = assembly.GetName().ToString();
            this.logName = "TestNode";
            this.ConfigureLogging();
        }

        /// <summary>
        /// Use MessageBox.Show to display the message.
        /// </summary>
        /// <param name="message">Message to display.</param>
        private static void DisplayMessage(string message)
        {
            MessageBox.Show(
                message,
                string.Empty,
                MessageBoxButtons.OK,
                MessageBoxIcon.None,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.RtlReading);
        }

        /// <summary>
        /// Set log to file (optional). 
        /// Creates folder Log in the application folder (or use LogControl.Instance.LogFolder to write somewhere else).
        /// Folder is created automatically - if there is no permission it fails silently.
        /// </summary>
        private void ConfigureLogging()
        {
            LogControl.Instance.LogToFile = true;
            LogControl.Instance.LoggingName = this.logName;
            LogControl.Instance.SetLoggingLevel(LogLevel.Debug);
            GD92EventHandler.Instance.LogHeaderInfo = this.assemblyInformation;
       }

        /// <summary>
        /// Note that GD92 activation runs in a new thread so this returns before gateway is fully configured.
        /// </summary>
        private void ActivateGD92()
        {
            ControlResult r = this.ConnectGD92();
            if (r != ControlResult.Success)
            {
                DisplayMessage("Activation failed - see log for details");
            }
        }

        /// <summary>
        /// Activate the GD92 component.
        /// GD92 activation runs in a separate thread and only sets Activated at the end.
        /// Assume that this is going to happen and set the reference to the singleton that handles GD92 events.
        /// </summary>
        /// <returns>Result returned by the Activate method of GD92 component.</returns>
        private ControlResult ConnectGD92()
        {
            System.Diagnostics.Debug.Assert(this.gd92Handler == null, "Call to ConnectGD92 is expected only if there is not yet any GD92Handler");
            ControlResult r = this.gd92Component.Activate();
            if (r == ControlResult.Success)
            {
                this.gd92Handler = GD92EventHandler.Instance;
            }
            else
            {
                // Activation was not possible - maybe because another instance of GD92Component
                // is already activated.
                EventControl.Emergency(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "GD92Component reported - {0}",
                    r.ToString()));
            }

            return r;
        }

        /// <summary>
        /// Deactivate GD92 communications (also event control, currently).
        /// Note that shut down uses a separate thread and may complete after this method returns.
        /// </summary>
        /// <returns>Result returned by Deactivate method of GD92Component.</returns>
        public ControlResult DeactivateGD92()
        {
            ControlResult r;

            EventControl.Alert(this.logName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "De-Activating {0}",
                this.assemblyInformation));

            this.gd92Handler = null;

            r = this.gd92Component.Deactivate();
            if (r != ControlResult.Success)
            {
                EventControl.Emergency(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "GD92Component reported - {0}",
                    r.ToString()));
            }

            return r;
        }

        /// <summary>
        /// De-activate when the form closes.
        /// </summary>
        /// <param name="sender">Not used.</param>
        /// <param name="e">Not used.</param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.gd92Handler != null)
            {
                this.DeactivateGD92();
            }

            LogControl.Instance.CloseLoggingForm();
        }

        /// <summary>
        /// Activate.
        /// Begin processing the event queue. There is a 1 second timer to service the event queue.
        /// EventControl fires Log events itself but uses the GD92Component to fire message and node events.
        /// Queue an alert to show identity in the log.
        /// Activate the GD92Component to listen for and make IP connections, depending on Registry configuration.
        /// </summary>
        /// <param name="sender">Not used.</param>
        /// <param name="e">Not used.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.gd92Handler == null)
            {
                EventControl.Instance.Activate(this.gd92Component);

                EventControl.Alert(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Activating {0}",
                    this.assemblyInformation));

                this.ActivateGD92();
            }
            else
            {
                DisplayMessage("Already activated");
            }
        }

        /// <summary>
        /// Log.
        /// Open logging window.
        /// </summary>
        /// <param name="sender">Not used.</param>
        /// <param name="e">Not used.</param>
        private void button2_Click(object sender, EventArgs e)
        {
            LogControl.Instance.OpenLoggingForm();
        }

        /// <summary>
        /// Deactivate.
        /// Shut down GD92Component and EventControl.
        /// </summary>
        /// <param name="sender">Not used.</param>
        /// <param name="e">Not used.</param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (this.gd92Handler == null)
            {
                DisplayMessage("Not activated");
            }
            else
            {
                this.DeactivateGD92();
                this.gd92Handler = null;
            }
        }
    }
}

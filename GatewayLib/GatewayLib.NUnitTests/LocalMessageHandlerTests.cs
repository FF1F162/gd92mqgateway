﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;

    [TestFixture]
    public class LocalMessageHandlerTests
    {
        private static ILogConfiguration testConfiguration;
        private static IFilter testFilter;
        private static string localNodeName;
        
        [TestFixtureSetUp]
        public static void Init()
        {
// No trace of any log file to be found as a result of running this
//            LogControl.Instance.LogToFile = true;
//            LogControl.Instance.LoggingName = "TestStormGateway";
//            LogControl.Instance.SetLoggingLevel(LogLevel.Debug);
//            EventControl.Instance.Activate();
//            Assembly assembly = Assembly.GetExecutingAssembly();
//            EventControl.Emerg("Test", string.Format(System.Globalization.CultureInfo.InvariantCulture, 
//                "Activating {0}", assembly.GetName().ToString()));
// Pretty much as expected this did not open a form (although test completed successfully)
//            LogControl.Instance.OpenLoggingForm();
            localNodeName = "TestLocalNodeName";
            testConfiguration = new LogConfiguration();
            testConfiguration.LocalNodeName = localNodeName;
            testFilter = new LogFilter(testConfiguration);
        }
        
        /// <summary>
        /// Running this under debug shows it adds a log to the event queue.
        /// </summary>
        [Test]
        public void LocalMessageHandlerTest()
        {
            GD92Message rxMsg = new GD92Msg1();
            var handler = new LocalMessageHandler(rxMsg);
            testConfiguration.LogDirection = DirectionOfTransmission.Any;
            handler.LogReceivedMessage(testFilter);
        }
        
        /// <summary>
        /// Running this under debug shows it does not add a log to the event queue.
        /// </summary>
        [Test]
        public void LocalMessageHandlerNoLogTest()
        {
            GD92Message rxMsg = new GD92Msg1();
            var handler = new LocalMessageHandler(rxMsg);
            Assert.IsNull(rxMsg.Destination, "therefore direction is not local");
            testConfiguration.LogDirection = DirectionOfTransmission.Local;
            handler.LogReceivedMessage(testFilter);
        }
        
        /// <summary>
        /// Running this under debug shows it adds a log to the event queue.
        /// </summary>
        [Test]
        public void LocalMessageHandlerLogTest()
        {
            GD92Message rxMsg = new GD92Msg1();
            rxMsg.Destination = localNodeName;
            var handler = new LocalMessageHandler(rxMsg);
            Assert.AreEqual(rxMsg.Destination, testConfiguration.LocalNodeName, "therefore direction is local");
            testConfiguration.LogDirection = DirectionOfTransmission.Local;
            handler.LogReceivedMessage(testFilter);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;

    [TestFixture]
    public class StormAttendanceTests
    {
        const string CallsignCommentaryAddress = "FJK60P1 [RE,ABC:SOME COMMENT], High St, Maidstone";
        const string CallsignAddress = " FJK60P1 , High St, Maidstone";
        const string AddressOnly = " , High St, Maidstone";
        
        [Test]
        public void StormAttendanceCallsignTest()
        {
            var attendance = new StormAttendance(CallsignCommentaryAddress);
            const string Expected = "FJK60P1";
            Assert.AreEqual(Expected, attendance.Resources[0].Callsign);
        }
        
        [Test]
        public void StormAttendancePlainCallsignTest()
        {
            var attendance = new StormAttendance(CallsignAddress);
            const string Expected = "FJK60P1";
            Assert.AreEqual(Expected, attendance.Resources[0].Callsign);
        }
        
        [Test]
        public void StormAttendanceResourceTest()
        {
            var attendance = new StormAttendance(CallsignCommentaryAddress);
            const string Expected = "RE ABC SOME COMMENT";
            Assert.AreEqual(Expected, attendance.Resources[0].Commentary);
        }

        [Test]
        public void StormAttendanceResourceCallsignTest()
        {
            var attendance = new StormAttendance(CallsignCommentaryAddress);
            const string Expected = "FJK60P1";
            Assert.AreEqual(Expected, attendance.Resources[0].Callsign);
        }

        [Test]
        public void StormAttendancePlainResourceTest()
        {
            var attendance = new StormAttendance(CallsignAddress);
            const string Expected = "";
            Assert.AreEqual(Expected, attendance.Resources[0].Commentary);
        }
        
        [Test]
        public void StormAttendanceAddressTest()
        {
            var attendance = new StormAttendance(CallsignCommentaryAddress);
            const string Expected = "High St, Maidstone";
            Assert.AreEqual(Expected, attendance.Address);
        }
        
        [Test]
        public void StormAttendancePlainAddressTest()
        {
            var attendance = new StormAttendance(CallsignAddress);
            const string Expected = "High St, Maidstone";
            Assert.AreEqual(Expected, attendance.Address);
        }
        
        [Test]
        public void StormAttendanceNoAddressTest()
        {
            var attendance = new StormAttendance("FJK60P1 [:]");
            const string Expected = "";
            Assert.AreEqual(Expected, attendance.Address);
        }
        
        [Test]
        public void StormAttendancePlainNoAddressTest()
        {
            var attendance = new StormAttendance("FJK60P1 ");
            const string Expected = "";
            Assert.AreEqual(Expected, attendance.Address);
        }
        
        [Test]
        public void StormAttendanceNoAddressCommentaryTest()
        {
            var attendance = new StormAttendance("FJK60P1 [:]");
            const string Expected = "";
            Assert.AreEqual(Expected, attendance.Resources[0].Commentary);
        }
        
        [Test]
        public void StormAttendancePlainNoAddressCommentaryTest()
        {
            var attendance = new StormAttendance("FJK60P1 ");
            const string Expected = "";
            Assert.AreEqual(Expected, attendance.Resources[0].Commentary);
        }

        /// <summary>
        /// Zero length callsign is valid in GD92 but there is no point in mobilising it
        /// and a resources field with no callsign may cause problems on MDT and IPSE.
        /// </summary>
        [ExpectedException("System.ArgumentOutOfRangeException")]
        [Test]
        public void StormAttendanceNoAddressNoCallsignTest()
        {
            var attendance = new StormAttendance("[:]");
            const int Expected = 0;
            Assert.AreEqual(Expected, attendance.Resources.Count);
        }
        
        [Test]
        public void StormAttendanceEmptyTest()
        {
            var attendance = new StormAttendance("");
            const int Expected = 0;
            Assert.AreEqual(Expected, attendance.Resources.Count);
        }
        
        [Test]
        public void StormAttendanceTwoCallsignTest()
        {
            var attendance = new StormAttendance("FJK60P1 [:], FJK60P2 [:], High St, Maidstone");
            const string Expected = "FJK60P1";
            Assert.AreEqual(Expected, attendance.Resources[0].Callsign);
            const string Expected2 = "FJK60P2";
            Assert.AreEqual(Expected2, attendance.Resources[1].Callsign);
        }
        
        [Test]
        public void StormAttendancePlainTwoCallsignTest()
        {
            var attendance = new StormAttendance("FJK60P1 , FJK60P2, High St, Maidstone");
            const string Expected = "FJK60P1";
            Assert.AreEqual(Expected, attendance.Resources[0].Callsign);
            const string Expected2 = "FJK60P2";
            Assert.AreEqual(Expected2, attendance.Resources[1].Callsign);
        }
        
        /// <summary>
        /// If any callsign has attributes we must assume that all have.
        /// StormAttendance treats the two callsigns as one and this is not valid in GD92.
        /// </summary>
        [ExpectedException("System.ArgumentOutOfRangeException")]
        [Test]
        public void StormAttendanceMissingResourceTest()
        {
            var attendance = new StormAttendance("FJK60P1, FJK60P2 [:], High St, Maidstone");
            const string Expected = "FJK60P1, FJK60P2";
            Assert.AreEqual(Expected, attendance.Resources[0].Callsign);
        }

        /// <summary>
        /// This is similar to what is seen occasionally on site. 
        /// There is no callsign and the Msg2 has an empty Callsigns list.
        /// This shows that the address is available in the StormAttendance object if required.
        /// </summary>
        [Test]
        public void StormAttendanceAddressOnlyTest()
        {
            var attendance = new StormAttendance(AddressOnly);
            const int Expected = 0;
            Assert.AreEqual(Expected, attendance.Resources.Count);
            const string ExpectedAddress = "High St, Maidstone";
            Assert.AreEqual(ExpectedAddress, attendance.Address);
        }
    }
}

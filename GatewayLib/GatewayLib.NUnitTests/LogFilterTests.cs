﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Tests for some of the filter logic.
    /// </summary>
    [TestFixture]
    public class LogFilterTests
    {
        private static ILogConfiguration testConfiguration;
        private static IFilter testFilter;
        private static string localNodeName;
        
        #region DirectionOfTransmission

        [Test]
        public void DirectionOfTransmissionDefaultTest()
        {
            var message = new GD92Msg1();
            DirectionOfTransmission direction = testFilter.DirectionOfMessage(message);
            Assert.AreEqual(DirectionOfTransmission.ToNetwork, direction, "because that is the default");
        }
        
        [Test]
        public void DirectionOfTransmissionLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            DirectionOfTransmission direction = testFilter.DirectionOfMessage(message);
            Assert.AreEqual(DirectionOfTransmission.Local, direction, "because destination name shows local node");
        }

        [Test]
        public void DirectionOfTransmissionLocalDespiteNodeTypeTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            message.Destination = localNodeName;
            DirectionOfTransmission direction = testFilter.DirectionOfMessage(message);
            Assert.AreEqual(DirectionOfTransmission.Local, direction, "because destination name shows local node");
        }

        [Test]
        public void DirectionOfTransmissionFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            DirectionOfTransmission direction = testFilter.DirectionOfMessage(message);
            Assert.AreEqual(DirectionOfTransmission.FromNetwork, direction, "because destination node type is Storm");
        }

        [Test]
        public void DirectionOfTransmissionToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            DirectionOfTransmission direction = testFilter.DirectionOfMessage(message);
            Assert.AreEqual(DirectionOfTransmission.ToNetwork, direction, "because destination node type is not Storm");
        }

        #endregion
        
        #region DefaultConfig

        [Test]
        public void LogIsRequiredDefaultConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            this.ResettestConfiguration();
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredDefaultConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredDefaultConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredDefaultConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        #endregion

        #region ToNetworkConfig

        [Test]
        public void LogIsRequiredToNetworkConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual, "because ToNetwork is the default");
        }

        [Test]
        public void LogIsRequiredToNetworkConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredToNetworkConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredToNetworkConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredNoneConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.None;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        #endregion

        #region FromNetworkConfig

        [Test]
        public void LogIsRequiredFromNetworkConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredFromNetworkConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredFromNetworkConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredFromNetworkConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        #endregion
        
        #region Type0Config

        [Test]
        public void LogIsRequiredType0ConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredType0ConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredType0ConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredType0ConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        #endregion
        
        #region Type1Config

        [Test]
        public void LogIsRequiredType1ConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            testConfiguration.LogTypes.Add(1);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredType1ConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            testConfiguration.LogTypes.Add(1);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredType1ConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            testConfiguration.LogTypes.Add(1);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredType1ConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.LogTypes.Add(0);
            testConfiguration.LogTypes.Add(1);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        #endregion
        
        #region Node432Config

        [Test]
        public void LogIsRequiredNode432ConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.LogNodes.Add(432);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredNode432ConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.LogNodes.Add(432);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredNode432ConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.LogNodes.Add(432);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredNode432ConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.LogNodes.Add(432);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void LogIsRequiredNode432ConfigMessageTo432Test()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            message.DestAddress = new GD92Address(22, 432, 1);
            ResettestConfiguration();
            testConfiguration.LogNodes.Add(432);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void LogIsRequiredNode432ConfigMessageFrom432Test()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            message.FromAddress = new GD92Address(22, 432, 1);
            ResettestConfiguration();
            testConfiguration.LogNodes.Add(432);
            bool actual = testFilter.LogIsRequired(message);
            Assert.IsTrue(actual);
        }

        #endregion
        
        #region XmlDefaultConfig

        [Test]
        public void XmlIsRequiredDefaultConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredDefaultConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredDefaultConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredDefaultConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        #endregion

        #region XmlToNetworkConfig

        [Test]
        public void XmlIsRequiredToNetworkConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual, "because ToNetwork is the default");
        }

        [Test]
        public void XmlIsRequiredToNetworkConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredToNetworkConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredToNetworkConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.ToNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredXmlNoneConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.None;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredLogNoneConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.LogDirection = DirectionOfTransmission.None;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual, "because tests for log and xml are independent");
        }

        #endregion

        #region XmlFromNetworkConfig

        [Test]
        public void XmlIsRequiredFromNetworkConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredFromNetworkConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredFromNetworkConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredFromNetworkConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.XmlDirection = DirectionOfTransmission.FromNetwork;
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        #endregion
        
        #region XmlType0Config

        [Test]
        public void XmlIsRequiredType0ConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.XmlTypesToNetwork.Add(0);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredType0ConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.XmlTypesToNetwork.Add(0);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual, "because there is no type filter for local node messages");
        }

        [Test]
        public void XmlIsRequiredType0ConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.XmlTypesFromNetwork.Add(0);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredType0ConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.XmlTypesToNetwork.Add(0);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        #endregion
        
        #region XmlType1Config

        [Test]
        public void XmlIsRequiredType1ConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.XmlTypesToNetwork.Add(0);
            testConfiguration.XmlTypesToNetwork.Add(1);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredType1ConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.XmlTypesToNetwork.Add(0);
            testConfiguration.XmlTypesToNetwork.Add(1);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredType1ConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.XmlTypesToNetwork.Add(0);
            testConfiguration.XmlTypesToNetwork.Add(1);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredType1ConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.XmlTypesToNetwork.Add(0);
            testConfiguration.XmlTypesToNetwork.Add(1);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        #endregion
        
        #region XmlNode432Config

        [Test]
        public void XmlIsRequiredNode432ConfigDefaultMessageTest()
        {
            var message = new GD92Msg1();
            ResettestConfiguration();
            testConfiguration.XmlNodes.Add(432);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredNode432ConfigMessageLocalTest()
        {
            var message = new GD92Msg1();
            message.Destination = localNodeName;
            ResettestConfiguration();
            testConfiguration.XmlNodes.Add(432);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredNode432ConfigMessageFromNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Storm;
            ResettestConfiguration();
            testConfiguration.XmlNodes.Add(432);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredNode432ConfigMessageToNetworkTest()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            ResettestConfiguration();
            testConfiguration.XmlNodes.Add(432);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsFalse(actual);
        }

        [Test]
        public void XmlIsRequiredNode432ConfigMessageTo432Test()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            message.DestAddress = new GD92Address(22, 432, 1);
            ResettestConfiguration();
            testConfiguration.XmlNodes.Add(432);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        [Test]
        public void XmlIsRequiredNode432ConfigMessageFrom432Test()
        {
            var message = new GD92Msg1();
            message.DestNodeType = NodeType.Rsc;
            message.FromAddress = new GD92Address(22, 432, 1);
            ResettestConfiguration();
            testConfiguration.XmlNodes.Add(432);
            bool actual = testFilter.XmlLogRequired(message);
            Assert.IsTrue(actual);
        }

        #endregion
        
        [TestFixtureSetUp]
        public void Init()
        {
            localNodeName = "TestlocalNodeName";
            testConfiguration = new LogConfiguration();
            testConfiguration.LocalNodeName = localNodeName;
            testFilter = new LogFilter(testConfiguration);
        }
        
        private void ResettestConfiguration()
        {
            testConfiguration.LogDirection = DirectionOfTransmission.Any;
            testConfiguration.LogNodes.Clear();
            testConfiguration.LogTypes.Clear();
            testConfiguration.XmlDirection = DirectionOfTransmission.Any;
            testConfiguration.XmlNodes.Clear();
            testConfiguration.XmlTypesToNetwork.Clear();
            testConfiguration.XmlTypesFromNetwork.Clear();
        }
    }
}

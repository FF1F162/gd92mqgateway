﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using NUnit.Framework;
    using System;
    using System.IO;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Tests how the Msg5Handler deals with various text in the Callsign field
    /// of a Resource Status Request. If the text represents a station, the handler
    /// forwards a request for each callsign at the station. If the text represents
    /// a callsign it sends a request for the callsign.
    /// </summary>
    [TestFixture]
    public class Msg5HandlerTests
    {
        private static string TestInputPath;
        private static string TestOutputPath;
        private static MockMessageSender Sender;
        private static IMapBook MapBooks;
        private static HomeStations homeStations;

        /// <summary>
        /// Requires HomeStationsFilePath in app.config pointing to HomeStationsForMsg5handlerTests.txt containing
        /// FJK44P1,FJK44,701
        /// FJK60P1,FJK60,703
        /// FJK60R1,FJK60,705
        /// FJKN144,FJK60,706
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestOutput");
            Sender = new MockMessageSender();
            MapBooks = new KentAtoZ();
            homeStations = new HomeStations();
            homeStations.DataFilePath = Path.Combine(TestInputPath, "HomeStationsForMsg5handlerTests.txt");
            homeStations.UpdateHomeStationData();
        }

        /// <summary>
        /// Fails because the callsign length is not valid.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.GatewayLib.GatewayException", ExpectedMessage = "Station name NotSet has unexpected length.")]
        public void Msg5HandlerNewMessageTest()
        {
            var message = new GD92Msg5();
            Assert.AreEqual("NotSet", message.Callsign);
            var handler = new Msg5Handler(message, homeStations);
            handler.ProcessReceivedMessage(Sender, MapBooks);
        }

        /// <summary>
        /// Fails because the callsign length is not valid.
        /// Current processing means the callsign is treated as a station name
        /// and the message is acknowledged by sending Msg50 before validating
        /// the name.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.GatewayLib.GatewayException", ExpectedMessage = "Station name  has unexpected length.")]
        public void Msg5HandlerNoCallsignTest()
        {
            SendNoRequestForStation(string.Empty);
        }

        [Test]
        public void Msg5HandlerFullCallsignTest()
        {
            SendRequest("FJK60P1", "FJK60P1");
        }

        [Test]
        public void Msg5HandlerAllZCallsignTest()
        {
            SendRequest("ZZZZZZZ", "ZZZZZZZ");
        }

        /// <summary>
        /// Station name is valid but there are no data in homeStations.
        /// </summary>
        [Test]
        public void Msg5HandlerNoStationTest()
        {
            SendNoRequestForStation("FJK61");
        }

        /// <summary>
        /// Station name is valid but there are no data in homeStations.
        /// </summary>
        [Test]
        public void Msg5HandlerNoKentStationTest()
        {
            SendNoRequestForStation("Z61");
        }

        [Test]
        public void Msg5HandlerFullStationTest()
        {
            SendRequestsForStation60("FJK60");
        }

        [Test]
        public void Msg5HandlerKentStationTest()
        {
            SendRequestsForStation60("S60");
        }

        [Test]
        public void Msg5HandlerStationCodeTest()
        {
            SendRequestsForStation60("60");
        }

        [Test]
        public void Msg5HandlerCoreCallsignTest()
        {
            SendRequest("60R1", "FJK60R1");
        }

        [Test]
        public void Msg5HandlerCoreCallsignXXXXTest()
        {
            SendRequest("XXXX", "FJKXXXX");
        }

        /// <summary>
        /// Expected is the last of three callsigns for station 60 in homeStations.
        /// If the test is successful, there is a message sent for each callsign
        /// and the sender retains the last for inspection.
        /// </summary>
        /// <param name="stationIdentifier">Identifier of station 60.</param>
        private void SendRequestsForStation60(string stationIdentifier)
        {
            const string Expected = "FJKN144";
            SendRequest(stationIdentifier, Expected);
        }

        /// <summary>
        /// Create a new Msg5 as if just received and a handler to process it.
        /// The mock sender saves the (last) message instead of actually sending it.
        /// </summary>
        /// <param name="callsign">Text representing callsign or station.</param>
        /// <param name="expected">Callsign in the last request to be sent.</param>
        private void SendRequest(string callsign, string expected)
        {
            var message = new GD92Msg5();
            message.Callsign = callsign;
            var handler = new Msg5Handler(message, homeStations);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg5;
            Assert.AreEqual(expected, sent.Callsign);
        }

        /// <summary>
        /// Create a new Msg5 as if just received and a handler to process it.
        /// The test succeeds if the station name is in the correct format
        /// but there is no callsign for the station in the homeStation data.
        /// The mock sender retains the acknowledgement sent for the received Msg5
        /// (this is always sent regardless of whether or not anything is forwarded).
        /// </summary>
        /// <param name="station">Text representing station.</param>
        private void SendNoRequestForStation(string station)
        {
            var message = new GD92Msg5();
            message.Callsign = station;
            var handler = new Msg5Handler(message, homeStations);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg50;
            Assert.IsNotNull(sent);
        }
    }
}

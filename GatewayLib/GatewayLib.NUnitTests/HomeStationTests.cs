﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;

    [TestFixture]
    public class HomeStationTests
    {
        [Test]
        public void HomeStationConstructNodeNameTest()
        {
            var station = new HomeStation("FJK40", "809");
            const string Expected = "Node809";
            string actual = station.NodeName;
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void HomeStationNodeNameExpectedInRegistryTest()
        {
            var station = new HomeStation("ZMA", "TMPMA");
            const string Expected = "TMPMA";
            string actual = station.NodeName;
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void HomeStationDefaultConstructorNodeNameTest()
        {
            var station = new HomeStation();
            const string Expected = "NodeUnknown";
            string actual = station.NodeName;
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void HomeStationDefaultConstructorCodeTest()
        {
            var station = new HomeStation();
            const string Expected = "DF";
            string actual = station.Code;
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void ExtractStationCodeTest()
        {
            const string Expected = "40";
            string actual = HomeStation.ExtractStationCode("40");
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void ExtractStationCodeFromNationalStationNameTest()
        {
            const string Expected = "40";
            string actual = HomeStation.ExtractStationCode("FJK40");
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void ExtractStationCodeFromTemporaryStationNameTest()
        {
            const string Expected = "MA";
            string actual = HomeStation.ExtractStationCode("ZMA");
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        [ExpectedException("Thales.KentFire.GatewayLib.GatewayException")]
        public void ExtractStationCodeTooShortTest()
        {
            const string Expected = "";
            string actual = HomeStation.ExtractStationCode(String.Empty);
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        [ExpectedException("Thales.KentFire.GatewayLib.GatewayException")]
        public void ExtractStationCodeFromNationalStationNameOverTheBorderTest()
        {
            const string Expected = "40";
            string actual = HomeStation.ExtractStationCode("FJS40");
            Assert.AreEqual(Expected, actual);
        }

        [Test]
        [ExpectedException("Thales.KentFire.GatewayLib.GatewayException")]
        public void ExtractStationCodeFromShortCallsignNameTest()
        {
            const string Expected = "11";
            string actual = HomeStation.ExtractStationCode("N611");
            Assert.AreEqual(Expected, actual);
        }
    }
}

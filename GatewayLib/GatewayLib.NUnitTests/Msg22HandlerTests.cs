﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.MapLib;

    [TestFixture]
    public class Msg22HandlerTests
    {
        private static string TestInputPath;
        private static string TestOutputPath;
        private static MockMessageSender Sender;
        private static IMapBook MapBooks;

        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestOutput");
            Sender = new MockMessageSender();
            MapBooks = new KentAtoZ();
        }
        
        [Test]
        public void Msg22HandlerEmptyMessageTest()
        {
            var message = new GD92Msg22();
            var handler = new Msg22Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg22;
            Assert.IsNotNull(sent);
        }
        
        /// <summary>
        /// Single sequence test.
        /// There are two date time operations - could be in different seconds - repeated trials did not fail.
        /// </summary>
        [Test]
        public void Msg22HandlerSingleMessageTest()
        {
            var message = new GD92Msg22();
            message.IncidentNumber = "1";
            const string text = "A23456789";
            string expected = DateTime.Now.ToString(Msg22Handler.SequenceNumberFormat) + ": " + text;
            message.Update = text;
            var handler = new Msg22Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg22;
            Assert.IsNotNull(sent);
            Assert.AreEqual(expected, sent.Update);
        }

        /// <summary>
        /// Two sequence test.
        /// There are two date time operations - could be in different seconds - repeated trials did not fail
        /// Actually one did out of about 40. And another 28 march.
        /// </summary>
        [Test]
        public void Msg22HandlerFiveMessageTest()
        {
            var now = DateTime.Now;
            CheckNextMessage("73", 0, now);
            CheckNextMessage("73", 1, now);
            CheckNextMessage("73", 2, now);
            CheckNextMessage("73", 3, now);
            CheckNextMessage("73", 4, now);
            CheckNextMessage("74", 0, now);
        }
        
        private void CheckNextMessage(string incidentNumber, int sequenceNumber, DateTime now)
        {
            var message = new GD92Msg22();
            message.IncidentNumber = incidentNumber;
            string text = "A23456789 - " + sequenceNumber.ToString();
            var nowPlusSequence = now.AddSeconds(sequenceNumber);
            string expected = nowPlusSequence.ToString(Msg22Handler.SequenceNumberFormat) + ": " + text;
            message.Update = text;
            var handler = new Msg22Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg22;
            Assert.IsNotNull(sent);
            Assert.AreEqual(expected, sent.Update);
        }
    }
}

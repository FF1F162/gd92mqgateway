﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// The reply handler deals with a Nak to a message that has been forwarded by returning
    /// the Nak information to the original sender.
    /// </summary>
    [TestFixture]
    public class Reply51HandlerTests
    {
        private static string TestInputPath;
        private static string TestOutputPath;
        private static MockMessageSender sender;

        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestOutput");
            sender = new MockMessageSender();
        }
        
        /// <summary>
        /// Do just enough to satisfy assertions (1) the Nak has a reference to the forwarded message
        /// (2) The forwarded message has a reference to the original message (GatewayOriginal)
        /// This shows that problems of missing data are handled gracefully.
        /// </summary>
        [Test]
        public void EmptyMessageTest()
        {
            var originalMessage = new GD92Msg27();
            var forwardedMessage =  new GD92Msg27();
            forwardedMessage.GatewayOriginal = originalMessage;
            var receivedNak = new GD92Msg51(forwardedMessage);
            var handler = new Reply51Handler(receivedNak);
            handler.ProcessReplyReceived(sender);
            var forwardedNak = sender.MessageSent as GD92Msg51;
            Assert.IsNull(forwardedNak);
        }
        
        /// <summary>
        /// Create messages from data recorded in the log (BitConverter output transformed using
        /// TestGateway ToXML facility). Change utf-16 to utf-8.
        /// Looks as if MDG wrongly puts its own address in the OriginalDestination field.
        /// This tests that the reply handler creates a Nak for sending with correct From address.
        /// Note that the Frame actually transmitted takes details from the original frame (not tested here).
        /// </summary>
        [Test]
        public void Reply51Handler_NakFromMDGForTextFromStormTest()
        {
            var originalMessage = GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormOriginal_Logged.txt"));
            originalMessage.AckRequired = true;
            Assert.AreEqual(703, originalMessage.DestAddress.Node, "The original destination");
            var forwardedMessage = GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormForwarded.txt"));
            forwardedMessage.GatewayOriginal = originalMessage;
            var receivedNak = GD92Message.ReadFromFile<GD92Msg51>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormNakFromMDGForForwarded.txt"));
            receivedNak.OriginalMessage = forwardedMessage;
            Assert.AreEqual(224, receivedNak.FromAddress.Node, "This is the MDG reporting no bearer");
            Assert.AreEqual(224, receivedNak.OriginalDestination.Node, "Should be 703 - fault in RSC");
            var handler = new Reply51Handler(receivedNak);
            handler.ProcessReplyReceived(sender);
            var forwardedNak = sender.MessageSent as GD92Msg51;
            Assert.IsNotNull(forwardedNak);
            Assert.AreEqual(703, forwardedNak.OriginalDestination.Node, "Now taken from destination of original message");
            Assert.AreEqual(224, forwardedNak.FromAddress.Node, "Source of Nak is 224 for MDG");
        }
        
        /// <summary>
        /// Create messages from data recorded in the log (BitConverter output transformed using
        /// TestGateway ToXML facility). Change utf-16 to utf-8.
        /// In the received Nak change the node from 224 to 703 and reason from 1 to 8.
        /// </summary>
        [Test]
        public void Reply51Handler_NakFromDestinationForTextFromStormTest()
        {
            var originalMessage = GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormOriginal_Logged.txt"));
            originalMessage.AckRequired = true;
            var forwardedMessage =  GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormForwarded.txt"));
            forwardedMessage.GatewayOriginal = originalMessage;
            var receivedNak = GD92Message.ReadFromFile<GD92Msg51>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormNakFromDestinationForForwarded.txt"));
            receivedNak.OriginalMessage = forwardedMessage;
            Assert.AreEqual(703, receivedNak.FromAddress.Node, "This is from edited data");
            Assert.AreEqual(703, receivedNak.OriginalDestination.Node, "The original destination");
            Assert.AreEqual(originalMessage.DestAddress.Node, receivedNak.OriginalDestination.Node, "Told you so");
            var handler = new Reply51Handler(receivedNak);
            handler.ProcessReplyReceived(sender);
            var forwardedNak = sender.MessageSent as GD92Msg51;
            Assert.AreEqual(703, forwardedNak.OriginalDestination.Node, "The original destination");
            Assert.AreEqual(703, forwardedNak.FromAddress.Node, "Source of Nak is the original destination");
        }
        
        /// <summary>
        /// Create messages from data recorded in the log (BitConverter output transformed using
        /// TestGateway ToXML facility). Change utf-16 to utf-8.
        /// In the received Nak change the node from 224 to 703 and reason from 1 to 9.
        /// </summary>
        [Test]
        public void Reply51Handler_NakWaitAckForTextFromStormTest()
        {
            var originalMessage = GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormOriginal.txt"));
            originalMessage.AckRequired = true;
            Assert.AreEqual(12, originalMessage.DestAddress.Port, "Data edited to show destination port 12");
            var forwardedMessage =  GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormForwarded.txt"));
            forwardedMessage.GatewayOriginal = originalMessage;
            var receivedNak = GD92Message.ReadFromFile<GD92Msg51>(Path.Combine(TestInputPath, "Reply51Handler_TextFromStormWaitAckFromDestinationForForwarded.txt"));
            receivedNak.OriginalMessage = forwardedMessage;
            Assert.AreEqual(703, receivedNak.FromAddress.Node, "This is from edited data");
            Assert.AreEqual(703, receivedNak.OriginalDestination.Node, "The original destination");
            Assert.AreEqual(originalMessage.DestAddress.Node, receivedNak.OriginalDestination.Node, "Told you so");
            var handler = new Reply51Handler(receivedNak);
            handler.ProcessReplyReceived(sender);
            var forwardedNak = sender.MessageSent as GD92Msg51;
            Assert.AreEqual(703, forwardedNak.OriginalDestination.Node, "The original destination");
            Assert.AreEqual(703, forwardedNak.FromAddress.Node, "Source of Nak is the original destination");
            Assert.AreEqual(12, forwardedNak.FromAddress.Port, "Source of Nak shows original destination port");
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    
    [TestFixture]
    public class HomeStationsTests
    {
        private static HomeStations homeStations;
        private static string TestInputPath;
        private static string TestOutputPath;
        
        /// <summary>
        /// Requires HomeStationsFilePath in app.config pointing to HomeStations.txt containing
        /// // line of comment
        /// FJK44P1,FJK44,701
        ///  FJK60P1  ,           S60,703
        /// TEST,X60,  705
        /// end of data.        
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestOutput");
            homeStations = new HomeStations();
            homeStations.DataFilePath = Path.Combine(TestInputPath, "HomeStations.txt");
            homeStations.UpdateHomeStationData();
        }
        
        [Test]
        public void HomeStationForCallsignUnknownTest()
        {
            string actual = homeStations.HomeStationCodeForCallsign("FJK44P7");
            const string Expected = "DF";
            Assert.AreEqual(Expected, actual);
        }

        [Test]
        public void HomeStationForCallsign44P1Test()
        {
            string actual = homeStations.HomeStationCodeForCallsign("FJK44P1");
            const string Expected = "44";
            Assert.AreEqual(Expected, actual);
        }

        [Test]
        public void HomeStationForCallsign60P1Test()
        {
            string actual = homeStations.HomeStationCodeForCallsign("FJK60P1");
            const string Expected = "60";
            Assert.AreEqual(Expected, actual);
        }

        [Test]
        public void HomeStationForCallsignTestTest()
        {
            string actual = homeStations.HomeStationCodeForCallsign("TEST");
            const string Expected = "60";
            Assert.AreEqual(Expected, actual);
        }

        [Test]
        public void HomeStationNodeForCallsignTestTest()
        {
            string actual = homeStations.HomeStationNodeNameForCallsign("TEST");
            const string Expected = "Node705";
            Assert.AreEqual(Expected, actual);
        }

        [Test]
        public void HomeStationCallsignsWithHomeStationTest()
        {
            Collection<string> callsigns = homeStations.CallsignsWithHomeStation("N44");
            string actual = callsigns[0];
            const string Expected = "FJK44P1";
            Assert.AreEqual(Expected, actual);
            Assert.AreEqual(1, callsigns.Count);
        }

        [Test]
        public void HomeStationCallsignsWithHomeStationUnknownTest()
        {
            Collection<string> callsigns = homeStations.CallsignsWithHomeStation("DF");
            Assert.AreEqual(0, callsigns.Count);
        }
        
        [Test]
        public void HomeStationCallsignsWithHomeStation60Test()
        {
            Collection<string> callsigns = homeStations.CallsignsWithHomeStation("FJK60");
            Assert.AreEqual(2, callsigns.Count);
            Assert.IsTrue(callsigns.Contains("TEST"));
            Assert.IsTrue(callsigns.Contains("FJK60P1"));
        }
        
        [Test]
        [ExpectedException("Thales.KentFire.GatewayLib.GatewayException")]
        public void HomeStationCallsignsWithHomeStationEmptyTest()
        {
            Collection<string> callsigns = homeStations.CallsignsWithHomeStation(String.Empty);
            Assert.AreEqual(0, callsigns.Count);
        }
        
        [Test]
        public void HomeStationCallsignsReportTest()
        {
            const string Expected = @"Home station data for callsigns
FJK44P1,44,Node701
FJK60P1,60,Node703
TEST,60,Node705";
            string actual = homeStations.ReportCurrentData();
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void HomeStationWithNonKentPrefixTest()
        {
            var stations = new HomeStations();
            stations.DataFilePath = Path.Combine(TestInputPath, "HomeStationsFJS.txt");
            stations.UpdateHomeStationData();
            const string Expected = "Home station data for callsigns";
            string actual = stations.ReportCurrentData();
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        public void HomeStationDuplicateCallsignTest()
        {
            var stations = new HomeStations();
            stations.DataFilePath = Path.Combine(TestInputPath, "HomeStationsDuplicateCallsign.txt");
            stations.UpdateHomeStationData();
            const string Expected = "Home station data for callsigns";
            string actual = stations.ReportCurrentData();
            Assert.AreEqual(Expected, actual);
        }
        
        [Test]
        [ExpectedException("System.IO.FileNotFoundException")]
        public void HomeStationMissingFileTest()
        {
            var stations = new HomeStations();
            stations.DataFilePath = Path.Combine(TestInputPath, "HomeStationsNoSuchFile.txt");
            stations.UpdateHomeStationData();
            const string Expected = "Home station data for callsigns";
            string actual = stations.ReportCurrentData();
            Assert.AreEqual(Expected, actual);
        }
    }
}

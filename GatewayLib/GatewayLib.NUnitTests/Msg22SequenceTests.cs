﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using Thales.KentFire.GatewayLib;
    using NUnit.Framework;
    
    /// <summary>
    /// Tests for generating a unique sequence number (so the MDT displays every log update).
    /// Ensure each test uses a different incident number otherwise they interfere.
    /// </summary>
    [TestFixture]
    public class Msg22SequenceTests
    {
        private static DateTime Now;
        private static DateTime NowPlusOne;
        private static DateTime NowPlusTen;
        private static DateTime NowPlusYear;
        
        /// <summary>
        /// Ensure all tests are using the same time.
        /// This is converted to UTC for the call to Msg22Sequence.SequenceForIncident. The expected
        /// sequence numbers are in local time.
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            Now = DateTime.Now;
            NowPlusOne = Now.AddSeconds(1);
            NowPlusTen = Now.AddSeconds(10);
            NowPlusYear = Now.AddYears(1);
            Msg22Sequence.ClearForTest();
        }
        
        /// <summary>
        /// Call SequenceForIncident and check the sequence number returned is always different
        /// (sequence number returned = datetime + offset) for several calls in the same second
        /// and in subsequent seconds until time advances with no calls.
        /// </summary>
        [Test]
        public void Msg22SequenceSameIncidentMultipleCallsTest()
        {
            DoATest(Now, 0);
            DoATest(Now, 1);
            DoATest(Now, 2);
            DoATest(Now, 3);
            DoATest(NowPlusOne, 3); // offset from NowPlusOne
            DoATest(Now, 5);        // show the offset from the original time
            DoATest(Now, 6);        // show the offset from the original time
            DoATest(NowPlusTen, 0);
            DoATest(NowPlusTen, 1);
            DoATest(Now, 12);       // show the offset from the original time
            DoATest(NowPlusTen, 3);
            DoATest(NowPlusYear, 0);
        }

        /// <summary>
        /// If there is a request for the same incident number at the same second in the next year
        /// the sequence number is repeated (MMddHHmmss is the same for Now and NowPlusYear).
        /// </summary>
        [Test]
        public void Msg22SequenceSameIncidentAfterOneYearTest()
        {
            DoATest(Now, 0, "34");
            DoATest(Now, 1, "34");
            DoATest(NowPlusYear, 0, "34");
        }
        
        /// <summary>
        /// After moving into the next second older records (for other incidents) are no longer needed.
        /// Show that a call for incident 36 removes an earlier record for incident 35.
        /// </summary>
        [Test]
        public void Msg22SequenceOldRecordRemovedTest()
        {
            DoATest(Now, 0, "35");
            DoATest(NowPlusTen, 0, "36");
            // DoATest(Now, 1, "35"); // this passed before record removal was implemented
            DoATest(Now, 0, "35");
        }
        
        /// <summary>
        /// Test where several Msg22 arrive for the same incident. A couple of seconds later
        /// a record arrives for another incident. Check that the record for the first
        /// incident is not removed (because it is in advance of current time).
        /// </summary>
        [Test]
        public void Msg22SequenceOldRecordStillNeededTest()
        {
            DoATest(Now, 0, "37");
            DoATest(Now, 1, "37");
            DoATest(Now, 2, "37");
            DoATest(Now, 3, "37");
            DoATest(NowPlusOne, 0, "38");
            DoATest(Now, 4, "37");
        }
        
        /// <summary>
        /// Check the offset corresponds to the number of previous calls to SequenceForIncident
        /// in the same second.
        /// </summary>
        /// <param name="now">Second when SequenceForIncident is called.</param>
        /// <param name="offset">Expected offset (in seconds) of the time returned.</param>
        private void DoATest(DateTime now, int offset)
        {
            DoATest(now, offset, "33");
        }
        
        /// <summary>
        /// Use the time (now) and offset to generate the expected sequence for the incident, based on
        /// local time. Note that SequenceForIncident requires UTC.
        /// </summary>
        /// <param name="now">Time initialised for the tests.</param>
        /// <param name="offset">Number of seconds to be added to now.</param>
        /// <param name="incident">Incident number.</param>
        private void DoATest(DateTime now, int offset, string incident)
        {
            DateTime nowPlusOffset = now.AddSeconds(offset);
            string expected = nowPlusOffset.ToString(Msg22Handler.SequenceNumberFormat);
            string actual = Msg22Sequence.SequenceForIncident(incident, now.ToUniversalTime());
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Confirm that Kind is set for DateTime.Now and UtcNow but not for a new DateTime
        /// </summary>
        [Test]
        public void Msg22SequenceDateTimeKindTest()
        {
            var utcNow = DateTime.UtcNow;
            Assert.AreEqual(DateTimeKind.Utc, utcNow.Kind);
            var dt = DateTime.Now;
            Assert.AreEqual(DateTimeKind.Local, dt.Kind);
            var truncatedTime = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
            Assert.AreEqual(DateTimeKind.Unspecified, truncatedTime.Kind);
        }

        /// <summary>
        /// Confirm how local time behaves (BST or GMT)
        /// This test will fail during April when adding six months is not enough to get out of BST.
        /// </summary>
        [Test]
        public void Msg22SequenceDateTimeInSixMonthsTest()
        {
            var timeNow = DateTime.Now;
            var timeInSixMonths = timeNow.AddMonths(6);
            Assert.AreEqual(DateTimeKind.Local, timeInSixMonths.Kind);
            Assert.AreEqual(timeNow.Hour, timeInSixMonths.Hour); // AddMonths is not BST - GMT aware

            var utcTimeNow = DateTime.UtcNow;
            var utcTimeInSixMonths = utcTimeNow.AddMonths(6);
            Assert.AreEqual(DateTimeKind.Utc, utcTimeInSixMonths.Kind);
            Assert.AreEqual(utcTimeNow.Hour, utcTimeInSixMonths.Hour);

            var timeNowToUtc = timeNow.ToUniversalTime();
            var timeToUtcInSixMonths = timeNowToUtc.AddMonths(6);
            Assert.AreEqual(DateTimeKind.Utc, timeToUtcInSixMonths.Kind);
            Assert.AreEqual(timeNowToUtc.Hour, timeToUtcInSixMonths.Hour);
            Assert.AreEqual(timeNowToUtc.Hour, utcTimeInSixMonths.Hour);

            var utcTimeToLocal = utcTimeNow.ToLocalTime();
            Assert.AreEqual(DateTimeKind.Local, utcTimeToLocal.Kind);
            Assert.AreEqual(timeNow.Hour, utcTimeToLocal.Hour); // using BST - GMT setting for now

            var utcTimeInSixMonthsToLocal = utcTimeInSixMonths.ToLocalTime();
            Assert.AreEqual(DateTimeKind.Local, utcTimeInSixMonthsToLocal.Kind);
            Assert.AreNotEqual(timeNow.Hour, utcTimeInSixMonthsToLocal.Hour); // using BST - GMT setting for six months hence
        }

        /// <summary>
        /// Check working of ToLocalTime on Sep 15 2015.
        /// </summary>
        [Test]
        public void Msg22SequenceInBSTTest()
        {
            var utcTimeInBst = new DateTime(2015, 9, 15, 0, 30, 30, DateTimeKind.Utc); // local (BST) 01:30:30
            var utcTimeInChange = new DateTime(2015, 9, 15, 1, 30, 30, DateTimeKind.Utc);  // local (BST) 02:30:30
            var utcTimeInGmt = new DateTime(2015, 9, 15, 2, 30, 30, DateTimeKind.Utc); // local (BST) 03:30:30
            var localInBst = utcTimeInBst.ToLocalTime();
            var localInChange = utcTimeInChange.ToLocalTime();
            var localInGmt = utcTimeInGmt.ToLocalTime();
            Assert.AreEqual(1, localInBst.Hour);
            Assert.AreEqual(2, localInChange.Hour);
            Assert.AreEqual(3, localInGmt.Hour);
        }

        /// <summary>
        /// Check working of ToLocalTime on Oct 25 2015 (when clocks go back at 02:00 local time, 01:00 UTC).
        /// </summary>
        [Test]
        public void Msg22SequenceBstToGmtTest()
        {
            var utcTimeInBst = new DateTime(2015, 10, 25, 0, 30, 30, DateTimeKind.Utc); // local (BST) 01:30:30
            var utcTimeInChange = new DateTime(2015, 10, 25, 1, 30, 30, DateTimeKind.Utc);  // local (GMT) 01:30:30
            var utcTimeInGmt = new DateTime(2015, 10, 25, 2, 30, 30, DateTimeKind.Utc); // local (GMT) 02:30:30
            var localInBst = utcTimeInBst.ToLocalTime();
            var localInChange = utcTimeInChange.ToLocalTime();
            var localInGmt = utcTimeInGmt.ToLocalTime();
            Assert.AreEqual(1, localInBst.Hour);
            Assert.AreEqual(1, localInChange.Hour);
            Assert.AreEqual(2, localInGmt.Hour);
            Assert.IsTrue(localInBst.IsDaylightSavingTime());
            Assert.IsFalse(localInChange.IsDaylightSavingTime());
            Assert.IsFalse(localInGmt.IsDaylightSavingTime());
        }

        /// <summary>
        /// Test where incident messages arrive across time change.
        /// </summary>
        [Test]
        public void Msg22SequenceTimeChangeTest()
        {
            var utcNow = new DateTime(2015, 10, 25, 0, 59, 58, DateTimeKind.Utc); // local (BST) 01:59:58
            Assert.IsTrue(utcNow.ToLocalTime().IsDaylightSavingTime());
            const string Incident = "40";
            string actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1025015958", actual);
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1025015959", actual);
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1125010000", actual);
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1125010001", actual);
            var utcGmt = new DateTime(2015, 10, 25, 1, 1, 0, DateTimeKind.Utc);
            Assert.IsFalse(utcGmt.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcGmt);
            Assert.AreEqual("1125010100", actual);
        }
        
        /// <summary>
        /// Test where incident messages arrive across New Year.
        /// </summary>
        [Test]
        public void Msg22SequenceYearChangeTest()
        {
            var utcNow = new DateTime(2015, 12, 31, 23, 59, 58, DateTimeKind.Utc);
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            const string Incident = "41";
            string actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1231235958", actual, "First time for incident just before midnight");
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1231235959", actual, "Next in bunch of messages sent in same second");
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1301000000", actual, "Next in bunch - time now in January, month adjusted to be greater than previous");
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1301000001", actual, "Next in bunch - month adjusted");

            utcNow = new DateTime(2016, 01, 01, 1, 1, 0, DateTimeKind.Utc);
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1301010100", actual, "Month adjusted");

            utcNow = new DateTime(2016, 01, 01, 2, 1, 0, DateTimeKind.Utc);
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1301020100", actual, "Over two hours since start of incident - record weeded after creating this sequence");

            utcNow = new DateTime(2016, 01, 01, 2, 2, 0, DateTimeKind.Utc);
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("0101020200", actual, "Over two hours since start of incident - record weeded, month not adjusted");
        }

        /// <summary>
        /// Test where incident stays open across time change and into the new year.
        /// Adjustment of month ends at next time change (GMT to BST) but begins again if incident
        /// stays opem more than a year (assertions in line with this behaviour commented out below). 
        /// Current assertions depend on removal of first incident times older than 2 hours.
        /// </summary>
        [Test]
        public void Msg22SequenceTimeChangeAndNewYearTest()
        {
            var utcNow = new DateTime(2015, 10, 25, 0, 59, 58, DateTimeKind.Utc); // local (BST) 01:59:58
            Assert.IsTrue(utcNow.ToLocalTime().IsDaylightSavingTime());
            const string Incident = "42";
            string actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1025015958", actual, "Incident started in BST");

            utcNow = new DateTime(2015, 10, 25, 1, 1, 0, DateTimeKind.Utc); 
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1125010100", actual, "Month 10 > 11 adjusted for incident started in BST less than 2 hours ago");

            utcNow = new DateTime(2015, 11, 25, 1, 1, 0, DateTimeKind.Utc); 
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("1225010100", actual, "Month 11 > 12 adjusted for incident started in BST - record weeded after creating this sequence");

            utcNow = new DateTime(2015, 12, 25, 1, 1, 0, DateTimeKind.Utc);
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            //Assert.AreEqual("1325010100", actual, "Month 12 > 13 adjusted for incident started in BST");
            Assert.AreEqual("1225010100", actual, "Month not adjusted");

            utcNow = new DateTime(2016, 1, 25, 1, 1, 0, DateTimeKind.Utc);
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            //Assert.AreEqual("1425010100", actual, "Month 01 > 02 > 14 adjusted for BST to GMT and New Year");
            Assert.AreEqual("1325010100", actual, "Month 01 > 13 adjusted for New Year");

            utcNow = new DateTime(2016, 4, 25, 1, 1, 0, DateTimeKind.Utc); // local (BST) 02:0100
            Assert.IsTrue(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            Assert.AreEqual("0425020100", actual, "Now back into BST so no adjustment");

            utcNow = new DateTime(2016, 12, 25, 1, 1, 0, DateTimeKind.Utc); 
            Assert.IsFalse(utcNow.ToLocalTime().IsDaylightSavingTime());
            actual = Msg22Sequence.SequenceForIncident(Incident, utcNow);
            //Assert.AreEqual("1325010100", actual, "Month 12 > 13 adjusted for incident started in BST");
            Assert.AreEqual("1325010100", actual, "Month 12 > 13 adjusted for incident started in BST (April record not yet weeded)");
        }
    }
}

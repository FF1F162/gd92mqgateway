﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.MapLib;

    [TestFixture]
    public class Msg2HandlerTests
    {
        private static string TestInputPath;
        private static string TestOutputPath;
        private static MockMessageSender Sender;
        private static IMapBook MapBook;

        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestOutput");
            Sender = new MockMessageSender();
            MapBook = new KentAtoZ();
        }
        
        [Test]
        public void EmptyMessageTest()
        {
            var message = new GD92Msg2();
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
        }
        
        /// <summary>
        /// GD-92 allows 120 characters in the address field.
        /// </summary>
        [Test]
        public void MaxLengthAddressTest()
        {
            var message = new GD92Msg2();
            const string Expected = "A23456789 B23456789 C23456789 D23456789 E23456789 F23456789 G23456789 H23456789 I23456789 J23456789 K23456789 L23456789X";
            message.Address = Expected;
            message.Street = "";
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
            Assert.AreEqual(Expected, message6.Address);
        }

        /// <summary>
        /// RSC allows 140 characters. Actually anything up to 255 is safe but RSC truncates to 140 for display.
        /// </summary>
        [Test]
        public void ShortAddressTest()
        {
            var message = new GD92Msg2();
            const string Expected = "A23456789 B23456789";
            message.Address = Expected;
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
            Assert.AreEqual(Expected, message6.Address);
        }

        /// <summary>
        /// RSC allows 140 characters. Actually anything up to 255 is safe but RSC truncates to 140 for display.
        /// </summary>
        [Test]
        public void OverLengthAddressTest()
        {
            var message = new GD92Msg2();
            const string overLengthInput = "A23456789 B23456789 C23456789 D23456789 E23456789 F23456789 G23456789 H23456789 I23456789 J23456789 K23456789 L23456789 M23456789";
            const string ExpectedAddress = "A23456789 B23456789 C23456789 D23456789 E23456789 F23456789 G23456789 H23456789 I23456789 J23456789 K23456789 L23456789 ";
            const string ExpectedHouseNumber = "M23456789";
            message.Address = overLengthInput;
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
            Assert.AreEqual(ExpectedAddress, message6.Address);
            Assert.AreEqual(ExpectedHouseNumber, message6.HouseNumber);
        }

        [Test]
        public void MaxLengthElementsAddressTest()
        {
            var message = new GD92Msg2();
            const string ExpectedAddress = "A23456789 B23456789 C23456789 D23456789 E23456789 F23456789 G23456789 H23456789 I23456789 J23456789 K23456789 L23456789A";
            const string ExpectedHouseNumber = ", A2345678";
            const string ExpectedStreet = "9H, A23456789 B23456789 C23456789 D23456";
            const string ExpectedSubDistrict = "789S, A23456789 B23456789 C234";
            const string ExpectedDistrict = "56789S, A23456789 B23456789 C2";
            const string ExpectedTown = "3456789D, A23456789 B23456789 ";
            const string ExpectedCounty = "C23456789T, A2345678";
            message.Address = ExpectedAddress;
            message.HouseNumber = "A23456789H";
            message.Street = "A23456789 B23456789 C23456789 D23456789S";
            message.SubDistrict = "A23456789 B23456789 C23456789S";
            message.District = "A23456789 B23456789 C23456789D";
            message.Town = "A23456789 B23456789 C23456789T";
            message.County = "A23456789 B23456789C";
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
            Assert.AreEqual(ExpectedAddress, message6.Address);
            Assert.AreEqual(ExpectedHouseNumber, message6.HouseNumber);
            Assert.AreEqual(ExpectedStreet, message6.Street);
            Assert.AreEqual(ExpectedSubDistrict, message6.SubDistrict);
            Assert.AreEqual(ExpectedDistrict, message6.District);
            Assert.AreEqual(ExpectedTown, message6.Town);
            Assert.AreEqual(ExpectedCounty, message6.County);
        }

        [Test]
        public void AllElementsExceptCountyAddressTest()
        {
            var message = new GD92Msg2();
            const string ExpectedAddress = "A23456789 B23456789 C23456789 D23456789 E23456789 F23456789 G23456789 H23456789 I23456789 J23456789 K23456789 L23456789A";
            const string ExpectedHouseNumber = ", A2345678";
            const string ExpectedStreet = "9H, A23456789 B23456789 C23456789 D23456";
            const string ExpectedSubDistrict = "789S, A23456789 B23456789 C234";
            const string ExpectedDistrict = "56789S, A23456789 B23456789 C2";
            const string ExpectedTown = "3456789D, A23456789 B23456789 ";
            const string ExpectedCounty = "C23456789T";
            message.Address = ExpectedAddress;
            message.HouseNumber = "A23456789H";
            message.Street = "A23456789 B23456789 C23456789 D23456789S";
            message.SubDistrict = "A23456789 B23456789 C23456789S";
            message.District = "A23456789 B23456789 C23456789D";
            message.Town = "A23456789 B23456789 C23456789T";
            message.County = "";
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
            Assert.AreEqual(ExpectedAddress, message6.Address);
            Assert.AreEqual(ExpectedHouseNumber, message6.HouseNumber);
            Assert.AreEqual(ExpectedStreet, message6.Street);
            Assert.AreEqual(ExpectedSubDistrict, message6.SubDistrict);
            Assert.AreEqual(ExpectedDistrict, message6.District);
            Assert.AreEqual(ExpectedTown, message6.Town);
            Assert.AreEqual(ExpectedCounty, message6.County);
        }

        [Test]
        public void AllElementsExceptTownAndCountyAddressTest()
        {
            var message = new GD92Msg2();
            const string ExpectedAddress = "A23456789 B23456789 C23456789 D23456789 E23456789 F23456789 G23456789 H23456789 I23456789 J23456789 K23456789 L23456789A";
            const string ExpectedHouseNumber = ", A2345678";
            const string ExpectedStreet = "9H, A23456789 B23456789 C23456789 D23456";
            const string ExpectedSubDistrict = "789S, A23456789 B23456789 C234";
            const string ExpectedDistrict = "56789S, A23456789 B23456789 C2";
            const string ExpectedTown = "3456789D";
            const string ExpectedCounty = "";
            message.Address = ExpectedAddress;
            message.HouseNumber = "A23456789H";
            message.Street = "A23456789 B23456789 C23456789 D23456789S";
            message.SubDistrict = "A23456789 B23456789 C23456789S";
            message.District = "A23456789 B23456789 C23456789D";
            message.Town = "";
            message.County = "";
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
            Assert.AreEqual(ExpectedAddress, message6.Address);
            Assert.AreEqual(ExpectedHouseNumber, message6.HouseNumber);
            Assert.AreEqual(ExpectedStreet, message6.Street);
            Assert.AreEqual(ExpectedSubDistrict, message6.SubDistrict);
            Assert.AreEqual(ExpectedDistrict, message6.District);
            Assert.AreEqual(ExpectedTown, message6.Town);
            Assert.AreEqual(ExpectedCounty, message6.County);
        }
        
        /// <summary>
        /// Check that incident address remains unpopulated
        /// </summary>
        [Test]
        public void Msg2HandlerFirstAttendanceTest()
        {
            var message = new GD92Msg2();
            message.Text = @"INCIDENT NUMBER:  FJK-201505-0417

INCIDENT TIME:  09:17:43 21/05/2015

INCIDENT TYPE:  FIRE CHIMNEY

FIRST ATTENDANCE:  FJK60P1 [EXP:], LOWER RD, EAST FARLEIGH, MAIDSTONE

LOCATION COMMENT:  ";
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNullOrEmpty(message6.Address);
            Assert.AreEqual("LOWER RD, EAST FARLEIGH, MAIDSTONE", message6.FirstAttendance.Address);
        }
        
        /// <summary>
        /// Check that incident address is copied from address in dummy first attendance
        /// </summary>
        [Test]
        public void Msg2HandlerSecondAttendanceTest()
        {
            var message = new GD92Msg2();
            message.Text = @"INCIDENT NUMBER:  FJK-201505-0417

INCIDENT TIME:  09:17:43 21/05/2015

INCIDENT TYPE:  FIRE CHIMNEY

FIRST ATTENDANCE:  LOWER RD, EAST FARLEIGH, MAIDSTONE

SECOND ATTENDANCE:  FJK60P1 [EXP:], SECOND RD, EAST ATTENDANCE, MAIDSTONE

LOCATION COMMENT:  ";
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.IsNullOrEmpty(message6.FirstAttendance.Address);
            Assert.AreEqual("SECOND RD, EAST ATTENDANCE, MAIDSTONE", message6.SecondAttendance.Address);
            Assert.AreEqual("LOWER RD, EAST FARLEIGH, MAIDSTONE", message6.Address);
        }
        
        /// <summary>
        /// Check that mobilisation time is copied and SentAt remains at the default.
        /// </summary>
        [Test]
        public void Msg2HandlerMobTimeAndDateTest()
        {
            var message = new GD92Msg2();
            var mobDateTime = new DateTime(2016, 4, 12, 12, 30, 0);
            message.MobTimeAndDate = mobDateTime;
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBook);
            var message6 = Sender.MessageSent as GD92Msg6;
            Assert.AreEqual(mobDateTime, message6.MobTimeAndDate);
            Assert.AreEqual(DateTime.MinValue, message6.SentAt);
        }
    }
}

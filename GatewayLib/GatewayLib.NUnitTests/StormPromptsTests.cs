﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using System.Configuration;
    using System.Collections.ObjectModel;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;
    
    [TestFixture]
    public class StormPromptsTests
    {
        private static string TestInputPath;
        private static string TestOutputPath;
        private static string Msg2Data;
        private static string Msg2DataSplit;
        private static string Msg2DataCrlf;
        private static string Msg2DataPriority;

        /// <summary>
        /// Use Notepad to copy the Msg2 Text field from the XML output in a log from site to Msg2Data.
        /// Currently (Dec 2014) Storm is using \r\r as the separator (Macintosh) which means VS2010 reports inconsistent
        /// line endings for this file.
        /// The Crlf tests show that extraction also works for data in Windows format (\r\n\r\n)
        /// Differences in the data are: assistance on same line, All resources list spelt correctly.
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = FolderFromAppConfigOrDefault("TestOutput");
            Msg2Data = @"Incident Time:  15:20:41 28/11/2014Incident Type:  FIRE CHIMNEY, FIRE CHIMNEYAssistance message:  assistanceAll resources list:  FJK11P1,FJK20P1,FJK11R1First Attendance:  FJK21P1 [:], ALEX FARM, DUCK LN, SHADOXHURST, ASHFORDMap book: mapbookOSGR:  597024137541Tel Number:  12345Loc Info:  infoLocation Notices:  noticeLocation Risk:  riskLocation Comment:  comment";
            Msg2DataSplit = @"Incident Time:  15:20:41 28/12/2014

Incident Type:  FIRE CHIMNEY, FIRE CHIMNEY

Assistance message: 

All resources list:  FJK21P1,FJK20P1,FJK11R1

First Attendance:  

Second Attendance:  FJK11P1 [:], ALEX FARM, DRAKE LN, SHADOXHURST, ASHFORD

Map book: (1) mapbook (2) split

OSGR:  (1) 597024137541 (2) 597024139541

Tel Number:  12345

Loc Info:  info

Location Notices:  notice

Location Risk:  (1) risk (2) risk

Location Comment:  comment

";
            // Note blank incident time
            Msg2DataCrlf = @"Incident Time:  

Incident Type:  FIRE CHIMNEY, FIRE CHIMNEY

Assistance message:  assistance

All resources list:  FJK11P1,FJK20P1,FJK11R1

First Attendance:  FJK21P1 [:], ALEX FARM, DUCK LN, SHADOXHURST, ASHFORD

Map book: mapbook

OSGR:  597024137541

Tel Number:  12345

Loc Info:  info

Location Notices:  * notice 1
* notice 2

Location Risk:  risk

Location Comment:  comment

";
            // Data from live SGW1 log via TestGateway ToXML - cut from MobiliseMessage <text> field
            Msg2DataPriority = @"INCIDENT NUMBER:  FJK-201509-0358INCIDENT TIME:  02:07:13 06/09/2015INCIDENT TYPE:  AFA SMOKE ALARMASSISTANCE MESSAGE:  ALL RESOURCES LIST:  FIRST ATTENDANCE:  FJK91R1 [RPL:],FJK91P1 [P(BA):], 1, DOLA AVE, DEAL, CT149QHOSGR:  636793152471PRIORITY:  IMMEDIATE  LOCATION NOTICES:  LOCATION RISK:  LOCATION COMMENT:  ";
        }
        public const string DefaultPathToTestFolders = @"C:\git\kfrs\StormGateway\GatewayLib";
        public static string FolderFromAppConfigOrDefault(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? Path.Combine(DefaultPathToTestFolders, key);
        }
        
        /// <summary>
        ///A test for ExtractIncidentTime.
        ///</summary>
        [Test]
        public void ExtractIncidentTimeTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            DateTime expected = DateTime.Parse("28-11-2014 15:20:41");
            DateTime actual;
            actual = target.ExtractIncidentTime();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractIncidentTime. Demonstrate flexibility of DateTime.Parse.
        ///</summary>
        [Test]
        public void ExtractIncidentTimeBeforeDateTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            DateTime expected = DateTime.Parse("15:20:41 28-11-2014");
            DateTime actual;
            actual = target.ExtractIncidentTime();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractIncidentTime where there is none.
        ///</summary>
        [Test]
        public void ExtractIncidentTimeEmptyTest()
        {
            string msg2Text = Msg2DataCrlf;
            var target = new StormPrompts(msg2Text);
            DateTime expected = DateTime.MinValue;
            DateTime actual;
            actual = target.ExtractIncidentTime();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractAssistanceMessage.
        ///</summary>
        [Test]
        public void ExtractAssistanceMessageTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "assistance";
            string actual;
            actual = target.ExtractAssistanceMessage();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractIncidentType.
        ///</summary>
        [Test]
        public void ExtractIncidentTypeTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "FIRE CHIMNEY, FIRE CHIMNEY";
            string actual;
            actual = target.ExtractIncidentType();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocInfo.
        ///</summary>
        [Test]
        public void ExtractLocInfoOrOperatorInfoTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "info";
            string actual;
            actual = target.ExtractOperatorInfo();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocationComment.
        ///</summary>
        [Test]
        public void ExtractLocationCommentTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "comment"; 
            string actual;
            actual = target.ExtractLocationComment();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocationNotices.
        ///</summary>
        [Test]
        public void ExtractLocationNoticesTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "notice";
            string actual;
            actual = target.ExtractLocationNotices();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocationRisk.
        ///</summary>
        [Test]
        public void ExtractLocationRiskTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "risk";
            string actual;
            actual = target.ExtractLocationRisk();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractMapBook.
        ///</summary>
        [Test]
        public void ExtractMapBookTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "mapbook";
            string actual;
            actual = target.ExtractMapBook();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractMapBookSplit.
        ///</summary>
        [Test]
        public void ExtractMapBookSplitTest()
        {
            string msg2Text = Msg2DataSplit;
            var target = new StormPrompts(msg2Text);
            const string Expected = "See below";
            string actual;
            actual = target.ExtractMapBook();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractOsgr.
        ///</summary>
        [Test]
        public void ExtractOsgrTest()
        {
            string msg2Text = Msg2Data; 
            var target = new StormPrompts(msg2Text);
            const string Expected = "597024137541";
            string actual;
            actual = target.ExtractOSGridReference();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractTelNumber.
        ///</summary>
        [Test]
        public void ExtractTelNumberTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            const string Expected = "12345"; 
            string actual;
            actual = target.ExtractTelNumber();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for AllOtherResources.
        ///</summary>
        [Test]
        public void AllOtherResourcesTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            var expected = new GD92Msg6Resource("FJK11P1", string.Empty);
            Collection<GD92Msg6Resource> actual = target.AllOtherResources.Resources;
            Assert.AreEqual(expected.Callsign, actual[0].Callsign);
            Assert.AreEqual(expected.Commentary, actual[0].Commentary);
        }

        /// <summary>
        ///A test for FirstAttendanceResources.
        ///</summary>
        [Test]
        public void FirstAttendanceCallsignsTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            var expected = new GD92Msg6Resource("FJK21P1", "");
            Collection<GD92Msg6Resource> actual = target.FirstAttendance.Resources;
            Assert.AreEqual(expected.Callsign, actual[0].Callsign);
            Assert.AreEqual(expected.Commentary, actual[0].Commentary);
        }

        /// <summary>
        ///A test for SecondAttendanceCallsigns.
        ///</summary>
        [Test]
        public void SecondAttendanceCallsignsTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            var expected = new Collection<GD92Msg6Resource>();
            Collection<GD92Msg6Resource> actual = target.SecondAttendance.Resources;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SecondAttendanceCallsignsSplit.
        ///</summary>
        [Test]
        public void FirstAttendanceCallsignsSplitTest()
        {
            string msg2Text = Msg2DataSplit;
            var target = new StormPrompts(msg2Text);
            var expected = new Collection<GD92Msg6Resource>();
            Collection<GD92Msg6Resource> actual = target.FirstAttendance.Resources;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SecondAttendanceResourcesSplit.
        ///</summary>
        [Test]
        public void SecondAttendanceCallsignsSplitTest()
        {
            string msg2Text = Msg2DataSplit;
            var target = new StormPrompts(msg2Text);
            var expected = new GD92Msg6Resource("FJK11P1", "");
            Collection<GD92Msg6Resource> actual = target.SecondAttendance.Resources;
            Assert.AreEqual(expected.Callsign, actual[0].Callsign);
            Assert.AreEqual(expected.Commentary, actual[0].Commentary);
        }

        /// <summary>
        ///A test for ExtractOsgr.
        ///</summary>
        [Test]
        public void ExtractOsgrTestCrlf()
        {
            string msg2Text = Msg2DataCrlf; 
            var target = new StormPrompts(msg2Text);
            const string Expected = "597024137541";
            string actual;
            actual = target.ExtractOSGridReference();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractTelNumber.
        ///</summary>
        [Test]
        public void ExtractTelNumberTestCrlf()
        {
            string msg2Text = Msg2DataCrlf;
            var target = new StormPrompts(msg2Text);
            const string Expected = "12345"; 
            string actual;
            actual = target.ExtractTelNumber();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// A test for ExtractLocationNotices
        /// Note this also tests blank INCIDENT TIME.
        ///</summary>
        [Test]
        public void ExtractLocationNoticesAndResidueCrlfTest()
        {
            string msg2Text = Msg2DataCrlf;
            var target = new StormPrompts(msg2Text);
            target.ExtractAssistanceMessage();
            target.ExtractIncidentTime();
            target.ExtractIncidentType();
            target.ExtractMapBook();
            target.ExtractOperatorInfo();
            const string Expected = @"
* notice 1
* notice 2

Location Risk:  risk

Location Comment:  comment
";
            string actual;
            actual = target.ExtractLocationNoticesAndResidue();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocationNoticesAndResidue.
        ///</summary>
        [Test]
        public void ExtractLocationNoticesAndResidueTest()
        {
            string msg2Text = Msg2Data;
            var target = new StormPrompts(msg2Text);
            target.ExtractAssistanceMessage();
            target.ExtractIncidentTime();
            target.ExtractIncidentType();
            target.ExtractMapBook();
            target.ExtractOperatorInfo();
            const string Expected = @"
notice

Location Risk:  risk

Location Comment:  comment
";
            string actual;
            actual = target.ExtractLocationNoticesAndResidue();
            Assert.AreEqual(Expected, actual);
        }
        
        /// <summary>
        ///A test for ExtractLocationNoticesAndResidueSplit.
        ///</summary>
        [Test]
        public void ExtractLocationNoticesAndResidueSplitTest()
        {
            string msg2Text = Msg2DataSplit;
            var target = new StormPrompts(msg2Text);
            target.ExtractAssistanceMessage();
            target.ExtractIncidentTime();
            target.ExtractIncidentType();
            target.ExtractMapBook();
            target.ExtractOperatorInfo();
            const string Expected = @"
notice

Map book: (1) mapbook (2) split

OSGR:  (1) 597024137541 (2) 597024139541

Location Risk:  (1) risk (2) risk

Location Comment:  comment
";
            string actual;
            actual = target.ExtractLocationNoticesAndResidue();
            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// A test for ExtractLocationNoticesAndResidue using data from the live system
        /// with proposed Priority: field added.
        ///</summary>
        [Test]
        public void ExtractLocationNoticesAndResidueLiveTest()
        {
            string msg2Text = Msg2DataPriority;
            var target = new StormPrompts(msg2Text);
            target.ExtractAssistanceMessage();
            target.ExtractIncidentTime();
            target.ExtractIncidentType();
            target.ExtractMapBook();
            target.ExtractOperatorInfo();
            // From live SGW1 log vie TestGateway ToXML Msg6 <LocationNotices> field with PRIORITY added
            const string Expected = @"


INCIDENT NUMBER:  FJK-201509-0358

PRIORITY:  IMMEDIATE

LOCATION RISK:

LOCATION COMMENT:
";
            string actual;
            actual = target.ExtractLocationNoticesAndResidue();
            Assert.AreEqual(Expected, actual);
        }

    }
}


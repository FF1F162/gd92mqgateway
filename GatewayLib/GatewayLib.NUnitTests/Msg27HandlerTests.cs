﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.MapLib;

    [TestFixture]
    public class Msg27HandlerTests
    {
        private static string TestInputPath;
        private static string TestOutputPath;
        private static MockMessageSender Sender;
        private static IMapBook MapBooks;
        private static string Msg27DataCROnly;
        private static string Msg27DataCRLF;

        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = StormPromptsTests.FolderFromAppConfigOrDefault("TestOutput");
            Sender = new MockMessageSender();
            MapBooks = new KentAtoZ();
            Msg27DataCROnly = @"This is the first lineThis is the second lineThe third line has just one break:This is the fourth line";
            Msg27DataCRLF = @"This is the first line

This is the second line

The third line has just one break:
This is the fourth line
";
        }

        [Test]
        public void Msg27HandlerEmptyMessageTest()
        {
            var message = new GD92Msg27();
            var handler = new Msg27Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg27;
            Assert.IsNotNull(sent);
        }

        [Test]
        public void Msg27HandlerSingleLineTest()
        {
            var message = new GD92Msg27();
            const string Expected = "This is a single line";
            message.Text = Expected;
            var handler = new Msg27Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg27;
            Assert.AreEqual(Expected, sent.Text);
        }

        [Test]
        public void Msg27HandlerMultiLineTest()
        {
            var message = new GD92Msg27();
            string Expected = Msg27DataCRLF;
            message.Text = Expected;
            var handler = new Msg27Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg27;
            Assert.AreEqual(Expected, sent.Text);
        }

        [Test]
        public void Msg27HandlerConvertCROnlyToCRLFTest()
        {
            var message = new GD92Msg27();
            message.Text = Msg27DataCROnly;
            var handler = new Msg27Handler(message);
            handler.ProcessReceivedMessage(Sender, MapBooks);
            var sent = Sender.MessageSent as GD92Msg27;
            Assert.AreEqual(Msg27DataCRLF, sent.Text);
        }
    }
}

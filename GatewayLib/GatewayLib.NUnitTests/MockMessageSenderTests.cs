﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    
    [TestFixture]
    public class MockMessageSenderTests
    {
        [Test]
        public void MaxSerialNumberTest()
        {
            var sender = new MockMessageSender();
            sender.LastSerialNumber = int.MaxValue;
            sender.SendMessage(null);
            Assert.AreEqual(1, sender.LastSerialNumber);
        }
        
        [Test]
        public void MinSerialNumberTest()
        {
            var sender = new MockMessageSender();
            sender.LastSerialNumber = int.MinValue;
            sender.SendMessage(null);
            Assert.AreEqual(1, sender.LastSerialNumber);
        }
        
        [Test]
        public void MinusOneSerialNumberTest()
        {
            var sender = new MockMessageSender();
            sender.LastSerialNumber = -1;
            sender.SendMessage(null);
            Assert.AreEqual(1, sender.LastSerialNumber);
        }
        
        [Test]
        public void ZeroSerialNumberTest()
        {
            var sender = new MockMessageSender();
            sender.LastSerialNumber = 0;
            sender.SendMessage(null);
            Assert.AreEqual(1, sender.LastSerialNumber);
        }
        
        [Test]
        public void SerialNumberWrapTest()
        {
            var sender = new MockMessageSender();
            sender.LastSerialNumber = GD92Component.MaxSerialNumber;
            sender.SendMessage(null);
            Assert.AreEqual(1, sender.LastSerialNumber);
        }
        
        [Test]
        public void SerialNumberIncrementTest()
        {
            var sender = new MockMessageSender();
            sender.LastSerialNumber = GD92Component.MaxSerialNumber - 1;
            sender.SendMessage(null);
            Assert.AreEqual(GD92Component.MaxSerialNumber, sender.LastSerialNumber);
        }
    }
}

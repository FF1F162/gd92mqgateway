﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Properties for control of logging.
    /// </summary>
    public interface ILogConfiguration
    {
        LogLevel LoggingLevel { get; set; }

        string LocalNodeName { get; set; }

        DirectionOfTransmission LogDirection { get; set; }

        Collection<int> LogTypes { get; }

        Collection<int> LogNodes { get; }

        DirectionOfTransmission XmlDirection { get; set; }

        Collection<int> XmlTypesToNetwork { get; }

        Collection<int> XmlTypesFromNetwork { get; }

        Collection<int> XmlNodes { get; }

        void RefreshFromAppConfig();
    }
}

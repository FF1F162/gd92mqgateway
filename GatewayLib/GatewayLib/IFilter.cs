﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Use properties of message to filter for logging purposes and to determine direction of transmission.
    /// </summary>
    public interface IFilter
    {
        bool LogIsRequired(GD92Message message);

        bool XmlLogRequired(GD92Message message);

        DirectionOfTransmission DirectionOfMessage(GD92Message message);
    }
}

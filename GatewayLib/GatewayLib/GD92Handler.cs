﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Threading;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.LogLib;

    /// <summary>
    /// Handle events from the GD92Component.
    /// This class also handles configuration after first connection (which indicates that GD92 activation is complete).
    /// This includes the facility to configure the gateway as an emulator for Storm or RSC-based node.
    /// </summary>
    public sealed class GD92Handler
    {
        private static readonly GD92Handler Self = new GD92Handler();

        private readonly GD92Component gd92Component = GD92Component.Instance;

        /// <summary>
        /// List of directly-connected nodes (MTA to MTA connection).
        /// </summary>
        private readonly List<string> nodesConnected; 

        private readonly string logName;

        private ILogConfiguration logConfiguration;
        private IFilter filter;
        private MessageHandlerFactory messageHandlers;
        private ReplyHandlerFactory replyHandlers;
        private IHomeStations homeStations;
        private string localNodeName;

        private string stormEmulatorName;
        private string rscEmulatorName;
        private bool isStormEmulator;
        private bool isRscEmulator;

        /// <summary>
        /// Prevents a default instance of the <see cref="GD92Handler"/> class from being created.
        /// </summary>
        private GD92Handler()
        {
            this.logName = "GW_GD92Handler";
            this.localNodeName = "not configured";
            this.nodesConnected = new List<string>();
            this.gd92Component.MessageReceived += new EventHandler<MessageReceivedEventArgs>(this.GD92Component_MessageReceived);
            this.gd92Component.NodeConnected += new EventHandler<NodeConnectedEventArgs>(this.GD92Component_NodeConnected);
            this.gd92Component.NodeConnectionLost += new EventHandler<NodeConnectionLostEventArgs>(this.GD92Component_NodeConnectionLost);
            this.gd92Component.ReplyReceived += new EventHandler<ReplyReceivedEventArgs>(this.GD92Component_ReplyReceived);

            this.ReadStormEmulatorNameFromAppConfigOrDefault();
            this.ReadRscEmulatorNameFromAppConfigOrDefault();
        }

        /// <summary>
        /// Access to singleton for non-static method calls and properties.
        /// </summary>
        /// <value>Single instance of class.</value>
        public static GD92Handler Instance
        {
            get
            {
                return Self;
            }
        }

        /// <summary>
        /// Shows whether or not there is a connection.
        /// </summary>
        /// <value>True if there is connection to one or more nodes.</value>
        public bool Connected
        {
            get { return this.nodesConnected.Count > 0; }
        }

        /// <summary>
        /// Shows if this node is configured to emulate an IPSE or MDT in the way that it sends test messages
        /// and in the way that it responds to messages addressed directly to this node.
        /// </summary>
        /// <value>True if this emulates an RSC-based node.</value>
        public bool IsRscEmulator
        {
            get { return this.isRscEmulator; }
        }

        /// <summary>
        /// Shows if this node is configured to emulate Storm in the way that it sends test messages
        /// and in the way that it responds to messages addressed directly to this node.
        /// </summary>
        /// <value>True if this emulates Storm.</value>
        public bool IsStormEmulator
        {
            get { return this.isStormEmulator; }
        }

        /// <summary>
        /// Read the Storm emulator name from app.config. If null, set it to the default.
        /// </summary>
        private void ReadStormEmulatorNameFromAppConfigOrDefault()
        {
            string appKey = LogConfiguration.AppSettingKeyStormEmulatorName;
            string defaultName = LogConfiguration.DefaultStormEmulatorName;
            this.stormEmulatorName = ConfigurationManager.AppSettings[appKey] ?? defaultName;
        }

        /// <summary>
        /// Read the RSC emulator name from app.config. If null, set it to the default.
        /// </summary>
        private void ReadRscEmulatorNameFromAppConfigOrDefault()
        {
            string appKey = LogConfiguration.AppSettingKeyRscEmulatorName;
            string defaultName = LogConfiguration.DefaultRscEmulatorName;
            this.rscEmulatorName = ConfigurationManager.AppSettings[appKey] ?? defaultName;
        }

        /// <summary>
        /// Do emulator configuration only after reading from the Registry to get the name of the local node.
        /// If the node name is the same as an emulator name, the node is to behave as an emulator.
        /// </summary>
        private void UpdateEmulatorFlags()
        {
            System.Diagnostics.Debug.Assert(!this.stormEmulatorName.Equals(this.rscEmulatorName), "if emulator names are the same rscEmulatorName is ignored");
            if (GD92Component.LocalNodeName.Equals(this.stormEmulatorName, StringComparison.Ordinal))
            {
                this.isStormEmulator = true;
            }
            else if (GD92Component.LocalNodeName.Equals(this.rscEmulatorName, StringComparison.Ordinal))
            {
                this.isRscEmulator = true;
            }
        }

        #region GD92Component Event Handlers

        /// <summary>
        /// Handle reply received event.
        /// </summary>
        /// <description>
        /// Get a handler from the ReplyHandlerFactory, which works out what is required based on
        /// original message type and direction of travel. Call all methods in the handler interface
        /// (there are two - to make it clear what is going on).
        /// </description>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event parameters.</param>
        private void GD92Component_ReplyReceived(object sender, ReplyReceivedEventArgs e)
        {
            try
            {
                this.CheckConfigurationAndLoadIfMissing();
                GD92Message receivedMessage = e.Message;
                IReplyHandler handler = this.replyHandlers.HandlerForReply(receivedMessage);
                handler.LogReplyReceived(this.filter);
                handler.ProcessReplyReceived(this.gd92Component);
            }
            catch (Exception ex)
            {
                EventControl.Emergency(this.logName, ex.ToString());
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }
        }
        
        /// <summary>
        /// Load configuration in the usual way if it is missing (because there has been
        /// no connection to another node). This avoids null reference exception when the
        /// local node (UA) sends a message and GD92Component replies with Nak(no bearer).
        /// </summary>
        private void CheckConfigurationAndLoadIfMissing()
        {
            if (this.messageHandlers == null)
            {
               this.ProcessFirstConnection();
            }
        }

        /// <summary>
        /// Handle node connection lost event.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event parameters.</param>
        private void GD92Component_NodeConnectionLost(object sender, NodeConnectionLostEventArgs e)
        {
            try
            {
                if (this.nodesConnected.Contains(e.NodeName))
                {
                    this.nodesConnected.Remove(e.NodeName);
                    EventControl.Note(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "{0} {1} connection lost", 
                        e.NodeName,
                        e.MtaName));
                    bool isFirstMta = this.gd92Component.ScheduleRestartIfThisIsFirstMta(e);
                    if (isFirstMta)
                    {
                        EventControl.Note(this.logName, "Connection lost to FirstMta - restart scheduled");
                    }
                }
                else
                {
                    EventControl.Warn(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "{0} {1} connection lost but was not in NodesConnected", 
                        e.NodeName, 
                        e.MtaName));
                }
            }
            catch (Exception ex)
            {
                EventControl.Emergency(this.logName, ex.ToString());
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2); 
                throw;
            }
        }

        /// <summary>
        /// Handle node connected event.
        /// Use this to set up the message handler factory initially (in ProcessFirstConnection) -
        /// configuration needs local node name, which is not available until GD92Component
        /// activates.
        /// For first connection and after restart ensure that all MTAs are activated - if FirstMta
        /// is configured, only the first is activated initially.
        /// Whenever a node connects, refresh the home stations data. This is preferable to restart or timed refresh.
        /// If the configuration changes just reconnect an MDG (or restart).
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event parameters.</param>
        private void GD92Component_NodeConnected(object sender, NodeConnectedEventArgs e)
        {
            try
            {
                if (this.messageHandlers == null)
                {
                    this.ProcessFirstConnection();
                }

                if (this.nodesConnected.Count == 0)
                {
                    this.gd92Component.ActivateFully();
                }

                if (!this.nodesConnected.Contains(e.NodeName))
                {
                    this.nodesConnected.Add(e.NodeName);
                    EventControl.Note(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "{0} {1} connected", 
                        e.NodeName, 
                        e.MtaName));
                    bool restartWasScheduled = this.gd92Component.CancelRestartIfThisIsFirstMta(e);
                    if (restartWasScheduled)
                    {
                        EventControl.Note(this.logName, "FirstMta re-connected - restart cancelled");
                    }

                    this.homeStations.UpdateHomeStationData();
                    EventControl.Note(this.logName, this.homeStations.ReportCurrentData());
                }
                else
                {
                    EventControl.Warn(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "{0} {1} already connected", 
                        e.NodeName, 
                        e.MtaName));
                }
            }
            catch (Exception ex)
            {
                EventControl.Emergency(this.logName, ex.ToString());
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }
        }

        /// <summary>
        /// Complete configuration. This must be done after the GD92Component has completed start up
        /// and read what is needed from the Registry. A good indicator of this is first connection.
        /// </summary>
        private void ProcessFirstConnection()
        {
            this.localNodeName = GD92Component.LocalNodeName;
            this.logConfiguration = Gateway.Instance.Configuration;
            this.logConfiguration.LocalNodeName = GD92Component.LocalNodeName;
            this.filter = new LogFilter(this.logConfiguration);
            this.homeStations = new HomeStations();
            this.UpdateEmulatorFlags();
            this.messageHandlers = new MessageHandlerFactory(this.filter, this.homeStations);
            this.messageHandlers.NodeIsRscEmulator = this.isRscEmulator;
            this.messageHandlers.NodeIsStormEmulator = this.isStormEmulator;
            this.replyHandlers = new ReplyHandlerFactory(this.filter, this.homeStations);
            this.PrepareLogFileHeaderInformation();
        }
        
        private void PrepareLogFileHeaderInformation()
        {
            LogControl.Instance.HeaderInformation = string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0} {1}", 
                this.localNodeName, 
                Gateway.Instance.AssemblyInformation);
        }

        /// <summary>
        /// Handle message received event (for an unsolicited message, not a reply).
        /// Get a handler from the MessageHandlers factory, which works out what is required based on
        /// message type and direction of travel. Call all methods in the handler interface.
        /// Do not re-throw GatewayException as this sort probably does not justify stopping the gateway outright.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event parameters.</param>
        private void GD92Component_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            try
            {
                this.CheckConfigurationAndWarnIfMissing();
                this.logConfiguration.RefreshFromAppConfig(); // pick up any change since previous message
                GD92Message receivedMessage = e.Message;
                IMessageHandler handler = this.messageHandlers.HandlerForMessage(receivedMessage);
                handler.LogReceivedMessage(this.filter);
                handler.ProcessReceivedMessage(this.gd92Component, Gateway.Instance.MapBook);
            }
            catch (GatewayException ex)
            {
                EventControl.Emergency(this.logName, ex.ToString());
            }
            catch (Exception ex)
            {
                EventControl.Emergency(this.logName, ex.ToString());
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }
        }
        
        /// <summary>
        /// This is to avoid null reference exception if there has been no call
        /// to ProcessFirstConnection (but it is hard to see how a message can be
        /// received if there is no connection).
        /// </summary>
        private void CheckConfigurationAndWarnIfMissing()
        {
            if (this.messageHandlers == null)
            {
                throw new GD92LibException("Message handler configuration missing");
            }
        }

        #endregion 
    }
}

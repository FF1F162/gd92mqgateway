﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;

    public class TestLogConfiguration : LogConfiguration
    {
        /// <summary>
        /// Do nothing - so logging leve is updated from the Logging window 
        /// not from the configuration file.
        /// </summary>
        protected override void UpdateLoggingLevel()
        {
        }
    }
}

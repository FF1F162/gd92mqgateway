﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Base class implementing default behaviour for dealing with replies.
    /// The default behaviour is to send Ack to the sender of the original message.
    /// </summary>
    internal class ReplyHandler : IReplyHandler
    {
        public const string NameOfLocalReplyHandler = "GW_LocalReply";

        /// <summary>
        /// Initialises a new instance of the <see cref="ReplyHandler"/> class.
        /// </summary>
        /// <param name="message">Reply message.</param>
        public ReplyHandler(GD92Message message)
        {
            System.Diagnostics.Debug.Assert(message != null, "To check");
            System.Diagnostics.Debug.Assert(message.OriginalMessage != null, "Because a reply must have an original message");
            this.LogName = "GW_ReplyHandler";
            this.ReceivedReply = message;
            this.GatewayOriginal = message.OriginalMessage.GatewayOriginal;
        }

        /// <summary>
        /// The reply being handled.
        /// </summary>
        /// <value>Reply message.</value>
        protected GD92Message ReceivedReply { get; set; }

        /// <summary>
        /// The gateway intercepted this message and sent on a copy, which is the message
        /// that the reply refers to.
        /// </summary>
        /// <value>Intercepted message.</value>
        protected GD92Message GatewayOriginal { get; set; }

        /// <summary>
        /// To be overwritten by sub-class (if any).
        /// </summary>
        /// <value>Name of class (used in log).</value>
        protected string LogName { get; set; }

        /// <summary>
        /// To find out if the log is required.
        /// </summary>
        /// <value>Filter used to find out if the log is required.</value>
        protected IFilter Filter { get; set; }

        /// <summary>
        /// To forward reply.
        /// </summary>
        /// <value>Component used to forward reply.</value>
        protected IGD92MessageSender MessageSender { get; set; }

        /// <summary>
        /// Default is to show serial numbers, type, source and destination in the log.
        /// </summary>
        /// <param name="filter">Component used to test if logging is required.</param>
        public void LogReplyReceived(IFilter filter)
        {
            this.Filter = filter;
            if (EventControl.LogEventLevel >= LogLevel.Filter &&
                this.Filter.LogIsRequired(this.ReceivedReply))
            {
                EventControl.Note(this.LogName, this.ReportSerialNumbersTypesAndAddresses());
                if (this.Filter.XmlLogRequired(this.ReceivedReply))
                {
                    this.DoLogging();
                }
            }
        }
        
        /// <summary>
        /// The default is to forward Ack to the original sender using the addresses
        /// and sequence number saved with the orignal message intercepted by the gateway.
        /// For a reply to a message sent by the gateway on its own behalf GatewayOriginal is null.
        /// </summary>
        /// <param name="messageSender">Component used to send messages.</param>
        public void ProcessReplyReceived(IGD92MessageSender messageSender)
        {
            System.Diagnostics.Debug.Assert(messageSender != null, "Component used to send messages must not be null");
            this.MessageSender = messageSender;
            System.Diagnostics.Debug.Assert(
                this.GatewayOriginal != null || this.LogName == NameOfLocalReplyHandler,
               "There must be an intercepted message unless this is the local reply handler.");
            if (this.GatewayOriginal != null || this.LogName == NameOfLocalReplyHandler)
            {
                this.ProcessReply();
            }
        }
        
        #region Logging

        /// <summary>
        /// Measure time difference.
        /// </summary>
        /// <param name="before">Time at start.</param>
        /// <param name="after">Time at end.</param>
        /// <returns>Difference in seconds.</returns>
        internal static double ElapsedTime(DateTime before, DateTime after)
        {
            // Remove assertion as it interferes with unit tests where time is not an issue.
            // System.Diagnostics.Debug.Assert(before <= after, "Time before must be less than time after");
            return after.Subtract(before).TotalSeconds;
        }

        /// <summary>
        /// Provide the current time if the time has not been set. This is to provide a sensible log
        /// where the reply message is not sent.
        /// </summary>
        /// <param name="targetTime">Time or default (0) time.</param>
        /// <returns>Time (or current time if not set).</returns>
        internal static DateTime TimeNowIfNotSet(DateTime targetTime)
        {
            if (targetTime == DateTime.MinValue)
            {
                targetTime = DateTime.UtcNow;
            }

            return targetTime;
        }

        /// <summary>
        /// Override this to log XML (which requires casting the message to type).
        /// </summary>
        protected virtual void DoLogging()
        {
        }
        
        /// <summary>
        /// Optional record of reply received. Superseded by audit, probably.
        /// </summary>
        /// <returns>Record to be logged.</returns>
        protected string ReportSerialNumbersTypesAndAddresses()
        {
            string serialNumberOfMessageSavedByGateway = (this.GatewayOriginal != null) ?
                this.GatewayOriginal.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture) : 
                "none";
            return string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Received reply {0} to {1} ({2}) type {3} from {4} for {5}",
                this.ReceivedReply.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.ReceivedReply.OriginalMessage.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                serialNumberOfMessageSavedByGateway,
                this.ReceivedReply.MsgType.ToString(),
                this.ReceivedReply.FromAddress.ToString(),
                this.ReceivedReply.DestAddress.ToString());
        }

        #endregion

        #region Process reply

        /// <summary>
        /// This must be overridden to deal with Nak and ResourceStatus
        /// or whatever the local reply handler needs to do (probably nothing).
        /// </summary>
        protected virtual void ProcessReply()
        {
            this.SendAckToOriginalSender(string.Empty);
        }
        
        /// <summary>
        /// Send Ack Msg50 to the sender of the message intercepted and forwarded by the gateway
        /// (the Ack is addressed as if sent from the destination, not the gateway).
        /// Assume this is a manual acknowledgement if the ManualAck was set in the original message.
        /// </summary>
        /// <param name="comment">Comment added to the log.</param>
        protected void SendAckToOriginalSender(string comment)
        {
            var reply = new GD92Msg50(this.GatewayOriginal);
            this.PopulateSourceAndDestinationFromOriginal(reply);
            string action = "ack" + comment;
            if (this.GatewayOriginal.AckRequired)
            {
                this.MessageSender.SendMessage(reply);
            }
            else
            {
                action += " not sent";
            }

            this.AuditTransitRecord(reply, action, this.GatewayOriginal.ManualAck);
        }
        
        /// <summary>
        /// Take the destination and source from the original message (reversed for reply).
        /// Source is used but other header details are copied from the original message frame during encoding.
        /// </summary>
        /// <param name="reply">Reply message being populated.</param>
        protected void PopulateSourceAndDestinationFromOriginal(GD92Message reply)
        {
            reply.Destination = this.GatewayOriginal.From;
            reply.DestAddress = this.GatewayOriginal.FromAddress;
            reply.From = this.GatewayOriginal.Destination;
            reply.FromAddress = this.GatewayOriginal.DestAddress;
        }

        /// <summary>
        /// Audit transit of message through the gateway. There is expected to be a record
        /// for each new (unsolicited) message that is forwarded when the gateway receives
        /// acknowledgement or the message times out. RSC always requires acknowledgement.
        /// Message from Storm may not require acknowledgement but the gateway always requires
        /// acknowledgement for the copy it forwards to the RSC - the gateway only forwards
        /// acknowledgement to Storm if it was requested.
        /// </summary>
        /// <param name="reply">Message being sent in reply.</param>
        /// <param name="reason">Purpose of reply.</param>
        /// <param name="manualAck">Manual acknowledgement setting.</param>
        protected void AuditTransitRecord(GD92Message reply, string reason, bool manualAck)
        {
            if (this.GatewayOriginal != null)
            {
                double elapsedTime = 0;
                var original = new MessageTransitData(this.GatewayOriginal, elapsedTime);
                elapsedTime = ElapsedTime(this.GatewayOriginal.ReceivedAt, this.ReceivedReply.OriginalMessage.SentAt);
                var forwarded = new MessageTransitData(this.ReceivedReply.OriginalMessage, elapsedTime);
                elapsedTime = ElapsedTime(this.GatewayOriginal.ReceivedAt, this.ReceivedReply.ReceivedAt);
                var replyReceived = new MessageTransitData(this.ReceivedReply, elapsedTime);
                elapsedTime = ElapsedTime(this.GatewayOriginal.ReceivedAt, TimeNowIfNotSet(reply.SentAt));
                var forwardedReply = new MessageTransitData(reply, elapsedTime);
                EventControl.Audit(this.LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Audit transit {0}, {1}, {2}, {3}, {4}, {5}, {6}",
                    this.GatewayOriginal.ReceivedAt.ToLocalTime().ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                    original.ToString(),
                    forwarded.ToString(),
                    replyReceived.ToString(),
                    forwardedReply.ToString(),
                    reason,
                    manualAck ? "manual" : "-"));
            }
        }
        
        #endregion
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Reply handler for Msg20 ResourceStatus ** from Storm **
    /// This may be sent in response to Msg5.
    /// Or in response to Msg20 status update. It would be far preferable if Storm
    /// just sent an Ack and then later notification (as for an update in Storm).
    /// </summary>
    internal class Reply20Handler : ReplyHandler
    {
        private readonly GD92Msg20 resourceStatus;
        private readonly IHomeStations homeStations;

        /// <summary>
        /// Initialises a new instance of the <see cref="Reply20Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Reply message (type 20).</param>
        /// <param name="homeStations">Component to provide home station data.</param>
        public Reply20Handler(GD92Message message, IHomeStations homeStations)
            : base(message)
        {
            this.LogName = "GW_Reply20";
            this.resourceStatus = message as GD92Msg20;
            System.Diagnostics.Debug.Assert(this.resourceStatus != null, "To make sure the cast worked");
            this.homeStations = homeStations;
            System.Diagnostics.Debug.Assert(homeStations != null, "To check home station data is provided");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoLogging()
        {
            base.DoLogging();
            EventControl.Note(this.LogName, this.resourceStatus.ToXml());
        }

        /// <summary>
        /// Ignore Msg20 with status 0 - this seems to mean the callsign is not configured in Storm
        /// For reply to Msg5 station request send a new Msg20 to the original sender.
        /// For reply to Msg5 callsign request send a copy of the Msg20 in reply to the original sender.
        /// For reply to Msg20 send Ack (as MOBS does) then send new Msg20 to notify the callsign and station.
        /// </summary>
        protected override void ProcessReply()
        {
            if (this.resourceStatus.StatusCode > 0)
            {
                if (this.GatewayOriginal.MsgType == GD92MsgType.ResourceStatusRequest)
                {
                    var request = this.GatewayOriginal as GD92Msg5;
                    if (request.StationRequest)
                    {
                        this.SendResourceStatusForStationRequest();
                    }
                    else
                    {
                        this.ForwardResourceStatusToOriginalSender();
                    }
                }
                else if (this.GatewayOriginal.MsgType == GD92MsgType.ResourceStatus)
                {
                    this.SendAckToOriginalSender(" status changed");
                    this.SendNewStatusNotifications();
                }
                else
                {
                    throw new GatewayException(string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Msg20 received in reply to {0}",
                        Enum.GetName(typeof(GD92MsgType), this.GatewayOriginal.MsgType)));
                }
            }
        }

        /// <summary>
        /// Send the data in a new Msg20 as there has already been a reply to the original Msg5.
        /// </summary>
        private void SendResourceStatusForStationRequest()
        {
            GD92Msg20 newMessage = this.resourceStatus.Copy();
            Msg20Handler.PopulateRemarksField(newMessage, this.homeStations);
            this.SendNewStatusNotificationToOriginator(newMessage);
        }
        
        /// <summary>
        /// Send resopnse to Msg5.
        /// Take the destination and source from the original message (reversed for reply).
        /// Source is used but other header details are copied from the original message frame during encoding.
        /// </summary>
        private void ForwardResourceStatusToOriginalSender()
        {
            var reply = new GD92Msg20(this.GatewayOriginal);
            this.PopulateSourceAndDestinationFromOriginal(reply);

            this.resourceStatus.CopyFieldsTo(reply);
            Msg20Handler.PopulateRemarksField(reply, this.homeStations);
            this.MessageSender.SendMessage(reply);
            this.AuditTransitRecord(reply, "res", false);
        }
        
        /// <summary>
        /// Send new Msg20 to notify originator, callsign and station of the new status
        /// Usually the originator is MDT or IPSE but it could be a reserve MDT, maybe, 
        /// or the test application.
        /// </summary>
        private void SendNewStatusNotifications()
        {
            GD92Msg20 newMessage = this.resourceStatus.Copy();
            Msg20Handler.PopulateRemarksField(newMessage, this.homeStations);
            this.SendNewStatusNotificationToOriginator(newMessage);
            this.SendCopyOfNewStatusNotificationToCallsignIfNotOriginator(newMessage);
            this.SendCopyOfNewStatusNotificationToHomeStationIfNotOriginator(newMessage);
        }
        
        /// <summary>
        /// Send notification to the Originator.
        /// </summary>
        /// <param name="messageToOriginator">Message to send.</param>
        private void SendNewStatusNotificationToOriginator(GD92Message messageToOriginator)
        {
            this.MessageSender.SendMessage(messageToOriginator);
        }
        
        /// <summary>
        /// This does much the same as Msg20Handler.SendCopyToHomeStation
        /// Copy the message and send it to the home station of the callsign.
        /// </summary>
        /// <param name="messageToOriginator">Message to copy.</param>
        private void SendCopyOfNewStatusNotificationToHomeStationIfNotOriginator(GD92Msg20 messageToOriginator)
        {
            string stationNodeName = this.homeStations.HomeStationNodeNameForCallsign(messageToOriginator.Callsign);
            if (stationNodeName != HomeStation.UnknownNodeName)
            {
                this.SendCopyIfDestinationIsNotOriginator(stationNodeName, messageToOriginator);
            }
        }
        
        /// <summary>
        /// Send notification to the MDT
        /// TODO allow for the case where original Msg20 did not come from the callsign MDT
        /// That requires keeping track of the node used for each callsign. That is liable to change
        /// when a callsign swaps to a reserve appliance.
        /// </summary>
        /// <param name="messageToOriginator">Message to copy.</param>
        private void SendCopyOfNewStatusNotificationToCallsignIfNotOriginator(GD92Msg20 messageToOriginator)
        {
            try
            {
                string callsignNodeName = this.DummyNodeNameForCallsign();
                this.SendCopyIfDestinationIsNotOriginator(callsignNodeName, messageToOriginator);
            }
            catch (GD92LibException ex)
            {
                EventControl.Warn(this.LogName, ex.Message);
            }
        }
        
        /// <summary>
        /// Placeholder for new functinality to send notification to callsign when it is not the originator.
        /// Use messageToOriginator.Callsign as key to find callsign node.
        /// Meanwhile this always shows the orignator, so no message is sent.
        /// </summary>
        /// <returns>Node name.</returns>
        private string DummyNodeNameForCallsign()
        {
            return this.resourceStatus.Destination;
        }

        /// <summary>
        /// This gets called for home station and for callsign.
        /// Only send a message if the destination is not the originator.
        /// </summary>
        /// <param name="destination">Name of home station or callsign.</param>
        /// <param name="messageToOriginator">Message to copy from.</param>
        private void SendCopyIfDestinationIsNotOriginator(string destination, GD92Msg20 messageToOriginator)
        {
            if (destination != messageToOriginator.Destination)
            {
                GD92Msg20 message = messageToOriginator.CopyAndSetGatewayOriginal(this.resourceStatus);
                message.Destination = destination;
                EventControl.Info(this.LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Sending copy of {0} to {1}",
                    messageToOriginator.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    destination));
                this.MessageSender.SendMessage(message);
            }
        }
    }
}

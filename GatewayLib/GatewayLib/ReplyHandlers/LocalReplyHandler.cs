﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Handle reply sent to the gateway node itself. This will be needed if messages
    /// are sent from the gateway (possibly for test purposes).
    /// </summary>
    internal class LocalReplyHandler : ReplyHandler
    {
        private readonly GD92Msg51 nak;
        private readonly GD92Msg20 resourceStatus;

        /// <summary>
        /// Initialises a new instance of the <see cref="LocalReplyHandler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Reply message.</param>
        public LocalReplyHandler(GD92Message message)
            : base(message)
        {
            this.LogName = ReplyHandler.NameOfLocalReplyHandler;
            if (message.MsgType == GD92MsgType.Nak)
            {
                this.nak = message as GD92Msg51;
            }
            
            if (message.MsgType == GD92MsgType.ResourceStatus)
            {
                this.resourceStatus = message as GD92Msg20;
            }
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoLogging()
        {
            base.DoLogging();
            if (this.nak != null)
            {
                EventControl.Note(this.LogName, this.nak.ToXml());
            }
            
            if (this.resourceStatus != null)
            {
                EventControl.Note(this.LogName, this.resourceStatus.ToXml());
            }
        }

        /// <summary>
        /// Do nothing.
        /// </summary>
        protected override void ProcessReply()
        {
        }
    }
}

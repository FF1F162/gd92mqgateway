﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Provide a method to return a reply handler of the right type.
    /// </summary>
    internal class ReplyHandlerFactory
    {
        private readonly IFilter filter;
        private readonly IHomeStations homeStations;

        /// <summary>
        /// Initialises a new instance of the <see cref="ReplyHandlerFactory"/> class.
        /// </summary>
        /// <param name="filter">Component to find direction of transmission.</param>
        /// <param name="homeStations">Component that links callsigns and stations.</param>
        public ReplyHandlerFactory(IFilter filter, IHomeStations homeStations)
        {
            this.filter = filter;
            this.homeStations = homeStations;
            System.Diagnostics.Debug.Assert(filter != null, "To check a filter is provided");
            System.Diagnostics.Debug.Assert(homeStations != null, "To check home station data is provided");
        }

        /// <summary>
        /// Return the type of handler needed for the reply.
        /// </summary>
        /// <param name="message">Reply message.</param>
        /// <returns>Handler appropriate for the reply.</returns>
        internal IReplyHandler HandlerForReply(GD92Message message)
        {
            var direction = this.filter.DirectionOfMessage(message);
            return (direction == DirectionOfTransmission.Local) ?
                new LocalReplyHandler(message) :
                this.HandlerForReplyType(message);
        }
        
        private IReplyHandler HandlerForReplyType(GD92Message message)
        {
            IReplyHandler handler = null;
            GD92MsgType messageType = message.MsgType;
            switch (messageType)
            {
                case GD92MsgType.ResourceStatus:
                    handler = new Reply20Handler(message, this.homeStations);
                    break;
                case GD92MsgType.Ack:
                    handler = new ReplyHandler(message);
                    break;
                case GD92MsgType.Nak:
                    handler = new Reply51Handler(message);
                    break;
                default:
                    handler = new ReplyHandler(message);
                    break;
            }
            
            return handler;
        }
    }
}

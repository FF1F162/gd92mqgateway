﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Provides methods to log and process replies.
    /// </summary>
    internal interface IReplyHandler
    {
        void LogReplyReceived(IFilter filter);
        
        void ProcessReplyReceived(IGD92MessageSender messageSender);
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Reply handler for Msg51 Nak.
    /// </summary>
    internal class Reply51Handler : ReplyHandler
    {
        private readonly GD92Msg51 nak;

        /// <summary>
        /// Initialises a new instance of the <see cref="Reply51Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Reply message (type 51).</param>
        public Reply51Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Reply51";
            this.nak = message as GD92Msg51;
            System.Diagnostics.Debug.Assert(this.nak != null, "To check the cast worked");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoLogging()
        {
            base.DoLogging();
            EventControl.Note(this.LogName, this.nak.ToXml());
        }

        /// <summary>
        /// If the codes are valid send a similar acknowledgement to the original sender
        /// The timeout code is not valid GD-92 but in this case there is no sense in forwarding anything
        /// as the message will (eventually) time out for the sender.
        /// (this may be why there is no official Nak reason for timeout).
        /// </summary>
        protected override void ProcessReply()
        {
            if (this.TransmissionTimedOut())
            {
                this.AuditTransmissionTimedOutOrInvalidNak(this.nak.Reason);
            }
            else
            {
                try
                {
                    this.nak.Validate();
                    this.ForwardNakToOriginalSender();
                }
                catch (ArgumentOutOfRangeException e)
                {
                    EventControl.Warn(this.LogName, e.Message);
                    this.AuditTransmissionTimedOutOrInvalidNak("invalid");
                }
            }
        }
        
        /// <summary>
        /// Test if the Nak indicates transmission timed out.
        /// </summary>
        /// <returns>True if Nak reason code indicates timeout.</returns>
        private bool TransmissionTimedOut()
        {
            return this.nak.ReasonCodeSet == (int)GD92ReasonCodeSet.General &&
                   this.nak.ReasonCode == (int)GD92GeneralReasonCode.TransmissionTimedOut;
        }
        
        /// <summary>
        /// Prepare a dummy reply and log a transit audit.
        /// </summary>
        /// <param name="reason">Explanation for dummy reply.</param>
        private void AuditTransmissionTimedOutOrInvalidNak(string reason)
        {
            GD92Msg51 dummyReply = this.PrepareReply();
            dummyReply.SentAt = DateTime.UtcNow;
            this.AuditTransitRecord(dummyReply, reason, false);
        }
        
        /// <summary>
        /// Create a Nak message for the message originally received by the gateway.
        /// If the Nak received comes from the original destination populate source and destination
        /// from the original message - to make sure the port number is the same.
        /// If the Nak comes from an intervening node (e.g. from MDG reporting no bearer),
        /// copy the destination and source and message details from the Nak received.
        /// Source is used but other header details are copied from the original received frame during encoding.
        /// </summary>
        /// <returns>Nak to be returned to original sender.</returns>
        private GD92Msg51 PrepareReply()
        {
            var reply = new GD92Msg51(this.GatewayOriginal);
            if (this.nak.FromAddress.Node == this.GatewayOriginal.DestAddress.Node)
            {
                this.PopulateSourceAndDestinationFromOriginal(reply);
            }
            else
            {
                reply.Destination = this.nak.Destination;
                reply.DestAddress = this.nak.DestAddress;
                reply.FromAddress = this.nak.FromAddress;
                reply.From = this.nak.From;
            }

            reply.OriginalDestination = this.GatewayOriginal.DestAddress;
            reply.ReasonCodeSet = this.nak.ReasonCodeSet;
            reply.ReasonCode = this.nak.ReasonCode;
            return reply;
        }
        
        private void ForwardNakToOriginalSender()
        {
            GD92Msg51 reply = this.PrepareReply();
            string action = this.nak.Reason;
            if (this.GatewayOriginal.AckRequired)
            {
                this.MessageSender.SendMessage(reply);
            }
            else
            {
                action += " not sent";
            }
            
            this.AuditTransitRecord(reply, action, false);
        }
    }
}

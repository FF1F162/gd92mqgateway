﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.GD92;
    
    /// <summary>
    /// Used to store data about the home station of a callsign.
    /// Move static validation etc methods here from HomeStations so they can be tested more easily.
    /// </summary>
    public class HomeStation
    {
        public const string UnknownNodeName = "NodeUnknown";
        
        private readonly string nodeName;
        private readonly string stationCode;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="HomeStation"/> class.
        /// </summary>
        public HomeStation()
        {
            this.stationCode = LogConfiguration.DefaultHomeStationCode;
            this.nodeName = UnknownNodeName;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="HomeStation"/> class.
        /// </summary>
        /// <param name="code">Two-character station code.</param>
        /// <param name="nodeNumber">Node number.</param>
        public HomeStation(string code, string nodeNumber)
        {
            var trimChars = new char[] { ' ', '"' };
            this.stationCode = ExtractStationCode(code.Trim(trimChars));
            this.nodeName = BuildNodeName(nodeNumber.Trim(trimChars));
        }
        
        /// <summary>
        /// As created automatically by GD92Lib from node number e.g. Node702.
        /// </summary>
        /// <value>Name of node.</value>
        public string NodeName 
        { 
            get
            { 
                return this.nodeName; 
            } 
        }
        
        /// <summary>
        /// Two-character station code.
        /// </summary>
        /// <value>Station code.</value>
        public string Code
        {
            get
            {
                return this.stationCode;
            }
        }

        /// <summary>
        /// The station code is the last two characters of the station name.
        /// </summary>
        /// <param name="station">Station name.</param>
        /// <returns>Code (last two characters of the name).</returns>
        public static string ExtractStationCode(string station)
        {
            ValidateStationName(station);
            int codeIndex = station.Length - LogConfiguration.StationCodeLength;
            return station.Substring(codeIndex);
        }

        /// <summary>
        /// Check that the name length corresponds to station code (2), Kent scheme (3)
        /// or national (FJK plus code).
        /// In the Kent scheme station codes are letters for Over-The-Border (OTB) and
        /// temporary stations e.g. ZMA. These codes may appear in Msg5 request from IPSE.
        /// In the national scheme, OTB stations can have the same code as Kent stations
        /// so national station names are not suitable unless they are used in full
        /// (and this requires code change in IPSE and in HomeStations.cs).
        /// </summary>
        /// <param name="station">Station code, group letter and code (e.g. S60) or national name (e.g. FJK60).</param>
        public static void ValidateStationName(string station)
        {
            if (station.Length != LogConfiguration.KentStationNameLength &&
                station.Length != LogConfiguration.StandardStationNameLength &&
                station.Length != LogConfiguration.StationCodeLength)
            {
                throw new GatewayException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Station name {0} has unexpected length.",
                    station));
            }

            if (station.Length == LogConfiguration.StandardStationNameLength &&
                !station.StartsWith(LogConfiguration.KentCallsignPrefix, StringComparison.Ordinal))
            {
                throw new GatewayException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Station name {0} does not begin FJK. Use the Kent two-letter code for OTB stations.",
                    station));
            }
        }

        /// <summary>
        /// Build a name by adding 'Node' in front of the number supplied in the configuration. 
        /// This is the same as the name generated automatically when the first message arrives from the node.
        /// This also caters for non-numeric names in case the station node is configured in the Registry
        /// or the name in the configuration is already in the 'Node702' format.
        /// </summary>
        /// <param name="node">Node number or name.</param>
        /// <returns>N plus node number or name unchanged.</returns>
        private static string BuildNodeName(string node)
        {
            string nodeName;
            if (char.IsDigit(node[0]))
            {
                nodeName = GD92Component.NodeNamePrefix + node;
            }
            else
            {
                nodeName = node;
            }

            return nodeName;
        }
    }
}

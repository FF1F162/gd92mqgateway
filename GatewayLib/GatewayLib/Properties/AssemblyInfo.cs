﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GatewayLib")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Thales")]
[assembly: AssemblyProduct("GatewayLib")]
[assembly: AssemblyCopyright("Copyright © Thales 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3f752e5d-f46d-4800-8b0c-406f25328d33")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.41.0")]
[assembly: AssemblyFileVersion("1.0.41.0")]

[assembly: InternalsVisibleTo("GatewayLib.NUnitTests")]

[assembly: System.CLSCompliant(true)]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("GatewayLibTests")]

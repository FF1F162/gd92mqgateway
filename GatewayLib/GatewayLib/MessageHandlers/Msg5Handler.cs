﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Resource status request.
    /// </summary>
    internal class Msg5Handler : MessageHandler
    {
        private readonly GD92Msg5 resourceStatusRequest;
        private readonly IHomeStations homeStations;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg5Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <param name="homeStations">Component to obtain callsigns at each station.</param>
        public Msg5Handler(GD92Message message, IHomeStations homeStations)
            : base(message)
        {
            this.LogName = "GW_Msg5";
            this.resourceStatusRequest = message as GD92Msg5;
            System.Diagnostics.Debug.Assert(this.resourceStatusRequest != null, "message must cast to GD92Msg5");
            this.homeStations = homeStations;
            this.DetermineTypeOfRequest();
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.resourceStatusRequest.ToXml());
        }

        /// <summary>
        /// If the message is a station request (as determined when constructing the handler),
        /// reply with Ack then forward a separate Msg5 for each callsign at the station (none if not found).
        /// These messages use sequence numbers generated by the gateway and Storm uses the same number in the Msg20 reply.
        /// To avoid confusion with messages that use sequence numbers generated by Storm, the Msg20 MUST be treated as a reply.
        /// When Msg20 messages arrive at the gateway in reply, the data must be sent in a new message to the original sender.
        /// Otherwise (if the callsign appears actually to be a callsign) just send a copy of the message to the destination
        /// When the Msg20 arrives at the gateway in reply, the Reply20Handler sends the data in a reply to the original sender.
        /// Note: the constructor sets the StationRequest flag.
        /// Note: the Reply20Handler uses the StationRequest flag to show (if set) that the Msg20 should be sent new (not as a reply).
        /// </summary>
        protected override void ProcessMessage()
        {
            if (this.resourceStatusRequest.StationRequest)
            {
                this.AcknowledgeWithAck();
                this.SendRequestForEachCallsignAtStation(this.resourceStatusRequest.Callsign);
            }
            else
            {
                this.SendRequestForSingleCallsign();
            }
        }

        /// <summary>
        /// A four-character name is probably the core of a (Kent) callsign. Help the operator by reinstating it.
        /// (Core callsign names are seen repeatedly in logs of HomeStation exceptions.).
        /// </summary>
        /// <param name="callsign">Full or short callsign name.</param>
        /// <returns>Unchanged callsign (if full) or new string with FJK prepended (if core).</returns>
        private static string AddFJKtoCallsignCore(string callsign)
        {
            string returnedCallsign = callsign;
            if (callsign.Length == LogConfiguration.CallsignCoreLength)
            {
                returnedCallsign = LogConfiguration.KentCallsignPrefix + callsign;
            }

            return returnedCallsign;
        }

        /// <summary>
        /// Full callsign name is 7 characters (FJK plus core). Assume a name of 4 characters is a callsign core.
        /// Treat anything else as a station request.
        /// </summary>
        private void DetermineTypeOfRequest()
        {
            System.Diagnostics.Debug.Assert(!this.resourceStatusRequest.StationRequest, "callsign request is expected by default");
            if (this.resourceStatusRequest.Callsign.Length < LogConfiguration.StandardCallsignLength &&
                this.resourceStatusRequest.Callsign.Length != LogConfiguration.CallsignCoreLength)
            {
                this.resourceStatusRequest.StationRequest = true;
            }
        }

        /// <summary>
        /// Send a status request for each callsign as if sent by the original sender.
        /// Set SendWithNoAckRequired = true to prevent Storm sending Ack (as well as Msg20).
        /// Set SendWithNoReplyExpected = false so this Msg5 is saved pending reply.
        /// </summary>
        /// <param name="station">Name of station.</param>
        private void SendRequestForEachCallsignAtStation(string station)
        {
            Collection<string> callsigns = this.homeStations.CallsignsWithHomeStation(station);
            foreach (string callsign in callsigns)
            {
                GD92Msg5 newMessage = this.resourceStatusRequest.Copy();
                newMessage.Callsign = callsign;
                newMessage.SendWithNoAckRequired = true;
                newMessage.SendWithNoReplyExpected = false;
                this.MessageSender.SendMessage(newMessage);
            }
        }

        /// <summary>
        /// Send a status request for the callsign as if sent by the orignal sender.
        /// Copy the original message and add FJK if necessary.
        /// Default settings mean this Msg5 is saved pending reply and AckRequired is set.
        /// When Storm sends Ack, TransmittedFrames ignores it (because the Ack matches a Msg5 and Msg20 is expected).
        /// But sometimes Storm sends Ack AFTER the Msg20. So to avoid the 'no matching frame' warning
        /// set the flags as for the station request.
        /// </summary>
        private void SendRequestForSingleCallsign()
        {
            GD92Msg5 newMessage = this.resourceStatusRequest.Copy();
            newMessage.Callsign = AddFJKtoCallsignCore(newMessage.Callsign);
            newMessage.SendWithNoAckRequired = true;
            newMessage.SendWithNoReplyExpected = false;
            this.MessageSender.SendMessage(newMessage);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Resource status message (from Storm) addressed to the MDT to notify change of status
    /// Forward to MDT and also to SEE.
    /// </summary>
    internal class Msg20Handler : MessageHandler
    {
        private readonly GD92Msg20 resourceStatus;
        private readonly IHomeStations homeStations;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg20Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <param name="homeStations">Component to provide home statio code for the callsign.</param>
        public Msg20Handler(GD92Message message, IHomeStations homeStations)
            : base(message)
        {
            this.LogName = "GW_Msg20";
            this.resourceStatus = message as GD92Msg20;
            System.Diagnostics.Debug.Assert(this.resourceStatus != null, "to show the cast has succeeded");
            this.homeStations = homeStations;
        }

        /// <summary>
        /// Add tokenized data to the Remarks field rather as MOBS does.
        /// The Err field may need to be present before the IPSE or MDT allows status update.
        /// The Hm field controls whether the callsign is in the local list or other on the IPSE.
        /// </summary>
        /// <param name="message">Message being prepared.</param>
        /// <param name="homeStations">Component to provide home station.</param>
        public static void PopulateRemarksField(GD92Msg20 message, IHomeStations homeStations)
        {
            string station = homeStations.HomeStationCodeForCallsign(message.Callsign);
            message.Remarks = string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "\r\n|Err:{0}\r\n|Hm:{1}",
                LogConfiguration.ErrorStatusOk,
                station);
        }
        
        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.resourceStatus.ToXml());
        }

        /// <summary>
        /// Send a copy of the message to the destination and to the home station.
        /// </summary>
        protected override void ProcessMessage()
        {
            GD92Msg20 newMessage = this.resourceStatus.Copy();
            PopulateRemarksField(newMessage, this.homeStations);
            this.MessageSender.SendMessage(newMessage);
            this.SendCopyToHomeStation(newMessage);
        }
        
        /// <summary>
        /// Send a copy of the message sent to the MDT to the home station of the callsign.
        /// Add a log to help with diagnosis if the destination in the HomeStation data
        /// is not in the router configuration.
        /// </summary>
        /// <param name="messageToCallsign">Message to be copied.</param>
        private void SendCopyToHomeStation(GD92Msg20 messageToCallsign)
        {
            string destination = this.homeStations.HomeStationNodeNameForCallsign(messageToCallsign.Callsign);
            if (destination != HomeStation.UnknownNodeName)
            {
                GD92Msg20 stationMessage = messageToCallsign.CopyAndSetGatewayOriginal(this.resourceStatus);
                stationMessage.Destination = destination;
                EventControl.Info(this.LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Sending copy of {0} to home station {1}",
                    messageToCallsign.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    stationMessage.Destination));
                this.MessageSender.SendMessage(stationMessage);
            }
        }
    }
}

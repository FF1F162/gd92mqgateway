﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using System.Text;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Represents Storm prompt attendance structure: comma separated callsign data and address elements.
    /// See Storm SRB Insert 2954.
    /// Comma is used to separate both address and attribute elements.
    /// Normally each callsign has attributes [x:x] but sometimes this is missing (if the callsign
    /// is mobilised by drag and drop) so use FJK to separate callsign and address elements.
    /// Callsign data correspond to a GD92Msg6Resource object - attributes are handled as commentary.
    /// </summary>
    public class StormAttendance
    {
        private const string AttributesStart = "[";
        private const string AttributesEnd = "]";

        private readonly char[] trimCharacters = new char[] { '\r', '\n', ' ', ',' };
        private readonly char[] elementSeparator = new char[] { ',' };
        private readonly char[] resourceSeparator = new char[] { ']' };
        private readonly char[] attributeSeparators = new char[] { ']', '[', ',', ':', ' ' };

        private readonly StringBuilder addressBuilder;
        private readonly Collection<GD92Msg6Resource> resources;

        private readonly string attendance;
        private string resourcesWithAttributes;
        private string plainResourcesAndAddress;
        private StringBuilder attributeBuilder;

        /// <summary>
        /// Initialises a new instance of the <see cref="StormAttendance"/> class.
        /// Constructor extracts data from the attendance string to populate the backing fields.
        /// </summary>
        /// <param name="attendance">First or Second Attendance Prompt.</param>
        public StormAttendance(string attendance)
        {
            this.attendance = attendance;
            this.addressBuilder = new StringBuilder();
            this.resources = new Collection<GD92Msg6Resource>();
            this.SeparateCallsignsWithAttributes();
            this.ExtractResourcesWithAttributes();
            this.ExtractResourcesAndAddress();
        }

        /// <summary>
        /// Address extracted from Storm attendance data.
        /// </summary>
        /// <value>Attendance address.</value>
        public string Address 
        {
            get 
            { 
                return this.addressBuilder.ToString(); 
            } 
        }

        /// <summary>
        /// List of resources extracted from Storm attendance data.
        /// Each resource comprises callsign and optional commentary.
        /// The commentary shows -  [ comma-separated list of skills or equipment : support text ].
        /// </summary>
        /// <value>Resources required to attend.</value>
        public Collection<GD92Msg6Resource> Resources 
        { 
            get 
            { 
                return this.resources; 
            } 
        }

        /// <summary>
        /// Sending a malformed callsign is likely to give the IPSE or MDT problems.
        /// </summary>
        /// <param name="callsign">Callsign to check.</param>
        private static void CheckCallsign(string callsign)
        {
            GD92Message.ValidateCallsign("callsign", callsign);
            foreach (char character in callsign)
            {
                if (!char.IsLetterOrDigit(character))
                {
                    throw new GatewayException(string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Callsign {0} contains unexpected character {1}",
                        callsign,
                        character));
                }
            }
        }

        /// <summary>
        /// Divide the attendance into two: callsigns (with attributes) and anything else.
        /// Sometimes the attendance shows plain callsigns (with no attributes) followed by the address.
        /// </summary>
        private void SeparateCallsignsWithAttributes()
        {
            int endOfAttibutesIndex = this.attendance.LastIndexOf(AttributesEnd, StringComparison.Ordinal);
            if (endOfAttibutesIndex > 0)
            {
                this.plainResourcesAndAddress = this.attendance.Substring(endOfAttibutesIndex + 1);
                this.resourcesWithAttributes = this.attendance.Substring(0, endOfAttibutesIndex);
            }
            else
            {
                this.plainResourcesAndAddress = this.attendance;
                this.resourcesWithAttributes = string.Empty;
            }
        }

        /// <summary>
        /// Assume every callsign has attributes in the form FJKxxxx [a,b,c:ddd eee fff]
        /// or null [:] attributes.
        /// </summary>
        private void ExtractResourcesWithAttributes()
        {
            string[] resources = this.resourcesWithAttributes.Split(this.resourceSeparator);
            foreach (string resource in resources)
            {
                this.ExtractCallsignAndAttributes(resource);
            }
        }
        
        /// <summary>
        /// Use FJK to identify resource element. Anything else must be an address element.
        /// Put a comma between each element of the address in the stringbuilder.
        /// </summary>
        private void ExtractResourcesAndAddress()
        {
            string[] elements = this.plainResourcesAndAddress.Split(this.elementSeparator);
            foreach (string element in elements)
            {
                if (element.Contains(LogConfiguration.KentCallsignPrefix))
                {
                    this.ExtractCallsignAndAttributes(element);
                }
                else
                {
                    this.addressBuilder.AppendFormat(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "{0}{1}",
                        (this.addressBuilder.Length > 0) ? ", " : string.Empty,
                        element.Trim(this.trimCharacters));
                }
            }
        }
        
        /// <summary>
        /// Extract callsign and, optionally, attributes from the string to create a resource record
        /// and add the record to the list of resources.
        /// </summary>
        /// <param name="resource">Callsign and (maybe) attributes.</param>
        private void ExtractCallsignAndAttributes(string resource)
        {
            if (resource.Contains(AttributesStart))
            {
                this.ExtractAttributesAndCallsign(resource);
            }
            else
            {
                this.ExtractCallsignWithoutAttributes(resource);
            }
        }
        
        /// <summary>
        /// Use the [ character at the start of the attributes to identify the two parts of the field.
        /// Create a resource record and add it to the list.
        /// The resource commentary is expected to be a b c ddd eee fff (similar to MOBS output)
        /// as brackets or colon apparently upset the IPSE.
        /// </summary>
        /// <param name="resource">Callsign and attributes.</param>
        private void ExtractAttributesAndCallsign(string resource)
        {
            int attributeIndex = resource.IndexOf(AttributesStart, StringComparison.Ordinal);
            string callsign = resource.Substring(0, attributeIndex);
            callsign = callsign.Trim(this.trimCharacters);
            CheckCallsign(callsign);
            string attributes = this.ExtractPlainAttributes(resource.Substring(attributeIndex));
            this.AddResource(callsign, attributes);
        }

        /// <summary>
        /// Convert attributes into words separated by a single space (as in MOBS output)
        /// omitting brackets, commas, colon.
        /// </summary>
        /// <param name="rawAttributes">Attributes from Storm.</param>
        /// <returns>Space-separated words.</returns>
        private string ExtractPlainAttributes(string rawAttributes)
        {
            this.attributeBuilder = new StringBuilder();
            string[] attributes = rawAttributes.Split(this.attributeSeparators);
            foreach (string attribute in attributes)
            {
                this.attributeBuilder.AppendFormat(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0}{1}",
                    (this.attributeBuilder.Length > 0) ? " " : string.Empty,
                    attribute);
            }

            return this.attributeBuilder.ToString();
        }
        
        /// <summary>
        /// Create a resource with empty comment field and add it to the list.
        /// </summary>
        /// <param name="callsign">Callsign with no attributes.</param>
        private void ExtractCallsignWithoutAttributes(string callsign)
        {
            this.AddResource(callsign.Trim(this.trimCharacters), string.Empty);
        }
        
        /// <summary>
        /// Create a GD92 Msg6 resource, putting attributes in the comment field
        /// Add it to the list of resources.
        /// </summary>
        /// <param name="callsign">Trimmed callsign name.</param>
        /// <param name="attributes">Trimmed attributes.</param>
        private void AddResource(string callsign, string attributes)
        {
            if (callsign.Length > 0)
            {
                var message6Resource = new GD92Msg6Resource(callsign, attributes);
                this.Resources.Add(message6Resource);
            }
        }
    }
}

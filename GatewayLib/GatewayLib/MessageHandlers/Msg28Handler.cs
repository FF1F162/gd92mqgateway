﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Mobilise Command - forward the message data unchanged.
    /// </summary>
    internal class Msg28Handler : MessageHandler
    {
        private readonly GD92Msg28 peripheralStatusMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg28Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg28Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg28";
            this.peripheralStatusMessage = message as GD92Msg28;
            System.Diagnostics.Debug.Assert(this.peripheralStatusMessage != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.peripheralStatusMessage.ToXml());
        }

        /// <summary>
        /// Send a copy of the message to the destination.
        /// </summary>
        protected override void ProcessMessage()
        {
            GD92Msg1 newMessage = this.peripheralStatusMessage.Copy();
            this.MessageSender.SendMessage(newMessage);
        }
    }
}

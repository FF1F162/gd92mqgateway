﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Resource status request in Storm Emulator - reply with Ack, if required, and Msg20 with hard-coded data.
    /// </summary>
    internal class Msg20HandlerInStormEmulator : LocalMessageHandlerForEmulator
    {
        private readonly GD92Msg20 resourceStatus;
        private readonly IHomeStations homeStations;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg20HandlerInStormEmulator"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <param name="homeStations">Component to obtain callsigns at each station.</param>
        public Msg20HandlerInStormEmulator(GD92Message message, IHomeStations homeStations)
            : base(message)
        {
            this.LogName = "GW_Storm_Msg20";
            this.resourceStatus = message as GD92Msg20;
            System.Diagnostics.Debug.Assert(this.resourceStatus != null, "message must cast to GD92Msg20");
            this.homeStations = homeStations;
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.resourceStatus.ToXml());
        }

        /// <summary>
        /// Send Msg20 with status echoing the status in the received message.
        /// Potentially, as for Msg5HandlerInStormEmulator, use HomeStations as a way of getting configuration for tests.
        /// To keep FxCop happy for now, use it for a log message.
        /// </summary>
        protected override void AcknowledgeReceivedMessage()
        {
            string homeStationCode = this.homeStations.HomeStationCodeForCallsign(this.resourceStatus.Callsign);
            if (homeStationCode.Equals(LogConfiguration.DefaultHomeStationCode, StringComparison.Ordinal))
            {
                EventControl.Info(this.LogName, "No home station for callsign");
            }

            this.SendResourceStatusAsReply();
        }

        /// <summary>
        /// Follow the pattern of AcknowledgeWithAck.
        /// Note that since this is sent as a reply there is no acknowledgement required and none expected
        /// (this happens automatically with no need to set the SendWith... flags in the message).
        /// </summary>
        private void SendResourceStatusAsReply()
        {
            var reply = new GD92Msg20(this.resourceStatus); // TODO remove assertion that argument is Msg5
            this.PopulateMessageData(reply);
            this.PopulateDestinationFromReceivedMessage(reply);
            this.PopulateSourceFromReceivedMessage(reply);

            this.MessageSender.SendMessage(reply);
            this.AuditReply(reply, "res", this.resourceStatus.ManualAck);
        }

        /// <summary>
        /// Populate the callsign field from the original Resource Status message.
        /// The gateway is expected to replace Remarks with home station data.
        /// Populate status from the original Resource Status message.
        /// </summary>
        /// <param name="copy">Target of copy operation.</param>
        private void PopulateMessageData(GD92Msg20 copy)
        {
            copy.Callsign = this.resourceStatus.Callsign;
            copy.StatusCode = this.resourceStatus.StatusCode;
            copy.Remarks = "Remarks set by Storm emulator.";
        }
    }
}

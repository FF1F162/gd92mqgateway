﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Handle messages addressed to the gateway (these are not expected).
    /// </summary>
    public class LocalMessageHandler : MessageHandler
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="LocalMessageHandler"/> class.
        /// </summary>
        /// <param name="message">GD92 message of any kind.</param>
        public LocalMessageHandler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Local";
        }

        /// <summary>
        /// Log message content in a warning. No messages are expected direct to the gateway.
        /// This does not actually log XML - cast to message type would be needed.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Warn(this.LogName, this.ReportSerialNumberTypeAndAddresses());
        }

        /// <summary>
        /// Message to gateway is not expected.
        /// Nak as not accepted, since this is probably a mistake by the sender.
        /// </summary>
        protected override void ProcessMessage()
        {
            this.AcknowledgeWithNakNotAccepted();
        }
    }
}

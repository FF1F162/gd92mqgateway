﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Handle text messages from the network (nodes based on RSC) - do not forward to Storm
    /// - use base ProcessMessage to send Ack.
    /// If there is a requirement to change this (to send multi-block text to Storm)
    /// a new type 27 encoder will be required to provide a character count in each frame
    /// (details in GD92Lib.FrameDecoder.cs).
    /// </summary>
    internal class Msg27FromRscHandler : MessageHandler
    {
        private readonly GD92Msg27 textMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg27FromRscHandler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg27FromRscHandler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg27FromRSC";
            this.textMessage = message as GD92Msg27;
            System.Diagnostics.Debug.Assert(this.textMessage != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.textMessage.ToXml());
        }
    }
}

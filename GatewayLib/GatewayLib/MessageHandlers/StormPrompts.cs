﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using System.Text;
 
    /// <summary>
    /// Storm has a feature called GD92 prompts, which is used to put information into the Msg2 Text field.
    /// Each prompt has a label e.g. GD92PROMPT1 = 'Incident Time:'
    /// The label can be changed through Storm user interface. If this happens (so it does not match one of the tokens here)
    /// the prompt should be included among the residue after the Location Notices (as it is not specifically excluded).
    /// See Storm SRB Insert 2912 and 2954.
    /// This class provides methods to extract the text associated with each prompt from the Msg2 Text field.
    /// </summary>
    public class StormPrompts
    {
        private const string IncidentTimeToken = "Incident Time:";
        private const string IncidentTypeToken = "Incident Type:";
        private const string AllResourcesListToken = "All resources list:";
        private const string AssistanceMessageToken = "Assistance message:";
        private const string FirstAttendanceToken = "First Attendance:";
        private const string SecondAttendanceToken = "Second Attendance:";
        private const string MapBookToken = "Map book:";
        private const string OsgrToken = "OSGR:";
        private const string TelNumberToken = "Tel Number:";
        private const string LocInfoToken = "Loc Info:";
        private const string OperatorInfoToken = "Operator Info:";
        private const string LocationNoticesToken = "Location Notices:";
        private const string LocationRiskToken = "Location Risk:";
        private const string LocationCommentToken = "Location Comment:";
        private const string ExtractedToken = "Extracted";
        private const string SplitIncidentMarker = "(1)";

        private readonly string[] fields;

        private readonly string[] fieldSeparators = new string[] { "\r\r", "\r\n\r\n", "\n\n" };
        private readonly char[] trimEndCharacters = new char[] { '\r', '\n', ' ', ',' };

        private StormAttendance firstAttendance;
        private StormAttendance secondAttendance;
        private StormAttendance allOtherResources;

        /// <summary>
        /// Initialises a new instance of the <see cref="StormPrompts"/> class.
        /// Splits the Msg2 Text field at double line breaks.
        /// Currently (Dec 2014) these are CRCR but could easily change to  CRLFCRLF.
        /// </summary>
        /// <param name="msg2Text">Text field from Msg2.</param>
        public StormPrompts(string msg2Text)
        {
            this.fields = msg2Text.Split(this.fieldSeparators, StringSplitOptions.RemoveEmptyEntries);
            this.ExtractAllOtherResources();
            this.ExtractFirstAttendance();
            this.ExtractSecondAttendance();
        }

        /// <summary>
        /// Extracted from GD92PROMPT4.
        /// The prompt name is All resources list but the list does not include resources listed in the attendance prompt.
        /// The prompt does not include the address, so this field is empty.
        /// </summary>
        /// <value>Second attendance.</value>
        public StormAttendance AllOtherResources
        {
            get
            {
                return this.allOtherResources;
            }
        }

        /// <summary>
        /// Extracted from GD92PROMPT5.
        /// Atttendance to a normal incident or to the first location of a split incident.
        /// </summary>
        /// <value>First attendance.</value>
        public StormAttendance FirstAttendance 
        { 
            get
            { 
                return this.firstAttendance; 
            } 
        }

        /// <summary>
        /// Extracted from GD92PROMPT6.
        /// Atttendance to the second location of a split incident.
        /// </summary>
        /// <value>Second attendance.</value>
        public StormAttendance SecondAttendance 
        { 
            get 
            { 
                return this.secondAttendance; 
            } 
        }

        /// <summary>
        /// Corresponds to field in Msg6 (from GD92PROMPT1).
        /// Tests show Parse works flexibly, with time and date in different order.
        /// May need to warn if the data are not valid - this should show the date of the incident or standby move.
        /// </summary>
        /// <returns>Incident time.</returns>
        public DateTime ExtractIncidentTime()
        {
            var dateAndTime = DateTime.MinValue;
            string field = this.ExtractField(IncidentTimeToken) ?? string.Empty;
            try
            {
                dateAndTime = DateTime.Parse(field);
            }
            catch (FormatException)
            {
            }

            return dateAndTime;
        }

        /// <summary>
        /// Seems to be as entered by operator so corresponds to the Msg6 field (from GD92PROMPT2)
        /// rather than to the add-on (incident code and description).
        /// </summary>
        /// <returns>Incident type.</returns>
        public string ExtractIncidentType()
        {
            return this.ExtractField(IncidentTypeToken) ?? string.Empty;
        }

        /// <summary>
        /// Corresponds to field in Msg6 (from GD92PROMPT3).
        /// Used with MakeUp.
        /// </summary>
        /// <returns>Assistance message.</returns>
        public string ExtractAssistanceMessage()
        {
            return this.ExtractField(AssistanceMessageToken) ?? string.Empty;
        }

        /// <summary>
        /// Corresponds to field in Msg6 (from GD92PROMPT7).
        /// This usually contains a single reference but for split attendance contains two.
        /// There is only room for one in Msg6 (16 chars) so if it is over length refer to the prompt.
        /// Leave the field unchanged in the array so it can be included in the location notice residue.
        /// </summary>
        /// <returns>Map book page and reference.</returns>
        public string ExtractMapBook()
        {
            string mapBook = this.ExtractWithOptionToReplaceField(MapBookToken, false) ?? string.Empty;
            if (mapBook.Contains(SplitIncidentMarker))
            {
                mapBook = "See below";
            }
            
            return mapBook;
        }

        /// <summary>
        /// Corresponds to field in Msg6 (from GD92PROMPT8).
        /// Normally duplicates what is already in the Msg2 MapRef field. For a split incident shows both references.
        /// </summary>
        /// <returns>OSGR for first attendance and second, if there is one.</returns>
        public string ExtractOSGridReference()
        {
            return this.ExtractField(OsgrToken) ?? string.Empty;
        }

        /// <summary>
        /// Corresponds to field in Msg6 (from GD92PROMPT9).
        /// Duplicates what is already in the Msg2 TelNumber field.
        /// </summary>
        /// <returns>Telephone number.</returns>
        public string ExtractTelNumber()
        {
            return this.ExtractField(TelNumberToken) ?? string.Empty;
        }

        /// <summary>
        /// Token Loc Info is probably wrong. Storm data source is Operator Info (from GD92PROMPT10).
        /// </summary>
        /// <returns>Operator information.</returns>
        public string ExtractOperatorInfo()
        {
            return this.ExtractField(LocInfoToken) ?? this.ExtractField(OperatorInfoToken) ?? string.Empty;
        }

        /// <summary>
        /// Corresponds to Notes in Msg6 Location Notices (from GD92PROMPT11).
        /// </summary>
        /// <returns>Location notices.</returns>
        public string ExtractLocationNotices()
        {
            return this.ExtractField(LocationNoticesToken) ?? string.Empty;
        }

        /// <summary>
        /// Corresponds to Risk type in Msg6 Location Notices (from GD92PROMPT12).
        /// </summary>
        /// <returns>Location risk.</returns>
        public string ExtractLocationRisk()
        {
            return this.ExtractField(LocationRiskToken) ?? string.Empty;
        }

        /// <summary>
        /// Corresponds to Operator comment in Msg6 Location Notices ? (from GD92PROMPT13).
        /// </summary>
        /// <returns>Location comment.</returns>
        public string ExtractLocationComment()
        {
            return this.ExtractField(LocationCommentToken) ?? string.Empty;
        }

        /// <summary>
        /// Location notices field holds location notices and then anything that is left over
        /// unless it duplicates a field in Msg2 or is specifically not required.
        /// Include the whole field (token: data).
        /// </summary>
        /// <returns>Location notices etc.</returns>
        public string ExtractLocationNoticesAndResidue()
        {
            var sb = new StringBuilder(Environment.NewLine);
            sb.AppendLine(this.ExtractLocationNotices());
            foreach (string field in this.fields)
            {
                if (!OmitFromLocationNotices(field))
                {
                    sb.AppendLine();
                    sb.AppendLine(field.Trim(this.trimEndCharacters));
                }
            }
            
            return sb.ToString();
        }

        /// <summary>
        /// Rules for including left-over prompt fields.
        /// </summary>
        /// <param name="field">Storm prompt or extracted marker.</param>
        /// <returns>True if prompt is required.</returns>
        private static bool OmitFromLocationNotices(string field)
        {
            return
                IsAlreadyExtractedToAFieldInMessage6(field) ||
                IsTelephoneNumber(field) ||
                IsSingleOSGridReference(field) ||
                IsSingleMapBookReference(field);
        }

        /// <summary>
        /// Field has been replaced in the array by the Extracted token.
        /// </summary>
        /// <param name="field">Storm prompt.</param>
        /// <returns>True if extracted.</returns>
        private static bool IsAlreadyExtractedToAFieldInMessage6(string field)
        {
            return field == ExtractedToken;
        }

        /// <summary>
        /// Telephone number prompt duplicates Msg2 tel_number field.
        /// </summary>
        /// <param name="field">Storm prompt.</param>
        /// <returns>True if this is a Tel Number prompt.</returns>
        private static bool IsTelephoneNumber(string field)
        {
            return field.StartsWith(TelNumberToken, StringComparison.OrdinalIgnoreCase);
        }
        
        /// <summary>
        /// Storm populates the Msg2 map_ref with the location of the attendance.
        /// Normally the OSGR prompt just repeats this, but for a split attendance it shows both locations
        /// labelled (1) and (2).
        /// </summary>
        /// <param name="field">Storm prompt.</param>
        /// <returns>True if OSGR prompt with single grid reference.</returns>
        private static bool IsSingleOSGridReference(string field)
        {
            return field.StartsWith(OsgrToken, StringComparison.OrdinalIgnoreCase) &&
                !field.Contains(SplitIncidentMarker);
        }

        /// <summary>
        /// Storm populates the Msg2 map_ref with the location of the attendance.
        /// Normally the OSGR prompt just repeats this, but for a split attendance it shows both locations
        /// labelled (1) and (2).
        /// </summary>
        /// <param name="field">Storm prompt.</param>
        /// <returns>True if Mab Book prompt with single reference.</returns>
        private static bool IsSingleMapBookReference(string field)
        {
            return field.StartsWith(MapBookToken, StringComparison.OrdinalIgnoreCase) &&
                !field.Contains(SplitIncidentMarker);
        }

        /// <summary>
        /// Search the fields for (the first) one that begins with the token (case insensitive).
        /// If found, extract the data and replace the target field with the Extracted token.
        /// </summary>
        /// <param name="token">Token that begins the target field.</param>
        /// <returns>Contents of field (not including the token) or null.</returns>
        private string ExtractField(string token)
        {
            bool replace = true;
            return this.ExtractWithOptionToReplaceField(token, replace);
        }

        /// <summary>
        /// Search the fields for (the first) one that begins with the token (case insensitive).
        /// </summary>
        /// <param name="token">Token that begins the target field.</param>
        /// <param name="replace">If true, replace target field in the array with Extracted token.</param>
        /// <returns>Contents of field (not including the token) or null.</returns>
        private string ExtractWithOptionToReplaceField(string token, bool replace)
        {
            string field = null;
            for (int i = 0; i < this.fields.Length; i++)
            {
                string tokenAndField = this.fields[i];
                if (tokenAndField.StartsWith(token, StringComparison.OrdinalIgnoreCase))
                {
                    field = tokenAndField.Substring(token.Length);
                    field = field.Trim(this.trimEndCharacters);
                    if (replace)
                    {
                        this.fields[i] = ExtractedToken;
                    }

                    break;
                }
            }
            
            return field;
        }
        
        /// <summary>
        /// Use the Storm prompt to create a StormAttendance 
        /// for atttendance to a normal incident or to the first location of a split incident.
        /// </summary>
        private void ExtractFirstAttendance()
        {
            string field = this.ExtractField(FirstAttendanceToken) ?? string.Empty;
            this.firstAttendance = new StormAttendance(field);
        }
        
        /// <summary>
        /// Use the Storm prompt to create a StormAttendance 
        /// for atttendance to the second location of a split incident.
        /// </summary>
        private void ExtractSecondAttendance()
        {
            string field = this.ExtractField(SecondAttendanceToken) ?? string.Empty;
            this.secondAttendance = new StormAttendance(field);
        }

        /// <summary>
        /// Use the Storm prompt to create a StormAttendance 
        /// for all (other) resources. The prompt name is All resources but it does not include the attendance.
        /// </summary>
        private void ExtractAllOtherResources()
        {
            string field = this.ExtractField(AllResourcesListToken) ?? string.Empty;
            this.allOtherResources = new StormAttendance(field);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Resource status message from SEE or MDT, usually to request change of callsign status.
    /// Forward status update to Storm but not AVL-only or Node-update messages.
    /// </summary>
    internal class Msg20FromRscHandler : MessageHandler
    {
        private readonly GD92Msg20 resourceStatus;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg20FromRscHandler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg20FromRscHandler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg20FromRsc";
            this.resourceStatus = message as GD92Msg20;
            System.Diagnostics.Debug.Assert(this.resourceStatus != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            if (this.MessageShouldBeLoggedAndForwarded())
            {
                EventControl.Filter(this.LogName, this.resourceStatus.ToXml());
            }
            else
            {
                EventControl.Note(this.LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "ResourceStatus not forwarded - {0}",
                    Enum.GetName(typeof(AvlType), this.resourceStatus.AvlType)));
            }
        }

        /// <summary>
        /// Send a copy of the message to the destination.
        /// </summary>
        protected override void ProcessMessage()
        {
            if (this.MessageShouldBeLoggedAndForwarded())
            {
                GD92Msg20 newMessage = this.resourceStatus.Copy();
                newMessage.Remarks = TruncateRemarksAfterTimeDate(newMessage.Remarks);
                this.MessageSender.SendMessage(newMessage);
            }
            else
            {
                this.AcknowledgeWithAck();
            }
        }

        /// <summary>
        /// As a precaution do not send information that is not used or expected by Storm.
        /// </summary>
        /// <param name="remarks">Remarks field.</param>
        /// <returns>Time and date (only) from remarks field.</returns>
        private static string TruncateRemarksAfterTimeDate(string remarks)
        {
            int expectedLength = LogConfiguration.Msg20RemarksTimeDateFormat.Length;
            string truncated = remarks;
            if (remarks.Length > expectedLength)
            {
                truncated = remarks.Substring(0, expectedLength);
            }
            
            return truncated; 
        }
        
        /// <summary>
        /// Do not forward AVL or node update messages.
        /// </summary>
        /// <returns>True or false.</returns>
        private bool MessageShouldBeLoggedAndForwarded()
        {
            return this.resourceStatus.AvlType != (int)AvlType.AvlOnly &&
                   this.resourceStatus.AvlType != (int)AvlType.NodeUpdate;
        }
    }
}

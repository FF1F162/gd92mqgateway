﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Log Update message from Storm.
    /// Provide a sequence number so the MDT displays the text in the correct place.
    /// </summary>
    internal class Msg22Handler : MessageHandler
    {
        public const string SequenceNumberFormat = "MMddHHmmss";

        private readonly GD92Msg22 logUpdate;

        /// <summary>
        /// This is used as a sequence number, inserted at the beginning of the Update field.
        /// MOBS maintains a series for each incident and provides synchronization facilities. Storm does not.
        /// Instead provide a number based on date and time.
        /// </summary>
        private string sequenceNumber;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg22Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg22Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg22";
            this.logUpdate = message as GD92Msg22;
            System.Diagnostics.Debug.Assert(this.logUpdate != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.logUpdate.ToXml());
        }

        /// <summary>
        /// Send a copy of the message to the destination
        /// Copy is type 27 as the MDT probably expects synchronization information in Msg22
        /// and the gateway cannot easily provide that.
        /// </summary>
        protected override void ProcessMessage()
        {
            var newMessage = new GD92Msg22();
            this.CopyDataTo(newMessage);
            this.MessageSender.SendMessage(newMessage);
            this.LogNewMessage(newMessage);
        }
        
        /// <summary>
        /// Copy data from the received message to the new message to be sent on to the destination.
        /// The text in the Update field must be prepended with a sequence number.
        /// </summary>
        /// <param name="copy">Message to be sent.</param>
        private void CopyDataTo(GD92Msg22 copy)
        {
            copy.CopySourceDestManAckAndOriginalFrom(this.logUpdate);
            copy.Callsign = this.logUpdate.Callsign;
            copy.IncidentNumber = this.logUpdate.IncidentNumber;
            copy.Update = this.ConstructUpdateField();
        }

        /// <summary>
        /// Generate a sequence number and insert it in front of the update text provided by Storm.
        /// Make sure the result fits the space allowed in Msg22. 
        /// This may truncate the message by up to 12 characters.
        /// </summary>
        /// <returns>Sequence number and update text, possibly truncated.</returns>
        private string ConstructUpdateField()
        {
            this.GenerateSequenceNumber();
            string update = this.sequenceNumber + ": " + this.logUpdate.Update;
            return (update.Length > GD92Message.MaxUpdateLength) ?
                update.Substring(0, GD92Message.MaxUpdateLength) :
                update;
        }

        /// <summary>
        /// Provide a number that fits within the scope allowed by the MDT i.e. less than 7FFFFFFF (2147483647).
        /// </summary>
        private void GenerateSequenceNumber()
        {
            this.sequenceNumber = Msg22Sequence.SequenceForIncident(this.logUpdate.IncidentNumber);
        }

        /// <summary>
        /// Log the new message as XML if configured to do so.
        /// </summary>
        /// <param name="message">Message 22 constructed from Message 22.</param>
        private void LogNewMessage(GD92Msg22 message)
        {
            if (EventControl.LogEventLevel >= LogLevel.Filter &&
                this.Filter.XmlLogRequired(message))
            {
                EventControl.Filter(this.LogName, message.ToXml());
            }
        }
    }
}

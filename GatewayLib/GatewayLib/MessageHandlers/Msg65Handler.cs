﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Printer Status - forward the message data unchanged.
    /// </summary>
    internal class Msg65Handler : MessageHandler
    {
        private readonly GD92Msg65 printerStatusMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg65Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg65Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg65";
            this.printerStatusMessage = message as GD92Msg65;
            System.Diagnostics.Debug.Assert(this.printerStatusMessage != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.printerStatusMessage.ToXml());
        }

        /// <summary>
        /// Send a copy of the message to the destination.
        /// </summary>
        protected override void ProcessMessage()
        {
            GD92Msg65 newMessage = this.printerStatusMessage.Copy();
            this.MessageSender.SendMessage(newMessage);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Resource status request in Storm Emulator - reply with Ack, if required, and Msg20 with hard-coded data.
    /// </summary>
    internal class Msg5HandlerInStormEmulator : LocalMessageHandlerForEmulator
    {
        private readonly GD92Msg5 resourceStatusRequest;
        private readonly IHomeStations homeStations;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg5HandlerInStormEmulator"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// Set AckRequired so the acknowledgement method is called regardless of the setting in the message.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <param name="homeStations">Component to obtain callsigns at each station.</param>
        public Msg5HandlerInStormEmulator(GD92Message message, IHomeStations homeStations)
            : base(message)
        {
            this.LogName = "GW_Storm_Msg5";
            this.resourceStatusRequest = message as GD92Msg5;
            System.Diagnostics.Debug.Assert(this.resourceStatusRequest != null, "message must cast to GD92Msg5");
            this.homeStations = homeStations;

            this.AckRequired = true;
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.resourceStatusRequest.ToXml());
        }

        /// <summary>
        /// Reply with Ack if required, then also send Msg20.
        /// </summary>
        protected override void AcknowledgeReceivedMessage()
        {
            if (this.resourceStatusRequest.AckRequired)
            {
                this.AcknowledgeWithAck();
            }

            this.SendResourceStatusAsReply();
        }

        /// <summary>
        /// Follow the pattern of AcknowledgeWithAck.
        /// Note that since this is sent as a reply there is no acknowledgement required and none expected
        /// (this happens automatically with no need to set the SendWith... flags in the message).
        /// </summary>
        private void SendResourceStatusAsReply()
        {
            var reply = new GD92Msg20(this.resourceStatusRequest);
            this.PopulateMessageData(reply);
            this.PopulateDestinationFromReceivedMessage(reply);
            this.PopulateSourceFromReceivedMessage(reply);

            this.MessageSender.SendMessage(reply);
            this.AuditReply(reply, "res", this.resourceStatusRequest.ManualAck);
        }

        /// <summary>
        /// Populate the callsign field in the copy from the original Resource Status Request message.
        /// The gateway is expected to replace Remarks with home station data.
        /// Populate status according to whether or not the callsign is in home stations data.
        /// If not, status remains 0 and the gateway does not forward the message.
        /// </summary>
        /// <param name="copy">Target of copy operation.</param>
        private void PopulateMessageData(GD92Msg20 copy)
        {
            copy.Callsign = this.resourceStatusRequest.Callsign;
            string homeStationCode = this.homeStations.HomeStationCodeForCallsign(copy.Callsign);
            copy.Remarks = "Remarks set by Storm emulator.";
            if (!homeStationCode.Equals(LogConfiguration.DefaultHomeStationCode, StringComparison.Ordinal))
            {
                copy.StatusCode = (int)StatusCode.BS;
            }
        }
    }
}

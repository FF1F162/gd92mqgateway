﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Base class implements default behaviour for the interface that deals with received messages (not replies).
    /// MessageHandlerFactory supplies a subclass appropriate for message type, where the message originated
    /// and where it is addressed.
    /// Subclasses override DoLogging and ProcessMessage to implement the behaviour required.
    /// Only call DoLogging if the filter shows logging (maybe as XML) is required.
    /// </summary>
    public class MessageHandler : IMessageHandler
    {
        private readonly GD92Message receivedMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageHandler"/> class.
        /// </summary>
        /// <param name="message">Received message.</param>
        public MessageHandler(GD92Message message)
        {
            System.Diagnostics.Debug.Assert(message != null, "To check");
            this.LogName = "GW_MessageHandler";
            this.receivedMessage = message;
        }

        /// <summary>
        /// To be overwritten by sub-class.
        /// </summary>
        /// <value>Name of class (used in log).</value>
        protected string LogName { get; set; }

        /// <summary>
        /// To find out if the log is required.
        /// </summary>
        /// <value>Filter used to find out if the log is required.</value>
        protected IFilter Filter { get; set; }

        /// <summary>
        /// To send acknowledgement.
        /// </summary>
        /// <value>Component used to send reply.</value>
        protected IGD92MessageSender MessageSender { get; set; }

        /// <summary>
        /// To convert OS grid reference to map book page(s).
        /// </summary>
        /// <value>Component used to convert map reference.</value>
        protected IMapBook MapBook { get; set; }

        /// <summary>
        /// Log details of the received message (if logging is required).
        /// Default is to show type, source and destination in the log.
        /// </summary>
        /// <param name="filter">Component used to test if logging is required.</param>
        public void LogReceivedMessage(IFilter filter)
        {
            System.Diagnostics.Debug.Assert(filter != null, "To check there is a filter");
            this.Filter = filter;
            if (EventControl.LogEventLevel >= LogLevel.Filter &&
                this.Filter.LogIsRequired(this.receivedMessage))
            {
                EventControl.Filter(this.LogName, this.ReportSerialNumberTypeAndAddresses());
                if (this.Filter.XmlLogRequired(this.receivedMessage))
                {
                    this.DoXmlLogging();
                }
            }
        }
        
        /// <summary>
        /// Process the message.
        /// </summary>
        /// <param name="messageSender">Component used to send reply.</param>
        /// <param name="mapBook">Component used to provide map book reference in Msg6.</param>
        public void ProcessReceivedMessage(IGD92MessageSender messageSender, IMapBook mapBook)
        {
            this.MessageSender = messageSender;
            this.MapBook = mapBook;
            this.ProcessMessage();
        }
        
        #region Logging

        /// <summary>
        /// Override to show handler-specific information.
        /// </summary>
        protected virtual void DoXmlLogging()
        {
        }
        
        /// <summary>
        /// Return a string to report the basic details.
        /// </summary>
        /// <returns>Details for the log.</returns>
        protected string ReportSerialNumberTypeAndAddresses()
        {
            return string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Received message {0} type {1} from {2} for {3}",
                this.receivedMessage.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.receivedMessage.MsgType.ToString(),
                this.receivedMessage.FromAddress.ToString(),
                this.receivedMessage.DestAddress.ToString());
        }

        #endregion

        #region Process Message 

        /// <summary>
        /// Override for each type of message.
        /// Default is simply to acknowledge the message as if received by the destination.
        /// </summary>
        protected virtual void ProcessMessage()
        {
            this.AcknowledgeWithAck();
        }

        #endregion

        #region Acknowledgement methods

        /// <summary>
        /// Send Msg50 in acknowledgement.
        /// This is appropriate where the message cannot be handled at the destination
        /// and it has been decided to ignore it.
        /// </summary>
        protected void AcknowledgeWithAck()
        {
            var reply = new GD92Msg50(this.receivedMessage);
            this.PopulateDestinationFromReceivedMessage(reply);
            this.PopulateSourceFromReceivedMessage(reply);

            this.MessageSender.SendMessage(reply);
            this.AuditReply(reply, "ack", this.receivedMessage.ManualAck);
        }
        
        /// <summary>
        /// Send Msg51 in acknowledgement - Nak NOT ACCEPTED
        /// Source of the reply will be the local router (set in SendMessage).
        /// This is appropriate where the message type is not expected.
        /// </summary>
        protected void AcknowledgeWithNakNotAccepted()
        {
            this.AcknowledgeWithNak(GD92ReasonCodeSet.General, GD92GeneralReasonCode.NotAccepted);
        }

        /// <summary>
        /// Send Msg51 in acknowledgement
        /// Source of the reply will be the local router (set in SendMessage).
        /// </summary>
        /// <param name="set">Reason code set.</param>
        /// <param name="code">Reason code.</param>
        protected void AcknowledgeWithNak(GD92ReasonCodeSet set, GD92GeneralReasonCode code)
        {
            var reply = new GD92Msg51(this.receivedMessage);
            this.PopulateDestinationFromReceivedMessage(reply);
            reply.From = GD92Component.LocalRouterToken;
            System.Diagnostics.Debug.Assert(reply.FromAddress.Brigade == 0, "Brigade must be 0 to set default address in SendMessage");
            
            reply.OriginalDestination = this.receivedMessage.DestAddress;
            reply.ReasonCodeSet = (int)set;
            reply.ReasonCode = (int)code;

            this.MessageSender.SendMessage(reply);
            this.AuditReply(reply, code.ToString(), this.receivedMessage.ManualAck);
        }

        /// <summary>
        /// Send the reply as if it came from the destination of the original message.
        /// </summary>
        /// <param name="reply">Reply message being populated.</param>
        protected void PopulateSourceFromReceivedMessage(GD92Message reply)
        {
            reply.From = this.receivedMessage.Destination;
            reply.FromAddress = this.receivedMessage.DestAddress;
            reply.FromNodeType = this.receivedMessage.DestNodeType;
        }

        /// <summary>
        /// Send the reply to the source (From) of the original message.
        /// </summary>
        /// <param name="reply">Reply message being populated.</param>
        protected void PopulateDestinationFromReceivedMessage(GD92Message reply)
        {
            reply.Destination = this.receivedMessage.From;
            reply.DestAddress = this.receivedMessage.FromAddress;
            reply.DestNodeType = this.receivedMessage.FromNodeType;
        }

        /// <summary>
        /// Audit in a similar way to message transit.
        /// </summary>
        /// <param name="reply">Message being sent in reply.</param>
        /// <param name="reason">Purpose of reply.</param>
        /// <param name="manualAck">Manual acknowledgement setting.</param>
        protected void AuditReply(GD92Message reply, string reason, bool manualAck)
        {
            double elapsedTime = 0;
            var original = new MessageTransitData(this.receivedMessage, elapsedTime);
            elapsedTime = ReplyHandler.ElapsedTime(this.receivedMessage.ReceivedAt, ReplyHandler.TimeNowIfNotSet(reply.SentAt));
            var sentReply = new MessageTransitData(reply, elapsedTime);
            EventControl.Audit(this.LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Audit reply {0}, {1}, {2}, {3}, {4}",
                this.receivedMessage.ReceivedAt.ToLocalTime().ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                original.ToString(),
                sentReply.ToString(),
                reason,
                manualAck ? "manual" : "-"));
        }

        #endregion
    }
}

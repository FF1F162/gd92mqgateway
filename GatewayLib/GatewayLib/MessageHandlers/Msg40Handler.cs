﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// AlertCrew - if alerter group is set, forward the message data unchanged.
    /// If alerter group is not set, convert to Msg1 so IPSE can use the peripheral flags.
    /// MDT processing is the same for Msg40 and Msg1 so it accepts either for mobilisation.
    /// </summary>
    internal class Msg40Handler : MessageHandler
    {
        private readonly GD92Msg40 alertCrewMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg40Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message (type 40).</param>
        public Msg40Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg40";
            this.alertCrewMessage = message as GD92Msg40;
            System.Diagnostics.Debug.Assert(this.alertCrewMessage != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.alertCrewMessage.ToXml());
        }

        /// <summary>
        /// If the alert group is empty (two spaces) send the peripheral flags as an early mobilisation message.
        /// (This is a manual operator facility used instead of pre-alert.)
        /// Otherwise, send a copy of the message to the destination.
        /// </summary>
        protected override void ProcessMessage()
        {
            if (this.alertCrewMessage.AlertGroup.StartsWith("  ", StringComparison.Ordinal))
            {
                this.SendEarlyMobiliseCommand();
            }
            else
            {
                GD92Msg40 newMessage = this.alertCrewMessage.Copy();
                this.MessageSender.SendMessage(newMessage);
            }
        }

        /// <summary>
        /// Send the peripherals flags in a Msg1. The reason for doing this is that currently the station end (IPSE)
        /// ignores peripheral flags in Msg40 (and this avoids having to change that).
        /// </summary>
        private void SendEarlyMobiliseCommand()
        {
            var newMessage = new GD92Msg1();
            this.CopyDataTo(newMessage);
            this.MessageSender.SendMessage(newMessage);
            this.LogNewMessage(newMessage);
        }

        /// <summary>
        /// Populate fields in the copy from the original Alert Crew message.
        /// </summary>
        /// <param name="copy">Target of copy operation.</param>
        private void CopyDataTo(GD92Msg1 copy)
        {
            copy.CopySourceDestManAckAndOriginalFrom(this.alertCrewMessage);
            copy.Sounders = this.alertCrewMessage.Sounders;
            copy.Lights = this.alertCrewMessage.Lights;
            copy.Spare2 = this.alertCrewMessage.Spare2;
            copy.Spare3 = this.alertCrewMessage.Spare3;
            copy.Spare4 = this.alertCrewMessage.Spare4;
            copy.Doors = this.alertCrewMessage.Doors;
            copy.StandbySounder = this.alertCrewMessage.StandbySounder;
            copy.Appliance9 = this.alertCrewMessage.Appliance9;
            copy.Appliance1 = this.alertCrewMessage.Appliance1;
            copy.Appliance2 = this.alertCrewMessage.Appliance2;
            copy.Appliance3 = this.alertCrewMessage.Appliance3;
            copy.Appliance4 = this.alertCrewMessage.Appliance4;
            copy.Appliance5 = this.alertCrewMessage.Appliance5;
            copy.Appliance6 = this.alertCrewMessage.Appliance6;
            copy.Appliance7 = this.alertCrewMessage.Appliance7;
            copy.Appliance8 = this.alertCrewMessage.Appliance8;
            copy.Lights = this.alertCrewMessage.Lights;
        }

        /// <summary>
        /// Log the new message as XML if configured to do so.
        /// </summary>
        /// <param name="message">Message 1 constructed from Message 40.</param>
        private void LogNewMessage(GD92Msg1 message)
        {
            if (EventControl.LogEventLevel >= LogLevel.Filter &&
                this.Filter.XmlLogRequired(message))
            {
                EventControl.Filter(this.LogName, message.ToXml());
            }
        }
    }
}

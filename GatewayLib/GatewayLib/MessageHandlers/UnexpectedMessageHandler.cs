﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Message type is not expected (from this direction).
    /// </summary>
    internal class UnexpectedMessageHandler : MessageHandler
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="UnexpectedMessageHandler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public UnexpectedMessageHandler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Unexpected";
        }

        /// <summary>
        /// Type not supported - cannot interpret as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, "Message not forwarded (type not supported)");
        }

        /// <summary>
        /// Message type or direction is not expected.
        /// Nak as not accepted, since this is probably a mistake by the sender.
        /// </summary>
        protected override void ProcessMessage()
        {
            this.AcknowledgeWithNakNotAccepted();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Provide a method to return a handler of the right variety. This depends primarily
    /// on message type but also in on direction through the gateway. 
    /// LocalHandler deals with messages addressed directly to the node (gateway or emulator).
    /// </summary>
    internal class MessageHandlerFactory
    {
        private readonly IFilter filter;
        private readonly IHomeStations homeStations;

        private readonly string logName;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="MessageHandlerFactory"/> class.
        /// </summary>
        /// <param name="filter">Component to find direction of transmission.</param>
        /// <param name="homeStations">Component that links callsigns and stations.</param>
        public MessageHandlerFactory(IFilter filter, IHomeStations homeStations)
        {
            this.logName = "GW_MessageHandlerFactory";
            this.filter = filter;
            this.homeStations = homeStations;
            System.Diagnostics.Debug.Assert(filter != null, "An IFilter for logging must be provided");
            System.Diagnostics.Debug.Assert(homeStations != null, "There must be a provider of home station data");
        }

        /// <summary>
        /// True if messages directly addressed to this node are to be given handlers that behave like the RSC.
        /// </summary>
        public bool NodeIsRscEmulator { get; set; }

        /// <summary>
        /// True if messages directly addressed to this node are to be given handlers that behave like Storm.
        /// </summary>
        public bool NodeIsStormEmulator { get; set; }

        /// <summary>
        /// Return the type of handler needed for the message - depends on message type,
        /// direction of travel etc.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <returns>Handler appropriate for the message.</returns>
        internal IMessageHandler HandlerForMessage(GD92Message message)
        {
            IMessageHandler handler = null;
            var direction = this.filter.DirectionOfMessage(message);
            
            if (direction == DirectionOfTransmission.Local)
            {
                handler = this.HandlerForMessageToLocalNode(message);
            }
            else
            {
                handler = (direction == DirectionOfTransmission.ToNetwork) ?
                    this.HandlerForMessageToNetwork(message) :
                    this.HandlerForMessageFromNetwork(message);
            }
            
            return handler;
        }

        /// <summary>
        /// Allow for different emulators.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <returns>Handler appropriate for the message.</returns>
        private IMessageHandler HandlerForMessageToLocalNode(GD92Message message)
        {
            IMessageHandler handler;

            if (this.NodeIsStormEmulator)
            {
                handler = this.HandlerForMessageToLocalStormEmulator(message);
            }
            else if (this.NodeIsRscEmulator)
            {
                handler = this.HandlerForMessageToLocalRscEmulator(message);
            }
            else
            {
                handler = new LocalMessageHandler(message);
            }

            return handler;
        }
        
        /// <summary>
        /// Return a handler for message types expected to go to MDT or IPSE nodes (RSC-based).
        /// The list includes Type 6 for testing with MOBS (Storm sends Type 2 instead).
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <returns>Handler appropriate for the message.</returns>
        private IMessageHandler HandlerForMessageToNetwork(GD92Message message)
        {
            IMessageHandler handler = null;
            GD92MsgType messageType = message.MsgType;
            switch (messageType)
            {
                case GD92MsgType.MobiliseCommand:
                    handler = new Msg1Handler(message);
                    break;
                case GD92MsgType.MobiliseMessage:
                    handler = new Msg2Handler(message);
                    break;
                case GD92MsgType.MobiliseMessage6:
                    handler = new Msg6Handler(message);
                    break;
                case GD92MsgType.ResourceStatus:
                    handler = new Msg20Handler(message, this.homeStations);
                    break;
                case GD92MsgType.LogUpdate:
                    handler = new Msg22Handler(message);
                    break;
                case GD92MsgType.Text:
                    handler = new Msg27Handler(message);
                    break;
                case GD92MsgType.AlertCrew:
                    handler = new Msg40Handler(message);
                    break;
                default:
                    handler = new UnexpectedMessageHandler(message);
                    break;
            }
            
            return handler;
        }

        /// <summary>
        /// Return a handler for message types expected from MDT or IPSE nodes (RSC-based).
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <returns>Handler appropriate for the message.</returns>
        private IMessageHandler HandlerForMessageFromNetwork(GD92Message message)
        {
            IMessageHandler handler = null;
            GD92MsgType messageType = message.MsgType;
            switch (messageType)
            {
                case GD92MsgType.ResourceStatusRequest:
                    handler = new Msg5Handler(message, this.homeStations);
                    break;
                case GD92MsgType.ResourceStatus:
                    handler = new Msg20FromRscHandler(message);
                    break;
                case GD92MsgType.LogUpdate:
                    handler = new Msg22FromRscHandler(message);
                    break;
                case GD92MsgType.Text:
                    handler = new Msg27FromRscHandler(message);
                    break;
                case GD92MsgType.PeripheralStatus:
                    handler = new Msg28Handler(message);
                    break;
                case GD92MsgType.AlertStatus:
                    handler = new Msg42Handler(message);
                    break;
                case GD92MsgType.PrinterStatus:
                    handler = new Msg65Handler(message);
                    break;
                default:
                    handler = new UnexpectedMessageHandler(message);
                    break;
            }
            
            return handler;
        }

        /// <summary>
        /// Return a handler to emulate MDT or IPSE nodes (RSC-based).
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <returns>Handler appropriate for the message.</returns>
        private IMessageHandler HandlerForMessageToLocalRscEmulator(GD92Message message)
        {
            IMessageHandler handler = null;
            GD92MsgType messageType = message.MsgType;
            switch (messageType)
            {
                case GD92MsgType.MobiliseCommand:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                case GD92MsgType.MobiliseMessage6:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                case GD92MsgType.ResourceStatus:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                case GD92MsgType.LogUpdate:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                case GD92MsgType.Text:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                case GD92MsgType.AlertCrew:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                default:
                    handler = new UnexpectedMessageHandler(message);
                    break;
            }

            EventControl.Info(this.logName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "RSC emulator using {0} for {1} {2}",
                handler.ToString(),
                message.SerialNumber,
                message.MsgType));

            return handler;
        }

        /// <summary>
        /// Return a handler that emulates behaviour of Storm.
        /// </summary>
        /// <param name="message">Received message.</param>
        /// <returns>Handler appropriate for the message.</returns>
        private IMessageHandler HandlerForMessageToLocalStormEmulator(GD92Message message)
        {
            IMessageHandler handler = null;
            GD92MsgType messageType = message.MsgType;
            switch (messageType)
            {
                case GD92MsgType.ResourceStatusRequest:
                    handler = new Msg5HandlerInStormEmulator(message, this.homeStations);
                    break;
                case GD92MsgType.ResourceStatus:
                    handler = new Msg20HandlerInStormEmulator(message, this.homeStations);
                    break;
                case GD92MsgType.PeripheralStatus:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                case GD92MsgType.AlertStatus:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                case GD92MsgType.PrinterStatus:
                    handler = new LocalMessageHandlerForEmulator(message);
                    break;
                default:
                    handler = new UnexpectedMessageHandler(message);
                    break;
            }

            EventControl.Info(this.logName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Storm emulator using {0} for {1} {2}",
                handler.ToString(),
                message.SerialNumber,
                message.MsgType));

            return handler;
        }
    }
}

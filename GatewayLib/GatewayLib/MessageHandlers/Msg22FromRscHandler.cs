﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Log Update message.
    /// When sent from the RSC (in an MDT) this is a request for synchronization. Storm does not provide
    /// this facility so simply acknowledge the message (and ignore it).
    /// </summary>
    internal class Msg22FromRscHandler : MessageHandler
    {
        private readonly GD92Msg22 logUpdate;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg22FromRscHandler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg22FromRscHandler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg22FromRsc";
            this.logUpdate = message as GD92Msg22;
            System.Diagnostics.Debug.Assert(this.logUpdate != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.logUpdate.ToXml());
        }
    }
}

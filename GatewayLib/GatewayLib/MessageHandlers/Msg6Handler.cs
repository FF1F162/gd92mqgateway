﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Mobilise - Type 6 Kent special - forward the message data unchanged.
    /// This is for test purposes only. Storm sends Message 2 instead and RSC nodes do not mobilise.
    /// </summary>
    internal class Msg6Handler : MessageHandler
    {
        private readonly GD92Msg6 mobiliseMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg6Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg6Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg6";
            this.mobiliseMessage = message as GD92Msg6;
            System.Diagnostics.Debug.Assert(this.mobiliseMessage != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.mobiliseMessage.ToXml());
        }

        /// <summary>
        /// Send a copy of the message to the destination.
        /// </summary>
        protected override void ProcessMessage()
        {
            GD92Msg6 newMessage = this.mobiliseMessage.Copy();
            this.MessageSender.SendMessage(newMessage);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using System.Text;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Mobilise message - convert to Kent mobilise message
    /// prompts is Storm terminology for tagged text in the Message 2 Text field.
    /// KFRS maintain a spreadsheet showing details of the conversion required:
    /// 'Thales Gateway Msg2 to Msg6 conversion.xlsx'.
    /// </summary>
    internal class Msg2Handler : MessageHandler
    {
        private readonly GD92Msg2 mobiliseMessage;
        private readonly StormPrompts prompts;
        private StringBuilder addressBuilder;
        private string address;
        private int addressOffset;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg2Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg2Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg2";
            this.mobiliseMessage = message as GD92Msg2;
            System.Diagnostics.Debug.Assert(this.mobiliseMessage != null, "to show the cast has succeeded");
            string convertedText = Msg27Handler.ConvertToRegularLineEndings(this.mobiliseMessage.Text);
            this.prompts = new StormPrompts(convertedText);
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.mobiliseMessage.ToXml());
        }

        /// <summary>
        /// Transfer the data to Kent mobilise message and send it to the destination.
        /// </summary>
        protected override void ProcessMessage()
        {
            var newMessage = new GD92Msg6();
            this.CheckCallsignsInAttendance();
            this.CopyDataTo(newMessage);
            this.MessageSender.SendMessage(newMessage);
            this.LogNewMessage(newMessage);
        }
        
        /// <summary>
        /// Copy data from this type 2 message to a type 6.
        /// </summary>
        /// <param name="copy">Message type 6 to send in place of type 2.</param>
        private void CopyDataTo(GD92Msg6 copy)
        {
            copy.CopySourceDestManAckAndOriginalFrom(this.mobiliseMessage);
            copy.MobilisationType = this.mobiliseMessage.MobilisationType;
            copy.AssistanceMessage = this.prompts.ExtractAssistanceMessage();
            this.ResourcesListFromPrompts(copy);
            this.AllResourcesListFromPrompts(copy);
            copy.IncidentTimeAndDate = this.prompts.ExtractIncidentTime();
            copy.MobTimeAndDate = this.mobiliseMessage.MobTimeAndDate;
            copy.IncidentNumber = this.mobiliseMessage.IncidentNumber;
            copy.IncidentType = this.prompts.ExtractIncidentType();
            this.PopulateAddressAndPostcodeFields(copy);
            this.FirstAttendanceFromPrompts(copy);
            this.SecondAttendanceFromPrompts(copy);
            copy.MapBook = this.MapBook.MapBookForGridReference(this.mobiliseMessage.MapRef);
            copy.MapRef = this.mobiliseMessage.MapRef;
            copy.HowReceived = string.Empty;
            copy.TelNumber = this.mobiliseMessage.TelNumber;
            copy.OperatorInfo = this.prompts.ExtractOperatorInfo();
            copy.WaterInformation = string.Empty;
            copy.LocationNotices = this.prompts.ExtractLocationNoticesAndResidue();
            copy.AddText = string.Empty;
        }
        
        /// <summary>
        /// Storm puts the callsign(s) that are the target of the mobilisation in either
        /// the First Attendance or the Second Attendance (so try both).
        /// </summary>
        /// <param name="copy">Message type 6 to send in place of type 2.</param>
        private void ResourcesListFromPrompts(GD92Msg6 copy)
        {
            foreach (GD92Msg6Resource resource in this.prompts.FirstAttendance.Resources)
            {
                copy.ResourcesList.Add(resource);
            }
            
            foreach (GD92Msg6Resource resource in this.prompts.SecondAttendance.Resources)
            {
                copy.ResourcesList.Add(resource);
            }
        }
        
        /// <summary>
        /// Use the resources from the All resorces list prompt.
        /// Actually this does not include the resources in first or second attendance.
        /// </summary>
        /// <param name="copy">Message type 6 to send in place of type 2.</param>
        private void AllResourcesListFromPrompts(GD92Msg6 copy)
        {
            foreach (GD92Msg6Resource resource in this.prompts.AllOtherResources.Resources)
            {
                copy.AllResourcesList.Add(resource);
            }
        }
        
        /// <summary>
        /// Use the callsigns from the First Attendance prompt and the address.
        /// Use the address only if this is a first attendance (if there are no callsigns
        /// it is a second attendance and the first attendance address is required in 
        /// the incident address field).
        /// </summary>
        /// <param name="copy">Message type 6 to send in place of type 2.</param>
        private void FirstAttendanceFromPrompts(GD92Msg6 copy)
        {
            foreach (GD92Msg6Resource resource in this.prompts.FirstAttendance.Resources)
            {
                copy.FirstAttendance.Callsigns.Add(resource.Callsign);
            }
            
            if (copy.FirstAttendance.Callsigns.Count > 0)
            {
                copy.FirstAttendance.Address = this.prompts.FirstAttendance.Address;
            }
        }
        
        /// <summary>
        /// Use the callsigns from the Second Attendance prompt and the address (which is
        /// empty unless this is a mobilisation to the second location of a split incident).
        /// </summary>
        /// <param name="copy">Message type 6 to send in place of type 2.</param>
        private void SecondAttendanceFromPrompts(GD92Msg6 copy)
        {
            foreach (GD92Msg6Resource resource in this.prompts.SecondAttendance.Resources)
            {
                copy.SecondAttendance.Callsigns.Add(resource.Callsign);
            }
            
            copy.SecondAttendance.Address = this.prompts.SecondAttendance.Address;
        }
        
        /// <summary>
        /// Build an address string from fields in Msg2 (for second attendance use the
        /// address in the dummy first attendance). Use the address to populate
        /// the available fields in Msg6. Populate postcode separately (for first attendance
        /// only; for second attendance the postcode, like the rest of the Msg2 address,
        /// is for the second attendance not the incident).
        /// </summary>
        /// <param name="copy">Message type 6 to send in place of type 2.</param>
        private void PopulateAddressAndPostcodeFields(GD92Msg6 copy)
        {
            if (this.prompts.SecondAttendance.Resources.Count > 0)
            {
                this.UseFirstAttendanceAddressAsIncidentAddress();
                copy.Postcode = string.Empty;
            }
            else
            {
                this.AddressFromElements();
                copy.Postcode = this.mobiliseMessage.Postcode;
            }
            
            this.RePackageAddressIntoMsg6Elements(copy);
        }

        /// <summary>
        /// For a second attendance (at a split incident) Storm puts the second address in Msg2
        /// but supplies the incident address in the First Attendance Address field.
        /// Copy this to the address.
        /// </summary>
        private void UseFirstAttendanceAddressAsIncidentAddress()
        {
            this.address = this.prompts.FirstAttendance.Address;
        }

        /// <summary>
        /// Use all the address elements sent by Storm to create a comma-separated address.
        /// Pack it into the space available in Msg6.
        /// RSC concatenates address elements into a single address. It does not insert commas or spaces.
        /// </summary>
        /// <param name="copy">Message type 6 to send in place of type 2.</param>
        private void RePackageAddressIntoMsg6Elements(GD92Msg6 copy)
        {
            copy.Address = this.PartOfAddressInElement(GD92Message.MaxAddressLength);
            copy.HouseNumber = this.PartOfAddressInElement(GD92Message.MaxHouseNumberLength);
            copy.Street = this.PartOfAddressInElement(GD92Message.MaxStreetLength);
            copy.SubDistrict = this.PartOfAddressInElement(GD92Message.MaxSubDistrictLength);
            copy.District = this.PartOfAddressInElement(GD92Message.MaxDistrictLength);
            copy.Town = this.PartOfAddressInElement(GD92Message.MaxTownLength);
            copy.County = this.PartOfAddressInElement(GD92Message.MaxCountyLength);
        }

        /// <summary>
        /// If there is any of the address remaining, pack as much as possible into the element.
        /// Increment the offset accordingly.
        /// </summary>
        /// <param name="elementLength">Maximum length allowed by GD-92.</param>
        /// <returns>Characters from the address up to the maximum allowed for the element.</returns>
        private string PartOfAddressInElement(int elementLength)
        {
            string partOfAddressInElement = string.Empty;
            int lengthRemaining = this.address.Length - this.addressOffset;
            if (lengthRemaining > 0)
            {
                int lengthRequired = lengthRemaining > elementLength ? elementLength : lengthRemaining;
                partOfAddressInElement = this.address.Substring(this.addressOffset, lengthRequired);
                this.addressOffset += lengthRequired;
            }

            return partOfAddressInElement;
        }

        /// <summary>
        /// Use a stringbuilder member to concatenate address elements.
        /// Storm sends address in separate elements provided in GD92.
        /// MOBS just uses the root address field and sets the others null.
        /// </summary>
        private void AddressFromElements()
        {
            this.addressBuilder = new StringBuilder();
            this.AppendElement(this.mobiliseMessage.Address);
            this.AppendElement(this.mobiliseMessage.HouseNumber);
            this.AppendElement(this.mobiliseMessage.Street);
            this.AppendElement(this.mobiliseMessage.SubDistrict);
            this.AppendElement(this.mobiliseMessage.District);
            this.AppendElement(this.mobiliseMessage.Town);
            this.AppendElement(this.mobiliseMessage.County);
            this.address = this.addressBuilder.ToString();
        }
        
        /// <summary>
        /// Add an element to the address.
        /// </summary>
        /// <param name="element">Address element.</param>
        private void AppendElement(string element)
        {
            string trimmedElement = element.Trim();
            if (trimmedElement.Length > 0)
            {
                this.BeginOrAddToAddress(trimmedElement);
            }
        }
        
        /// <summary>
        /// Use a separator for second and subsequent parts of the address.
        /// </summary>
        /// <param name="trimmedElement">Element to add to the address.</param>
        private void BeginOrAddToAddress(string trimmedElement)
        {
            if (this.addressBuilder.Length > 0)
            {
                this.addressBuilder.AppendFormat(", {0}", trimmedElement);
            }
            else
            {
                this.addressBuilder.Append(trimmedElement);
            }
        }
        
        /// <summary>
        /// Check that one or other attendance is populated but not both (that is the Storm way
        /// even if the callsigns are mobilised from the same station).
        /// Check that all callsigns in Msg2 callsign_list are in the attendance.
        /// The callsign_list is not used otherwise so it is not necessary to check that
        /// it contains all callsigns in the attendance.
        /// </summary>
        private void CheckCallsignsInAttendance()
        {
            if (this.prompts.FirstAttendance.Resources.Count > 0 && this.prompts.SecondAttendance.Resources.Count > 0)
            {
                EventControl.Warn(this.LogName, "Both First and Second Attendance populated - using first");
            }
            
            if (this.prompts.SecondAttendance.Resources.Count > 0 && string.IsNullOrWhiteSpace(this.prompts.FirstAttendance.Address))
            {
                EventControl.Warn(this.LogName, "First Attendance address is missing");
            }

            Collection<GD92Msg6Resource> resources = this.prompts.FirstAttendance.Resources.Count > 0 ?
                this.prompts.FirstAttendance.Resources : this.prompts.SecondAttendance.Resources;
            if (resources.Count == 0)
            {
                // Warning is not justified - this happens in a message sent to home station when a callsign is mobilised
                // from a different station. 
                // Note that the First Attendance address is not copied to Msg6 (as there is no attendance).
                EventControl.Info(this.LogName, "Neither First nor Second Attendance populated");
            }
            else
            {
                foreach (string callsign in this.mobiliseMessage.CallsignList)
                {
                    bool found = false;
                    foreach (GD92Msg6Resource resource in resources)
                    {
                        found |= resource.Callsign == callsign;
                    }
                    
                    if (!found)
                    {
                        EventControl.Warn(this.LogName, "Attendance omits callsign " + callsign);
                    }
                }
            }
        }
        
        /// <summary>
        /// Log the new message as XML if configured to do so.
        /// </summary>
        /// <param name="message">Message 6 constructed from Message 2.</param>
        private void LogNewMessage(GD92Msg6 message)
        {
            if (EventControl.LogEventLevel >= LogLevel.Filter &&
                this.Filter.XmlLogRequired(message))
            {
                EventControl.Filter(this.LogName, message.ToXml());
            }
        }
    }
}

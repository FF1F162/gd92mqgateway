﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.GD92;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Provides methods to log and process received GD92 messages.
    /// </summary>
    internal interface IMessageHandler
    {
        void LogReceivedMessage(IFilter filter);
        
        void ProcessReceivedMessage(IGD92MessageSender messageSender, IMapBook mapBook);
    }
}

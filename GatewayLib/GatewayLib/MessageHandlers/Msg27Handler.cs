﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Handle Text messages (from Storm) - forward to destination.
    /// </summary>
    internal class Msg27Handler : MessageHandler
    {
        private readonly GD92Msg27 textMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg27Handler"/> class.
        /// Cast the GD92Message object to type to expose message content.
        /// </summary>
        /// <param name="message">Received message.</param>
        public Msg27Handler(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Msg27";
            this.textMessage = message as GD92Msg27;
            System.Diagnostics.Debug.Assert(this.textMessage != null, "to show the cast has succeeded");
        }

        /// <summary>
        /// Assume all line breaks are similar: CRLF, CR or LF.
        /// If no CRLF detected change CR or LF to CRLF
        /// Storm currently uses CR only, like an old Mac, but it will probably change to use CRLF.
        /// </summary>
        /// <param name="text">Text for conversion.</param>
        /// <returns>Text with regular line endings.</returns>
        internal static string ConvertToRegularLineEndings(string text)
        {
            const string RegularNewline = "\r\n";
            string convertedText = text;
            if (!convertedText.Contains(RegularNewline))
            {
                if (convertedText.Contains("\r"))
                {
                    convertedText = convertedText.Replace("\r", RegularNewline);
                }
                else
                {
                    convertedText = convertedText.Replace("\n", RegularNewline);
                }
            }

            return convertedText;
        }

        /// <summary>
        /// Log message content as XML.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Filter(this.LogName, this.textMessage.ToXml());
        }

        /// <summary>
        /// Send a copy of the message to the destination.
        /// </summary>
        protected override void ProcessMessage()
        {
            GD92Msg27 newMessage = this.textMessage.Copy();
            newMessage.Text = ConvertToRegularLineEndings(this.textMessage.Text);
            this.MessageSender.SendMessage(newMessage);
        }
    }
}

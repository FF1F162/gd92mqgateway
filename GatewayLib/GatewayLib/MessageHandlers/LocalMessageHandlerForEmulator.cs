﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Threading;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;

    /// <summary>
    /// General handler for messages addressed to the local node, when this is an emulator.
    /// </summary>
    internal class LocalMessageHandlerForEmulator : MessageHandler, IDisposable
    {
        private Timer replyTimer;
        private int replyCode;

        /// <summary>
        /// Initialises a new instance of the <see cref="LocalMessageHandlerForEmulator"/> class.
        /// </summary>
        /// <param name="message">GD92 message of any kind.</param>
        public LocalMessageHandlerForEmulator(GD92Message message)
            : base(message)
        {
            this.LogName = "GW_Emulator_Local";
            this.AckRequired = message.AckRequired;
        }

        /// <summary>
        /// Call Dispose for the timer and set reference null.
        /// </summary>
        public void Dispose()
        {
            if (this.replyTimer != null)
            {
                this.replyTimer.Dispose();
                this.replyTimer = null;
            }
        }

        /// <summary>
        /// Flag to show if acknowledgement is required. By default this is taken from the setting in the message.
        /// </summary>
        protected bool AckRequired { get; set; }

        /// <summary>
        /// Log message content in a warning.
        /// This does not actually log XML - cast to message type would be needed.
        /// </summary>
        protected override void DoXmlLogging()
        {
            EventControl.Warn(this.LogName, this.ReportSerialNumberTypeAndAddresses());
        }

        /// <summary>
        /// If acknowledgement is required use the current settings posted in the Gateway properties.
        /// </summary>
        protected override void ProcessMessage()
        {
            if (this.AckRequired)
            {
                this.replyCode = Gateway.Instance.EmulatorAcknowledgementCode;
                int delay = Gateway.Instance.EmulatorAcknowledgementDelayInSeconds;
                if (delay > 0)
                {
                    this.SetReplyTimer(delay);
                }
                else
                {
                    this.ReplyToReceivedMessage(null);
                }
            }
            else
            {
                EventControl.Info(this.LogName, "No acknowledgement requied");
            }
        }

        /// <summary>
        /// Send Msg50 in acknowledgement or override to send something else (e.g. Msg20 for Msg5).
        /// </summary>
        protected virtual void AcknowledgeReceivedMessage()
        {
            this.AcknowledgeWithAck();
        }

        /// <summary>
        /// Set the reply timer.
        /// </summary>
        /// <param name="secs">Timeout in seconds.</param>
        private void SetReplyTimer(int secs)
        {
            System.Diagnostics.Debug.Assert(this.replyTimer == null, "Reply timer must not be set");
            this.replyTimer = new Timer(this.ReplyToReceivedMessage, null, (long)TimeSpan.FromSeconds(secs).TotalMilliseconds, 0);
        }

        /// <summary>
        /// Send the reply.
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ReplyToReceivedMessage(object state)
        {
            try
            {
                if (this.replyCode < 0)
                {
                    this.AcknowledgeReceivedMessage();
                }
                else
                {
                    this.AcknowledgeWithNak(GD92ReasonCodeSet.General, (GD92GeneralReasonCode)this.replyCode);
                }
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "ProcessReply - {0}",
                    e.ToString()));
                throw;
            }
        }
    }
}

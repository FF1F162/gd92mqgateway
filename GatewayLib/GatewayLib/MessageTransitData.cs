﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Holds data extracted from a GD92Message to be used for logging purposes.
    /// </summary>
    public class MessageTransitData
    {
        private readonly double elapsed;
        private readonly GD92Address source;
        private readonly GD92Address destination;
        private readonly int sequence;
        private readonly int serial;
        private readonly int messageType;
        private readonly char manualAck;

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageTransitData"/> class.
        /// </summary>
        /// <param name="message">Message being logged.</param>
        /// <param name="secondsElapsed">Time taken.</param>
        public MessageTransitData(GD92Message message, double secondsElapsed)
        {
            this.elapsed = secondsElapsed;
            this.source = message.FromAddress;
            this.destination = message.DestAddress;
            this.sequence = message.GD92Sequence;
            this.serial = message.SerialNumber;
            this.messageType = (int)message.MsgType;
            this.manualAck = message.ManualAck ? 'y' : 'n';
        }

        public override string ToString()
        {
            return string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0},{1},{2},{3},{4},{5},{6}",
                this.elapsed.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.source.ToString(),
                this.destination.ToString(),
                this.sequence.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.serial.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.messageType.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.manualAck);
        }
    }
}

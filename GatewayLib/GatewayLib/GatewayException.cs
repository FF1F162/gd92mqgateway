﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception raised in GatewayLib code.
    /// </summary>
    [Serializable]
    public class GatewayException : Exception, ISerializable
    {
        public GatewayException()
        {
        }

        public GatewayException(string message) : base(message)
        {
        }

        public GatewayException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected GatewayException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

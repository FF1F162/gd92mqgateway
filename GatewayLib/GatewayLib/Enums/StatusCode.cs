﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;

    /// <summary>
    /// StatusCode enumeration to convert int representation in Msg20 to text
    /// and vice versa (sort of). 
    /// </summary>
    public enum StatusCode
    {
        Unknown = 0,
        MI = 1,
        IA = 2,
        AI = 3,
        BS = 4,
        OF = 5,
        HM = 7,
        GT = 8,
        PG = 9,
        MS = 10,
        AM = 11,
        DF = 15,
        AS = 19,
        SB = 25,
        DR = 28,
        MR = 29,
        OT = 33,
        MA = 35,
        ST = 50,
        RS = 51,
        NU = 52,
        UN = 53,
        UC = 56,
        RF = 57,
        MF = 58,
        SF = 59,
        AO = 61,
        CN = 62,
        IC = 64,
        TS = 65,
        TG = 66,
        CW = 67,
        PE = 68
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;

    /// <summary>
    /// Direction of travel of messages through the gateway.
    /// Other directions (to particular ranges of nodes) could be added if needed.
    /// </summary>
    public enum DirectionOfTransmission
    {
        None,
        ToNetwork,
        FromNetwork,
        Local,
        Any
    }
}

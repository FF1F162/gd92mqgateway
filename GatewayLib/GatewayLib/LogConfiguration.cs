﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.LogLib;

    /// <summary>
    /// Set of properties used with the LogFilter to control logging.
    /// (This applies only where execution of the log statement depends on the log level or filter.)
    /// And somewhere to keep constants that may become configurable.
    /// </summary>
    public class LogConfiguration : ILogConfiguration
    {
        public const string CommentMarker = "//";
        public const string AppSettingKeyHomeStationsPath = "HomeStationsFilePath";
        public const string DefaultHomeStationsPath = "HomeStations.csv";

        public const string AppSettingKeyMapBookPath = "MapBookFilePath";
        public const string DefaultMapBookPath = @"C:\git\kfrs\StormGateway\MapLib\TestInput";
        public const string AppSettingKeyMainRoadSection = "KentAtoZMainRoadSectionFile";
        public const string DefaultMainRoadSection = "sa_mr_kent2.unl";
        public const string AppSettingKeyStreetSection = "KentAtoZStreetSectionFile";
        public const string DefaultStreetSection = "sa_st_kent3.unl";
        public const string AppSettingKeyLargeScaleASection = "KentAtoZLargeScaleASectionFile";
        public const string DefaultLargeScaleASection = "sa_ls_kenta2.unl";
        public const string AppSettingKeyLargeScaleBSection = "KentAtoZLargeScaleBSectionFile";
        public const string DefaultLargeScaleBSection = "sa_ls_kentb2.unl";

        public const string AppSettingKeyStormEmulatorName = "StormEmulatorName";
        public const string DefaultStormEmulatorName = "STORM";
        public const string AppSettingKeyRscEmulatorName = "RscEmulatorName";
        public const string DefaultRscEmulatorName = "MDG1";

        public const string LoggingLevelKey = "LogLevel";
        public const string DefaultLoggingLevel = "Warning";
        public const string DefaultDirectionForLogging = "None";
        
        public const string Msg20RemarksTimeDateFormat = "HH:mm:ss dd-MM-yy";
        public const string DefaultHomeStationCode = "DF";
        public const string KentCallsignPrefix = "FJK";
        public const int StandardCallsignLength = 7;
        public const int CallsignCoreLength = 4;
        public const int StandardStationNameLength = 5;
        public const int KentStationNameLength = 3;
        public const int StationCodeLength = 2;
        public const int ErrorStatusOk = 0;

        private readonly Collection<int> logTypes;
        private readonly Collection<int> logNodes;
        private readonly Collection<int> xmlTypesToNetwork;
        private readonly Collection<int> xmlTypesFromNetwork;
        private readonly Collection<int> xmlNodes;

        private readonly string logName;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="LogConfiguration"/> class.
        /// </summary>
        public LogConfiguration()
        {
            this.logName = "GW_LogConfiguration";
            this.logTypes = new Collection<int>();
            this.logNodes = new Collection<int>();
            this.xmlTypesToNetwork = new Collection<int>();
            this.xmlTypesFromNetwork = new Collection<int>();
            this.xmlNodes = new Collection<int>();
        }

        /// <summary>
        /// Level of logging detail required.
        /// </summary>
        /// <value>Log level.</value>
        public LogLevel LoggingLevel { get; set; }
        
        /// <summary>
        /// Name of local node, to detect messages for which this is an end point.
        /// This must be obtained from the GD92Configuration (currently in Registry not app.config).
        /// </summary>
        /// <value>Name of local node.</value>
        public string LocalNodeName { get; set; }

        /// <summary>
        /// Restrict logging to messages travelling in the direction specified.
        /// Default direction All shows this is not to be used as a filter .
        /// </summary>
        /// <value>Direction or All.</value>
        public DirectionOfTransmission LogDirection { get; set; }
        
        /// <summary>
        /// Types that require logging.
        /// An empty list shows this is not to be used as a filter (all types logged).
        /// A list containing just 0 or some other non-existent type stops all logging.
        /// </summary>
        /// <value>Empty (for all) or a list of specific types.</value>
        public Collection<int> LogTypes 
        { 
            get 
            {
                return this.logTypes; 
            } 
        }
        
        /// <summary>
        /// If the source or destination node is in this list the message must be logged.
        /// An empty list shows this is not to be used as a filter.
        /// </summary>
        /// <value>Empty (for all) or a list of specific nodes.</value>
        public Collection<int> LogNodes 
        { 
            get 
            {
                return this.logNodes; 
            } 
        }
        
        /// <summary>
        /// Restrict XML logging to messages travelling in the direction specified.
        /// Default direction All shows this is not to be used as a filter.
        /// </summary>
        /// <value>Direction or All.</value>
        public DirectionOfTransmission XmlDirection { get; set; }
        
        /// <summary>
        /// Types of messages from Storm that require serializing to XML in the log.
        /// An empty list shows this is not to be used as a filter (all types logged).
        /// A list containing just 0 or some other non-existent type stops XML logging from Storm.
        /// </summary>
        /// <value>Empty (for all) or a list of specific types.</value>
        public Collection<int> XmlTypesToNetwork
        { 
            get
            {
                return this.xmlTypesToNetwork; 
            } 
        }
        
        /// <summary>
        /// If the message type is in this list the message must be serialized.
        /// as XML in the log. This is applied to messages from the network (i.e. from SEE or MDT).
        /// An empty list shows this is not to be used as a filter (all types logged).
        /// A list containing just 0 or some other non-existent type stops XML logging from network.
        /// </summary>
        /// <value>Empty (for all) or a list of types.</value>
        public Collection<int> XmlTypesFromNetwork 
        { 
            get 
            {
                return this.xmlTypesFromNetwork; 
            } 
        }

        /// <summary>
        /// If the source or destination node is in this list the message must be serialized.
        /// as XML in the log.
        /// An empty list shows this is not to be used as a filter.
        /// </summary>
        /// <value>Empty (for all) or a list of specific nodes.</value>
        public Collection<int> XmlNodes 
        { 
            get 
            {
                return this.xmlNodes; 
            } 
        }

        /// <summary>
        /// Read configuration from app.config and set level in LogControl.
        /// Note that LocalNodeName is configured in the Registry GD92 configuration.
        /// If the logging is misconfigured set the direction filter to None to stop all configurable logging.
        /// </summary>
        public void RefreshFromAppConfig()
        {
            try
            {
                ConfigurationManager.RefreshSection("appSettings");
                this.UpdateLoggingLevel();
                this.LogDirection = DirectionFromConfigOrDefault("LogDirection");
                this.XmlDirection = DirectionFromConfigOrDefault("XmlDirection");
                FillCollectionOfNumbersFromConfig(this.LogNodes, "LogNodes");
                FillCollectionOfNumbersFromConfig(this.LogTypes, "LogTypes");
                FillCollectionOfNumbersFromConfig(this.XmlNodes, "XmlNodes");
                FillCollectionOfNumbersFromConfig(this.XmlTypesToNetwork, "XmlTypesToNetwork");
                FillCollectionOfNumbersFromConfig(this.XmlTypesFromNetwork, "XmlTypesFromNetwork");
            }
            catch (ArgumentException ex)
            {
                this.LogDirection = DirectionOfTransmission.None;
                EventControl.Warn(this.logName, ex.Message);
            }
        }

        /// <summary>
        /// Override this if logging level should be controlled from the Log Window.
        /// rather than from the configuration file.
        /// </summary>
        protected virtual void UpdateLoggingLevel()
        {
            this.LoggingLevel = LogLevelFromConfigOrDefault();
            LogControl.Instance.SetLoggingLevel(this.LoggingLevel);
        }

        private static LogLevel LogLevelFromConfigOrDefault()
        {
            string appSetting = ConfigurationManager.AppSettings[LoggingLevelKey] ?? DefaultLoggingLevel;
            return (LogLevel)Enum.Parse(typeof(LogLevel), appSetting);
        }

        private static DirectionOfTransmission DirectionFromConfigOrDefault(string key)
        {
            string appSetting = ConfigurationManager.AppSettings[key] ?? DefaultDirectionForLogging;
            return (DirectionOfTransmission)Enum.Parse(typeof(DirectionOfTransmission), appSetting);
        }

        /// <summary>
        /// Empty the collection.
        /// Parse comma-separated list of numbers from the keyed item in app.config and add to the collection.
        /// </summary>
        /// <param name="collection">Collection to hold values read.</param>
        /// <param name="key">Key to item in app.config that holds the list.</param>
        private static void FillCollectionOfNumbersFromConfig(Collection<int> collection, string key)
        {
            collection.Clear();
            string appSetting = ConfigurationManager.AppSettings[key] ?? string.Empty;
            string[] data = appSetting.Split(new char[] { ',' });
            foreach (string item in data)
            {
                int result;
                if (int.TryParse(item, out result))
                {
                    collection.Add(result);
                }
            }
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;
    using Thales.KentFire.GD92;

    /// <summary>
    /// This can be applied to any log statement.
    /// But Xml log in particular requires processing and occupies space in the log file.
    /// Unless it is needed it should be switched off. This filter provides the means.
    /// </summary>
    public class LogFilter : IFilter
    {
        private readonly ILogConfiguration configuration;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="LogFilter"/> class.
        /// </summary>
        /// <param name="configuration">Logging configuration.</param>
        public LogFilter(ILogConfiguration configuration)
        {
            this.configuration = configuration;
            System.Diagnostics.Debug.Assert(configuration != null, "Logging configuration information must not be null.");
            System.Diagnostics.Debug.Assert(configuration.LocalNodeName != null, "Local node name must not be null.");
        }
        
        /// <summary>
        /// Decide if the message is travalling to the local node, to the network or to Storm.
        /// </summary>
        /// <param name="message">GD92 message.</param>
        /// <returns>Direction.</returns>
        public DirectionOfTransmission DirectionOfMessage(GD92Message message)
        {
            DirectionOfTransmission direction = DirectionOfTransmission.ToNetwork;
            if (message.Destination != null && 
                message.Destination.Equals(this.configuration.LocalNodeName))
            {
                direction = DirectionOfTransmission.Local;
            }
            else if (message.DestNodeType == NodeType.Storm)
            {
                direction = DirectionOfTransmission.FromNetwork;
            }
            
            return direction;
        }
        
        /// <summary>
        /// Compare the characteristics of the message with the configuration
        /// to see if logging is required.
        /// </summary>
        /// <param name="message">GD92 message.</param>
        /// <returns>True if log is required.</returns>
        public bool LogIsRequired(GD92Message message)
        {
            bool logIsRequired = true;
            logIsRequired &= this.FilterByDirection(message, this.configuration.LogDirection);
            logIsRequired &= FilterByType(message, this.configuration.LogTypes);
            logIsRequired &= FilterByNode(message, this.configuration.LogNodes);
            return logIsRequired;
        }
        
        /// <summary>
        /// Compare the characteristics of the message with the configuration
        /// to see if XML logging is required. Note there is no filter by type for local node.
        /// Currently, the local node handler does not do XML logging.
        /// </summary>
        /// <param name="message">GD92 message.</param>
        /// <returns>True if log is required.</returns>
        public bool XmlLogRequired(GD92Message message)
        {
            bool logIsRequired = true;
            logIsRequired &= this.FilterByDirection(message, this.configuration.XmlDirection);
            logIsRequired &= FilterByNode(message, this.configuration.XmlNodes);
            logIsRequired &= (this.DirectionOfMessage(message) == DirectionOfTransmission.ToNetwork) ?
                FilterByType(message, this.configuration.XmlTypesToNetwork) :
                FilterByType(message, this.configuration.XmlTypesFromNetwork);            
            return logIsRequired;
        }
        
        /// <summary>
        /// Only log messages of types listed in the collection.
        /// Filter is turned off (message logged regardless of node) if collection is empty.
        /// </summary>
        /// <param name="message">GD92 message.</param>
        /// <param name="configuredTypes">Types of message to be logged.</param>
        /// <returns>True if message should be logged.</returns>
        private static bool FilterByType(GD92Message message, Collection<int> configuredTypes)
        {
            return configuredTypes.Count == 0 || 
                   configuredTypes.Contains((int)message.MsgType);
        }

        /// <summary>
        /// Only log message if is from or to a node listed in the collection.
        /// Filter is turned off (message logged regardless of node) if collection is empty.
        /// </summary>
        /// <param name="message">GD92 message.</param>
        /// <param name="configuredNodes">Nodes for logging.</param>
        /// <returns>True if message should be logged.</returns>
        private static bool FilterByNode(GD92Message message, Collection<int> configuredNodes)
        {
            return configuredNodes.Count == 0 || 
                   configuredNodes.Contains(message.FromAddress.Node) ||
                   configuredNodes.Contains(message.DestAddress.Node);
        }
        
        /// <summary>
        /// Filter can be turned off (Any) or set to one only of the directions in the enumeration.
        /// </summary>
        /// <param name="message">GD92 message.</param>
        /// <param name="direction">Direction of travel.</param>
        /// <returns>Direction.</returns>
        private bool FilterByDirection(GD92Message message, DirectionOfTransmission direction)
        {
            return direction == DirectionOfTransmission.Any ||
                   direction == this.DirectionOfMessage(message);
        }
    }
}

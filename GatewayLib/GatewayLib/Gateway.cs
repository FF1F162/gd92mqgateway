﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Configuration;
    using System.Reflection;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.LogLib;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Gateway acts as the host application (HAP) for GD92Component.
    /// </summary>
    public class Gateway
    {
        private static readonly Gateway Self = new Gateway();

        private readonly string logName;
        private readonly string assemblyInformation;
        private readonly GD92Component gd92Component;

        private readonly object gatewayLock;

        private GD92Handler gd92Handler;
        
        private IMapBook mapBook;

        /// <summary>
        /// Prevents a default instance of the <see cref="Gateway"/> class from being created.
        /// </summary>
        private Gateway()
        {
            this.logName = "GW_Gateway";
            this.gatewayLock = new object();
            this.gd92Component = GD92Component.Instance;
            Assembly assembly = Assembly.GetExecutingAssembly();
            this.assemblyInformation = assembly.GetName().ToString();
        }

        #region Properties

        /// <summary>
        /// Access to singleton for non-static method calls and properties.
        /// </summary>
        /// <value>Single instance of class.</value>
        public static Gateway Instance
        {
            get
            {
                return Self;
            }
        }

        /// <summary>
        /// Name and version for the log file header.
        /// </summary>
        /// <value>Name and version.</value>
        public string AssemblyInformation
        {
            get { return this.assemblyInformation; }
        }
        
        /// <summary>
        /// Shows whether or not the GD92 component is activated.
        /// </summary>
        /// <value>True if the GD92 component is activated.</value>
        public bool Activated
        {
            get { return this.gd92Component.Activated; }
        }

        /// <summary>
        /// Shows whether or not there is a connection.
        /// </summary>
        /// <value>True if there is connection to one or more nodes.</value>
        public bool ConnectedToGD92
        {
            get { return this.gd92Handler != null && this.gd92Handler.Connected; }
        }

        /// <summary>
        /// Shows if this node is configured to emulate an IPSE or MDT in the way that it sends test messages
        /// and in the way that it responds to messages addressed directly to this node.
        /// </summary>
        /// <value>True if this emulates an RSC-based node.</value>
        public bool IsRscEmulator
        {
            get { return this.gd92Handler != null && this.gd92Handler.IsRscEmulator; }
        }

        /// <summary>
        /// Shows if this node is configured to emulate Storm in the way that it sends test messages
        /// and in the way that it responds to messages addressed directly to this node.
        /// </summary>
        /// <value>True if this emulates Storm.</value>
        public bool IsStormEmulator
        {
            get { return this.gd92Handler != null && this.gd92Handler.IsStormEmulator; }
        }

        /// <summary>
        /// Delay before sending acknowledgement for received message that requires acknowledgement.
        /// </summary>
        /// <value>True if this emulates Storm.</value>
        public int EmulatorAcknowledgementDelayInSeconds { get; set; }

        /// <summary>
        /// Indicates type of acknowledgement: -1 = Ack, 1-17 = valid reason codes for Nak, 18 = timeout.
        /// </summary>
        /// <value>Negative (Ack) or Nak General reason code.</value>
        public int EmulatorAcknowledgementCode { get; set; }

        /// <summary>
        /// Configuration source must be set to use a source that is appropriate
        /// (e.g. update Test logging level from the logging window not from the file).
        /// </summary>
        /// <value>Supplier of configuration.</value>
        public ILogConfiguration Configuration { get; set; }

        /// <summary>
        /// Map book data loaded from file at activation.
        /// </summary>
        /// <value>Data for translation of OS grid reference to map book page references.</value>
        public IMapBook MapBook
        {
            get { return this.mapBook; }
        }

        /// <summary>
        /// Logger for reporting to external monitoring e.g. Nagios via Windows Event Log.
        /// </summary>
        /// <value>Instance of logger.</value>
        public IExternalEventLogger EventLogger
        {
            get
            {
                return this.gd92Component.EventLogger;
            }

            set
            {
                this.gd92Component.EventLogger = value;
            }
        }

        #endregion

        /// <summary>
        /// Activate event handling, logging and GD92 communications.
        /// If loading configuration or anything else takes too long for the service OnStart timeout
        /// it will be necessary to do all this in a new thread.
        /// Note: GD92 startup is in a separate thread.
        /// </summary>
        /// <returns>Result returned by Activate method of GD92Component.</returns>
        public ControlResult Activate()
        {
            lock (this.gatewayLock)
            {
                System.Diagnostics.Debug.Assert(this.EventLogger != null, "Event logger must be set before activation.");
                System.Diagnostics.Debug.Assert(this.Configuration != null, "Configuration source must be set before activation.");
                this.Configuration.RefreshFromAppConfig();
                LogControl.Instance.LogToFile = true;
                LogControl.Instance.LoggingName = "StormGateway";
                LogControl.Instance.SetLoggingLevel(this.Configuration.LoggingLevel);

                this.mapBook = new KentAtoZ();

                // Begin processing the event queue.
                // Pass a reference to the GD92Component, which fires the message and node events
                // (EventHandling does not know how to do this itself).
                EventControl.Instance.Activate(this.gd92Component);

                EventControl.Alert(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Activating {0}", 
                    this.assemblyInformation));
            }

            return this.ConnectGD92();
        }

        /// <summary>
        /// Deactivate GD92 communications.
        /// </summary>
        /// <returns>Result returned by Deactivate method of GD92Component.</returns>
        public ControlResult Deactivate()
        {
            ControlResult r;

            // lock as a precaution against interruption from another call to Activate or Deactivate.
            lock (this.gatewayLock)
            {
                EventControl.Alert(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "De-Activating {0}",
                    this.assemblyInformation));

                this.gd92Handler = null;

                r = this.gd92Component.Deactivate(); // takes care of deactivating LogControl
                if (r != ControlResult.Success)
                {
                    EventControl.Emergency(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "GD92Component reported - {0}",
                        r.ToString()));
                }
            }

            return r;
        }

        /// <summary>
        /// Activate the GD92 component.
        /// GD92 activation runs in a separate thread and only sets Activated at the end.
        /// Assume that this is going to happen and set the reference to the singleton that handles GD92 events.
        /// </summary>
        /// <returns>Result returned by the Activate method of GD92 component.</returns>
        private ControlResult ConnectGD92()
        {
            System.Diagnostics.Debug.Assert(this.gd92Handler == null, "Call to ConnectGD92 is expected only if there is not yet any GD92Handler");
            ControlResult r = this.gd92Component.Activate();
            if (r == ControlResult.Success)
            {
                this.gd92Handler = GD92Handler.Instance;
            }
            else
            {
                // Activation was not possible - maybe because another instance of GD92Component
                // is already activated.
                EventControl.Emergency(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "GD92Component reported - {0}", 
                    r.ToString()));
            }

            return r;
        }
    }
}

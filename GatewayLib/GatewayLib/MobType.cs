﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thales.KentFire.GatewayLib
{
    /// <summary>
    /// Mobilisation type enumeration to convert representation in Msg6
    /// to text at the beginning of the text sent to TETRA radio
    /// There is a different list in GD92Messages (EGD92MsgType.cs)
    /// </summary>
    public enum MobType
    {
        PRE_ALERT = 0, // not used KFRS
        PROCEED = 1,
        PROCEED_STANDBY = 4,
        PROCEED_MAKE_UP = 7,
        PROCEED_HOME_STATION = 8,
        INFORM = 9,
        ASSIGN = 10,
        FURTHER_INFO = 96,
        NEW_TYPE = 97,
        NEW_LOCATION = 98,
        UPDATE = 99
    }
}

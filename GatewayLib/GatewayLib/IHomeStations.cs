﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Methods to update and to retrieve home station data.
    /// </summary>
    public interface IHomeStations
    {
        void UpdateHomeStationData();

        string ReportCurrentData();

        string HomeStationCodeForCallsign(string callsign);

        string HomeStationNodeNameForCallsign(string callsign);

        Collection<string> CallsignsWithHomeStation(string station);
    }
}

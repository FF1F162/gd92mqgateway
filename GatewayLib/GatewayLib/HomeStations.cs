﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Dictionary of callsigns showing home station for each.
    /// </summary>
    public class HomeStations : IHomeStations
    {
        private readonly string logName;
        
        private readonly object homeStationsLock;
        private Dictionary<string, HomeStation> homeStationsByCallsign;
        private Dictionary<string, HomeStation> pendingHomeStationsByCallsign;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="HomeStations"/> class.
        /// </summary>
        public HomeStations()
        {
            this.logName = "GW_HomeStations";
            this.homeStationsLock = new object();
            this.DataFilePath = string.Empty;
            this.homeStationsByCallsign = new Dictionary<string, HomeStation>();
        } 
        
        /// <summary>
        /// If this is left empty, UpdateHomeStationData reads it from configuration.
        /// </summary>
        /// <value>Path of the data file.</value>
        public string DataFilePath { get; set; }
        
        /// <summary>
        /// Call this to read the data after changes
        /// Populate the Pending (temporary) data dictionary. If load is successful 
        /// substitute pending for live data. If not, keep existing data and log a warning.
        /// </summary>
        public void UpdateHomeStationData()
        {
            this.ResolveDataFilePath();
            try 
            {
                this.LoadData();
            } 
            catch (GatewayException ex) 
            {
                EventControl.Warn(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Failed to update home station data: {0}",
                    ex.Message));
            }
        }

        /// <summary>
        /// Report current home stations data (for logging purposes).
        /// </summary>
        /// <returns>One line per callsign showing callsign, station code, station node name.</returns>
        public string ReportCurrentData()
        {
            var report = new StringBuilder("Home station data for callsigns");
            foreach (KeyValuePair<string, HomeStation> kvp in this.homeStationsByCallsign)
            {
                report.AppendLine();
                report.AppendFormat(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0},{1},{2}",
                    kvp.Key, 
                    kvp.Value.Code, 
                    kvp.Value.NodeName);
            }

            return report.ToString();
        }

        /// <summary>
        /// Set the lock and look up the station in the dictionary of stations by callsign.
        /// Return default code if the callsign is not in the list.
        /// </summary>
        /// <param name="callsign">Callsign name.</param>
        /// <returns>Station code.</returns>
        public string HomeStationCodeForCallsign(string callsign)
        {
            HomeStation station = this.HomeStationForCallsign(callsign);
            return station.Code;
        }

        /// <summary>
        /// Set the lock and look up the station in the dictionary of stations by callsign.
        /// Return HomeStation.UnknownNodeName if the callsign is not in the list.
        /// </summary>
        /// <param name="callsign">Callsign name.</param>
        /// <returns>Station node name.</returns>
        public string HomeStationNodeNameForCallsign(string callsign)
        {
            HomeStation station = this.HomeStationForCallsign(callsign);
            return station.NodeName;
        }

        /// <summary>
        /// Check all known callsigns and return a collection of all with the same home station.
        /// The comparison uses the station code (last two characters).
        /// </summary>
        /// <param name="station">Station name or code.</param>
        /// <returns>Callsigns with same home station.</returns>
        public Collection<string> CallsignsWithHomeStation(string station)
        {
            string stationCode = HomeStation.ExtractStationCode(station);
            var callsigns = new Collection<string>();
            lock (this.homeStationsLock)
            {
                foreach (KeyValuePair<string, HomeStation> kvp in this.homeStationsByCallsign)
                {
                    if (stationCode == kvp.Value.Code)
                    {
                        callsigns.Add(kvp.Key);
                    }
                }
            }

            if (callsigns.Count == 0)
            {
                EventControl.Warn(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Found no callsign for home station {0}",
                    station));
            }

            return callsigns;
        }

        /// <summary>
        /// Ignore blank lines and comment.
        /// </summary>
        /// <param name="line">Line of text or data.</param>
        /// <returns>True if the line is not empty and does NOT contain the comment marker.</returns>
        private static bool LineIsData(string line)
        {
            return (line.Length > 0) && !line.Contains(LogConfiguration.CommentMarker);
        }

        /// <summary>
        /// If not yet set, read the HomeStations path from app.config. If null, set it to the default.
        /// </summary>
        private void ResolveDataFilePath()
        {
            if (this.DataFilePath.Length == 0)
            {
                this.ReadDataFilePathFromAppConfigOrDefault();
            }
        }

        /// <summary>
        /// Read the HomeStations path from app.config. If null, set it to the default.
        /// </summary>
        private void ReadDataFilePathFromAppConfigOrDefault()
        {
            string appKey = LogConfiguration.AppSettingKeyHomeStationsPath;
            string defaultPath = LogConfiguration.DefaultHomeStationsPath;
            this.DataFilePath = ConfigurationManager.AppSettings[appKey] ?? defaultPath;
        }

        /// <summary>
        /// Read Homestations data into a new dictionary.
        /// If successful, set the lock and swap it for the old one.
        /// </summary>
        private void LoadData()
        {
            this.pendingHomeStationsByCallsign = new Dictionary<string, HomeStation>();
            this.ReadDataFromFile();
            lock (this.homeStationsLock)
            {
                this.homeStationsByCallsign = this.pendingHomeStationsByCallsign;
            }
        }

        /// <summary>
        /// Open the file and read all lines that contain data into the 'pending' dictionary.
        /// </summary>
        private void ReadDataFromFile()
        {
            string currentLine;
            using (var fileDataStream = new StreamReader(this.DataFilePath))
            {
                while ((currentLine = fileDataStream.ReadLine()) != null)
                {
                    if (LineIsData(currentLine))
                    {
                        this.ReadThreeColumns(currentLine);
                    }
                }
            }
        }

        /// <summary>
        /// Parse a line of data and if all is well add a new HomeStation object to the 'pending' dictionary.
        /// Exception may be thrown here or in the HomeStation constructor if parsing fails.
        /// </summary>
        /// <param name="currentLine">Line of data that represents a HomeStation.</param>
        private void ReadThreeColumns(string currentLine)
        {
            string[] parts = currentLine.Split(',');
            if (parts.Length != 3)
            {
                throw new GatewayException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Expected format is 'callsign, station, node'. This line is wrong:{0}", 
                    currentLine));
            }

            var trimChars = new char[] { ' ', '"' };
            this.AddRecordToPending(parts[0].Trim(trimChars), parts[1].Trim(trimChars), parts[2].Trim(trimChars));
        }

        /// <summary>
        /// Check for duplicate callsign, construct HomeStation and add to the dictionary.
        /// </summary>
        /// <param name="keyText">Callsign name.</param>
        /// <param name="name">Station name or code.</param>
        /// <param name="node">Station node number or name.</param>
        private void AddRecordToPending(string keyText, string name, string node)
        {
            if (this.pendingHomeStationsByCallsign.ContainsKey(keyText))
            {
                throw new GatewayException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Duplicate key {0} in {1}", 
                    keyText, 
                    this.DataFilePath));
            }
            else
            {
                var station = new HomeStation(name, node);
                this.pendingHomeStationsByCallsign.Add(keyText, station);
            }
        }

        /// <summary>
        /// Look up the home station for a callsign.
        /// </summary>
        /// <param name="callsign">Callsign name.</param>
        /// <returns>Station code for the home station of the callsign.</returns>
        private HomeStation HomeStationForCallsign(string callsign)
        {
            HomeStation valueForKey = null;
            lock (this.homeStationsLock)
            {
                if (this.homeStationsByCallsign != null)
                {
                    this.homeStationsByCallsign.TryGetValue(callsign, out valueForKey);
                }
            }

            if (valueForKey == null)
            {
                EventControl.Warn(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Missing home station data for callsign {0}",
                    callsign));
            }

            return valueForKey ?? new HomeStation();
        }
    }
}

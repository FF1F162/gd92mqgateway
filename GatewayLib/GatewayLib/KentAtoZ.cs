﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GatewayLib
{
    using System;
    using System.Configuration;
    using System.IO;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Wrapper for Kent A to Z map book sections.
    /// </summary>
    public class KentAtoZ : IMapBook
    {
        private readonly string logName; 
        private string dataPath;
        private string mainRoadFile;
        private string streetFile;
        private string largeScaleAFile;
        private string largeScaleBFile;

        private PageSections sections;

        public KentAtoZ()
        {
            this.logName = "KentAtoZ";
            this.ReadFromAppConfigOrDefault();
            this.sections = new PageSections();
            this.LoadSection(Path.Combine(this.dataPath, this.mainRoadFile));
            this.LoadSection(Path.Combine(this.dataPath, this.streetFile));
            this.LoadSection(Path.Combine(this.dataPath, this.largeScaleAFile));
            this.LoadSection(Path.Combine(this.dataPath, this.largeScaleBFile));
        }

        /// <summary>
        /// Return reference(s) to page and cell corresponding to the Ordnance Survey grid reference supplied.
        /// </summary>
        /// <param name="gridReference">Twelve digit OS Grid Reference.</param>
        /// <returns>First matching cell reference (if any) in each section or empty string.</returns>
        public string MapBookForGridReference(string gridReference)
        {
            return this.sections.MapBookForGridReference(gridReference);
        }

        /// <summary>
        /// Read the data path and names of section files from app.config. If null, set it to the default.
        /// </summary>
        private void ReadFromAppConfigOrDefault()
        {
            string appKey = LogConfiguration.AppSettingKeyMapBookPath;
            this.dataPath = ConfigurationManager.AppSettings[appKey] ?? LogConfiguration.DefaultMapBookPath;
            appKey = LogConfiguration.AppSettingKeyMainRoadSection;
            this.mainRoadFile = ConfigurationManager.AppSettings[appKey] ?? LogConfiguration.DefaultMainRoadSection;
            appKey = LogConfiguration.AppSettingKeyStreetSection;
            this.streetFile = ConfigurationManager.AppSettings[appKey] ?? LogConfiguration.DefaultStreetSection;
            appKey = LogConfiguration.AppSettingKeyLargeScaleASection;
            this.largeScaleAFile = ConfigurationManager.AppSettings[appKey] ?? LogConfiguration.DefaultLargeScaleASection;
            appKey = LogConfiguration.AppSettingKeyLargeScaleBSection;
            this.largeScaleBFile = ConfigurationManager.AppSettings[appKey] ?? LogConfiguration.DefaultLargeScaleBSection;
        }

        private void LoadSection(string fullFilePath)
        {
            try
            {
                this.sections.LoadAndAddSection(fullFilePath);
            }
            catch (System.IO.FileNotFoundException)
            {
                EventControl.Warn(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Section data file not found: {0}",
                    fullFilePath));
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                EventControl.Warn(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Section data path not found: {0}",
                    fullFilePath));
            }
            catch (MapLibException e)
            {
                EventControl.Warn(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Error in section data file: {0} - {1}",
                    fullFilePath,
                    e.Message));
            }
        }
    }
}

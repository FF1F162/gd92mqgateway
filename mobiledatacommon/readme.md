# MobileDataCommon #

## Introduction ##
This repo contains source code for common elements of mobile data (shared components, class libraries etc)

The format for readme.md files is based on Python markdown, more examples are available in the [bitbucket markdown tutorial](https://bitbucket.org/tutorials/markdowndemo/overview#markdown-header-links)
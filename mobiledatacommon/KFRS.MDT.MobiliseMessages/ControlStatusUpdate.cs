﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFRS.MDT.MobiliseMessages
{
    public class ControlStatusUpdate : StatusUpdateBase
    {
        private int _riders;

        public int Riders
        {
            get { return _riders; }
            set
            {
                if (_riders != value)
                {
                    _riders = value;
                }
            }
        }
    }
}

﻿using System;

namespace KFRS.MDT.MobiliseMessages
{
    public class Mobilisation
    {
        private string _incidentNumber;
        private string _incidentType;
        private DateTime _incidentDate;
        private string _address;

        public string IncidentNumber
        {
            get { return _incidentNumber; }
            set
            {
                if (_incidentNumber != value)
                {
                    _incidentNumber = value;
                }
            }
        }

        public string IncidentType
        {
            get { return _incidentType; }
            set
            {
                if (_incidentType != value)
                {
                    _incidentType = value;
                }
            }
        }

        public DateTime IncidentDate
        {
            get { return _incidentDate; }
            set
            {
                if (_incidentDate != value)
                {
                    _incidentDate = value;
                }
            }
        }

        public string Address
        {
            get { return _address; }

            set
            {
                if (_address != value)
                {
                    _address = value;
                }
            }
        }
    }
}

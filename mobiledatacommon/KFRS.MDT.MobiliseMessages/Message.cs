﻿using KFRS.MDT.MobiliseMessages.Enums;
using System;

namespace KFRS.MDT.MobiliseMessages
{
    public class Message : IEquatable<Message>
    {
        private readonly Guid _uniqueId;
        private int _sequenceNumber;
        private TimeStamp _timeStamp;
        private MessageType _messageType;
        private Priority _priority;
        private string _destination;
        private string _originator;
        private object _content;

        public Message(Guid Guid)
        {
            _uniqueId = Guid;
        }

        public Guid UniqueId
        {
            get { return _uniqueId; }
        }

        public int SequenceNumber  // Should this be Readonly?
        {
            get { return _sequenceNumber; }

            set
            {
                if (_sequenceNumber != value)
                {
                    _sequenceNumber = value;
                }
            }
        }

        public TimeStamp TimeStamp
        {
            get { return _timeStamp; }

            set
            {
                if (_timeStamp != value)
                {
                    _timeStamp = value;
                }
            }
        }

        public Priority Priority
        {
            get { return _priority; }

            set
            {
                if (_priority != value)
                {
                    _priority = value;
                }
            }
        }

        public string Destination
        {
            get { return _destination; }

            set
            {
                if ( _destination != value)
                {
                    _destination = value;
                }
            }
        }

        public string Originator
        {
            get { return _originator; }

            set
            {
                if (_originator != value)
                {
                    _originator = value;
                }
            }
        }

        public object Content
        {
            get { return _content; }

            set
            {
                if (_content != value)
                {
                    _content = value;
                }
            }
        }

        public MessageType MessageType
        {
            get { return _messageType; }

            set
            {
                if (_messageType != value)
                {
                    _messageType = value;
                }
            }
        }

        public bool Equals(Message other)
        {
            return (UniqueId == other.UniqueId);
        }

        public override int GetHashCode() 
        {
            return UniqueId.GetHashCode();
        }

    }
}

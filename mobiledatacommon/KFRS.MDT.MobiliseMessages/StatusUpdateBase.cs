﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFRS.MDT.MobiliseMessages
{
    public abstract class StatusUpdateBase
    {
        private string _callsign;
        private string _status;
        private string _incidentID;

        public string Callsign
        {
            get { return _callsign; }
            set
            {
                if (_callsign != value)
                {
                    _callsign = value;
                }
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                }
            }
        }

        public string IncidentID
        {
            get { return _incidentID; }
            set
            {
                if (_incidentID != value)
                {
                    _incidentID = value;
                }
            }
        }
    }
}

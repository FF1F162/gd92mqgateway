﻿using Newtonsoft.Json;
using System.Text;

namespace KFRS.MDT.MobiliseMessages.Helpers
{
    public class Serializers
    {
        public static byte[] Serialize(object toSerialize)
        {
            var jsonBody = JsonConvert.SerializeObject(toSerialize);
            byte[] serializedBuffer = Encoding.UTF8.GetBytes(jsonBody);

            return serializedBuffer;
        }


        public static object DeSerialize(byte[] toDeserialize)
        {
            string jsonBody = Encoding.UTF8.GetString(toDeserialize);
            object returnObject = JsonConvert.DeserializeObject(jsonBody); 

            return returnObject;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFRS.MDT.MobiliseMessages.Enums
{
    public enum MessageType
    {
        None,
        Mobilisation,
        ControlStatusUpdate, //Status change from Control to resource 
        ResourceStatusupdate, //Status change from resource to Control
        Ack,
        Nack
    }
}

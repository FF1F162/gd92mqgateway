﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using Thales.KentFire.GatewayLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Thales.KentFire.GD92;
using Thales.KentFire.MapLib;

namespace GatewayLibTests
{
    
    
    /// <summary>
    ///This is a test class for Msg2HandlerTest and is intended
    ///to contain all Msg2HandlerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class Msg2HandlerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        /// RSC allows 140 characters. Actually anything up to 255 is safe but RSC truncates to 140 for display.
        /// </summary>
        [TestMethod()]
        public void ShortAddressTest()
        {
            var sender = new GatewayLib.NUnitTests.MockMessageSender();
            var message = new GD92Msg2();
            string expected = "A23456789 B23456789";
            message.Address = expected;
            var handler = new Msg2Handler(message);
            handler.ProcessReceivedMessage(sender, new KentMapBooks());
            var message6 = sender.MessageSent as GD92Msg6;
            Assert.IsNotNull(message6);
            Assert.AreEqual(expected, message6.Address);
        }

        /// <summary>
        ///A test for Msg2Handler Constructor
        ///</summary>
        [TestMethod()]
        public void Msg2HandlerConstructorTest()
        {
            GD92Message message = null; // TODO: Initialize to an appropriate value
            Msg2Handler target = new Msg2Handler(message);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddressFromElements
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void AddressFromElementsTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            target.AddressFromElements();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AllResourcesListFromPrompts
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void AllResourcesListFromPromptsTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            GD92Msg6 copy = null; // TODO: Initialize to an appropriate value
            target.AllResourcesListFromPrompts(copy);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AppendElement
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void AppendElementTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            string element = string.Empty; // TODO: Initialize to an appropriate value
            target.AppendElement(element);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BeginOrAddToAddress
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void BeginOrAddToAddressTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            string trimmedElement = string.Empty; // TODO: Initialize to an appropriate value
            target.BeginOrAddToAddress(trimmedElement);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CheckCallsignsInAttendance
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void CheckCallsignsInAttendanceTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            target.CheckCallsignsInAttendance();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CopyDataTo
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void CopyDataToTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            GD92Msg6 copy = null; // TODO: Initialize to an appropriate value
            target.CopyDataTo(copy);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DoXmlLogging
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void DoXmlLoggingTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            target.DoXmlLogging();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for FirstAttendanceFromPrompts
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void FirstAttendanceFromPromptsTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            GD92Msg6 copy = null; // TODO: Initialize to an appropriate value
            target.FirstAttendanceFromPrompts(copy);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for LogNewMessage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void LogNewMessageTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            GD92Msg6 message = null; // TODO: Initialize to an appropriate value
            target.LogNewMessage(message);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PartOfAddressInElement
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void PartOfAddressInElementTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            int elementLength = 0; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.PartOfAddressInElement(elementLength);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ProcessMessage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void ProcessMessageTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            target.ProcessMessage();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for RePackageAddressIntoMsg6Elements
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void RePackageAddressIntoMsg6ElementsTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            GD92Msg6 copy = null; // TODO: Initialize to an appropriate value
            target.RePackageAddressIntoMsg6Elements(copy);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ResourcesListFromPrompts
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void ResourcesListFromPromptsTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            GD92Msg6 copy = null; // TODO: Initialize to an appropriate value
            target.ResourcesListFromPrompts(copy);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SecondAttendanceFromPrompts
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GatewayLib.dll")]
        public void SecondAttendanceFromPromptsTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Msg2Handler_Accessor target = new Msg2Handler_Accessor(param0); // TODO: Initialize to an appropriate value
            GD92Msg6 copy = null; // TODO: Initialize to an appropriate value
            target.SecondAttendanceFromPrompts(copy);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLib.NUnitTests
{
    using System;
    using Thales.KentFire.GD92;

	/// <summary>
	/// Provides access to the message sent for test purposes
	/// </summary>
	public class MockMessageSender : IGD92MessageSender
	{
		public MockMessageSender()
		{
		}
		
		/// <summary>
		/// Basis of serial number returned by SendMessage
		/// </summary>
		public int LastSerialNumber { get; set; }
		
		/// <summary>
		/// Reference to the argument of the most recent call of SendMessage
		/// </summary>
		public GD92Message MessageSent { get; private set; }
		
		/// <summary>
		/// Make the message available as a property of this class,
		/// increment the last serial number and return it
		/// </summary>
		/// <param name="message"></param>
		/// <returns>Serial number for the message</returns>
		public int SendMessage(GD92Message message)
		{
			this.MessageSent = message;
			IncrementSerialNumber();
			return this.LastSerialNumber;
		}
		private void IncrementSerialNumber()
		{
			if (this.LastSerialNumber >= GD92Component.MaxSerialNumber ||
			    this.LastSerialNumber < 0 )
            {
                this.LastSerialNumber = 0;
            }
			this.LastSerialNumber++;
		}
	}
}

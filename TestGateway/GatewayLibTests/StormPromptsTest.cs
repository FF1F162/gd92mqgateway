﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GatewayLibTests
{
    using Thales.KentFire.GatewayLib;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    /// <summary>
    ///This is a test class for StormPromptsTest and is intended
    ///to contain all StormPromptsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StormPromptsTest
    {

        private const string Msg2Data = @"Incident Time:  15:20:41 28/11/2014Incident Type:  FIRE CHIMNEY, FIRE CHIMNEYAssistance message:  assistanceAll resouces list:  FJK11P1,FJK20P1,FJK11R1First Attendance:  FJK21P1 [:], ALEX FARM, DUCK LN, SHADOXHURST, ASHFORDMap book:                                                                mapbookOSGR:  597024137541Tel Number:  12345Loc Info:  infoLocation Notices:  noticeLocation Risk:  riskLocation Comment:  comment";
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ExtractIncidentTime
        ///</summary>
        [TestMethod()]
        public void ExtractIncidentTimeTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            DateTime expected = DateTime.Parse("28-11-2014 15:20:41");
            DateTime actual;
            actual = target.ExtractIncidentTime();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractAssistanceMessage
        ///</summary>
        [TestMethod()]
        public void ExtractAssistanceMessageTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            string expected = "assistance";
            string actual;
            actual = target.ExtractAssistanceMessage();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractIncidentType
        ///</summary>
        [TestMethod()]
        public void ExtractIncidentTypeTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            string expected = "FIRE CHIMNEY, FIRE CHIMNEY";
            string actual;
            actual = target.ExtractIncidentType();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocationComment
        ///</summary>
        [TestMethod()]
        public void ExtractLocationCommentTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            string expected = "comment"; 
            string actual;
            actual = target.ExtractLocationComment();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocationNotices
        ///</summary>
        [TestMethod()]
        public void ExtractLocationNoticesTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            string expected = "notice";
            string actual;
            actual = target.ExtractLocationNotices();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractLocationRisk
        ///</summary>
        [TestMethod()]
        public void ExtractLocationRiskTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            string expected = "risk";
            string actual;
            actual = target.ExtractLocationRisk();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractMapBook
        ///</summary>
        [TestMethod()]
        public void ExtractMapBookTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            string expected = "mapbook";
            string actual;
            actual = target.ExtractMapBook();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ExtractTelNumber
        ///</summary>
        [TestMethod()]
        public void ExtractTelNumberTest()
        {
            string msg2Text = Msg2Data;
            StormPrompts target = new StormPrompts(msg2Text);
            string expected = "12345"; 
            string actual;
            actual = target.ExtractTelNumber();
            Assert.AreEqual(expected, actual);
        }
    }
}

﻿using KFRS.MDT.MobiliseMessages;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Text;




namespace RabbitMQConnection
{
    public class MessageEventArgs : EventArgs
    {
        public DateTime ReceivedTime;
        public Message Message;
    }

    // Utterly unsafe code not for production
    public class RabbitClient
    {
        public Message LastReceivedMessage;
        public event EventHandler<MessageEventArgs> MessageReceivedHandler;
        public static bool IsStarted { get; private set; }

        private static ConnectionFactory RabbitFactory;
        private static IConnection RabbitConnection;
        private static IModel RabbitChannel;
        private string QueueName;
        private string ExchangeName;
        private object lockObject = new object();
        private static EventingBasicConsumer Consumer;

        public RabbitClient(string queueName, string exchangeName)
        {
            QueueName = queueName;
            ExchangeName = exchangeName;
        }

        public void Start()
        {
            lock(lockObject)
            {
                if (!IsStarted)
                {
                    TestLogger("Start Rabbit client");
                    RabbitFactory = new ConnectionFactory() { HostName = "devsgw1", UserName = "dev", Password = "berry" };
                    RabbitConnection = RabbitFactory.CreateConnection();
                    RabbitChannel = RabbitConnection.CreateModel();
                    RabbitChannel.QueueDeclare(queue: QueueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
                    TestLogger("Connect to queue " + QueueName + "  on exchange " + ExchangeName);
                    RabbitChannel.QueueBind(queue: QueueName, exchange: ExchangeName, routingKey: QueueName);

                    Consumer = new EventingBasicConsumer(RabbitChannel);
                    Consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        object obj = Serializers.DeSerialize(body);
                        Message mess = obj as Message;
                        if (mess != null)
                        {
                            if (MessageReceivedHandler != null)
                            {
                                MessageReceivedHandler.Invoke(this, new MessageEventArgs { ReceivedTime = DateTime.UtcNow, Message = mess });
                            }
                        }
                        else
                        {
                            // some error logging
                        }
                    };

                    RabbitChannel.BasicConsume(queue: QueueName, noAck: true, consumer: Consumer);
                    IsStarted = true;
                }
            }
        }
        
        public void Stop()
        {
            lock(lockObject)
            {
                if (IsStarted)
                {
                    RabbitChannel.Close();
                    RabbitConnection.Close();
                    RabbitChannel.Dispose();
                    RabbitChannel.Dispose();
                    IsStarted = false;
                }
            }
        }

        public int SendMessageToQueue(string queueName, string exchange, Message msg)
        {
            lock(lockObject)
            {
                if (!IsStarted)
                    return 0;

                var body = Serializers.Serialize(msg);
                RabbitChannel.BasicPublish(exchange: exchange, routingKey: queueName, basicProperties: null, body: body);
                return body.Length;
            }
        }
        
        public class Serializers
        {
            public static byte[] Serialize(object toSerialize)
            {
                var jsonBody = JsonConvert.SerializeObject(toSerialize);
                byte[] serializedBuffer = Encoding.UTF8.GetBytes(jsonBody);

                return serializedBuffer;
            }


            public static object DeSerialize(byte[] toDeserialize)
            {
                string jsonBody = Encoding.UTF8.GetString(toDeserialize);
                object returnObject = JsonConvert.DeserializeObject(jsonBody);

                return returnObject;
            }
        }

        private object logLockObject = new object();
        const string LogFileName = @"C:\POCLogs\Router.txt";

        public void TestLogger(string message)
        {
            lock (logLockObject)
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(DateTime.UtcNow.ToString() + " : " + message);
                }
            }
        }
    }
}

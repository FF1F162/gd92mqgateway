﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.StormGway
{
    using System;
    using System.ServiceProcess;
    using System.Threading;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.LogLib;

    /// <summary>
    /// Overrides ServiceBase methods to start and shut down the service.
    /// </summary>
    public partial class StormGatewayService : ServiceBase
    {
        public StormGatewayService()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The try-catch is intended to make sure the service stops if there is a problem
        /// during start-up.
        /// </summary>
        /// <param name="args">None used.</param>
        protected override void OnStart(string[] args)
        {
            try
            {
                Gateway.Instance.Configuration = new LogConfiguration();
                Gateway.Instance.Configuration.RefreshFromAppConfig();
                Gateway.Instance.EventLogger = new ExternalEventLogger(this.ServiceName);
                Gateway.Instance.Activate();
            }
            catch (Exception)
            {
                this.ExitCode = 1064; // ? ERROR_EXCEPTION_IN_SERVICE
                this.Stop();
                throw;
            }
        }

        /// <summary>
        /// Give the shutdown thread in GD92Component time to work.
        /// Swallow any exception so the service controller (SCM) does not leave the service
        /// partially running (as happened 20 Feb 2015).
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                Gateway.Instance.Deactivate();
                Thread.Sleep(EventControl.EventQueueTimeSpan * 3);
            }
            catch (Exception)
            {
                this.ExitCode = 1064; // ? ERROR_EXCEPTION_IN_SERVICE
            }
            finally
            {
                LogControl.Instance.FlushSavedLogs();
                LogControl.Instance.Dispose();
            }
        }
    }
}

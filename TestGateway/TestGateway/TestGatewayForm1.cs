﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.TestGateway
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.GatewayLib;
    using Thales.KentFire.GD92;
    using Thales.KentFire.GD92.Decoders;
    using Thales.KentFire.LogLib;

    /// <summary>
    /// Test container for the Storm gateway (normally runs as a service).
    /// </summary>
    public sealed partial class TestGatewayForm1 : Form
    {
        public const string DefaultPathToTestFolders = @"C:\git\kfrs\StormGateway\TestGateway";
        public const string DefaultAppConfigKeyForTestInputPath = "TestInput";
        private string testInputPath;

        private IGD92MessageSender messageSender = GD92Component.Instance;
        private Gateway gateway = Gateway.Instance;
        private string logName;
        private int resourceStatusCode;
        private IRouter nodeLookup;
        private DecoderFactory decoders;
        private GD92Message message;
        private List<GD92Message> messages;
        private List<Frame> messageFrames;

        private delegate void SetTextCallback(string text);

        /// <summary>
        /// Initialises a new instance of the <see cref="TestGatewayForm1"/> class.
        /// </summary>
        public TestGatewayForm1()
        {
            this.InitializeComponent();

            this.logName = "TestGateway";
            this.Text = this.logName + " ";
            LogControl.Instance.LoggingName = this.logName;

            comboBox1.Items.AddRange(Enum.GetNames(typeof(StatusCode)));

            // register event handler to close connections and tidy up if form is closed
            this.FormClosed += new FormClosedEventHandler(this.Form1_FormClosed);

            // register event handler to set the local node name in the form title
            // (waiting for this event ensures that the GD92 component has had time to read the name)
            GD92Component.Instance.NodeConnected += new EventHandler<NodeConnectedEventArgs>(this.GD92Component_NodeConnected);

            // Use MockRouter to get node information e.g. for special handling of Msg27 from Storm
            // Use Router only when activated.
            this.nodeLookup = new MockRouter();
            this.decoders = new DecoderFactory(this.nodeLookup);
            this.messages = new List<GD92Message>();
            this.messageFrames = new List<Frame>();

            this.gateway.EmulatorAcknowledgementCode = -1;
            this.textBox7.Text = "Ack";
            this.gateway.EmulatorAcknowledgementDelayInSeconds = 0;
            this.textBox8.Text = "0";
            this.testInputPath = FolderFromAppConfigOrDefault(DefaultAppConfigKeyForTestInputPath);
        }

        /// <summary>
        /// Return test input or output path in the same way as for unit tests.
        /// The given key forms the last part of the default name.
        /// </summary>
        /// <param name="key">Key for entry in App.config.</param>
        /// <returns>Folder as defined in App.config or default.</returns>
        private static string FolderFromAppConfigOrDefault(string key)
        {
            string folder = null;
            folder = ConfigurationManager.AppSettings[key];
            if (folder == null)
            {
                folder = Path.Combine(DefaultPathToTestFolders, key);
            }

            return folder;
        }

        /// <summary>
        /// Use MessageBox.Show to display the text.
        /// </summary>
        /// <param name="text">Text to display.</param>
        private static void DisplayText(string text)
        {
            MessageBox.Show(
                text,
                string.Empty,
                MessageBoxButtons.OK,
                MessageBoxIcon.None,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.RtlReading);
        }

        private static byte[] CleanAndConvert(string testData)
        {
            string terminated = LogReader.TerminateAtStop(testData);
            terminated = LogReader.ConvertToBitConverterFormat(terminated);
            return LogReader.ConvertToByteArray(terminated);
        }
        
        private static string FrameHeaderFields(Frame frame)
        {
            var sb = new StringBuilder("Byte");
            sb.Append(Environment.NewLine);
            sb.Append("0                               01 (SOH) expected");
            sb.Append(Environment.NewLine);
            sb.Append("1-3   Source address            ");
            sb.Append(frame.Source.ToString());
            sb.Append(Environment.NewLine);
            sb.Append("4-5   Frame message length      ");
            sb.Append(frame.FrameMessageLength);
            sb.Append(Environment.NewLine);
            sb.Append("5     Destination count         ");
            sb.Append(frame.DestinationCount);
            sb.Append(Environment.NewLine);
            sb.Append("6-8   Destination address       ");
            sb.Append(frame.Destination.ToString());
            sb.Append(Environment.NewLine);
            sb.Append("9     Protocol version          ");
            sb.Append(frame.ProtocolVersion.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append(Environment.NewLine);
            sb.Append("9     Priority                  ");
            sb.Append(frame.Priority.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append(Environment.NewLine);
            sb.Append("10-11 Sequence number           ");
            sb.Append(frame.SequenceNumber.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append(Environment.NewLine);
            sb.Append("11    Acknowledgement required  ");
            sb.Append(frame.AckRequired.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append(Environment.NewLine);
            sb.Append("12    Message type              ");
            sb.Append(frame.MessageType.ToString());
            sb.Append(Environment.NewLine);
            sb.Append("      Block                     ");
            sb.Append(frame.Block.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append(" of ");
            sb.Append(frame.OfBlocks.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append(Environment.NewLine);
            return sb.ToString();
        }

        /// <summary>
        /// Handle node connected event.
        /// In the absence of an 'Activated' event this serves to show the configuration has been read.
        /// Use this to retrieve the name of the local node and display it in the title.
        /// While activated, use the information in the Registry (rather than in MockRouter).
        /// Unsubscribe to prevent further additions.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event parameters.</param>
        private void GD92Component_NodeConnected(object sender, NodeConnectedEventArgs e)
        {
            this.SetText(GD92Component.LocalNodeName);
            this.nodeLookup = Router.Instance;
            this.decoders = new DecoderFactory(this.nodeLookup);
            GD92Component.Instance.NodeConnected -= this.GD92Component_NodeConnected;
        }

        /// <summary>
        /// Use thread-safe access to add the local node name to the title of the form.
        /// </summary>
        /// <param name="text">Local node name.</param>
        private void SetText(string text)
        {
            if (this.InvokeRequired)
            {
                var d = new SetTextCallback(this.SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.Text += text;
            }
        }
            
        /// <summary>
        /// De-activate when the form closes.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DeactivateGateway();
            LogControl.Instance.CloseLoggingForm();
        }

        /// <summary>
        /// Activate - read gateway configuration and activate GD-92.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button1_Click(object sender, EventArgs e)
        {
            if (Gateway.Instance.Activated)
            {
                DisplayText("Already activated");
            }
            else
            {
                Gateway.Instance.Configuration = new TestLogConfiguration();
                Gateway.Instance.Configuration.LoggingLevel = EventLib.LogLevel.Info;
                Gateway.Instance.EventLogger = new ExternalEventLogger();

                // open logging form to collect logs but do not show until required
                LogControl.Instance.OpenLoggingForm(false);

                this.ActivateGateway();
            }
        }

        /// <summary>
        /// Activate the gateway and check the result of activating GD-92.
        /// Note that activation runs in a new thread so this returns before gateway is fully configured.
        /// </summary>
        private void ActivateGateway()
        {
            GD92.ControlResult r = this.gateway.Activate();
            if (r != GD92.ControlResult.Success)
            {
                DisplayText("Activation failed - see log for details");
            }
        }

        /// <summary>
        /// DeActivate the gateway and GD-92 without closing the form.
        /// Return to using MockRouter for node information.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button2_Click(object sender, EventArgs e)
        {
            if (!this.gateway.Activated)
            {
                DisplayText("Not activated");
            }
            else
            {
                this.DeactivateGateway();
                this.nodeLookup = new MockRouter();
                this.decoders = new DecoderFactory(this.nodeLookup);
            }
        }

        /// <summary>
        /// De-activate the gateway (and GD-92).
        /// </summary>
        private void DeactivateGateway()
        {
            GD92.ControlResult r = this.gateway.Deactivate();
            if (r == GD92.ControlResult.Success)
            {
                DisplayText("Gateway dectivated");
            }
        }

        /// <summary>
        /// Log - open the logging form.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button3_Click(object sender, EventArgs e)
        {
            LogControl.Instance.OpenLoggingForm();
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.resourceStatusCode = (int)Enum.Parse(typeof(StatusCode), comboBox1.SelectedItem.ToString(), false);
        }

        /// <summary>
        /// Send Resource Status.
        /// When emulating Storm do not require or expect acknowledgement.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button4_Click(object sender, EventArgs e)
        {
            var resourceStatus = new GD92Msg20();
            resourceStatus.Callsign = textBox2.Text;
            resourceStatus.StatusCode = this.resourceStatusCode;
            var sendTime = DateTime.Now.ToString(
                LogConfiguration.Msg20RemarksTimeDateFormat,
                System.Globalization.DateTimeFormatInfo.InvariantInfo);
            resourceStatus.Remarks = (textBox4.Text.Length > 0) ? 
                sendTime + " " + textBox4.Text : 
                sendTime;
            if (this.gateway.IsStormEmulator)
            {
                resourceStatus.SendWithNoAckRequired = true;
                resourceStatus.SendWithNoReplyExpected = true;
            }

            this.SendMessage(resourceStatus);
        }

        /// <summary>
        /// Send Request - for resource status.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button5_Click(object sender, EventArgs e)
        {
            var statusRequest = new GD92Msg5();
            statusRequest.Callsign = textBox2.Text;
            this.SendMessage(statusRequest);
        }

        private void SendMessage(GD92Message messageToSend)
        {
            try
            {
                messageToSend.Destination = textBox1.Text;
                this.messageSender.SendMessage(messageToSend);
            }
            catch (Exception ex)
            {
                DisplayText(ex.ToString());
            }
        }

        /// <summary>
        /// Send Log Update.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button6Click(object sender, System.EventArgs e)
        {
            var logUpdate = new GD92Msg22();
            logUpdate.Callsign = textBox2.Text;
            logUpdate.IncidentNumber = textBox3.Text;
            logUpdate.Update = textBox4.Text;
            this.SendMessage(logUpdate);
        }

        /// <summary>
        /// Send Msg6 with all address elements full as Msg6 - this can be sent to the IPSE and MDT
        /// to check that they deal with the address correctly.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button7_Click(object sender, EventArgs e)
        {
            this.MaxLengthElementsAddressTest();
        }

        /// <summary>
        /// Send Msg6 with minimum details and all address elements populated to the limit.
        /// </summary>
        private void MaxLengthElementsAddressTest()
        {
            var message6 = new GD92Msg6();
            message6.IncidentNumber = textBox3.Text;
            message6.Address = "A23456789 B23456789 C23456789 D23456789 E23456789 F23456789 G23456789 H23456789 I23456789 J23456789 K23456789 L23456789A";
            message6.HouseNumber = "A23456789H";
            message6.Street = "A23456789 B23456789 C23456789 D23456789S";
            message6.SubDistrict = "A23456789 B23456789 C23456789S";
            message6.District = "A23456789 B23456789 C23456789D";
            message6.Town = "A23456789 B23456789 C23456789T";
            message6.County = "A23456789 B23456789C";
            this.SendMessage(message6);
        }

        /// <summary>
        /// Show ASCII
        /// Read data in BitConverter format from text box 5 and display it in text box 6
        /// as lines of ten characters in byte and ASCII format.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button8_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] messageData = CleanAndConvert(this.textBox5.Text);
                var logger = new ByteLogger(messageData);
                this.textBox6.Text = logger.PrintLog(0, messageData.Length);
            }
            catch (Exception ex)
            {
                DisplayText(ex.Message);
                this.textBox6.Text = string.Empty;
            }
        }
        
        /// <summary>
        /// Show XML
        /// Decode the list of message frames collected previously or if none read date directly from text box 5.
        /// Display XML output in text box 6.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button9_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.messageFrames.Count == 0)
                {
                    this.textBox6.Text = this.DecodeLogData();
                }
                else
                {
                    Frame[] frames = this.messageFrames.ToArray();
                    this.textBox6.Text = this.DecodeFramesAndMessage(frames);
                }
            }
            catch (Exception ex)
            {
                DisplayText(ex.Message);
                this.textBox6.Text = string.Empty;
                this.message = null;
            }
        }

        /// <summary>
        /// Read log data directly from the text box and decode it as a single frame message.
        /// </summary>
        /// <returns>XML representation of decoded message.</returns>
        private string DecodeLogData()
        {
            var frames = new Frame[1];
            frames[0] = this.ReconstructFrameFromLogData(this.textBox5.Text);
            return this.DecodeFramesAndMessage(frames);
        }

        /// <summary>
        /// Read log data from the text box and reconstruct it as a frame.
        /// </summary>
        /// <param name="text">Line of data from log.</param>
        /// <returns>Decoded frame.</returns>
        private Frame ReconstructFrameFromLogData(string text)
        {
            byte[] bytes = CleanAndConvert(text);
            var frame = new Frame(bytes);
            var frameDecoder = new FrameDecoder(frame, this.nodeLookup);
            frameDecoder.DecodeFrame();
            return frame;
        }

        /// <summary>
        /// Decode the frames then get a decoder for the message type. Save the message in an instance variable
        /// in case it needs to be added to the list for sending.
        /// </summary>
        /// <param name="frames">Frames comprising the message.</param>
        /// <returns>XML representation of decoded message.</returns>
        private string DecodeFramesAndMessage(Frame[] frames)
        {
            foreach (Frame frame in frames)
            {
                var frameDecoder = new FrameDecoder(frame, this.nodeLookup);
                frameDecoder.DecodeFrame();
            }

            var decoder = this.decoders.DecoderForMessageFrames(frames);
            this.message = decoder.DecodeMessage();
            return decoder.SerializeDecodedMessage();
        }

        /// <summary>
        /// Add message to list for sending.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button10_Click(object sender, EventArgs e)
        {
            if (this.message != null)
            {
                this.message.ResetCommonFields();
                this.messages.Add(this.message);
                this.message = null;
                this.textBox6.Clear();
            }
            else
            {
                DisplayText("No message to add");
            }
        }

        /// <summary>
        /// Send messages in list.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button11_Click(object sender, EventArgs e)
        {
            foreach (GD92Message testMessage in this.messages)
            {
                this.SendMessage(testMessage);
            }
        }

        /// <summary>
        /// Clear the list of messages.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button12_Click(object sender, EventArgs e)
        {
            this.messages.Clear();
        }

        /// <summary>
        /// Add frame.
        /// Decode the data in the text box as a frame then validate the block and OfBlocks against any frames already
        /// collected before adding it to the list.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button13_Click(object sender, EventArgs e)
        {
            try
            {
                Frame frame = this.ReconstructFrameFromLogData(this.textBox5.Text);
                this.messageFrames.Add(frame);
                this.textBox5.Text = string.Empty;
            }
            catch (Exception ex)
            {
                DisplayText(ex.Message);
            }
        }

        /// <summary>
        /// Clear frames.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button14_Click(object sender, EventArgs e)
        {
            this.ClearFramesAndMessage();
        }

        /// <summary>
        /// Show Block and OfBlocks of frames collected.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button15_Click(object sender, EventArgs e)
        {
            var frameList = new StringBuilder();
            foreach (Frame frame in this.messageFrames)
            {
                frameList.AppendFormat("Frame {0} block {1} of {2}",
                    frame.SequenceNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    frame.Block.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    frame.OfBlocks.ToString(System.Globalization.CultureInfo.InvariantCulture));
                frameList.AppendLine();
            }

            DisplayText(frameList.ToString());
        }

        /// <summary>
        /// Acknowledgement code.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void TextBox7_TextChanged(object sender, EventArgs e)
        {
            int code = -1;
            if (int.TryParse(this.textBox7.Text, out code))
            {
                this.gateway.EmulatorAcknowledgementCode = code;
            }
            else
            {
                this.gateway.EmulatorAcknowledgementCode = -1;
                this.textBox7.Text = string.Empty;
            }

            if (code < 0)
            {
                this.gateway.EmulatorAcknowledgementCode = -1;
                this.textBox7.Text = string.Empty;
            }
        }

        /// <summary>
        /// Acknowledgement delay.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void TextBox8_TextChanged(object sender, EventArgs e)
        {
            int delay = 0;
            if (int.TryParse(this.textBox8.Text, out delay))
            {
                if (delay >= 0)
                {
                    this.gateway.EmulatorAcknowledgementDelayInSeconds = delay;
                }
            }
            else
            {
                this.gateway.EmulatorAcknowledgementDelayInSeconds = 0;
                this.textBox8.Text = "0";
            }

            if (delay < 0)
            {
                this.gateway.EmulatorAcknowledgementDelayInSeconds = 0;
                this.textBox8.Text = "0";
            }
        }
        
        /// <summary>
        /// Show frame.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button16Click(object sender, EventArgs e)
        {
            try
            {
                Frame frame = this.ReconstructFrameFromLogData(this.textBox5.Text);
                this.textBox6.Text = FrameHeaderFields(frame);
            }
            catch (Exception ex)
            {
                DisplayText(ex.Message);
                this.textBox6.Text = string.Empty;
            }
        }

        /// <summary>
        /// Read all frames in file.
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button17_Click(object sender, EventArgs e)
        {
            try
            {
                this.ReadFramesFromFile();
            }
            catch (Exception ex)
            {
                DisplayText(ex.Message);
            }
        }

        /// <summary>
        /// Read hexadecimal data (as output by BitConverter to the StormGateway log).
        /// Ignore lines that do not begin with SOH.
        /// Load frames comprising a message into the list of frames. 
        /// Decode message and add to the list of messages.
        /// </summary>
        private void ReadFramesFromFile()
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                openFileDialog1.InitialDirectory = this.testInputPath;
                using (var file = new StreamReader(openFileDialog1.FileName))
                {
                    while (!file.EndOfStream)
                    {
                        string line = file.ReadLine();
                        if (line.StartsWith("01-", StringComparison.Ordinal))
                        {
                            Frame frame;
                            frame = this.ReconstructFrameFromLogData(line);
                            this.messageFrames.Add(frame);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Clear the working area ready for the next input.
        /// </summary>
        private void ClearFramesAndMessage()
        {
            this.messageFrames.Clear();
            this.message = null;
        }

        /// <summary>
        /// Send all frames.
        /// Read each one from the list, create a RawFrameMsg and send it to the Node (specified elsewhere on the form).
        /// </summary>
        /// <param name="sender">Event publisher.</param>
        /// <param name="e">Event arguments.</param>
        private void Button18_Click(object sender, EventArgs e)
        {
            foreach (Frame frame in this.messageFrames)
            {
                var rawFrameMessage = new RawFrameMsg();
                rawFrameMessage.LogOfFrameBytes = BitConverter.ToString(frame.FrameBytes, 0, frame.Length);
                this.SendMessage(rawFrameMessage);
            }
        }
    }
}

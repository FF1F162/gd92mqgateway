﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace MapBook.NUnitTests
{
    using NUnit.Framework;
    using System;
    using System.Configuration;
    using System.IO;
    using System.Xml.Serialization;
    using Thales.Kentfire.MapLib;
    
    [TestFixture]
    public class MapBookTests
    {
		public const string DefaultPathToTestFolders = @"C:\git\kfrs\StormGateway\MapBook";
		
        private static string TestInputPath;
        private static string TestOutputPath;
        
        [TestFixtureSetUp]
		public void Init()
		{
            TestInputPath = FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = FolderFromAppConfigOrDefault("TestOutput");
		}
		
        public static string FolderFromAppConfigOrDefault(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? Path.Combine(DefaultPathToTestFolders, key);
        }

        [Test]
        public void ReadAndWriteWestKentMapBook()
        {
            var westKentData = ReadFromFile<MapBookData>(Path.Combine(TestInputPath, "WestKent.txt"));
            SaveToFile<MapBookData>(westKentData, Path.Combine(TestOutputPath, "WestKent.txt"));
        }

        [Test]
        public void ReadWestKentMapBookAndWriteWithAdjustments()
        {
            var westKentData = ReadFromFile<MapBookData>(Path.Combine(TestInputPath, "WestKent.txt"));

            var adjustment = new PageAdjustment();
            adjustment.BaseNorth = westKentData.MaximumNorth - 40;
            Assert.AreEqual(adjustment.BaseNorth, westKentData.MinimumNorth + (16 * 40));
            adjustment.PageNumber = 11;
            adjustment.NewBaseEast = westKentData.PageRows[16].FirstEast + (10 * 30);
            adjustment.NewBaseNorth = adjustment.BaseNorth - 10;
            adjustment.NewPageNumber = 5;
            westKentData.PageAdjustments.Add(adjustment);
            SaveToFile<MapBookData>(westKentData, Path.Combine(TestOutputPath, "WestKentAdjusted.txt"));
        }

        [Test]
        public void ReadAndWriteEastKentMapBook()
        {
            var eastKentData = ReadFromFile<MapBookData>(Path.Combine(TestInputPath, "EastKent.txt"));
            SaveToFile<MapBookData>(eastKentData, Path.Combine(TestOutputPath, "EastKent.txt"));
        }

        [Test]
        public void WestKentMapBookCalculatorTest()
        {
            var data = ReadFromFile<MapBookData>(Path.Combine(TestInputPath, "WestKent.txt"));
            var calculator = new MapBookCalculator(data);
            string actual = calculator.MapBookPageAndCellForGridReference(5800, 1600);
            const string expected = "85C5";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void WestKentNoEmptyRowsTest()
        {
            var data = ReadFromFile<MapBookData>(Path.Combine(TestInputPath, "WestKentNoEmptyRows.txt"));
            var calculator = new MapBookCalculator(data);
            string actual = calculator.MapBookPageAndCellForGridReference(5800, 1600);
            const string expected = "85C5";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void EastKentMapBookCalculatorTest()
        {
            var data = ReadFromFile<MapBookData>(Path.Combine(TestInputPath, "EastKent.txt"));
            var calculator = new MapBookCalculator(data);
            string actual = calculator.MapBookPageAndCellForGridReference(5800, 1600);
            const string expected = "55C5";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void EastKentNoEmptyRowsTest()
        {
            var data = ReadFromFile<MapBookData>(Path.Combine(TestInputPath, "EastKentNoEmptyRows.txt"));
            var calculator = new MapBookCalculator(data);
            string actual = calculator.MapBookPageAndCellForGridReference(5800, 1600);
            const string expected = "55C5";
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Serialize as XML to file using the full path specified. Suppress output of namespaces to avoid interference with tests.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">object to serialize</param>
        /// <param name="fileName">Full path of file to write</param>
        public static void SaveToFile<T>(T data, string fileName)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            var s = new XmlSerializer(typeof(T));
            using (TextWriter myWriter = new StreamWriter(fileName))
            {
                s.Serialize(myWriter, data, ns);
            }
        }
        
        /// <summary>
        /// Read XML string from file and deserialize to object of the type specified.
        /// </summary>
        /// <typeparam name="T">Type that can be de-serialized from XML.</typeparam>
        /// <param name="fileName">Full path of the file containing XML.</param>
        /// <returns></returns>
        public static T ReadFromFile<T>(string fileName)
        {
            var d = new XmlSerializer(typeof(T));
            using (var f = new FileStream(fileName, FileMode.Open))
            {
                return (T)d.Deserialize(f);
            }
        }
    }
}

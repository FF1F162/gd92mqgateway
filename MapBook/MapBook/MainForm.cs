﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace MapBook
{
    using System;
    using System.IO;
    using System.Windows.Forms;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Test application for map book reference from OS grid reference.
    /// </summary>
    public partial class MainForm : Form
    {
        //public const string DataPath = @"C:\git\kfrs\StormGateway\MapLib\TestInput";
        public const string DataPath = @".\";

        private IMapBook mapBooks;
        private PageSections aToZMapBooks;
        
        public MainForm()
        {
            this.InitializeComponent();
            
            this.mapBooks = new KentMapBooks();
            this.aToZMapBooks = new PageSections();
            aToZMapBooks.LoadAndAddSection(Path.Combine(DataPath, "sa_mr_kent2.csv"));
            aToZMapBooks.LoadAndAddSection(Path.Combine(DataPath, "sa_st_kent3.csv"));
            aToZMapBooks.LoadAndAddSection(Path.Combine(DataPath, "sa_ls_kenta2.csv"));
            aToZMapBooks.LoadAndAddSection(Path.Combine(DataPath, "sa_ls_kentb2.csv"));
        }
        
        private void Button1Click(object sender, EventArgs e)
        {
            this.textBox3.Text = string.Empty;
            this.SetCoOrdinates();
            this.textBox3.Text = this.mapBooks.MapBookForGridReference(this.textBox4.Text);
            this.textBox5.Text = this.aToZMapBooks.MapBookForGridReference(this.textBox4.Text);
        }
        
        private void SetCoOrdinates()
        {
            if (this.textBox1.Text.Length > 0)
            {
                this.textBox4.Text = this.textBox1.Text + "00" + this.textBox2.Text + "00";
            }
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2016 All Rights Reserved

namespace MapLib.NUnitTests
{
    using NUnit.Framework;
    using System;
    using System.Configuration;
    using System.IO;
    using System.Xml.Serialization;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Read map-book page specification data provided by A to Z and use OS grid references to retrieve records.
    /// (The original csv has been converted to unl format.)
    /// </summary>
    [TestFixture]
    public class PageSectionsTests
    {
        public const string DataPath = @"C:\git\kfrs\StormGateway\MapLib\TestInput";

        private static PageSections Sections;

        [TestFixtureSetUp]
        public void Init()
        {
            Sections = new PageSections();
            Sections.LoadAndAddSection(Path.Combine(DataPath, "sa_mr_kent2.unl"));
            Sections.LoadAndAddSection(Path.Combine(DataPath, "sa_st_kent3.unl"));
            Sections.LoadAndAddSection(Path.Combine(DataPath, "sa_ls_kenta2.unl"));
            Sections.LoadAndAddSection(Path.Combine(DataPath, "sa_ls_kentb2.unl"));
        }

        /// <summary>
        /// Find page and cell references for an OS grid reference.
        /// </summary>
        [Test]
        public void MapBookForGridReferenceTest()
        {
            string reference = Sections.MapBookForGridReference("539600170100");
            //Assert.AreEqual("1K13 3J69 1H233", reference);
            Assert.AreEqual("13 1K, 69 3J, 233 1H", reference);
        }

        /// <summary>
        /// Find page and cell references for an OS grid reference.
        /// </summary>
        [Test]
        public void MapBookForGridReference2Test()
        {
            string reference = Sections.MapBookForGridReference("549600175100");
            //Assert.AreEqual("6E6 2C58", reference);
            Assert.AreEqual("6 6E, 58 2C", reference);
        }

        /// <summary>
        /// OS grid reference falls (in the sea) outside the map book pages.
        /// </summary>
        [Test]
        public void MapBookForGridReferenceNotFoundTest()
        {
            string reference = Sections.MapBookForGridReference("649600170100");
            Assert.AreEqual(PageSection.NotFound, reference);
        }

        /// <summary>
        /// Find page and cell references for an OS grid reference, specifying book name.
        /// </summary>
        [Test]
        public void MapBookForGridReferenceBookNameTest()
        {
            var newSections = new PageSections();
            newSections.LoadAndAddSection(Path.Combine(DataPath, "sa_mr_kent2.unl"), "MR");
            newSections.LoadAndAddSection(Path.Combine(DataPath, "sa_st_kent3.unl"), "ST");
            string reference = newSections.MapBookForGridReference("549600155100");
            Assert.AreEqual("MR24 2E ST118 6C", reference);
        }

        /// <summary>
        /// Find page and cell references for an OS grid reference, specifying book name, from csv.
        /// </summary>
        [Test]
        public void MapBookForGridReferenceBookNameCsvTest()
        {
            var newSections = new PageSections();
            newSections.LoadAndAddSection(Path.Combine(DataPath, "sa_mr_kent2.csv"), "MR");
            newSections.LoadAndAddSection(Path.Combine(DataPath, "sa_st_kent3.csv"), "ST");
            string reference = newSections.MapBookForGridReference("549600155100");
            Assert.AreEqual("MR24 2E ST118 6C", reference);
        }

        /// <summary>
        /// Find page and cell references for an OS grid reference with no data loaded.
        /// </summary>
        [Test]
        public void MapBookForGridReferenceNoDataTest()
        {
            var newSections = new PageSections();
            string reference = newSections.MapBookForGridReference("549600155100");
            Assert.AreEqual(PageSection.NotFound, reference);
        }

        /// <summary>
        /// Find page and cell references for an OS grid reference with empty file loaded.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Section contains no data")]
        public void MapBookForGridReferenceEmptyFileTest()
        {
            var newSections = new PageSections();
            newSections.LoadAndAddSection(Path.Combine(DataPath, "sa_empty.csv"));
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2016 All Rights Reserved

namespace MapLib.NUnitTests
{
    using NUnit.Framework;
    using System;
    using System.Configuration;
    using System.IO;
    using System.Xml.Serialization;
    using Thales.KentFire.MapLib;

    /// <summary>
    /// Read map-book page specification data provided by A to Z and use OS grid references to retrieve records.
    /// (The original csv has been converted to unl format.)
    /// </summary>
    [TestFixture]
    public class PageSectionTests
    {
        public const string DataPath = @"C:\git\kfrs\StormGateway\MapLib\TestInput";

        [Test]
        //[ExpectedException(ExpectedExceptionName = "System.ArgumentException", ExpectedMessage = "Empty path name is not legal.")]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Section file path is empty")]
        public void ReadDataFromFileNoPathTest()
        {
            var section = new PageSection();
            section.LoadData();
        }

        /// <summary>
        /// Read in the 'main road' pages.
        /// </summary>
        [Test]
        public void ReadDataFromFileMainRoadTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_mr_kent2.unl"));
            section.LoadData();
            Assert.AreEqual(1540, section.CellCount);
        }

        /// <summary>
        /// Read in the 'main road' pages (csv version).
        /// </summary>
        [Test]
        public void ReadDataFromFileMainRoadCsvTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_mr_kent2.csv"));
            section.LoadData();
            Assert.AreEqual(1540, section.CellCount);
        }

        /// <summary>
        /// Read in the 'street map' pages.
        /// </summary>
        [Test]
        public void ReadDataFromFileStreetMapTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_st_kent3.unl"));
            section.LoadData();
            Assert.AreEqual(10980, section.CellCount);
        }

        /// <summary>
        /// Read in the 'large scale A' pages.
        /// </summary>
        [Test]
        public void ReadDataFromFileLargeScaleATest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kenta2.unl"));
            section.LoadData();
            Assert.AreEqual(324, section.CellCount);
        }

        /// <summary>
        /// Read in the 'large scale B' pages.
        /// </summary>
        [Test]
        public void ReadDataFromFileLargeScaleBTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kentb2.unl"));
            section.LoadData();
            Assert.AreEqual(24, section.CellCount);
        }

        /// <summary>
        /// Check that LoadData discards or clears old data before loading new.
        /// </summary>
        [Test]
        public void ReadDataFromFileReadAgainTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kentb2.unl"));
            section.LoadData();
            section.LoadData();
            Assert.AreEqual(24, section.CellCount);
        }

        /// <summary>
        /// Read in the 'large scale B' pages. Ignore header lines
        /// and empty embedded lines.
        /// </summary>
        [Test]
        public void ReadDataFromFileLargeScaleBHeaderTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kentb2header.unl"));
            section.LoadData();
            Assert.AreEqual(24, section.CellCount);
        }

        /// <summary>
        /// Read in the 'large scale B' pages. Throw exception for embedded line in wrong format.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Expected format is 'cell|page|east|west|north|south'. This line is wrong:This line is expected to cause exception")]
        public void ReadDataFromFileLargeScaleBEmbeddedTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kentb2embedded.unl"));
            section.LoadData();
        }

        /// <summary>
        /// Read in the 'large scale B' pages. Throw exception for missing digit.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Expected six-digit number. Instead found:15800")]
        public void ReadDataFromFileLargeScaleBMissingDigitTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kentb2missingdigit.unl"));
            section.LoadData();
        }

        /// <summary>
        /// Read in the 'large scale B' pages with final digit missing from each line. 
        /// Throw exception for missing digit after header count exceeds maximum number allowed.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Expected six-digit number. Instead found:15801")]
        public void ReadDataFromFileLargeScaleBMissingDigitThroughoutTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kentb2missingdigits.unl"));
            section.LoadData();
        }

        /// <summary>
        /// Retrieve a record from the 'large scale A' pages.
        /// </summary>
        [Test]
        public void CellMatchingOSReferenceLargeScaleATest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kenta2.unl"));
            section.LoadData();
            var cell = section.CellMatchingOSReference("539600170100");
            Assert.AreEqual("1H", cell.CellReference);
        }

        /// <summary>
        /// Retrieve a record from the 'large scale A' pages (csv version).
        /// </summary>
        [Test]
        public void CellMatchingOSReferenceLargeScaleACsvTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_ls_kenta2.csv"));
            section.LoadData();
            var cell = section.CellMatchingOSReference("539600170100");
            Assert.AreEqual("1H", cell.CellReference);
        }

        /// <summary>
        /// Retrieve a record from near the end of the 'street' pages.
        /// </summary>
        [Test]
        public void CellMatchingOSReferenceStreetTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_st_kent3.unl"));
            section.LoadData();
            var cell = section.CellMatchingOSReference("596100133600");
            Assert.AreEqual("9F", cell.CellReference);
        }

        /// <summary>
        /// Retrieve a record from near the end of the 'street' pages (csv version).
        /// </summary>
        [Test]
        public void CellMatchingOSReferenceStreetCsvTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_st_kent3.csv"));
            section.LoadData();
            var cell = section.CellMatchingOSReference("596100133600");
            Assert.AreEqual("9F", cell.CellReference);
        }

        /// <summary>
        /// Search the 'street' pages for a reference that is valid but not covered
        /// (this requires traversing the whole list).
        /// </summary>
        [Test]
        public void CellMatchingOSReferenceNotInStreetTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_st_kent3.unl"));
            section.LoadData();
            var cell = section.CellMatchingOSReference("646100133600");
            Assert.AreEqual("0?", cell.CellReference);
        }

        /// <summary>
        /// Specify non-existent file name.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Section file not found")]
        public void LoadDataMissingFileTest()
        {
            var section = new PageSection(Path.Combine(DataPath, "sa_xxx.unl"));
            section.LoadData();
        }

        /// <summary>
        /// Specify non-existent path and file name.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Section file not found")]
        public void LoadDataMissingPathAndFileTest()
        {
            var section = new PageSection("sa_xxx.unl");
            section.LoadData();
        }

        /// <summary>
        /// Specify non-existent path and file name.
        /// </summary>
        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Section file not found")]
        public void LoadDataBadPathAndMissingFileTest()
        {
            var section = new PageSection(@"C:\git\sa_xxx.unl");
            section.LoadData();
        }
    }
}

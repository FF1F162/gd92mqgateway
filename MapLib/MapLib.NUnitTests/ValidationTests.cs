﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace MapLib.NUnitTests
{
    using NUnit.Framework;
    using System;
    using System.Configuration;
    using System.IO;
    using System.Xml.Serialization;
    using Thales.KentFire.MapLib;

    [TestFixture]
    public class ValidationTests
    {
        [Test]
        public void NumberFromDigitsTest()
        {
            int value = Validation.NumberFromDigits("123456");
            Assert.AreEqual(123456, value);
        }

        [Test]
        public void NumberFromDigitsSpaceBeforeTest()
        {
            int value = Validation.NumberFromDigits(" 123456");
            Assert.AreEqual(123456, value);
        }

        [Test]
        public void NumberFromDigitsSpaceAfterTest()
        {
            int value = Validation.NumberFromDigits("123456 ");
            Assert.AreEqual(123456, value);
        }

        [Test]
        public void NumberFromDigitsLetterBeforeTest()
        {
            int value = Validation.NumberFromDigits("A123456");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void NumberFromDigitsLetterAfterTest()
        {
            int value = Validation.NumberFromDigits("123456A");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void NumberFromDigitsEmptyTest()
        {
            int value = Validation.NumberFromDigits("");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void NumberFromDigitsTenTest()
        {
            int value = Validation.NumberFromDigits("1234567890");
            Assert.AreEqual(1234567890, value);
        }

        [Test]
        public void NumberFromDigitsOverflowTest()
        {
            int value = Validation.NumberFromDigits("2234567890");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void NumberFromSixDigitsNotSixTest()
        {
            int value = Validation.NumberFromDigits("EAST");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void CoordinateFromSixDigitsTest()
        {
            int value = Validation.CoordinateFromSixDigits("567890");
            Assert.AreEqual(567890, value);
        }

        [Test]
        public void CoordinateFromSixDigitsLeadingZerosTest()
        {
            int value = Validation.CoordinateFromSixDigits("000890");
            Assert.AreEqual(890, value);
        }

        [Test]
        public void CoordinateFromSixDigitsAllZerosTest()
        {
            int value = Validation.CoordinateFromSixDigits("000000");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void CoordinateFromSixDigitsEmbeddedLetterTest()
        {
            int value = Validation.CoordinateFromSixDigits("567A90");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void CoordinateFromSixDigitsEmbeddedSpaceTest()
        {
            int value = Validation.CoordinateFromSixDigits("567 90");
            Assert.AreEqual(0, value);
        }

        [Test]
        public void CoordinateFromSixDigitsSpaceBeforeTest()
        {
            int value = Validation.CoordinateFromSixDigits(" 56789");
            Assert.AreEqual(56789, value);
        }

        [Test]
        public void CoordinateFromSixDigitsSpaceAfterTest()
        {
            int value = Validation.CoordinateFromSixDigits("56789 ");
            Assert.AreEqual(56789, value);
        }

        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Expected six-digit number. Instead found:EAST")]
        public void CoordinateFromSixDigitsNotSixTest()
        {
            int value = Validation.CoordinateFromSixDigits("EAST");
            Assert.AreEqual(0, value);
        }

        [Test]
        [ExpectedException(ExpectedExceptionName = "Thales.KentFire.MapLib.MapLibException", ExpectedMessage = "Expected six-digit number. Instead found:")]
        public void CoordinateFromSixDigitsEmptyTest()
        {
            int value = Validation.CoordinateFromSixDigits("");
            Assert.AreEqual(0, value);
        }
    }
}

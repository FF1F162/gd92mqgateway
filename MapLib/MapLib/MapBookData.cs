﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Collections.ObjectModel;
    using System.Xml.Serialization;
    
    /// <summary>
    /// Data describing the page layout of the mapbook, capable of saving to XML.
    /// Page row descriptions, minimum and maximum northing and rules for inserts etc.
    /// </summary>
    [XmlRootAttribute]
    public class MapBookData
    {
        private Collection<MapBookPageRow> pageRows;
        private Collection<PageAdjustment> pageAdjustments;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="MapBookData"></see> class.
        /// </summary>
        public MapBookData()
        {
            this.pageRows = new Collection<MapBookPageRow>();
            this.pageAdjustments = new Collection<PageAdjustment>();
        }
        
        /// <summary>
        /// References less than MinimumNorth are not valid.
        /// </summary>
        /// <value>Southernmost extent of map book.</value>
        [XmlAttribute]
        public int MinimumNorth { get; set; }

        /// <summary>
        /// References >= MaximumNorth are not valid.
        /// </summary>
        /// <value>Northernmost extent of map book.</value>
        [XmlAttribute]
        public int MaximumNorth { get; set; }

        /// <summary>
        /// Page number and co-ordinate data for all rows in the map book
        /// in order of northing (beginning with the southernmost and ending at the top of the map).
        /// </summary>
        /// <value>Array of page rows.</value>
        [XmlArray]
        public Collection<MapBookPageRow> PageRows 
        { 
            get { return this.pageRows; }
        }
        
        /// <summary>
        /// Adjustments for pages that are numbered out of sequence or appear as inserts.
        /// </summary>
        /// <value>Array of page adjustments.</value>
        [XmlArray]
        public Collection<PageAdjustment> PageAdjustments 
        { 
            get { return this.pageAdjustments; }
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2016 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Text;

    /// <summary>
    /// Section of cells (using the same scale) read from data file provided by A to Z
    /// (there is a separate data file for each scale).
    /// The data provided by A to Z was originally pre-processed (by script 'convert')
    /// to remove header line and separate cell reference and page number.
    /// The constructor now tests for csv and if found does all the processing.
    /// </summary>
    public class PageSection
    {
        public const string NotFound = "";
        public const string CommaSeparatedFileExtension = "csv";
        public const char DefaultFieldSeparator = '|';
        public const char CommaFieldSeparator = ',';
        public const int DefaultNumberOfFields = 6;
        public const int CommaSeparatedNumberOfFields = 5;
        public const int MaxHeaderLines = 4;

        /// <summary>
        /// Lock to make access to the list thread safe.
        /// </summary>
        private readonly object cellsInSectionLock;

        /// <summary>
        /// Set to comma when processing a csv file.
        /// </summary>
        private char fieldSeparator;

        /// <summary>
        /// Set to 5 when processing a csv file (cell reference and page are separated by space).
        /// </summary>
        private int expectedNumberOfFields;

        /// <summary>
        /// Number of lines ignored (because of incorrect format) counting from the top of the file.
        /// </summary>
        private int headerLineCount;

        /// <summary>
        /// List of cells available for search.
        /// </summary>
        private List<PageCell> cellsInSection;

        /// <summary>
        /// Create in order to load data. Move to CellsInSection if loading is successful.
        /// </summary>
        private List<PageCell> pendingCells;

        /// <summary>
        /// Initialises a new instance of the <see cref="PageSection"/> class.
        /// </summary>
        public PageSection()
        {
            this.cellsInSectionLock = new object();
            this.cellsInSection = new List<PageCell>();
            this.fieldSeparator = DefaultFieldSeparator;
            this.expectedNumberOfFields = DefaultNumberOfFields;
            this.DataFilePath = string.Empty;
            this.BookName = string.Empty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PageSection"/> class.
        /// Set the parameters for the data format according to the file extension.
        /// </summary>
        /// <param name="dataFilePath">Full path of file.</param>
        public PageSection(string dataFilePath) 
            : this()
        {
            this.DataFilePath = dataFilePath;
            if (this.DataFilePath.EndsWith(CommaSeparatedFileExtension, StringComparison.OrdinalIgnoreCase))
            {
                this.fieldSeparator = CommaFieldSeparator;
                this.expectedNumberOfFields = CommaSeparatedNumberOfFields;
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PageSection"/> class. 
        /// </summary>
        /// <param name="dataFilePath">Full path of file.</param>
        /// <param name="bookName">Book name (needed if there are sections in different books).</param>
        public PageSection(string dataFilePath, string bookName)
            : this(dataFilePath)
        {
            this.BookName = bookName;
        }
        
        /// <summary>
        /// Path and name of file. Set this before calling LoadData.
        /// </summary>
        /// <value>Full path of the data file.</value>
        public string DataFilePath { get; private set; }

        /// <summary>
        /// Name of book e.g. to cater for WK or EK.
        /// </summary>
        /// <value>Name of book.</value>
        public string BookName { get; private set; }

        /// <summary>
        /// Number of cells (lines of data).
        /// </summary>
        /// <value>Count of items in list.</value>
        public int CellCount
        {
            get { return this.cellsInSection.Count; }
        }

        /// <summary>
        /// Returns the first cell in the list that matches the OS grid reference provided.
        /// If none matches, the cell returned is empty.
        /// </summary>
        /// <param name="gridReference">Twelve digit OS grid reference.</param>
        /// <returns>First matching cell or empty cell.</returns>
        public PageCell CellMatchingOSReference(string gridReference)
        {
            int easting = Validation.EastingFromOSGridReference(gridReference);
            int northing = Validation.NorthingFromOSGridReference(gridReference);
            PageCell cell = null;
            lock (this.cellsInSectionLock)
            {
                cell = this.cellsInSection.Find(x =>
                        easting < x.East &&
                        easting >= x.West &&
                        northing < x.North &&
                        northing >= x.South);
                
// This does the same thing and is easier to debug but seems marginally slower
//                  foreach (AtoZCell x in this.cellsInSection)
//                {
//                    if (easting < x.East &&
//                        easting >= x.West &&
//                        northing < x.North &&
//                        northing >= x.South)
//                    {
//                        cell = x;
//                        break;
//                    }
//                }
            }
            
            return cell ?? new PageCell();
        }

        /// <summary>
        /// Adapted from Homestations. This provides for refresh of data (although that
        /// is not needed if this is called once at start up).
        /// </summary>
        public void LoadData()
        {
            this.CheckFilePath();
            this.pendingCells = new List<PageCell>();
            this.ReadDataFromFile();
            this.CheckPendingData();
            lock (this.cellsInSectionLock)
            {
                this.cellsInSection = this.pendingCells;
            }
        }

        /// <summary>
        /// Ignore blank lines.
        /// </summary>
        /// <param name="line">Line of text or data.</param>
        /// <returns>True if the line is not empty.</returns>
        private static bool LineIsData(string line)
        {
            return line.Length > 0;
        }

        /// <summary>
        /// Assume that the cell reference is the first 2 characters.
        /// </summary>
        /// <param name="cellAndPage">Two-character cell reference, space and page number.</param>
        /// <returns>Cell reference.</returns>
        private static string CellReference(string cellAndPage)
        {
            return cellAndPage.Substring(0, 2);
        }

        /// <summary>
        /// Assume that the page number begins at index 3.
        /// </summary>
        /// <param name="cellAndPage">Two-character cell reference, space and page number.</param>
        /// <returns>Page number.</returns>
        private static string PageNumber(string cellAndPage)
        {
            return cellAndPage.Substring(3);
        }

        private void CheckFilePath()
        {
            if (this.DataFilePath.Length == 0)
            {
                throw new MapLibException("Section file path is empty");
            }
        }

        private void CheckPendingData()
        {
            if (this.pendingCells.Count == 0)
            {
                throw new MapLibException("Section contains no data");
            }
        }

        /// <summary>
        /// Open the file and read all lines that contain data.
        /// </summary>
        private void ReadDataFromFile()
        {
            string currentLine;
            try
            {
                using (var fileDataStream = new StreamReader(this.DataFilePath))
                {
                    while ((currentLine = fileDataStream.ReadLine()) != null)
                    {
                        if (LineIsData(currentLine))
                        {
                            this.ReadColumns(currentLine);
                        }
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                throw new MapLibException("Section file not found", ex);
            }
        }

        /// <summary>
        /// Parse a line of data and if all is well add a new object to the list.
        /// Ignore header lines at the top of the file that are not correctly formatted
        /// (e.g. comment, column descriptors etc.). Throw an exception if there are
        /// too many such lines - there may be a problem with the data format.
        /// </summary>
        /// <param name="currentLine">Line of data.</param>
        private void ReadColumns(string currentLine)
        {
            try
            {
                if (this.expectedNumberOfFields == DefaultNumberOfFields)
                {
                    this.ReadBarSeparatedColumns(currentLine);
                }
                else
                {
                    this.ReadCommaSeparatedColumns(currentLine);
                }
            }
            catch (MapLibException)
            {
                this.headerLineCount++;
                if (this.pendingCells.Count > 0 || this.headerLineCount > MaxHeaderLines)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Parse a line of data and if all is well add a new object to the list.
        /// </summary>
        /// <param name="currentLine">Line of data that represents a HomeStation.</param>
        private void ReadBarSeparatedColumns(string currentLine)
        {
            string[] parts = currentLine.Split(this.fieldSeparator);
            if (parts.Length != this.expectedNumberOfFields)
            {
                throw new MapLibException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Expected format is 'cell|page|east|west|north|south'. This line is wrong:{0}",
                    currentLine));
            }

            this.AddRecordToList(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5]);
        }

        /// <summary>
        /// Parse a line of data and if all is well add a new object to the list.
        /// </summary>
        /// <param name="currentLine">Line of data that represents a HomeStation.</param>
        private void ReadCommaSeparatedColumns(string currentLine)
        {
            string[] parts = currentLine.Split(this.fieldSeparator);
            if (parts.Length != this.expectedNumberOfFields)
            {
                throw new MapLibException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Expected format is 'cell page,east,west,north,south'. This line is wrong:{0}",
                    currentLine));
            }

            this.AddRecordToList(CellReference(parts[0]), PageNumber(parts[0]), parts[1], parts[2], parts[3], parts[4]);
        }

        /// <summary>
        /// Parse and validate the fields and add the new cell to the list.
        /// </summary>
        /// <param name="cell">Cell reference (digit and letter).</param>
        /// <param name="page">Page number.</param>
        /// <param name="east">Maximum easting.</param>
        /// <param name="west">Minimum easting.</param>
        /// <param name="north">Maximum northing.</param>
        /// <param name="south">Minimum northing.</param>
        private void AddRecordToList(string cell, string page, string east, string west, string north, string south)
        {
            var newCell = new PageCell(cell, page, east, west, north, south);
            this.pendingCells.Add(newCell);
        }
    }
}

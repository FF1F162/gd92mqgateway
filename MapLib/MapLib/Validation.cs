﻿// Copyright (c) Thales UK Limited 2016 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Text;

    /// <summary>
    /// Static methods to validate OS grid reference, cell reference etc.
    /// and to convert between int and string representation of easting and northing.
    /// </summary>
    public sealed class Validation
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Validation"/> class from being created.
        /// </summary>
        private Validation()
        {
        }

        /// <summary>
        /// Convert digits to a number. Return zero if the conversion fails.
        /// </summary>
        /// <param name="digits">String of digits.</param>
        /// <returns>Number or zero.</returns>
        public static int NumberFromDigits(string digits)
        {
            int number;
            if (!int.TryParse(digits, out number))
            {
                number = 0;
            }

            return number;
        }

        /// <summary>
        /// Convert six digit OS grid easting or northing to a number.
        /// </summary>
        /// <param name="sixDigits">Six-digit string.</param>
        /// <returns>Co-ordinate as a number.</returns>
        public static int CoordinateFromSixDigits(string sixDigits)
        {
            if (sixDigits.Length != 6)
            {
                throw new MapLibException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Expected six-digit number. Instead found:{0}",
                    sixDigits));
            }

            return NumberFromDigits(sixDigits);
        }

        /// <summary>
        /// Extracts six digit OS grid easting and converts to a number.
        /// </summary>
        /// <param name="gridReference">Twelve-digit string.</param>
        /// <returns>Easting as a number.</returns>
        public static int EastingFromOSGridReference(string gridReference)
        {
            string twelveDigits = (gridReference != null && gridReference.Length >= 12) ? gridReference : "000000000000";
            return CoordinateFromSixDigits(twelveDigits.Substring(0, 6));
        }

        /// <summary>
        /// Extracts six digit OS grid northing and converts to a number.
        /// </summary>
        /// <param name="gridReference">Twelve-digit string.</param>
        /// <returns>Northing as a number.</returns>
        public static int NorthingFromOSGridReference(string gridReference)
        {
            string twelveDigits = (gridReference != null && gridReference.Length >= 12) ? gridReference : "000000000000";
            return CoordinateFromSixDigits(twelveDigits.Substring(6, 6));
        }
    }
}

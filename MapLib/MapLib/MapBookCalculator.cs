﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    
    /// <summary>
    /// Represents a MapBook in which pages systematically cover an area with adjoining pages.
    /// Each page is divided into squares (cells) of 0.5 km that coincide with the Ordnance Survey grid. 
    /// The majority of pages are in rows from left (West) to right (East) across the area,
    /// each covering squares with the same northing. Pages in a row cover cells with consecutive
    /// eastings. Within some rows there are breaks in the sequence where a page would
    /// otherwise show mostly sea. Some pages have inserts that show small areas of land that would
    /// otherwise occupy a different page.
    /// </summary>
    public class MapBookCalculator
    {
        /// <summary>
        /// Page (and row) height in units of 100 metres.
        /// </summary>
        public const int PageHeight = 40;
        
        /// <summary>
        /// Page (and row) height in units of 100 metres.
        /// </summary>
        public const int PageWidth = 30;
        
        /// <summary>
        /// Length of the side of a map book cell in units of 100 metres.
        /// </summary>
        public const int CellSize = 5;
        
        /// <summary>
        /// Page number and co-ordinate data for all rows in the map book.
        /// </summary>
        private MapBookData book;
        
        /// <summary>
        /// Row of pages that holds the reference - determined from northing.
        /// </summary>
        private int rowIndex;
        
        /// <summary>
        /// Initially the calculation sets this to a notional page number that 
        /// assumes the map is divided into whole pages. After adjustment for missing pages
        /// and inserts it shows the page number in the map book.
        /// </summary>
        private int pageNumber;
        
        /// <summary>
        /// Easting of left edge of page. This may be adjusted (moved West) for inserts
        /// to give the cell reference letters required.
        /// </summary>
        private int baseE;
        
        /// <summary>
        /// Northing of bottom edge of page. This may be adjusted (moved South, partly overlapping
        /// the row below) so the page shows more land than sea.
        /// </summary>
        private int baseN;
        
        /// <summary>
        /// Number of cell containing the grid reference (x-axis)
        /// From 1 to 6 across a normal page but more if baseE has been adjusted. For reference
        /// the number is converted to a letter.
        /// </summary>
        private int cellEast;
        
        /// <summary>
        /// Number of cell containing the grid reference (y-axis)
        /// From 1 to 8 up the page.
        /// </summary>
        private int cellNorth;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="MapBookCalculator"></see> class.
        /// </summary>
        /// <param name="mapBookData">Data for the map book.</param>
        public MapBookCalculator(MapBookData mapBookData)
        {
            this.book = mapBookData;
        }
        
        /// <summary>
        /// This works out the page and cell reference in the Philip's Map Book corresponding to
        /// the Ordnance Survey grid reference supplied. Do a simple calculation
        /// to find the row of pages from the northing, then look up data about the row from
        /// the array supplied in the constructor. A simple calculation gives the page that would
        /// normally be expected to hold the OS grid reference provided. 
        /// Look for an adjustment record in case the page is an insert on another page  
        /// or there are gaps in the sequence. The base easting or northing may need adjusting.
        /// Finally determine the reference for the cell within the page.
        /// </summary>
        /// <param name="east">Four-figure easting.</param>
        /// <param name="north">Four-figure northing.</param>
        /// <returns>Page and cell reference.</returns>
        public string MapBookPageAndCellForGridReference(int east, int north)
        {
            this.InitializePageAndCell();
            if (north >= this.book.MinimumNorth && north < this.book.MaximumNorth)
            {
                this.FindBaseNorthing(north);
                this.FindPageNumberAndBaseEasting(east);
                this.MakeAdjustment();
                this.FindCellCoordinates(east, north);
            }
            
            return this.CellReference();
        }

        /// <summary>
        /// Most pages are 3 km wide and 4 km high with squares of 0.5 km.
        /// Squares on the x axis (easting) are labelled A to F.
        /// Columns for inserts are labelled G upwards.
        /// In most cases, where map books overlap, the pages coincide.
        /// Exception is EK1 TODO.
        /// </summary>
        /// <param name="cellNumber">Number of the cell.</param>
        /// <returns>Letter corresponding to the cell number.</returns>
        private static string CellLetterFromNumber(int cellNumber)
        {
            char letter;
            switch (cellNumber)
            {
                case 1: letter = 'A';
                    break;
                case 2: letter = 'B';
                    break;
                case 3: letter = 'C';
                    break;
                case 4: letter = 'D';
                    break;
                case 5: letter = 'E';
                    break;
                case 6: letter = 'F';
                    break;
                case 7: letter = 'G';
                    break;
                case 8: letter = 'H';
                    break;
                case 9: letter = 'I';
                    break;
                case 10: letter = 'J';
                    break;
                default: letter = 'X';
                    break;
            }

            return letter.ToString();
        }

        private static string CellLetterFromNumber2(int cellNumber)
        {
            return (cellNumber >= 1 && cellNumber <= 10) ? Convert.ToChar(64 + cellNumber).ToString() : "X";
        }


        /// <summary>
        /// Clear fields used for output in case the reference is out of bounds.
        /// </summary>
        private void InitializePageAndCell()
        {
            this.pageNumber = 0;
            this.cellEast = 0;
            this.cellNorth = 0;
        }

        /// <summary>
        /// Find the index of the row of pages containing the northing.
        /// </summary>
        /// <param name="north">Northing of the bottom edge of the pages in the row.</param>
        private void FindBaseNorthing(int north)
        {
            this.rowIndex = (north - this.book.MinimumNorth) / PageHeight;
            this.baseN = this.book.MinimumNorth + (this.rowIndex * PageHeight);
        }

        /// <summary>
        /// Set page number 0 if the grid easting is outside the range encompassed by the pages
        /// Assume all pages are 3 km wide (6 cells) when calculating page offset.
        /// </summary>
        /// <param name="east">Four-figure easting.</param>
        private void FindPageNumberAndBaseEasting(int east)
        {
            if (this.book.PageRows[this.rowIndex].FirstPage == 0 ||
                east < this.book.PageRows[this.rowIndex].FirstEast ||
                east >= this.book.PageRows[this.rowIndex].LastEast)
            {
                this.pageNumber = 0;
            }
            else
            {
                int offset = (east - this.book.PageRows[this.rowIndex].FirstEast) / PageWidth;
                this.pageNumber = this.book.PageRows[this.rowIndex].FirstPage + offset;
                this.baseE = this.book.PageRows[this.rowIndex].FirstEast + (offset * 30);
            }
        }
        
        /// <summary>
        /// Substitute the page number in the map book for the page number calculated initially.
        /// (Page number is 0 for any reference where there is a gap between pages e.g. West Kent page 4 to 5)
        /// Base North needs changing for some pages that show the North coast so the page
        /// shows more land than sea.
        /// Base East needs changing for inserts to provide a unique cell reference letter.
        /// </summary>
        private void MakeAdjustment()
        {
            foreach (PageAdjustment adjustment in this.book.PageAdjustments)
            {
                if (adjustment.BaseNorth == this.baseN &&
                    adjustment.PageNumber == this.pageNumber)
                {
                    this.baseE = adjustment.NewBaseEast;
                    this.baseN = adjustment.NewBaseNorth;
                    this.pageNumber = adjustment.NewPageNumber;
                    break;
                }
            }
        }

        /// <summary>
        /// Use the offset from base east and north (bottom left corner of the page) divided by cell width.
        /// </summary>
        /// <param name="east">Four-figure easting.</param>
        /// <param name="north">Four-figure northing.</param>
        private void FindCellCoordinates(int east, int north)
        {
            this.cellNorth = 1 + ((north - this.baseN) / CellSize);
            this.cellEast = 1 + ((east - this.baseE) / CellSize);
        }

        /// <summary>
        /// Convert the cell east co-ordinate to a letter.
        /// </summary>
        /// <returns>Page number, cell east letter, cell north.</returns>
        private string CellReference()
        {
            return (this.pageNumber == 0) ? string.Empty :
                string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0}{1}{2}",
                    this.pageNumber,
                    CellLetterFromNumber(this.cellEast),
                    this.cellNorth);
        }
    }
}

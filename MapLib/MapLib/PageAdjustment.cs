﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Xml.Serialization;
    
    /// <summary>
    /// The initial output of the calculator needs adjusting to allow for inserts
    /// and for gaps where pages would show only sea.
    /// This class shows the adjustment needed for a single page
    /// serialized to XML as part of the map book data.
    /// </summary>
    [XmlRootAttribute]
    public class PageAdjustment
    {
        public PageAdjustment()
        {
        }
        
        /// <summary>
        /// Identifies the row (independently of where the array of rows is based).
        /// </summary>
        /// <value>Four-figure northing.</value>
        [XmlAttribute]
        public int BaseNorth { get; set; }
        
        /// <summary>
        /// Together with the row, this identifies the page.
        /// </summary>
        /// <value>Page number calculated initially.</value>
        [XmlAttribute]
        public int PageNumber { get; set; }
        
        /// <summary>
        /// There is a new number when the map book does not show areas of sea
        /// (so whole pages have a lower number than expected)
        /// or where the area is an insert on another page.
        /// </summary>
        /// <value>Page number in map book.</value>
        [XmlAttribute]
        public int NewPageNumber { get; set; }
        
        /// <summary>
        /// Some pages on the North coast are shifted down so they overlap with
        /// the page to the South. This affects the north part of the cell reference.
        /// </summary>
        /// <value>Four-figure northing.</value>
        [XmlAttribute]
        public int NewBaseNorth { get; set; }
        
        /// <summary>
        /// Inserts must have a cell reference letter (east part) that is different
        /// from cells on the main page. It is as though the x-axis starts before
        /// the left edge of the insert.
        /// </summary>
        /// <value>Four-figure easting.</value>
        [XmlAttribute]
        public int NewBaseEast { get; set; }
    }
}

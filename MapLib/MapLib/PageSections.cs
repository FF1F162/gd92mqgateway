﻿// Copyright (c) Thales UK Limited 2016 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Collection of PageSections to search for a cell corresponding to a given OS Grid Reference.
    /// </summary>
    public class PageSections : IMapBook
    {
        /// <summary>
        /// Lock to make access to the list thread safe.
        /// This probably makes the lock in PageSection redundant. Keeping it is probably OK
        /// since all access to the PageSection instances is through this class.
        /// </summary>
        private readonly object sectionsLock;

        /// <summary>
        /// Sections to search.
        /// </summary>
        private List<PageSection> sections;

        /// <summary>
        /// Initialises a new instance of the <see cref="PageSections"/> class.
        /// </summary>
        public PageSections()
        {
            this.sectionsLock = new object();
            this.sections = new List<PageSection>();
        }

        /// <summary>
        /// Read data and add to the list of sections.
        /// </summary>
        /// <param name="dataFilePath">Full path of data file.</param>
        public void LoadAndAddSection(string dataFilePath)
        {
            this.LoadAndAddSection(new PageSection(dataFilePath));
        }

        /// <summary>
        /// Read data and add to the list of sections. Book name is needed if sections are in
        /// separate books (each with pages starting at 1) e.g. WK and EK.
        /// </summary>
        /// <param name="dataFilePath">Full path of data file.</param>
        /// <param name="bookName">Name of book (needed only if there is more than one).</param>
        public void LoadAndAddSection(string dataFilePath, string bookName)
        {
            this.LoadAndAddSection(new PageSection(dataFilePath, bookName));
        }

        /// <summary>
        /// Return reference(s) to page and cell corresponding to the Ordnance Survey grid reference supplied.
        /// </summary>
        /// <param name="gridReference">Twelve digit OS Grid Reference.</param>
        /// <returns>First matching cell reference (if any) in each section or empty string.</returns>
        public string MapBookForGridReference(string gridReference)
        {
            string cellReference = string.Empty;
            lock (this.sectionsLock)
            {
                foreach (PageSection section in this.sections)
                {
                    PageCell cell = section.CellMatchingOSReference(gridReference);
                    if (cell.PageNumber > 0)
                    {
                        cellReference = string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "{0}{1}{2}",
                            cellReference,
                            cellReference.Length > 0 ? Separator(section.BookName) : string.Empty,
                            PageAndCell(section.BookName, cell));
                    }
                }
            }

            return cellReference.Length > 0 ? cellReference : PageSection.NotFound;
        }

        private static string Separator(string bookName)
        {
            return bookName.Length > 0 ? " " : ", ";
        }

        /// <summary>
        /// Return the index entry for a cell i.e. (book,) page number, cell reference.
        /// </summary>
        /// <param name="bookName">Name of book (if any).</param>
        /// <param name="cell">Reference to cell object.</param>
        /// <returns>Index entry showing page and cell.</returns>
        private static string PageAndCell(string bookName, PageCell cell)
        {
            return bookName.Length > 0 ? 
                PageAndCellForSeparateBooks(bookName, cell) : 
                PageAndCellForSingleBook(cell);
        }

        /// <summary>
        /// Provide appropriate format where the book is provided (e.g. WK or EK).
        /// </summary>
        /// <param name="bookName">Name representing the book.</param>
        /// <param name="cell">Reference to cell object.</param>
        /// <returns>Index entry showing book, page and cell.</returns>
        private static string PageAndCellForSeparateBooks(string bookName, PageCell cell)
        {
            return string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "{0}{1} {2}",
                            bookName,
                            cell.PageNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                            cell.CellReference);
        }

        /// <summary>
        /// Provide format for when all sections are in a single book.
        /// </summary>
        /// <param name="cell">Reference to cell object.</param>
        /// <returns>Index entry showing page and cell.</returns>
        private static string PageAndCellForSingleBook(PageCell cell)
        {
            return string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "{0} {1}",
                            cell.PageNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                            cell.CellReference);
        }

        /// <summary>
        /// Read data for the section then add it to the list.
        /// </summary>
        /// <param name="section">Newly created section.</param>
        private void LoadAndAddSection(PageSection section)
        {
            section.LoadData();
            this.AddSection(section);
        }

        /// <summary>
        /// Add a section to the list of sections.
        /// </summary>
        /// <param name="section">Newly created section.</param>
        private void AddSection(PageSection section)
        {
            System.Diagnostics.Debug.Assert(section.CellCount > 0, "No point adding an empty section.");
            lock (this.sectionsLock)
            {
                this.sections.Add(section);
            }
        }
    }
}

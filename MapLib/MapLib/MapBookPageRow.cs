﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Xml.Serialization;
    
    /// <summary>
    /// Represents a row of pages in the map book.
    /// Assumes pages for the same northings are numbered consecutively.
    /// </summary>
    [XmlRootAttribute]
    public class MapBookPageRow
    {
        public MapBookPageRow()
        {
        }
        
        public MapBookPageRow(int minPage, int minEast, int maxEast, int maxPage)
        {
            this.FirstPage = minPage;
            this.FirstEast = minEast;
            this.LastEast = maxEast;
            this.LastPage = maxPage;
        }
        
        /// <summary>
        /// Number of the first page in the row.
        /// </summary>
        /// <value>Page number.</value>
        [XmlAttribute]
        public int FirstPage { get; set; }
        
        /// <summary>
        /// Easting  at the left boundry of the first page in the row.
        /// </summary>
        /// <value>Minimum easting.</value>
        [XmlAttribute]
        public int FirstEast { get; set; }
        
        /// <summary>
        /// Easting  at the right boundry of the last page in the row.
        /// </summary>
        /// <value>Maximum easting.</value>
        [XmlAttribute]
        public int LastEast { get; set; }
        
        /// <summary>
        /// Number of the last page in the row.
        /// </summary>
        /// <value>Page number.</value>
        [XmlAttribute]
        public int LastPage { get; set; } 
    }
}

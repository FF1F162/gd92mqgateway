﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Exception raised in MapLib code.
    /// </summary>
    [Serializable]
    public class MapLibException : Exception, ISerializable
    {
        public MapLibException()
        {
        }

        public MapLibException(string message)
            : base(message)
        {
        }

        public MapLibException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected MapLibException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

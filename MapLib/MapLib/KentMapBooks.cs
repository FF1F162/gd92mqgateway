﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Xml.Serialization;
    
    /// <summary>
    /// Holds data for the two Kent Mapbooks.
    /// Grid references are to a precision of 100 metres.
    /// </summary>
    public class KentMapBooks : IMapBook
    {
        /// <summary>
        /// Default path is the test input folder.
        /// </summary>
        public const string DefaultPathToMapBookFolder = @"C:\git\kfrs\StormGateway\MapLib\TestInput";

        /// <summary>
        /// Key to the path of the West Kent data in App.Config.
        /// Doubles as basis for default file name.
        /// </summary>
        public const string AppConfigKeyWestKentMapBookPath = "WestKentMapBook";

        /// <summary>
        /// Key to the path of the East Kent data in App.Config.
        /// Doubles as basis for default file name.
        /// </summary>
        public const string AppConfigKeyEastKentMapBookPath = "EastKentMapBook";
        
        /// <summary>
        /// Represents the West Kent map book.
        /// </summary>
        private MapBookCalculator westKent;
        
        /// <summary>
        /// Represents the East Kent map book.
        /// </summary>
        private MapBookCalculator eastKent;
        
        public KentMapBooks()
        {
            this.LoadWestKentMapBook();
            this.LoadEastKentMapBook();
        }

        /// <summary>
        /// Return reference to page and cell in the Philip's Map Books for West and East Kent
        /// corresponding to the Ordnance Survey grid reference supplied. 
        /// Substitute twelve zeroes if the reference is not valid (and return an empty string).
        /// </summary>
        /// <param name="gridReference">Twelve digit OS Grid Reference.</param>
        /// <returns>West and/or East Kent page number and cell or empty string.</returns>
        public string MapBookForGridReference(string gridReference)
        {
            string twelveDigits = (gridReference != null && gridReference.Length >= 12) ? gridReference : "000000000000"; 
            int east;
            if (!int.TryParse(twelveDigits.Substring(0, 4), out east))
            {
                east = 0;
            }

            int north;
            if (!int.TryParse(twelveDigits.Substring(6, 4), out north))
            {
                north = 0;
            }

            return this.MapBookForGridReference(east, north);
        }

        private static string FolderFromAppConfigOrDefault(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? Path.Combine(DefaultPathToMapBookFolder, key) + ".txt";
        }

        /// <summary>
        /// Read XML string from file and deserialize to object of the type specified.
        /// </summary>
        /// <typeparam name="T">Type that can be de-serialized from XML.</typeparam>
        /// <param name="fileName">Full path of the file containing XML.</param>
        /// <returns>Object constructed from XML.</returns>
        private static T ReadFromFile<T>(string fileName)
        {
            var d = new XmlSerializer(typeof(T));
            using (var f = new FileStream(fileName, FileMode.Open))
            {
                return (T)d.Deserialize(f);
            }
        }

        /// <summary>
        /// Return reference to page and cell in the Philip's Map Books for West and East Kent
        /// corresponding to the Ordnance Survey grid reference supplied.
        /// </summary>
        /// <param name="east">Four-figure easting.</param>
        /// <param name="north">Four-figure northing.</param>
        /// <returns>West and/or East Kent page number and cell.</returns>
        private string MapBookForGridReference(int east, int north)
        {
            string refWK = string.Empty;
            string refEK = string.Empty;
            string pageAndCell = this.westKent.MapBookPageAndCellForGridReference(east, north);
            if (pageAndCell.Length > 0)
            {
                refWK = "WK" + pageAndCell;
            }

            pageAndCell = this.eastKent.MapBookPageAndCellForGridReference(east, north);
            if (pageAndCell.Length > 0)
            {
                refEK = "EK" + pageAndCell;
            }

            return string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0}{1}{2}",
                refWK,
                refWK.Length > 0 && refEK.Length > 0 ? " " : string.Empty,
                refEK);
        }

        /// <summary>
        /// Load data for pages in the West Kent map book. 
        /// </summary>
        private void LoadWestKentMapBook()
        {
            string path = FolderFromAppConfigOrDefault(AppConfigKeyWestKentMapBookPath);
            var westKentData = ReadFromFile<MapBookData>(path);
            this.westKent = new MapBookCalculator(westKentData);
        }
        
        /// <summary>
        /// Load data for pages in the East Kent map book. 
        /// </summary>
        private void LoadEastKentMapBook()
        {
            string path = FolderFromAppConfigOrDefault(AppConfigKeyEastKentMapBookPath);
            var eastKentData = ReadFromFile<MapBookData>(path);
            this.eastKent = new MapBookCalculator(eastKentData);
        }
    }
}

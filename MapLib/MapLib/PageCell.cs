﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Represents a cell in the map book.
    /// </summary>
    [XmlRootAttribute]
    public class PageCell
    {
        public const string UnknownCellReference = "0?";

        /// <summary>
        /// Initialises a new instance of the <see cref="PageCell"></see> class.
        /// </summary>
        public PageCell()
        {
            this.CellReference = UnknownCellReference;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PageCell"></see> class.
        /// </summary>
        /// <param name="cell">Cell reference.</param>
        /// <param name="page">Page number.</param>
        /// <param name="east">Six-figure Easting at right.</param>
        /// <param name="west">Six-figure Easting at left.</param>
        /// <param name="north">Six-figure Northing at top.</param>
        /// <param name="south">Six-figure Northing at bottom.</param>
        public PageCell(string cell, string page, string east, string west, string north, string south)
        {
            this.PageNumber = Validation.NumberFromDigits(page);
            this.CellReference = cell;
            this.East = Validation.CoordinateFromSixDigits(east);
            this.West = Validation.CoordinateFromSixDigits(west);
            this.South = Validation.CoordinateFromSixDigits(south);
            this.North = Validation.CoordinateFromSixDigits(north);
        }

        /// <summary>
        /// Number of the page where the cell appears.
        /// </summary>
        /// <value>Page number.</value>
        [XmlAttribute]
        public int PageNumber { get; set; }

        /// <summary>
        /// Row (1-9) and column (A-N) forming cell reference.
        /// </summary>
        /// <value>Cell reference.</value>
        [XmlAttribute]
        public string CellReference { get; set; }

        /// <summary>
        /// Easting  at the left boundry of the cell.
        /// </summary>
        /// <value>Minimum easting.</value>
        [XmlAttribute]
        public int West { get; set; }

        /// <summary>
        /// Easting  at the right boundry of the cell.
        /// </summary>
        /// <value>Maximum easting.</value>
        [XmlAttribute]
        public int East { get; set; }

        /// <summary>
        /// Northing at the lower boundry of the cell.
        /// </summary>
        /// <value>Minimum northing.</value>
        [XmlAttribute]
        public int South { get; set; }

        /// <summary>
        /// Northing at the top boundry of the cell.
        /// </summary>
        /// <value>Maximum northing.</value>
        [XmlAttribute]
        public int North { get; set; }
    }
}

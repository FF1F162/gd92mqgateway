﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.MapLib
{
    using System;

    /// <summary>
    /// Provide page references in the West and or East Kent map books from 12-figure OS Grid reference.
    /// </summary>
    public interface IMapBook
    {
        string MapBookForGridReference(string gridReference);
    }
}

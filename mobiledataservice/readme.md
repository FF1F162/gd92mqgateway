# MobileDataService #

## Introduction ##
This repo contains source code for the Mobile Data Service. The Mobile Data Service translates GD92 messages from the GD92 Service and exchanges these with mobile data clients (MDTs).

The format for readme.md files is based on Python markdown, more examples are available in the [bitbucket markdown tutorial](https://bitbucket.org/tutorials/markdowndemo/overview#markdown-header-links)
﻿using MDTGateway.Helpers;
using MDTGateway.Mobilise;
using System.Windows;

namespace MDTGateway
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            GatewayViewModel context = new GatewayViewModel();
            this.DataContext = context;

            Gateway.RecieveMessageFromQueue(GatewayConfig.StatusQueueName, GatewayConfig.ExchangeName);

            InitializeComponent();
        }
    }
}

﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using KFRS.MDT.MobiliseMessages;
using KFRS.MDT.MobiliseMessages.Enums;
using MDTGateway.Helpers;
using System;
using System.Windows.Input;

namespace MDTGateway.Mobilise
{
    public class GatewayViewModel : ViewModelBase
    {
        #region Fields
        private string _displayMessage;
        private Message _currentMessage;

        private ICommand _sendMessageCommand;

        private string _queueName;
        private string _mobiliseExchange;
        #endregion

        #region Constructors
        public GatewayViewModel()
        {
            _queueName = GatewayConfig.QueueName;
            _mobiliseExchange = GatewayConfig.ExchangeName;
        }
        #endregion

        #region Properties and Commands
        public string DisplayMessage
        {
            get { return _displayMessage; }
            set
            {
                if (value != _displayMessage)
                {
                    _displayMessage = value;
                    RaisePropertyChanged("DisplayMessage");
                }
            }
        }

        public Message CurrentMessage
        {
            get { return _currentMessage; }
            set
            {
                if (value != _currentMessage)
                {
                    _currentMessage = value;
                    RaisePropertyChanged("CurrentMessage");
                }
            }
        }

        public ICommand SendMessageCommand
        {
            get
            {
                if (_sendMessageCommand == null)
                {
                    //TODO: Change this to Async.
                    _sendMessageCommand = new RelayCommand(() => { Mobilise_Click(); });
                }
                return _sendMessageCommand;
            }
        }
        #endregion

        #region Methods
        private void Mobilise_Click()
        {
            // TODO: Error handling.
            Gateway.SendMessageToQueue(_queueName, _mobiliseExchange, CreateTestMobMessage());

            DisplayMessage = "Message sent " + DateTime.Now.ToString();
        }

        /// <summary>
        /// Test method which creates a test mobilistaion message for testing.
        /// </summary>
        /// <returns>Message with Mobilisation object as it's content.</returns>
        private Message CreateTestMobMessage()
        {
            Message envelope = new Message(Guid.NewGuid());
            Mobilisation content = new Mobilisation();

            content.Address = "73 Apple lane, Maidstone ME4 9UY";
            content.IncidentDate = DateTime.Now;
            content.IncidentNumber = "T0023";
            content.IncidentType = "FIRE";

            envelope.SequenceNumber = 101;
            envelope.Destination = "FJK11R1";
            envelope.Originator = "Control";
            envelope.Priority = Priority.Medium;
            envelope.MessageType = MessageType.Mobilisation;
            envelope.Content = content;

            return envelope;
        }
        #endregion
    }
}

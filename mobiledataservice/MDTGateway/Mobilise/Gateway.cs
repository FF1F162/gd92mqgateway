﻿using KFRS.MDT.MobiliseMessages;
using KFRS.MDT.MobiliseMessages.Helpers;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace MDTGateway.Mobilise
{
    public static class Gateway
    {
        /// <summary>
        /// Sends a message to a RabbitMQ queue.
        /// </summary>
        /// <param name="queueName">Queue name in RabbitMQ</param>
        /// <param name="mobiliseExchange">Exchange name in RabbitMQ</param>
        /// <param name="msg">Message contents to send.</param>
        public static void SendMessageToQueue(string queueName, string exchange, Message msg)
        {
            if (!string.IsNullOrEmpty(queueName) || !string.IsNullOrEmpty(exchange))
            {
                var factory = new ConnectionFactory() { HostName = "devsgw1", UserName = "dev", Password = "berry" };
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);

                        var body = Serializers.Serialize(msg);

                        channel.BasicPublish(exchange: exchange, routingKey: queueName, basicProperties: null, body: body);
                    }
                }
            }
        }

        /// <summary>
        /// Set up the receiver.
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="exchange"></param>
        public static void RecieveMessageFromQueue(string queueName, string exchange)
        {
            if (!string.IsNullOrEmpty(queueName) || !string.IsNullOrEmpty(exchange))
            {
                var factory = new ConnectionFactory() { HostName = "devsgw1", UserName = "dev", Password = "berry" };
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
                        channel.QueueBind(queue: queueName, exchange: exchange, routingKey: queueName);

                        var consumer = new EventingBasicConsumer(channel);
                        consumer.Received += (model, ea) =>
                        {
                            var body = ea.Body;
                            var message = Serializers.DeSerialize(body);
                            // NOTE: The message doesn't go anywhere.
                        };

                        channel.BasicConsume(queue: queueName, noAck: true, consumer: consumer);
                    }
                }
            }
        }
    }
}

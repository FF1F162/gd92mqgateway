﻿using System.Configuration;

namespace MDTGateway.Helpers
{
    public static class GatewayConfig
    {
        public static string QueueName
        {
            get
            {
                string queueName = string.Empty;
                if (ConfigurationManager.AppSettings["mobiliseQueue"] != null)
                {
                    queueName = ConfigurationManager.AppSettings["mobiliseQueue"].ToString();
                }
                return queueName;
            }
        }

        public static string StatusQueueName
        {
            get
            {
                string queueName = string.Empty;
                if (ConfigurationManager.AppSettings["statusQueue"] != null)
                {
                    queueName = ConfigurationManager.AppSettings["statusQueue"].ToString();
                }
                return queueName;
            }
        }

        public static string ExchangeName
        {
            get
            {
                string exchangeName = string.Empty;
                if (ConfigurationManager.AppSettings["mobiliseExchange"] != null)
                {
                    exchangeName = ConfigurationManager.AppSettings["mobiliseExchange"].ToString();
                }
                return exchangeName;
            }
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using Microsoft.Win32;
    using Thales.KentFire.EventLib;
    using System.IO;




    internal sealed class Router : IRouter
    {
        private const int MinimumNodeNumberWhenAddingNodeAutomatically = 200;

        // private const string RegistryKeyMtaData = @"Software\Thales\Kent GD92\MtaData\";
        // private const string RegistryKeyNodeData = @"Software\Thales\Kent GD92\NodeData\";
        private const string LogName = "Router";
        
        private static readonly Router Self = new Router();
        private static readonly string RegistryKeyMtaData = UserAgent.Instance.RegistryKeyRoot + @"\MtaData\";
        private static readonly string RegistryKeyNodeData = UserAgent.Instance.RegistryKeyRoot + @"\NodeData\";
        private static readonly string RegistryKeyOldMtaRoot = UserAgent.Instance.RegistryKeyRoot + @"\";
        private readonly object routerLock;
        private readonly object countLock;
        private readonly Dictionary<string, Mta> mtaByName;
        private readonly Dictionary<string, Node> nodeByName;
        private readonly Dictionary<NodeAddress, Node> nodeByAddress; // excludes Secondary nodes
        private readonly Dictionary<string, Bearer> bearerByName;

        private readonly List<int> configuredBrigades = new List<int>();
        private string thisNodeName;
        
        /// <summary>
        /// Prevents a default instance of the <see cref="Router"/> class from being created.
        /// The Router maintains Node, MTA and Bearer data read from the Registry at activation
        /// and provides methods that return references to Node objects for routing purposes.
        /// The lock is intended to prevent more than one thread altering a Dictionary at the same time.
        /// </summary>
        private Router()
        {
            this.routerLock = new object();
            this.countLock = new object();
            this.mtaByName = new Dictionary<string, Mta>();
            this.nodeByName = new Dictionary<string, Node>();
            this.nodeByAddress = new Dictionary<NodeAddress, Node>();
            this.bearerByName = new Dictionary<string, Bearer>();
            this.configuredBrigades = new List<int>();
            TestLogger("Router ctor");
        }

        internal static Router Instance
        {
            get
            {
                return Self;
            }
        }

        /// <summary>
        /// Check the address refers to a node that is already configured 
        /// or can be used to create a new node.
        /// </summary>
        /// <param name="address">Address to validate.</param>
        /// <returns>True if valid.</returns>
        public bool IsValidAddress(PortAddress address)
        {
            bool isConfigured = false;
            lock (this.routerLock)
            {
                isConfigured = this.nodeByAddress.ContainsKey(new NodeAddress(address));
            }

            return isConfigured || this.AddressIsValidForNewNode(address);
        }

        /// <summary>
        /// Return the node represented by the nodeName key or a default (with address that is not valid).
        /// If the name is a valid node number try to retrieve a node with this address, or create it if configured to do so.
        /// </summary>
        /// <param name="nodeNameOrNumber">Name of node or node number.</param>
        /// <returns>Node object.</returns>
        public Node FindOrCreateNode(string nodeNameOrNumber)
        {
            TestLogger("FindOrCreateNode");
            Node node  = new Node(string.Empty, new PortAddress());
            lock (this.routerLock)
            {
                try
                {
                    int nodeNumber;
                    if (this.nodeByName.ContainsKey(nodeNameOrNumber))
                    {
                        FindOrCreateNode(" Found " + nodeNameOrNumber);
                        node = this.nodeByName[nodeNameOrNumber];
                    }
                    else
                    {
                        if (int.TryParse(nodeNameOrNumber, out nodeNumber) &&
                            !string.IsNullOrEmpty(UserAgent.Instance.GetDefaultRoute()))
                        {
                            PortAddress address = this.ConstructAddressFromNodeNumber(nodeNumber);
                            node = this.FindOrCreateDestinationNode(address);
                            FindOrCreateNode(" Got " + nodeNumber);
                        }
                    }
                }
                catch (ArgumentOutOfRangeException e)
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Failed to add destination node - {0}",
                        e.ToString()));
                }
            }

            return node;
        }

        /// <summary>
        /// Return the node represented by the nodeName key or a default (with address that is not valid).
        /// </summary>
        /// <param name="nodeName">Name of node.</param>
        /// <returns>Node object.</returns>
        public Node GetNodeByNameOrDefault(string nodeName)
        {
            Node node;
            lock (this.routerLock)
            {
                node = this.nodeByName.ContainsKey(nodeName) ? 
                    this.nodeByName[nodeName] : 
                    new Node(string.Empty, new PortAddress());
            }
            
            return node;
        }

        /// <summary>
        /// Return the node represented by the NodeAddress key or a default (with address that is not valid).
        /// </summary>
        /// <param name="address">GD92 address.</param>
        /// <returns>Node object.</returns>
        public Node GetNodeByAddressOrDefault(NodeAddress address)
        {
            Node node;
            lock (this.routerLock)
            {
                node = this.nodeByAddress.ContainsKey(address) ? 
                    this.nodeByAddress[address] : 
                    new Node(string.Empty, new PortAddress());
            }
            
            return node;
        }

        /// <summary>
        /// Instead of routing as normal, divert the frame to the Gateway (if configured).
        /// </summary>
        /// <param name="frame">Frame to be routed.</param>
        internal static void DivertToGatewayIfAny(Frame frame)
        {
            Node gatewayNode = UserAgent.Instance.GetGatewayNode();
            if (gatewayNode != null)
            {
                frame.Routes = gatewayNode.Routes;
            }
        }

        /// <summary>
        /// Activate a named MTA.
        /// </summary>
        /// <param name="mtaName">Name of MTA.</param>
        internal void ActivateMta(string mtaName)
        {
            lock (this.routerLock)
            {
                if (this.mtaByName.ContainsKey(mtaName))
                {
                    FindOrCreateNode("ActivateMta " + mtaName);
                    this.mtaByName[mtaName].Activate();
                }
            }
        }
        
        /// <summary>
        /// Activate all MTAs (if not already activated).
        /// </summary>
        internal void ActivateMtas()
        {
            lock (this.routerLock)
            {
                foreach (KeyValuePair<string, Mta> kvp in this.mtaByName)
                {
                    if (!kvp.Value.Activated)
                    {
                        TestLogger("ActivateMtas - " + kvp.Value.MtaName);
                        kvp.Value.Activate();
                    }
                }
            }
        }
        
        internal void DeactivateAndRemoveAllMtas()
        {
            // allow any count in progress to complete before removing bearers
            lock (this.countLock) 
            {
                this.bearerByName.Clear();
            }
            
            lock (this.routerLock)
            {
                foreach (KeyValuePair<string, Mta> kvp in this.mtaByName)
                {
                    kvp.Value.Deactivate();
                }
                
                this.mtaByName.Clear();
            }
        }
        
        internal void RemoveAllNodes()
        {
            lock (this.routerLock)
            {
                foreach (KeyValuePair<string, Node> kvp in this.nodeByName)
                {
                    kvp.Value.TxQueue.Clear();
                }
                
                this.nodeByName.Clear();
                this.nodeByAddress.Clear();
            }
        }

        internal int GetFrameCountForBearerQueues()
        {
            int total = 0;
            
            // use a separate lock to avoid interfering with message traffic
            // GetCount does not set a lock on the PriorityQueue so the count is indicative only
            lock (this.countLock)
            {
                foreach (KeyValuePair<string, Bearer> kvp in this.bearerByName)
                {
                    total += kvp.Value.SendQueue.GetCount();
                }
            }
            
            return total;
        }
        
        internal Bearer GetBearer(string bearerName)
        {
            Bearer bearer = null;
            TestLogger("GetBearer " + bearerName);
            // bearerByName has its own lock to avoid interfering with message traffic
            // should probably put it in a class of its own
            lock (this.countLock)
            {
                bearer = this.bearerByName[bearerName];
            }
            
            return bearer;
        }

        /// <summary>
        /// Set the names of the source and destination node and routes to the destination
        /// for the first frame and copy to the rest.
        /// </summary>
        /// <param name="frameArray">Array of frames comprising messsage.</param>
        internal void SetRoutes(Frame[] frameArray)
        {
            TestLogger("SetRoutes");
            int numberOfFrames = frameArray.GetLength(0);
            TestLogger(" numberOfFrames " + numberOfFrames.ToString());
            for (int index = 0; index < numberOfFrames; index++)
            {
                if (index == 0)
                {
                    TestLogger(" index 0");
                    this.SetRoutes(frameArray[index]);
                }
                else
                {
                    TestLogger(" index " + index.ToString());
                    frameArray[index].Routes = frameArray[0].Routes;
                    frameArray[index].SourceName = frameArray[0].SourceName;
                    frameArray[index].DestinationName = frameArray[0].DestinationName;
                }
            }
        }
        
        /// <summary>
        /// Set the names of the source and destination node and routes to the destination
        /// Use the dictionary to look up the node for the address
        /// If there is no match for the address and if a default route is set,
        /// create a new node (using the Registry to get the route).
        /// </summary>
        /// <param name="frame">Frame to be routed.</param>
        internal void SetRoutes(Frame frame)
        {
            lock (this.routerLock)
            {
                this.FindOrCreateDestinationNodeAndSetNameAndRoutes(frame);
                this.FindOrCreateSourceNodeAndSetSourceName(frame);
            }
        }
        
        internal void ReadMtas()
        {
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(RegistryKeyMtaData, false /* read only */);
            if (rk != null)
            {
                lock (this.routerLock)
                {
                    string[] names = rk.GetSubKeyNames();
                    foreach (string mtaName in names)
                    {
                        Mta mta = ReadMtaDetails(mtaName);
                        if (mta != null)
                        {
                            TestLogger("ReadMtas " + mtaName);

                            EventControl.Info(LogName, string.Format(
                                System.Globalization.CultureInfo.InvariantCulture,
                                "Adding MTA {0}", 
                                mtaName));
                            this.mtaByName.Add(mtaName, mta);
                        }
                    }
                }
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    RegistryKeyMtaData));
            }
            
            this.ReadOldMtas();
        }
        
        internal void ReadOldMtas()
        {
            TestLogger("ReadOldMtas");

            RegistryKey rk = Registry.LocalMachine.OpenSubKey(RegistryKeyOldMtaRoot, false /* read only */);
            if (rk != null)
            {
                lock (this.routerLock)
                {
                    string[] names = { "LAN", "LAN2", "MLAN", "TS", "GWAY", "GSM", "GPRS", "DHCP" };
                    foreach (string mtaName in names)
                    {
                        string subKey = RegistryKeyOldMtaRoot + mtaName;
                        TestLogger(" subKey " + subKey);
                        Mta mta = ReadMtaDetailsByFullKey(subKey, mtaName);
                        if (mta != null)
                        {
                            if (this.mtaByName.ContainsKey(mtaName))
                            {
                                EventControl.Warn(LogName, string.Format(
                                    System.Globalization.CultureInfo.InvariantCulture,
                                    "Ignoring duplicate MTA in Registry Root {0}", 
                                    mtaName));
                            }
                            else
                            {
                                EventControl.Info(LogName, string.Format(
                                    System.Globalization.CultureInfo.InvariantCulture,
                                    "Adding MTA from Registry Root {0}", 
                                    mtaName));
                                TestLogger(" add mta " + mtaName);
                                this.mtaByName.Add(mtaName, mta);
                            }
                        }
                    }
                }
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    RegistryKeyMtaData));
            }
        }

        internal void ReadConnections()
        {
            TestLogger("ReadConnections");
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(RegistryKeyNodeData, false /* read only */);
            if (rk != null)
            {
                lock (this.routerLock)
                {
                    string[] names = rk.GetSubKeyNames();
                    foreach (string nodeName in names)
                    {
                        TestLogger(" read node name " + nodeName);
                        this.ReadNodeConnections(nodeName);
                    }
                }
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    RegistryKeyNodeData));
            }
        }

        internal PriorityQueue ReadRouteDetails(string subKey, string keyNodeName)
        {
            TestLogger("ReadRouteDetails");
            TestLogger(" subKey " + subKey);
            TestLogger(" keyNodeName " + keyNodeName);

            PriorityQueue priorityQueue = null;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(subKey, false /* read only */);
            if (rk != null)
            {
                string field = string.Empty;
                string nodeName = string.Empty;
                string mtaName = string.Empty;
                try
                {
                    field = "Name";
                    object value = rk.GetValue(field);
                    nodeName = (string)value;
                    field = "Mta";
                    value = rk.GetValue(field);
                    mtaName = (string)value;
                    Mta mta = this.mtaByName[mtaName];
                    Bearer bearer = mta.GetBearer(nodeName);
                    priorityQueue = bearer.SendQueue;

                    TestLogger(" nodeName " + nodeName);
                    TestLogger(" mtaName " + mtaName);

                }
                catch (ArgumentNullException e)
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Route to {0} Name or Mta field missing - {1}",
                        keyNodeName, 
                        e.Message));
                }
                catch (KeyNotFoundException e)
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Route to {0} Name {1} or Mta {2} not configured - {3}", 
                        keyNodeName, 
                        nodeName, 
                        mtaName, 
                        e.Message));
                }
            }

            

            return priorityQueue;
        }
        
        internal Node ReadNodeDetails(string nodeName)
        {
            TestLogger("ReadNodeDetails");
            Node node = null;
            string subKey = RegistryKeyNodeData + nodeName;
            string field = string.Empty;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(subKey, false /* read only */);
            if (rk != null)
            {
                try
                {
                    field = "Brigade";
                    object value = rk.GetValue(field);
                    var brigade = (int)value;
                    field = "Node";
                    value = rk.GetValue(field);
                    var nodeNumber = (int)value;
                    field = "Port";
                    value = rk.GetValue(field);
                    var port = (int)value;
                    var portAddress = new PortAddress(brigade, nodeNumber, port);
                    var gd92Type = NodeType.Rsc;
                    field = "Type";
                    value = rk.GetValue(field);
                    if (value != null)
                    {
                        // Set ignoreCase true
                        gd92Type = (NodeType)Enum.Parse(typeof(NodeType), value.ToString(), true);
                    }

                    node = new Node(nodeName, portAddress, gd92Type);

                    if (!this.configuredBrigades.Contains(brigade))
                    {
                        this.configuredBrigades.Add(brigade);
                    }

                    // these are not mandatory fields
                    field = "ManTimeout";
                    value = rk.GetValue(field);
                    if (value != null)
                    {
                        node.ManTimeout = (int)value;
                    }
                    
                    field = "Secondary";
                    value = rk.GetValue(field);
                    if (value != null)
                    {
                        node.Secondary = ((int)value > 0) ? true : false;
                    }

                    TestLogger(" brigade " + brigade.ToString());
                    TestLogger(" nodeNumber " + nodeNumber.ToString());
                    TestLogger(" port " + port.ToString());
                    TestLogger(" ManTimeout " + gd92Type.ToString());
                    TestLogger(" node.ManTimeout " + node.ManTimeout.ToString());
                    TestLogger(" Secondary " + node.Secondary.ToString());


                }
                catch (ArgumentException e)
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Registry data invalid under key {0} - {1}", 
                        subKey, 
                        e.Message));
                }
                catch (NullReferenceException e)
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Registry data field {0} missing under key {1} - {2}", 
                        field, 
                        subKey, 
                        e.Message));
                }
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    subKey));
            }
            
            return node;
        }
        
        internal void ReadNodes()
        {
            this.thisNodeName = UserAgent.Instance.ReadNodeName();
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(RegistryKeyNodeData, false /* read only */);
            if (rk != null)
            {
                lock (this.routerLock)
                {
                    string[] names = rk.GetSubKeyNames();
                    foreach (string nodeName in names)
                    {
                        Node node = this.ReadNodeDetails(nodeName);
                        if (node != null)
                        {
                            EventControl.Info(LogName, string.Format(
                                System.Globalization.CultureInfo.InvariantCulture,
                                "Adding node {0} {1} {2}{3}{4}", 
                                nodeName, 
                                node.NodeAddress.ToString(),
                                node.GD92Type.ToString(),
                                node.Secondary ? " (Secondary) Port " : string.Empty,
                                node.Secondary ? node.Address.Port.ToString(System.Globalization.CultureInfo.InvariantCulture) : string.Empty));
                            this.ReadNodeRoutes(node);
                            this.nodeByName.Add(nodeName, node);
                            if (node.Secondary == false)
                            {
                                this.nodeByAddress.Add(node.NodeAddress, node);
                            }
                        }
                    }
                }
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    RegistryKeyNodeData));
            }
        }
        
        private static Mta ReadMtaDetails(string mtaName)
        {
            string subKey = RegistryKeyMtaData + mtaName;
            return ReadMtaDetailsByFullKey(subKey, mtaName);
        }
        
        private static Mta ReadMtaDetailsByFullKey(string subKey, string mtaName)
        {
            Mta mta = null;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(subKey, false /* read only */);
            if (rk != null)
            {
                mta = new Mta(mtaName);
                object value = rk.GetValue("AckTimeout");
                mta.SetAckTimeout(value);
                value = rk.GetValue("ConnectionInterval");
                mta.SetConnectionInterval(value);
                value = rk.GetValue("EnquiryInterval");
                mta.SetEnquiryInterval(value);
                value = rk.GetValue("ListeningPort");
                mta.SetListeningPort(value);
                value = rk.GetValue("MaxAckPending");
                mta.SetMaxAckPending(value);
                value = rk.GetValue("UseDatagrams");
                mta.SetUseDatagrams(value);
                value = rk.GetValue("FrameAckCharSent");
                mta.SetFrameAckCharSent(value);
                value = rk.GetValue("FrameAckCharExpected");
                mta.SetFrameAckCharExpected(value);
                value = rk.GetValue("UseMQ");
                mta.SetUseMQ(value);
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    subKey));
            }
            
            return mta;
        }

        private Bearer ReadConnectionDetails(string nodeName, Mta mta)
        {
            TestLogger("ReadConnectionDetails");

            Bearer bearer = null;
            string subKey = RegistryKeyNodeData + nodeName + @"\Connections\" + mta.MtaName;
            string field = string.Empty;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(subKey, false /* read only */);
            if (rk != null)
            {
                try
                {
                    var addressBytes = new byte[4];
                    field = "Inet Addr 1";
                    object value = rk.GetValue(field);
                    addressBytes[0] = (byte)((int)value);
                    field = "Inet Addr 2";
                    value = rk.GetValue(field);
                    addressBytes[1] = (byte)((int)value);
                    field = "Inet Addr 3";
                    value = rk.GetValue(field);
                    addressBytes[2] = (byte)((int)value);
                    field = "Inet Addr 4";
                    value = rk.GetValue(field);
                    addressBytes[3] = (byte)((int)value);
                    var ipAddress = new IPAddress(addressBytes);
                    field = "IPort";
                    value = rk.GetValue(field);
                    int ipPort = (int)value;

                    TestLogger(" ipAddress " + ipAddress.ToString());
                    TestLogger(" ipPort " + ipPort.ToString());
                    TestLogger(" mta.UseMQ " + mta.UseMQ.ToString());
                    TestLogger(" mta.UseDatagrams " + mta.UseDatagrams.ToString());

                    if (mta.UseMQ)
                    {
                        bearer = new RabbitBearer(nodeName, mta.MtaName, ipAddress, ipPort);
                    }
                    else
                    {
                        bearer = mta.UseDatagrams ?
                            new UdpBearer(nodeName, mta.MtaName, ipAddress, ipPort) :
                            new Bearer(nodeName, mta.MtaName, ipAddress, ipPort);
                    }

                    TestLogger(" Add bearer for " + bearer.ConnectionName);
                    TestLogger(mta.UseMQ ? " Uses MQ" : " Uses socket");

                    this.bearerByName.Add(bearer.ConnectionName, bearer);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Registry data invalid under key {0} - {1}", 
                        subKey, 
                        e.Message));
                }
                catch (NullReferenceException e)
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Registry data field {0} missing under key {1} - {2}", 
                        field, 
                        subKey, 
                        e.Message));
                }
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    subKey));
            }
            
            return bearer;
        }
        
        private void ReadNodeConnections(string nodeName)
        {
            string subKey = RegistryKeyNodeData + nodeName + @"\Connections";
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(subKey, false /* read only */);
            if (rk != null)
            {
                string[] names = rk.GetSubKeyNames();
                foreach (string mtaName in names)
                {
                    if (this.mtaByName.ContainsKey(mtaName))
                    {
                        Mta mta = this.mtaByName[mtaName];
                        Bearer bearer = this.ReadConnectionDetails(nodeName, mta);
                        if (bearer != null)
                        {
                            EventControl.Info(LogName, string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                                "Adding bearer {0} {1} {2}{3}",
                                bearer.ConnectionName, 
                                bearer.ConnectionIPAddress.ToString(),
                                bearer.ConnectionPort.ToString(System.Globalization.CultureInfo.InvariantCulture),
                                mta.UseDatagrams ? " (UTP)" : string.Empty));
                            mta.AddBearer(nodeName, bearer);
                        }
                    }
                    else
                    {
                        EventControl.Warn(LogName, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "No bearer {0} {1} - MTA name not recognised",
                            nodeName, 
                            mtaName));
                    }
                }
            }
            else
            {
                EventControl.Info(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "No direct connection to node {0}", 
                    nodeName));
            }
        }
        
        /// <summary>
        /// Read routes from the Registry and configure the node.
        /// </summary>
        /// <param name="node">Node to be configured with routes.</param>
        private void ReadNodeRoutes(Node node)
        {
            string subKey = RegistryKeyNodeData + node.Name + @"\Routes";
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(subKey, false /* read only */);
            if (rk != null)
            {
                string[] names = rk.GetSubKeyNames();
                int numRoutes = names.GetLength(0);
                var preferenceList = new PriorityQueue[numRoutes];
                for (int prefIndex = 0; prefIndex < numRoutes; prefIndex++)
                {
                    int pref = prefIndex + 1;
                    string routeKey = subKey + "\\" + pref.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    PriorityQueue bearerQueue = this.ReadRouteDetails(routeKey, node.Name);
                    if (bearerQueue != null)
                    {
                        EventControl.Info(LogName, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "Adding route via bearer {0} preference {1}", 
                            bearerQueue.Name, 
                            pref.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                        preferenceList[prefIndex] = bearerQueue;
                    }
                    else
                    {
                        EventControl.Warn(LogName, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "Route to {0} preference {1} is missing", 
                            node.Name,
                            pref.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                        break;
                    }
                }
                
                node.Routes = preferenceList;
            }
            else
            {
                if (0 != string.Compare(this.thisNodeName, node.Name, System.StringComparison.Ordinal))
                {
                    EventControl.Warn(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "No route to node {0}",
                        node.Name));
                }
            }
        }
        
        /// <summary>
        /// Check that the brigade is the same as one already configured (via the Registry initially).
        /// Check that the node is above a minimum number.
        /// Force the port to 1.
        /// </summary>
        /// <param name="address">Address requiring new node.</param>
        /// <returns>Address of new node (port set to 1).</returns>
        private PortAddress ValidAddressForNewNode(PortAddress address)
        {
            if (!this.configuredBrigades.Contains(address.Brigade))
            {
                throw new GD92LibException("Unexpected brigade number");
            }
            
            if (address.Node < MinimumNodeNumberWhenAddingNodeAutomatically)
            {
                throw new GD92LibException("Node number too low");
            }
            
            return new PortAddress(address.Brigade, address.Node, 1);
        }

        /// <summary>
        /// Check this is a gateway then try to create the address for a new node.
        /// </summary>
        /// <param name="address">Address to validate.</param>
        /// <returns>True if the address can be created.</returns>
        private bool AddressIsValidForNewNode(PortAddress address)
        {
            bool isValid = false;
            Node gateway = UserAgent.Instance.GetGatewayNode();
            if (gateway != null)
            {
                try
                {
                    this.ValidAddressForNewNode(address);
                    isValid = true;
                }
                catch (GD92LibException e)
                {
                    EventControl.Info(LogName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Address is not valid for adding as a new node - {0}", 
                        e.Message));
                }
            }

            return isValid;
        }

        /// <summary>
        /// Create a new name automatically, based on the node number.
        /// </summary>
        /// <param name="address">Address of new node.</param>
        /// <returns>Node name.</returns>
        private string NodeNameFromAddress(PortAddress address)
        {
            string name = GD92Component.NodeNamePrefix + address.Node.ToString(System.Globalization.CultureInfo.InvariantCulture);
            Debug.Assert(!this.nodeByName.ContainsKey(name), "Node with same name must not exist previously");
            return name;
        }

        /// <summary>
        /// Use the first brigade number configured in the Registry or 22 if there is none.
        /// Use node number unchanged.
        /// Use port number 1.
        /// </summary>
        /// <param name="nodeNumber">Node number for the address.</param>
        /// <returns>PortAddress constructed from node number.</returns>
        private PortAddress ConstructAddressFromNodeNumber(int nodeNumber)
        {
            int brigade = this.configuredBrigades.Count > 0 ? this.configuredBrigades[0] : 22;
            return new PortAddress(brigade, nodeNumber, 1);
        }

        /// <summary>
        /// Use the destination address to find the node. If necessary create a new node if configured to do so.
        /// Set the frame destination name from the node name and set the routes.
        /// Routes field is null if destination is not found or cannot be created.
        /// </summary>
        /// <param name="frame">Frame requiring destination name and routes.</param>
        private void FindOrCreateDestinationNodeAndSetNameAndRoutes(Frame frame)
        {
            Node destinationNode = this.FindOrCreateDestinationNode(frame.Destination);
            frame.DestinationName = destinationNode.Name;
            frame.Routes = destinationNode.Routes;
        }

        /// <summary>
        /// Use the address to find the node or create a new one if configured to do so.
        /// Always returns a node object but this could be a dummy with null Routes.
        /// </summary>
        /// <param name="destinationNodeAddress">Address of destination.</param>
        /// <returns>Node for address, dummy if not found or create fails.</returns>
        private Node FindOrCreateDestinationNode(PortAddress destinationNodeAddress)
        {
            Node destinationNode = new Node(string.Empty, new PortAddress()); // dummy node
            try
            {
                if (this.nodeByAddress.ContainsKey((NodeAddress)destinationNodeAddress))
                {
                    destinationNode = this.nodeByAddress[(NodeAddress)destinationNodeAddress];
                }
                else if (!string.IsNullOrEmpty(UserAgent.Instance.GetDefaultRoute()))
                {
                    destinationNode = this.AddNodeForUnknownAddress(destinationNodeAddress);
                }
            }
            catch (GD92LibException e)
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Failed to add destination node - {0}",
                    e.ToString()));
                throw;
            }

            return destinationNode;
        }

        /// <summary>
        /// Check that a node exists corresponding to the source address (or create if configured to do so).
        /// Use the source address to find the node (and assert that the name is the same as SourceName).
        /// If not found, and the DefaultRoute is configured, create a new node 
        /// and populate the SourceName field in the frame.
        /// </summary>
        /// <param name="frame">Frame with source to be checked.</param>
        private void FindOrCreateSourceNodeAndSetSourceName(Frame frame)
        {
            try
            {
                Node sourceNode;
                var sourceNodeAddress = (NodeAddress)frame.Source;
                if (this.nodeByAddress.ContainsKey(sourceNodeAddress))
                {
                    sourceNode = this.nodeByAddress[sourceNodeAddress];
                    Debug.Assert(frame.SourceName == sourceNode.Name, "SourceName must be the same as the name of the node found by address");
                }
                else if (!string.IsNullOrEmpty(UserAgent.Instance.GetDefaultRoute()))
                {
                    sourceNode = this.AddNodeForUnknownAddress(frame.Source);
                    frame.SourceName = sourceNode.Name;
                }
            }
            catch (GD92LibException e)
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Failed to add source node - {0}", 
                    e.ToString()));
                throw;
            }
        }
        
        /// <summary>
        /// Create a new node and read routes from the Registry.
        /// The routes are for the node used to forward messages to the new node.
        /// This could be any node actually configured in the Registry (e.g. MDG1)
        /// The node name is initially the name of the forwarding node. After reading
        /// the routes, set the new name and add the node to the dictionaries.
        /// If there are no routes there is a warning in the log and no bearer to the node.
        /// </summary>
        /// <param name="address">Address requiring new node.</param>
        /// <returns>Node object.</returns>
        private Node AddNodeForUnknownAddress(PortAddress address)
        {
            var newNode = new Node(UserAgent.Instance.GetDefaultRoute(), this.ValidAddressForNewNode(address));
            EventControl.Note(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Adding new node for {0} via {1}", 
                address.ToString(), 
                newNode.Name));
            this.ReadNodeRoutes(newNode);
            newNode.SetName(this.NodeNameFromAddress(address));
            this.nodeByName.Add(newNode.Name, newNode);
            this.nodeByAddress.Add(newNode.NodeAddress, newNode);
            EventControl.Note(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Added new node {0} address {1}", 
                newNode.Name, 
                newNode.NodeAddress.ToString()));
            return newNode;
        }




        private object logLockObject = new object();
        const string LogFileName = @"C:\POCLogs\Router.txt";

        public void TestLogger(string message)
        {
            lock (logLockObject)
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(DateTime.UtcNow.ToString() + " : " + message);
                }
            }
        }

    }
}

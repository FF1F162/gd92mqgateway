﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Diagnostics;
    using Thales.KentFire.GD92.Decoders;

    /// <summary>
    /// FrameConverter Includes code for encoding and decoding frames for transmission over wire
    /// Byte order of Word16 and Word32 is defined in GD-92 A.2.2. 
    /// RSC code includes #if defined (_BIG_ENDIAN) since it depends on the byte order of the
    /// machine it is running on when interpreting unions and WORD32 etc. 
    /// The new component depends on numeric value and makes no assumptions about how numbers
    /// are stored.
    /// </summary>
    public sealed class FrameConverter
    {
        public const int MaxForByte = 255;
        public const int MaxForWord = 65535;
        internal const int MaxFrameMessageLength = 1008; // allow 15 for envelope
        internal const int MaxLength = 1023;
        internal const int MaxPriority = 9;
        internal const int MaxProtocolVersion = 15;
        internal const int MaxFrameSequenceNumber = 32767;
        internal const int DefaultNumberOfFramesInMessage = 1;
        internal const int NoOffset = 0;

        internal const int ByteSize = 256;
        internal const int DestinationCountSize = 64;
        internal const int PortSize = 64;
        internal const int ProtVerSize = 16;
        internal const int AckReqSize = 2;

        internal const int SourceIndex = 1;          // SOH at index 0
        internal const int CountAndLengthIndex = 4;  // SOH <source>
        internal const int DestinationIndex = 6;     // SOH <source> <count&length>
        internal const int MessageIndexAfterHeaderForSingleDestination = 13;
        internal const int AddressLength = 3;
        internal const int ProtToSeqLength = 3;

        // 0     1        4              6      9               10        12
        // SOH + source + count&length + dest + prot&priority + ack&seq + message_type + bcc + EOT
        //  1      3           2           3         1             2           1          1     1
        // Destinations are counted separately in received messages even though only one destination
        // is expected. Only one destination is allowed in a frame constructed by the component.
        // If this changes, message index is no longer fixed and a separate byte array is required 
        // to hold the message data (as now implemented in Encoder)
        internal const int RestOfEnvelopeLength = 12; // SOH-EOT omitting destinations and message

        internal const int MaxUncompressedRepeats = 3;
        internal const int MaxDataSize = 820; // as for RSC - much less than MaxFrameMessageLength
        internal const int BlockOfBlocksLength = 2;
        internal const int Word16Length = 2;
        internal const int ManAckRequiredLength = 1;

        internal const string IncidentDateTimeFormat = "ddMMMyyHHmmss";
        internal const int DefaultIncidentNumber = 9999;

        // Long compressed string holds a maximum of 65535 (FFFF) bytes
        // This can be held in about 65 max-sized blocks or 80 of MaxDataSize
        // Allow 120 as blocks sent by Storm contain approximately 520 bytes of data 
        internal const int MaxFeasibleNumberOfBlocks = 120;

        private static IGD92SequenceProvider gd92SequenceNumberProvider;
        private static IFrameProvider frameArrayProvider;
        private static IRouter nodeLookup;
        private static DecoderFactory decoders;
        private static EncoderFactory encoders;
        private static Node localNode;
        private static IReceivedFrames uaReceivedFrames;

        /// <summary>
        /// Prevents a default instance of the <see cref="FrameConverter"/> class from being created.
        /// </summary>
        private FrameConverter()
        {
        }

        #region Set dependencies

        /// <summary>
        /// Set the node that provides source name and address when constructing a new Nak.
        /// </summary>
        /// <param name="node">Local node object.</param>
        internal static void SetLocalNode(Node node)
        {
            Debug.Assert(node != null, "Node must not be null");
            localNode = node;
        }

        /// <summary>
        /// Set the provider of received frames, keyed by message serial number
        /// Data from the frame is used when constructing a reply.
        /// </summary>
        /// <param name="r">Received frames provider.</param>
        internal static void SetUAReceivedFrames(IReceivedFrames r)
        {
            Debug.Assert(r != null, "Received frames provider must not be null");
            uaReceivedFrames = r;
        }

        /// <summary>
        /// Set up the factory that provides an encoder for each message to be transmitted (apart from Ack and Nak,
        /// which are encoded using methods in this class).
        /// </summary>
        /// <param name="s">Sequence number provider.</param>
        internal static void SetSequenceProviderAndEncoders(IGD92SequenceProvider s)
        {
            Debug.Assert(s != null, "Sequence number provider must not be null");
            Debug.Assert(uaReceivedFrames != null, "Received frames provider must not be null - must be set prior to this call");
            gd92SequenceNumberProvider = s;
            frameArrayProvider = new FrameProvider(s, uaReceivedFrames);
            encoders = new EncoderFactory(frameArrayProvider);
        }

        /// <summary>
        /// Set up the factory that provides a decoder for each message (from an array of frames).
        /// </summary>
        /// <param name="n">Node lookup provider.</param>
        internal static void SetNodeLookupAndDecoders(IRouter n)
        {
            Debug.Assert(n != null, "Reference to router for node lookup must not be null");
            nodeLookup = n;
            decoders = new DecoderFactory(nodeLookup);
        }

        #endregion

        #region Decoding

        /// <summary>
        /// This is used by the Bearer (SocketEnd) when working out how many bytes to read 
        /// in order to receive the whole frame. 
        /// Destination count and message data length are in the two bytes after the source address.
        /// It is possible that these have not yet been read.
        /// The rest of the envelope, including BCC and EOT, is of fixed length.
        /// </summary>
        /// <param name="buffer">Read buffer.</param>
        /// <param name="offset">Offset into buffer.</param>
        /// <param name="bytesRead">Number of bytes available.</param>
        /// <returns>Length of frame or 0 if part missing.</returns>
        internal static int DecodeLength(byte[] buffer, int offset, int bytesRead)
        {
            int destinationCount;
            int messageLength;
            int length = 0;
            if (bytesRead < DestinationIndex)
            {
                length = 0; // count and length have not yet been read
            }
            else
            {
                byte countAndLength1 = buffer[offset + CountAndLengthIndex];
                byte countAndLength2 = buffer[offset + CountAndLengthIndex + 1];
                int countAndLength = (countAndLength1 * ByteSize) + countAndLength2;
                messageLength = countAndLength / DestinationCountSize;
                destinationCount = countAndLength - (messageLength * DestinationCountSize);
                length = RestOfEnvelopeLength + (AddressLength * destinationCount) + messageLength;
            }

            return length;
        }

        /// <summary>
        /// Called by the bearer to build a Frame object from bytes received.
        /// </summary>
        /// <param name="frameBytes">Array of bytes comprising a frame.</param>
        /// <returns>Frame decoded from bytes.</returns>
        internal static Frame BuildFrame(byte[] frameBytes)
        {
            var frame = new Frame(frameBytes);
            var decoder = new FrameDecoder(frame, nodeLookup);
            decoder.DecodeFrame();
            return frame;
        }

        /// <summary>
        /// Called by the User Agent to decode and log a message (or reply).
        /// </summary>
        /// <param name="frames">Array of frames comprising message.</param>
        /// <returns>Message decoded from bytes.</returns>
        internal static GD92Message BuildMessageFromFrames(Frame[] frames)
        {
            Debug.Assert(frames != null, "Frame array must not be null");
            Debug.Assert(frames.Length > 0, "Frame array must contain at least one frame");
            IDecoder decoder = decoders.DecoderForMessageFrames(frames);
            return decoder.DecodeMessage();
        }

        #endregion

        #region Encoding

        /// <summary>
        /// Called by the User Agent to encode and log a message (or reply)
        /// submitted by the host application.
        /// </summary>
        /// <param name="message">Message to encode.</param>
        /// <returns>Array of encoded frames.</returns>
        internal static Frame[] BuildFramesFromMessage(GD92Message message)
        {
            Debug.Assert(gd92SequenceNumberProvider != null, "Sequence number provider must not be null");
            IMessageEncoder encoder = encoders.EncoderForMessageType(message);
            Frame[] frameArray = encoder.EncodeMessage(message);
            return frameArray;
        }

        /// <summary>
        /// The User Agent uses this when acknowledging a frame immediately. This happens for all
        /// blocks except the last in a long (multi-block) message.
        /// </summary>
        /// <param name="frame">Frame to be acknowledged.</param>
        /// <returns>Acknowledgement frame.</returns>
        internal static Frame ConstructAckForFrame(Frame frame)
        {
            var ackFrame = frame.CopyForAcknowledgement();
            ackFrame.ProtocolVersion = GD92Header.AckProtocolVersion;
            ackFrame.MessageType = GD92MsgType.Ack;
            ackFrame.FrameBytes = new byte[MaxLength];
            ackFrame.FrameMessageLength = 0;
            Encoder.EncodeEnvelope(ackFrame);
            return ackFrame;
        }
        
        /// <summary>
        /// Used when reporting timeout, no bearer etc. Port number 0 indicates router.
        /// Copy the frame and swap Source and Destination
        /// Use Source (as original destination) when constructing the  Nak message field
        /// Then overwrite the Source with the address of the local router. 
        /// </summary>
        /// <param name="frame">Frame requiring nak.</param>
        /// <param name="reason">Reason code.</param>
        /// <returns>Nak frame.</returns>
        internal static Frame ConstructNakForFrameFromRouter(Frame frame, int reason)
        {
            Frame nakFrame = CopyFrameAndEncodeMessageField(frame, GD92ReasonCodeSet.General, reason);
            nakFrame.Source = AddressForRouter();
            nakFrame.SourceName = localNode.Name;
            nakFrame.SourceNodeType = NodeType.Gateway;
            Encoder.EncodeEnvelope(nakFrame);
            return nakFrame;
        }

        /// <summary>
        /// Supply address of local router.
        /// </summary>
        /// <returns>Router address.</returns>
        internal static PortAddress AddressForRouter()
        {
            PortAddress address = localNode.Address;
            return new PortAddress(address.Brigade, address.Node, GD92Header.RouterPortNumber);
        }

        /// <summary>
        /// Used for Nak(WaitAck), Nak(InvMess) etc
        /// Assume GENERAL reason code set.
        /// </summary>
        /// <param name="frame">Frame requiring nak.</param>
        /// <param name="reason">Reason code.</param>
        /// <returns>Nak frame.</returns>
        internal static Frame ConstructNakForFrame(Frame frame, int reason)
        {
            return ConstructNakForFrame(frame, GD92ReasonCodeSet.General, reason);
        }

        /// <summary>
        /// Copy the frame swapping Source and Destination.
        /// Use Source (as original destination) when constructing the  Nak message field
        /// Encode the nak frame.
        /// </summary>
        /// <param name="frame">Frame requiring nak.</param>
        /// <param name="set">Reason code set.</param>
        /// <param name="reason">Reason code.</param>
        /// <returns>Nak frame.</returns>
        internal static Frame ConstructNakForFrame(Frame frame, GD92ReasonCodeSet set, int reason)
        {
            Frame nakFrame = CopyFrameAndEncodeMessageField(frame, set, reason);
            Encoder.EncodeEnvelope(nakFrame);
            return nakFrame;
        }

        /// <summary>
        /// Populate new nak frame from data in original frame and reason provided.
        /// Encode the message part of the frame byte array.
        /// </summary>
        /// <param name="frame">Frame requiring nak.</param>
        /// <param name="set">Reason code set.</param>
        /// <param name="reason">Reason code.</param>
        /// <returns>Nak frame.</returns>
        private static Frame CopyFrameAndEncodeMessageField(Frame frame, GD92ReasonCodeSet set, int reason)
        {
            Frame nakFrame = frame.CopyForAcknowledgement();
            nakFrame.ProtocolVersion = GD92Header.NakProtocolVersion;
            nakFrame.MessageType = GD92MsgType.Nak;
            nakFrame.EncodeNakMessageField(set, reason);
            return nakFrame;
        }

        #endregion
    }
}

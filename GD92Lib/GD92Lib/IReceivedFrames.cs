﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Interface to retrieve frame by serial number from the dictionary of frames received by the UA
    /// Used when constructing acknowledgement for the frame.
    /// </summary>
    internal interface IReceivedFrames
    {
        Frame GetFrameToAcknowledge(int serialNumber);

        void AddFramePendingReply(Frame frame);
    }
}

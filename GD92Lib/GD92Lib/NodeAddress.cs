﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// NodeAddress - used when port number must be disregarded.
    /// implements IEquatable T as it is used as a Dictionary key.
    /// </summary>
    internal struct NodeAddress : IEquatable<NodeAddress>
    {
        private readonly short brigade;
        private readonly short node;

        /// <summary>
        /// Initialises a new instance of the <see cref="NodeAddress"/> struct.
        /// </summary>
        /// <param name="brigade">Brigade number.</param>
        /// <param name="node">Node number.</param>
        public NodeAddress(int brigade, int node)
        {
            this.brigade = GD92Message.ValidateBrigade("Brigade", brigade);
            this.node = GD92Message.ValidateNode("Node", node);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NodeAddress"/> struct.
        /// </summary>
        /// <param name="portAddress">PortAddress (port is discarded).</param>
        public NodeAddress(PortAddress portAddress)
        {
            this.brigade = (short)portAddress.Brigade;
            this.node = (short)portAddress.Node;
        }

        /// <summary>
        /// Gets Brigade number.
        /// </summary>
        /// <value>Brigade number.</value>
        internal int Brigade
        {
            get
            {
                return this.brigade;
            }
        }

        /// <summary>
        /// Gets Node number.
        /// </summary>
        /// <value>Node number.</value>
        internal int Node
        {
            get
            {
                return this.node;
            }
        }

        /// <summary>
        /// Implement == operator to avoid boxing.
        /// </summary>
        /// <param name="first">NodeAddress.</param>
        /// <param name="second">NodeAddress to compare.</param>
        /// <returns>True if all fields are the same.</returns>
        public static bool operator ==(NodeAddress first, NodeAddress second)
        {
            return first.Equals(second);
        }

        /// <summary>
        /// Implement != operator to avoid boxing.
        /// </summary>
        /// <param name="first">GD92Address.</param>
        /// <param name="second">GD92Address to compare.</param>
        /// <returns>True if not all fields are the same.</returns>
        public static bool operator !=(NodeAddress first, NodeAddress second)
        {
            return !first.Equals(second);
        }

        /// <summary>
        /// Override Equals(object) as part of IEquatable T. See also PortAddress and GD92Address.
        /// Check for null and different type then cast to type and call the type-specific Equals method.
        /// </summary>
        /// <param name="obj">Object to be compared with this.</param>
        /// <returns>True if the object equals this.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            var na = (NodeAddress)obj;
            return this.Equals(na);
        }

        /// <summary>
        /// Override Equals(type) as part of IEquatableT. See also PortAddress and GD92Address.
        /// Check all fields are the same.
        /// </summary>
        /// <param name="other">A NodeAddress.</param>
        /// <returns>True if all fields are the same.</returns>
        public bool Equals(NodeAddress other)
        {
            return (this.brigade == other.brigade) && (this.node == other.node);
        }

        /// <summary>
        /// Brigade is always the same.
        /// There is no justification for anything more complicated.
        /// </summary>
        /// <returns>Value of Node field.</returns>
        public override int GetHashCode()
        {
            return this.Node;
        }

        /// <summary>
        /// Return representation of node.
        /// </summary>
        /// <returns>Brigade Node.</returns>
        public override string ToString()
        {
            return string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                "{0} {1}",
                this.brigade.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.node.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}

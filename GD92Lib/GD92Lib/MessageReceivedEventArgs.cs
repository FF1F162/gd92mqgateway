﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    public class MessageReceivedEventArgs : EventArgs
    {
        private GD92Message message;

        public MessageReceivedEventArgs(GD92Message newMessage)
        {
            this.message = newMessage;
        }

        public GD92Message Message
        {
            get { return this.message; }
        }
    }
}

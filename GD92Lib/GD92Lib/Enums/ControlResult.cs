﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    public enum ControlResult
    {
        Success,
        UnknownBearer,
        AlreadyEnabled,
        AlreadyDisabled,
        AlreadyConnected,
        AlreadyDisconnected,
        NoSocketConnection,
        ExceptionCaught,
        NoAction
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Type of Message Tramsfer Agent (Mlan stands for MOBS LAN)
    /// </summary>
    public enum MtaType
    {
        None,
        Mlan,
        Tcp,
        Udp
    }
}

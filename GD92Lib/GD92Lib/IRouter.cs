﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Supply node objects by lookup.
    /// </summary>
    internal interface IRouter
    {
        Node GetNodeByNameOrDefault(string nodeName);

        Node GetNodeByAddressOrDefault(NodeAddress address);
    }
}

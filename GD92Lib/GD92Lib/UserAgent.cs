﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Threading;
    using Microsoft.Win32;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// UserAgent, like Bearer, services a PriorityQueue, accepting frames routed to the local node.
    /// This queue appears as a single route to the local node.
    /// For routing purposes the local node appears as just another node where frames can be sent.
    /// There is a single instance of this class.
    /// </summary>
    internal sealed class UserAgent : IDisposable
    {
        private const string AppConfigRegistryKey = @"GD92RegistryKey";
        private const string DefaultRegistryKeyRoot = @"Software\SysecaTest\Kent MDG";

        private const int PriorityQueueInterval = 1000; // milliseconds
        private const string LogName = "UA";

        private static readonly UserAgent Self = new UserAgent();
        private readonly PriorityQueue priorityQueue;
        private readonly string registryKeyRoot;
        private readonly string registryKeyPrivate;
        private Node localNode = null;
        private Node gateway = null;
        private string defaultRoute = string.Empty;
        private string firstMtaToActivate = string.Empty;
        private int firstMtaTimeout;
        private int retries;
        private int noAckTimeout;

        private Timer priorityQueueTimer;
        private bool processingPriorityQueue;

        private ReceivedFrames receivedFrames;
        private TransmittedFrames transmittedFrames;
        private ISerialNumberProvider serialNumberProvider;

        /// <summary>
        /// Prevents a default instance of the <see cref="UserAgent"/> class from being created.
        /// </summary>
        private UserAgent()
        {
            this.priorityQueue = new PriorityQueue("Local UserAgent");
            this.registryKeyRoot = RegistryKeyRootFromConfigOrDefault();
            this.registryKeyPrivate = this.registryKeyRoot + @"\Private";
        }

        /// <summary>
        /// Dispose of the priority queue timer.
        /// </summary>
        public void Dispose()
        {
            if (this.priorityQueueTimer != null)
            {
                this.priorityQueueTimer.Dispose();
                this.priorityQueueTimer = null;
            }
        }

        #region Properties

        /// <summary>
        /// Provide access to the single instance.
        /// </summary>
        internal static UserAgent Instance
        {
            get
            {
                return Self;
            }
        }

        /// <summary>
        /// Name of this node (local node).
        /// </summary>
        internal string NodeName
        {
            get
            {
                if (this.localNode != null)
                {
                    return this.localNode.Name;
                }
                else
                {
                    throw new MissingFieldException("Local node is not configured");
                }
            }
        }
        
        /// <summary>
        /// Address of this node (local node).
        /// </summary>
        internal PortAddress GD92Address
        {
            get
            {
                if (this.localNode != null)
                {
                    return new PortAddress(this.localNode.NodeAddress);
                }
                else
                {
                    throw new MissingFieldException("Local node is not configured");
                }
            }
        }

        /// <summary>
        /// No acknowledgement timeout.
        /// Acknowldegement for a transmitted message is expected within this number of seconds.
        /// </summary>
        internal int NoAckTimeout
        {
            get { return this.noAckTimeout; }
        }

        /// <summary>
        /// Manual acknowledgement timeout for this node.
        /// This is used when calculating how long a received frame should be retained.
        /// </summary>
        internal int LocalManTimeout
        {
            get { return this.localNode.ManTimeout; }
        }

        /// <summary>
        /// Number of retries. Currently (Mar 2015) retries are not fully implemented.
        /// </summary>
        internal int Retries
        {
            get { return this.retries; }
        }

        /// <summary>
        /// Registry key that includes GD92 configuration for the node.
        /// </summary>
        internal string RegistryKeyRoot
        {
            get { return this.registryKeyRoot; }
        }

        /// <summary>
        /// Registry key that includes the last-allocated frame and serial numbers.
        /// </summary>
        internal string RegistryKeyPrivate
        {
            get { return this.registryKeyPrivate; }
        }

        #endregion

        /// <summary>
        /// Read node name. 
        /// Used by router in advance of ReadConfiguration, which depends on items set up in the router.
        /// </summary>
        /// <returns>Name of the local node.</returns>
        internal string ReadNodeName()
        {
            string nodeName = string.Empty;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(this.RegistryKeyRoot, false /* read only */);
            if (rk != null)
            {
                object value = rk.GetValue("Name");
                nodeName = (string)value;
            }
            
            return nodeName;
        }

        /// <summary>
        /// Get gateway node.
        /// This can be configured to show that this node is a gateway.
        /// </summary>
        /// <returns>Node object or null if this is not a gateway.</returns>
        internal Node GetGatewayNode()
        {
            return this.gateway;
        }

        /// <summary>
        /// Get name of node to use as the default route.
        /// This can be configured to allow the router to create nodes automatically.
        /// </summary>
        /// <returns>Node name.</returns>
        internal string GetDefaultRoute()
        {
            return this.defaultRoute;
        }

        /// <summary>
        /// Get name of first MTA to activate. Activate others only when there is a connection using the first.
        /// This can be configured to prevent MDGs connecting to a gateway with no connection to Storm.
        /// </summary>
        /// <returns>Name of MTA.</returns>
        internal string GetFirstMtaToActivate()
        {
            return this.firstMtaToActivate;
        }

        /// <summary>
        /// Get the setting for the timer used to restart the gateway after losing connection to the first MTA.
        /// The timer is cancelled if the connection is restored within the timeout.
        /// </summary>
        /// <returns>Number of seconds before restart following loss of connection.</returns>
        internal int GetFirstMtaTimeout()
        {
            return this.firstMtaTimeout;
        }

        /// <summary>
        /// Read the items in the root of the Registry entry.
        /// </summary>
        internal void ReadConfiguration()
        {
            string subKey = this.RegistryKeyRoot;
            string field = string.Empty;
            string message = string.Empty;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(subKey, false /* read only */);
            if (rk != null)
            {
                try
                {
                    this.ReadName(field = "Name", rk);
                    this.ReadNoAckTimeout(field = "NoAckTimeout", rk);
                    this.ReadRetries(field = "Retries", rk);
                    this.ReadGatewayName(field = "GatewayName", rk);
                    this.ReadDefaultRoute(field = "DefaultRoute", rk);
                    this.ReadFirstMta(field = "FirstMta", rk);
                    if (!string.IsNullOrEmpty(this.firstMtaToActivate))
                    {
                        this.ReadFirstMtaTimeoutOrDefault(field = "FirstMtaTimeout", rk);
                    }
                }
                catch (ArgumentNullException e)
                {
                    message = string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Registry data field {0} missing under key {1} - {2}", 
                        field, 
                        subKey, 
                        e.Message);
                    EventControl.Warn(LogName, message);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    message = string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Registry data invalid under key {0} - {1}", 
                        subKey, 
                        e.Message);
                }
                catch (NullReferenceException e)
                {
                    message = string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Registry data field {0} missing under key {1} - {2}", 
                        field, 
                        subKey, 
                        e.Message);
                }
            }
            else
            {
                message = string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}", 
                    subKey);
            }

            if (message.Length > 0)
            {
                throw new GD92LibException(message);
            }
        }
        
        /// <summary>
        /// Activate the user agent.
        /// </summary>
        /// <param name="serialProvider">Provider of message serial numbers.</param>
        internal void Activate(ISerialNumberProvider serialProvider)
        {
            Debug.Assert(this.priorityQueueTimer == null, "Expected to be null before activation");
            this.priorityQueueTimer = new Timer(this.ProcessPriorityQueue, null, 0, PriorityQueueInterval);
            Debug.Assert(this.priorityQueue != null, "Priority queue must not be null");
            if (this.priorityQueue != null)
            {
                this.priorityQueue.Connected = true; // no lock for this
            }
            
            Debug.Assert(this.receivedFrames == null, "Expected to be null before activation");
            this.receivedFrames = new ReceivedFrames();
            Debug.Assert(this.transmittedFrames == null, "Expected to be null before activation");
            this.transmittedFrames = new TransmittedFrames();
            this.serialNumberProvider = serialProvider;
            FrameConverter.SetLocalNode(this.localNode);
            FrameConverter.SetUAReceivedFrames(this.receivedFrames);
            FrameConverter.SetSequenceProviderAndEncoders(new GD92SequenceProvider());
            FrameConverter.SetNodeLookupAndDecoders(Router.Instance);
            var preferenceList = new PriorityQueue[1];
            preferenceList[0] = this.priorityQueue;
            this.localNode.Routes = preferenceList;
        }
        
        /// <summary>
        /// Deactivate the user agent. Dispose of timers and cleare received and transmitted frames.
        /// </summary>
        internal void Deactivate()
        {
            if (this.priorityQueueTimer != null)
            {
                this.priorityQueueTimer.Dispose();
                this.priorityQueueTimer = null;
            }

            if (this.receivedFrames != null)
            {
                this.receivedFrames.ClearAndCancelFrameTimers();
                this.receivedFrames = null;
            }

            if (this.transmittedFrames != null)
            {
                this.transmittedFrames.Clear();
                this.transmittedFrames = null;
            }
        }

        /// <summary>
        /// If acknowledgement is required (or expected regardless of ack_req setting) add the frame to transmitted frames.
        /// Queue the frame for the first bearer in the routes configured.
        /// </summary>
        /// <param name="frame">Frame to be transmitted.</param>
        internal void TransmitFrame(Frame frame)
        {
            this.transmittedFrames.AddForAcknowledgement(frame);
            TransmittedFrames.TransmitFrame(frame);
        }

        /// <summary>
        /// Remove the frame from the UA received frames as there is probably no longer any need to keep it.
        /// </summary>
        /// <param name="frame">Frame that has timed out.</param>
        internal void DoReceivedFrameTimedOutProcessing(Frame frame)
        {
            this.receivedFrames.RemoveFrame(frame);
        }
        
        /// <summary>
        /// Send a Nak for the frame so it is logged and removed from the UA transmitted frames.
        /// </summary>
        /// <param name="frame">Frame that has timed out.</param>
        internal void DoTransmittedFrameTimedOutProcessing(Frame frame)
        {
            this.SendFrameTimedOutToSelf(frame);
        }

        /// <summary>
        /// I think the original idea was to notify the HAP by raising a reply event.
        /// For the gateway this is diverted and then filtered and logged by the Reply51Handler.
        /// </summary>
        /// <param name="frame">Timed out frame.</param>
        internal void SendFrameTimedOutToSelf(Frame frame)
        {
            Frame nakFrame = FrameConverter.ConstructNakForFrameFromRouter(frame, (int)GD92GeneralReasonCode.TransmissionTimedOut);
            EventControl.Info(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0} Replying to self about Frame {1} with Nak (TransmissionTimedOut) {2} of {3} bytes{4}{5}",
                this.priorityQueue.Name,
                frame.Ident.ToString(),
                nakFrame.Ident.ToString(),
                nakFrame.Length,
                Environment.NewLine,
                BitConverter.ToString(nakFrame.FrameBytes, 0, nakFrame.Length)));
            Router.Instance.SetRoutes(nakFrame);
            Router.DivertToGatewayIfAny(nakFrame);
            nakFrame.QueueForNextBearer();
        }

        #region Property validation

        /// <summary>
        /// Check it is within range and cast to short. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="noAckTimeout">Number to validate.</param>
        /// <returns>Number as short.</returns>
        private static short ValidateNoAckTimeout(int noAckTimeout)
        {
            if (noAckTimeout < 0 || noAckTimeout > FrameConverter.MaxForByte)
            {
                throw new ArgumentOutOfRangeException("noAckTimeout");
            }

            return (short)noAckTimeout;
        }

        /// <summary>
        /// Check it is within range and cast to short. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="retries">Number to validate.</param>
        /// <returns>Number as short.</returns>
        private static short ValidateRetries(int retries)
        {
            if (retries < 0 || retries > FrameConverter.MaxForByte)
            {
                throw new ArgumentOutOfRangeException("retries");
            }

            return (short)retries;
        }

        /// <summary>
        /// Check it is within range and cast to short. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="firstMtaTimeoutInSeconds">Number to validate.</param>
        /// <returns>Number as int.</returns>
        private static int ValidateFirstMtaTimeout(int firstMtaTimeoutInSeconds)
        {
            if (firstMtaTimeoutInSeconds < 0 || firstMtaTimeoutInSeconds > FrameConverter.MaxForWord)
            {
                throw new ArgumentOutOfRangeException("firstMtaTimeoutInSeconds");
            }

            return firstMtaTimeoutInSeconds;
        }

        #endregion

        #region Registry access

        /// <summary>
        /// Look for the registry key in the application configuration.
        /// This allows for separate test configuration under a different root.
        /// </summary>
        /// <returns>Registry key.</returns>
        private static string RegistryKeyRootFromConfigOrDefault()
        {
            return ConfigurationManager.AppSettings[AppConfigRegistryKey] ?? DefaultRegistryKeyRoot;
        }

        /// <summary>
        /// Get the node from node data already configured.
        /// Throw ArgumentOutOfRangeException if the node cannot be found.
        /// </summary>
        /// <param name="field">Name of configuration field.</param>
        /// <param name="nodeName">Node name.</param>
        /// <returns>Node object.</returns>
        private static Node ConfiguredNodeForName(string field, string nodeName)
        {
            Node nodeForName = Router.Instance.GetNodeByNameOrDefault(nodeName);
            if (nodeForName.Name != nodeName)
            {
                throw new ArgumentOutOfRangeException(field, string.Format(
                                System.Globalization.CultureInfo.InvariantCulture,
                                "{1} not found in NodeData",
                                field,
                                nodeName));
            }

            return nodeForName;
        }

        /// <summary>
        /// Read name and check it is already configured in the node data.
        /// </summary>
        /// <param name="field">Name of field.</param>
        /// <param name="rk">Registry key.</param>
        private void ReadName(string field, RegistryKey rk)
        {
            object value = rk.GetValue(field);
            string nodeName = (string)value;
            if (nodeName.Length > 0)
            {
                this.localNode = ConfiguredNodeForName(field, nodeName);
            }
        }

        /// <summary>
        /// Read no acknowledgement timeout.
        /// </summary>
        /// <param name="field">Name of field.</param>
        /// <param name="rk">Registry key.</param>
        private void ReadNoAckTimeout(string field, RegistryKey rk)
        {
            object value = rk.GetValue(field);
            EventControl.Note(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0} set to {1}",
                field,
                value));
            this.noAckTimeout = ValidateNoAckTimeout((int)value);
        }

        /// <summary>
        /// Read retries.
        /// </summary>
        /// <param name="field">Name of field.</param>
        /// <param name="rk">Registry key.</param>
        private void ReadRetries(string field, RegistryKey rk)
        {
            object value = rk.GetValue(field);
            EventControl.Note(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0} set to {1} (note: retries are not implemented)",
                field,
                value));
            this.retries = ValidateRetries((int)value);
        }

        /// <summary>
        /// Read gateway name (if configured) and check it is already configured in the node data.
        /// </summary>
        /// <param name="field">Name of field.</param>
        /// <param name="rk">Registry key.</param>
        private void ReadGatewayName(string field, RegistryKey rk)
        {
            object value = rk.GetValue(field);
            if (value != null)
            {
                string nodeName = (string)value;
                EventControl.Note(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} set to {1}",
                    field,
                    nodeName));
                if (nodeName.Length > 0)
                {
                    this.gateway = ConfiguredNodeForName(field, nodeName);
                }
            }
        }

        private void ReadDefaultRoute(string field, RegistryKey rk)
        {
            object value = rk.GetValue(field);
            if (value != null)
            {
                this.defaultRoute = (string)value;
                EventControl.Note(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} set to {1}",
                    field,
                    this.defaultRoute));
            }
        }

        private void ReadFirstMta(string field, RegistryKey rk)
        {
            object value = rk.GetValue(field);
            if (value != null)
            {
                this.firstMtaToActivate = (string)value;
                EventControl.Note(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} set to {1}",
                    field,
                    this.firstMtaToActivate));
            }
        }

        private void ReadFirstMtaTimeoutOrDefault(string field, RegistryKey rk)
        {
            object value = rk.GetValue(field);
            if (value != null)
            {
                this.firstMtaTimeout = ValidateFirstMtaTimeout((int)value);
            }
            else
            {
                this.firstMtaTimeout = FirstMta.DefaultFirstMtaTimeoutInSeconds;
            }

            EventControl.Note(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0} set to {1}",
                field,
                this.firstMtaTimeout));
        }

        #endregion

        #region Methods called by timer

        private void ProcessPriorityQueue(object state)
        {
            if (this.processingPriorityQueue == true)
            {
                EventControl.Debug(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} Flag processingPriorityQueue found set", 
                    this.priorityQueue.Name));
                
                // the flag gets reset after an exception so this means that 
                // the previous thread is slow or deadlocked - do not make things worse
                // this happened quite often during testing so demote from ERR to DEBUG 
            }
            else
            {
                this.ReceiveAllFramesOnQueue();
            }
        }
        
        private void ReceiveAllFramesOnQueue()
        {
            try
            {
                this.processingPriorityQueue = true;
                Frame frame;
                do
                {
                    frame = this.priorityQueue.GetHighestPriorityFrame();
                    if (frame != null)
                    {
                        this.DoReceivedFrameProcessing(frame);
                    }
                }
                while (frame != null);
            }
            catch (Exception e)
            {
                EventControl.Emergency(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "ProcessPriorityQueue - {0}", 
                    e.ToString()));
                throw;
            }
            finally
            {
                this.processingPriorityQueue = false;
            }
        }
        
        /// <summary>
        /// Different processing is required for replies and unsolicited frames
        /// Some unsolicited frames do not require acknowledgement (e.g. Msg20 from Storm)
        /// A reply must not require acknowledgement and comprises only one frame.
        /// Types 50 and 51 are always replies. Type 20 can be a reply to type 5.
        /// Query transmitted frames (within its lock) to find a frame with the same 
        /// sequence number and source address matching destination address.
        /// </summary>
        /// <param name="frame">Received frame.</param>
        private void DoReceivedFrameProcessing(Frame frame)
        {
            try
            {
                if (!frame.AckRequired && frame.OfBlocks == 1 &&
                    (frame.MessageType == GD92MsgType.Ack ||
                     frame.MessageType == GD92MsgType.Nak ||
                     this.transmittedFrames.ContainsMatchingFrame(frame)))
                {
                    this.ProcessReplyFrame(frame);
                }
                else 
                {
                    this.ProcessUnsolicitedFrame(frame);
                }
            }
            catch (GD92LibDecoderException e)
            {
                this.LogAndRespondToInvalidMessage(frame, e.Message);
            }
            catch (ArgumentOutOfRangeException e)
            {
                this.LogAndRespondToInvalidMessage(frame, e.Message);
            }
            catch (IndexOutOfRangeException e)
            {
                this.LogAndRespondToInvalidMessage(frame, e.Message);
            }
        }
        
        private void LogAndRespondToInvalidMessage(Frame frame, string exceptionMessage)
        {
            EventControl.Warn(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Received message type {0} decode failed {1}",
                frame.MessageType.ToString(), 
                exceptionMessage));
            if (frame.AckRequired) 
            {
                this.SendNakInvMessForFrame(frame);
            }
        }
        
        private void ProcessReplyFrame(Frame frame)
        {
            var frames = new Frame[1];
            frames[0] = frame;
            GD92Message reply = FrameConverter.BuildMessageFromFrames(frames);
            
            // at this point the AcknowledgedMessage is not known - get it now . . .
            bool raiseEvent = this.transmittedFrames.DoReplyProcessing(frame, reply);
            if (raiseEvent)
            {
                int serialNumber = this.serialNumberProvider.GetSerialNumber();
                if (serialNumber != GD92Component.StatusFail)
                {
                    reply.SetSerialNumber(serialNumber);
                }
                
                EventControl.Info(LogName, string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                    "Received message type {0} serial number {1} in reply to {2}",
                    reply.MsgType.ToString(), 
                    reply.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    reply.OriginalMessage.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                GD92Component.QueueReplyReceivedEvent(reply);
            }
        }
        
        /// <summary>
        /// Process a frame of a new message (not a reply). If a frame with the same source and sequence number
        /// has already been received, log it as a duplicate; otherwise add it to ReceivedMessages. If all frames
        /// are now received, process the message; otherwise acknowledge this frame if required. Note that the sender
        /// may require this acknowledgement before sending the next frame of the message.
        /// </summary>
        /// <param name="frame">Received frame.</param>
        private void ProcessUnsolicitedFrame(Frame frame)
        {
            bool added = this.receivedFrames.AddFrameAndSetTimeout(frame);
            if (added)
            {
                Frame[] frames = this.receivedFrames.GetAllFramesForMessage(frame);
                if (frames != null)
                {
                    this.ProcessCompletelyReceivedMessage(frames, frame);
                }
                else if (frame.AckRequired)
                {
                    this.SendAckForFrame(frame);
                }
            }
            else
            {
                EventControl.Info(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} Received Duplicate Frame {1} (discarded)",
                    this.priorityQueue.Name, 
                    frame.Ident.ToString()));
            }
        }
        
        /// <summary>
        /// Decode the message and allocate a serial number. The HAP provides this as key when sending a reply.
        /// Save the message pending reply and raise the message received event for the HAP to process.
        /// </summary>
        /// <param name="frames">Frames comprising the message.</param>
        /// <param name="lastFrameReceived">Last block of the message.</param>
        private void ProcessCompletelyReceivedMessage(Frame[] frames, Frame lastFrameReceived)
        {
            GD92Message message = FrameConverter.BuildMessageFromFrames(frames);
            int serialNumber = this.serialNumberProvider.GetSerialNumber();
            message.SetSerialNumber(serialNumber);
            this.AcknowledgeFrameOrSavePendingReply(lastFrameReceived, message);
            EventControl.Info(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Received message type {0} serial number {1}",
                message.MsgType.ToString(), 
                message.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            GD92Component.QueueMessageReceivedEvent(message);
        }
        
        /// <summary>
        /// This used to send Ack automatically (if required) but now assumes that the HAP will handle
        /// acknowledgement. If this is not a gateway send Nak(wait_ack) if manual acknowledgement is set.
        /// In the RSC this is configurable for each message type.
        /// </summary>
        /// <param name="lastFrameReceived">Last frame of message.</param>
        /// <param name="message">Received message.</param>
        private void AcknowledgeFrameOrSavePendingReply(Frame lastFrameReceived, GD92Message message)
        {
            this.receivedFrames.AddFramePendingReply(lastFrameReceived);
            if (this.gateway == null && message.ManualAck)
            {
                this.SendWaitAckForFrame(lastFrameReceived);
            }
        }
        
        private void SendAckForFrame(Frame frame)
        {
            Frame ackFrame = FrameConverter.ConstructAckForFrame(frame);
            EventControl.Info(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0} Replying to Frame {1} with Ack {2} of {3} bytes{4}{5}",
                this.priorityQueue.Name, 
                frame.Ident.ToString(),
                ackFrame.Ident.ToString(), 
                ackFrame.Length,
                Environment.NewLine,
                BitConverter.ToString(ackFrame.FrameBytes, 0, ackFrame.Length)));
            Router.Instance.SetRoutes(ackFrame);
            ackFrame.QueueForNextBearer();
        }
        
        private void SendWaitAckForFrame(Frame frame)
        {
            Frame ackFrame = FrameConverter.ConstructNakForFrame(frame, (int)GD92GeneralReasonCode.WaitAcknowledgement);
            EventControl.Info(LogName, string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                "{0} Replying to Frame {1} with Nak (WaitAck) {2} of {3} bytes{4}{5}",
                this.priorityQueue.Name, 
                frame.Ident.ToString(), 
                ackFrame.Ident.ToString(), 
                ackFrame.Length,
                Environment.NewLine,
                BitConverter.ToString(ackFrame.FrameBytes, 0, ackFrame.Length)));
            Router.Instance.SetRoutes(ackFrame);
            ackFrame.QueueForNextBearer();
        }
        
        private void SendNakInvMessForFrame(Frame frame)
        {
            Frame ackFrame = FrameConverter.ConstructNakForFrame(frame, (int)GD92GeneralReasonCode.InvalidMessage);
            EventControl.Info(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0} Replying to Frame {1} with Nak (InvMess) {2} of {3} bytes{4}{5}",
                this.priorityQueue.Name, 
                frame.Ident.ToString(), 
                ackFrame.Ident.ToString(), 
                ackFrame.Length,
                Environment.NewLine,
                BitConverter.ToString(ackFrame.FrameBytes, 0, ackFrame.Length)));
            Router.Instance.SetRoutes(ackFrame);
            ackFrame.QueueForNextBearer();
        }
        
        #endregion
    }
}

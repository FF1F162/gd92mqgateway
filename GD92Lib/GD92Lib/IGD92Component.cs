﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Interface documented in User Guide S174_9.doc.
    /// </summary>
    internal interface IGD92Component
    {
        event EventHandler<LogEventArgs> Log;

        event EventHandler<MessageReceivedEventArgs> MessageReceived;

        event EventHandler<ReplyReceivedEventArgs> ReplyReceived;

        event EventHandler<NodeConnectedEventArgs> NodeConnected;

        event EventHandler<NodeConnectionLostEventArgs> NodeConnectionLost;

        ControlResult Activate();

        bool Activated { get; }

        ControlResult ConnectBearer(string nodeName, string bearerType);

        ControlResult ConnectBearer(string bearerName);

        ControlResult Deactivate();

        ControlResult DisableBearer(string nodeName, string bearerType);

        ControlResult DisableBearer(string bearerName);

        ControlResult DisconnectBearer(string nodeName, string bearerType);

        ControlResult DisconnectBearer(string bearerName);

        ControlResult EnableBearer(string nodeName, string bearerType);

        ControlResult EnableBearer(string bearerName);

        int GetNumberOfQueuedFrames();

        LogLevel LogEventLevel { get; set; }

        int SendMessage(GD92Message message);
    }
}

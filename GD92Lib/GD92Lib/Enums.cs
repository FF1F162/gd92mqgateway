﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;

namespace Thales.KentFire.GD92
{
    public enum MtaType
    {
        NULL,
        MLAN,
        TCP,
        UDP
    }
    public enum ControlType
    {
        DisableBearer,
        EnableBearer,
        DisconnectBearer,
        ConnectBearer
    }
    public enum ControlResult
    {
        Success,
	    UnknownBearer,
	    AlreadyEnabled,
	    AlreadyDisabled,
	    AlreadyConnected,
	    AlreadyDisconnected,
	    NoSocketConnection,
        ExceptionCaught,
	    NoAction
    } 
}

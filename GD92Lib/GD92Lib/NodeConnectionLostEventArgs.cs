﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    public class NodeConnectionLostEventArgs : EventArgs
    {
        public const string DefaultReason = "No reason supplied";
        public const string EnquiryTimedOut = "Enquiry timed out";
        public const string FrameNotAcknowledged = "Frame not acknowledged";

        private string nodeName;
        private string mtaName;
        private string reason;

        public NodeConnectionLostEventArgs()
        {
            this.nodeName = string.Empty;
            this.mtaName = string.Empty;
            this.reason = DefaultReason;
        }

        public NodeConnectionLostEventArgs(string nodeName, string mtaName, string reason)
        {
            this.nodeName = nodeName;
            this.mtaName = mtaName;
            this.reason = reason;
        }

        public NodeConnectionLostEventArgs(string nodeName, string mtaName)
        {
            this.nodeName = nodeName;
            this.mtaName = mtaName;
            this.reason = DefaultReason;
        }

        public string NodeName
        {
            get { return this.nodeName; }
        }

        public string MtaName
        {
            get { return this.mtaName; }
        }
        
        public string Reason
        {
            get { return this.reason; }
        }
    }
}

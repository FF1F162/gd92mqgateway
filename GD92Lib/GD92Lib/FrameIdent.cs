﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Frame identifier struct comprising sender address and sequence number.
    /// Implements IEquatable as it is used as a dictionary key (might have been easier to use the string representation?).
    /// </summary>
    internal struct FrameIdent : IEquatable<FrameIdent>
    {
        private readonly PortAddress senderAddress;
        private readonly short sequence;

        /// <summary>
        /// Initialises a new instance of the <see cref="FrameIdent"/> struct.
        /// </summary>
        /// <param name="senderAddress">Address from the source field of the frame.</param>
        /// <param name="sequence">GD92 sequence number.</param>
        public FrameIdent(PortAddress senderAddress, int sequence)
        {
            this.senderAddress = senderAddress;
            this.sequence = FrameIdent.ValidateSequence(sequence);
        }

        /// <summary>
        /// Address from the source field of the frame.
        /// </summary>
        /// <value>GD92 address.</value>
        internal PortAddress SenderAddress
        {
            get
            {
                return this.senderAddress;
            }
        }

        /// <summary>
        /// GD92 sequence number.
        /// </summary>
        /// <value>Sequence number.</value>
        internal int Sequence
        {
            get
            {
                return this.sequence;
            }
        }

        /// <summary>
        /// Implement == operator.
        /// </summary>
        /// <param name="a">First to compare.</param>
        /// <param name="b">Second to compare.</param>
        /// <returns>True if objects are equal.</returns>
        public static bool operator ==(FrameIdent a, FrameIdent b)
        {
            return a.Equals(b);
        }

        /// <summary>
        /// Implement != operator.
        /// </summary>
        /// <param name="a">First to compare.</param>
        /// <param name="b">Second to compare.</param>
        /// <returns>True if objects are not equal.</returns>
        public static bool operator !=(FrameIdent a, FrameIdent b)
        {
            return !a.Equals(b);
        }

        /// <summary>
        /// Override Equals(object) as part of IEquatableT. See also GD92Address, PortAddress and NodeAddress.
        /// Check for null and different type then cast to type and call the type-specific Equals method.
        /// </summary>
        /// <param name="obj">Object to be compared with this.</param>
        /// <returns>True if the object equals this.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            var pa = (FrameIdent)obj;
            return this.Equals(pa);
        }

        /// <summary>
        /// Override Equals(type) as part of IEquatableT. See also PortAddress and NodeAddress.
        /// Check all fields are the same.
        /// </summary>
        /// <param name="other">A FrameIdent.</param>
        /// <returns>True if all fields are the same.</returns>
        public bool Equals(FrameIdent other)
        {
            return (this.senderAddress == other.SenderAddress) && (this.sequence == other.Sequence);
        }

        /// <summary>
        /// Combine node and sequence number.
        /// </summary>
        /// <returns>Node number shifted plus sequence.</returns>
        public override int GetHashCode()
        {
            return (this.senderAddress.Node << 15) + this.sequence;
        }

        /// <summary>
        /// String comprises Brigade Node Port Sequence.
        /// </summary>
        /// <returns>Brigade Node Port Sequence.</returns>
        public override string ToString()
        {
            return string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "{0} {1}",
                this.senderAddress.ToString(),
                this.sequence.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Check that the sequence number is within GD-92 limits (and fits in a short).
        /// </summary>
        /// <param name="sequence">Number to be validated.</param>
        /// <returns>Valid short integer.</returns>
        internal static short ValidateSequence(int sequence)
        {
            return Frame.ValidateSequenceNumber(sequence);
        }
    }
}

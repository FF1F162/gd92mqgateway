﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Problem when decoding from received bytes to frame or message.
    /// </summary>
    [Serializable]
    public class GD92LibDecoderException : Exception, ISerializable
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibDecoderException"/> class.
        /// </summary>
        public GD92LibDecoderException()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibDecoderException"/> class.
        /// </summary>
        /// <param name="message">Message text.</param>
         public GD92LibDecoderException(string message) 
             : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibDecoderException"/> class.
        /// </summary>
        /// <param name="message">Message text.</param>
        /// <param name="innerException">Inner exception.</param>
        public GD92LibDecoderException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibDecoderException"/> class.
        /// This constructor is needed for serialization.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        protected GD92LibDecoderException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }
    }
}

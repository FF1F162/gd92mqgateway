﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Problem when encoding message into frame(s).
    /// </summary>
    [Serializable]
    public sealed class GD92LibEncoderException : Exception
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibEncoderException"/> class.
        /// </summary>
        public GD92LibEncoderException()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibEncoderException"/> class.
        /// </summary>
        /// <param name="message">Message text.</param>
        public GD92LibEncoderException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibEncoderException"/> class.
        /// </summary>
        /// <param name="message">Message text.</param>
        /// <param name="innerException">Inner exception.</param>
        public GD92LibEncoderException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibEncoderException"/> class.
        /// This constructor is needed for serialization.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        private GD92LibEncoderException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Problem in the GD92 Library.
    /// </summary>
    [Serializable]
    public sealed class GD92LibException : Exception
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibException"/> class.
        /// </summary>
        public GD92LibException()
            : base()
        {
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibException"/> class.
        /// </summary>
        /// <param name="message">Name of field being changed.</param>
        public GD92LibException(string message)
            : base(message)
        {
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibException"/> class.
        /// </summary>
        /// <param name="message">Message text.</param>
        /// <param name="innerException">Inner exception.</param>
        public GD92LibException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92LibException"/> class.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        private GD92LibException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

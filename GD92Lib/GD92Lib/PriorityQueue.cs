﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Comprises four queues holding frames queued for transmission on a bearer.
    /// A frame is added to the queue according to the message priority 
    /// e.g. 1 mobilise, 3 resource status.
    /// </summary>
    internal class PriorityQueue
    {
        private readonly string name;
        private readonly object queueLock;
        private readonly Queue<Frame> sendQueue1; // Priority 1 highest
        private readonly Queue<Frame> sendQueue2;
        private readonly Queue<Frame> sendQueue3;
        private readonly Queue<Frame> sendQueue4; // Priority 4 and lower

        /// <summary>
        /// Initialises a new instance of the <see cref="PriorityQueue"/> class.
        /// </summary>
        /// <param name="name">Queue name.</param>
        internal PriorityQueue(string name)
        {
            this.name = name;
            this.queueLock = new object();
            this.sendQueue1 = new Queue<Frame>();
            this.sendQueue2 = new Queue<Frame>();
            this.sendQueue3 = new Queue<Frame>();
            this.sendQueue4 = new Queue<Frame>();
        }

        /// <summary>
        /// Name of bearer - required for debug and logging.
        /// </summary>
        internal string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Do not use the lock when changing this (for fear of deadlock).
        /// Allegedly, this is made thread-proof by a lock on the Bearer.
        /// </summary>
        internal bool Connected { get; set; }

        /// <summary>
        /// Lock and add the frame to the priority queue.
        /// </summary>
        /// <param name="frame">Frame to be queued for transmission.</param>
        /// <returns>Result - Success or NoSocketConnection.</returns>
        internal ControlResult AddFrame(Frame frame)
        {
            ControlResult result = ControlResult.NoSocketConnection;
            lock (this.queueLock)
            {
                if (this.Connected)
                {
                    int priority = frame.Priority;
                    System.Diagnostics.Debug.Assert(priority > 0, "Priority must be 1 or greater");
                    switch (priority)
                    {
                        case 1:
                            this.sendQueue1.Enqueue(frame);
                            result = ControlResult.Success;
                            break;
                        case 2:
                            this.sendQueue2.Enqueue(frame);
                            result = ControlResult.Success;
                            break;
                        case 3:
                            this.sendQueue3.Enqueue(frame);
                            result = ControlResult.Success;
                            break;
                        default:
                            this.sendQueue4.Enqueue(frame);
                            result = ControlResult.Success;
                            break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Lock the priority queues and if connected get the highest priority frame.
        /// If not connected, the frames are about to be re-routed.
        /// </summary>
        /// <returns>Frame or null.</returns>
        internal Frame GetHighestPriorityFrame()
        {
            Frame frame = null;
            lock (this.queueLock)
            {
                // connected could probably be tested outside the lock as it is set outside the lock.
                if (this.Connected)
                {
                    frame = DequeueIfFrameIsNull(this.sendQueue1, frame);
                    frame = DequeueIfFrameIsNull(this.sendQueue2, frame);
                    frame = DequeueIfFrameIsNull(this.sendQueue3, frame);
                    frame = DequeueIfFrameIsNull(this.sendQueue4, frame);
                }
            }

            return frame;
        }

        /// <summary>
        /// Lock the priority queues and get the highest priority frame.
        /// </summary>
        /// <returns>Frame or null.</returns>
        internal Frame GetFrameToReRoute()
        {
            Frame frame = null;
            lock (this.queueLock)
            {
                frame = DequeueIfFrameIsNull(this.sendQueue1, frame);
                frame = DequeueIfFrameIsNull(this.sendQueue2, frame);
                frame = DequeueIfFrameIsNull(this.sendQueue3, frame);
                frame = DequeueIfFrameIsNull(this.sendQueue4, frame);
            }

            return frame;
        }

        /// <summary>
        /// Count all frames queued for transmission.
        /// </summary>
        /// <returns>Number of frames.</returns>
        internal int GetCount()
        {
            int count = 0;
            lock (this.queueLock)
            {
                count += this.sendQueue1.Count;
                count += this.sendQueue2.Count;
                count += this.sendQueue3.Count;
                count += this.sendQueue4.Count;
            }

            return count;
        }

        /// <summary>
        /// If there is no frame already dequeued, return the first frame from this queue or null if it is empty.
        /// </summary>
        /// <param name="queue">Target queue.</param>
        /// <param name="frame">Frame already dequeue or null.</param>
        /// <returns>Frame or null.</returns>
        private static Frame DequeueIfFrameIsNull(Queue<Frame> queue, Frame frame)
        {
            if (frame == null)
            {
                if (queue.Count > 0)
                {
                    frame = queue.Dequeue();
                }
            }

            return frame;
        }
    }
}

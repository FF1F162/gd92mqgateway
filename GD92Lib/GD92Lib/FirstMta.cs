﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Threading;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Singleton class to control restart of the gateway after a short interval, to give Storm a chance to reconnect
    /// avoiding the need to restart the gateway automatically.
    /// The timer starts when the node configured as FirstMta drops its connection and is cancelled if the node reconnects.
    /// The restart interval is short (1 second) if connection closed because Storm is unresponsive (no reply to ENQs).
    /// </summary>
    internal sealed class FirstMta : IDisposable
    {
        public const int DefaultFirstMtaTimeoutInSeconds = 30;
        public const int ShortFirstMtaTimeoutInSeconds = 1;

        private static readonly FirstMta Self = new FirstMta();
        private readonly string logName; 
        private readonly object timerLock;

        private Timer restartTimer;
        private NodeConnectionLostEventArgs connectionLostEventArgs;

        /// <summary>
        /// Prevents a default instance of the <see cref="FirstMta"/> class from being created.
        /// </summary>
        private FirstMta()
        {
            this.logName = "FirstMta";
            this.timerLock = new object();
            this.connectionLostEventArgs = new NodeConnectionLostEventArgs();
        }

        /// <summary>
        /// Provide access to members of the singleton.
        /// </summary>
        /// <value>Singleton instance.</value>
        internal static FirstMta Instance
        {
            get
            {
                return Self;
            }
        }

        /// <summary>
        /// Call Dispose for the timer and set reference null.
        /// </summary>
        public void Dispose()
        {
            if (this.restartTimer != null)
            {
                this.restartTimer.Dispose();
                this.restartTimer = null;
            }
        }

        /// <summary>
        /// Set the restart timer and store details of the event for logging purposes.
        /// Restart immediately if the connection to Storm has timed out (no response to enquiries).
        /// </summary>
        /// <param name="eventArgs">Details of connection lost event.</param>
        internal void ScheduleRestart(NodeConnectionLostEventArgs eventArgs)
        {
            lock (this.timerLock)
            {
                if (this.restartTimer == null)
                {
                    int timeout = DetermineRestartTimeoutInMilliseconds(eventArgs);
                    this.connectionLostEventArgs = eventArgs;
                    this.restartTimer = new Timer(this.ProcessRestart, null, timeout, 0);
                    EventControl.Info(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Restart scheduled in {0} milliseconds",
                        timeout));
                }
                else
                {
                    EventControl.Warn(this.logName, "Restart timer already set");
                }
            }
        }

        /// <summary>
        /// Cancel the restart timer and reset stored event arguments and the reference to the timer.
        /// The restart timer is not set at first connection after start up or restart.
        /// </summary>
        /// <param name="eventArgs">Details of connection event.</param>
        /// <returns>True if there was a restart scheduled.</returns>
        internal bool CancelRestart(NodeConnectedEventArgs eventArgs)
        {
            bool restartCancelled = false;
            if (this.connectionLostEventArgs.MtaName.Length > 0)
            {
                System.Diagnostics.Debug.Assert(this.connectionLostEventArgs.MtaName.Equals(eventArgs.MtaName, StringComparison.Ordinal), "Name of MTA should be the same");
                System.Diagnostics.Debug.Assert(this.connectionLostEventArgs.NodeName.Equals(eventArgs.NodeName, StringComparison.Ordinal), "Name of node should be the same");
                System.Diagnostics.Debug.Assert(this.restartTimer != null, "If event arguments are stored, timer is expected to be set");
            }

            restartCancelled = this.CancelRestart();
            return restartCancelled;
        }

        /// <summary>
        /// Cancel the restart timer and reset stored event arguments and the reference to the timer.
        /// The restart timer is not set at first connection after start up or restart.
        /// </summary>
        /// <returns>True if there was a restart scheduled.</returns>
        internal bool CancelRestart()
        {
            bool restartCancelled = false;
            lock (this.timerLock)
            {
                if (this.restartTimer != null)
                {
                    this.restartTimer.Dispose();
                    this.ResetTimerFields();
                    EventControl.Info(this.logName, "Restart cancelled");
                    restartCancelled = true;
                }
                else
                {
                    EventControl.Info(this.logName, "Restart is not scheduled");
                }
            }

            return restartCancelled;
        }

        /// <summary>
        /// Timout depends on the reason for losing connection.
        /// Restart should not be delayed further if Storm has already been timed out for lack of response to ENQ.
        /// </summary>
        /// <param name="eventArgs">Arguments including reason for losing the connection.</param>
        /// <returns>Timeout in milliseconds.</returns>
        private static int DetermineRestartTimeoutInMilliseconds(NodeConnectionLostEventArgs eventArgs)
        {
            int timeoutInSeconds = DefaultFirstMtaTimeoutInSeconds;
            if (eventArgs.Reason.Equals(NodeConnectionLostEventArgs.EnquiryTimedOut, StringComparison.Ordinal))
            {
                timeoutInSeconds = ShortFirstMtaTimeoutInSeconds;
            }
            else
            {
                timeoutInSeconds = UserAgent.Instance.GetFirstMtaTimeout();
            }

            return (int)TimeSpan.FromSeconds(timeoutInSeconds).TotalMilliseconds;
        }

        /// <summary>
        /// Shut down and re-activate the GD92 component.
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ProcessRestart(object state)
        {
            lock (this.timerLock)
            {
                if (this.restartTimer != null)
                {
                    EventControl.Note(this.logName, "Restarting after no reconnection within the timeout");
                    GD92Component.Instance.Reactivate();
                    if (GD92Component.Instance.Activated)
                    {
                        this.WriteReactivateEventForExternalMonitoring();
                    }
                }

                this.ResetTimerFields();
            }
        }

        /// <summary>
        /// Write a warning in the Windows Application event log to be picked up by Nagios.
        /// </summary>
        private void WriteReactivateEventForExternalMonitoring()
        {
            string eventText = string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Gateway {0} reactivated after connection to {1} {2} closed - {3}",
                        GD92Component.LocalNodeName,
                        this.connectionLostEventArgs.NodeName,
                        this.connectionLostEventArgs.MtaName,
                        this.connectionLostEventArgs.Reason);
            GD92Component.Instance.EventLogger.WriteEvent(eventText);
        }

        /// <summary>
        /// Clear the timer reference and stored event details.
        /// </summary>
        private void ResetTimerFields()
        {
            this.restartTimer = null;
            this.connectionLostEventArgs = new NodeConnectionLostEventArgs();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using Microsoft.Win32;
    using Thales.KentFire.EventLib;

    internal class SerialNumberProvider : ISerialNumberProvider
    {
        private const string RegistryKeySerialNumber = "SerialNumber";
        private const string LogName = "SerialNumberProvider";
        private static readonly object Lock = new object();
        
        /// <summary>
        /// Initialises a new instance of the <see cref="SerialNumberProvider"/> class.
        /// </summary>
        public SerialNumberProvider()
        {
        }

        /// <summary>
        /// Get a new serial number: read the previous number from the Registry, increment it
        /// and write it back to the Registry.
        /// Ensure that serial number is within range or reset to 1 (not 0).
        /// </summary>
        /// <returns>New serial number.</returns>
        public int GetSerialNumber()
        {
            int serialNumber = GD92Component.StatusFail;
            lock (Lock)
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(
                    UserAgent.Instance.RegistryKeyPrivate, 
                    true /* writable */);
                if (rk != null)
                {
                    object value = rk.GetValue(RegistryKeySerialNumber);
                    if (value != null)
                    {
                        serialNumber = (int)value;
                        if (serialNumber >= GD92Component.MaxSerialNumber || serialNumber < 0)
                        {
                            serialNumber = 0;
                        }
                        
                        serialNumber++;
                        rk.SetValue(RegistryKeySerialNumber, serialNumber);
                    }
                }
            }
            
            if (serialNumber == GD92Component.StatusFail)
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Registry access failed for key {0}\\{1}",
                    UserAgent.Instance.RegistryKeyPrivate,
                    RegistryKeySerialNumber));
            }
            
            return serialNumber;
        }
    }
}

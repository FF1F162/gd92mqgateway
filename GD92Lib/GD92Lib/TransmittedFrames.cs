﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// This class is concerned mostly with reply processing (UserAgent calls DoReplyProcessing).
    /// It contains a dictionary of all frames transmitted by the User Agent, keyed by GD-92 sequence number. This is populated
    /// by calls to TransmitFrame. If the frame requires acknowledgement, it is added to the dictionary before being passed to
    /// a bearer for transmission (if possible).
    /// The last stage in reply processing is to get the next frame (if any) from the TransmitQueue for the destination node
    /// and call TransmitFrame.
    /// </summary>
    internal class TransmittedFrames
    {
        private const string LogName = "TransmittedFrames";
        private readonly object transmittedFramesLock;
        private readonly Dictionary<int, Frame> transmittedBySeq;

        /// <summary>
        /// Initialises a new instance of the <see cref="TransmittedFrames"/> class.
        /// </summary>
        internal TransmittedFrames()
        {
            this.transmittedFramesLock = new object();
            this.transmittedBySeq = new Dictionary<int, Frame>();
        }

        /// <summary>
        /// Log transmission then add to the next bearer queue, if any.
        /// </summary>
        /// <param name="frame">Frame for transmission.</param>
        internal static void TransmitFrame(Frame frame)
        {
            LogTransmission(frame);
            frame.QueueForNextBearer();
        }

        /// <summary>
        /// Lock. Count the number of frames in the dictionary.
        /// </summary>
        /// <returns>Number of frames awaiting acknowledgement.</returns>
        internal int GetTransmittedCount()
        {
            int count = 0;
            lock (this.transmittedFramesLock)
            {
                count = this.transmittedBySeq.Count;
            }

            return count;
        }

        /// <summary>
        /// Lock. Cancel the transmit timer in each frame then clear the dictionary.
        /// </summary>
        internal void Clear()
        {
            lock (this.transmittedFramesLock)
            {
                foreach (KeyValuePair<int, Frame> kvp in this.transmittedBySeq)
                {
                    kvp.Value.CancelTransmitTimer();
                }

                this.transmittedBySeq.Clear();
            }
        }

        /// <summary>
        /// Add to the dictionary of transmitted messages only if reply is anticipated.
        /// Set timer in case there is no acknowledgement.
        /// </summary>
        /// <param name="frame">Frame for transmission.</param>
        internal void AddForAcknowledgement(Frame frame)
        {
            if (frame.AckRequired || frame.ReplyExpected)
            {
                this.AddFrameAndSetTimer(frame);
            }
        }
        
        /// <summary>
        /// Lock. Return true if there is a transmitted frame with this sequence number
        /// and the source address matches the destination of the received frame.
        /// </summary>
        /// <param name="receivedFrame">Frame (acknowledgement) to be matched with transmitted frame.</param>
        /// <returns>True if there is a matching frame.</returns>
        internal bool ContainsMatchingFrame(Frame receivedFrame)
        {
            bool frameFound = false;
            lock (this.transmittedFramesLock)
            {
                Frame transmittedFrame = this.GetFrame(receivedFrame.SequenceNumber);
                if (transmittedFrame != null && receivedFrame.Destination == transmittedFrame.Source)
                {
                    frameFound = true;
                }
            }

            return frameFound;
        }

        /// <summary>
        /// Match the reply to the original frame and process accordingly.
        /// </summary>
        /// <param name="replyFrame">Frame comprising the reply.</param>
        /// <param name="reply">Message constructed from frame.</param>
        /// <returns>Event is required to notify the host application.</returns>
        internal bool DoReplyProcessing(Frame replyFrame, GD92Message reply)
        {
            bool eventRequired = false;
            bool discardRequired = false;
            bool waitAck = false;
            bool resendFrame = false;
            bool extendTimeout = false;
            Frame sentFrame = null;
            lock (this.transmittedFramesLock)
            {
                sentFrame = this.GetAcknowledgedFrame(replyFrame);
                if (sentFrame != null)
                {
                    SetOriginalMessageInReply(replyFrame, reply, sentFrame);
                    GetReplyInfo(reply, out waitAck, out resendFrame, out extendTimeout);
                    if (waitAck == false && resendFrame == false && extendTimeout == false)
                    {
                        discardRequired = NakForMultiFrame(reply, sentFrame);
                        this.RemoveFrameOrMessageFrames(discardRequired, sentFrame);
                        eventRequired = CheckIfEventIsRequired(reply, sentFrame);
                    }
                    else
                    {
                        if (resendFrame)
                        {
                            sentFrame.ResetNextRoute();
                        }

                        sentFrame.CancelTransmitTimer();
                        
                        // Further processing to reset the timer must be done outside the current lock.
                        // In order to reset the timer we need to get the ManTimeout from the node data.
                        // This requires a lock on the list of nodes.
                    }
                    
                    if (waitAck && UserAgent.Instance.GetGatewayNode() != null)
                    {
                        // notify the gateway so it can forward the wait ack.
                        eventRequired = true;
                        System.Diagnostics.Debug.Assert(!sentFrame.Message.ReplyEventRaised, "This flag needs to remain false until the Ack arrives");
                    }
                }
            }
            
            // do this outside the TransmittedFrames lock
            if (sentFrame != null)
            {
                Node node = Router.Instance.GetNodeByNameOrDefault(sentFrame.DestinationName); // lock node list
                if (waitAck || extendTimeout)
                {
                    // Extend the timeout of the frame to allow for manual acknowledgement
                    this.ResetTransmitTimeout(sentFrame, node.ManTimeout); // lock to change the timeout
                }

                if (resendFrame)
                {
                    this.ResetTransmitTimeout(sentFrame, UserAgent.Instance.NoAckTimeout); // lock to change the timeout
                    TransmitFrame(sentFrame);
                }
                else if (!sentFrame.Acknowledged)
                {
                    Frame nextFrameToSend = node.TxQueue.GetNextFrameToSend(sentFrame, discardRequired);
                    sentFrame.Acknowledged = true; // No longer queued pending acknowledgement
                    if (nextFrameToSend != null)
                    {
                        this.AddForAcknowledgement(nextFrameToSend); // lock to Add the frame
                        TransmitFrame(nextFrameToSend); 
                    }
                 }
            }
            
            return eventRequired;
        }
        
        /// <summary>
        /// Log frame identity and destination and byte representation.
        /// </summary>
        /// <param name="frame">Frame to be transmitted.</param>
        private static void LogTransmission(Frame frame)
        {
            EventControl.Info(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Routing frame {0} of {1} bytes to {2}{3}{4}",
                frame.Ident.ToString(),
                frame.Length,
                frame.Destination.ToString(),
                Environment.NewLine,
                BitConverter.ToString(frame.FrameBytes, 0, frame.Length)));
        }

        /// <summary>
        /// Check if the reply is Nak (wait_ack) or Nak (check_error).
        /// For timeout (code not in GD92), check if the timeout should be extended in case acknowledgement is delayed
        /// beyond the initial timeout (NoAckTimeout).
        /// </summary>
        /// <param name="reply">Reply message.</param>
        /// <param name="waitAck">True for wait ack.</param>
        /// <param name="resend">True for check error.</param>
        /// <param name="extendTimeout">True for initial timeout.</param>
        private static void GetReplyInfo(GD92Message reply, out bool waitAck, out bool resend, out bool extendTimeout)
        {
            waitAck = false;
            resend = false;
            extendTimeout = false;
            if (reply.MsgType == GD92MsgType.Nak)
            {
                var nak = reply as GD92Msg51;
                if (nak.ReasonCodeSet == (int)GD92ReasonCodeSet.General &&
                    nak.ReasonCode == (int)GD92GeneralReasonCode.WaitAcknowledgement)
                {
                    waitAck = true;
                }

                if (nak.ReasonCodeSet == (int)GD92ReasonCodeSet.General &&
                    nak.ReasonCode == (int)GD92GeneralReasonCode.CheckError)
                {
                    resend = true;
                }

                if (nak.ReasonCodeSet == (int)GD92ReasonCodeSet.General &&
                    nak.ReasonCode == (int)GD92GeneralReasonCode.TransmissionTimedOut)
                {
                    // initialTimeLimit must be in the future just after the first timeout (typically 20sec) matures
                    // and in the past after the second timeout (typically 300sec) matures.
                    int betweenFirstAndSecondTimeouts = 2 * UserAgent.Instance.NoAckTimeout;
                    DateTime initialTimeLimit = reply.OriginalMessage.SentAt.AddSeconds(betweenFirstAndSecondTimeouts);
                    if (DateTime.UtcNow < initialTimeLimit)
                    {
                        extendTimeout = true;
                    }
                }
            }
        }

        private static bool CheckIfEventIsRequired(GD92Message reply, Frame sentFrame)
        {
            bool eventRequired = true;
            if (sentFrame.Message.ReplyEventRaised == true)
            {
                eventRequired = false; // do not raise another event
            }
            else if (reply.MsgType == GD92MsgType.Ack)
            {
                if (sentFrame.Block != sentFrame.OfBlocks)
                {
                    // only raise an event for the last block - and this is not it
                    eventRequired = false;
                }

                // This may need to be more sophisticated - e.g. check for frames remaining in the
                // transmitted list.
                // As it is, the last block could be sent and acknowledged while two others
                // remain in flight.
            }

            // There is no event for WaitAck either - this has already been tested
            if (eventRequired)
            {
                sentFrame.Message.ReplyEventRaised = true;
            }

            return eventRequired;
        }

        /// <summary>
        /// Return true if the reply is a Nak and the frame is not the last.
        /// </summary>
        /// <param name="reply">Reply message.</param>
        /// <param name="sentFrame">Frame being acknowledged.</param>
        /// <returns>True if the reply is a Nak and the frame is not the last.</returns>
        private static bool NakForMultiFrame(GD92Message reply, Frame sentFrame)
        {
            return sentFrame.OfBlocks > 1 &&
                sentFrame.OfBlocks != sentFrame.Block && 
                reply.MsgType == GD92MsgType.Nak;
        }

        /// <summary>
        /// Check references then copy the reference from the sent frame to the OriginalMessage reference in the reply message.
        /// </summary>
        /// <param name="replyFrame">Reply frame (to check message reference refers to reply).</param>
        /// <param name="reply">Reply message.</param>
        /// <param name="sentFrame">Transmitted frame.</param>
        private static void SetOriginalMessageInReply(Frame replyFrame, GD92Message reply, Frame sentFrame)
        {
            System.Diagnostics.Debug.Assert(
                reply.OriginalMessage == null || object.ReferenceEquals(reply.OriginalMessage, sentFrame.Message),
                "Reference to original message in the reply must be null or refer to the message the reply is acknowledging");
            System.Diagnostics.Debug.Assert(
                object.ReferenceEquals(replyFrame.Message, reply),
                 "The message reference in the reply frame is expected to refer to the reply message");
            reply.OriginalMessage = sentFrame.Message;
        }

        /// <summary>
        /// Log Info or Warning depending on whether or not an acknowledged frame is expected.
        /// If there is a Nak for any frame in a multi-block message all frames are discarded. 
        /// Assume that this is always the reason if there is no acknowledged frame for a Nak.
        /// </summary>
        /// <param name="replyFrame">Reply frame.</param>
        private static void LogAcknowledgedFrameNotFound(Frame replyFrame)
        {
            if (replyFrame.MessageType == GD92MsgType.Nak)
            {
                EventControl.Info(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} received from {1} - no matching frame, assume discard",
                    replyFrame.MessageType.ToString(),
                    replyFrame.Ident.ToString()));
            }
            else
            {
                EventControl.Warn(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} received from {1} - no matching frame",
                    replyFrame.MessageType.ToString(),
                    replyFrame.Ident.ToString()));
            }
        }

        /// <summary>
        /// Remove acknowledged frame. Remove all frames in the message if discard is required.
        /// </summary>
        /// <param name="discardRequired">Remove all frames comprising message.</param>
        /// <param name="sentFrame">Acknowledged frame.</param>
        private void RemoveFrameOrMessageFrames(bool discardRequired, Frame sentFrame)
        {
            if (discardRequired)
            {
                this.RemoveAllTransmittedFramesForMessage(sentFrame);
            }
            else
            {
                this.RemoveTransmittedFrame(sentFrame);
            }
        }

        /// <summary>
        /// Add the frame to the dictionary and set the acknowledgement timer in the frame.
        /// </summary>
        /// <param name="frame">Frame expecting reply.</param>
        private void AddFrameAndSetTimer(Frame frame)
        {
            lock (this.transmittedFramesLock)
            {
                this.transmittedBySeq.Add(frame.SequenceNumber, frame);
                frame.SetTransmitTimer(UserAgent.Instance.NoAckTimeout);
            }
        }

        /// <summary>
        /// Lock. Set the new timeout.
        /// </summary>
        /// <param name="frame">Target frame.</param>
        /// <param name="timeout">Timout in seconds.</param>
        private void ResetTransmitTimeout(Frame frame, int timeout)
        {
            lock (this.transmittedFramesLock)
            {
                // assume previous timer cancelled (done in CheckWaitAck)
                frame.SetTransmitTimer(timeout);
                EventControl.Debug(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Frame {0} timeout reset to {1} secs",
                    frame.Ident.ToString(),
                    timeout.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }
        }

        /// <summary>
        /// Return reference to frame in the dictionary of transmitted frames.
        /// </summary>
        /// <param name="sequenceNumberKey">Key to dictionary.</param>
        /// <returns>Frame identified by sequence number or null.</returns>
        private Frame GetFrame(int sequenceNumberKey)
        {
            Frame sentFrame = null;
            bool frameFound = this.transmittedBySeq.ContainsKey(sequenceNumberKey);
            if (frameFound)
            {
                sentFrame = this.transmittedBySeq[sequenceNumberKey];
            }

            return sentFrame;
        }

        /// <summary>
        /// Use the sequence number to find the frame and remove it from the dictionary.
        /// Cancel the transmit timer in the frame.
        /// </summary>
        /// <param name="sentFrame">Frame to be removed.</param>
        private void RemoveTransmittedFrame(Frame sentFrame)
        {
            Frame removedFrame = this.transmittedBySeq[sentFrame.SequenceNumber];
            System.Diagnostics.Debug.Assert(object.ReferenceEquals(sentFrame, removedFrame), "Frames must be the same");
            this.transmittedBySeq.Remove(sentFrame.SequenceNumber);
            sentFrame.CancelTransmitTimer();
            EventControl.Debug(LogName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Frame {0} removed",
                removedFrame.Ident.ToString()));
        }

        /// <summary>
        /// Use the sequence number provider to find the numbers for all the frames in the message.
        /// Remove them from the dictionary and cancel their timers.
        /// </summary>
        /// <param name="sentFrame">Frame in message to be removed.</param>
        private void RemoveAllTransmittedFramesForMessage(Frame sentFrame)
        {
            int[] frameNumberArray = GD92SequenceProvider.GetFrameNumbers(
                sentFrame.SequenceNumber,
                sentFrame.Block,
                sentFrame.OfBlocks);
            for (int index = 0; index < sentFrame.OfBlocks; index++)
            {
                int sequenceNumber = frameNumberArray[index];
                Frame removedFrame = this.GetFrame(sequenceNumber);
                if (removedFrame != null)
                {
                    this.RemoveTransmittedFrame(removedFrame);
                }
            }
        }

        /// <summary>
        /// Use the sequence number of a reply to find the original message in the
        /// dictionary of transmitted frames. Return null if not found. Also return null if the original message
        /// is type 5 and the reply is type 50 and this is a gateway - Storm sends Ack as well as Msg20
        /// if Msg5 has ack_req set - in this case ignore the type 50 and wait for type 20. Note that this does
        /// not always work as Storm sometimes sends the Ack AFTER Msg20, in which case the Ack is unmatched (Warning).
        /// RSC emulator may send station request (station instead of callsign) for which gateway sends Ack
        /// followed by (apparently unsolicited) Msg20 from Storm for all the callsigns at the station.
        /// </summary>
        /// <param name="replyFrame">Reply frame.</param>
        /// <returns>Original frame or null.</returns>
        private Frame GetAcknowledgedFrame(Frame replyFrame)
        {
            Frame sentFrame = this.GetFrame(replyFrame.SequenceNumber);
            if (sentFrame == null)
            {
                LogAcknowledgedFrameNotFound(replyFrame);
            }
            else if (sentFrame.MessageType == GD92MsgType.ResourceStatusRequest && 
                     replyFrame.MessageType == GD92MsgType.Ack &&
                     UserAgent.Instance.GetGatewayNode() != null)
            {
                EventControl.Info(LogName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Ignoring {0} received for {1} - ResourceStatus expected",
                    replyFrame.MessageType.ToString(),
                    sentFrame.Ident.ToString()));
                sentFrame = null;
            }
            
            return sentFrame;
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Provide frame data for use during message encoding.
    /// </summary>
    internal interface IFrameProvider
    {
        Frame[] SetUpFramesWithNewSequenceNumbers(int numberRequiredForMessage);

        Frame[] SetUpFrameForAcknowledgement(int messageSerialNumber);
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Description of IGD92MessageSender.
    /// </summary>
    public interface IGD92MessageSender
    {
        int SendMessage(GD92Message message);
    }
}

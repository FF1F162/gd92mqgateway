﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.SocketLib;
    using System.IO;

    /// <summary>
    /// Represents Bearers of a particular type, with similar configuration, read from the Registry
    /// Acts as TCP server, listening for connnect requests. After connection the socket is passed to the Bearer for the node.
    /// </summary>
    /// <description>
    /// TCP MTA requires a single listening socket for the bearer collection
    /// (note that bearers also try to establish their connection by calling the remote node)
    /// The Activate method calls StartListening to set up the listening socket and bind it to
    /// the IPEndPoint. This remains in place until the Deactivate method closes the socket.
    /// StartListening calls Listen which calls BeginAccept. The callback method identifies the
    /// client, passes the new socket to the Bearer and starts the Bearer reading. It finishes by
    /// calling Listen again to continue the process. When Deactivate closes the socket, the
    /// callback method gets called and there is a NullReferenceException to end the process.
    /// This avoids the synchronisation difficulties associated with a dedicated thread or timer.
    /// </description>
    internal class Mta : IDisposable
    {
        private readonly Dictionary<string, Bearer> bearersByNodeName;
        private readonly Dictionary<IPAddress, Bearer> bearersByIPAddress;

        private readonly object mtaLock;
        private readonly string mtaName;
        private bool activated;
        private int ackTimeout;
        private int connectionInterval;
        private int enquiryInterval;
        private AsciiChar frameAckCharExpected;
        private AsciiChar frameAckCharSent;
        private int listeningPort;
        private int maxAckPending;
        private bool useDatagrams;
        private bool useMQ;
        private IPEndPoint listeningEndPoint;
        private Socket listeningSocket;

        /// <summary>
        /// Initialises a new instance of the <see cref="Mta"/> class.
        /// </summary>
        /// <param name="mtaName">Name of MTA.</param>
        internal Mta(string mtaName)
        {
            this.mtaLock = new object();
            this.mtaName = mtaName;
            this.bearersByNodeName = new Dictionary<string, Bearer>();
            this.bearersByIPAddress = new Dictionary<IPAddress, Bearer>();

            // Set defaults for values read from Registry
            this.ackTimeout = 2;
            this.connectionInterval = 0; // no connection attempts
            this.enquiryInterval = 0; // no enquiries
            this.frameAckCharExpected = AsciiChar.BEL;
            this.frameAckCharSent = AsciiChar.BEL;
            this.listeningPort = 17400;
            this.maxAckPending = 3;
            this.useDatagrams = false;
            this.useMQ = false;
        }

        /// <summary>
        /// Dispose of the listening socket.
        /// </summary>
        public void Dispose()
        {
            if (this.listeningSocket != null)
            {
                this.listeningSocket.Dispose();
                this.listeningSocket = null;
            }
        }

        #region Properties

        /// <summary>
        /// Name that identifies the type of MTA (group of bearers).
        /// </summary>
        internal string MtaName
        {
            get { return this.mtaName; }
        }

        /// <summary>
        /// True if the Activate method has been called.
        /// </summary>
        internal bool Activated
        {
            get { return this.activated; }
        }

        /// <summary>
        /// Acknowledgement timeout (following ENQ).
        /// </summary>
        internal int AckTimeout
        {
            get { return this.ackTimeout; }
        }

        /// <summary>
        /// Interval in seconds between connection attempts.
        /// </summary>
        internal int ConnectionInterval
        {
            get { return this.connectionInterval; }
        }

        /// <summary>
        /// Interval in seconds between sending ENQ.
        /// </summary>
        internal int EnquiryInterval
        {
            get { return this.enquiryInterval; }
        }

        /// <summary>
        /// IP port number for connecting bearers of this type.
        /// </summary>
        internal int ListeningPort
        {
            get { return this.listeningPort; }
        }

        /// <summary>
        /// Number of ENQ sent without reply (ACK) before bearer is closed as failed.
        /// </summary>
        internal int MaxAckPending
        {
            get { return this.maxAckPending; }
        }

        /// <summary>
        /// Use UDP (instead of TCP).
        /// </summary>
        internal bool UseDatagrams
        {
            get { return this.useDatagrams; }
        }

        internal bool UseMQ
        {
            get { return this.useMQ; }
        }


        /// <summary>
        /// Character expected to acknowledge a frame (aka block). 
        /// </summary>
        internal AsciiChar FrameAckCharExpected
        {
            get { return this.frameAckCharExpected; }
        }

        /// <summary>
        /// Character sent to acknowledge a frame (aka block). 
        /// </summary>
        internal AsciiChar FrameAckCharSent
        {
            get { return this.frameAckCharSent; }
        }
        
        /// <summary>
        /// Acknowledgement timeout after sending ENQ (and maybe after sending a frame, depending on configuration).
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetAckTimeout(object value)
        {
            if (value != null)
            {
                int ackTimeout = (int)value;
                if (ackTimeout > 1)
                {
                    this.ackTimeout = ackTimeout;
                }
                else
                {
                    EventControl.Warn(this.mtaName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "AckTimeout {0} unexpected - using default {1}",
                        ackTimeout.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        this.ackTimeout.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
            }
        }

        /// <summary>
        /// Interval between connection attempts (0 means never try to connect).
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetConnectionInterval(object value)
        {
            if (value != null)
            {
                int connectionInterval = (int)value;
                if (connectionInterval >= 0)
                {
                    this.connectionInterval = connectionInterval;
                }
                else
                {
                    EventControl.Warn(this.mtaName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "ConnectionInterval {0} unexpected - using default {1}",
                        connectionInterval.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        this.connectionInterval.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
            }
        }

        /// <summary>
        /// Interval between sending ENQ (0 means never send).
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetEnquiryInterval(object value)
        {
            if (value != null)
            {
                int enquiryInterval = (int)value;
                if (enquiryInterval >= 0)
                {
                    this.enquiryInterval = enquiryInterval;
                }
                else
                {
                    EventControl.Warn(this.mtaName, string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                        "EnquiryInterval {0} unexpected - using default {1}",
                        enquiryInterval.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        this.enquiryInterval.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
            }
        }

        /// <summary>
        /// Listening port.
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetListeningPort(object value)
        {
            if (value != null)
            {
                int listeningPort = (int)value;
                if (listeningPort >= 0)
                {
                    this.listeningPort = listeningPort;
                }
                else
                {
                    EventControl.Warn(this.mtaName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "ListeningPort {0} unexpected - using default {1}",
                        listeningPort.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        this.listeningPort.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
            }
        }

        /// <summary>
        /// Number of ENQ sent without reply (ACK) before bearer is closed as failed.
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetMaxAckPending(object value)
        {
            if (value != null)
            {
                int maxAckPending = (int)value;
                if (maxAckPending >= 0)
                {
                    this.maxAckPending = maxAckPending;
                }
                else
                {
                    EventControl.Warn(this.mtaName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "MaxAckPending {0} unexpected - using default {1}",
                        maxAckPending.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        this.maxAckPending.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
            }
        }

        /// <summary>
        /// Use UDP instead of TCP.
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetUseDatagrams(object value)
        {
            if (value != null)
            {
                int useDatagrams = (int)value;
                if (useDatagrams > 0)
                {
                    this.useDatagrams = true;
                }
                else if (useDatagrams != 0)
                {
                    EventControl.Warn(this.mtaName, string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                        "UseDatagrams {0} unexpected - using default {1}",
                        useDatagrams.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        0));
                }
            }
        }

        internal void SetUseMQ(object value)
        {
            if (value != null)
            {
                int tempUseMQ = (int)value;
                if (tempUseMQ > 0)
                {
                    this.useMQ = true;
                }
                else if (tempUseMQ != 0)
                {
                    EventControl.Warn(this.mtaName, string.Format(System.Globalization.CultureInfo.InvariantCulture,
                        "UseMQ {0} unexpected - using default {1}",
                        useMQ.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        0));
                }
            }
        }

        /// <summary>
        /// Character expected to acknowledge a frame (aka block). 
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetFrameAckCharExpected(object value)
        {
            if (value != null)
            {
                string frameAckChar = (string)value;
                this.frameAckCharExpected = this.AsciiCharFromNameOrDefault(frameAckChar, this.frameAckCharExpected);
            }
            else
            {
                EventControl.Warn(this.mtaName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "FrameAckCharExpected is not set - using default {0}",
                    this.frameAckCharExpected.ToString()));
            }

            EventControl.Info(this.mtaName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "FrameAckCharExpected is  {0}",
                this.frameAckCharExpected.ToString()));
        }

        /// <summary>
        /// Character sent to acknowledge a frame (aka block). 
        /// </summary>
        /// <param name="value">Read from the Registry.</param>
        internal void SetFrameAckCharSent(object value)
        {
            if (value != null)
            {
                string frameAckChar = (string)value;
                this.frameAckCharSent = this.AsciiCharFromNameOrDefault(frameAckChar, this.frameAckCharSent);
            }
            else
            {
                EventControl.Warn(this.mtaName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "FrameAckCharSent is not set - using default {0}",
                    this.frameAckCharSent.ToString()));
            }

            EventControl.Info(this.mtaName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "FrameAckCharSent is  {0}",
                this.frameAckCharSent.ToString()));
        }

        #endregion

        /// <summary>
        /// Add bearer to the collections held in the MTA.
        /// </summary>
        /// <param name="nodeName">Node name key.</param>
        /// <param name="bearer">Bearer to add to the collections.</param>
        internal void AddBearer(string nodeName, Bearer bearer)
        {
            lock (this.mtaLock)
            {
                this.bearersByNodeName.Add(nodeName, bearer);
                this.bearersByIPAddress.Add(bearer.ConnectionIPAddress, bearer);
            }
        }

        /// <summary>
        /// Returns a reference to the bearer with this name.
        /// </summary>
        /// <param name="nodeName">Node name.</param>
        /// <returns>Bearer with the given node name.</returns>
        internal Bearer GetBearer(string nodeName)
        {
            lock (this.mtaLock)
            {
                return this.bearersByNodeName[nodeName];
            }
        }

        /// <summary>
        /// Returns a reference to the bearer with this IP address.
        /// </summary>
        /// <param name="address">IP address.</param>
        /// <returns>Bearer with the given IP address.</returns>
        internal Bearer GetBearerByIPAddress(IPAddress address)
        {
            lock (this.mtaLock)
            {
                return this.bearersByIPAddress[address];
            }
        }

        /// <summary>
        /// Deactivate the bearers in this MTA and clear their references from the collections.
        /// Close any listening socket.
        /// </summary>
        internal void Deactivate()
        {
            lock (this.mtaLock)
            {
                foreach (KeyValuePair<string, Bearer> kvp in this.bearersByNodeName)
                {
                    kvp.Value.Deactivate();
                }

                this.bearersByNodeName.Clear();
                this.bearersByIPAddress.Clear();

                this.listeningEndPoint = null;
                if (this.listeningSocket != null)
                {
                    this.listeningSocket.Close();
                    this.listeningSocket = null;
                }

                this.activated = false;
            }
        }

        /// <summary>
        /// Activate the bearers in this MTA.
        /// </summary>
        internal void Activate()
        {
            lock (this.mtaLock)
            {
                foreach (KeyValuePair<string, Bearer> kvp in this.bearersByNodeName)
                {
                    kvp.Value.Activate(this);
                }

                if (!this.useDatagrams)
                {
                    this.StartListening();
                }

                this.activated = true;
            }
        }

        /// <summary>
        /// Set up listening socket and begin to accept connection requests asynchronously.
        /// </summary>
        private void StartListening()
        {
            TestLogger("StartListening");
            TestLogger(" listeningPort " + listeningPort.ToString());
            this.listeningEndPoint = new IPEndPoint(0, this.listeningPort);
            this.listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.listeningSocket.Bind(this.listeningEndPoint);
            this.listeningSocket.Listen(100);
            this.Listen();
        }

        /// <summary>
        /// Begin to accept connection requests.
        /// Catch and log socket exception.
        /// </summary>
        private void Listen()
        {
            try
            {
                this.listeningSocket.BeginAccept(new AsyncCallback(this.AcceptCallback), null);
            }
            catch (SocketException e)
            {
                EventControl.Debug(this.mtaName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Listen socket closed - {0}", 
                    e.Message));
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.mtaName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Listen - {0}", 
                    e.ToString()));
                throw;
            }
        }

        /// <summary>
        /// Callback method - accept connection request and complete (or reject) the connection.
        /// Call Listen to continue accepting requests.
        /// </summary>
        /// <param name="ar">Asynchronous result data.</param>
        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                lock (this.mtaLock)
                {
                    if (this.listeningSocket != null)
                    {
                        Socket connectingSocket = this.listeningSocket.EndAccept(ar);
                        this.ValidateAndConnect(connectingSocket);
                        this.Listen();
                    }
                    else
                    {
                        EventControl.Debug(this.mtaName, "AcceptCallback socket closed.");
                    }
                }
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.mtaName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "AcceptCallback - {0}", 
                    e.ToString()));
                throw;
            }
        }

        /// <summary>
        /// Only allow connection if the remote address is configured as a known bearer.
        /// </summary>
        /// <param name="connectingSocket">Socket for the bearer.</param>
        private void ValidateAndConnect(Socket connectingSocket)
        {
            var remoteEndPoint = connectingSocket.RemoteEndPoint as IPEndPoint;
            if (remoteEndPoint != null)
            {
                IPAddress address = remoteEndPoint.Address;
                try
                {
                    Bearer bearer = this.bearersByIPAddress[address];
                    bearer.ConnectSocket(connectingSocket);
                }
                catch (KeyNotFoundException e)
                {
                    EventControl.Note(this.mtaName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "Connection closed for Non-Authorised machine - {0} - {1}", 
                        address.ToString(), 
                        e.Message));
                    connectingSocket.Close();
                }
            }
        }

        /// <summary>
        /// Convert the given name to an ASCII enumeration or use the given default.
        /// </summary>
        /// <param name="name">Name of character.</param>
        /// <param name="defaultCharacter">Default character.</param>
        /// <returns>ASCII character enumeration.</returns>
        private AsciiChar AsciiCharFromNameOrDefault(string name, AsciiChar defaultCharacter)
        {
            AsciiChar character = defaultCharacter;
            switch (name)
            {
                case "NONE":
                    character = 0;
                    break;
                case "BEL":
                    character = AsciiChar.BEL;
                    break;
                case "SOH":
                    character = AsciiChar.SOH;
                    break;
                default:
                    EventControl.Warn(this.mtaName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Character {0} unexpected - using default {1}",
                        name,
                        defaultCharacter.ToString()));
                    break;
            }

            return character;
        }

        const string LogFileName = @"c:\PoCLogs\MtaLog.txt";
        private static object logLockObject = new object();

        public void TestLogger(string message)
        {
            lock (logLockObject)
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(DateTime.UtcNow.ToString() + " : " + message);
                }
            }
        }
    }
}

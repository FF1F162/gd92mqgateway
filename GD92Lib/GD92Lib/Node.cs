﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Represents a GD-92 node.
    /// </summary>
    internal class Node
    {
        internal const int DefaultManTimeout = 300; // five minutes
        private const int MaxManTimeout = 1200; // twenty minutes

        private readonly PortAddress address;
        private readonly NodeType gd92Type;
        private TransmitQueue transmitQueue;
        private string name;
        private short manTimeout;

        /// <summary>
        /// Initialises a new instance of the <see cref="Node"/> class.
        /// </summary>
        /// <param name="nodeName">Node name.</param>
        /// <param name="portAddress">Full GD92 address.</param>
        internal Node(string nodeName, PortAddress portAddress)
        {
            this.name = nodeName;
            this.address = portAddress;
            this.gd92Type = NodeType.Rsc;
            this.manTimeout = DefaultManTimeout;
            this.transmitQueue = new TransmitQueue(nodeName);
            this.Routes = new PriorityQueue[0];
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Node"/> class.
        /// Set node type differently from the default (Rsc).
        /// </summary>
        /// <param name="nodeName">Node name.</param>
        /// <param name="portAddress">Full GD92 address.</param>
        /// <param name="gd92Type">Type of node.</param>
        internal Node(string nodeName, PortAddress portAddress, NodeType gd92Type)
            : this(nodeName, portAddress)
        {
            this.gd92Type = gd92Type;
        }

        /// <summary>
        /// Name of the node.
        /// </summary>
        internal string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// Full GD92 address of the node.
        /// </summary>
        internal PortAddress Address
        {
            get { return this.address; }
        }

        /// <summary>
        /// Short GD92 address of the node (omits port).
        /// </summary>
        internal NodeAddress NodeAddress
        {
            get { return new NodeAddress(this.address); }
        }

        /// <summary>
        /// Type of node.
        /// </summary>
        internal NodeType GD92Type
        {
            get { return this.gd92Type; }
        }

        /// <summary>
        /// Manual timeout (longer where crew are on call, not on station).
        /// </summary>
        internal int ManTimeout
        {
            get
            {
                return this.manTimeout;
            }

            set
            {
                this.manTimeout = Node.ValidateManTimeout(value);
            }
        }

        /// <summary>
        /// True if this is a secondary node.
        /// </summary>
        internal bool Secondary { get; set; }

        /// <summary>
        /// Array of routes to the node, in order of preference.
        /// </summary>
        internal PriorityQueue[] Routes { get; set; }

        /// <summary>
        /// Queue for frames waiting to be transmitted to the node.
        /// </summary>
        internal TransmitQueue TxQueue
        {
            get { return this.transmitQueue; }
        }

        internal static short ValidateManTimeout(int manTimeout)
        {
            if (manTimeout < 0 || manTimeout > MaxManTimeout)
            {
                throw new ArgumentOutOfRangeException("manTimeout", manTimeout, "ManTimeout is negative or too large");
            }
            
            return (short)manTimeout;
        }

        /// <summary>
        /// Needed when the node is created under a different name initially, for the purpose of
        /// reading the routes for that node (default routes to be used for the new node).
        /// </summary>
        /// <param name="newName">Name to apply.</param>
        internal void SetName(string newName)
        {
            this.name = newName;
            this.transmitQueue = new TransmitQueue(newName);
        }
    }
}

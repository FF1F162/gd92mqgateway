﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Interface to provide a serial number for each message sent or received
    /// for reference by the host application (not part of GD-92)
    /// This method normally requires access to the Registry
    /// The interface allows this to be mocked.
    /// </summary>
    internal interface ISerialNumberProvider
    {
        int GetSerialNumber();
    }
}

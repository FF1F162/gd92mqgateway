﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Simplified IRouter that returns node objects by name
    /// This is needed to get the type of the node, e.g. Storm (or something else) for Msg27DecoderTests
    /// so for non-default types, the address in the test data needs to appear in the mock node data here.
    /// </summary>
    internal class MockRouter : IRouter
    {
        private readonly Dictionary<string, Node> nodeByName;
        private readonly Dictionary<NodeAddress, Node> nodeByAddress;

        public MockRouter()
        {
            this.nodeByName = new Dictionary<string, Node>();
            this.nodeByAddress = new Dictionary<NodeAddress, Node>();
            this.AddNode("Storm", new PortAddress(22, 0, 10), NodeType.Storm);
            this.AddNode("Ranch", new PortAddress(22, 105, 10), NodeType.Storm);
            this.AddNode("Shed", new PortAddress(22, 101, 10), NodeType.Storm);
            this.AddNode("SGW1", new PortAddress(22, 116, 1), NodeType.Gateway);
            this.AddNode("MOB1", new PortAddress(22, 1, 10), NodeType.Mobs);
        }

        public Node GetNodeByNameOrDefault(string nodeName)
        {
            return this.nodeByName.ContainsKey(nodeName) ? this.nodeByName[nodeName] : new Node("MockNodeName", new PortAddress());
        }
        
        public Node GetNodeByAddressOrDefault(NodeAddress address)
        {
            return this.nodeByAddress.ContainsKey(address) ? this.nodeByAddress[address] : new Node("MockNodeNameForAddress", new PortAddress());
        }
        
        private void AddNode(string nodeName, PortAddress address, NodeType nodeType)
        {
            var newNode = new Node(nodeName, address, nodeType);
            this.nodeByName.Add(newNode.Name, newNode);
            this.nodeByAddress.Add((NodeAddress)newNode.NodeAddress, newNode);
        }
    }
}

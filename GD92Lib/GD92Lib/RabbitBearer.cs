﻿namespace Thales.KentFire.GD92
{
    using KFRS.MDT.MobiliseMessages;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;
    using Thales.KentFire.EventLib;
    using RabbitMQConnection;
    using System.IO;



    internal class RabbitBearer : Bearer
    {
        private const string DefaultRabbitQueueName = "StormGD92Queue";
        private const int MessageArrivalTimeout = 2000; // ms
        private const string ExchangeName = "";
        private const string QueueName = "";

        private static RabbitClient RabbitClient;
        private static AutoResetEvent auto = new AutoResetEvent(true); // initially signaled
        private int SequenceNumber = 0;
        private int LastSendLength = 0;
        private static object logLockObject = new object();

        const string LogFileName = @"c:\PoCLogs\RabbitBearerLog.txt";

        Message LatestMessage;
        List<Message> ReceivedMessages = new List<Message>();
        private readonly byte[] readBuffer;
        // port and IP address of MQServer
        private IPEndPoint MQServer;
        private string MQQueueName;

        internal RabbitBearer(string nodeName, string mtaName, IPAddress ipAddress, int ipPort)
            : base(nodeName, mtaName, ipAddress, ipPort)
        {
            TestLogger(" RabbitBearer ctor");
            this.readBuffer = this.GetReadBuffer();
            Debug.Assert(this.readBuffer != null, "Must have a read buffer");

            TestLogger(" QueueName " + QueueName);
            TestLogger(" ExchangeName " + ExchangeName);

            if (RabbitClient == null)
            {
                RabbitClient = new RabbitClient(QueueName, ExchangeName);
                RabbitClient.MessageReceivedHandler += RabbitMessageReceived;
            }
        }

        
      
        protected override void SetConnectionDetails()
        {
            TestLogger("SetConnectionDetails");

            // create socket - not used but required in SocketEnd
            this.MQQueueName = DefaultRabbitQueueName;
            TestLogger(" MQQueueName " + MQQueueName);

            this.RemoteIPEndpoint = new IPEndPoint(this.IPAddress, this.IPort + UserAgent.Instance.GD92Address.Node);
            TestLogger(" RemoteIPEndpoint " + RemoteIPEndpoint.ToString());

            Node node = Router.Instance.GetNodeByNameOrDefault(this.NodeName);
            TestLogger(" NodeName " + NodeName);
            
            EventControl.Debug(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "MQ Server is {0}:{1} queue is {2}",
                this.MQServer.Address.ToString(),
                this.MQServer.Port.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.MQQueueName));

            this.SocketSetupInterval = this.Mta.ConnectionInterval * Bearer.Milliseconds;
            if (this.SocketSetupInterval <= 0)
            {
                this.SocketSetupInterval = Bearer.DefaultSocketSetupInterval;
            }

            TestLogger(" SocketSetupInterval " + SocketSetupInterval.ToString());
            this.SocketSetupTimer = new Timer(this.ProcessConnection, 0, 0, this.SocketSetupInterval);
            TestLogger(" SetConnectionDetails end");

        }

        protected override void SetUpSocket()
        {
            // Debug.Assert(this.Socket == null, "Socket must be null initially");
            TestLogger("SetUpSocket");
            this.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);





        }

        protected override void BeginToConnect()
        {
            // open connection to RabbitMQ

            TestLogger("BeginToConnect");
            RabbitClient.Start();

        }

        // override function in SocketEnd
        protected virtual void Receive(int offset)
        {
            

            // should call this.BeginReceive(offset)


        }


       

     
        protected override void BeginReceive(int offset)
        {
            // get message from RabbitMQ
            // push data int this.readBuffer

            TestLogger("BeginReceive");
            // call to ReceiveCallback will result in EndReceive being called
            IAsyncResult result = (IAsyncResult)this.Socket;

            bool gotMessage = GetMessage();
            TestLogger(" gotmessage " + gotMessage.ToString());

            if (!gotMessage)
            {
                TestLogger(" Wait one");
                if (auto.WaitOne(MessageArrivalTimeout))
                {
                    gotMessage = GetMessage();
                    TestLogger(" got message = " + gotMessage.ToString());
                }
            }

            if (!gotMessage)
            {
                TestLogger(" No message");
                LatestMessage = null;
            }

            TestLogger(" Start task");
            Task t = new Task(() => ReceiveCallback(result)); // does this need to be done as a Task since it is a mangling of an async response?
            t.Start();


            TestLogger(" BeginReceive end");

        }

        private bool GetMessage()
        {
            TestLogger("GetMessage");
            if (ReceivedMessages.Count > 0)
            {
                LatestMessage = ReceivedMessages[0];
                ReceivedMessages.RemoveAt(0);
                TestLogger(" Pulled one message from ReceivedMessages");
                return true;
            }

            return false;
        }
       
        public void RabbitMessageReceived(object sender, MessageEventArgs args)
        {
            ReceivedMessages.Add(args.Message);
            auto.Set();
        }


        protected override int EndReceive(IAsyncResult ar)
        {
            // byte[] ?
            TestLogger("EndReceive");
            if (LatestMessage != null)
            {
                int messageLength = ((byte[])LatestMessage.Content).Length;
                TestLogger(" messageLength " + messageLength.ToString());
                return messageLength;   // return number of bytes read
            }
            else
            {
                TestLogger(" LatestMessage is null");
            }
            

            return 0;
        }

       
        protected override void BeginSend(byte[] byteData, int numberOfBytes)
        {
            TestLogger("BeginSend");
            TestLogger(" byteData length " + ((byteData == null) ? 0 : byteData.Length).ToString());
            // send to rabbit mq here
            Message mess = new Message(Guid.NewGuid());
            mess.Content = (object)byteData;
            mess.MessageType = KFRS.MDT.MobiliseMessages.Enums.MessageType.Nack;
            mess.Destination = "kfrs";
            mess.Originator = "?";
            mess.Priority = KFRS.MDT.MobiliseMessages.Enums.Priority.Medium;
            mess.SequenceNumber = SequenceNumber++;
            //mess.TimeStamp. = DateTime.UtcNow;
            
            LastSendLength = RabbitClient.SendMessageToQueue(QueueName, ExchangeName, mess);

            LogMessage(mess);

            TestLogger(" LastSendLength = " + LastSendLength.ToString());

        }

        private void LogMessage(Message mess)
        {
            TestLogger("  Message type " + mess.MessageType.ToString());
            TestLogger("  Destination " + mess.Destination);
            TestLogger("  Originator " + mess.Originator);
            TestLogger("  Priority " + mess.Priority.ToString());
            TestLogger("  SequenceNumber " + mess.SequenceNumber.ToString());
        }


        protected override int EndSend(IAsyncResult ar)
        {
            //int bytesSent = this.Socket.EndSendTo(ar);
            //return bytesSent;

            TestLogger("EndSend, LastSendLength = " + LastSendLength.ToString());
            return LastSendLength;
        }

        public void TestLogger(string message)
        {
            lock (logLockObject)
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(DateTime.UtcNow.ToString() + " : " + message);
                }
            }
        }

    }





}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Encoder for PeripheralStatus message (type 28).
    /// </summary>
    internal class Msg28Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg28Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg28Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        /// <summary>
        /// Encode data from the message object
        /// Assume fields have been validated
        /// Write data into the (base) Target byte array then copy into frame(s).
        /// </summary>
        /// <param name="message">Message to encode.</param>
        /// <returns>Array containing frame.</returns>
        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.PeripheralStatus, "Message type must be 28");
            var peripheralStatus = message as GD92Msg28;
            System.Diagnostics.Debug.Assert(peripheralStatus != null, "Message type must be 28");
            if (peripheralStatus == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 28");
            }
            else
            {
                int index = 0;
                index = this.EncodeWord16(index, (int)peripheralStatus.IpPeripherals);
                index = this.EncodeWord16(index, (int)peripheralStatus.OpPeripherals);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }

            return frames;
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Provide an array of frames to hold data when encoding a message for transmission
    /// and begin to populate the frames with data.
    /// A new message needs a GD92 sequence number for each frame (block)
    /// A reply needs data copied from the last frame of the message being acknowledged.
    /// </summary>
    internal class FrameProvider : IFrameProvider
    {
        private readonly IGD92SequenceProvider sequenceNumberProvider;
        private readonly IReceivedFrames receivedFrames;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="FrameProvider"/> class.
        /// Initialize with the resources needed to provide sequence numbers for new frames
        /// or to retrieve the last frame of a received message (for a reply).
        /// </summary>
        /// <param name="sequenceProvider">Provider of GD92 sequence numbers (for new message).</param>
        /// <param name="receivedFrames">Provider of access to received frames (for reply).</param>
        internal FrameProvider(IGD92SequenceProvider sequenceProvider,
                               IReceivedFrames receivedFrames)
        {
            this.sequenceNumberProvider = sequenceProvider;
            this.receivedFrames = receivedFrames;
        }
        
        /// <summary>
        /// Set up frames for a new message, providing sequence number and Block OfBlocks.
        /// </summary>
        /// <param name="numberRequiredForMessage">Size of message in blocks (frames).</param>
        /// <returns>Array of frames populated with sequence numbers.</returns>
        public Frame[] SetUpFramesWithNewSequenceNumbers(int numberRequiredForMessage)
        {
            var frameArray = new Frame[numberRequiredForMessage];
            var sequenceNumbers = new int[numberRequiredForMessage];
            this.sequenceNumberProvider.FillArrayWithSequenceNumbers(sequenceNumbers);
            for (int i = 0; i < numberRequiredForMessage; i++)
            {
                frameArray[i] = new Frame();
                frameArray[i].SequenceNumber = sequenceNumbers[i];
                frameArray[i].Block = i + 1;
                frameArray[i].OfBlocks = numberRequiredForMessage;
            }
            
            return frameArray;
        }
        
        /// <summary>
        /// Use the message serial number to get the last frame of the message being acknowledged
        /// Set up an acknowledgement frame, using Frame.CopyForAcknowledgement.
        /// </summary>
        /// <param name="messageSerialNumber">Serial number of original message.</param>
        /// <returns>Array containing a single frame with data needed to encode acknowledgement.</returns>
        public Frame[] SetUpFrameForAcknowledgement(int messageSerialNumber)
        {
            Frame originalFrame = this.receivedFrames.GetFrameToAcknowledge(messageSerialNumber);
            var frameArray = new Frame[1];
            frameArray[0] = originalFrame.CopyForAcknowledgement();
            return frameArray;
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Raw Frame message (type 255).
    /// The code duplicates what is in LogReader.cs.
    /// </summary>
    internal class RawFrameMsgEncoder : Encoder, IMessageEncoder
    {
        private const char ByteSeparatorCharacter = '-';

        /// <summary>
        /// Initialises a new instance of the <see cref="RawFrameMsgEncoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal RawFrameMsgEncoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        /// <summary>
        /// Confirm that the message is of the type expected.
        /// Use the message data to populate a frame in the usual way. This
        /// determines the routing of the frame.
        /// Substitute data to be transmitted with the data in the message
        /// (the raw frame may show different addressing information etc).
        /// </summary>
        /// <param name="message">Message to be encoded.</param>
        /// <returns>Frame array from the message.</returns>
        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.RawFrame, "Message type must be 255");
            var rawFrame = message as RawFrameMsg;
            System.Diagnostics.Debug.Assert(rawFrame != null, "Message type must be 255");
            if (rawFrame == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 255");
            }
            else
            {
                this.TargetDataLength = 0;
                frames = this.BuildMessageFrames(message);
                System.Diagnostics.Debug.Assert(frames.Length == 1, "One frame is expected (similar to Ack)");
                
                // Overwrite the data to be transmitted
                frames[0].FrameBytes = ConvertToByteArray(rawFrame.LogOfFrameBytes);
                frames[0].Length = frames[0].FrameBytes.Length;
            }
            
            return frames;
        }
        
        /// <summary>
        /// Read the output of BitConverter.ToString back into a byte array.
        /// The string must be exclusively 2-digit hexadecimal separated by hyphen.
        /// </summary>
        /// <param name="testData">Data as output by BitConverter.</param>
        /// <returns>Array of bytes.</returns>
        private static byte[] ConvertToByteArray(string testData)
        {
            var output = new byte[CalculateOutputLength(testData)];
            char[] separators = { ByteSeparatorCharacter };
            string[] splitData = testData.Split(separators);
            if (output.Length != splitData.Length)
            {
                throw new System.FormatException("string must be exclusively 2-digit hexadecimal");
            }
            
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = ConvertTwoHexDigitsToByte(splitData[i]);
            }
            
            return output;
        }
        
        /// <summary>
        /// Each byte in the output is represented in the input by a separator and two digits.
        /// </summary>
        /// <param name="testData">Data as output by BitConverter.</param>
        /// <returns>Expected length of output.</returns>
        private static int CalculateOutputLength(string testData)
        {
            return (testData.Length / 3) + 1;
        }
        
        /// <summary>
        /// Validate input and use Convert.
        /// </summary>
        /// <param name="digits">Two hexadecimal digits.</param>
        /// <returns>Byte represented by the input.</returns>
        private static byte ConvertTwoHexDigitsToByte(string digits)
        {
            if (digits.Length != 2)
            {
                throw new ArgumentOutOfRangeException("digits", digits, "two-character hexadecimal expected");
            }
            
            const int BaseSixteen = 16;
            return Convert.ToByte(digits, BaseSixteen);
        }
    }
}

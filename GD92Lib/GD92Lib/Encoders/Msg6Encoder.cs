﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.ObjectModel;
    
    /// <summary>
    /// Encoder for Kent Mobilise message (type 6) - includes overrides for multi-block.
    /// </summary>
    internal class Msg6Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg6Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg6Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        /// <summary>
        /// Encode data from the message object (nothing new to be added here)
        /// Assume fields have been validated
        /// Write data into the (base) Target byte array then copy into frame(s).
        /// </summary>
        /// <param name="message">Message to encode.</param>
        /// <returns>Frame array comprising encoded message.</returns>
        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.MobiliseMessage6, "Message type must be 6");
            var kentMobMessage = message as GD92Msg6;
            System.Diagnostics.Debug.Assert(kentMobMessage != null, "Message type must be 6");
            if (kentMobMessage == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 6");
            }
            else
            {
                this.EnsureTargetIsLargeEnough(kentMobMessage);
                this.EncodeFieldsInTargetArray(kentMobMessage);

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
 
        /// <summary>
        /// Allow for Block, OfBlocks and ManAck fields prepending the message data in every frame.
        /// </summary>
        /// <param name="frame">Frame to be encoded.</param>
        /// <returns>Length of message field (all data following message type field).</returns>
        protected override int CalculateFrameMessageLength(Frame frame)
        {
            return FrameConverter.BlockOfBlocksLength + FrameConverter.ManAckRequiredLength + frame.FrameDataLength;
        }
        
        /// <summary>
        /// Encode Block OfBlocks in every frame
        /// For sending to RSC encode manual acknowledgement required field in every frame.
        /// </summary>
        /// <param name="frame">Frame to be encoded.</param>
        /// <param name="index">Index into FrameBytes for writing the next byte.</param>
        /// <returns>Index for writing the first byte of actual message data.</returns>
        protected override int EncodeFieldsBeforeMessageData(Frame frame, int index)
        {
            index = frame.EncodeBlockOfBlocks(index);
            index = CalculateAndEncodeManAckRequired(frame, index);
            return index;
        }

        private static int EstimateUncompressedLength(GD92Msg6 kentMobMessage)
        {
            int count = 100; // margin for string wordcount etc
            count += 1; // kentMobMessage.MobilisationType
            count += kentMobMessage.AssistanceMessage.Length;
            count += EstimateResourcesLength(kentMobMessage.ResourcesList);
            count += EstimateResourcesLength(kentMobMessage.AllResourcesList);
            count += FrameConverter.IncidentDateTimeFormat.Length; // kentMobMessage.IncidentTimeAndDate
            count += FrameConverter.IncidentDateTimeFormat.Length; // kentMobMessage.MobTimeAndDate;
            count += kentMobMessage.IncidentNumber.Length;
            count += kentMobMessage.IncidentType.Length;
            count += kentMobMessage.Address.Length;
            count += kentMobMessage.HouseNumber.Length;
            count += kentMobMessage.Street.Length;
            count += kentMobMessage.SubDistrict.Length;
            count += kentMobMessage.District.Length;
            count += kentMobMessage.Town.Length;
            count += kentMobMessage.County.Length;
            count += kentMobMessage.Postcode.Length;
            count += EstimateAttendanceLength(kentMobMessage.FirstAttendance);
            count += EstimateAttendanceLength(kentMobMessage.SecondAttendance);
            count += kentMobMessage.MapBook.Length;
            count += kentMobMessage.MapRef.Length;
            count += kentMobMessage.HowReceived.Length;
            count += kentMobMessage.TelNumber.Length;
            count += kentMobMessage.OperatorInfo.Length;
            count += kentMobMessage.WaterInformation.Length;
            count += kentMobMessage.LocationNotices.Length;
            count += kentMobMessage.AddText.Length;
            return count;
        }
        
        private static int EstimateResourcesLength(Collection<GD92Msg6Resource> resources)
        {
            int count = 1; // for number of resources
            foreach (GD92Msg6Resource resource in resources)
            {
                count += 2; // for string byte counts
                count += resource.Callsign.Length;
                count += resource.Commentary.Length;
            }
            
            return count;
        }
        
        private static int EstimateAttendanceLength(GD92Msg6Attendance attendance)
        {
            int count = 2; // for number of callsigns and byte count for address
            foreach (string callsign in attendance.Callsigns)
            {
                count += callsign.Length;
            }
            
            count += attendance.Address.Length;
            return count;
        }

        /// <summary>
        /// Only the last frame has manual acknowledgement set and then only if it is required for the message
        /// (this is determined by the setting in the type 2 MobiliseMessage from Storm)
        /// Assert that the field in the frame has not been set somewhere else in the code.
        /// </summary>
        /// <param name="frame">Frame to be encoded.</param>
        /// <param name="index">Index of man ack field.</param>
        /// <returns>Index after man ack field.</returns>
        private static int CalculateAndEncodeManAckRequired(Frame frame, int index)
        {
            System.Diagnostics.Debug.Assert(frame.Message != null, "Message reference must not be null");
            System.Diagnostics.Debug.Assert(!frame.ManAckRequired, "Man ack required must not be set previously");
            if (frame.Message != null && frame.Message.ManualAck && frame.Block == frame.OfBlocks)
            {
                frame.ManAckRequired = true;
            }
            
            index = frame.EncodeManAckRequired(index); 
            return index;
        }

        private void EnsureTargetIsLargeEnough(GD92Msg6 kentMobMessage)
        {
            int size = EstimateUncompressedLength(kentMobMessage);
            if (size > this.Target.Length)
            {
                this.IncreaseSizeOfTarget(size);
            }
        }
        
        private void EncodeFieldsInTargetArray(GD92Msg6 kentMobMessage)
        {
            int index = 0;
            index = this.EncodeWord8(index, kentMobMessage.MobilisationType);
            index = this.EncodeCompressedString(index, kentMobMessage.AssistanceMessage);
            index = this.EncodeMsg6ResourcesList(index, kentMobMessage.ResourcesList);
            index = this.EncodeMsg6ResourcesList(index, kentMobMessage.AllResourcesList);
            index = this.EncodeTimeAndDate(index, kentMobMessage.IncidentTimeAndDate);
            index = this.EncodeTimeAndDate(index, kentMobMessage.MobTimeAndDate);
            index = this.EncodeIncidentNumber(index, kentMobMessage.IncidentNumber);
            index = this.EncodeCompressedString(index, kentMobMessage.IncidentType);
            index = this.EncodeCompressedString(index, kentMobMessage.Address);
            index = this.EncodeString(index, kentMobMessage.HouseNumber);
            index = this.EncodeCompressedString(index, kentMobMessage.Street);
            index = this.EncodeCompressedString(index, kentMobMessage.SubDistrict);
            index = this.EncodeCompressedString(index, kentMobMessage.District);
            index = this.EncodeCompressedString(index, kentMobMessage.Town);
            index = this.EncodeCompressedString(index, kentMobMessage.County);
            index = this.EncodeString(index, kentMobMessage.Postcode);
            index = this.EncodeMsg6Attendance(index, kentMobMessage.FirstAttendance);
            index = this.EncodeMsg6Attendance(index, kentMobMessage.SecondAttendance);
            index = this.EncodeString(index, kentMobMessage.MapBook);
            index = this.EncodeString(index, kentMobMessage.MapRef);
            index = this.EncodeString(index, kentMobMessage.HowReceived);
            index = this.EncodeString(index, kentMobMessage.TelNumber);
            index = this.EncodeCompressedString(index, kentMobMessage.OperatorInfo);
            index = this.EncodeCompressedString(index, kentMobMessage.WaterInformation);
            index = this.EncodeLongCompressedString(index, kentMobMessage.LocationNotices);
            index = this.EncodeCompressedString(index, kentMobMessage.AddText);
            this.TargetDataLength = index;
        }
    }
}

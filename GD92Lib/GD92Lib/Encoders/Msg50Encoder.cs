﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Ack message (type 50).
    /// </summary>
    internal class Msg50Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg50Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg50Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.Ack, "Message type must be 50");
            var ackMessage = message as GD92Msg50;
            System.Diagnostics.Debug.Assert(ackMessage != null, "Message type must be 50");
            if (ackMessage == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 50");
            }
            else
            {
                this.TargetDataLength = 0;

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

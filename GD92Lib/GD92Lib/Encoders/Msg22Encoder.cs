﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Log Update message (type 22).
    /// </summary>
    internal class Msg22Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg22Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">Frame provider.</param>
        internal Msg22Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.LogUpdate, "Message type must be 22");
            var logUpdate = message as GD92Msg22;
            System.Diagnostics.Debug.Assert(logUpdate != null, "Message type must be 22");
            if (logUpdate == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 22");
            }
            else
            {
                int index = 0;
                index = this.EncodeString(index, logUpdate.Callsign);
                index = this.EncodeIncidentNumber(index, logUpdate.IncidentNumber);
                index = this.EncodeCompressedString(index, logUpdate.Update);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

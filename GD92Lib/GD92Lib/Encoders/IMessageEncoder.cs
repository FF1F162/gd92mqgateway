﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Implemented by the encoder subclass for each type of message.
    /// Return an array of Frame objects (usually just one, more for a long message)
    /// each containing a byte array ready for transmission.
    /// </summary>
    internal interface IMessageEncoder
    {
        Frame[] EncodeMessage(GD92Message message);
    }
}

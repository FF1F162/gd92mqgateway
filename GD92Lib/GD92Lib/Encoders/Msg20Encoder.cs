﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Resource Status message (type 20) 
    /// GD-92 allows for more than one callsign in the message, but we ask for one status at a time.
    /// </summary>
    internal class Msg20Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg20Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">Frame provider.</param>
        internal Msg20Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.ResourceStatus, "Message type must be 20");
            var resourceStatus = message as GD92Msg20;
            System.Diagnostics.Debug.Assert(resourceStatus != null, "Message type must be 20");
            if (resourceStatus == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 20");
            }
            else
            {
                int index = 0;
                index = this.EncodeString(index, resourceStatus.Callsign);
                index = this.EncodeWord8(index, resourceStatus.AvlType);
                index = this.EncodeString(index, resourceStatus.AvlData);
                index = this.EncodeWord8(index, resourceStatus.StatusCode);
                index = this.EncodeCompressedString(index, resourceStatus.Remarks);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.ObjectModel;
    using System.Text;
    using Thales.KentFire.GD92;
    using Thales.KentFire.SocketLib;
    
    /// <summary>
    /// Base class that provides common functionality for encoding a GD92Message object
    /// into an array of bytes in GD-92 format prior to transmission. Long messages need splitting into
    /// blocks (frames). The result of encoding is an array of Frame objects that hold a byte array 
    /// and the information used to construct it (including the message object).
    /// </summary>
    internal class Encoder
    {
        private const int ByteSize = FrameConverter.ByteSize;
        private const int DestinationCountSize = FrameConverter.DestinationCountSize;
        private const int PortSize = FrameConverter.PortSize;
        private const int ProtVerSize = FrameConverter.ProtVerSize;
        private const int AckReqSize = FrameConverter.AckReqSize;
        private const int DefaultLength = FrameConverter.MaxLength;
        
        private readonly IFrameProvider frameProvider;
        
        private byte[] target;
        private byte[] uncompressedBuffer;
        private byte[] compressedBuffer;

        /// <summary>
        /// Initialises a new instance of the <see cref="Encoder"/> class.
        /// Construct the encoder, providing a default target byte array sufficient for one frame.
        /// </summary>
        /// <param name="frameProvider">Frame provider.</param>
        internal Encoder(IFrameProvider frameProvider)
        {
            this.frameProvider = frameProvider;
            this.target = new byte[DefaultLength];
        }
        
        /// <summary>
        /// Byte array for building the entire encoded message
        /// If the message is larger than one frame, the default target array must be replaced
        /// with an array sufficiently large to hold the entire message.
        /// </summary>
        protected byte[] Target
        {
            get { return this.target; }
        }
        
        /// <summary>
        /// It is up to sub-classes to keep track of this and assign it at the end of EncodeMessage.
        /// </summary>
        protected int TargetDataLength { get; set; }

        /// <summary>
        /// Encode the bytes comprising the envelope (bytes comprising the message are prepared separately).
        /// </summary>
        /// <param name="frame">Frame object holding data and array of bytes.</param>
        internal static void EncodeEnvelope(Frame frame)
        {
            byte[] frameBytes = frame.FrameBytes;
            int index = 0;
            frameBytes[index++] = (byte)AsciiChar.SOH;
            index = frame.EncodeCommsAddress(index, frame.Source);
            System.Diagnostics.Debug.Assert(frame.DestinationCount == 1, "Only one destination allowed");
            index = frame.EncodeCountAndLength(index);
            index = frame.EncodeCommsAddress(index, frame.Destination);
            index = frame.EncodeProtocolAndPriority(index);
            index = frame.EncodeAckAndSeq(index);
            index = frame.EncodeWord8(index, (int)frame.MessageType);
            System.Diagnostics.Debug.Assert(index == FrameConverter.MessageIndexAfterHeaderForSingleDestination, "Index must be just after end of header");

            // skip message bytes prepared in advance
            index += frame.FrameMessageLength;
            index = frame.EncodeBlockCheckCharacter(index);
            frameBytes[index] = (byte)AsciiChar.EOT;
            frame.Length = index + 1;
        }

        #region Virtual methods for frame building

        /// <summary>
        /// The frame message field comprises everything after the header (i.e. after the message type byte)
        /// In multi-block messages the actual message data is prepended by Block and OfBlocks bytes
        /// There may also be a manual acknowledgement byte in all frames (BBN-style) or possibly just the first
        /// Override CalculateFrameMessageLength to get the behaviour required.
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <returns>Length of message field (all data following message type field).</returns>
        protected virtual int CalculateFrameMessageLength(Frame frame)
        {
            return frame.FrameDataLength;
        }
        
        /// <summary>
        /// The default is to prepend nothing (so index stays the same)
        /// Override to encode Block OfBlocks etc using the same rules as in CalculateFrameMessageLength.
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <param name="index">Index into FrameBytes for writing the next byte.</param>
        /// <returns>Index for writing the first byte of actual message data.</returns>
        protected virtual int EncodeFieldsBeforeMessageData(Frame frame, int index)
        {
            return index;
        }

        #endregion

        #region Frame building

        /// <summary>
        /// Call this only after calling one of the Encode methods to populate the target byte array.
        /// </summary>
        /// <param name="message">Message to encode.</param>
        /// <returns>Array of encoded frames.</returns>
        protected Frame[] BuildMessageFrames(GD92Message message)
        {
            int numberOfFramesInMessage = (this.TargetDataLength / FrameConverter.MaxDataSize) + 1;
            var frameArray = this.ObtainFrameArray(message, numberOfFramesInMessage);
            int targetIndex = 0;
            for (int i = 0; i < numberOfFramesInMessage; i++)
            {
                AddHeaderInformationFromMessage(frameArray[i], message);
                targetIndex = this.EncodeFrame(frameArray[i], targetIndex);
                message.GD92Sequence = frameArray[i].SequenceNumber; // ends up with the last sequence number
            }
            
            System.Diagnostics.Debug.Assert(targetIndex == this.TargetDataLength, "Target index is expected to be the same as the index");
            return frameArray;
        }
        
        #endregion

        #region Protected encoding methods and helpers

        protected int EncodeCommsAddress(int index, PortAddress pa)
        {
            int byte1 = pa.Brigade;
            int nodeAndPort = (pa.Node * FrameConverter.PortSize) + pa.Port;
            int byte2 = nodeAndPort / FrameConverter.ByteSize;
            int byte3 = nodeAndPort - (byte2 * FrameConverter.ByteSize);
            this.Target[index++] = (byte)byte1;
            this.Target[index++] = (byte)byte2;
            this.Target[index++] = (byte)byte3;
            return index;
        }
        
        protected int EncodeMsg6ResourcesList(int index, Collection<GD92Msg6Resource> resourcesList)
        {
            index = this.EncodeWord8(index, resourcesList.Count);
            foreach (GD92Msg6Resource resource in resourcesList)
            {
                index = this.EncodeMsg6Resource(index, resource);
            }
            
            return index;
        }
        
        protected int EncodeIncidentNumber(int index, string incidentNumber)
        {
            int number;
            try
            {
                number = int.Parse(incidentNumber, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (System.FormatException)
            {
                number = FrameConverter.DefaultIncidentNumber;
            }
            
            index = this.EncodeWord32(index, number);
            return index;
        }
        
        protected int EncodeTimeAndDate(int index, DateTime dateTime)
        {
            index = this.EncodeFixedLengthString(index, 
                                            dateTime.ToString(FrameConverter.IncidentDateTimeFormat, System.Globalization.CultureInfo.InvariantCulture),
                                            FrameConverter.IncidentDateTimeFormat.Length);
            return index;
        }
        
        protected int EncodeMsg6Attendance(int index, GD92Msg6Attendance attendance)
        {
            System.Diagnostics.Debug.Assert(attendance != null, "Attendance must not be null");
            index = this.EncodeCallsignList(index, attendance.Callsigns);
            if (attendance.Callsigns.Count > 0)
            {
                index = this.EncodeCompressedString(index, attendance.Address);
            }
            
            return index;
        }
        
        protected int EncodeCallsignList(int index, Collection<string> callsignList)
        {
            index = this.EncodeWord8(index, callsignList.Count);
            foreach (string callsign in callsignList)
            {
                index = this.EncodeString(index, callsign);
            }
            
            return index;
        }
        
        protected int EncodeBoolian(int index, bool trueOrFalse)
        {
            index = this.EncodeWord8(index, trueOrFalse ? 1 : 0);
            return index;
        }
        
        protected int EncodeWord8(int index, int word8)
        {
            this.Target[index++] = (byte)word8;
            return index;
        }
        
        protected int EncodeWord16(int index, int word16)
        {
            int byte1 = word16 / ByteSize;
            int byte2 = word16 - (byte1 * ByteSize);
            this.Target[index++] = (byte)byte1;
            this.Target[index++] = (byte)byte2;
            return index;
        }
        
        protected int EncodeWord32(int index, int word32)
        {
            int byte1 = word32 / (ByteSize * ByteSize * ByteSize);
            int byte2 = (word32 - (byte1 * ByteSize * ByteSize * ByteSize)) / (ByteSize * ByteSize);
            int byte3 = (word32 - (byte1 * ByteSize * ByteSize * ByteSize) -
                (byte2 * ByteSize * ByteSize)) / ByteSize;
            int byte4 = word32 - (byte1 * ByteSize * ByteSize * ByteSize) -
                (byte2 * ByteSize * ByteSize) - (byte3 * ByteSize);
            this.Target[index++] = (byte)byte1;
            this.Target[index++] = (byte)byte2;
            this.Target[index++] = (byte)byte3;
            this.Target[index++] = (byte)byte4;
            return index;
        }
        
        protected int EncodeFixedLengthString(int index, string inputValue, int length)
        {
            int stringLength = inputValue.Length;
            if (stringLength != length)
            {
                throw new GD92LibEncoderException();
            }
            
            Encoding ascii = Encoding.ASCII;
            ascii.GetBytes(inputValue, 0, stringLength, this.Target, index);
            return index + stringLength;
        }
        
        protected int EncodeString(int index, string inputValue)
        {
            int stringLength = inputValue.Length;
            if (stringLength > FrameConverter.MaxForByte)
            {
                throw new GD92LibEncoderException("string longer than 255 chars");
            }
            
            this.Target[index++] = (byte)stringLength;
            Encoding ascii = Encoding.ASCII;
            ascii.GetBytes(inputValue, 0, stringLength, this.Target, index);
            return index + stringLength;
        }
        
        protected int EncodeCompressedString(int index, string inputValue)
        {
            int compressedLength = this.CompressIntoBuffer(inputValue);
            if (compressedLength > FrameConverter.MaxForByte)
            {
                throw new GD92LibEncoderException("string longer than 255 chars after compression");
            }
            
            index = this.EncodeWord8(index, compressedLength);
            Buffer.BlockCopy(this.compressedBuffer, 0, this.Target, index, compressedLength);
            return index + compressedLength;
        }
        
        protected int EncodeLongCompressedString(int index, string inputValue)
        {
            int compressedLength = this.CompressIntoBuffer(inputValue);
            if (compressedLength > FrameConverter.MaxForWord)
            {
                throw new GD92LibEncoderException("string too long after compression");
            }
            
            this.EnsureTargetIsLargeEnough(index, compressedLength);
            index = this.EncodeWord16(index, compressedLength);
            Buffer.BlockCopy(this.compressedBuffer, 0, this.Target, index, compressedLength);
            index += compressedLength;
            return index;
        }
        
        protected void IncreaseSizeOfTarget(int targetLength)
        {
            var newTarget = new byte[targetLength];
            Buffer.BlockCopy(this.target, 0, newTarget, 0, this.target.Length);
            this.target = newTarget;
        }
        
        /// <summary>
        /// For a new message add source and destinatinon, priority and set AckRequired true 
        /// For a reply this is already taken care of during copy from original frame - mostly
        /// - for a Nak the source could be different from the original destination.
        /// </summary>
        /// <param name="frame">Frame to be updated.</param>
        /// <param name="message">Message data source.</param>
        private static void AddHeaderInformationFromMessage(Frame frame, GD92Message message)
        {
            frame.Message = message;
            if (message.OriginalMessage == null)
            {
                frame.SourceName = message.From;
                frame.Source = new PortAddress(message.FromAddress);
                frame.DestinationName = message.Destination;
                frame.Destination = DestinationAddressWithAdjustedPortNumber(message);
                frame.Priority = message.Header.Priority;
                frame.AckRequired = !message.SendWithNoAckRequired;
                frame.ReplyExpected = !message.SendWithNoReplyExpected;
            }
            else if (!string.IsNullOrWhiteSpace(message.From))
            {
                // For a Nak the source can be different from the destination copied from the original frame
                // but the destination should be the same.
                System.Diagnostics.Debug.Assert(message.DestAddress.Node == frame.Destination.Node, "destination supplied by the HAP must match the calculated destination");
                frame.SourceName = message.From;
                frame.Source = new PortAddress(message.FromAddress);
            }
            
            frame.ProtocolVersion = message.Header.ProtocolVersion;
            frame.MessageType = message.MsgType;
        }
        
        /// <summary>
        /// Substitute the standard port number (depends on type of message) if the port number is the default.
        /// </summary>
        /// <param name="message">Message containing destination address to be adjusted.</param>
        /// <returns>Adjusted address (or original if non-default port).</returns>
        private static PortAddress DestinationAddressWithAdjustedPortNumber(GD92Message message)
        {
            int port = message.DestAddress.Port;
            if (port == GD92Header.DefaultPortNumber)
            {
                port = message.Header.StandardPort;
            }
            
            return new PortAddress(message.DestAddress.Brigade, message.DestAddress.Node, port);
        }
        
        private Frame[] ObtainFrameArray(GD92Message message, int numberOfFrames)
        {
            return (message.OriginalMessage != null) ?
                this.frameProvider.SetUpFrameForAcknowledgement(message.OriginalMessage.SerialNumber) :
                this.frameProvider.SetUpFramesWithNewSequenceNumbers(numberOfFrames);
        }
        
        private int EncodeFrame(Frame frame, int targetIndex)
        {
            frame.FrameBytes = new byte[FrameConverter.MaxLength];
            this.EncodeMessageField(frame, targetIndex);
            EncodeEnvelope(frame); // requires message length so do this after encoding the message
            targetIndex += frame.FrameDataLength;
            return targetIndex;
        }
        
        /// <summary>
        /// Encode message field (except for Msg 50, which has no message field).
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <param name="targetIndex">Index of message field in target array.</param>
        private void EncodeMessageField(Frame frame, int targetIndex)
        {
            if (this.TargetDataLength > 0)
            {
                frame.FrameDataLength = this.CalculateFrameDataLength(frame.Block, frame.OfBlocks, targetIndex);
                frame.FrameMessageLength = this.CalculateFrameMessageLength(frame);
                this.PopulateFrameBytesWithMessageData(frame, targetIndex);
            }
        }
        
        /// <summary>
        /// Calculate the number of bytes of message data to be copied from the Target byte array.
        /// </summary>
        /// <param name="block">Frame number.</param>
        /// <param name="ofBlocks">Total number of frames.</param>
        /// <param name="targetIndex">Index into the target data.</param>
        /// <returns>Number of bytes of target data to go in the frame.</returns>
        private int CalculateFrameDataLength(int block, int ofBlocks, int targetIndex)
        {
            int length = FrameConverter.MaxDataSize; // default for all frames except the last
            if (block == ofBlocks) 
            {
                // last block
                length = this.TargetDataLength - targetIndex;
            }
            
            System.Diagnostics.Debug.Assert(length <= FrameConverter.MaxDataSize, "Length must not exceed maximum");
            System.Diagnostics.Debug.Assert(length > 0, "Length must be greater than 0");
            System.Diagnostics.Debug.Assert(block <= ofBlocks, "Block number must not exceed number of blocks");
            return length;
        }
        
        private void PopulateFrameBytesWithMessageData(Frame frame, int targetIndex)
        {
            int index = FrameConverter.MessageIndexAfterHeaderForSingleDestination;
            index = this.EncodeFieldsBeforeMessageData(frame, index);
            Buffer.BlockCopy(this.Target, targetIndex, frame.FrameBytes, index, frame.FrameDataLength);
            index += frame.FrameDataLength;
            System.Diagnostics.Debug.Assert(index == frame.FrameMessageLength + FrameConverter.MessageIndexAfterHeaderForSingleDestination, "Index must equal length of message data plus header");
        }
        
        private int EncodeMsg6Resource(int index, GD92Msg6Resource resource)
        {
            index = this.EncodeString(index, resource.Callsign);
            index = this.EncodeCompressedString(index, resource.Commentary);
            return index;
        }
        
        /// <summary>
        /// This is for Msg27. For Msg6 the size is estimated beforehand.
        /// Other types fit within a single frame.
        /// </summary>
        /// <param name="index">Index at start of message text.</param>
        /// <param name="compressedLength">Length of compressed text.</param>
        private void EnsureTargetIsLargeEnough(int index, int compressedLength)
        {
            int newTargetLength = index 
                + FrameConverter.Word16Length 
                + compressedLength
                + FrameConverter.MaxForByte;
            if (newTargetLength > this.Target.Length)
            {
                this.IncreaseSizeOfTarget(newTargetLength);
            }    
        }
        
        private int CompressIntoBuffer(string inputValue)
        {
            int stringLength = inputValue.Length;
            this.uncompressedBuffer = new byte[stringLength];
            this.compressedBuffer = new byte[stringLength];
            Encoding ascii = Encoding.ASCII;
            ascii.GetBytes(inputValue, 0, stringLength, this.uncompressedBuffer, 0);
            return this.CompressBytesInBuffer();
        }
        
        private int CompressBytesInBuffer()
        {
            int compressedIndex = 0;
            int uncompressedLength = this.uncompressedBuffer.Length;
            for (int allIndex = 0; allIndex < uncompressedLength;)
            {
                byte byteValue = this.uncompressedBuffer[allIndex];
                if (byteValue == (byte)AsciiChar.ESC)
                {
                    this.compressedBuffer[compressedIndex++] = (byte)AsciiChar.ESC;
                    this.compressedBuffer[compressedIndex++] = (byte)AsciiChar.ESC;
                    this.compressedBuffer[compressedIndex++] = 1;
                    allIndex++;
                }
                else
                {
                    int byteCount = this.CountRepeatedBytesInUncompressedBuffer(allIndex, byteValue);
                    if (byteCount > FrameConverter.MaxUncompressedRepeats)
                    {
                        this.compressedBuffer[compressedIndex++] = (byte)AsciiChar.ESC;
                        this.compressedBuffer[compressedIndex++] = byteValue;
                        this.compressedBuffer[compressedIndex++] = (byte)byteCount;
                    }
                    else
                    {
                        for (int count = 0; count < byteCount; count++)
                        {
                            this.compressedBuffer[compressedIndex++] = byteValue;
                        }
                    }
                    
                    allIndex += byteCount;
                }
            }
            
            return compressedIndex;
        }
        
        private int CountRepeatedBytesInUncompressedBuffer(int allIndex, byte byteValue)
        {
            int byteCount = 0;
            int uncompressedLength = this.uncompressedBuffer.Length;
            for (int byteIndex = allIndex; byteIndex < uncompressedLength; byteIndex++)
            {
                if (this.uncompressedBuffer[byteIndex] == byteValue)
                {
                    byteCount++;
                }
                else
                {
                    break;
                }
                
                if (byteCount == FrameConverter.MaxForByte)
                {
                    break;
                }
            }
            
            return byteCount;
        }

        #endregion
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for MobiliseCommand message (type 1).
    /// </summary>
    internal class Msg1Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg1Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">Frame provider.</param>
        internal Msg1Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.MobiliseCommand, "Message type must be 1");
            var mobiliseCommand = message as GD92Msg1;
            System.Diagnostics.Debug.Assert(mobiliseCommand != null, "Message type must be 1");
            if (mobiliseCommand == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 1");
            }
            else
            {
                int index = 0;
                index = this.EncodeWord16(index, (int)mobiliseCommand.OpPeripherals);
                index = this.EncodeBoolian(index, mobiliseCommand.ManualAck);
                this.TargetDataLength = index;
                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

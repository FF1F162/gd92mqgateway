﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Encoder for Alerter Status message (type 42).
    /// </summary>
    internal class Msg42Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg42Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg42Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.AlertStatus, "Message type must be 42");
            var alertStatus = message as GD92Msg42;
            System.Diagnostics.Debug.Assert(alertStatus != null, "Message type must be 42");
            if (alertStatus == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 42");
            }
            else
            {
                int index = 0;
                index = this.EncodeFixedLengthString(index, alertStatus.AlerterStatus, GD92Message.AlertStatusLength);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }

            return frames;
        }
    }
}

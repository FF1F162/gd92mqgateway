﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Printer Status message (type 65).
    /// </summary>
    internal class Msg65Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg65Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg65Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.PrinterStatus, "Message type must be 65");
            var printerStatus = message as GD92Msg65;
            System.Diagnostics.Debug.Assert(printerStatus != null, "Message type must be 65");
            if (printerStatus == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 65");
            }
            else
            {
                int index = 0;
                index = this.EncodeWord8(index, (int)printerStatus.PrinterStatus);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

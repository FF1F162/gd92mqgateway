﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Encoder for Mobilise message (type 2) - includes overrides for multi-block
    /// This version of Msg2Encoder encodes elements of the address, as does MOBS and GD-92
    /// </summary>
    internal class Msg2MOBSEncoder : Msg2Encoder, IMessageEncoder
    {
        internal Msg2MOBSEncoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }
        protected override int EstimateAddressLength(GD92Msg2 mobMessage)
        {
            int count = 0;
            count += mobMessage.Address.Length;
            count += mobMessage.HouseNumber.Length;
            count += mobMessage.Street.Length;
            count += mobMessage.SubDistrict.Length;
            count += mobMessage.District.Length;
            count += mobMessage.Town.Length;
            count += mobMessage.County.Length;
            count += mobMessage.Postcode.Length;
            return count;
        }
        protected override int EncodeAddressInTargetArray(int index, GD92Msg2 mobMessage)
        {
            index = EncodeCompressedString(index, mobMessage.Address);
            index = EncodeString(index, mobMessage.HouseNumber);
            index = EncodeCompressedString(index, mobMessage.Street);
            index = EncodeCompressedString(index, mobMessage.SubDistrict);
            index = EncodeCompressedString(index, mobMessage.District);
            index = EncodeCompressedString(index, mobMessage.Town);
            index = EncodeCompressedString(index, mobMessage.County);
            index = EncodeString(index, mobMessage.Postcode);
            return index;
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for AlertCrew message (type 40).
    /// </summary>
    internal class Msg40Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg40Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg40Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        /// <summary>
        /// Encode data from the message object
        /// Assume fields have been validated
        /// Write data into the (base) Target byte array then copy into frame(s).
        /// </summary>
        /// <param name="message">Message to encode.</param>
        /// <returns>Array containing frame.</returns>
        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.AlertCrew, "Message type must be 40");
            var alertCrew = message as GD92Msg40;
            System.Diagnostics.Debug.Assert(alertCrew != null, "Message type must be 40");
            if (alertCrew == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 40");
            }
            else
            {
                int index = 0;
                index = this.EncodeFixedLengthString(index, alertCrew.AlertGroup, GD92Message.AlertGroupLength);
                index = this.EncodeBoolian(index, alertCrew.ManualAck);
                index = this.EncodeWord16(index, (int)alertCrew.OpPeripherals);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

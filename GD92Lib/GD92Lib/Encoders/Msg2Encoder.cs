﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Encoder for Mobilise message (type 2) - includes overrides for multi-block.
    /// </summary>
    internal class Msg2Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg2Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg2Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        /// <summary>
        /// Encode data from the message object (nothing new to be added here)
        /// Assume fields have been validated
        /// Write data into the (base) Target byte array then copy into frame(s).
        /// </summary>
        /// <param name="message">Message to encode.</param>
        /// <returns>Array of frames comprising message.</returns>
        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.MobiliseMessage, "Message type must be 2");
            var mobMessage = message as GD92Msg2;
            System.Diagnostics.Debug.Assert(mobMessage != null, "Message type must be 2");
            if (mobMessage == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 2");
            }
            else
            {
                this.EnsureTargetIsLargeEnough(mobMessage);
                this.EncodeFieldsInTargetArray(mobMessage);

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
        
        protected virtual int EncodeAddressInTargetArray(int index, GD92Msg2 mobMessage)
        {
            index = this.EncodeCompressedString(index, mobMessage.Address);
            index = this.EncodeString(index, mobMessage.HouseNumber);
            index = this.EncodeCompressedString(index, mobMessage.Street);
            index = this.EncodeCompressedString(index, mobMessage.SubDistrict);
            index = this.EncodeCompressedString(index, mobMessage.District);
            index = this.EncodeCompressedString(index, mobMessage.Town);
            index = this.EncodeCompressedString(index, mobMessage.County);
            index = this.EncodeString(index, mobMessage.Postcode);
            return index;
        }

        /// <summary>
        /// Allow for Block, OfBlocks and ManAck fields prepending the message data in every frame.
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <returns>Length of all message data.</returns>
        protected override int CalculateFrameMessageLength(Frame frame)
        {
            return FrameConverter.BlockOfBlocksLength + FrameConverter.ManAckRequiredLength + frame.FrameDataLength;
        }
        
        /// <summary>
        /// Encode Block OfBlocks in every frame
        /// For sending to RSC encode manual acknowledgement required field in every frame.
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <param name="index">Index into FrameBytes for writing the next byte.</param>
        /// <returns>Index for writing the first byte of actual message data.</returns>
        protected override int EncodeFieldsBeforeMessageData(Frame frame, int index)
        {
            index = frame.EncodeBlockOfBlocks(index);
            index = CalculateAndEncodeManAckRequired(frame, index);
            return index;
        }

        protected virtual int EstimateAddressLength(GD92Msg2 mobMessage)
        {
            int count = 0;
            count += mobMessage.Address.Length;
            count += mobMessage.HouseNumber.Length;
            count += mobMessage.Street.Length;
            count += mobMessage.SubDistrict.Length;
            count += mobMessage.District.Length;
            count += mobMessage.Town.Length;
            count += mobMessage.County.Length;
            count += mobMessage.Postcode.Length;
            return count;
        }
        
        private static int EstimateCallsignListLength(Collection<string> callsignList)
        {
            int count = 1; // for number of callsigns
            foreach (string callsign in callsignList)
            {
                count += callsign.Length;
            }
            
            return count;
        }
        
        /// <summary>
        /// Only the last frame has manual acknowledgement set and then only if it is required for the message
        /// Assert that the field in the frame has not been set somewhere else in the code.
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <param name="index">Index of man ack field.</param>
        /// <returns>Index after man ack field.</returns>
        private static int CalculateAndEncodeManAckRequired(Frame frame, int index)
        {
            System.Diagnostics.Debug.Assert(frame.Message != null, "Message must not be null");
            System.Diagnostics.Debug.Assert(!frame.ManAckRequired, "Man ack required must not be set previously");
            if (frame.Message != null && frame.Message.ManualAck && frame.Block == frame.OfBlocks)
            {
                frame.ManAckRequired = true;
            }
            
            index = frame.EncodeManAckRequired(index);
            return index;
        }
        
        private void EnsureTargetIsLargeEnough(GD92Msg2 mobMessage)
        {
            int size = this.EstimateUncompressedLength(mobMessage);
            if (size > this.Target.Length)
            {
                this.IncreaseSizeOfTarget(size);
            }
        }
        
        private int EstimateUncompressedLength(GD92Msg2 mobMessage)
        {
            int count = 100; // margin for string wordcount etc
            count += FrameConverter.IncidentDateTimeFormat.Length; // mobMessage.MobTimeAndDate;
            count += EstimateCallsignListLength(mobMessage.CallsignList);
            count += mobMessage.IncidentNumber.Length;
            count += 1; // mobMessage.MobilisationType
            count += this.EstimateAddressLength(mobMessage);
            count += mobMessage.MapRef.Length;
            count += mobMessage.TelNumber.Length;
            count += mobMessage.Text.Length;
            return count;
        }
        
        private void EncodeFieldsInTargetArray(GD92Msg2 mobMessage)
        {
            int index = 0;
            index = this.EncodeTimeAndDate(index, mobMessage.MobTimeAndDate);
            index = this.EncodeCallsignList(index, mobMessage.CallsignList);
            index = this.EncodeIncidentNumber(index, mobMessage.IncidentNumber);
            index = this.EncodeWord8(index, mobMessage.MobilisationType);
            index = this.EncodeAddressInTargetArray(index, mobMessage);
            index = this.EncodeString(index, mobMessage.MapRef);
            index = this.EncodeString(index, mobMessage.TelNumber);
            index = this.EncodeLongCompressedString(index, mobMessage.Text);
            this.TargetDataLength = index;
        }
    }
}

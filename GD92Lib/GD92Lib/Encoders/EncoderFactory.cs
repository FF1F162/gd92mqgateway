﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Provide a method to return an encoder of the right type.
    /// </summary>
    internal class EncoderFactory
    {
        private readonly IFrameProvider frameProvider;

        /// <summary>
        /// Initialises a new instance of the <see cref="EncoderFactory"/> class.
        /// The frame provider has methods that return an array of frames initialized with
        /// data that cannot be provided by the message class (each frame in a new message requires 
        /// a serial number and a reply needs data from the last frame of the received message).
        /// The encoder completes the frame using data from the message (each type has different fields).
        /// </summary>
        /// <param name="frameProvider">Provider of frame array with serial number or reply data.</param>
        internal EncoderFactory(IFrameProvider frameProvider)
        {
            this.frameProvider = frameProvider;
        }

        /// <summary>
        /// Return an encoder corresponding to the message type.
        /// </summary>
        /// <param name="message">Message to be encoded.</param>
        /// <returns>Encoder for message type.</returns>
        internal IMessageEncoder EncoderForMessageType(GD92Message message)
        {
            GD92MsgType messageType = message.MsgType;
            IMessageEncoder encoder = null;
            switch (messageType)
            {
                case GD92MsgType.MobiliseCommand:
                    encoder = new Msg1Encoder(this.frameProvider);
                    break;
                case GD92MsgType.MobiliseMessage:
                    encoder = new Msg2Encoder(this.frameProvider);
                    break;
                case GD92MsgType.ResourceStatusRequest:
                    encoder = new Msg5Encoder(this.frameProvider);
                    break;
                case GD92MsgType.MobiliseMessage6:
                    encoder = new Msg6Encoder(this.frameProvider);
                    break;
                case GD92MsgType.ResourceStatus:
                    encoder = new Msg20Encoder(this.frameProvider);
                    break;
                case GD92MsgType.LogUpdate:
                    encoder = new Msg22Encoder(this.frameProvider);
                    break;
                case GD92MsgType.Text:
                    encoder = new Msg27Encoder(this.frameProvider);
                    break;
                case GD92MsgType.PeripheralStatus:
                    encoder = new Msg28Encoder(this.frameProvider);
                    break;
                case GD92MsgType.AlertCrew:
                    encoder = new Msg40Encoder(this.frameProvider);
                    break;
                case GD92MsgType.AlertStatus:
                    encoder = new Msg42Encoder(this.frameProvider);
                    break;
                case GD92MsgType.Ack:
                    encoder = new Msg50Encoder(this.frameProvider);
                    break;
                case GD92MsgType.Nak:
                    encoder = new Msg51Encoder(this.frameProvider);
                    break;
                case GD92MsgType.PrinterStatus:
                    encoder = new Msg65Encoder(this.frameProvider);
                    break;
                case GD92MsgType.RawFrame:
                    encoder = new RawFrameMsgEncoder(this.frameProvider);
                    break;
                default:
                    throw new GD92LibEncoderException(string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Encoder not available for message type {0}",
                        messageType));
            }
            
            return encoder;
        }
    }
}

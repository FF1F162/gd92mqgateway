﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Resource Status Request message (type 5) 
    /// GD-92 allows for more than one callsign in the message, but we ask for one status at a time.
    /// </summary>
    internal class Msg5Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg5Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">FrameProvider.</param>
        internal Msg5Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.ResourceStatusRequest, "Message type must be 5");
            var resourceStatusRequest = message as GD92Msg5;
            System.Diagnostics.Debug.Assert(resourceStatusRequest != null, "Message type must be 5");
            if (resourceStatusRequest == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 5");
            }
            else
            {
                int index = 0;
                index = this.EncodeString(index, resourceStatusRequest.Callsign);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

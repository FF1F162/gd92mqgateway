﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
	using System;
    using System.Configuration;
	
	/// <summary>
	/// Encoder for Resource Status Request message (type 5) 
    /// The reply is expected to be type 20 with the same sequence number.
    /// Storm now provides this, but if the ack_req flag is set it also sends type 50
	/// </summary>
    internal class Msg5ToStormEncoder : Msg5Encoder
    {
        internal Msg5ToStormEncoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        protected override bool AckRequiredSettingForNewMessage()
        {
            return AckRequiredFromConfigOrDefault();
        }
        private static bool AckRequiredFromConfigOrDefault()
        {
            string appSetting = ConfigurationManager.AppSettings["AckRequiredForMsg5"] ?? "true";
            return !(appSetting.Equals("false", StringComparison.OrdinalIgnoreCase) ||
                     appSetting.Equals("0", StringComparison.OrdinalIgnoreCase));
        }
    }
}

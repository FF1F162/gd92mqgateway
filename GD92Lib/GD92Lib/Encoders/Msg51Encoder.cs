﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Nak message (type 51).
    /// </summary>
    internal class Msg51Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg51Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">Frame provider.</param>
        internal Msg51Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.Nak, "Message type must be 51");
            var nakMessage = message as GD92Msg51;
            System.Diagnostics.Debug.Assert(nakMessage != null, "Message type must be 51");
            if (nakMessage == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 51");
            }
            else
            {
                int index = 0;
                index = this.EncodeWord8(index, 1); // destination count
                index = this.EncodeCommsAddress(index, new PortAddress(nakMessage.OriginalDestination));
                index = this.EncodeWord8(index, (int)nakMessage.ReasonCodeSet);
                index = this.EncodeWord8(index, nakMessage.ReasonCode);
                this.TargetDataLength = index;
                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
    }
}

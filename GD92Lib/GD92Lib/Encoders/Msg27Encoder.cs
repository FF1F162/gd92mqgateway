﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    
    /// <summary>
    /// Encoder for Text message (type 27) - includes overrides for multi-block
    /// Note that this class is probably not suitable for sending multi-block text to Storm
    /// Instead of a single overall count in block 1, Storm probably requires a count for each block.
    /// </summary>
    internal class Msg27Encoder : Encoder, IMessageEncoder
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg27Encoder"/> class.
        /// </summary>
        /// <param name="frameProvider">Frame provider.</param>
        internal Msg27Encoder(IFrameProvider frameProvider)
            : base(frameProvider)
        {
        }

        public Frame[] EncodeMessage(GD92Message message)
        {
            Frame[] frames;
            System.Diagnostics.Debug.Assert(message.MsgType == GD92MsgType.Text, "Message type must be 27");
            var textMessage = message as GD92Msg27;
            System.Diagnostics.Debug.Assert(textMessage != null, "Message type must be 27");
            if (textMessage == null)
            {
                throw new GD92LibEncoderException("Message was null after cast to type 27");
            }
            else
            {
                int index = 0;
                index = this.EncodeLongCompressedString(index, textMessage.Text);
                this.TargetDataLength = index;

                frames = this.BuildMessageFrames(message);
            }
            
            return frames;
        }
        
        /// <summary>
        /// Allow for Block and OfBlocks fields prepending the message data in every frame.
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <returns>Length of all message data.</returns>
        protected override int CalculateFrameMessageLength(Frame frame)
        {
            return FrameConverter.BlockOfBlocksLength + frame.FrameDataLength;
        }
        
        /// <summary>
        /// Encode Block OfBlocks in every frame.
        /// </summary>
        /// <param name="frame">Frame being encoded.</param>
        /// <param name="index">Index into FrameBytes for writing the next byte.</param>
        /// <returns>Index for writing the first byte of actual message data.</returns>
        protected override int EncodeFieldsBeforeMessageData(Frame frame, int index)
        {
            return frame.EncodeBlockOfBlocks(index);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Threading;
    using Thales.KentFire.EventLib;
    using System.IO;


    /// <summary>
    /// Singleton class that encompasses GD-92 router, MTAs and User Agent. 
    /// Includes control of EventHandler activation.
    /// </summary>
    public sealed class GD92Component : IGD92Component, IGD92MessageSender, IProcessEvent, IDisposable
    {
        public const int StatusFail = -999;
        public const int MaxSerialNumber = FrameConverter.MaxFrameSequenceNumber;
        public const string LocalRouterToken = "LocalRouter";
        public const string NodeNamePrefix = "Node";
        public const string MutexName = @"Global\CKentRSCCtrl";

        private static readonly GD92Component Self = new GD92Component();
        private static readonly ISerialNumberProvider SerialNumberProvider = new SerialNumberProvider(); 
        private static string logName = "GD92Component";
        private static Mutex mutex;

        public event EventHandler<LogEventArgs> Log;

        public event EventHandler<MessageReceivedEventArgs> MessageReceived;

        public event EventHandler<ReplyReceivedEventArgs> ReplyReceived;

        public event EventHandler<NodeConnectedEventArgs> NodeConnected;

        public event EventHandler<NodeConnectionLostEventArgs> NodeConnectionLost;

        // toggled by Activate() Deactivate() methods
        private bool active;
        private bool starting;
        private bool stopping;

        private Timer activationTimer;

        /// <summary>
        /// Prevents a default instance of the <see cref="GD92Component"/> class from being created.
        /// Do the minimum here
        /// No need to set up the event queue as it is a static member of EventQueue.
        /// Processing the event queue is started by Activate and stopped by DeActivate. 
        /// In the RSC the event queue is serviced by the main Windows thread via TestDialog, 
        /// which is set up in the constructor. Here we use the Timer to allocate a pool thread.
        /// The container must call Activate to get things working and should check for SUCCESS
        /// then wait for the first log event and check that the component is Activated.
        /// </summary>
        private GD92Component()
        {
        }

        /// <summary>
        /// Provide access to members of the singleton.
        /// </summary>
        /// <value>Singleton instance.</value>
        public static GD92Component Instance
        {
            get
            {
                return Self;
            }
        }

        /// <summary>
        /// Name of this node - so a gateway handler can detect a directly-addressed message.
        /// </summary>
        /// <value>Local node name.</value>
        public static string LocalNodeName 
        {
            get
            {
                return UserAgent.Instance.NodeName;
            }
        }

        /// <summary>
        /// Get or set the log event level.
        /// </summary>
        /// <value>Log event level.</value>
        public LogLevel LogEventLevel
        {
            get
            {
                return EventControl.LogEventLevel;
            }

            set
            {
                EventControl.LogEventLevel = value;
            }
        }

        /// <summary>
        /// Logger for reporting to external monitoring e.g. Nagios via Windows Event Log.
        /// </summary>
        /// <value>Instance of logger.</value>
        public IExternalEventLogger EventLogger { get; set; }

        /// <summary>
        /// True if the component is activated (not starting, stopping or deactivated)
        /// either with FirstMta or with all MTAs activated.
        /// </summary>
        /// <value>True if activated.</value>
        public bool Activated
        {
            get
            {
                return this.active && !this.starting && !this.stopping;
            }
        }

        #region Static methods

        public static void QueueReplyReceivedEvent(GD92Message replyMessage)
        {
            var args = new ReplyReceivedEventArgs(replyMessage);
            var item = new EventQueueItem(EventType.ReplyReceived, args);
            EventQueueHolder.QueueEvent(item);
        }

        public static void QueueMessageReceivedEvent(GD92Message receivedMessage)
        {
            var args = new MessageReceivedEventArgs(receivedMessage);
            var item = new EventQueueItem(EventType.MessageReceived, args);
            EventQueueHolder.QueueEvent(item);
        }

        public static void QueueNodeConnectedEvent(string nodeName, string mtaName)
        {
            var args = new NodeConnectedEventArgs(nodeName, mtaName);
            var item = new EventQueueItem(EventType.NodeConnected, args);
            EventQueueHolder.QueueEvent(item);
        }

        public static void QueueNodeConnectionLostEvent(string nodeName, string mtaName)
        {
            var args = new NodeConnectionLostEventArgs(nodeName, mtaName);
            var item = new EventQueueItem(EventType.NodeConnectionLost, args);
            EventQueueHolder.QueueEvent(item);
        }

        public static void QueueNodeConnectionLostEvent(string nodeName, string mtaName, string reason)
        {
            var args = new NodeConnectionLostEventArgs(nodeName, mtaName, reason);
            var item = new EventQueueItem(EventType.NodeConnectionLost, args);
            EventQueueHolder.QueueEvent(item);
        }

        #endregion

        /// <summary>
        /// Activate the component using a new thread.
        /// Use mutex to detect if another instance (or the RSC) is already running.
        /// </summary>
        /// <returns>Control result.</returns>
        public ControlResult Activate()
        {
            ControlResult result = ControlResult.NoAction;
            try
            {
                if ((!this.active || mutex == null) && !this.stopping && !this.starting)
                {
                    this.active = true;
                    this.starting = true;
                    bool first = IsFirstInstance();
                    if (first)
                    {
                        Debug.Assert(this.activationTimer == null, "Timer must be null - not already set");
                        this.activationTimer = new Timer(this.ProcessStartup, null, 0, 0);
                        result = ControlResult.Success;
                    }
                    else
                    {
                        this.active = false;
                        this.starting = false;
                        result = ControlResult.AlreadyEnabled;
                    }
                }
            }
            catch (Exception e)
            {
                this.active = false;
                this.starting = false;
                result = ControlResult.ExceptionCaught;
                EventControl.Emergency(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Activate caught exception - {0}", 
                    e.ToString()));
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }

            return result;
        }

        /// <summary>
        /// Activate the bearers in any MTA that has not already been activated.
        /// </summary>
        public void ActivateFully()
        {
            if (this.active)
            {
                Router.Instance.ActivateMtas();
            }
        }

        /// <summary>
        /// Use a timer thread to shut down the component.
        /// </summary>
        /// <returns>Control result.</returns>
        public ControlResult Deactivate()
        {
            ControlResult result = ControlResult.NoAction;
            try
            {
                if (this.active && !this.stopping && !this.starting)
                {
                    this.stopping = true;
                    Debug.Assert(this.activationTimer == null, "Timer must be null - not already set");
                    this.activationTimer = new Timer(this.ProcessShutdown, null, 0, 0);
                    ReleaseMutex();
                    result = ControlResult.Success;
                }
            }
            catch (Exception e)
            {
                result = ControlResult.ExceptionCaught;
                EventControl.Emergency(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Deactivate caught exception - {0}", 
                    e.ToString()));
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }

            return result;
        }

        /// <summary>
        /// Reactivate the component by shutting down directly (leaving mutex and event handling in place)
        /// then activate as normal but without using a timer thread.
        /// Check that the component is activated (this prevents restart during shutdown).
        /// </summary>
        public void Reactivate()
        {
            if (this.Activated)
            {
                DeactivateAndRemoveConfiguration();
                this.starting = true;
                this.ProcessStartup();
            }
        }

        /// <summary>
        /// Schedule reactivation if there is a first MTA configured and the name matches the MTA in the event arguments.
        /// </summary>
        /// <param name="eventArgs">Details of connection lost event.</param>
        /// <returns>True if restart is scheduled.</returns>
        public bool ScheduleRestartIfThisIsFirstMta(NodeConnectionLostEventArgs eventArgs)
        {
            bool restart = false;
            if (this.Activated)
            {
                string firstMta = UserAgent.Instance.GetFirstMtaToActivate();
                if (eventArgs.MtaName.Equals(firstMta, StringComparison.Ordinal))
                {
                    FirstMta.Instance.ScheduleRestart(eventArgs);
                    restart = true;
                }
            }

            return restart;
        }

        /// <summary>
        /// Cancel reactivation if there is a first MTA configured and the name matches the MTA in the event arguments.
        /// </summary>
        /// <param name="eventArgs">Details of connection event.</param>
        /// <returns>True if restart is cancelled.</returns>
        public bool CancelRestartIfThisIsFirstMta(NodeConnectedEventArgs eventArgs)
        {
            bool restartCancelled = false;
            if (this.Activated)
            {
                string firstMta = UserAgent.Instance.GetFirstMtaToActivate();
                if (eventArgs.MtaName.Equals(firstMta, StringComparison.Ordinal))
                {
                    restartCancelled = FirstMta.Instance.CancelRestart(eventArgs);
                }
            }

            return restartCancelled;
        }

        /// <summary>
        /// Clean up any resources being used directly.
        /// </summary>
        public void Dispose()
        {
            if (mutex != null)
            {
                mutex.ReleaseMutex();
                mutex.Dispose();
                mutex = null;
            }

            if (this.activationTimer != null)
            {
                this.activationTimer.Dispose();
                this.activationTimer = null;
            }
        }

        /// <summary>
        /// Timestamp the message, give it a serial number and make public properties read-only.
        /// </summary>
        /// <param name="message">Message to be sent.</param>
        /// <returns>New serial number for reference to the message.</returns>
        public int SendMessage(GD92Message message)
        {
            TestLogger("SendMessage");

            int serialNumber = StatusFail;
            if (this.active && !this.stopping && !this.starting)
            {
                try
                {
                    CompleteAddressingInformation(message);
                    message.SentAt = DateTime.UtcNow;
                    serialNumber = SerialNumberProvider.GetSerialNumber();
                    TestLogger(" message.SentAt " + message.SentAt.ToString());
                    TestLogger(" serialNumber " + serialNumber.ToString());

                    if (serialNumber != StatusFail)
                    {
                        message.SetSerialNumber(serialNumber);
                        message.SetReadOnly();
                        Node destinationNode = Router.Instance.GetNodeByNameOrDefault(message.Destination);
                        Frame[] frames = FrameConverter.BuildFramesFromMessage(message);
                        TestLogger(" frames length " + frames.Length.ToString());
                        if (frames != null && frames.GetLength(0) > 0)
                        {
                            LogMessageSend(message);
                            Router.Instance.SetRoutes(frames);
                            destinationNode.TxQueue.Send(frames);
                        }
                        else
                        {
                            LogMessageSendFailure(message);
                            serialNumber = StatusFail;
                        }
                    }
                    else
                    {
                        TestLogger(" status fail");
                    }
                }
                catch (GD92LibException e)
                {
                    EventControl.Warn(logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Message {0} SendMessage caught GD92LibException - {1}",
                        serialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture), 
                        e.Message));
                    serialNumber = StatusFail;
                }
                catch (GD92LibEncoderException e)
                {
                    EventControl.Warn(logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Message {0} SendMessage encoding failed - {1}",
                        serialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture), 
                        e.Message));
                    serialNumber = StatusFail;
                }
                catch (Exception e)
                {
                    EventControl.Emergency(logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Message {0} SendMessage caught exception - {1}",
                        serialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture), 
                        e.ToString()));
                    Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                    throw;
                }
            }

            return serialNumber;
        }

        #region IProcessEvent methods

        /// <summary>
        /// This allows GD92Component to fire events that are stored in EventLib.EventQueue
        /// Called by EventControl.ProcessEventQueue.
        /// </summary>
        /// <param name="item">Item from the event queue.</param>
        public void ProcessEvent(EventQueueItem item)
        {
            switch (item.EventType)
            {
                case EventType.Log:
                    this.FireLog(item.EventArgs as LogEventArgs);
                    break;
                case EventType.MessageReceived:
                    this.FireMessageReceived(item.EventArgs as MessageReceivedEventArgs);
                    break;
                case EventType.ReplyReceived:
                    this.FireReplyReceived(item.EventArgs as ReplyReceivedEventArgs);
                    break;
                case EventType.NodeConnected:
                    this.FireNodeConnected(item.EventArgs as NodeConnectedEventArgs);
                    break;
                case EventType.NodeConnectionLost:
                    this.FireNodeConnectionLost(item.EventArgs as NodeConnectionLostEventArgs);
                    break;
                default:
                    Debug.Assert(false, "Event type is not defined");
                    break;
            }
        }

        #endregion

        public int GetNumberOfQueuedFrames()
        {
            int numberOfQueuedMessages = StatusFail;
            try
            {
                if (this.active && !this.stopping && !this.starting)
                {
                    numberOfQueuedMessages = Router.Instance.GetFrameCountForBearerQueues();
                    EventControl.Debug(logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "GetNumberOfQueuedMessages returning {0}",
                        numberOfQueuedMessages));
                }
            }
            catch (Exception e)
            {
                EventControl.Emergency(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "GetNumberOfQueuedFrames caught exception - {0}", 
                    e.ToString()));
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }

            return numberOfQueuedMessages;
        }

        public ControlResult DisableBearer(string nodeName, string bearerType)
        {
            return this.ControlBearer(ControlType.DisableBearer, nodeName, bearerType);
        }

        public ControlResult EnableBearer(string nodeName, string bearerType)
        {
            return this.ControlBearer(ControlType.EnableBearer, nodeName, bearerType);
        }

        public ControlResult DisconnectBearer(string nodeName, string bearerType)
        {
            return this.ControlBearer(ControlType.DisconnectBearer, nodeName, bearerType);
        }

        public ControlResult ConnectBearer(string nodeName, string bearerType)
        {
            return this.ControlBearer(ControlType.ConnectBearer, nodeName, bearerType);
        }

        public ControlResult DisableBearer(string bearerName)
        {
            return this.ControlBearer(ControlType.DisableBearer, bearerName);
        }

        public ControlResult EnableBearer(string bearerName)
        {
            return this.ControlBearer(ControlType.EnableBearer, bearerName);
        }

        public ControlResult DisconnectBearer(string bearerName)
        {
            return this.ControlBearer(ControlType.DisconnectBearer, bearerName);
        }

        public ControlResult ConnectBearer(string bearerName)
        {
            return this.ControlBearer(ControlType.ConnectBearer, bearerName);
        }

        internal ControlResult ControlBearer(ControlType action, string bearerName)
        {
            EventControl.Debug(logName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "{0} called for {1}", 
                action.ToString(), 
                bearerName));
            return this.DoControlBearer(action, bearerName);
        }

        internal ControlResult ControlBearer(ControlType action, string node, string mta)
        {
            EventControl.QueueLogEvent(LogLevel.Debug, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "{0} called for node {1} mta {2}",
                action.ToString(), 
                node, 
                mta));
            return this.DoControlBearer(action, node + " " + mta);
        }

        internal ControlResult DoControlBearer(ControlType action, string bearerName)
        {
            ControlResult result = ControlResult.NoAction;
            Bearer bearer = null;
            try
            {
                if (this.active && !this.stopping && !this.starting)
                {
                    bearer = Router.Instance.GetBearer(bearerName);
                    switch (action)
                    {
                        case ControlType.ConnectBearer:
                            result = bearer.Connect();
                            break;
                        case ControlType.DisconnectBearer:
                            result = bearer.Disconnect();
                            break;
                        case ControlType.EnableBearer:
                            result = bearer.Enable();
                            break;
                        case ControlType.DisableBearer:
                            result = bearer.Disable();
                            break;
                        default:
                            Debug.Assert(false, "Action is not defined");
                            break;
                    }
                }
            }
            catch (KeyNotFoundException e)
            {
                EventControl.Warn(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "{0} {1} unknown bearer - {2}",
                    action.ToString(), 
                    bearerName, 
                    e.Message));
                result = ControlResult.UnknownBearer;
            }
            catch (Exception e)
            {
                EventControl.Emergency(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "{0} {1} caught exception - {2}",
                    action.ToString(),
                    bearerName, 
                    e.ToString()));
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }

            return result;
        }
        
        #region Internal Properties

        internal bool Active 
        { 
            get 
            { 
                return this.active; 
            } 
        }

        internal bool Starting 
        { 
            get 
            { 
                return this.starting; 
            } 
        }

        internal bool Stopping 
        { 
            get 
            { 
                return this.stopping; 
            } 
        }

        #endregion

       /// <summary>
        /// Use Mutex to detect another instance of this component or the RSC.
        /// If another instance stopped without releasing the mutex, it will be abandoned.
        /// As we are just using it to detect another instance it is safe to take ownership.
        /// </summary>
        /// <returns>True if no other is detected.</returns>
        private static bool IsFirstInstance()
        {
            mutex = new Mutex(false, MutexName);
            bool owned = false;
            try
            {
                owned = mutex.WaitOne(TimeSpan.Zero, false);
            }
            catch (AbandonedMutexException)
            {
                mutex.ReleaseMutex();
                owned = mutex.WaitOne(TimeSpan.Zero, false);
            }

            return owned;
        }

        /// <summary>
        /// If possible, release the mutex so another instance of the component can activate.
        /// This works when the component is controlled via Activate and Deactivate buttons.
        /// But the service uses different thread so there is (always?) an application exception:
        /// 'Object synchronization method was called from an unsynchronized block of code.'
        /// This may release the mutex anyway but otherwise, when the process shuts down, 
        /// the mutex is abandoned. There is now code to deal with this
        /// so it seems safe to ignore the exception. It has never caused a problem in any case.
        /// </summary>
        private static void ReleaseMutex()
        {
            Debug.Assert(mutex != null, "Mutex is expected not to be null");
            if (mutex != null)
            {
                try
                {
                    mutex.ReleaseMutex();
                    mutex.Close();
                    mutex = null;
                }
                catch (ApplicationException)
                {
                }
            }
        }

        #region Send helper methods

        /// <summary>
        /// Supply source name and address of this node if none is supplied.
        /// Look up destination addresses, given name, or supply defaults.
        /// </summary>
        /// <param name="message">Message being sent.</param>
        private static void CompleteAddressingInformation(GD92Message message)
        {
            SupplyDefaultSourceAddress(message);
            CompleteDestinationAddress(message);
        }

        /// <summary>
        /// If source address is not set (Brigade 0), assume the message is from this node. There is no provision
        /// for looking up the source address using the From name.
        /// If From name is LocalRouterToken set port to 0 so reply appears to come from the router.
        /// </summary>
        /// <param name="message">Message being sent.</param>
        private static void SupplyDefaultSourceAddress(GD92Message message)
        {
            if (message.FromAddress.Brigade == 0)
            {
                var sourceAddress = (GD92Address)UserAgent.Instance.GD92Address;
                if (message.From != null && message.From.Equals(LocalRouterToken, StringComparison.Ordinal))
                {
                    sourceAddress.Port = 0;
                }
                else
                {
                    Debug.Assert(string.IsNullOrWhiteSpace(message.From), "No address lookup for source name");
                }

                message.From = UserAgent.Instance.NodeName;
                message.FromAddress = sourceAddress;
                message.FromNodeType = NodeType.Gateway;
            }
        }

        /// <summary>
        /// If destination name is empty, this is probably a reply.
        /// Otherwise use the name to get the address from the router.
        /// </summary>
        /// <param name="message">Message being sent.</param>
        private static void CompleteDestinationAddress(GD92Message message)
        {
            if (string.IsNullOrEmpty(message.Destination))
            {
                throw new GD92LibException("Message send failed - destination missing");
            }
            else
            {
                FindDestinationInRouter(message);
            }
        }

        /// <summary>
        /// Use the destination name to look up the address in the router.
        /// </summary>
        /// <param name="message">Message being sent.</param>
        private static void FindDestinationInRouter(GD92Message message)
        {
            Node destinationNode = Router.Instance.GetNodeByNameOrDefault(message.Destination);
            if (string.IsNullOrEmpty(destinationNode.Name))
            {
                destinationNode = Router.Instance.FindOrCreateNode(message.Destination);
            }

            if (string.IsNullOrEmpty(destinationNode.Name))
            {
                throw new GD92LibException("Message send failed - unknown destination name " + message.Destination);
            }
            else
            {
                message.DestAddress = (GD92Address)destinationNode.Address;
                message.DestNodeType = destinationNode.GD92Type;
            }
        }

        /// <summary>
        /// Log message type and serial number.
        /// </summary>
        /// <param name="message">Message sent.</param>
        private static void LogMessageSend(GD92Message message)
        {
            if (message.OriginalMessage != null)
            {
                EventControl.Info(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Sending message type {0} serial number {1} in reply to {2}",
                    message.MsgType.ToString(),
                    message.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    message.OriginalMessage.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }
            else
            {
                EventControl.Info(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Sending message type {0} serial number {1}",
                    message.MsgType.ToString(),
                    message.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }
        }

        /// <summary>
        /// Log failure to send message as a warning.
        /// </summary>
        /// <param name="message">Message not sent.</param>
        private static void LogMessageSendFailure(GD92Message message)
        {
            EventControl.Warn(logName, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Failed to send message type {0} serial number {1}",
                message.MsgType.ToString(),
                message.SerialNumber.ToString(System.Globalization.CultureInfo.InvariantCulture)));
        }

        #endregion

        #region Methods called by timer

        /// <summary>
        /// Close all connections and remove configuration
        /// to complete.
        /// </summary>
        private static void DeactivateAndRemoveConfiguration()
        {
            UserAgent.Instance.Deactivate();
            Router.Instance.DeactivateAndRemoveAllMtas();
            Router.Instance.RemoveAllNodes();
        }

        /// <summary>
        /// Check flags, read configuration and start up (method signature for call by timer).
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ProcessStartup(object state)
        {
           // activationTimer.Dispose();
            this.ProcessStartup();
        }

        /// <summary>
        /// Check flags, read configuration and start up.
        /// </summary>
        private void ProcessStartup()
        {
            Debug.Assert(this.Active == true, "Flag must be set true");
            Debug.Assert(this.Starting == true, "Flag must be set true");
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                EventControl.Alert(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Activating {0}", 
                    assembly.GetName().ToString()));
                Router.Instance.ReadMtas();
                Router.Instance.ReadConnections();
                Router.Instance.ReadNodes();
                UserAgent.Instance.ReadConfiguration();
                UserAgent.Instance.Activate(SerialNumberProvider);
                EventControl.Alert(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "This node is {0} address {1}", 
                    UserAgent.Instance.NodeName, 
                    UserAgent.Instance.GD92Address.ToString()));
                Node gateway = UserAgent.Instance.GetGatewayNode();
                if (gateway != null)
                {
                    EventControl.Alert(logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "Gateway is {0} node {1}", 
                        gateway.Name, 
                        gateway.NodeAddress.ToString()));
                }

                string firstMta = UserAgent.Instance.GetFirstMtaToActivate();
                if (string.IsNullOrEmpty(firstMta))
                {
                    Router.Instance.ActivateMtas();
                }
                else
                {
                    Router.Instance.ActivateMta(firstMta);
                }

                this.starting = false;
                this.activationTimer = null;
            }
            catch (Exception e)
            {
                EventControl.Emergency(logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Cannot activate component - {0}",
                    e.ToString()));
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }
        }

        /// <summary>
        /// Close connections and remove configuration.
        /// Sleep for 2 seconds (i.e. 2 x EventQueueTimeSpan) to allow for writing logs
        /// before setting flags and exit (or before rethrowing an exception).
        /// Do not stop all event handling - it is not started by this class so don't stop it.
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ProcessShutdown(object state)
        {
            Debug.Assert(this.Active == true, "Flag must be set true");
            Debug.Assert(this.Stopping == true, "Flag must be set true");
            try
            {
                EventControl.Alert(logName, "Shutting down - please allow a few seconds");
                FirstMta.Instance.CancelRestart();
                DeactivateAndRemoveConfiguration();

                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
            }
            catch (Exception e)
            {
                EventControl.Emergency(logName, string.Format(System.Globalization.CultureInfo.InvariantCulture, "Error during shut down - {0}", e.ToString()));
                Thread.Sleep(EventControl.EventQueueTimeSpan * 2);
                throw;
            }
            finally
            {
                this.stopping = false;
                this.active = false;
                this.activationTimer = null;
            }
        }

        #endregion

        private void FireLog(LogEventArgs e)
        {
            if (this.Log != null)
            {
                this.Log(this, e);
            }
        }

        private void FireMessageReceived(MessageReceivedEventArgs e)
        {
            if (this.MessageReceived != null)
            {
                this.MessageReceived(this, e);
            }
        }

        private void FireReplyReceived(ReplyReceivedEventArgs e)
        {
            if (this.ReplyReceived != null)
            {
                this.ReplyReceived(this, e);
            }
        }

        private void FireNodeConnected(NodeConnectedEventArgs e)
        {
            if (this.NodeConnected != null)
            {
                this.NodeConnected(this, e);
            }
        }

        private void FireNodeConnectionLost(NodeConnectionLostEventArgs e)
        {
            if (this.NodeConnectionLost != null)
            {
                this.NodeConnectionLost(this, e);
            }
        }

        const string LogFileName = @"C:\POCLogs\GD92Component.txt";
        object logLockObject = new object();
        public void TestLogger(string message)
        {
            lock (logLockObject)
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(DateTime.UtcNow.ToString() + " : " + message);
                }
            }
        }
    }
}

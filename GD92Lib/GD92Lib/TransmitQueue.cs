﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Thales.KentFire.EventLib;
    using System.IO;



    /// <summary>
    /// This queues messages (in one or more frames) for transmission to ports on a node. 
    /// There is a separate queue for each port and for each priority on that port.
    /// Up to three frames from a message can be sent immediately to each port. If reply is expected, the last frame
    /// is added to the queue to show transmission in progress. When a reply arrives there is a call to GetNextFrameToSend
    /// to remove the acknowledged frame and send the next (if any).
    /// </summary>
    internal class TransmitQueue
    {
        internal const int MaxFramesInFlight = 3;
        
        private readonly string logName;
        private readonly object transmitQueueLock;
        private readonly Dictionary<int, Queue<Frame>> pendingByPortAndPriority;

        /// <summary>
        /// Working reference to the queue for a particular port and priority. Access protected by lock.
        /// </summary>
        private Queue<Frame> workingQueue;

        /// <summary>
        /// Initialises a new instance of the <see cref="TransmitQueue"/> class.
        /// </summary>
        /// <param name="name">Name of queue (Node name).</param>
        internal TransmitQueue(string name)
        {
            this.logName = "TransmitQueue for " + name;
            this.transmitQueueLock = new object();
            this.pendingByPortAndPriority = new Dictionary<int, Queue<Frame>>();
        }

        /// <summary>
        /// Send a message to the node this queue belongs to. Send acknowledgement messages (single frame) immediately.
        /// For messages requiring acknowledgement:
        /// If there is a frame already queued pending acknowledgement add all the frames to the queue pending transmission.
        /// Otherwise send up to three frames and save the last sent and any remaining frames.
        /// </summary>
        /// <param name="messageInFrames">Frames comprising the message.</param>
        internal void Send(Frame[] messageInFrames)
        {
            Debug.Assert(messageInFrames[0].Routes != null, "Expected set by previous call to router");
            int numberToSendNow = 0;
            if (messageInFrames[0].AckRequired || messageInFrames[0].ReplyExpected)
            {
                numberToSendNow = this.QueueFramesPendingAcknowledgement(messageInFrames);
            }
            else
            {
                numberToSendNow = messageInFrames.GetLength(0);
                Debug.Assert(numberToSendNow == 1, "Single frame expected for acknowledgement"); 
            }
            
            // do this outside the lock
            for (int index = 0; index < numberToSendNow; index++)
            {
                UserAgent.Instance.TransmitFrame(messageInFrames[index]);
            }
        }

        /// <summary>
        /// This is called from TransmittedFrames when a frame has been acknowledged.
        /// Lock. Find the correct queue.
        /// Discard all frame(s) for a message that has been Nak'd and return the next for transmission (or null). 
        /// Discard frame that has been acknowledged and return the next for transmission (or null).
        /// Compare the frame with the head of the queue to find if transmission is complete.
        /// If so, dequeue the frame at the head of the queue and return the next for transmission (or null).
        /// </summary>
        /// <param name="sentFrame">Acknowledged frame.</param>
        /// <param name="discardMessage">True if acknowledgement was Nak.</param>
        /// <returns>Next frame to transmit or null.</returns>
        internal Frame GetNextFrameToSend(Frame sentFrame, bool discardMessage)
        {
            Frame nextFrameToSend = null;
            lock (this.transmitQueueLock)
            {
                int port = PortAndPriorityForFrame(sentFrame);
                this.SetWorkingQueueForPort(port);
                if (discardMessage)
                {
                    this.DiscardAndLogAllMessageFrames(port, sentFrame);
                    nextFrameToSend = this.FrameAtHeadOfQueue();
                }
                else 
                {
                    if (this.FrameAtHeadOfQueueIsPartOfSameMessage(sentFrame))
                    {
                        if (this.FrameAtHeadOfQueueIsAcknowledged(sentFrame))
                        {
                            nextFrameToSend = this.DiscardHeadAndGetNextFrame(port);
                        }
                        else
                        {
                            EventControl.Note(this.logName, string.Format(
                                System.Globalization.CultureInfo.InvariantCulture,
                                "PortPriority {0} frame {1} acknowledged. Queue count {2}",
                                port.ToString(System.Globalization.CultureInfo.InvariantCulture),
                                sentFrame.Ident.ToString(),
                                this.workingQueue.Count.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                        }
                    }
                    else
                    {
                        // This may result in sending more than one message without acknowledgement,
                        // if there is a Nak for another frame in a discarded message. This is probably prevented
                        // by removing all the frames from TransmittedMessages.
                        nextFrameToSend = this.FrameAtHeadOfQueue();
                    }
                }

                if (nextFrameToSend != null)
                {
                    this.LogStateOfQueue(port, nextFrameToSend);
                }
            }
            
            return nextFrameToSend;
        }

        /// <summary>
        /// Count the number of frames that have not yet been transmitted (do not count any frame at the head of the queue).
        /// </summary>
        /// <returns>Number of frames not yet sent to the node.</returns>
        internal int GetPendingCount()
        {
            int count = 0;
            lock (this.transmitQueueLock)
            {
                foreach (KeyValuePair<int, Queue<Frame>> kvp in this.pendingByPortAndPriority)
                {
                    int queueCount = kvp.Value.Count;
                    if (queueCount > 0)
                    {
                        // The frame at the head of the queue has been transmitted
                        queueCount--;
                    }
                    
                    count += queueCount;
                }
            }
            
            return count;
        }
        
        /// <summary>
        /// Clear frames from all queues.
        /// </summary>
        internal void Clear()
        {
            lock (this.transmitQueueLock)
            {
                foreach (KeyValuePair<int, Queue<Frame>> kvp in this.pendingByPortAndPriority)
                {
                    kvp.Value.Clear();
                }
            }
        }

        /// <summary>
        /// Previously there was just one queue for transmission to each port.
        /// Now there is a separate queue for frames at each priority.
        /// </summary>
        /// <param name="frame">Owner of the port and priority.</param>
        /// <returns>Hash of port (x 10) + priority.</returns>
        private static int PortAndPriorityForFrame(Frame frame)
        {
            return (frame.Destination.Port * 10) + frame.Priority;
        }

        /// <summary>
        /// Dequeue any frames that belong to the same message as the sent frame.
        /// </summary>
        /// <param name="port">Port to show in log.</param>
        /// <param name="sentFrame">Acknowledged frame.</param>
        private void DiscardAndLogAllMessageFrames(int port, Frame sentFrame)
        {
            while (this.FrameAtHeadOfQueueIsPartOfSameMessage(sentFrame))
            {
                Frame discarded = this.workingQueue.Dequeue();
                EventControl.Info(this.logName, string.Format(
                   System.Globalization.CultureInfo.InvariantCulture,
                   "PortPriority {0} frame {1} discarded. Queue count {2}",
                   port.ToString(System.Globalization.CultureInfo.InvariantCulture),
                   discarded == null ? "NULL" : discarded.Ident.ToString(),
                   this.workingQueue.Count.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }
        }
        
        /// <summary>
        /// Peek to see if the frames belong to the same message.
        /// </summary>
        /// <param name="sentFrame">Acknowledged frame.</param>
        /// <returns>True if same.</returns>
        private bool FrameAtHeadOfQueueIsPartOfSameMessage(Frame sentFrame)
        {
            bool same = false;
            if (this.workingQueue.Count > 0)
            {
                Frame nextFrame = this.workingQueue.Peek();
                same = object.ReferenceEquals(nextFrame.Message, sentFrame.Message);
            }

            return same;
        }

        /// <summary>
        /// Peek to see if the frames are the same.
        /// </summary>
        /// <param name="sentFrame">Acknowledged frame.</param>
        /// <returns>True if same.</returns>
        private bool FrameAtHeadOfQueueIsAcknowledged(Frame sentFrame)
        {
            bool same = false;
            if (this.workingQueue.Count > 0)
            {
                Frame nextFrame = this.workingQueue.Peek();
                same = object.ReferenceEquals(nextFrame, sentFrame);
            }

            return same;
        }

        /// <summary>
        /// Log port, frame at head of queue and number of frames in the queue.
        /// </summary>
        /// <param name="port">Port for log.</param>
        /// <param name="transmitting">Frame at head.</param>
        private void LogStateOfQueue(int port, Frame transmitting)
        {
            Frame head = this.FrameAtHeadOfQueue();
            EventControl.Info(this.logName, string.Format(
               System.Globalization.CultureInfo.InvariantCulture,
               "PortPriority {0} head {1} {2}{3}. Queue count {4}",
               port.ToString(System.Globalization.CultureInfo.InvariantCulture),
               head == null ? "NULL" : head.Ident.ToString(),
               transmitting == null ? "not yet acknowledged" : "transmitting ",
               transmitting == null ? string.Empty : transmitting.Ident.ToString(),
               this.workingQueue.Count.ToString(System.Globalization.CultureInfo.InvariantCulture)));
        }

        /// <summary>
        /// Lock. Retrieve or create the queue for the destination port. 
        /// If previous message is still queued (not yet acknowledged) queue all frames. 
        /// Otherwise queue the last frame to be sent (as a marker) and any frames in excess of the maximum number 
        /// that GD92 allows to be sent without acknowledgement.
        /// </summary>
        /// <param name="messageInFrames">Frames comprising a message.</param>
        /// <returns>Number to send now.</returns>
        private int QueueFramesPendingAcknowledgement(Frame[] messageInFrames)
        {
            Debug.Assert(messageInFrames[0].Block == 1, "Block 1 must be first in array");
            int numberToSendNow = 0;
            lock (this.transmitQueueLock)
            {
                int port = PortAndPriorityForFrame(messageInFrames[0]);
                this.FindOrCreateQueueForPortAndPriority(port);
                bool transmitInProgress = this.workingQueue.Count > 0;
                if (transmitInProgress)
                {
                    this.QueueAllFrames(port, messageInFrames);
                }
                else
                {
                    numberToSendNow = this.QueueLastSentAndAnyNotSent(port, messageInFrames);
                }
            }
            
            return numberToSendNow;
        }

        /// <summary>
        /// Search the dictionary for the correct queue. Create a queue for the port-priority if there is none already.
        /// </summary>
        /// <param name="portAndPriority">PortAndPriority key to dictionary.</param>
        private void FindOrCreateQueueForPortAndPriority(int portAndPriority)
        {
            TestLogger("FindOrCreateQueueForPortAndPriority");
            TestLogger(" portAndPriority " + portAndPriority.ToString());
            this.SetWorkingQueueForPort(portAndPriority, true);
        }
        
        /// <summary>
        /// Set the workingQueue reference to the queue for the port-priority. Throw exception if it is missing.
        /// </summary>
        /// <param name="portAndPriority">Port number key to dictionary.</param>
        private void SetWorkingQueueForPort(int portAndPriority)
        {
            TestLogger("SetWorkingQueueForPort");
            TestLogger(" portAndPriority " + portAndPriority.ToString());
            this.SetWorkingQueueForPort(portAndPriority, false);
        }

        /// <summary>
        /// Set the workingQueue reference to the queue for the port. If there is none, create it or throw exception
        /// depending on the setting of the create flag.
        /// </summary>
        /// <param name="portAndPriority">Port number key to dictionary.</param>
        /// <param name="create">If true, create new queue.</param>
        private void SetWorkingQueueForPort(int portAndPriority, bool create)
        {
            TestLogger("SetWorkingQueueForPort");
            TestLogger(" portAndPriority " + portAndPriority.ToString());
            TestLogger(" create " + create.ToString());

            this.workingQueue = null;
            if (this.pendingByPortAndPriority.ContainsKey(portAndPriority))
            {
                TestLogger(" contains key");
                this.workingQueue = this.pendingByPortAndPriority[portAndPriority];
            }
            else if (create)
            {
                TestLogger(" create key");
                this.workingQueue = new Queue<Frame>();
                this.pendingByPortAndPriority.Add(portAndPriority, this.workingQueue);
            }
            else
            {
                TestLogger(" no key and create is false");
                throw new GD92LibException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Pending PortPriority {0} not found",
                    portAndPriority.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }
        }

        /// <summary>
        /// Add all frames to the queue.
        /// </summary>
        /// <param name="port">PortAndPriority to show in log.</param>
        /// <param name="messageInFrames">Frames comprising message.</param>
        private void QueueAllFrames(int port, Frame[] messageInFrames)
        {
            int numberOfFrames = messageInFrames.GetLength(0);
            for (int index = 0; index < numberOfFrames; index++)
            {
                this.AddFrame(port, messageInFrames[index]);
            }
        }

        /// <summary>
        /// GD92 allows up to 3 frames to be sent without acknowledgement. Queue the last frame to be sent (this will show that
        /// transmission is in progress). Also queue any frames in excess of the limit.
        /// </summary>
        /// <param name="port">PortAndPriority to show in log.</param>
        /// <param name="messageInFrames">Frames comprising message.</param>
        /// <returns>Number of frames to be sent now.</returns>
        private int QueueLastSentAndAnyNotSent(int port, Frame[] messageInFrames)
        {
            int numberOfFrames = messageInFrames.GetLength(0);
            int numberToSendNow = numberOfFrames >= MaxFramesInFlight ? MaxFramesInFlight : numberOfFrames;
            for (int index = 0; index < numberOfFrames; index++)
            {
                if (index < numberToSendNow)
                {
                    if (index == (numberToSendNow - 1))
                    {
                        // add the last-to-be-sent frame to the Pending queue 
                        this.AddFrame(port, messageInFrames[index]);
                        Debug.Assert(this.workingQueue.Count == 1, "Only one frame expected in pending queue");
                    }

                    EventControl.Info(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "PortPriority {0} expecting to send frame {1} immediately. Queue count {2}",
                        port.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        messageInFrames[index].Ident.ToString(),
                        this.workingQueue.Count.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
                else
                {
                    this.AddFrame(port, messageInFrames[index]);
                }
            }

            return numberToSendNow;
        }

        /// <summary>
        /// Add the frame to the queue for the port. Normally only one message is expected at a time. Note details
        /// if a queue builds up.
        /// </summary>
        /// <param name="port">PortAndPriority number to log.</param>
        /// <param name="frame">Frame to add to queue.</param>
        private void AddFrame(int port, Frame frame)
        {
            this.workingQueue.Enqueue(frame);
            if (this.workingQueue.Count > 1)
            {
                EventControl.Note(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "PortPriority {0} adding frame {1} Queue count {2}",
                    port.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    frame.Ident.ToString(),
                    this.workingQueue.Count.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }
        }

        /// <summary>
        /// Discard the frame (if any) at the head of the queue. Return a reference to the new head
        /// of the queue or null.
        /// </summary>
        /// <param name="port">PortAndPriority number to log.</param>
        /// <returns>Frame to transmit or null.</returns>
        private Frame DiscardHeadAndGetNextFrame(int port)
        {
            Frame acknowledged = this.workingQueue.Dequeue(); // bring the next frame (if any) to the head of the queue
            Debug.Assert(acknowledged != null, "called when frame is acknowledged - null not expeced");
            if (acknowledged != null)
            {
                EventControl.Note(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "PortPriority {0} removing acknowledged frame {1}. Queue count {2}",
                    port.ToString(System.Globalization.CultureInfo.InvariantCulture),
                    acknowledged.Ident.ToString(),
                    this.workingQueue.Count.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            } 

            return this.FrameAtHeadOfQueue();
        }

        /// <summary>
        /// Return a reference to the frame (if any) at the head of the queue.
        /// of the queue or null.
        /// </summary>
        /// <returns>Frame or null.</returns>
        private Frame FrameAtHeadOfQueue()
        {
            Frame headFrame = null;
            if (this.workingQueue.Count > 0)
            {
                headFrame = this.workingQueue.Peek();
            }

            return headFrame;
        }

        const string LogFileName = @"C:\POCLogs\GD92Component.txt";
        object logLockObject = new object();
        public void TestLogger(string message)
        {
            lock (logLockObject)
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(DateTime.UtcNow.ToString() + " : " + message);
                }
            }
        }

    }
}

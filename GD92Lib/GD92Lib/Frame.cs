﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Represents a message block, including the array of bytes transmitted or received
    /// and fields corresponding to data within the array
    /// Only one destination is allowed (any others are ignored).
    /// Data in the frame is not protected by a lock. Reference to the frame is on the call stack
    /// or in a single queue so is accessible to only one thread at a time.
    /// </summary>
    internal class Frame : IDisposable
    {
        private readonly string logName; 
        private short frameMessageLength; // number of bytes in the <message> part of the frame
        private byte destinationCount;
        private short sequenceNumber;
        private short length;
        private int indexOfNextRoute;
        private Timer receivedTimer;
        private Timer transmittedTimer;

        /// <summary>
        /// Initialises a new instance of the <see cref="Frame"/> class.
        /// </summary>
        internal Frame()
        {
            this.logName = "Frame";
            this.Source = new PortAddress(); 
            this.Destination = new PortAddress(); 
            this.Block = 1;
            this.OfBlocks = 1;
            this.destinationCount = 1;
            this.SourceName = "Unknown";
            this.DestinationName = "Unknown";
            this.Routes = new PriorityQueue[0];
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Frame"/> class.
        /// Constructor used when converting from binary
        /// Make a copy of the byte array.
        /// </summary>
        /// <param name="frameBytes">Byte array to copy into frame.</param>
        internal Frame(byte[] frameBytes) : this()
        {
            int frameLength = frameBytes.GetLength(0);
            this.FrameBytes = new byte[frameLength];
            Buffer.BlockCopy(frameBytes, 0, this.FrameBytes, 0, frameLength);
            this.Length = frameLength;
        }

        /// <summary>
        /// Call Dispose for the timers and set reference null.
        /// </summary>
        public void Dispose()
        {
            if (this.receivedTimer != null)
            {
                this.receivedTimer.Dispose();
                this.receivedTimer = null;
            }

            if (this.transmittedTimer != null)
            {
                this.transmittedTimer.Dispose();
                this.transmittedTimer = null;
            }
        }

        #region Properties
        
        /// <summary>
        /// Key comprising source address and frame sequence number.
        /// </summary>
        /// <value>Frame identity.</value>
        internal FrameIdent Ident
        {
            get { return new FrameIdent(this.Source, this.SequenceNumber); }
        }

        /// <summary>
        /// Array of possible routes in order of preference.
        /// </summary>
        /// <value>Array of priority queues.</value>
        internal PriorityQueue[] Routes { get; set; }

        /// <summary>
        /// Address of the message sender.
        /// </summary>
        /// <value>Address of sender.</value>
        internal PortAddress Source { get; set; }
        
        /// <summary>
        /// Type of source node - affects decode and handling.
        /// </summary>
        /// <value>Source type enumeration.</value>
        internal NodeType SourceNodeType { get; set; }

        /// <summary>
        /// Type of destination node - affects encode and handling.
        /// </summary>
        /// <value>Destination type enumeration.</value>
        internal NodeType DestNodeType { get; set; }

        /// <summary>
        /// Address of the message destination.
        /// </summary>
        /// <value>Address of destination.</value>
        internal PortAddress Destination { get; set; }

        /// <summary>
        /// Name of the message sender.
        /// </summary>
        /// <value>Source name.</value>
        internal string SourceName { get; set; }

        /// <summary>
        /// Name of the destination.
        /// </summary>
        /// <value>Destination name.</value>
        internal string DestinationName { get; set; }

        /// <summary>
        /// We stick to 1 although GD-92 allows more.
        /// </summary>
        /// <value>Number of destination addresses.</value>
        internal int DestinationCount
        {
            get
            {
                return this.destinationCount;
            }

            set
            {
                this.destinationCount = ValidateDestinationCount(value);
            }
        }

        /// <summary>
        /// Frame sequence number.
        /// </summary>
        /// <value>GD92 sequence number.</value>
        internal int SequenceNumber
        {
            get
            {
                return this.sequenceNumber;
            }

            set
            {
                this.sequenceNumber = ValidateSequenceNumber(value);
            }
        }

        /// <summary>
        /// Number of bytes of frame data in FrameBytes - includes SOH and BCC.
        /// </summary>
        /// <value>Number of bytes comprising entire frame.</value>
        internal int Length
        {
            get
            {
                return this.length;
            }

            set
            {
                this.length = ValidateLength(value);
            }
        }

        /// <summary>
        /// Number of bytes in the message field - includes all bytes after the message_type field
        /// up to (but not including) BCC. 
        /// </summary>
        /// <value>Number of bytes in message fields in frame.</value>
        internal int FrameMessageLength
        {
            get
            {
                return this.frameMessageLength;
            }

            set
            {
                this.frameMessageLength = ValidateFrameMessageLength(value);
            }
        }

        /// <summary>
        /// Number of bytes of message data, excluding Block, OfBlocks, ManAckRequired,
        /// which may appear immediately after the message_type field.
        /// </summary>
        /// <value>Number of bytes of message data in frame.</value>
        internal int FrameDataLength { get; set; }
        
        /// <summary>
        /// Enumeration representing GD-92 message type (using the same integer value).
        /// </summary>
        /// <value>Message type enumerateion.</value>
        internal GD92MsgType MessageType { get; set; }

        /// <summary>
        /// Message priority - depends on message type, see GD92Header class.
        /// </summary>
        /// <value>Message priority.</value>
        internal int Priority { get; set; }

        /// <summary>
        /// This corresponds to the ack_req flag in the encoded frame
        /// Usually true for unsolicited message (the Source expects acknowledgement from the Destination)
        /// Storm interprets this very literally so if it is set it always sends Message 50
        /// even though it may send some other reply as well (e.g. Message 20 for Message 5)
        /// Set false in a reply.
        /// </summary>
        /// <value>True if ack_req is true.</value>
        internal bool AckRequired { get; set; }

        /// <summary>
        /// Set true for an unsolicited message so it can be added to the transmit queue pending reply
        /// regardless of the setting of AckRequired.
        /// </summary>
        /// <value>True if awaiting acknowledgement.</value>
        internal bool ReplyExpected { get; set; }

        /// <summary>
        /// Set true when the frame is first acknowledged and removed from the TransmitQueue.
        /// Acknowledgement could be Nak(WaitAck). When the frame is subsequently acknowledged
        /// or timed out, this flag is found true
        /// indicating that the frame is no longer queued pending acknowledgement. 
        /// Note that it may remain in TransmittedFrames pending final, or delayed, acknowledgement.
        /// </summary>
        /// <value>True after any sort of acknowledgement.</value>
        internal bool Acknowledged { get; set; }

        /// <summary>
        /// This depends on message type as specified in GD-92 (see GD92Header).
        /// </summary>
        /// <value>Protocol version.</value>
        internal int ProtocolVersion { get; set; }

        /// <summary>
        /// Index into FrameBytes to the start of message data (before any Block, OfBlocks etc)
        /// Together with MessageIndex this is used to calculate the number of non-data bytes
        /// at the start of the message.
        /// </summary>
        /// <value>Index into FrameBytes at start of message.</value>
        internal int MessageBlockIndex { get; set; }

        /// <summary>
        /// Index into FrameBytes to the start of message data (after any Block, OfBlocks etc).
        /// </summary>
        /// <value>Index into FrameBytes at message data (after ambiguous fields).</value>
        internal int MessageIndex { get; set; }

        /// <summary>
        /// Block identifies the position of this frame within the message
        /// Fields block and of_blocks appear at the beginning of the message (not in the header)
        /// in EVERY frame of a multi-block message (text and mobilise messages).
        /// </summary>
        /// <value>Number of this frame.</value>
        internal int Block { get; set; }

        /// <summary>
        /// The total number of blocks (frames) comprising the message
        /// Fields block and of_blocks appear at the beginning of the message (not in the header)
        /// in EVERY frame of a multi-block message (text and mobilise messages).
        /// </summary>
        /// <value>Number of frames in message.</value>
        internal int OfBlocks { get; set; }

        /// <summary>
        /// For BBN this field was repeated in EVERY frame and is set only in the last
        /// (or not set, if ManAck is not required). Arguably it is needed only in one block
        /// (at the beginning of the message) as it is needed only when all blocks are received.
        /// </summary>
        /// <value>True if manual acknowledgement is required.</value>
        internal bool ManAckRequired { get; set; }

        /// <summary>
        /// This is the entire binary block: SOH header message bcc EOT.
        /// </summary>
        /// <value>Byte array for entire frame.</value>
        internal byte[] FrameBytes { get; set; }

        /// <summary>
        /// Reference to the message of which this frame is a part. Set by encoder or decoder.
        /// Null if not yet decoded.
        /// </summary>
        /// <value>Message or null.</value>
        internal GD92Message Message { get; set; }
        
        #endregion

        #region Property validation

        /// <summary>
        /// Validate sequence number and cast to short. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="seq">Number to validate.</param>
        /// <returns>Number as short integer.</returns>
        internal static short ValidateSequenceNumber(int seq)
        {
            if (seq < 0 || seq > FrameConverter.MaxFrameSequenceNumber)
            {
                throw new ArgumentOutOfRangeException("seq", seq, "SequenceNumber negative or too large");
            }

            return (short)seq;
        }

        /// <summary>
        /// Validate priority and cast to byte. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="priority">Number to validate.</param>
        /// <returns>Number as byte.</returns>
        internal static byte ValidatePriority(int priority)
        {
            if (priority < 0 || priority > FrameConverter.MaxPriority)
            {
                throw new ArgumentOutOfRangeException("priority", priority, "Priority negative or too large");
            }

            return (byte)priority;
        }

        /// <summary>
        /// Validate message type and cast to byte. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="messageType">Message type enumeration.</param>
        /// <returns>Type as byte.</returns>
        internal static byte ValidateMessageType(GD92MsgType messageType)
        {
            int type = (int)messageType;
            if (type < 0 || type > FrameConverter.MaxForByte)
            {
                throw new ArgumentOutOfRangeException("messageType", messageType, "Message type negative or too large");
            }

            return (byte)type;
        }

        /// <summary>
        /// Validate destination count and cast to byte. ArgumentOutOfRangeException if not valid.
        /// RSC allows one only (and so does the gateway) but that is not enforced here.
        /// </summary>
        /// <param name="destinationCount">Number to validate.</param>
        /// <returns>Number as byte.</returns>
        internal static byte ValidateDestinationCount(int destinationCount)
        {
            if (destinationCount < 1 || destinationCount > FrameConverter.MaxForByte)
            {
                throw new ArgumentOutOfRangeException("destinationCount", destinationCount, "Destination count negative or too large");
            }

            return (byte)destinationCount;
        }

        /// <summary>
        /// Validate overall frame length including SOH and ETX. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="length">Number to validate.</param>
        /// <returns>Number as short.</returns>
        internal static short ValidateLength(int length)
        {
            if (length < 0 || length > FrameConverter.MaxLength)
            {
                throw new ArgumentOutOfRangeException("length", length, "Overall frame length negative or too large");
            }

            return (short)length;
        }

        /// <summary>
        /// Validate frame message length. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="length">Number to validate.</param>
        /// <returns>Number as short.</returns>
        internal static short ValidateFrameMessageLength(int length)
        {
            if (length < 0 || length > FrameConverter.MaxFrameMessageLength)
            {
                throw new ArgumentOutOfRangeException("length", length, "FrameMessageLength negative or too large");
            }

            return (short)length;
        }

        /// <summary>
        /// Validate protocol version. ArgumentOutOfRangeException if not valid.
        /// </summary>
        /// <param name="protocol">Number to validate.</param>
        /// <returns>Number as byte.</returns>
        internal static byte ValidateProtocol(int protocol)
        {
            if (protocol < 0 ||
                protocol > FrameConverter.MaxProtocolVersion)
            {
                throw new ArgumentOutOfRangeException("protocol", protocol, "Protocol negative or too large");
            }

            return (byte)protocol;
        }

        /// <summary>
        /// Check fields are within GD-92 limits as implemented.
        /// </summary>
        internal void Validate()
        {
            ValidateDestinationCount(this.DestinationCount);
            ValidateFrameMessageLength(this.FrameMessageLength);
            ValidateLength(this.Length);
            ValidateMessageType(this.MessageType);
            ValidatePriority(this.Priority);
            ValidateProtocol(this.ProtocolVersion);
            ValidateSequenceNumber(this.SequenceNumber);
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Add this frame to the next available bearer priority queue in the array of routes configured.
        /// If there is no bearer and acknowledgement is required, send Nak.
        /// </summary>
        internal void QueueForNextBearer()
        {
            // Expect we have called the Router to set Routes prior to calling this method
            // If there are no routes the array of priority queues is empty
            Debug.Assert(this.Routes != null, "Routes must not be null - set before calling this method");
            PriorityQueue bearerPriorityQueue = null;
            ControlResult result = ControlResult.NoAction;
            int numberOfRoutes = this.Routes.GetLength(0); // could be 0
            while (bearerPriorityQueue == null && this.indexOfNextRoute < numberOfRoutes)
            {
                bearerPriorityQueue = this.Routes[this.indexOfNextRoute];
                this.indexOfNextRoute++; // the index now equates to the current route preference
                if (bearerPriorityQueue != null)
                {
                    result = bearerPriorityQueue.AddFrame(this);
                    if (result == ControlResult.Success)
                    {
                        EventControl.Info(this.logName, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture, 
                            "{0} added to queue for {1} (preference {2}) at priority {3}",
                            this.Ident.ToString(),
                            bearerPriorityQueue.Name,
                            this.indexOfNextRoute,
                            this.Priority));
                    }
                    else
                    {
                        EventControl.Info(this.logName, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture, 
                            "{0} preference {1} - {2} - not connected",
                            this.Ident.ToString(),
                            this.indexOfNextRoute, 
                            bearerPriorityQueue.Name));
                        bearerPriorityQueue = null; // try the next one
                    }
                }
            }

            if (result != ControlResult.Success)
            {
                EventControl.Info(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "{0} no bearer to {1}",
                    this.Ident.ToString(), 
                    this.Destination.ToString()));

                // avoid generating Nak in reply to Nak
                if (this.AckRequired) 
                {
                    Frame nakFrame = FrameConverter.ConstructNakForFrameFromRouter(this, (int)GD92GeneralReasonCode.NoBearer);
                    EventControl.Info(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "Replying to Frame {0} with Nak (No Bearer) {1} of {2} bytes{3}{4}",
                        this.Ident.ToString(), 
                        nakFrame.Ident.ToString(), 
                        nakFrame.Length, 
                        Environment.NewLine,
                        BitConverter.ToString(nakFrame.FrameBytes, 0, nakFrame.Length)));
                    Router.Instance.SetRoutes(nakFrame);
                    Router.DivertToGatewayIfAny(nakFrame);
                    nakFrame.QueueForNextBearer();

                    // This was tested but it is probably not necessary to use a new thread here
                    // nakFrame.AsyncQueueForNextBearer();
                }
            }
        }

        /// <summary>
        /// Decrement index (before re-sending the frame on the same route).
        /// </summary>
        internal void ResetNextRoute()
        {
            if (this.indexOfNextRoute > 0)
            {
                this.indexOfNextRoute--;
            }
        }

        /// <summary>
        /// Set the transmit timer.
        /// </summary>
        /// <param name="secs">Timeout in seconds.</param>
        internal void SetTransmitTimer(int secs)
        {
            Debug.Assert(this.receivedTimer == null, "Received timer must not be set");
            Debug.Assert(this.transmittedTimer == null, "Transmit timer must not yet be set");
            this.transmittedTimer = new Timer(this.ProcessTransmittedFrameTimedOut, null, secs * Bearer.Milliseconds, 0);
        }

        /// <summary>
        /// Cancel the transmit timer.
        /// </summary>
        internal void CancelTransmitTimer()
        {
            Debug.Assert(this.transmittedTimer != null, "Transmit timer must not be null");
            if (this.transmittedTimer != null)
            {
                this.transmittedTimer.Dispose();
                this.transmittedTimer = null;
            }
        }

        /// <summary>
        /// Set the received frame timer. Called within a lock on ReceivedFrames.
        /// </summary>
        /// <param name="secs">Timeout in seconds.</param>
        internal void SetReceivedTimer(int secs)
        {
            Debug.Assert(this.receivedTimer == null, "Received timer must not yet be set");
            this.receivedTimer = new Timer(this.ProcessReceivedTimedOut, null, secs * Bearer.Milliseconds, 0);
        }

        /// <summary>
        /// Cancel the received frame timer. Called within a lock on ReceivedFrames.
        /// </summary>
        internal void CancelReceivedTimer()
        {
            Debug.Assert(this.receivedTimer != null, "Received timer must not be null");
            if (this.receivedTimer != null)
            {
                this.receivedTimer.Dispose();
                this.receivedTimer = null;
            }
        }

        #endregion

        #region Methods for encoding
        
        /// <summary>
        /// Provide a new Frame object with the same sequence number and priority
        /// Swap the source and destination name and address fields 
        /// GD-92 says use the local address as the source of a reply but MOBS and the RSC
        /// do not recognise this if the port number is different. Just use the destination
        /// like they do. This allows a gateway to reply on behalf of other nodes.
        /// Set AckRequired false.
        /// Set the message index.
        /// Protocol version is set individually for Ack and Nak.
        /// </summary>
        /// <returns>Acknowledgement frame.</returns>
        internal Frame CopyForAcknowledgement()
        {
            var copy = new Frame();
            copy.SequenceNumber = this.SequenceNumber;
            copy.Priority = this.Priority;
            copy.Source = this.Destination;
            copy.SourceName = this.DestinationName;
            copy.Destination = this.Source;
            copy.DestinationName = this.SourceName;
            Debug.Assert(copy.AckRequired == false, "AckRequired must be false in an acknowledgement");
            Debug.Assert(copy.ReplyExpected == false, "ReplyExpected must be false in an acknowledgement");
            copy.MessageIndex = FrameConverter.MessageIndexAfterHeaderForSingleDestination;
            return copy;
        }
        
        /// <summary>
        /// Use the Source address as the original destination (assume source and destination
        /// were swapped when copying the original frame).
        /// </summary>
        /// <param name="set">Reason code set.</param>
        /// <param name="reason">Reason code.</param>
        internal void EncodeNakMessageField(GD92ReasonCodeSet set, int reason)
        {
            this.FrameBytes = new byte[FrameConverter.MaxLength];
            int index = FrameConverter.MessageIndexAfterHeaderForSingleDestination;
            index = this.EncodeWord8(index, 1); // destination count
            index = this.EncodeCommsAddress(index, this.Source);
            index = this.EncodeWord8(index, (int)set);
            index = this.EncodeWord8(index, reason);
            this.FrameMessageLength = index - FrameConverter.MessageIndexAfterHeaderForSingleDestination;
            this.FrameDataLength = this.FrameMessageLength;
        }
        
        /// <summary>
        /// Encode GD92 address field at the given index (node extends into third byte).
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <param name="pa">Full GD92 address.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeCommsAddress(int index, PortAddress pa)
        {
            int byte1 = pa.Brigade;
            int nodeAndPort = (pa.Node * FrameConverter.PortSize) + pa.Port;
            int byte2 = nodeAndPort / FrameConverter.ByteSize;
            int byte3 = nodeAndPort - (byte2 * FrameConverter.ByteSize);
            this.FrameBytes[index++] = (byte)byte1;
            this.FrameBytes[index++] = (byte)byte2;
            this.FrameBytes[index++] = (byte)byte3;
            return index;
        }

        /// <summary>
        /// Encode GD92 destination count and message length fields at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeCountAndLength(int index)
        {
            int word16 = (this.FrameMessageLength * FrameConverter.DestinationCountSize) + this.DestinationCount;
            return this.EncodeWord16(index, word16);
        }

        /// <summary>
        /// Encode GD92 protocol and priority fields at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeProtocolAndPriority(int index)
        {
            int word8 = (this.Priority * FrameConverter.ProtVerSize) + this.ProtocolVersion;
            return this.EncodeWord8(index, word8);
        }

        /// <summary>
        /// Encode GD92 seq and ack fields at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeAckAndSeq(int index)
        {
            int word16 = (this.SequenceNumber * FrameConverter.AckReqSize) + (this.AckRequired ? 1 : 0);
            return this.EncodeWord16(index, word16);
        }

        /// <summary>
        /// Encode block check field at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeBlockCheckCharacter(int index)
        {
            int blockCheck = 0;
            for (int frameIndex = 1; frameIndex < index; frameIndex++)
            {
                blockCheck ^= this.FrameBytes[frameIndex];
            }

            return this.EncodeWord8(index, blockCheck);
        }

        /// <summary>
        /// Encode GD92 word8 at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <param name="word8">Number to encode.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeWord8(int index, int word8)
        {
            this.FrameBytes[index++] = (byte)word8;
            return index;
        }

        /// <summary>
        /// Encode GD92 word16 at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <param name="word16">Number to encode.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeWord16(int index, int word16)
        {
            int byte1 = word16 / FrameConverter.ByteSize;
            int byte2 = word16 - (byte1 * FrameConverter.ByteSize);
            this.FrameBytes[index++] = (byte)byte1;
            this.FrameBytes[index++] = (byte)byte2;
            return index;
        }

        /// <summary>
        /// Encode GD92 block and of blocks fields at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeBlockOfBlocks(int index)
        {
            this.FrameBytes[index++] = (byte)this.Block;
            this.FrameBytes[index++] = (byte)this.OfBlocks;
            return index;
        }

        /// <summary>
        /// Encode GD92 man ack required field at the given index.
        /// </summary>
        /// <param name="index">Index of start.</param>
        /// <returns>Index after end.</returns>
        internal int EncodeManAckRequired(int index)
        {
            this.FrameBytes[index++] = (byte)(this.ManAckRequired ? 1 : 0);
            return index;
        }

        #endregion

        #region Methods called by the timers
        
        /// <summary>
        /// Remove the frame from the received queue.
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ProcessReceivedTimedOut(object state)
        {
            try
            {
                UserAgent.Instance.DoReceivedFrameTimedOutProcessing(this);
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "{0} ProcessReceivedTimedOut - {1}",
                    this.Ident.ToString(), 
                    e.ToString()));
                throw;
            }
        }

        /// <summary>
        /// Get the UserAgent to send Nak (timed out) to itself.
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ProcessTransmittedFrameTimedOut(object state)
        {
            try
            {
                UserAgent.Instance.DoTransmittedFrameTimedOutProcessing(this);
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.logName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0} ProcessTransmittedFrameTimedOut - {1}",
                    this.Ident.ToString(), 
                    e.ToString()));
                throw;
            }
        }

        #endregion
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Interface to provide GD-92 sequence number for messages sent by the host application
    /// These methods normally require access to the Registry
    /// The interface allows this to be mocked when testing FrameConverter.
    /// </summary>
    internal interface IGD92SequenceProvider
    {
        int GetSingleSequenceNumber();

        void FillArrayWithSequenceNumbers(int[] frameNumberArray);
    }
}

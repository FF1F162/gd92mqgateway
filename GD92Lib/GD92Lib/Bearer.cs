﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using Thales.KentFire.EventLib;
    using Thales.KentFire.SocketLib;

    /// <summary>
    /// Handles traffic to and from a particular node directly connected to this one. 
    /// Together with Mta it functions as a GD-92 Message Transfer Agent.
    /// Bearer can act as TCP client, trying at configured intervals to make a connection if none exits
    /// Alternatively, the Mta sets up the connection on receiving a request from the remote node.
    /// Subclass UDPBearer allows UDP operation.
    /// </summary>
    internal class Bearer : SocketEnd, IDisposable
    {
        internal const int Milliseconds = 1000;
        internal const int DefaultSocketSetupInterval = 60 * Milliseconds; // max 1 min without a UDP socket

        private const int PriorityQueueInterval = 1000; // milliseconds

        private readonly PriorityQueue priorityQueue;
        private readonly Queue<Frame> sentQueue;

        private Timer connectionTimer;  // cancel on receipt of anything
        private Timer nextEnqTimer;     // periodic
        private Timer enqAckTimer;   // cancel on receipt of ACK
        private Timer frameAckTimer; // cancel on receipt of FrameAckCharExpected

        private Timer socketSetupTimer;
        private Timer priorityQueueTimer;
        private bool processingPriorityQueue;
        private string reasonForClosing;

        /// <summary>
        /// Prevent socket set up after Disable.
        /// </summary>
        /// <value>True if connection is enabled.</value>
        private bool connectionEnabled;

        /// <summary>
        /// Holds frames waiting to be transmitted (comprises four queues for messages of different priority).
        /// </summary>
        internal PriorityQueue SendQueue
        {
            get { return this.priorityQueue; }
        }

        /// <summary>
        /// Name of node connected using this bearer. Key to dictionary of bearers in the MTA.
        /// </summary>
        /// <value>Node name.</value>
        protected string NodeName { get; set; }

        /// <summary>
        /// Name of MTA containing this bearer.
        /// </summary>
        /// <value>Name of MTA.</value>
        protected string MtaName { get; set; }

        /// <summary>
        /// MTA containing this bearer. Holds configuration data, set by Activate.
        /// </summary>
        /// <value>MTA instance.</value>
        protected Mta Mta { get; set; }

        /// <summary>
        /// Remote IP end point.
        /// </summary>
        /// <value>IP end point.</value>
        protected IPEndPoint RemoteIPEndpoint { get; set; }

        /// <summary>
        /// Socket set-up timer.
        /// </summary>
        /// <value>System Threading Timer.</value>
        protected Timer SocketSetupTimer
        {
            get
            {
                return this.socketSetupTimer;
            }

            set
            {
                this.socketSetupTimer = value;
            }
        }

        /// <summary>
        /// Interval for the set-up timer.
        /// </summary>
        /// <value>Interval milliseconds.</value>
        protected int SocketSetupInterval { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Bearer"/> class.
        /// </summary>
        /// <param name="nodeName">Name of node.</param>
        /// <param name="mtaName">Name of MTA.</param>
        /// <param name="ipAddress">Remote IP address.</param>
        /// <param name="ipPort">Remote port.</param>
        internal Bearer(string nodeName, string mtaName, IPAddress ipAddress, int ipPort)
        {
            this.NodeName = nodeName;
            this.MtaName = mtaName;
            this.Name = this.NodeName + " " + this.MtaName; 
            this.IPAddress = ipAddress; 
            this.IPort = ipPort;
            this.priorityQueue = new PriorityQueue(this.NodeName + " " + this.MtaName);
            this.sentQueue = new Queue<Frame>();
            this.SocketSetupInterval = DefaultSocketSetupInterval;
            this.reasonForClosing = NodeConnectionLostEventArgs.DefaultReason;
        }

        /// <summary>
        /// Using CancelTimer would be neater but FxCop needs explicit Dispose on the fields.
        /// </summary>
        public void Dispose()
        {
            if (this.connectionTimer != null)
            {
                this.connectionTimer.Dispose();
                this.connectionTimer = null;
            }

            if (this.enqAckTimer != null)
            {
                this.enqAckTimer.Dispose();
                this.enqAckTimer = null;
            }

            if (this.frameAckTimer != null)
            {
                this.frameAckTimer.Dispose();
                this.frameAckTimer = null;
            }

            if (this.nextEnqTimer != null)
            {
                this.nextEnqTimer.Dispose();
                this.nextEnqTimer = null;
            }

            if (this.priorityQueueTimer != null)
            {
                this.priorityQueueTimer.Dispose();
                this.priorityQueueTimer = null;
            }

            if (this.SocketSetupTimer != null)
            {
                this.SocketSetupTimer.Dispose();
                this.SocketSetupTimer = null;
            }
        }

        /// <summary>
        /// Count the number of frames waiting to be transmitted.
        /// </summary>
        /// <returns>Number of frames.</returns>
        internal int GetNumberOfUnsentFrames()
        {
            return this.priorityQueue.GetCount();
        }

        /// <summary>
        /// Set up the Mta dependency and configuration and start the timer to service the send queue.
        /// </summary>
        /// <param name="mta">MTA holding this bearer.</param>
        internal void Activate(Mta mta)
        {
            Debug.Assert(this.priorityQueueTimer == null, "Timer reference must be null (not set previously)");
            this.priorityQueueTimer = new Timer(this.ProcessPriorityQueue, null, 0, PriorityQueueInterval);
            Debug.Assert(this.Mta == null, "Mta reference must be null (not set previously)");
            this.Mta = mta;
            this.SetConnectionDetails();
            this.connectionEnabled = true;
        }

        /// <summary>
        /// Cancel timers and close the connection.
        /// </summary>
        internal void Deactivate()
        {
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by Deactivate");
                if (this.priorityQueueTimer != null)
                {
                    CancelTimer(ref this.priorityQueueTimer);
                }

                if (this.SocketSetupTimer != null)
                {
                    CancelTimer(ref this.socketSetupTimer);
                }

                this.CloseConnection();
                this.connectionEnabled = false;
            }
        }

        /// <summary>
        /// Close current connection and set a flag to show that connection should not be attemted or allowed.
        /// </summary>
        /// <returns>Control result.</returns>
        internal ControlResult Disable()
        {
            ControlResult result = ControlResult.NoAction;
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by Disable");
                if (this.connectionEnabled == true)
                {
                    this.CloseConnection();
                    this.connectionEnabled = false;
                    result = ControlResult.Success;
                }
                else
                {
                    result = ControlResult.AlreadyDisabled;
                }
            }

            return result;
        }

        /// <summary>
        /// Reset the flag to show that connection is allowed and begin connection attempts (if configured to do so).
        /// </summary>
        /// <returns>Control result.</returns>
        internal ControlResult Enable()
        {
            ControlResult result = ControlResult.NoAction;
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by Enable");
                if (this.connectionEnabled == false)
                {
                    Debug.Assert(this.priorityQueueTimer != null, "Timer must not be null");
                    Debug.Assert(this.Mta != null, "MTA must not be null");
                    this.connectionEnabled = true;
                    this.ProcessConnectionLocked(0);
                    result = ControlResult.Success;
                }
                else
                {
                    result = ControlResult.AlreadyEnabled;
                }
            }

            return result;
        }

        /// <summary>
        /// Provided to override the normal connection state (see User Guide S174_9.doc 12.2.6)
        /// Close the existing connection (if any).
        /// </summary>
        /// <returns>Control result.</returns>
        internal ControlResult Disconnect()
        {
            ControlResult result = ControlResult.NoAction;
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by Disconnect");
                if (this.priorityQueue.Connected == true)
                {
                    this.CloseConnection();
                    result = ControlResult.Success;
                }
                else
                {
                    result = ControlResult.AlreadyDisconnected;
                }
            }

            return result;
        }

        /// <summary>
        /// Provided to override the normal connection state (see User Guide S174_9.doc 12.2.6)
        /// Depends on existing socket connection.
        /// </summary>
        /// <returns>Control result.</returns>
        internal ControlResult Connect()
        {
            ControlResult result = ControlResult.NoAction;
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by Connect");
                if (this.priorityQueue.Connected == false)
                {
                    if (this.SocketConnected == true)
                    {
                        this.OpenConnection();
                        result = ControlResult.Success;
                    }
                    else
                    {
                        result = ControlResult.NoSocketConnection;
                    }
                }
                else
                {
                    result = ControlResult.AlreadyConnected;
                }
            }

            return result;
        }

        /// <summary>
        /// Set the socket for this bearer and begin to receive data asynchronously.
        /// </summary>
        /// <param name="socket">Socket from MTA listener.</param>
        internal void ConnectSocket(Socket socket)
        {
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by ConnectSocket");
                this.ConnectSocketLocked(socket);
            }
        }

        /// <summary>
        /// Set up the remote end point using the IP address for this bearer
        /// and set up the timer for connecting to it as TCP client.
        /// This method can be overridden to cater for UDP.
        /// </summary>
        protected virtual void SetConnectionDetails()
        {
            this.RemoteIPEndpoint = new IPEndPoint(this.IPAddress, this.IPort);
            int connectionInterval = this.Mta.ConnectionInterval * Milliseconds;
            if (connectionInterval > 0)
            {
                this.SocketSetupTimer = new Timer(this.ProcessConnection, 0, 0, connectionInterval);
            }
        }

        /// <summary>
        /// Set the lock before calling this
        /// Logs connection lost, marks send queue as not connected, cancels timers and closes the socket.
        /// </summary>
        protected override void CloseConnection()
        {
            Debug.Assert(this.priorityQueue != null, "Timer reference must not  be null");
            if (this.priorityQueue != null)
            {
                if (this.priorityQueue.Connected == true)
                {
                    GD92Component.QueueNodeConnectionLostEvent(this.NodeName, this.MtaName, this.reasonForClosing);
                    EventControl.Info(this.Name, "Closing connection");
                }

                this.priorityQueue.Connected = false; // no lock for this
            }

            CancelTimer(ref this.nextEnqTimer);
            CancelTimer(ref this.enqAckTimer);
            CancelTimer(ref this.frameAckTimer);
            CancelTimer(ref this.connectionTimer);
            this.CloseSocket();
        }
        
        /// <summary>
        /// Provide a reason for closing the connection. 
        /// The reason is propagated in the event arguments.
        /// Restore the default in the member variable.
        /// </summary>
        /// <param name="reason">Reason for closing.</param>
        protected void CloseConnection(string reason)
        {
            this.reasonForClosing = reason;
            this.CloseConnection();
            this.reasonForClosing = NodeConnectionLostEventArgs.DefaultReason;
        }

        /// <summary>
        /// Set up connection requested by remote node. Called with lock set.
        /// Check that there is no existing connection.
        /// Begin to receive data asynchronously and begin periodic connection processing.
        /// Connection is considered open only after data is received.
        /// </summary>
        /// <param name="socket">Socket from MTA listener.</param>
        protected void ConnectSocketLocked(Socket socket)
        {
            if (this.connectionEnabled == true)
            {
                // this.Socket stores the socket when connecting as a client
                // the socket could be replaced by the mta listener while client connection is
                // in progress (can be avoided by configuring one side to act as server only).
                if (this.Socket != null && object.ReferenceEquals(this.Socket, socket) == false)
                {
                    this.CloseConnection();
                    EventControl.Info(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "Existing socket for {0} closed and replaced",
                        this.IPAddress.ToString()));
                }

                this.Socket = socket;
                this.SocketConnected = true;
                this.Receive(0);
                this.ProcessConnectionLocked(0);
            }
            else
            {
                socket.Close();
                EventControl.Info(this.Name, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Connection not enabled - connecting socket for {0} closed",
                    this.IPAddress.ToString()));
            }
        }

        #region Overridden SocketEnd methods

        /// <summary>
        /// Open connection - cancel connection timer, log connection and schedule enquiries.
        /// </summary>
        protected override void DoDataReceivedProcessing()
        {
            Debug.Assert(this.priorityQueue != null, "Queue must not  be null");
            if (this.priorityQueue != null)
            {
                if (this.priorityQueue.Connected == false)
                {
                    this.OpenConnection();
                }
            }
        }

        /// <summary>
        /// Cancel enquiry timer.
        /// </summary>
        protected override void DoAckReceivedProcessing()
        {
            CancelTimer(ref this.enqAckTimer);
        }

        /// <summary>
        /// If configured, process frame acknowledgement.
        /// </summary>
        protected override void DoBellReceivedProcessing()
        {
            if (this.Mta.FrameAckCharExpected == AsciiChar.BEL)
            {
                this.DoFrameAcknowledgedProcessing();
            }
        }

        /// <summary>
        /// If configured, process frame acknowledgement.
        /// </summary>
        protected override void DoStartOfHeaderReceivedProcessing()
        {
            if (this.Mta.FrameAckCharExpected == AsciiChar.SOH)
            {
                this.DoFrameAcknowledgedProcessing();
            }
        }

        /// <summary>
        /// Tidy up after frame is acknowledged.
        /// This is only needed if frame acknowledgement is configured.
        /// </summary>
        protected override void DoFrameAcknowledgedProcessing()
        {
            CancelTimer(ref this.frameAckTimer);
            this.sentQueue.Clear();
        }

        /// <summary>
        /// Send frame acknowledgement character if configured to do so.
        /// </summary>
        protected override void DoSendFrameAckProcessing()
        {
            if (this.Mta.FrameAckCharSent > 0)
            {
                this.Send(this.Mta.FrameAckCharSent);
            }
        }

        /// <summary>
        /// SocketEnd calls this in a new thread to process the data.
        /// Build Frame and queue for next bearer.
        /// </summary>
        /// <param name="value">Entire frame SOH to EOT.</param>
        protected override void HandleReceivedFrame(byte[] value)
        {
            try
            {
                if (value != null)
                {
                    Frame frame = FrameConverter.BuildFrame(value);
                    EventControl.Info(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Frame {0} of {1} bytes received for {2}{3}{4}",
                        frame.Ident.ToString(),
                        frame.Length,
                        frame.Destination.ToString(),
                        Environment.NewLine,
                        BitConverter.ToString(value, 0, value.GetLength(0))));

                    if (frame.Source == UserAgent.Instance.GD92Address)
                    {
                        this.LogWarning(frame, "circular routing detected");
                    }
                    else if (!Router.Instance.IsValidAddress(frame.Destination))
                    {
                        this.LogWarningAndSendNak(frame, "destination invalid", GD92GeneralReasonCode.InvalidMessage);
                    }
                    else if (frame.OfBlocks > FrameConverter.MaxFeasibleNumberOfBlocks)
                    {
                        this.LogWarningAndSendNak(frame, "number of blocks is too large", GD92GeneralReasonCode.Abort);
                    }
                    else
                    {
                        Router.Instance.SetRoutes(frame);
                        Router.DivertToGatewayIfAny(frame);
                        frame.QueueForNextBearer();
                    }
                }
            }
            catch (GD92LibException e)
            {
                EventControl.Warn(this.Name, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Caught exception - {0}",
                    e.ToString()));
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.Name, e.ToString());
                throw;
            }
        }
        
        /// <summary>
        /// Return the length of the frame, which begins at the offset in the buffer.
        /// Return 0 if the destination count and message data length have not yet been read.
        /// </summary>
        /// <param name="buffer">Read buffer.</param>
        /// <param name="offset">Offset into buffer.</param>
        /// <param name="bytesRead">Bytes available.</param>
        /// <returns>Length of frame or zero if incomplete.</returns>
        protected override int DecodeLength(byte[] buffer, int offset, int bytesRead)
        {
            int length = FrameConverter.DecodeLength(buffer, offset, bytesRead);
            if (length == 0)
            {
                EventControl.Warn(this.Name, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Bytes read {0} - reading more to get message length", 
                    bytesRead));
            }

            return length;
        }

        #endregion

        /// <summary>
        /// Called by timer to do periodic connection processing.
        /// </summary>
        /// <param name="state">State object holding number of acknowledgements pending.</param>
        protected void ProcessConnection(object state)
        {
            try
            {
                lock (this.Lock)
                {
                    EventControl.Debug(this.Name, "Locked by ProcessConnection");
                    int ackPending = (int)state;

                    // For the periodic call ackPending is 0 so it does nothing
                    // unless the connection has been closed so this.Socket is null
                    if (this.Socket == null || ackPending > 0)
                    {
                        this.ProcessConnectionLocked(ackPending);
                    }
                }
            }
            catch (SocketException e)
            {
                EventControl.Warn(this.Name, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "ProcessConnection closing connection - {0}",
                    e.Message));
                this.CloseConnection();
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.Name, e.ToString());
                throw;
            }
        }

        /// <summary>
        /// If connection is enabled and configured, initially set up the socket and try to send an enquiry character.
        /// Set up a timer so this method gets called again if no acknowledgement is received within the timeout.
        /// The logic is similar to ProcessEnquiry, which sends ENQ periodically to check the connection still exists.
        /// </summary>
        /// <param name="ackPending">Number of times this has been called previously.</param>
        protected void ProcessConnectionLocked(int ackPending)
        {
            if (this.connectionEnabled == true)
            {
                if (this.Socket == null)
                {
                    this.SetUpSocket();
                }
                else if (this.Mta.ConnectionInterval > 0)
                {
                    EventControl.Info(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "ProcessConnection {0} {1} ACKs pending", 
                        this.Name, 
                        ackPending));
                    if (ackPending >= this.Mta.MaxAckPending)
                    {
                        EventControl.Note(this.Name, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "ProcessConnection closing connection after {0} ENQs",
                            ackPending));
                        this.CloseConnection(NodeConnectionLostEventArgs.EnquiryTimedOut);
                    }
                    else
                    {
                        this.Send(AsciiChar.ENQ);
                        ackPending++;
                        this.connectionTimer = new Timer(
                            this.ProcessConnection, 
                            ackPending, 
                            this.Mta.ConnectionInterval * Milliseconds,
                            0);
                    }
                }
            }
        }

        /// <summary>
        /// If configured to connect, set up a socket and begin to connect asynchronously.
        /// </summary>
        protected virtual void SetUpSocket()
        {
            if (this.Mta.ConnectionInterval > 0)
            {
                this.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                this.Socket.BeginConnect(this.RemoteIPEndpoint, new AsyncCallback(this.BearerConnectCallback), this.Socket);
            }
        }

        /// <summary>
        /// Get highest priority frame and begin to send it. Or return null if not connected.
        /// If frame acknowledgement is configured, add the frame to the sent queue and set the acknowledgement timer.
        /// </summary>
        /// <returns>Frame or null.</returns>
        protected Frame GetNextFrameAndSendIt()
        {
            Frame frame = this.priorityQueue.GetHighestPriorityFrame();
            if (frame != null)
            {
                lock (this.Lock)
                {
                    EventControl.Debug(this.Name, "Locked by GetNextFrameAndSendIt");
                    if (this.Mta.FrameAckCharExpected > 0)
                    {
                        this.sentQueue.Enqueue(frame);
                        this.frameAckTimer = new Timer(this.ProcessFrameTimedOut, null, this.Mta.AckTimeout * Milliseconds, 0);
                    }

                    this.Send(frame.FrameBytes, frame.Length);
                }
            }

            return frame;
        }

        /// <summary>
        /// Dispose the timer and set the reference to null.
        /// </summary>
        /// <param name="timer">Reference to a timer.</param>
        private static void CancelTimer(ref Timer timer)
        {
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
        }

        /// <summary>
        /// Cancel connection timer, log connection and schedule enquiries.
        /// </summary>
        private void OpenConnection()
        {
            CancelTimer(ref this.connectionTimer);
            GD92Component.QueueNodeConnectedEvent(this.NodeName, this.MtaName);
            EventControl.Info(this.Name, "Connected");
            this.ScheduleEnquiries();
            this.priorityQueue.Connected = true; // no lock for this in PriorityQueue
        }

        /// <summary>
        /// Alternative to SocketEnd.ConnectCallback.
        /// </summary>
        /// <param name="ar">Data including reference to the new socket.</param>
        private void BearerConnectCallback(IAsyncResult ar)
        {
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by ConnectCallback");
                var connectSocket = ar.AsyncState as Socket;

                // check that the socket has not been replaced by a connection from the mta listener
                if (connectSocket != null && object.ReferenceEquals(this.Socket, connectSocket))
                {
                    try
                    {
                        connectSocket.EndConnect(ar);
                        this.ConnectSocketLocked(connectSocket);
                    }
                    catch (SocketException e)
                    {
                        EventControl.Note(this.Name, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "ConnectCallback closing connection - {0}", 
                            e.Message));
                        this.CloseConnection();
                    }
                    catch (Exception e)
                    {
                        EventControl.Emergency(this.Name, e.ToString());
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// If enquiries are configured set up the timer - call ProcessEnquiry now and after each enquiry interval.
        /// </summary>
        private void ScheduleEnquiries()
        {
            // assumes lock set
            if (this.Mta.EnquiryInterval > 0)
            {
                this.nextEnqTimer = new Timer(this.ProcessEnquiry, 0, 0, this.Mta.EnquiryInterval * Milliseconds);
            }
        }

        /// <summary>
        /// Initially begin to send an enquiry character.
        /// Set up a timer so this method gets called again if no acknowledgement is received within the timeout.
        /// The logic is similar to ProcessConnection, which sends ENQ initially to find out if a connection exists.
        /// </summary>
        /// <param name="state">State object holding number of acknowledgements pending.</param>
        private void ProcessEnquiry(object state)
        {
            try
            {
                lock (this.Lock)
                {
                    int ackPending = (int)state;
                    EventControl.Info(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Locked by ProcessEnquiry {0} ACKs pending", 
                        ackPending));
                    if (ackPending >= this.Mta.MaxAckPending)
                    {
                        EventControl.Note(this.Name, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "ProcessEnquiry closing connection after {0} ENQs unanswered", 
                            ackPending));
                        this.CloseConnection(NodeConnectionLostEventArgs.EnquiryTimedOut);
                    }
                    else
                    {
                        this.Send(AsciiChar.ENQ);
                        ackPending++;
                        this.enqAckTimer = new Timer(this.ProcessEnquiry, ackPending, this.Mta.AckTimeout * Milliseconds, 0);
                    }
                }
            }
            catch (SocketException e)
            {
                EventControl.Note(this.Name, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "ProcessEnquiry closing connection - {0}", 
                    e.Message));
                this.CloseConnection();
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.Name, e.ToString());
                throw;
            }
        }

        /// <summary>
        /// Called every second by the timer set when the bearer is activated.
        /// If no acknowledgement is expected (FrameAckCharExpected = 0) 
        /// the frameAcktimer is never found set so this call removes all messages from the queue and begins send for each.
        /// If acknowledgement is expected this is expected to send one frame at each call.
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ProcessPriorityQueue(object state)
        {
            if (this.processingPriorityQueue == true)
            {
                EventControl.Info(this.Name, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Processing flag found set for PriorityQueue {0}", 
                    this.priorityQueue.Name));
            }
            else
            {
                try
                {
                    this.processingPriorityQueue = true;
                    Frame frame;
                    do
                    {
                        frame = null;
                        if (this.frameAckTimer == null)
                        {
                            // returns null if the bearer is not connected
                            frame = this.GetNextFrameAndSendIt();
                        }
                    }
                    while (frame != null);
                }
                catch (SocketException e)
                {
                    EventControl.Note(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "ProcessPriorityQueue closing connection - {0}", 
                        e.Message));
                    this.CloseConnection();
                }
                catch (Exception e)
                {
                    EventControl.Emergency(this.Name, e.ToString());
                    throw;
                }
                finally
                {
                    this.processingPriorityQueue = false;
                }
            }
        }

        /// <summary>
        /// Close the connection and re-route queued frames to the next bearer.
        /// </summary>
        /// <param name="state">State not used.</param>
        private void ProcessFrameTimedOut(object state)
        {
            try
            {
                Frame frame = null;
                var tempQueue = new Queue<Frame>();
                lock (this.Lock)
                {
                    EventControl.Note(this.Name, "ProcessFrameTimedOut closing connection - frame ACK not received");
                    this.frameAckTimer = null;
                    this.CloseConnection(NodeConnectionLostEventArgs.FrameNotAcknowledged);
                    while (this.sentQueue.Count > 0)
                    {
                        frame = this.sentQueue.Dequeue();
                        if (frame != null)
                        {
                            tempQueue.Enqueue(frame);
                        }
                    }
                }

                while (tempQueue.Count > 0)
                {
                    frame = tempQueue.Dequeue();
                    frame.QueueForNextBearer();
                }

                do
                {
                    frame = this.priorityQueue.GetFrameToReRoute();
                    if (frame != null)
                    {
                        frame.QueueForNextBearer();
                    }
                }
                while (frame != null);
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.Name, e.ToString());
                throw;
            }
        }
        
        /// <summary>
        /// Put a warning in the log and return Nak to sender.
        /// </summary>
        /// <param name="frame">Frame causing the warning.</param>
        /// <param name="warning">Explanation of the problem.</param>
        /// <param name="reasonCode">Nak reason code.</param>
        private void LogWarningAndSendNak(Frame frame, string warning, GD92GeneralReasonCode reasonCode)
        {
            this.LogWarning(frame, warning);
            Frame nakFrame = FrameConverter.ConstructNakForFrameFromRouter(frame, (int)reasonCode);
            EventControl.Info(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Replying to {0} with Nak ({1}) {2}{3}",
                frame.Ident.ToString(),
                reasonCode.ToString(),
                Environment.NewLine,
                BitConverter.ToString(nakFrame.FrameBytes, 0, nakFrame.Length)));
            Router.Instance.SetRoutes(nakFrame);
            nakFrame.QueueForNextBearer();
        }

        /// <summary>
        /// Put a warning in the log.
        /// </summary>
        /// <param name="frame">Frame causing the warning.</param>
        /// <param name="warning">Explanation of the problem.</param>
        private void LogWarning(Frame frame, string warning)
        {
            EventControl.Warn(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Received frame {0} for {1} - {2}",
                frame.Ident.ToString(),
                frame.Destination.ToString(),
                warning));
        }
    }
}

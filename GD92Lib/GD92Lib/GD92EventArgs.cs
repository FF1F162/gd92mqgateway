﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    public class MessageReceivedEventArgs : EventArgs
    {
        private GD92Message message;

        public MessageReceivedEventArgs(GD92Message newMessage)
        {
            this.message = newMessage;
        }

        public GD92Message Message
        {
            get { return this.message; }
        }
    }

    public class ReplyReceivedEventArgs : EventArgs
    {
        private GD92Message message;

        public ReplyReceivedEventArgs(GD92Message newMessage)
        {
            this.message = newMessage;
        }

        public GD92Message Message
        {
            get { return this.message; }
        }
    }

    public class NodeConnectedEventArgs : EventArgs
    {
        private string nodeName;
        private string mtaName;

        public NodeConnectedEventArgs(string nodeName, string mtaName)
        {
            this.nodeName = nodeName;
            this.mtaName = mtaName;
        }

        public string NodeName
        {
            get { return this.nodeName; }
        }

        public string MtaName
        {
            get { return this.mtaName; }
        }
    }

    public class NodeConnectionLostEventArgs : EventArgs
    {
        private string nodeName;
        private string mtaName;

        public NodeConnectionLostEventArgs(string nodeName, string mtaName)
        {
            this.nodeName = nodeName;
            this.mtaName = mtaName;
        }

        public string NodeName
        {
            get { return this.nodeName; }
        }

        public string MtaName
        {
            get { return this.mtaName; }
        }
    }
}

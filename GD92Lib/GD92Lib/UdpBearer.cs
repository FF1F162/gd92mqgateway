﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using Thales.KentFire.EventLib;

    internal class UdpBearer : Bearer
    {
        /// <summary>
        /// This provides acces to SocketEnd.readBuffer (alternative to making that protected).
        /// </summary>
        private readonly byte[] readBuffer;

        /// <summary>
        /// Same as Bearer.RemoteIPEndpoint.
        /// </summary>
        private EndPoint remoteEndpoint;

        private IPEndPoint localIPEndpoint;
        private IPEndPoint ipEndpointFrom;   
        private EndPoint endpointFrom;

        /// <summary>
        /// Initialises a new instance of the <see cref="UdpBearer"/> class.
        /// </summary>
        /// <param name="nodeName">Name of node.</param>
        /// <param name="mtaName">Name of MTA.</param>
        /// <param name="ipAddress">Remote IP address.</param>
        /// <param name="ipPort">Remote port.</param>
        internal UdpBearer(string nodeName, string mtaName, IPAddress ipAddress, int ipPort)
            : base(nodeName, mtaName, ipAddress, ipPort)
        {
            this.readBuffer = this.GetReadBuffer();
            Debug.Assert(this.readBuffer != null, "Must have a read buffer");
        }

        /// <summary>
        /// Override Bearer.SetConnectionDetails for UDP.
        /// </summary>
        protected override void SetConnectionDetails()
        {
            this.RemoteIPEndpoint = new IPEndPoint(this.IPAddress, this.IPort + UserAgent.Instance.GD92Address.Node);
            this.remoteEndpoint = (EndPoint)this.RemoteIPEndpoint;
            this.ipEndpointFrom = new IPEndPoint(this.IPAddress, this.IPort + UserAgent.Instance.GD92Address.Node);
            this.endpointFrom = (EndPoint)this.RemoteIPEndpoint;
            Node node = Router.Instance.GetNodeByNameOrDefault(this.NodeName);
            this.localIPEndpoint = new IPEndPoint(IPAddress.Any, this.Mta.ListeningPort + node.Address.Node);
            EventControl.Debug(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "Local IPEndpoint is {0} {1}", 
                this.localIPEndpoint.Address.ToString(),
                this.localIPEndpoint.Port.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            this.SocketSetupInterval = this.Mta.ConnectionInterval * Bearer.Milliseconds;
            if (this.SocketSetupInterval <= 0)
            {
                this.SocketSetupInterval = Bearer.DefaultSocketSetupInterval;
            }

            this.SocketSetupTimer = new Timer(ProcessConnection, 0, 0, this.SocketSetupInterval);
        }

        /// <summary>
        /// Override Bearer.SetUpSocket to use UDP.
        /// </summary>
        protected override void SetUpSocket()
        {
            Debug.Assert(this.Socket == null, "Socket must be null initially");
            this.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            this.Socket.Bind(this.localIPEndpoint);
            this.ConnectSocketLocked(this.Socket);
        }

        /// <summary>
        /// Overrides SocketEnd.BeginReceive in order to use BeginReceiveFrom.
        /// </summary>
        /// <param name="offset">Offset into read buffer.</param>
        protected override void BeginReceive(int offset)
        {
            this.Socket.BeginReceiveFrom(
                this.readBuffer, 
                offset, 
                Bearer.BufferSize, 
                SocketFlags.None,
                ref this.remoteEndpoint,
                new AsyncCallback(ReceiveCallback), 
                this.Socket);
        }

        /// <summary>
        /// Overrides SocketEnd.EndReceive in order to use EndReceiveFrom.
        /// </summary>
        /// <param name="ar">IAsyncResult data.</param>
        /// <returns>Number of bytes received.</returns>
        protected override int EndReceive(IAsyncResult ar)
        {
            int bytesRead = this.Socket.EndReceiveFrom(ar, ref this.endpointFrom);
            EventControl.Debug(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "EndReceive {0} bytes added to read buffer from {1} {2}",
                bytesRead.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.ipEndpointFrom.Address.ToString(),
                this.ipEndpointFrom.Port.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            return bytesRead;
        }

        /// <summary>
        /// Overrides SocketEnd.BeginSend in order to use SendTo.
        /// </summary>
        /// <param name="byteData">Array of bytes to send.</param>
        /// <param name="numberOfBytes">Number to send, starting at beginning.</param>
        protected override void BeginSend(byte[] byteData, int numberOfBytes)
        {
            this.Socket.BeginSendTo(byteData, 0, numberOfBytes, 0, this.remoteEndpoint, new AsyncCallback(SendCallback), this.Socket);
            Debug.Assert(object.ReferenceEquals(this.RemoteIPEndpoint, this.remoteEndpoint), "They are supposed to be the same");
            EventControl.Debug(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "Sending datagram to {1} {2}",
                this.RemoteIPEndpoint.Address.ToString(),
                this.RemoteIPEndpoint.Port.ToString(System.Globalization.CultureInfo.InvariantCulture)));
        }

        /// <summary>
        /// Call with lock set.
        /// End asynchronous send using EndSendTo.
        /// </summary>
        /// <param name="ar">IAsyncResult data.</param>
        /// <returns>Number of bytes sent.</returns>
        protected override int EndSend(IAsyncResult ar)
        {
            int bytesSent = this.Socket.EndSendTo(ar);
            return bytesSent;
        }
    }
}

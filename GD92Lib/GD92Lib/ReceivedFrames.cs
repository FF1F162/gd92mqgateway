﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Holds two dictionaries for reference to frames received by the UA.
    /// ReceivedFrames (1) allows check for duplicate and (2) saves frames in a multi-block message for later decoding.
    /// ReceivedFramesPendingReply refers to the last frame of a message for later reply (by the host application).
    /// </summary>
    internal class ReceivedFrames : IReceivedFrames
    {
        private readonly string logName = "ReceivedFrames";
        private readonly object framesLock;
        private readonly Dictionary<FrameIdent, Frame> receivedFrames;
        private readonly Dictionary<int, Frame> receivedFramesPendingReply;
        
        /// <summary>
        /// Working variable for access only within the lock. Array for all frames comprising the message.
        /// Initially populated with frame just received, to be compared with other frames in ReceivedFrames.
        /// </summary>
        private Frame[] messageFrames;

        /// <summary>
        /// Working variable for access only within the lock. Index of frame just received.
        /// </summary>
        private int referenceFrameIndex;

        /// <summary>
        /// Working variable for access only within the lock. Block number of frame to be retrieved from ReceivedFrames.
        /// </summary>
        private int blockRequired;

        /// <summary>
        /// Initialises a new instance of the <see cref="ReceivedFrames"/> class.
        /// </summary>
        internal ReceivedFrames()
        {
            this.framesLock = new object();
            this.receivedFrames = new Dictionary<FrameIdent, Frame>();
            this.receivedFramesPendingReply = new Dictionary<int, Frame>();
        }

        /// <summary>
        /// Use message serial number (not GD-92) to get the frame (for encoding an acknowledgement).
        /// </summary>
        /// <param name="serialNumber">Serial number.</param>
        /// <returns>Frame or null.</returns>
        public Frame GetFrameToAcknowledge(int serialNumber)
        {
            return this.GetFramePendingReply(serialNumber);
        }

        /// <summary>
        /// Add frame to the ReceivedFramesPendingReply dictionary (keyed by message SerialNumber).
        /// </summary>
        /// <param name="frame">Frame to add.</param>
        public void AddFramePendingReply(Frame frame)
        {
            lock (this.framesLock)
            {
                if (frame.Message != null)
                {
                    int serialNumber = frame.Message.SerialNumber;
                    if (this.receivedFramesPendingReply.ContainsKey(serialNumber) == false)
                    {
                        this.receivedFramesPendingReply.Add(serialNumber, frame);
                    }
                    else
                    {
                        Frame resident = this.receivedFramesPendingReply[serialNumber];
                        EventControl.Warn(this.logName, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture, 
                            "ReceivedFramesPendingReply serial number {0} duplicated - frame {1} replaced by {2}",
                            serialNumber, 
                            resident.Ident.ToString(), 
                            frame.Ident.ToString()));
                        this.receivedFramesPendingReply.Remove(serialNumber);
                        this.receivedFramesPendingReply.Add(serialNumber, frame);
                        Debug.Assert(false, "Frame with same serial number is not expected");
                    }
                }
                else
                {
                    Debug.Assert(false, "Undecoded frame (Message null) is not expected");
                }
            }
        }

        /// <summary>
        /// Cancel timers for all frames in ReceivedFrames then clear both dictionaries.
        /// </summary>
        internal void ClearAndCancelFrameTimers()
        {
            lock (this.framesLock)
            {
                foreach (KeyValuePair<FrameIdent, Frame> kvp in this.receivedFrames)
                {
                    kvp.Value.CancelReceivedTimer();
                }

                this.receivedFrames.Clear();
                this.receivedFramesPendingReply.Clear();
            }
        }

        /// <summary>
        /// Add frame to the ReceivedFrames dictionary (keyed by FrameIdent)
        /// Set the received timer in the frame, so it automatically removes itself from both dictionaries on timeout
        /// Set the expiry to allow enough time to handle the entire message. This is the maximum time the remote UA
        /// will spend trying to send the message (assuming that the router parameters are the same).
        /// </summary>
        /// <param name="frame">Frame to add.</param>
        /// <returns>True if added. False if frame with same ident is already received.</returns>
        internal bool AddFrameAndSetTimeout(Frame frame)
        {
            bool added = false;
            lock (this.framesLock)
            {
                if (this.receivedFrames.ContainsKey(frame.Ident) == false)
                {
                    int timeout = ((1 + UserAgent.Instance.Retries) * frame.OfBlocks * UserAgent.Instance.NoAckTimeout) + UserAgent.Instance.LocalManTimeout;
                    frame.SetReceivedTimer(timeout);
                    this.receivedFrames.Add(frame.Ident, frame);
                    added = true;
                }
            }

            return added;
        }

        /// <summary>
        /// Remove frame from both dictinoaries and tidy up the frame - cancel timer and set reference to null.
        /// </summary>
        /// <param name="frame">Frame to remove.</param>
        /// <returns>True if removed. False if not found.</returns>
        internal bool RemoveFrame(Frame frame)
        {
            bool removed = false;
            lock (this.framesLock)
            {
                removed = this.RemoveFrameFromReceivedFrames(frame);
                this.RemoveFramePendingReply(frame);
                frame.CancelReceivedTimer();
            }

            return removed;
        }

        /// <summary>
        /// Used to test if all frames have arrived, and if so retrieve them.
        /// It would be easier if we could assume that frames in a message have consecutive sequence numbers
        /// and wrap around to 0. But GD-92 just says the numbers should all be different.
        /// Note that the RSC does not assume this but makes the match using message type and block number.
        /// Storm occasionally skips a sequence number but DOES seem to send all frames for the same message consecutively.
        /// </summary>
        /// <param name="frame">Frame (part of multi-block message).</param>
        /// <returns>Null if any frame in the message is not yet received.</returns>
        internal Frame[] GetAllFramesForMessage(Frame frame)
        {
            Frame[] frames = null;
            if (frame.OfBlocks == 1)
            {
                // There is no need to set the lock if the message comprises just one frame.
                frames = new Frame[1];
                frames[0] = frame;
            }
            else if (frame.Block == frame.OfBlocks)
            {
                // This is the last frame of the message so by now all the others should be waiting in ReceivedFrames.
                // Set the lock to get exclusive access to the dictionary and working variables.
                lock (this.framesLock)
                {
                    this.CollectMessageFrames(frame);
                    frames = this.messageFrames;
                }
            }
            else
            {
                EventControl.Info(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Frame {0} is block {1} of {2}. Further processing deferred until all blocks received",
                        frame.Ident.ToString(),
                        frame.Block.ToString(System.Globalization.CultureInfo.InvariantCulture),
                        frame.OfBlocks.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }

            return frames;
        }
        
        /// <summary>
        /// Use the expected sequence numbers to search backwards for the previous frames comprising the message.
        /// If any frame is left null, search regardless of sequence number.
        /// Storm does not allocate numbers consecutively in the same message so use message type and destination
        /// as well as block number to make sure we have the correct frame.
        /// These assumptions apply to messages of the same type sent to the same destination.
        /// 1. Assume that message blocks are sent in order (e.g. 1 of 3, 2 of 3, 3 of 3).
        /// Then we only need to collect frames when the last arrives (instead of when each arrives).
        /// 2. Assume that the sender waits for acknowledgement of all message blocks before sending the next message.
        /// This is essential to avoid mix-up between messages; mandated in GD-92?
        /// 3. Assume that message blocks have sequence numbers in ascending order (with wrap around to 0 or 1
        /// and perhaps with gaps in the sequence).
        /// 4. Assume that sequence numbers for successive messages with the same number of blocks do not overlap.
        /// Assumptions 3 and 4 are avoided by not depending on the sequence number when searching ReceivedFrames.
        /// </summary>
        /// <param name="lastFrame">Last block of the message.</param>
        private void CollectMessageFrames(Frame lastFrame)
        {
            Debug.Assert(lastFrame.Block == lastFrame.OfBlocks, "Only collect frames when last is received");
            this.messageFrames = new Frame[lastFrame.OfBlocks];
            this.referenceFrameIndex = lastFrame.OfBlocks - 1;
            this.messageFrames[this.referenceFrameIndex] = lastFrame;
            this.blockRequired = lastFrame.OfBlocks - 1;
            this.CollectFramesInSequence();
            this.CollectOutOfSequenceFrames();
            this.WarnAndSetNullIfMessageIsNotComplete();
        }
        
        /// <summary>
        /// Usually sequence numbers and block numbers are allocated sequentially.
        /// So it is usually not necessary to search the entire dictionary for each frame.
        /// But if any frame is not found, give up and try the other method.
        /// </summary>
        private void CollectFramesInSequence()
        {
            Frame referenceFrame = this.messageFrames[this.referenceFrameIndex];
            int sequence = referenceFrame.SequenceNumber;
            for (; this.blockRequired > 0; this.blockRequired--)
            {
                sequence = GD92SequenceProvider.GetPreviousFrameNumber(sequence);
                var frameIdent = new FrameIdent(referenceFrame.Source, sequence);
                if (this.receivedFrames.ContainsKey(frameIdent))
                {
                    Frame frame = this.receivedFrames[frameIdent];
                    if (this.FrameIsBlockRequired(frame))
                    {
                        this.messageFrames[this.blockRequired - 1] = frame;
                    }
                    else
                    {
                        EventControl.Note(this.logName, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture,
                            "Frame {0} is not part of the same message. Searching for out-of-sequence frames . . .",
                            frameIdent.ToString()));
                        break;
                    }
                }
                else
                {
                    EventControl.Note(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Frame {0} not found in ReceivedFrames. Searching for out-of-sequence frames . . .",
                        frameIdent.ToString()));
                    break;
                }
            }
        }

        /// <summary>
        /// For each null in the array, iterate through the whole dictionary to find
        /// the frame required.
        /// </summary>
        private void CollectOutOfSequenceFrames()
        {
            for (int i = 0; i < this.messageFrames.Length; i++)
            {
                if (this.messageFrames[i] == null)
                {
                    this.blockRequired = i + 1;
                    this.FindRequiredBlockInReceivedFrames();
                }
            }
        }
        
        /// <summary>
        /// Iterate through the whole dictionary to find the frame required.
        /// </summary>
        private void FindRequiredBlockInReceivedFrames()
        {
            foreach (KeyValuePair<FrameIdent, Frame> kvp in this.receivedFrames)
            {
                if (this.FrameIsBlockRequired(kvp.Value))
                {
                    this.messageFrames[this.blockRequired - 1] = kvp.Value;
                    break;
                }
            }
        }
        
        /// <summary>
        /// Check if the frame is the block required. Exclude any frame that is already decoded
        /// (i.e. has a reference to the decoded message).
        /// </summary>
        /// <param name="frame">Frame being tested.</param>
        /// <returns>True if frame matches exactly.</returns>
        private bool FrameIsBlockRequired(Frame frame)
        {
            Frame referenceFrame = this.messageFrames[this.referenceFrameIndex];
            return frame.Message == null &&
                frame.Destination == referenceFrame.Destination &&
                frame.MessageType == referenceFrame.MessageType &&
                frame.OfBlocks == referenceFrame.OfBlocks &&
                frame.Block == this.blockRequired;
        }
        
        /// <summary>
        /// Warn if any frame is left null. And set the array to null to avoid trying to process it.
        /// </summary>
        private void WarnAndSetNullIfMessageIsNotComplete()
        {
            int missingBlock = 0; // block numbers start at 1
            for (int i = 0; i < this.messageFrames.Length; i++)
            {
                if (this.messageFrames[i] == null)
                {
                    missingBlock = i + 1;
                    EventControl.Warn(this.logName, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Frame {0} processing failed - block {1} missing",
                        this.messageFrames[this.referenceFrameIndex].Ident.ToString(),
                        missingBlock.ToString(System.Globalization.CultureInfo.InvariantCulture)));
                }
            }
            
            if (missingBlock > 0)
            {
                this.messageFrames = null;
            }
        }
        
        /// <summary>
        /// Call with lock set.
        /// Remove the given frame. Just return false if frame not found.
        /// </summary>
        /// <param name="frame">Frame to remove.</param>
        /// <returns>True if found (and removed).</returns>
        private bool RemoveFrameFromReceivedFrames(Frame frame)
        {
            bool removed = false;
            if (this.receivedFrames.ContainsKey(frame.Ident) == true)
            {
                this.receivedFrames.Remove(frame.Ident);
                removed = true;
            }

            return removed;
        }

        /// <summary>
        /// Call with lock set.
        /// Remove the given frame.
        /// </summary>
        /// <param name="frame">Frame to remove.</param>
        private void RemoveFramePendingReply(Frame frame)
        {
            Debug.Assert(frame.Message != null, "Frame is expected to refer to a message passed to the HAP");
            int serialNumber = int.MaxValue; // never allocated as a message serial number
            if (frame.Message != null)
            {
                serialNumber = frame.Message.SerialNumber;
            }

            if (this.receivedFramesPendingReply.ContainsKey(serialNumber) == true)
            {
                this.receivedFramesPendingReply.Remove(serialNumber);
            }
        }

        /// <summary>
        /// Find frame keyed by serial number in the collection held pending reply (by the host application).
        /// GD92LibEncoderException if frame is not found.
        /// </summary>
        /// <param name="serialNumber">Serial number.</param>
        /// <returns>Frame to be replied to.</returns>
        private Frame GetFramePendingReply(int serialNumber)
        {
            Frame frame = null;
            lock (this.framesLock)
            {
                if (this.receivedFramesPendingReply.ContainsKey(serialNumber) == true)
                {
                    frame = this.receivedFramesPendingReply[serialNumber];
                }
                else
                {
                    throw new GD92LibEncoderException(string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Message serial number {0} is not in the queue pending reply - no reply sent",
                        serialNumber));
                }
            }

            return frame;
        }
    }
}

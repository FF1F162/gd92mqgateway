﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Mobilise message object.
    /// </summary>
    internal class Msg2Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg2 mobiliseMessage;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg2Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame(s).</param>
        public Msg2Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.mobiliseMessage = new GD92Msg2();
            this.Message = this.mobiliseMessage;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.mobiliseMessage.MobTimeAndDate = this.DecodeIncidentDateTime();
            this.DecodeCallsignList(this.mobiliseMessage.CallsignList);
            this.mobiliseMessage.IncidentNumber = this.DecodeIncidentNumber();
            this.mobiliseMessage.MobilisationType = this.DecodeWord8();
            this.mobiliseMessage.Address = this.DecodeCompressedString();
            this.mobiliseMessage.HouseNumber = this.DecodeString();
            this.mobiliseMessage.Street = this.DecodeCompressedString();
            this.mobiliseMessage.SubDistrict = this.DecodeCompressedString();
            this.mobiliseMessage.District = this.DecodeCompressedString();
            this.mobiliseMessage.Town = this.DecodeCompressedString();
            this.mobiliseMessage.County = this.DecodeCompressedString();
            this.mobiliseMessage.Postcode = this.DecodeString();
            this.mobiliseMessage.MapRef = this.DecodeString();
            this.mobiliseMessage.TelNumber = this.DecodeString();
            this.mobiliseMessage.Text = this.DecodeLongCompressedString();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.mobiliseMessage.ToXml();
        }
    }
}

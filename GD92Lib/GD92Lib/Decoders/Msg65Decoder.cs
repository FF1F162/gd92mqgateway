﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Printer Status message object.
    /// </summary>
    internal class Msg65Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg65 printerStatus;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg65Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg65Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.printerStatus = new GD92Msg65();
            this.Message = this.printerStatus;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.printerStatus.PrinterStatus = this.DecodeWord8();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.printerStatus.ToXml();
        }
    }
}

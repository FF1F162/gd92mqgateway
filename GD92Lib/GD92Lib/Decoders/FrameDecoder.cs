﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;
    
    /// <summary>
    /// Class to build a frame object by decoding from an array of bytes as transmitted.
    /// Use node configuration data to set the type of source and destination node
    /// - affects message decode, handling and encode.
    /// </summary>
    internal class FrameDecoder
    {
        private readonly byte[] buffer;
        private readonly Frame newFrame;
        private readonly IRouter nodeLookup;

        /// <summary>
        /// Initialises a new instance of the <see cref="FrameDecoder"/> class.
        /// Constructor arguments supply the frame to be decoded (holds array of bytes to decode)
        /// and a reference used to find the type of source and destination nodes.
        /// </summary>
        /// <param name="frame">Frame object.</param>
        /// <param name="nodeLookup">Node lookup provider.</param>
        internal FrameDecoder(Frame frame, IRouter nodeLookup)
        {
            this.buffer = frame.FrameBytes;
            this.newFrame = frame;
            this.nodeLookup = nodeLookup;
        }
        
        /// <summary>
        /// Use data in the array of bytes received to populate fields in the Frame object.
        /// Positions of Source and CoutAndLength bytes are fixed.
        /// If there is more than one destination it is ignored
        /// Block OfBlocks and ManualAck count as part of the message data but are translated
        /// with the frame (only present in multi-block types)
        /// Word count in Msg27 (different in Storm and MOBS) is also treated as non-data.
        /// </summary>
        internal void DecodeFrame()
        {
            this.newFrame.Source = this.DecodeCommsAddress(FrameConverter.SourceIndex);
            this.SetNameAndNodeTypeForSourceAddress(this.newFrame.Source);
            this.newFrame.Destination = this.DecodeCommsAddress(FrameConverter.DestinationIndex);
            this.SetNameAndNodeTypeForDestinationAddress(this.newFrame.Destination);
            this.DecodeCountAndLength();
            System.Diagnostics.Debug.Assert(this.newFrame.DestinationCount == 1, "Destination count must be 1");
            int index = this.CalculateIndexFollowingDestinationOrDestinations();
            index = this.DecodeProtocolAndPriority(index);
            index = this.DecodeSequenceNumberAndAckRequired(index);
            index = this.DecodeMessageType(index);
            this.newFrame.MessageBlockIndex = index;
            System.Diagnostics.Debug.Assert(index == 13, "because we only expect one destination");
            index = this.DecodeBlockOfBlocks(index);
            index = this.DecodeManualAckRequired(index);
            this.newFrame.MessageIndex = index;
            this.AdjustMessageIndexForTextMessageType27();
            this.SetFrameDataLength();
        }
        
        private PortAddress DecodeCommsAddress(int index)
        {
            return Decoder.DecodeCommsAddress(this.buffer[index], this.buffer[index + 1], this.buffer[index + 2]);
        }
        
        private void DecodeCountAndLength()
        {
            byte countAndLength1 = this.buffer[FrameConverter.CountAndLengthIndex];
            byte countAndLength2 = this.buffer[FrameConverter.CountAndLengthIndex + 1];
            int countAndLength = Decoder.DecodeWord16(countAndLength1, countAndLength2);
            this.newFrame.FrameMessageLength = countAndLength / FrameConverter.DestinationCountSize;
            this.newFrame.DestinationCount = countAndLength - (this.newFrame.FrameMessageLength * FrameConverter.DestinationCountSize);
            this.newFrame.Length = FrameConverter.RestOfEnvelopeLength + (FrameConverter.AddressLength * this.newFrame.DestinationCount) + this.newFrame.FrameMessageLength;
        }
        
        private void SetNameAndNodeTypeForSourceAddress(PortAddress address)
        {
            Node node = this.NodeForAddress(address);
            this.newFrame.SourceName = node.Name;
            this.newFrame.SourceNodeType = node.GD92Type;
        }
        
        private void SetNameAndNodeTypeForDestinationAddress(PortAddress address)
        {
            Node node = this.NodeForAddress(address);
            this.newFrame.DestinationName = node.Name;
            this.newFrame.DestNodeType = node.GD92Type;
        }
        
        private Node NodeForAddress(PortAddress address)
        {
            return this.nodeLookup.GetNodeByAddressOrDefault((NodeAddress)address);
        }
        
        private int CalculateIndexFollowingDestinationOrDestinations()
        {
            return FrameConverter.DestinationIndex + (FrameConverter.AddressLength * this.newFrame.DestinationCount);
        }
        
        private int DecodeProtocolAndPriority(int index)
        {
            byte protAndPriority = this.buffer[index++];
            this.newFrame.Priority = protAndPriority / FrameConverter.ProtVerSize;
            this.newFrame.ProtocolVersion = protAndPriority - (this.newFrame.Priority * FrameConverter.ProtVerSize);
            return index;
        }
        
        private int DecodeSequenceNumberAndAckRequired(int index)
        {
            int ackAndSeq = Decoder.DecodeWord16(this.buffer[index++], this.buffer[index++]);
            this.newFrame.SequenceNumber = ackAndSeq / FrameConverter.AckReqSize;
            this.newFrame.AckRequired = (ackAndSeq - (this.newFrame.SequenceNumber * FrameConverter.AckReqSize) == 1) ? true : false;
            return index;
        }
        
        private int DecodeMessageType(int index)
        {
            this.newFrame.MessageType = (GD92MsgType)this.buffer[index++];
            return index;
        }
        
        private int DecodeBlockOfBlocks(int index)
        {
            if (this.newFrame.MessageType == GD92MsgType.MobiliseMessage ||
                this.newFrame.MessageType == GD92MsgType.MobiliseMessage6 ||
                this.newFrame.MessageType == GD92MsgType.Text)
            {
                this.newFrame.Block = this.buffer[index++];
                this.newFrame.OfBlocks = this.buffer[index++];
            }
            
            return index;
        }
        
        private int DecodeManualAckRequired(int index)
        {
            if (this.newFrame.MessageType == GD92MsgType.MobiliseMessage ||
                this.newFrame.MessageType == GD92MsgType.MobiliseMessage6)
            {
                this.newFrame.ManAckRequired = this.buffer[index++] == 1;
            }
            
            return index;
        }
        
        /// <summary>
        /// For Msg27 disregard the two-byte character count (for long compressed string).
        /// Storm puts one of these at the beginning of each block that shows the
        /// character count within the block. MOBS and RSC (and BBN?) just have one
        /// count in the first block to show the length of the whole message.
        /// </summary>
        private void AdjustMessageIndexForTextMessageType27()
        {
            if (this.newFrame.MessageType == GD92MsgType.Text)
            {
                if (this.newFrame.SourceNodeType == NodeType.Storm)
                {
                    this.newFrame.MessageIndex += FrameConverter.Word16Length;
                }
                else if (this.newFrame.Block == 1)
                {
                    this.newFrame.MessageIndex += FrameConverter.Word16Length;
                }
            }
        }
        
        private void SetFrameDataLength()
        {
            int nonData = this.newFrame.MessageIndex - this.newFrame.MessageBlockIndex;
            this.newFrame.FrameDataLength = this.newFrame.FrameMessageLength - nonData;
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Provides a method to return a decoder of the right type
    /// This depends on the type of message and also, in some cases, its origin (available by node lookup)
    /// where MOBS and Storm implement GD-92 differently.
    /// </summary>
    internal class DecoderFactory
    {
        private readonly IRouter nodeLookup;

        /// <summary>
        /// Initialises a new instance of the <see cref="DecoderFactory"/> class.
        /// Node lookup is not used currently.
        /// Previously it was used to set Frame.SourceNodeType but frame decode now does this. There may
        /// be some (performance) problem with doing that for every frame instead of waiting until it is needed.
        /// It may also be needed to test the direction of the message if decoding needs to be different.
        /// This is actually the case with Msg27 but that is handled in FrameDecoder.cs.
        /// </summary>
        /// <param name="nodeLookup">Node lookup provider.</param>
        internal DecoderFactory(IRouter nodeLookup)
        {
            this.nodeLookup = nodeLookup;
        }

        /// <summary>
        /// Return an Decoder corresponding to the message type.
        /// </summary>
        /// <param name="allFrames">Array of frames comprising a message.</param>
        /// <returns>Decoder for the message.</returns>
        internal IDecoder DecoderForMessageFrames(Frame[] allFrames)
        {
            IDecoder decoder = null;
            GD92MsgType messageType = allFrames[0].MessageType;
            switch (messageType)
            {
                case GD92MsgType.MobiliseCommand:
                    decoder = new Msg1Decoder(allFrames);
                    break;
                case GD92MsgType.MobiliseMessage:
                    decoder = this.ChooseMsg2Decoder(allFrames);
                    break;
                case GD92MsgType.ResourceStatusRequest:
                    decoder = new Msg5Decoder(allFrames);
                    break;
                case GD92MsgType.MobiliseMessage6:
                    decoder = new Msg6Decoder(allFrames);
                    break;
                case GD92MsgType.ResourceStatus:
                    decoder = new Msg20Decoder(allFrames);
                    break;
                case GD92MsgType.LogUpdate:
                    decoder = new Msg22Decoder(allFrames);
                    break;
                case GD92MsgType.Text:
                    decoder = new Msg27Decoder(allFrames);
                    break;
                case GD92MsgType.PeripheralStatus:
                    decoder = new Msg28Decoder(allFrames);
                    break;
                case GD92MsgType.AlertCrew:
                    decoder = new Msg40Decoder(allFrames);
                    break;
                case GD92MsgType.AlertStatus:
                    decoder = new Msg42Decoder(allFrames);
                    break;
                case GD92MsgType.Ack:
                    decoder = new Msg50Decoder(allFrames);
                    break;
                case GD92MsgType.Nak:
                    decoder = new Msg51Decoder(allFrames);
                    break;
                case GD92MsgType.PrinterStatus:
                    decoder = new Msg65Decoder(allFrames);
                    break;
                default:
                    throw new GD92LibDecoderException(string.Format(
                        System.Globalization.CultureInfo.InvariantCulture,
                        "Decoder not available for message type {0}", 
                        messageType));
            }
            
            return decoder;
        }
        
        /// <summary>
        /// Retain this code as an example in case a special decoder is needed
        /// that depends on the node type. Better than removing it and all the
        /// plumbing only to have to put it all back again.
        /// </summary>
        /// <param name="allFrames">Array of frames comprising message.</param>
        /// <returns>Decoder for Type 2 message.</returns>
        private IDecoder ChooseMsg2Decoder(Frame[] allFrames)
        {
            Node node = this.nodeLookup.GetNodeByAddressOrDefault((NodeAddress)allFrames[0].Source);
            System.Diagnostics.Debug.Assert(allFrames[0].SourceNodeType == node.GD92Type, "Node types should be the same");

            return new Msg2Decoder(allFrames);
        }
    }
}

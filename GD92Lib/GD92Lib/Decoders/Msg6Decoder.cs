﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Kent Mobilise message object.
    /// </summary>
    internal class Msg6Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg6 kentMobiliseMessage;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg6Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array of frames comprising message.</param>
        public Msg6Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.kentMobiliseMessage = new GD92Msg6();
            this.Message = this.kentMobiliseMessage;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.kentMobiliseMessage.MobilisationType = this.DecodeWord8();
            this.kentMobiliseMessage.AssistanceMessage = this.DecodeCompressedString();
            this.DecodeResourcesList(this.kentMobiliseMessage.ResourcesList);
            this.DecodeResourcesList(this.kentMobiliseMessage.AllResourcesList);           
            this.kentMobiliseMessage.IncidentTimeAndDate = this.DecodeIncidentDateTime();
            this.kentMobiliseMessage.MobTimeAndDate = this.DecodeIncidentDateTime();
            this.kentMobiliseMessage.IncidentNumber = this.DecodeIncidentNumber();
            this.kentMobiliseMessage.IncidentType = this.DecodeCompressedString();
            this.kentMobiliseMessage.Address = this.DecodeCompressedString();
            this.kentMobiliseMessage.HouseNumber = this.DecodeString();
            this.kentMobiliseMessage.Street = this.DecodeCompressedString();
            this.kentMobiliseMessage.SubDistrict = this.DecodeCompressedString();
            this.kentMobiliseMessage.District = this.DecodeCompressedString();
            this.kentMobiliseMessage.Town = this.DecodeCompressedString();
            this.kentMobiliseMessage.County = this.DecodeCompressedString();
            this.kentMobiliseMessage.Postcode = this.DecodeString();
            this.kentMobiliseMessage.FirstAttendance = this.DecodeAttendance();
            this.kentMobiliseMessage.SecondAttendance = this.DecodeAttendance();
            this.kentMobiliseMessage.MapBook = this.DecodeString();
            this.kentMobiliseMessage.MapRef = this.DecodeString();
            this.kentMobiliseMessage.HowReceived = this.DecodeString();
            this.kentMobiliseMessage.TelNumber = this.DecodeString();
            this.kentMobiliseMessage.OperatorInfo = this.DecodeCompressedString();
            this.kentMobiliseMessage.WaterInformation = this.DecodeCompressedString();
            this.kentMobiliseMessage.LocationNotices = this.DecodeLongCompressedString();
            this.kentMobiliseMessage.AddText = this.DecodeCompressedString();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.kentMobiliseMessage.ToXml();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Alert Crew message object.
    /// </summary>
    internal class Msg40Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg40 alertCrew;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg40Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg40Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.alertCrew = new GD92Msg40();
            this.Message = this.alertCrew;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.alertCrew.AlertGroup = this.DecodeFixedLengthString(GD92Message.AlertGroupLength);
            this.alertCrew.ManualAck = this.DecodeWord8() > 0;
            this.alertCrew.OpPeripherals = (OpPeripherals)this.DecodeWord16();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.alertCrew.ToXml();
        }
    }
}

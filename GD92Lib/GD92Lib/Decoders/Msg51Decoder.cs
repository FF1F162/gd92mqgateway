﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Nak message object.
    /// </summary>
    internal class Msg51Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg51 nak;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg51Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// In this case there is none.
        /// </summary>
        /// <param name="allFrames">Frame array containing single frame.</param>
        public Msg51Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.nak = new GD92Msg51();
            this.Message = this.nak;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.nak.OriginalDestination = this.DecodeOriginalDestination();
            this.nak.ReasonCodeSet = this.DecodeWord8();
            this.nak.ReasonCode = this.DecodeWord8();
            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.nak.ToXml();
        }
    }
}

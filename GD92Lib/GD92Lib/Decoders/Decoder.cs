﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;
    using System.Collections.ObjectModel;
    using System.Text;
    using Thales.KentFire.SocketLib; // for Ascii enumeration
    
    /// <summary>
    /// Base class for decoding frame message data and populating the fields in a GD-92 message.
    /// Header data (already decoded) are copied from the Frame object.
    /// There is a subclass for each type of message, including Ack (Msg50) which has an empty
    /// message field.
    /// All the decoding is done in this class, including keeping track of the index into the 
    /// array of received message bytes; the subclasses just call the appropriate method 
    /// for each field in turn.
    /// </summary>
    internal abstract class Decoder
    {
        private readonly Frame[] allFrames;
        private readonly int numberOfFrames;
        
        private readonly byte[] messageData;
        private readonly int messageDataLength;
        
        private byte[] uncompressed;
        
        private int index;

        /// <summary>
        /// Initialises a new instance of the <see cref="Decoder"/> class.
        /// Constructor requires all frames of the message in the correct order
        /// and prepares for decoding.
        /// </summary>
        /// <param name="allFrames">Array of frames comprising a message.</param>
        internal Decoder(Frame[] allFrames) 
        {
            this.allFrames = allFrames;
            this.numberOfFrames = this.allFrames.GetLength(0);
            this.CheckNumberOfFrames();
            this.CheckFrameOrder();
            this.messageDataLength = this.CountFrameMessageBytes();
            this.messageData = this.CopyAllMessageBytes();
        }
        
        /// <summary>
        /// Subclass must populate this field with a GD92Message of the correct type.
        /// </summary>
        protected GD92Message Message { get; set; }
        
        /// <summary>
        /// Override this to populate message fields with data decoded from the received bytes
        /// Call the base method to copy header data already decoded in the Frame.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public virtual GD92Message DecodeMessage()
        {
            System.Diagnostics.Debug.Assert(this.Message != null, "Message must not be null");
            this.CopyMessageReferenceToFrames();
            this.CopySourceAndDestinationDataFromFrameToMessage();
            this.CopyAckRequiredToMessage();
            this.CopySequenceNumberOfLastFrameToMessage();
            this.CopyManualAckRequiredToMessage();
            this.Message.ReceivedAt = DateTime.UtcNow;
            this.Message.SetReadOnly();
            return this.Message;
        }

        /// <summary>
        /// Override this to call the serializer for the message type.
        /// </summary>
        /// <returns>Representation of the message.</returns>
        public virtual string SerializeDecodedMessage()
        {
            return string.Empty;
        }
        
        #region Static decoding methods 
        
        /// <summary>
        /// Decode GD-92 address. Brigade, node and port are held in 3 bytes. Node occupies ten
        /// bits of the last two bytes.
        /// </summary>
        /// <param name="first">First byte.</param>
        /// <param name="second">Second byte.</param>
        /// <param name="third">Third byte.</param>
        /// <returns>PortAddress object.</returns>
        internal static PortAddress DecodeCommsAddress(byte first, byte second, byte third)
        {
            int brigade = first;
            int nodeAndPort = (second * FrameConverter.ByteSize) + third;
            int node = nodeAndPort / FrameConverter.PortSize;
            int port = nodeAndPort - (node * FrameConverter.PortSize);
            return new PortAddress(brigade, node, port); 
        }

        /// <summary>
        /// Byte converts straight to int.
        /// </summary>
        /// <param name="first">First byte.</param>
        /// <returns>Byte as integer.</returns>
        internal static int DecodeWord8(byte first)
        {
            return first;
        }

        /// <summary>
        /// First (left, lower index) byte is the more significant.
        /// </summary>
        /// <param name="first">First byte.</param>
        /// <param name="second">Second byte.</param>
        /// <returns>Sixteen bit word as integer.</returns>
        internal static int DecodeWord16(byte first, byte second)
        {
            return (first * FrameConverter.ByteSize) + second;
        }
        
        /// <summary>
        /// First byte is the most significant. Strictly word_32 is equivalent to unsigned int
        /// but this is only used for incident number.
        /// To detect potential overflow test that the leftmost bit is 0 (first byte less than 128).
        /// </summary>
        /// <param name="first">First byte.</param>
        /// <param name="second">Second byte.</param>
        /// <param name="third">Third byte.</param>
        /// <param name="fourth">Fourth byte.</param>
        /// <returns>Thirty-two bit word as integer.</returns>
        internal static int DecodeWord32(byte first, byte second, byte third, byte fourth)
        {
            if (first > byte.MaxValue / 2)
            {
                throw new GD92LibDecoderException("Value of word32 is larger than can be held in an int");
            }
            
            return (first * FrameConverter.ByteSize * FrameConverter.ByteSize * FrameConverter.ByteSize) +
                (second * FrameConverter.ByteSize * FrameConverter.ByteSize) +
                (third * FrameConverter.ByteSize) + fourth;
        }
        
        #endregion
        
        #region Decode using index into messageData

        protected int DecodeWord8()
        {
            return DecodeWord8(this.messageData[this.index++]);
        }

        protected int DecodeWord16()
        {
            return DecodeWord16(this.messageData[this.index++], this.messageData[this.index++]);
        }

        protected DateTime DecodeIncidentDateTime()
        {
            int stringLength = FrameConverter.IncidentDateTimeFormat.Length;
            string incidentDateTime = StringFromBytes(this.messageData, this.index, stringLength);
            this.index += stringLength;
            return DateTime.ParseExact(
                incidentDateTime, 
                FrameConverter.IncidentDateTimeFormat,
                System.Globalization.CultureInfo.InvariantCulture);
        }

        protected string DecodeIncidentNumber()
        {
            int number = DecodeWord32(this.messageData[this.index++], 
                                      this.messageData[this.index++], 
                                      this.messageData[this.index++], 
                                      this.messageData[this.index++]);
            return number.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }
        
        protected GD92Address DecodeOriginalDestination()
        {
            int numberOfDestinations = DecodeWord8();
            System.Diagnostics.Debug.Assert(numberOfDestinations == 1, "Only one destination is allowed, currently");
            var address = new PortAddress();
            for (int i = 0; i < numberOfDestinations; i++)
            {
                address = DecodeCommsAddress(
                                      this.messageData[this.index++],
                                      this.messageData[this.index++], 
                                      this.messageData[this.index++]);
            }
            
            return (GD92Address)address;
        }

        protected void DecodeResourcesList(Collection<GD92Msg6Resource> resourceList)
        {
            int resCount = DecodeWord8();
            for (int i = 0; i < resCount; i++)
            {
                string callsign = this.DecodeString();
                string commentary = this.DecodeCompressedString();
                var resource = new GD92Msg6Resource(callsign, commentary);
                resourceList.Add(resource);
            }
        }

        protected GD92Msg6Attendance DecodeAttendance()
        {
            string address = string.Empty;
            var callsigns = new Collection<string>();
            this.DecodeCallsignList(callsigns);
            if (callsigns.Count > 0)
            {
                address = this.DecodeCompressedString();
            }
            
            return new GD92Msg6Attendance(callsigns, address);
        }

        protected void DecodeCallsignList(Collection<string> callsigns)
        {
            int attendanceCount = DecodeWord8();
            if (attendanceCount > 0)
            {
                for (int i = 0; i < attendanceCount; i++)
                {
                    string callsign = this.DecodeString();
                    callsigns.Add(callsign);
                }
            }
        }

        protected string DecodeFixedLengthString(int stringLength)
        {
            string decodedString = StringFromBytes(this.messageData, this.index, stringLength);
            this.index += stringLength;
            return decodedString;
        }

        protected string DecodeString()
        {
            int stringLength = DecodeWord8();
            return this.DecodeFixedLengthString(stringLength);
        }

        protected string DecodeCompressedString()
        {
            int compressedLength = DecodeWord8();
            string decodedString = this.DecodeCompressedString(compressedLength);
            return decodedString;
        }

        protected string DecodeLongCompressedString()
        {
            int compressedLength = DecodeWord16();
            string decodedString = this.DecodeCompressedString(compressedLength);
            return decodedString;
        }
        
        protected string DecodeMessageDataAsLongCompressedString()
        {
            string decodedString = this.DecodeCompressedString(this.messageDataLength);
            return decodedString;
        }
        
        private static string StringFromBytes(byte[] bytes, int index, int stringLength)
        {
            var asciiChars = new char[stringLength];
            Encoding ascii = Encoding.ASCII;
            ascii.GetChars(bytes, index, stringLength, asciiChars, 0);
            return new string(asciiChars);
        }

        private string DecodeCompressedString(int compressedLength)
        {
            int uncompressedLength = this.UncompressString(compressedLength);
            this.index += compressedLength;
            return StringFromBytes(this.uncompressed, 0, uncompressedLength);
        }
        
        private int UncompressString(int compressedLength)
        {
            int uncompressedLength = this.GetUncompressedLength(compressedLength);
            this.uncompressed = new byte[uncompressedLength];
            int uncompressedIndex = 0;
            int end = this.index + compressedLength;
            for (int byteIndex = this.index; byteIndex < end; byteIndex++)
            {
                if (this.messageData[byteIndex] == (byte)AsciiChar.ESC)
                {
                    byteIndex++; // skip <ESC>
                    byte repeatedCharacter = this.messageData[byteIndex++]; // read char and move to number
                    int repeats = this.messageData[byteIndex];
                    for (int repeatIndex = 0; repeatIndex < repeats; repeatIndex++)
                    {
                        this.uncompressed[uncompressedIndex++] = repeatedCharacter;
                    }
                }
                else
                {
                    this.uncompressed[uncompressedIndex++] = this.messageData[byteIndex];
                }
            }
            
            System.Diagnostics.Debug.Assert(uncompressedLength == uncompressedIndex, "index is now expected to equal length");
            return uncompressedLength;
        }
        
        private int GetUncompressedLength(int length)
        {
            int uncompressedLength = 0;
            int end = this.index + length;
            for (int byteIndex = this.index; byteIndex < end; byteIndex++)
            {
                if (this.messageData[byteIndex] == (byte)AsciiChar.ESC)
                {
                    byteIndex += 2; // skip <ESC><char>
                    uncompressedLength += this.messageData[byteIndex];
                }
                else
                {
                    uncompressedLength++;
                }
            }
            
            return uncompressedLength;
        }
        
        #endregion
        
        #region Copy between Frame and Message

        private void CopyMessageReferenceToFrames()
        {
            foreach (Frame frame in this.allFrames)
            {
                frame.Message = this.Message;
            }
        }
        
        private void CopySourceAndDestinationDataFromFrameToMessage()
        {
            this.Message.From = this.allFrames[0].SourceName;
            this.Message.FromAddress = (GD92Address)this.allFrames[0].Source;
            this.Message.FromNodeType = this.allFrames[0].SourceNodeType;
            this.Message.Destination = this.allFrames[0].DestinationName;
            this.Message.DestAddress = (GD92Address)this.allFrames[0].Destination;
            this.Message.DestNodeType = this.allFrames[0].DestNodeType;
        }
        
        private void CopyAckRequiredToMessage()
        {
            this.Message.AckRequired = this.allFrames[this.numberOfFrames - 1].AckRequired;
        }
        
        /// <summary>
        /// MOBS (for BBN) sets this true in the last frame only for Msg6
        /// Storm may set it in Msg2
        /// For all other messages it is false.
        /// Note that for Msg1 and Msg40 the ManualAck is dealt with during message decoding, not with the frame,
        /// so the field is already set.
        /// </summary>
        private void CopyManualAckRequiredToMessage()
        {
            if (this.Message.MsgType != GD92MsgType.AlertCrew && this.Message.MsgType != GD92MsgType.MobiliseCommand)
            {
                this.Message.ManualAck = this.allFrames[this.numberOfFrames - 1].ManAckRequired;
            }
        }
        
        private void CopySequenceNumberOfLastFrameToMessage()
        {
            this.Message.GD92Sequence = this.allFrames[this.numberOfFrames - 1].SequenceNumber;
        }
        
        #endregion

        #region Methods used in constructor
        
        private void CheckNumberOfFrames()
        {
            if (this.allFrames[0].OfBlocks != this.numberOfFrames)
            {
                throw new GD92LibDecoderException("OfBlocks did not match number of frames");
            }
        }
        
        private void CheckFrameOrder()
        {
            for (int i = 0; i < this.numberOfFrames; i++)
            {
                if (i + 1 != this.allFrames[i].Block)
                {
                    throw new GD92LibDecoderException("Frame index ( + 1 ) did not match block number");
                }
            }
        }
        
        private int CountFrameMessageBytes()
        {
            int count = 0;
            for (int i = 0; i < this.numberOfFrames; i++)
            {
                count += this.allFrames[i].FrameDataLength;
            }
            
            return count;
        }
        
        private byte[] CopyAllMessageBytes()
        {
            var allBytes = new byte[this.messageDataLength];
            int allIndex = 0;
            for (int i = 0; i < this.numberOfFrames; i++)
            {
                Frame frame = this.allFrames[i];
                Buffer.BlockCopy(frame.FrameBytes, frame.MessageIndex, allBytes, allIndex, frame.FrameDataLength);
                allIndex += frame.FrameDataLength;
            }
            
            return allBytes;
        }
        
        #endregion
    }
}

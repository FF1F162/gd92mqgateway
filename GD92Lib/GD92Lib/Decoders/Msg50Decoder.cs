﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Ack message object.
    /// </summary>
    internal class Msg50Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg50 ack;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg50Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// In this case there is none.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg50Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.ack = new GD92Msg50();
            this.Message = this.ack;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.ack.ToXml();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Mobilise Command message object.
    /// </summary>
    internal class Msg1Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg1 mobiliseCommandMessage;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg1Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg1Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.mobiliseCommandMessage = new GD92Msg1();
            this.Message = this.mobiliseCommandMessage;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.mobiliseCommandMessage.OpPeripherals = (OpPeripherals)this.DecodeWord16();
            this.mobiliseCommandMessage.ManualAck = this.DecodeWord8() > 0;
            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.mobiliseCommandMessage.ToXml();
        }
    }
}

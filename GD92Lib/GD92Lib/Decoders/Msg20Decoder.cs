﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Resource Status message object.
    /// </summary>
    internal class Msg20Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg20 resourceStatus;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg20Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg20Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.resourceStatus = new GD92Msg20();
            this.Message = this.resourceStatus;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.resourceStatus.Callsign = this.DecodeString();
            this.resourceStatus.AvlType = this.DecodeWord8();
            this.resourceStatus.AvlData = this.DecodeString();
            this.resourceStatus.StatusCode = this.DecodeWord8();
            this.resourceStatus.Remarks = this.DecodeCompressedString();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.resourceStatus.ToXml();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Alert Crew message object.
    /// </summary>
    internal class Msg42Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg42 alertStatus;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg42Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg42Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.alertStatus = new GD92Msg42();
            this.Message = this.alertStatus;
        }

        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.alertStatus.AlerterStatus = this.DecodeFixedLengthString(GD92Message.AlertStatusLength);

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.alertStatus.ToXml();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Alert Crew message object.
    /// </summary>
    internal class Msg28Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg28 peripheralStatus;

        /// <summary>
        /// Initialises a new instance of the <see cref="Msg28Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg28Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.peripheralStatus = new GD92Msg28();
            this.Message = this.peripheralStatus;
        }

        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.peripheralStatus.IpPeripherals = (IpPeripherals)this.DecodeWord16();
            this.peripheralStatus.OpPeripherals = (OpPeripherals)this.DecodeWord16();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.peripheralStatus.ToXml();
        }
    }
}

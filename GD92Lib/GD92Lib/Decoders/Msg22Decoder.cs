﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Log Update message object.
    /// </summary>
    internal class Msg22Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg22 logUpdate;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg22Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg22Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.logUpdate = new GD92Msg22();
            this.Message = this.logUpdate;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.logUpdate.Callsign = this.DecodeString();
            this.logUpdate.IncidentNumber = this.DecodeIncidentNumber();
            this.logUpdate.Update = this.DecodeCompressedString();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.logUpdate.ToXml();
        }
    }
}

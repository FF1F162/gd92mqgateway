﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Mobilise message object.
    /// This decoder expects fields for elements in the address, as per GD-92 and MOBS 
    /// (these do not appear in messages from Storm)
    /// </summary>
    internal class Msg2MOBSDecoder : Decoder, IDecoder
    {
        private readonly GD92Msg2 MobiliseMessage;

        /// <summary>
        /// Constructor extracts data from frame(s) message field ready for decoding
        /// </summary>
        /// <param name="allFrames"></param>
        public Msg2MOBSDecoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.MobiliseMessage = new GD92Msg2();
            this.Message = this.MobiliseMessage;
        }

        /// <summary>
        /// Decode received data to populate fields in the message object
        /// </summary>
        /// <returns></returns>
        public override GD92Message DecodeMessage()
        {
            this.MobiliseMessage.SentAt = DecodeIncidentDateTime();
            DecodeCallsignList(this.MobiliseMessage.CallsignList);
            this.MobiliseMessage.IncidentNumber = DecodeIncidentNumber();
            this.MobiliseMessage.MobilisationType = DecodeWord8();
            this.MobiliseMessage.Address = DecodeCompressedString();
            this.MobiliseMessage.HouseNumber = DecodeString();
            this.MobiliseMessage.Street = DecodeCompressedString();
            this.MobiliseMessage.SubDistrict = DecodeCompressedString();
            this.MobiliseMessage.District = DecodeCompressedString();
            this.MobiliseMessage.Town = DecodeCompressedString();
            this.MobiliseMessage.County = DecodeCompressedString();
            this.MobiliseMessage.Postcode = DecodeString();
            this.MobiliseMessage.MapRef = DecodeString();
            this.MobiliseMessage.TelNumber = DecodeString();
            this.MobiliseMessage.Text = DecodeLongCompressedString();

            return base.DecodeMessage();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Text message object.
    /// </summary>
    internal class Msg27Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg27 text;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg27Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame(s).</param>
        public Msg27Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.text = new GD92Msg27();
            this.Message = this.text;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.text.Text = this.DecodeMessageDataAsLongCompressedString();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.text.ToXml();
        }
    }
}

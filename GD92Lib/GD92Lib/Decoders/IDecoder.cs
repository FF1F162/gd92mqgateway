﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Implemented by subclasses of Decoder for each type of message.
    /// Provide a method to populate frame and message objects from transmitted data.
    /// Provide a way to serialize the message.
    /// </summary>
    internal interface IDecoder
    {
        GD92Message DecodeMessage();

        string SerializeDecodedMessage();
    }
}

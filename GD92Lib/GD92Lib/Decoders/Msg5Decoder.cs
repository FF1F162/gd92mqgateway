﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92.Decoders
{
    using System;

    /// <summary>
    /// Decode received data into a new Resource Status Request message object.
    /// </summary>
    internal class Msg5Decoder : Decoder, IDecoder
    {
        private readonly GD92Msg5 resourceStatusRequest;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="Msg5Decoder"/> class.
        /// Constructor extracts data from frame(s) message field ready for decoding.
        /// </summary>
        /// <param name="allFrames">Array containing frame.</param>
        public Msg5Decoder(Frame[] allFrames)
            : base(allFrames)
        {
            this.resourceStatusRequest = new GD92Msg5();
            this.Message = this.resourceStatusRequest;
        }
        
        /// <summary>
        /// Decode received data to populate fields in the message object.
        /// </summary>
        /// <returns>Decoded message.</returns>
        public override GD92Message DecodeMessage()
        {
            this.resourceStatusRequest.Callsign = this.DecodeString();

            return base.DecodeMessage();
        }

        /// <summary>
        /// Call this after DecodeMessage.
        /// </summary>
        /// <returns>XML represenation of decoded message.</returns>
        public override string SerializeDecodedMessage()
        {
            return this.resourceStatusRequest.ToXml();
        }
    }
}

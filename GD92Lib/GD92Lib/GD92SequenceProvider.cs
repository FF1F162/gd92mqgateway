﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using Microsoft.Win32;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Provide sequence numbers for all frames (blocks) in a message and record the last number allocated
    /// in the Windows Registry.
    /// Provide a static method to work out the last number used by the previous message, given the block number of a frame,
    /// allowing for wrap-around from the maximum allowed sequence number to the minimum.
    /// </summary>
    internal class GD92SequenceProvider : IGD92SequenceProvider
    {
        public const int DefaultSequenceNumber = 1;
        public const int NumberToWrapTo = 0;
        public const string RegistryKeyFrameNumber = "FrameNumber";
        public const bool OpenToWrite = true;

        private const string LogName = "GD92Sequence";
        private static readonly object SequenceLock = new object();
        
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92SequenceProvider"/> class.
        /// </summary>
        public GD92SequenceProvider()
        {
        }

        /// <summary>
        /// Given a frame number and its position (block of blocks) return the frame numbers
        /// for all the blocks in the message. This copes with a multi-block message that embraces
        /// wrap-around but that is no longer expected as such a sequence is discarded when setting
        /// the frame numbers in a transmitted message. For received messages, do not use this method
        /// as Storm does not always use consecutive numbers.
        /// </summary>
        /// <param name="frameNumber">Sequence number of frame.</param>
        /// <param name="block">Block number of frame.</param>
        /// <param name="ofBlocks">Number of blocks (frames) in message.</param>
        /// <returns>Array GD-92 sequence numbers for all frames in the message.</returns>
        public static int[] GetFrameNumbers(int frameNumber, int block, int ofBlocks)
        {
            System.Diagnostics.Debug.Assert(ofBlocks > 1, "Not needed for a single block message");
            System.Diagnostics.Debug.Assert(block > 0, "Block number must be 1 or more");
            System.Diagnostics.Debug.Assert(block <= ofBlocks, "Block number must be within the number of blocks");
            int previousFrameNumber = GetFrameNumberPrecedingFirstBlockOfMessage(frameNumber, block);
            var frameNumberArray = new int[ofBlocks];
            SetFrameNumbers(previousFrameNumber, frameNumberArray);
            return frameNumberArray;
        }

        /// <summary>
        /// Return the number just before the sequence number supplied, allowing for wrap-around at 0.
        /// Beware that the numbers Storm uses may actually wrap at 1. RSC wraps at 0.
        /// </summary>
        /// <param name="currentFrameNumber">GD92 sequence number of a frame.</param>
        /// <returns>Sequence number just before the number supplied.</returns>
        public static int GetPreviousFrameNumber(int currentFrameNumber)
        {
            return currentFrameNumber > 0 ? currentFrameNumber - 1 : FrameConverter.MaxFrameSequenceNumber;
        }

        /// <summary>
        /// Get a single GD-92 sequence number (for a one-block message).
        /// </summary>
        /// <returns>Sequence number.</returns>
        public int GetSingleSequenceNumber()
        {
            var frameNumberArray = new int[1];
            this.FillArrayWithSequenceNumbers(frameNumberArray);
            return frameNumberArray[0];
        }

        /// <summary>
        /// Read the number last allocated or start at 1 if the key is missing.
        /// Provide sequence numbers in order for all the frames in a message.
        /// Allow for wrapping round to 0.
        /// BUT avoid including 0 in the numbers returned as Storm cannot handle it.
        /// Write the number last allocated in the Registry.
        /// </summary>
        /// <param name="frameNumberArray">Array to be filled.</param>
        public void FillArrayWithSequenceNumbers(int[] frameNumberArray)
        {
            int lastFrameNumber = GD92Component.StatusFail;
            lock (SequenceLock)
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(
                        UserAgent.Instance.RegistryKeyPrivate, OpenToWrite);
                if (rk != null)
                {
                    object value = rk.GetValue(RegistryKeyFrameNumber, DefaultSequenceNumber);
                    lastFrameNumber = (int)value;
                    lastFrameNumber = SetFrameNumbersDiscardingWrapAround(lastFrameNumber, frameNumberArray);
                    rk.SetValue(RegistryKeyFrameNumber, lastFrameNumber);
                }
            }

            if (lastFrameNumber == GD92Component.StatusFail)
            {
                throw new GD92LibException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Registry access failed for {0}\\{1}",
                    UserAgent.Instance.RegistryKeyPrivate,
                    RegistryKeyFrameNumber));
            }

            if (lastFrameNumber < 0 || lastFrameNumber > FrameConverter.MaxFrameSequenceNumber)
            {
                throw new GD92LibException(string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    "Invalid frame number in Registry location {0}\\{1}",
                    UserAgent.Instance.RegistryKeyPrivate,
                    RegistryKeyFrameNumber));
            }
        }

        /// <summary>
        /// Populate array with sequence numbers.
        /// Avoid wrap-around in the sequence numbers used for transmitted messages.
        /// Detect wrap-around (MaxFrameSequenceNumber > NumberToWrapTo) by comparing previous and last-allocated numbers.
        /// If previous is larger, get a new set of numbers.
        /// Single frame wraps to 0. Using that as previous gives 1 (so we avoid sending 0 to Storm).
        /// </summary>
        /// <param name="previousFrameNumber">Last sequence number used for previous message.</param>
        /// <param name="frameNumberArray">Array to populate with sequence numbers.</param>
        /// <returns>Last sequence number in the array.</returns>
        private static int SetFrameNumbersDiscardingWrapAround(int previousFrameNumber, int[] frameNumberArray)
        {
            int frameNumber = SetFrameNumbers(previousFrameNumber, frameNumberArray);
            if (frameNumber < previousFrameNumber)
            {
                frameNumber = SetFrameNumbers(frameNumber, frameNumberArray);
            }

            return frameNumber;
        }

        /// <summary>
        /// Populate array with sequence numbers. Normally just increment from the previous number.
        /// If the next number exceeds the maximum allowed use the minimum instead.
        /// </summary>
        /// <param name="previousFrameNumber">Last sequence number used for previous message.</param>
        /// <param name="frameNumberArray">Array to populate with sequence numbers.</param>
        /// <returns>Last sequence number in the array.</returns>
        private static int SetFrameNumbers(int previousFrameNumber, int[] frameNumberArray)
        {
            int frameNumber = previousFrameNumber;
            int numberOfFrames = frameNumberArray.GetLength(0);
            System.Diagnostics.Debug.Assert(numberOfFrames > 0, "Array must not be empty");
            for (int i = 0; i < numberOfFrames; i++)
            {
                frameNumber++;
                if (frameNumber > FrameConverter.MaxFrameSequenceNumber)
                {
                    frameNumber = NumberToWrapTo;
                }

                frameNumberArray[i] = frameNumber;
            }

            return frameNumber;
        }

        /// <summary>
        /// Count backwards from the block number to find the number immediately before the first frame
        /// of the message. This can be at or near the maximum allowed if the numbers wrapped around when
        /// setting the numbers for transmitted frames. In the absence of wrap-around this is one less than
        /// the block number of the first frame. Note that this is only used as the basis of a calculation;
        /// it does not imply that the number was actually used in a transmitted frame.
        /// </summary>
        /// <param name="frameNumber">GD92 sequence number of the frame.</param>
        /// <param name="block">Block number of the frame.</param>
        /// <returns>Last sequence number used in the previous message.</returns>
        private static int GetFrameNumberPrecedingFirstBlockOfMessage(int frameNumber, int block)
        {
            int previousFrameNumber = frameNumber;
            for (int blockNumber = block; blockNumber > NumberToWrapTo; blockNumber--)
            {
                previousFrameNumber--;
                if (previousFrameNumber < NumberToWrapTo)
                {
                    previousFrameNumber = FrameConverter.MaxFrameSequenceNumber;
                }
            }

            return previousFrameNumber;
        }
    }
}

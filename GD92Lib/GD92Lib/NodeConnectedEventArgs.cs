﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    public class NodeConnectedEventArgs : EventArgs
    {
        private string nodeName;
        private string mtaName;

        public NodeConnectedEventArgs()
        {
            this.nodeName = string.Empty;
            this.mtaName = string.Empty;
        }

        public NodeConnectedEventArgs(string nodeName, string mtaName)
        {
            this.nodeName = nodeName;
            this.mtaName = mtaName;
        }

        public string NodeName
        {
            get { return this.nodeName; }
        }

        public string MtaName
        {
            get { return this.mtaName; }
        }
    }
}

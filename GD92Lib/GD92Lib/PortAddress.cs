﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// GD92 address comprising brigade, node and port. See also GD92Address and NodeAddress.
    /// Implements IEquatable T as it is used as a Dictionary key.
    /// </summary>
    internal struct PortAddress : IEquatable<PortAddress>
    {
        private readonly short brigade;
        private readonly short node;
        private readonly short port;

        /// <summary>
        /// Initialises a new instance of the <see cref="PortAddress"/> struct.
        /// </summary>
        /// <param name="brigade">Brigade number.</param>
        /// <param name="node">Node number.</param>
        /// <param name="port">Port number.</param>
        public PortAddress(int brigade, int node, int port)
        {
            this.brigade = GD92Message.ValidateBrigade("Brigade", brigade);
            this.node = GD92Message.ValidateNode("Node", node);
            this.port = GD92Message.ValidatePort("Port", port);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PortAddress"/> struct.
        /// Adds a port to the node address.
        /// </summary>
        /// <param name="nodeAddress">Node address.</param>
        /// <param name="port">Port number.</param>
        public PortAddress(NodeAddress nodeAddress, int port)
        {
            this.brigade = (short)nodeAddress.Brigade;
            this.node = (short)nodeAddress.Node;
            this.port = GD92Message.ValidatePort("Port", port);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PortAddress"/> struct.
        /// Adds a default port to the node address.
        /// </summary>
        /// <param name="nodeAddress">Node address.</param>
        public PortAddress(NodeAddress nodeAddress)
        {
            this.brigade = (short)nodeAddress.Brigade;
            this.node = (short)nodeAddress.Node;
            this.port = GD92Header.DefaultPortNumber;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="PortAddress"/> struct.
        /// Uses the brigade node and port in the GD92 address supplied.
        /// </summary>
        /// <param name="a">GD92 address.</param>
        public PortAddress(GD92Address a)
        {
            this.brigade = GD92Message.ValidateBrigade("Brigade", a.Brigade);
            this.node = GD92Message.ValidateNode("Node", a.Node);
            this.port = GD92Message.ValidatePort("Port", a.Port);
        }

        /// <summary>
        /// Gets Brigade number.
        /// </summary>
        /// <value>Brigade number.</value>
        internal int Brigade
        {
            get
            {
                return this.brigade;
            }
        }

        /// <summary>
        /// Gets Node number.
        /// </summary>
        /// <value>Node number.</value>
        internal int Node
        {
            get
            {
                return this.node;
            }
        }

        /// <summary>
        /// Gets Port number.
        /// </summary>
        /// <value>Port number.</value>
        internal int Port
        {
            get
            {
                return this.port;
            }
        }

        /// <summary>
        /// Implement == operator to avoid boxing.
        /// </summary>
        /// <param name="first">PortAddress.</param>
        /// <param name="second">PortAddress to compare.</param>
        /// <returns>True if all fields are the same.</returns>
        public static bool operator ==(PortAddress first, PortAddress second)
        {
            return first.Equals(second);
        }

        /// <summary>
        /// Implement != operator to avoid boxing.
        /// </summary>
        /// <param name="first">PortAddress.</param>
        /// <param name="second">PortAddress to compare.</param>
        /// <returns>True if not all fields are the same.</returns>
        public static bool operator !=(PortAddress first, PortAddress second)
        {
            return !first.Equals(second);
        }

        /// <summary>
        /// This allows the explicit cast (NodeAddress)portAddress;.
        /// </summary>
        /// <param name="pa">Port Address.</param>
        /// <returns>Node address.</returns>
        public static explicit operator NodeAddress(PortAddress pa)
        {
            return pa.ToNodeAddress();
        }

        /// <summary>
        /// This allows the explicit cast (GD92Address)portAddress;.
        /// </summary>
        /// <param name="pa">Port Address.</param>
        /// <returns>GD92 address.</returns>
        public static explicit operator GD92Address(PortAddress pa)
        {
            return pa.ToGD92Address();
        }

        /// <summary>
        /// Use brigade and node to construct NodeAddress.
        /// </summary>
        /// <returns>Node address (port discarded).</returns>
        public NodeAddress ToNodeAddress()
        {
            return new NodeAddress(this);
        }

        /// <summary>
        /// Use members to construct GD92Address.
        /// </summary>
        /// <returns>GD92 address.</returns>
        public GD92Address ToGD92Address()
        {
            return new GD92Address(this.Brigade, this.Node, this.Port);
        }

        /// <summary>
        /// Override Equals(object) as part of IEquatableT. See also GD92Address, FrameIdent and NodeAddress.
        /// Check for null and different type then cast to type and call the type-specific Equals method.
        /// </summary>
        /// <param name="obj">Object to be compared with this.</param>
        /// <returns>True if the object equals this.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            var pa = (PortAddress)obj;
            return this.Equals(pa);
        }

        /// <summary>
        /// Override Equals(type) as part of IEquatableT. See also GD92Address, FrameIdent and NodeAddress.
        /// Check all fields are the same.
        /// </summary>
        /// <param name="other">A PortAddress.</param>
        /// <returns>True if all fields are the same.</returns>
        public bool Equals(PortAddress other)
        {
            return (this.brigade == other.Brigade) && (this.node == other.Node) && (this.port == other.Port); 
        }

        /// <summary>
        /// Brigade is always the same and there is a small number of values for Port.
        /// There is no justification for anything more complicated.
        /// </summary>
        /// <returns>Value of Node field.</returns>
        public override int GetHashCode()
        {
            return this.Node;
        }

        /// <summary>
        /// Brigade Node Port distinct from GD92Address which uses underline separator.
        /// </summary>
        /// <returns>Brigade Node Port.</returns>
        public override string ToString()
        {
            return string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                "{0} {1} {2}",
                this.brigade.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.node.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.port.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}

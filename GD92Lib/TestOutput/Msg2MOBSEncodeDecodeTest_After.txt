<?xml version="1.0" encoding="utf-8"?>
<GD92Msg2>
  <SerialNumber>0</SerialNumber>
  <MsgType>MobiliseMessage</MsgType>
  <Header ProtocolVersion="1" Priority="1" StandardPort="15" />
  <Destination>Unknown</Destination>
  <SentAt>2001-02-08T12:19:59</SentAt>
  <ReceivedAt>2014-11-25T16:10:05.0209942+00:00</ReceivedAt>
  <From>Unknown</From>
  <ManualAck>true</ManualAck>
  <FromAddress>
    <Brigade>22</Brigade>
    <Node>2</Node>
    <Port>14</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>0</Node>
    <Port>14</Port>
  </DestAddress>
  <CallsignList>
    <string>FJK60P1</string>
    <string>FJK60R1</string>
  </CallsignList>
  <IncidentNumber>1</IncidentNumber>
  <MobilisationType>1</MobilisationType>
  <Address>High Street, New Romney</Address>
  <MapRef>TN28 8AZ</MapRef>
  <TelNumber>999</TelNumber>
  <Text>This is some text</Text>
</GD92Msg2>
<?xml version="1.0" encoding="utf-8"?>
<GD92Msg27>
  <SerialNumber>0</SerialNumber>
  <MsgType>Text</MsgType>
  <Header ProtocolVersion="1" Priority="1" StandardPort="15" />
  <Destination>MockNodeNameForAddress</Destination>
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>2015-04-22T16:58:27.1852462+01:00</ReceivedAt>
  <From>Ranch</From>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>22</Brigade>
    <Node>105</Node>
    <Port>10</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>611</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>Storm</FromNodeType>
  <DestNodeType>Rsc</DestNodeType>
  <Text>|CrewReport:FJK11
|CrewReportTo:FJK11
|CrewReportAt:13:13:01 21-04-15
|Updated:13-13-00 21-04-15
|Interface:Current
|Records:13
FJK11|3767|WM|S|FJK11R1|Douglas Clive|BA ICSL1 AVH BWC1O MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4145|FF|S|FJK11R1|Morris Martin|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4169|FF|S|FJK11R1|Gretton Gary|BA EFAD ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4297|CM|S|FJK11R1|Hanley Tim|BA ICSL1 AVH ALP1O BWC1O MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4526|FF|S|FJK11A1|Laxton Mark|BA ALP1O BWC1O MD1O MD4O IRU1O|FJK11A1|Y||
FJK11|4585|CM|S|FJK11H9|Jones Neil|BA EFAD ICSL1 AVH ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O TL1C TL1O IRU1O|FJK11H9|Y||
FJK11|4643|FF|S||Cox Farrell|BA ALP1O BWC1O MD1O MD4O IRU1O||Y||
FJK11|4710|FF|S|FJK11A1|Gilfillan Alister|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O TL1C TL1O IRU1O|FJK11A1|Y||
FJK11|4734|FF|S||Haynes Andrew|BA ALP1O BWC1O MD1O MD4O IRU1O||Y||
FJK11|4787|FF|S|FJK11H9|Martin Glen|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11H9|Y||
FJK11|5657|FF|P|FJK11W1|Wood Spencer|BA EFAD ICSL1 BWC1D BWC1O|FJK11W1|Y||
FJK11|6027|FF|P|FJK11W1|Pritchett Daniel|BA BA|FJK11W1|Y||
FJK11|6066|FF|P||Charlton Mark|BA BWC1O||Y|| </Text>
</GD92Msg27>
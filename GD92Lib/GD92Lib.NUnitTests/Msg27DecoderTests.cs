﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    using Thales.KentFire.GD92.Decoders;
    using Thales.KentFire.LogLib;

    /// <summary>
    /// These tests include decoding text messages from Storm comprising one, two or three frames.
    /// The second three-frame message has a gap in the sequence numbering.
    /// The same data are used in ReceivedFramesTests.
    /// Storm puts a word count in each frame of a multi-block text message. FrameDecoder takes
    /// account of this if the source node type is Storm. For this to happen, the router
    /// (i.e. MockRouter) must be populated accordingly, in this case to show node 105 is type Storm.
    /// For text messages from the RSC the default applies (word count in first frame only).
    /// </summary>
	[TestFixture]
	public class Msg27DecoderTests
	{
        private readonly static IRouter TestRouter = new MockRouter();
        private static string TestInputPath;
        private static string TestOutputPath;
        //private static string OutputFileName = "Msg27DecoderDefaultOutput.txt";
        private static string TextSingle;
        private static string TextSingleBody;
        private static string Text1of2;
        private static string Text2of2;
        private static string TextTwoBlockBody;
        private static string Text1of3;
        private static string Text2of3;
        private static string Text3of3;
        private static string TextThreeBlockBody;
        private static string Text1of3Seq1;
        private static string Text2of3Seq3;
        private static string Text3of3Seq4;
        private static string TextThreeBlockBodySequenceGap;

        [TestFixtureSetUp]
		public void Init()
		{
            TestInputPath = EncoderTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = EncoderTests.FolderFromAppConfigOrDefault("TestOutput");
            TextSingle = ReadHexDataFromFile("TextSingle");
            Text1of2 = ReadHexDataFromFile("Text1of2");
            Text2of2 = ReadHexDataFromFile("Text2of2");
            Text1of3 = ReadHexDataFromFile("Text1of3");
            Text2of3 = ReadHexDataFromFile("Text2of3");
            Text3of3 = ReadHexDataFromFile("Text3of3");
            Text1of3Seq1 = ReadHexDataFromFile("Text1of3Seq1");
            Text2of3Seq3 = ReadHexDataFromFile("Text2of3Seq3");
            Text3of3Seq4 = ReadHexDataFromFile("Text3of3Seq4");
            TextSingleBody = @"|Interface:Current
|Records:4
FJK12|5674|FF|P|FJK12P1 FJK12P1|Vogel Peter|BA EFAD|FJK12P1 FJK12P1|Y||
FJK12|5879|FF|P|FJK12P1|Corner Phillip|BA|FJK12P1|Y||
FJK12|8020|CM|P||Cawdron David|BA EFAD ICSL1 MD1O||Y||
FJK12|9765|FF|P|FJK12P1|O'shea Paul|BA|FJK12P1|Y||</Text>
</GD92Msg27>";
            TextTwoBlockBody = @"|Interface:Current
|Records:11
FJK19|4076|FF|S|FJK19R1 FJK19R1 FJK19P1 FJK19P1|Bray Christopher|BA EFAD|FJK19R1 FJK19R1 FJK19P1 FJK19P1|N||
FJK19|4203|CM|S||Scott-Smith Michael|BA EFAD ICSL1||Y||
FJK19|4287|FF|S||Eastwood Adam|BA EFAD||Y||
FJK19|4433|WM|S||Redmond Christian|BA ICSL1 FFU1O ISU1O MD1O MD4O||Y||
FJK19|4458|FF|S|FJK19R1|Pearce Graham|BA|FJK19R1|N||
FJK19|4498|FF|S|FJK19R1|Lawson Sam|BA|FJK19R1|N||
FJK19|4618|FF|S||Law Westley|BA EFAD||Y||
FJK19|5101|FF|P||Staveley Neil|BA EFAD||Y||
FJK19|6015|FF|P||Emery Glenn|BA EFAD||Y||
FJK19|8009|FF|P|FJK19P1|Vaughan Melvyn|BA|FJK19P1|Y||
FJK19|8807|CM|P|FJK19P1|Southon Keith|BA|FJK19P1|Y|| </Text>
</GD92Msg27>";
            TextThreeBlockBody = @"|Interface:Current
|Records:13
FJK11|3767|WM|S|FJK11R1|Douglas Clive|BA ICSL1 AVH BWC1O MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4145|FF|S|FJK11R1|Morris Martin|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4169|FF|S|FJK11R1|Gretton Gary|BA EFAD ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4297|CM|S|FJK11R1|Hanley Tim|BA ICSL1 AVH ALP1O BWC1O MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4526|FF|S|FJK11A1|Laxton Mark|BA ALP1O BWC1O MD1O MD4O IRU1O|FJK11A1|Y||
FJK11|4585|CM|S|FJK11H9|Jones Neil|BA EFAD ICSL1 AVH ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O TL1C TL1O IRU1O|FJK11H9|Y||
FJK11|4643|FF|S||Cox Farrell|BA ALP1O BWC1O MD1O MD4O IRU1O||Y||
FJK11|4710|FF|S|FJK11A1|Gilfillan Alister|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O TL1C TL1O IRU1O|FJK11A1|Y||
FJK11|4734|FF|S||Haynes Andrew|BA ALP1O BWC1O MD1O MD4O IRU1O||Y||
FJK11|4787|FF|S|FJK11H9|Martin Glen|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11H9|Y||
FJK11|5657|FF|P|FJK11W1|Wood Spencer|BA EFAD ICSL1 BWC1D BWC1O|FJK11W1|Y||
FJK11|6027|FF|P|FJK11W1|Pritchett Daniel|BA BA|FJK11W1|Y||
FJK11|6066|FF|P||Charlton Mark|BA BWC1O||Y|| </Text>
</GD92Msg27>";
            TextThreeBlockBodySequenceGap = @"|Interface:Current
|Records:13
FJK11|3767|WM|S|FJK11R1|Douglas Clive|BA ICSL1 AVH BWC1O MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4145|FF|S|FJK11R1|Morris Martin|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4169|FF|S|FJK11R1|Gretton Gary|BA EFAD ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4297|CM|S|FJK11R1|Hanley Tim|BA ICSL1 AVH ALP1O BWC1O MD1O MD4O IRU1O|FJK11R1|Y||
FJK11|4526|FF|S|FJK11A1|Laxton Mark|BA ALP1O BWC1O MD1O MD4O IRU1O|FJK11A1|Y||
FJK11|4585|CM|S|FJK11H9|Jones Neil|BA EFAD ICSL1 AVH ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O TL1C TL1O IRU1O|FJK11H9|Y||
FJK11|4643|FF|S||Cox Farrell|BA ALP1O BWC1O MD1O MD4O IRU1O||Y||
FJK11|4710|FF|S|FJK11A1|Gilfillan Alister|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O TL1C TL1O IRU1O|FJK11A1|Y||
FJK11|4734|FF|S||Haynes Andrew|BA ALP1O BWC1O MD1O MD4O IRU1O||Y||
FJK11|4787|FF|S|FJK11H9|Martin Glen|BA EFAD ALP1C ALP1O BWC1D BWC1O IRU1D MD1O MD4O IRU1O|FJK11H9|Y||
FJK11|5657|FF|P|FJK11W1|Wood Spencer|BA EFAD ICSL1 BWC1D BWC1O|FJK11W1|Y||
FJK11|6027|FF|P|FJK11W1|Pritchett Daniel|BA BA|FJK11W1|Y||
FJK11|6066|FF|P||Charlton Mark|BA BWC1O||Y|| </Text>
</GD92Msg27>";
        }
		
        /// <summary>
        /// Read hexadecimal data (as output by BitConverter) from text input path defined in this file
        /// </summary>
        /// <param name="dataName"></param>
        /// <returns></returns>
		private static string ReadHexDataFromFile(string dataName)
		{
			string fileName = Path.Combine(TestInputPath, dataName + ".txt");
            using (var file = new StreamReader(fileName))
            {
            	return file.ReadToEnd();
            }
		}

        [Test]
        public void Msg27DecoderMessageInSingleBlockTest()
        {
            //OutputFileName = "Msg27DecoderMessageInSingleBlock.txt";
            string output = DecodeLogData(TextSingle);
            Assert.IsTrue(output.EndsWith(TextSingleBody, StringComparison.Ordinal));
        }

        [Test]
        public void Msg27DecoderMessageInTwoBlocksTest()
        {
            //OutputFileName = "Msg27DecoderMessageInTwoBlocks.txt";
            string output = DecodeMessageInTwoFrames(Text1of2, Text2of2);
            Assert.IsTrue(output.EndsWith(TextTwoBlockBody, StringComparison.Ordinal));
        }

        [Test]
		public void Msg27DecoderMessageInThreeBlocksTest()
		{
            //OutputFileName = "Msg27DecoderMessageInThreeBlocks.txt";
			string output = DecodeMessageInThreeFrames(Text1of3, Text2of3, Text3of3);
            Assert.IsTrue(output.EndsWith(TextThreeBlockBody, StringComparison.Ordinal));
		}

        [Test]
        public void Msg27DecoderMessageInThreeBlocksSequenceGapTest()
        {
            //OutputFileName = "Msg27DecoderMessageInThreeBlocksSequenceGap.txt";
            string output = DecodeMessageInThreeFrames(Text1of3Seq1, Text2of3Seq3, Text3of3Seq4);
            Assert.IsTrue(output.EndsWith(TextThreeBlockBodySequenceGap, StringComparison.Ordinal));
        }

		/// <summary>
		/// Convert log data for a single frame (as output by BitConverter) back to an array of bytes
		/// Use this to build an array of frames comprising the message (in this case just one frame)
		/// Decode and serialize the message to XML in a file (.txt)
		/// </summary>
		/// <param name="dataFromLog"></param>
		public static string DecodeLogData(string dataFromLog)
		{
			byte[] bytes = LogReader.ConvertToByteArray(dataFromLog);
			var frames = new Frame[1];
			frames[0] = new Frame(bytes);
			return DecodeFramesAndMessage(frames);
		}
		
		public static string DecodeMessageInTwoFrames(string firstFromLog, string second)
		{
			var frames = new Frame[2];
			frames[0] = new Frame(LogReader.ConvertToByteArray(firstFromLog));
			frames[1] = new Frame(LogReader.ConvertToByteArray(second));
			return DecodeFramesAndMessage(frames);
		}
		
		public static string DecodeMessageInThreeFrames(string firstFromLog, string second, string third)
		{
			var frames = new Frame[3];
			frames[0] = new Frame(LogReader.ConvertToByteArray(firstFromLog));
			frames[1] = new Frame(LogReader.ConvertToByteArray(second));
			frames[2] = new Frame(LogReader.ConvertToByteArray(third));
			return DecodeFramesAndMessage(frames);
		}
		
		private static string DecodeFramesAndMessage(Frame[] frames)
		{
			foreach (Frame frame in frames)
			{
	        	var frameDecoder = new FrameDecoder(frame, TestRouter);
	        	frameDecoder.DecodeFrame();
			}
			var decoder = new Msg27Decoder(frames);
			GD92Message message = decoder.DecodeMessage();
			var textMessage = message as GD92Msg27 ?? new GD92Msg27();
            //textMessage.ToXml(Path.Combine(TestOutputPath, OutputFileName)); // used for manual check initially
			return textMessage.ToXml();
		}
	}
}

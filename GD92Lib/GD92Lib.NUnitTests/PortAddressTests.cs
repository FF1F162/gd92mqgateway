﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
	using System;
	using NUnit.Framework;
	using Thales.KentFire.GD92;

	[TestFixture]
	public class PortAddressTests
	{
		[Test]
		public void PortAddressEmptyEqualityTest()
		{
			var empty = new PortAddress();
			var zero = new PortAddress(0, 0, 0);
			Assert.IsTrue(empty == zero);
		}
		
		[Test]
		public void PortAddressEmptyEqualsTest()
		{
			var empty = new PortAddress();
			var zero = new PortAddress(0, 0, 0);
			Assert.IsTrue(empty.Equals(zero));
		}
		
		[Test]
		public void PortAddressZeroEqualsTest()
		{
			var empty = new PortAddress();
			var zero = new PortAddress(0, 0, 0);
			Assert.IsTrue(zero.Equals(empty));
		}
		
		[Test]
		public void PortAddressNormalEqualsTest()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(22, 1, 12);
			Assert.IsTrue(first.Equals(second));
		}
		
		[Test]
		public void PortAddressMaxEqualsTest()
		{
			var first = new PortAddress(255, 1023, 63);
			var second = new PortAddress(255, 1023, 63);
			Assert.IsTrue(first.Equals(second));
		}
		
		[Test]
		public void PortAddressPortInequalityTest1()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(22, 1, 13);
			Assert.IsFalse(first.Equals(second));
		}
		
		[Test]
		public void PortAddressPortInequalityTest2()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(22, 1, 13);
			Assert.IsFalse(first == second);
		}
		
		[Test]
		public void PortAddressPortInequalityTest3()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(22, 1, 13);
			Assert.IsFalse(second == first);
		}
		
		[Test]
		public void PortAddressPortInequalityTest4()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(22, 1, 13);
			Assert.IsTrue(second != first);
		}
		
		[Test]
		public void PortAddressPortInequalityTest5()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(22, 1, 13);
			Assert.IsTrue(first != second);
		}
		
		[Test]
		public void PortAddressNodeInequalityTest1()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(22, 3, 12);
			Assert.IsFalse(first.Equals(second));
		}
		
		[Test]
		public void PortAddressBrigadeInequalityTest()
		{
			var first = new PortAddress(22, 1, 12);
			var second = new PortAddress(0, 1, 12);
			Assert.IsTrue(first != second);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void PortAddressBrigadeIsInvalidTest1()
		{
			var first = new PortAddress(-1, 1, 12);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void PortAddressBrigadeIsInvalidTest2()
		{
			var first = new PortAddress(256, 1, 12);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void PortAddressNodeIsInvalidTest1()
		{
			var first = new PortAddress(22, -1, 12);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void PortAddressNodeIsInvalidTest2()
		{
			var first = new PortAddress(22, 1024, 12);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void PortAddressPortIsInvalidTest1()
		{
			var first = new PortAddress(22, 1, -1);
		}
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void PortAddressPortIsInvalidTest2()
		{
			var first = new PortAddress(22, 1, 64);
		}

		[Test]
		public void PortAddressToStringTest1()
		{
			var first = new PortAddress();
			const string expected = "0 0 0";
			string actual = first.ToString();
			Assert.IsTrue(expected == actual);
		}
		
		[Test]
		public void PortAddressToStringTest2()
		{
			var first = new PortAddress(22, 1, 12);
			const string expected = "22 1 12";
			string actual = first.ToString();
			Assert.IsTrue(expected == actual);
		}
		
		[Test]
		public void PortAddressFromNodeAddressTest()
		{
			var expected = new PortAddress(22, 1, GD92Header.DefaultPortNumber);
			var node = new NodeAddress(22, 1);
			var actual = new PortAddress(node);
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void PortAddressFromNodeAddressAndPortTest()
		{
			var expected = new PortAddress(22, 1, 12);
			var node = new NodeAddress(22, 1);
			var actual = new PortAddress(node, 12);
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void PortAddressFromGD92AddressTest()
		{
			var expected = new PortAddress(22, 1, 12);
			var gd92Address = new GD92Address(22, 1, 12);
			var actual = new PortAddress(gd92Address);
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void PortAddressCastToNodeAddressTest()
		{
			var portAddress = new PortAddress(22, 1, 12);
			var expected = new NodeAddress(22, 1);
			var actual = (NodeAddress)portAddress;
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void PortAddressCastToGD92AddressTest()
		{
			var portAddress = new PortAddress(22, 1, 12);
			var expected = new GD92Address(22, 1, 12);
			var actual = (GD92Address)portAddress;
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void PortAddressToNodeAddressTest()
		{
			var portAddress = new PortAddress(22, 1, 12);
			var expected = new NodeAddress(22, 1);
			var actual = portAddress.ToNodeAddress();
			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void PortAddressToGD92AddressTest()
		{
			var portAddress = new PortAddress(22, 1, 12);
			var expected = new GD92Address(22, 1, 12);
			var actual = portAddress.ToGD92Address();
			Assert.AreEqual(expected, actual);
		}
	}
}

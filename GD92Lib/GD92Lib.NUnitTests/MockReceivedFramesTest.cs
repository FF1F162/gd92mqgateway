﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class MockReceivedFramesTest
	{
		[Test]
		public void GetFrameToAcknowledgeTest()
		{
			var receivedFrames = new MockReceivedFrames();
			const int serialNumber = 1234;
			Frame receivedFrame = receivedFrames.GetFrameToAcknowledge(serialNumber);
			Assert.AreEqual(999, receivedFrame.SequenceNumber);
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    using Thales.KentFire.GD92.Decoders;
    using Thales.KentFire.LogLib;

    [TestFixture]
    public class Msg20DecoderTests
    {
        private readonly static IRouter MockRouter = new MockRouter();
        private static string TestInputPath;
        private static string TestOutputPath;
        private static string ResourceStatusFJK44P1;
        private static string ResourceStatusFJK60P1;
        //private static string ResourceStatusFJK44P1Body;
        //private static string ResourceStatusFJK60P1Body;

        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = EncoderTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = EncoderTests.FolderFromAppConfigOrDefault("TestOutput");
            ResourceStatusFJK44P1 = ReadHexDataFromFile("ResourceStatusFJK44P1");
            ResourceStatusFJK60P1 = ReadHexDataFromFile("ResourceStatusFJK60P1");
            //ResourceStatusFJK44P1Body = @"";
            //ResourceStatusFJK60P1Body = @"";
        }
        /// <summary>
        /// Read hexadecimal data (as output by BitConverter) from text input path defined in this file
        /// </summary>
        /// <param name="dataName"></param>
        /// <returns></returns>
        private static string ReadHexDataFromFile(string dataName)
        {
            string fileName = Path.Combine(TestInputPath, dataName + ".txt");
            using (var file = new StreamReader(fileName))
            {
                return file.ReadToEnd();
            }
        }

        /// <summary>
        /// Message from Storm had two empty bytes after callsign instead of four expected
        /// (including status code)
        /// </summary>
        [Test]
        [ExpectedException("System.IndexOutOfRangeException")]
        public void Msg20FJK44P1()
        {
            string output = DecodeLogData(ResourceStatusFJK44P1);
            WriteDataToFile(output, "ResourceStatusFJK44P1");
            //Assert.IsTrue(output.EndsWith(ResourceStatusFJK44P1Body));
        }
        [Test]
        [ExpectedException("System.IndexOutOfRangeException")]
        public void Msg20FJK60P1()
        {
            string output = DecodeLogData(ResourceStatusFJK60P1);
            WriteDataToFile(output, "ResourceStatusFJK60P1");
            //Assert.IsTrue(output.EndsWith(ResourceStatusFJK60P1Body));
        }
        private static void WriteDataToFile(string data, string dataName)
        {
            string fileName = Path.Combine(TestOutputPath, dataName + ".txt");
            using (var file = new StreamWriter(fileName))
            {
                file.Write(data);
            }
        }

        /// <summary>
        /// Convert log data for a single frame (as output by BitConverter) back to an array of bytes
        /// Use this to build an array of frames comprising the message (in this case just one frame)
        /// Decode and serialize the message to XML in a file (.txt)
        /// </summary>
        /// <param name="dataFromLog"></param>
        private static string DecodeLogData(string dataFromLog)
        {
            byte[] bytes = LogReader.ConvertToByteArray(dataFromLog);
            var frames = new Frame[1];
            frames[0] = new Frame(bytes);
            return DecodeFramesAndMessage(frames);
        }
        private static string DecodeFramesAndMessage(Frame[] frames)
        {
            foreach (Frame frame in frames)
            {
                var frameDecoder = new FrameDecoder(frame, MockRouter);
                frameDecoder.DecodeFrame();
            }
            var decoder = new Msg20Decoder(frames);
            GD92Message message = decoder.DecodeMessage();
            var mobiliseMessage = message as GD92Msg20 ?? new GD92Msg20();
            return mobiliseMessage.ToXml();
        }
    }
}

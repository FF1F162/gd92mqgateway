﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    /// <summary>
    /// Test constructors.
    /// </summary>
    [TestFixture]
    public class NodeTests
    {
        [Test]
        public void NodeConstructorTest()
        {
            var node = new Node("Node", new PortAddress(22, 1, 1));
            NodeType expected = NodeType.Rsc;
            Assert.AreEqual(expected, node.GD92Type, "because this is the default");
        }
        
        [Test]
        public void NodeConstructorForNonDefaultTypeTest()
        {
            var node = new Node("Node", new PortAddress(22, 1, 1), NodeType.Storm);
            NodeType expected = NodeType.Storm;
            Assert.AreEqual(expected, node.GD92Type);
        }
    }
}

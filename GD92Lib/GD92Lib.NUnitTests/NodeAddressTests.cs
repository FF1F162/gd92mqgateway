﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
	using System;
	using NUnit.Framework;
	using Thales.KentFire.GD92;

	[TestFixture]
	public class NodeAddressTests
	{
		[Test]
		public void NodeAddressEmptyEqualityTest()
		{
			var empty = new NodeAddress();
			var zero = new NodeAddress(0, 0);
			Assert.IsTrue(empty == zero);
		}
		[Test]
		public void NodeAddressEmptyEqualsTest()
		{
			var empty = new NodeAddress();
			var zero = new NodeAddress(0, 0);
			Assert.IsTrue(empty.Equals(zero));
		}
		[Test]
		public void NodeAddressZeroEqualsTest()
		{
			var empty = new NodeAddress();
			var zero = new NodeAddress(0, 0);
			Assert.IsTrue(zero.Equals(empty));
		}
		[Test]
		public void NodeAddressNormalEqualsTest()
		{
			var first = new NodeAddress(22, 1);
			var second = new NodeAddress(22, 1);
			Assert.IsTrue(first.Equals(second));
		}
		[Test]
		public void NodeAddressMaxEqualsTest()
		{
			var first = new NodeAddress(255, 1023);
			var second = new NodeAddress(255, 1023);
			Assert.IsTrue(first.Equals(second));
		}
		
		[Test]
		public void NodeAddressNodeInequalityTest1()
		{
			var first = new NodeAddress(22, 1);
			var second = new NodeAddress(22, 2);
			Assert.IsFalse(first.Equals(second));
		}
		[Test]
		public void NodeAddressNodeInequalityTest2()
		{
			var first = new NodeAddress(22, 1);
			var second = new NodeAddress(22, 3);
			Assert.IsFalse(first == second);
		}
		[Test]
		public void NodeAddressNodeInequalityTest3()
		{
			var first = new NodeAddress(22, 1);
			var second = new NodeAddress(22, 3);
			Assert.IsFalse(second == first);
		}
		[Test]
		public void NodeAddressNodeInequalityTest4()
		{
			var first = new NodeAddress(22, 1);
			var second = new NodeAddress(22, 3);
			Assert.IsTrue(second != first);
		}
		[Test]
		public void NodeAddressNodeInequalityTest5()
		{
			var first = new NodeAddress(22, 1);
			var second = new NodeAddress(22, 3);
			Assert.IsTrue(first != second);
		}
		
		[Test]
		public void NodeAddressBrigadeInequalityTest()
		{
			var first = new NodeAddress(22, 1);
			var second = new NodeAddress(0, 1);
			Assert.IsTrue(first != second);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void NodeAddressBrigadeIsInvalidTest1()
		{
			var first = new NodeAddress(-1, 1);
		}
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void NodeAddressBrigadeIsInvalidTest2()
		{
			var first = new NodeAddress(256, 1);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void NodeAddressNodeIsInvalidTest1()
		{
			var first = new NodeAddress(22, -1);
		}
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void NodeAddressNodeIsInvalidTest2()
		{
			var first = new NodeAddress(22, 1024);
		}
		
		[Test]
		public void NodeAddressToStringTest1()
		{
			var first = new NodeAddress();
			string expected = "0 0";
			string actual = first.ToString();
			Assert.IsTrue(expected == actual);
		}
		[Test]
		public void NodeAddressToStringTest2()
		{
			var first = new NodeAddress(22, 1);
			string expected = "22 1";
			string actual = first.ToString();
			Assert.IsTrue(expected == actual);
		}
		
		[Test]
		public void NodeAddressFromPortAddressTest()
		{
			var port = new PortAddress(22, 456, 1);
			var first = new NodeAddress(port);
			var second = new NodeAddress(22, 456);
			Assert.IsTrue(first == second);
		}
		

	}
}

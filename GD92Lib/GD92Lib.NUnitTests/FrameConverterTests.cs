﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved


using System;
using NUnit.Framework;
using Thales.KentFire.GD92;

namespace GD92Lib.NUnitTests
{
	[TestFixture]
	public class FrameConverterTests
	{
		private static Node LocalNode;
		private static Node DestinationNode;
		
		[TestFixtureSetUp]
		public void Init()
		{
			FrameConverter.SetUAReceivedFrames(new MockReceivedFrames());
			FrameConverter.SetSequenceProviderAndEncoders(new MockGD92SequenceProvider());
            FrameConverter.SetNodeLookupAndDecoders(new MockRouter());
			LocalNode = new Node("THISNODE", new PortAddress(33, 0, 1));
			FrameConverter.SetLocalNode(LocalNode);
			DestinationNode = new Node("MDT456", new PortAddress(22, 456, 1));
		}
		
		#region MobiliseCommand Msg1

		[Test]
		public void MobiliseCommandTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg1());
			Assert.IsInstanceOf(typeof(GD92Msg1), messageFromFrames);
		}
		[Test]
		public void MobiliseCommandSourceNameTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg1());
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.From, "because the frame has not passed through the Router");
		}
		[Test]
		public void MobiliseCommandSourceAddressTest()
		{
			var messageIn = new GD92Msg1();
			messageIn.FromAddress = (GD92Address)LocalNode.Address;
			GD92Message messageFromFrames = ConvertToBinaryAndBack(messageIn);
			var expected = (GD92Address)LocalNode.Address;
			Assert.AreEqual(expected, messageFromFrames.FromAddress);
		}
		[Test]
		public void MobiliseCommandDestinationNameTest()
		{
			var messageIn = new GD92Msg1();
			messageIn.DestAddress = (GD92Address)DestinationNode.Address;
			GD92Message messageFromFrames = ConvertToBinaryAndBack(messageIn);
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.Destination, "because the frame has not passed through the Router");
		}
		[Test]
		public void MobiliseCommandToPeripheralsPortTest()
		{
			var messageIn = new GD92Msg1();
			messageIn.DestAddress = (GD92Address)DestinationNode.Address;
			GD92Message messageFromFrames = ConvertToBinaryAndBack(messageIn);
			var expected = new GD92Address(22, 456, GD92Header.PeripheralPortNumber); 
			Assert.AreEqual(expected, messageFromFrames.DestAddress, "because MobiliseCommand goes to Port 12");
		}
		[Test]
		public void MobiliseCommandToSpecificPortTest()
		{
			var messageIn = new GD92Msg1();
			messageIn.DestAddress = new GD92Address(22, 0, 14);
			var destination = new Node("MOB0", new PortAddress(22, 0, 14));
			GD92Message messageFromFrames = ConvertToBinaryAndBack(messageIn, destination);
			var expected = new GD92Address(22, 0, 14); 
			Assert.AreEqual(expected, messageFromFrames.DestAddress, "because the port in the destination node address is not 1");
		}

		#endregion


        #region Mobilise Msg6

        [Test]
        public void Mobilise6_InstanceTest()
        {
            var originalMessage = new GD92Msg6();
			originalMessage.FromAddress = (GD92Address)LocalNode.Address;
			originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
			originalMessage.IncidentTimeAndDate = DateTime.Now;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg6), messageFromFrames);
        }
//        [Test]
        public void Mobilise6_TextTest()
        {
            var originalMessage = new GD92Msg6();
            originalMessage.IncidentNumber = "1";
            originalMessage.LocationNotices = "Almost empty";
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg6)messageFromFrames;
            Assert.AreEqual(originalMessage.LocationNotices, message.LocationNotices);
        }
//        [Test]
        public void Mobilise6_TextResourceList()
        {
            var originalMessage = new GD92Msg6();
            originalMessage.IncidentNumber = "1";
            originalMessage.LocationNotices = "Almost empty";
            var resource = new GD92Msg6Resource("FJK60P1", "Commentary");
            originalMessage.ResourcesList.Add(resource);
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg6)messageFromFrames;
            Assert.AreEqual(originalMessage.LocationNotices, message.LocationNotices);
        }
//        [Test]
        public void Mobilise6_LongTextTest()
        {
            var originalMessage = new GD92Msg6();
            originalMessage.IncidentNumber = "1";
            originalMessage.LocationNotices = "Almost empty";
            // Converted apostrophes to ? to get this to work
            originalMessage.LocationNotices = @"This chapter will be about getting started with Git. We will begin at the beginning by
explaining some background on version control tools, then move on to how to get Git
running on your system and finally how to get it setup to start working with. At the end
of this chapter you should understand why Git is around, why you should use it and
you should be all setup to do so.
1.1 About Version Control
What is version control, and why should you care? Version control is a system that
records changes to a file or set of files over time so that you can recall specific versions
later. For the examples in this book you will use software source code as the files being
version controlled, though in reality you can do this with nearly any type of file on a
computer.
If you are a graphic or web designer and want to keep every version of an image
or layout (which you would most certainly want to), a Version Control System (VCS)
is a very wise thing to use. It allows you to revert files back to a previous state, revert
the entire project back to a previous state, compare changes over time, see who last
modified something that might be causing a problem, who introduced an issue and
when, and more. Using a VCS also generally means that if you screw things up or lose
files, you can easily recover. In addition, you get all this for very little overhead.
1.1.1 Local Version Control Systems
Many people?s version-control method of choice is to copy files into another directory
(perhaps a time-stamped directory, if they?re clever). This approach is very common
because it is so simple, but it is also incredibly error prone. It is easy to forget which
directory you?re in and accidentally write to the wrong file or copy over files you don?t
mean to.
To deal with this issue, programmers long ago developed local VCSs that had a
simple database that kept all the changes to files under revision control (see Figure
1.1).
One of the more popular VCS tools was a system called rcs, which is still distributed
with many computers today. Even the popular Mac OS X operating system
1";
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg6)messageFromFrames;
            Assert.AreEqual(originalMessage.LocationNotices, message.LocationNotices);
        }

        #endregion

        #region ResourceStatusRequest Msg5

		[Test]
		public void ResourceStatusRequestTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg5());
			Assert.IsInstanceOf(typeof(GD92Msg5), messageFromFrames);
		}
		[Test]
		public void ResourceStatusRequestCallsignNotSetTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg5());
			var message = (GD92Msg5)messageFromFrames;
			Assert.AreEqual(GD92Message.DefaultNotSet, message.Callsign, "because the callsign was not set");
		}

		#endregion

		#region Text Msg27

		[Test]
		public void Text27_InstanceTest()
		{
			var originalMessage = new GD92Msg27();
			originalMessage.Text = "Almost empty"; // Empty triggers assert length > 0
			GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			Assert.IsInstanceOf(typeof(GD92Msg27), messageFromFrames);
		}
		[Test]
		public void Text27_TextTest()
		{
			var originalMessage = new GD92Msg27();
			originalMessage.Text = "Almost empty"; 
			GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			var message = (GD92Msg27)messageFromFrames;
			Assert.AreEqual(originalMessage.Text, message.Text);
		}
		[Test]
		public void Text27_LongTextTest()
		{
			var originalMessage = new GD92Msg27();
			// Converted apostrophes to ? to get this to work
			originalMessage.Text = @"This chapter will be about getting started with Git. We will begin at the beginning by
explaining some background on version control tools, then move on to how to get Git
running on your system and finally how to get it setup to start working with. At the end
of this chapter you should understand why Git is around, why you should use it and
you should be all setup to do so.
1.1 About Version Control
What is version control, and why should you care? Version control is a system that
records changes to a file or set of files over time so that you can recall specific versions
later. For the examples in this book you will use software source code as the files being
version controlled, though in reality you can do this with nearly any type of file on a
computer.
If you are a graphic or web designer and want to keep every version of an image
or layout (which you would most certainly want to), a Version Control System (VCS)
is a very wise thing to use. It allows you to revert files back to a previous state, revert
the entire project back to a previous state, compare changes over time, see who last
modified something that might be causing a problem, who introduced an issue and
when, and more. Using a VCS also generally means that if you screw things up or lose
files, you can easily recover. In addition, you get all this for very little overhead.
1.1.1 Local Version Control Systems
Many people?s version-control method of choice is to copy files into another directory
(perhaps a time-stamped directory, if they?re clever). This approach is very common
because it is so simple, but it is also incredibly error prone. It is easy to forget which
directory you?re in and accidentally write to the wrong file or copy over files you don?t
mean to.
To deal with this issue, programmers long ago developed local VCSs that had a
simple database that kept all the changes to files under revision control (see Figure
1.1).
One of the more popular VCS tools was a system called rcs, which is still distributed
with many computers today. Even the popular Mac OS X operating system
1"; 
			GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			var message = (GD92Msg27)messageFromFrames;
			Assert.AreEqual(originalMessage.Text, message.Text);
		}

		#endregion

		#region ResourceStatus Msg20

		[Test]
		public void ResourceStatusTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg20());
			Assert.IsInstanceOf(typeof(GD92Msg20), messageFromFrames);
		}
		[Test]
		public void ResourceStatusCallsignNotSetTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg20());
			var message = (GD92Msg20)messageFromFrames;
			Assert.AreEqual(string.Empty, message.Callsign, "because the callsign was not set");
		}

		#endregion

		#region Alert Crew Msg40

		[Test]
		public void AlertCrewTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg40());
			Assert.IsInstanceOf(typeof(GD92Msg40), messageFromFrames);
		}
		[Test]
		public void AlertCrewSourceNameTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg40());
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.From, "because the frame has not passed through the Router");
		}
		[Test]
		public void AlertCrewSourceAddressTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg40());
			var expected = new GD92Address(0, 0, 0);
			Assert.AreEqual(expected, messageFromFrames.FromAddress, "As address is not set in the message");
		}
		[Test]
		public void AlertCrewDestinationNameTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg40());
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.Destination, "because the frame has not passed through the Router");
		}
		[Test]
		public void AlertCrewDataTest()
		{
			var originalMessage = new GD92Msg40();
			originalMessage.AlertGroup = "FA";
			originalMessage.ManualAck = true;
			GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.Destination, "because the frame has not passed through the Router");
		}

		#endregion

		#region Ack Msg50

		[Test]
		public void AckTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg50(new GD92Msg1()));
			Assert.IsInstanceOf(typeof(GD92Msg50), messageFromFrames);
		}
		[Test]
		public void AckSourceNameTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg50(new GD92Msg1()));
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.From, "because the frame has not passed through the Router");
		}
		[Test]
		public void AckSourceAddressTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg50(new GD92Msg1()));
			var expectedSource = new GD92Address(33, 555, 1);
			Assert.AreEqual(expectedSource, messageFromFrames.FromAddress, "because that is the destination set by MockReceivedFrames");
		}
		[Test]
		public void AckDestinationTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg50(new GD92Msg1()));
			var expectedDestination = new GD92Address(33, 444, 1);;
			Assert.AreEqual(expectedDestination, messageFromFrames.DestAddress, "because that is the source set by MockReceivedFrames");
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.Destination, "because the frame has not passed through the Router");
		}
		[Test]
		public void AckToOriginalSourcePortTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg50(new GD92Msg1()));
			Assert.IsInstanceOf(typeof(GD92Msg50), messageFromFrames);
			var expected = new GD92Address(33, 444, 1); 
			Assert.AreEqual(expected, messageFromFrames.DestAddress, "because Ack Destination is the original Source port");
		}

		#endregion
		

		#region Nak Msg51

		[Test]
		public void NakTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg51(new GD92Msg1()));
			Assert.IsInstanceOf(typeof(GD92Msg51), messageFromFrames);
		}
		[Test]
		public void NakSourceNameTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg51(new GD92Msg1()));
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.From, "because the frame has not passed through the Router");
		}
		[Test]
		public void NakSourceAddressTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg51(new GD92Msg1()));
			var expectedSource = new GD92Address(33, 555, 1);
			Assert.AreEqual(expectedSource, messageFromFrames.FromAddress, "because that is the destination set by MockReceivedFrames");
		}
		[Test]
		public void NakDestinationTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg51(new GD92Msg1()));
			var expectedDestination = new GD92Address(33, 444, 1);;
			Assert.AreEqual(expectedDestination, messageFromFrames.DestAddress, "because that is the source set by MockReceivedFrames");
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.Destination, "because the frame has not passed through the Router");
		}
		[Test]
		public void NakToOriginalSourcePortTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg51(new GD92Msg1()));
			var expected = new GD92Address(33, 444, 1); 
			Assert.AreEqual(expected, messageFromFrames.DestAddress, "because Ack Destination is the original Source port");
		}
		[Test]
		public void NakSetAndReasonUnchangedTest()
		{
			var message = new GD92Msg51(new GD92Msg1());
			var expectedSet = (int)GD92ReasonCodeSet.General;
			const int expectedCode = (int)GD92GeneralReasonCode.WaitAcknowledgement;
			message.ReasonCodeSet = expectedSet;
			message.ReasonCode = expectedCode;
			var messageFromFrames = (GD92Msg51)ConvertToBinaryAndBack(message);
			Assert.AreEqual(expectedSet, messageFromFrames.ReasonCodeSet);
			Assert.AreEqual(expectedCode, messageFromFrames.ReasonCode);
		}

		#endregion
		
		private GD92Message ConvertToBinaryAndBack(GD92Message message)
		{
			return ConvertToBinaryAndBack(message, DestinationNode);
		}
		private GD92Message ConvertToBinaryAndBack(GD92Message message, Node destination)
		{
            message.Destination = destination.Name;
            message.DestAddress = (GD92Address)destination.Address;
			Frame[] frames = FrameConverter.BuildFramesFromMessage(message);
			var newFrames = new Frame[frames.Length];
			for (int i = 0; i < frames.Length; i++)
			{
				newFrames[i] = FrameConverter.BuildFrame(frames[i].FrameBytes);
			}
			return FrameConverter.BuildMessageFromFrames(newFrames);
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    using Thales.KentFire.GD92.Decoders;
    using Thales.KentFire.LogLib;

    /// <summary>
    /// Tests based on data documented in TestInput\TextSourceLogs150421.txt
    /// The normal one and two frame messages are from Storm to node 706
    /// The first three-frame message is to node 611.
    /// The three-frame message with non-consecutive sequence numbers is to node 611.
    /// </summary>
    [TestFixture]
    public class ReceivedFramesTests
    {
        private readonly static IRouter TestRouter = new MockRouter();
        private static string TestInputPath;
        private static string TestOutputPath;
        private static Frame TextSingle;
        private static Frame Text1of2;
        private static Frame Text2of2;
        private static Frame Text1of3Adjusted;
        private static Frame Text1of3;
        private static Frame Text2of3;
        private static Frame Text3of3;
        private static Frame Text1of3Seq1;
        private static Frame Text2of3Seq3;
        private static Frame Text3of3Seq4;

        [TestFixtureSetUp]
        public void Init()
        {
            // Read local node configuration from Registry
            Router.Instance.ReadNodes();
            UserAgent.Instance.ReadConfiguration();
            TestInputPath = EncoderTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = EncoderTests.FolderFromAppConfigOrDefault("TestOutput");
            TextSingle = ReadFrameFromFile("TextSingle");
            Text1of2 = ReadFrameFromFile("Text1of2");
            Text2of2 = ReadFrameFromFile("Text2of2");
            Text1of3 = ReadFrameFromFile("Text1of3");
            Text2of3 = ReadFrameFromFile("Text2of3");
            Text3of3 = ReadFrameFromFile("Text3of3");
            Text1of3Seq1 = ReadFrameFromFile("Text1of3Seq1");
            Text2of3Seq3 = ReadFrameFromFile("Text2of3Seq3");
            Text3of3Seq4 = ReadFrameFromFile("Text3of3Seq4");
            Text1of3Adjusted = ReadFrameFromFile("Text1of3");
            Text1of3Adjusted.SequenceNumber = Text1of3.SequenceNumber - 1;
        }

        /// <summary>
        /// Cancel the timers in the frames.
        /// </summary>
        [TestFixtureTearDown]
        public void CleanUp()
        {
            // Need to do this before running RouterTests
            Router.Instance.RemoveAllNodes();
        }

        /// <summary>
        /// Read hexadecimal data (as output by BitConverter) and decode to a Frame.
        /// </summary>
        /// <param name="dataName">Path to data file</param>
        /// <returns>Frame constructed from log data in file.</returns>
		private static Frame ReadFrameFromFile(string dataName)
		{
		    string dataFromLog = ReadHexDataFromFile(dataName);
			byte[] bytes = LogReader.ConvertToByteArray(dataFromLog);
			var frame = new Frame(bytes);
        	var frameDecoder = new FrameDecoder(frame, TestRouter);
        	frameDecoder.DecodeFrame();
        	return frame;
		}

        /// <summary>
        /// Read hexadecimal data (as output by BitConverter) from text input path
        /// *** defined in this file ***.
        /// </summary>
        /// <param name="dataName"></param>
        /// <returns></returns>
		private static string ReadHexDataFromFile(string dataName)
		{
			string fileName = Path.Combine(TestInputPath, dataName + ".txt");
            using (var file = new StreamReader(fileName))
            {
            	return file.ReadToEnd();
            }
		}

        [Test]
        public void ReceivedFramesSingleFrameTest()
        {
            var testFrames = new ReceivedFrames();
            Frame[] frames = testFrames.GetAllFramesForMessage(TextSingle);
            Assert.AreEqual(TextSingle, frames[0], "Single frame returned directly (no need for matching)");
        }

        [Test]
        public void ReceivedFramesTwoFrameTest()
        {
            var testFrames = new ReceivedFrames();
            testFrames.AddFrameAndSetTimeout(Text1of2);
            Frame[] frames = testFrames.GetAllFramesForMessage(Text2of2);
            Assert.AreEqual(Text1of2, frames[0]);
            Assert.AreEqual(Text2of2, frames[1]);
            testFrames.ClearAndCancelFrameTimers();
        }

        [Test]
        public void ReceivedFramesTwoFrameFirstMissingTest()
        {
            var testFrames = new ReceivedFrames();
            Frame[] frames = testFrames.GetAllFramesForMessage(Text2of2);
            Assert.IsNull(frames);
        }

        [Test]
        public void ReceivedFramesThreeFrameTest()
        {
            var testFrames = new ReceivedFrames();
            testFrames.AddFrameAndSetTimeout(Text1of3);
            testFrames.AddFrameAndSetTimeout(Text2of3);
            Frame[] frames = testFrames.GetAllFramesForMessage(Text3of3);
            Assert.AreEqual(Text1of3, frames[0]);
            Assert.AreEqual(Text2of3, frames[1]);
            Assert.AreEqual(Text3of3, frames[2]);
            testFrames.ClearAndCancelFrameTimers();
        }

        [Test]
        public void ReceivedFramesThreeFrameSecondMissingTest()
        {
            var testFrames = new ReceivedFrames();
            testFrames.AddFrameAndSetTimeout(Text1of3);
            Frame[] frames = testFrames.GetAllFramesForMessage(Text3of3);
            Assert.IsNull(frames);
            testFrames.ClearAndCancelFrameTimers();
        }

        [Test]
        public void ReceivedFramesThreeFrameSequenceGapTest()
        {
            var testFrames = new ReceivedFrames();
            testFrames.AddFrameAndSetTimeout(Text1of3Seq1);
            testFrames.AddFrameAndSetTimeout(Text2of3Seq3);
            Frame[] frames = testFrames.GetAllFramesForMessage(Text3of3Seq4);
            Assert.AreEqual(Text1of3Seq1, frames[0]);
            Assert.AreEqual(Text2of3Seq3, frames[1]);
            Assert.AreEqual(Text3of3Seq4, frames[2]);
            testFrames.ClearAndCancelFrameTimers();
        }

        /// <summary>
        /// This mixes up the two messages. In practice the first message would
        /// be received and decoded before the frames arrive for the second message.
        /// </summary>
        [Test]
        public void ReceivedFramesThreeFrameAndThreeFrameSequenceGapTest()
        {
            var testFrames = new ReceivedFrames();
            testFrames.AddFrameAndSetTimeout(Text1of3);
            testFrames.AddFrameAndSetTimeout(Text2of3);
            testFrames.AddFrameAndSetTimeout(Text2of3);
            testFrames.AddFrameAndSetTimeout(Text1of3Seq1);
            testFrames.AddFrameAndSetTimeout(Text2of3Seq3);
            Frame[] frames = testFrames.GetAllFramesForMessage(Text3of3Seq4);
            Assert.AreEqual(Text1of3, frames[0]);
            Assert.AreEqual(Text2of3Seq3, frames[1]);
            Assert.AreEqual(Text3of3Seq4, frames[2]);
            testFrames.ClearAndCancelFrameTimers();
        }

        /// <summary>
        /// Two frame and three frame messages to different destinations.
        /// </summary>
        [Test]
        public void ReceivedFramesTwoFrameAndThreeFrameTest()
        {
            var testFrames = new ReceivedFrames();
            testFrames.AddFrameAndSetTimeout(Text1of2);
            testFrames.AddFrameAndSetTimeout(Text2of2);
            testFrames.AddFrameAndSetTimeout(Text1of3);
            testFrames.AddFrameAndSetTimeout(Text2of3);
            testFrames.AddFrameAndSetTimeout(Text3of3);
            Frame[] frames = testFrames.GetAllFramesForMessage(Text2of2);
            Assert.AreEqual(Text1of2, frames[0]);
            Assert.AreEqual(Text2of2, frames[1]);
            frames = testFrames.GetAllFramesForMessage(Text3of3);
            Assert.AreEqual(Text1of3, frames[0]);
            Assert.AreEqual(Text2of3, frames[1]);
            Assert.AreEqual(Text3of3, frames[2]);
            testFrames.ClearAndCancelFrameTimers();
        }

        /// <summary>
        /// This time there is a gap in
        /// sequence numbers in the three-frame message. It still works because the messages
        /// have different numbers of frames.
        /// </summary>
        [Test]
        public void ReceivedFramesTwoFrameAndThreeFrameAdjustedTest()
        {
            var testFrames = new ReceivedFrames();
            testFrames.AddFrameAndSetTimeout(Text1of2);
            testFrames.AddFrameAndSetTimeout(Text2of2);
            testFrames.AddFrameAndSetTimeout(Text1of3Adjusted);
            testFrames.AddFrameAndSetTimeout(Text2of3);
            testFrames.AddFrameAndSetTimeout(Text2of3);
            Frame[] frames = testFrames.GetAllFramesForMessage(Text2of2);
            Assert.AreEqual(Text1of2, frames[0]);
            Assert.AreEqual(Text2of2, frames[1]);
            frames = testFrames.GetAllFramesForMessage(Text3of3);
            Assert.AreEqual(Text1of3Adjusted, frames[0]);
            Assert.AreEqual(Text2of3, frames[1]);
            Assert.AreEqual(Text3of3, frames[2]);
            testFrames.ClearAndCancelFrameTimers();
        }
    }
}

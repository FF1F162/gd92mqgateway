﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{

    using System;
    using System.Configuration;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    using Thales.KentFire.LogLib;

    [TestFixture]
	public class EncoderTests
	{
		private static IReceivedFrames ReceivedTestFrames;
        private readonly static IRouter MockRouter = new MockRouter();
        private static EncoderFactory Encoders;
        private static string TestOutputPath;
        
        private static Node LocalNode;
        private static Node DestinationNode;
        private static string LongText;
        private static string testMsg2head;
        private static string testMsg2body;
        private static string testMsg2;
        private static string testMsg6head;
        private static string testMsg6body;
        private static string testMsg6bodySecondAttendance;
        private static string testMsg6rest;
        private static string testMsg6end;
        private static string testMsg6;
        private static string testMsg6SecondAttendance;
		
		[TestFixtureSetUp]
		public void Init()
		{
            TestOutputPath = FolderFromAppConfigOrDefault("TestOutput");
            IGD92SequenceProvider sequenceNumberProvider = new MockGD92SequenceProvider();
            ReceivedTestFrames = new MockReceivedFrames();
            IFrameProvider frameProvider= new FrameProvider(sequenceNumberProvider, ReceivedTestFrames);
            FrameConverter.SetNodeLookupAndDecoders(MockRouter);
            Encoders = new EncoderFactory(frameProvider);
            LocalNode = new Node("THISNODE", new PortAddress(33, 100, 1));
            DestinationNode = new Node("MDT456", new PortAddress(22, 456, 1));
            // Converted apostrophes to ? to get this to work when testing Msg27
            LongText = @"This chapter will be about getting started with Git. We will begin at the beginning by
explaining some background on version control tools, then move on to how to get Git
running on your system and finally how to get it setup to start working with. At the end
of this chapter you should understand why Git is around, why you should use it and
you should be all setup to do so.
1.1 About Version Control
What is version control, and why should you care? Version control is a system that
records changes to a file or set of files over time so that you can recall specific versions
later. For the examples in this book you will use software source code as the files being
version controlled, though in reality you can do this with nearly any type of file on a
computer.
If you are a graphic or web designer and want to keep every version of an image
or layout (which you would most certainly want to), a Version Control System (VCS)
is a very wise thing to use. It allows you to revert files back to a previous state, revert
the entire project back to a previous state, compare changes over time, see who last
modified something that might be causing a problem, who introduced an issue and
when, and more. Using a VCS also generally means that if you screw things up or lose
files, you can easily recover. In addition, you get all this for very little overhead.
1.1.1 Local Version Control Systems
Many people?s version-control method of choice is to copy files into another directory
(perhaps a time-stamped directory, if they?re clever). This approach is very common
because it is so simple, but it is also incredibly error prone. It is easy to forget which
directory you?re in and accidentally write to the wrong file or copy over files you don?t
mean to.
To deal with this issue, programmers long ago developed local VCSs that had a
simple database that kept all the changes to files under revision control (see Figure
1.1).
One of the more popular VCS tools was a system called rcs, which is still distributed
with many computers today. Even the popular Mac OS X operating system
1";
            testMsg2head = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg2>
  <SerialNumber>0</SerialNumber>
  <MsgType>MobiliseMessage</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""15"" />
  <Destination>MOB0</Destination>
  <SentAt>2001-02-08T12:19:59</SentAt>
  <ReceivedAt>2014-11-12T11:13:19.5167466+00:00</ReceivedAt>
  <From>MOB2</From>
  <ManualAck>true</ManualAck>
  <FromAddress>
    <Brigade>22</Brigade>
    <Node>2</Node>
    <Port>14</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>0</Node>
    <Port>14</Port>
  </DestAddress>
  ";
            testMsg2body = @"<MobTimeAndDate>2016-01-01T00:00:00</MobTimeAndDate>
  <CallsignList>
    <string>FJK60P1</string>
    <string>FJK60R1</string>
  </CallsignList>
  <IncidentNumber>1</IncidentNumber>
  <MobilisationType>1</MobilisationType>
  <Address>High Street, New Romney</Address>
  <HouseNumber />
  <Street />
  <SubDistrict />
  <District />
  <Town />
  <County />
  <Postcode />
  <MapRef>TN28 8AZ</MapRef>
  <TelNumber>999</TelNumber>
  <Text>This is some text</Text>
</GD92Msg2>";
            testMsg2 = testMsg2head + testMsg2body;

            testMsg6head = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6>
  <SerialNumber>0</SerialNumber>
  <MsgType>MobiliseMessage6</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""15"" />
  <Destination>MOB0</Destination>
  <SentAt>2001-02-08T12:19:59</SentAt>
  <ReceivedAt>2014-11-12T11:13:19.5167466+00:00</ReceivedAt>
  <From>MOB2</From>
  <ManualAck>true</ManualAck>
  <FromAddress>
    <Brigade>22</Brigade>
    <Node>2</Node>
    <Port>14</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>0</Node>
    <Port>14</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>Storm</DestNodeType>
  ";
            testMsg6body = @"<MobilisationType>1</MobilisationType>
  <AssistanceMessage />
  <ResourcesList>
    <GD92Msg6Resource>
      <Callsign>201</Callsign>
      <Commentary>SOME COMMENTARY</Commentary>
    </GD92Msg6Resource>
  </ResourcesList>
  <AllResourcesList />
  <IncidentTimeAndDate>2001-02-08T12:18:54</IncidentTimeAndDate>
  <MobTimeAndDate>2016-01-01T00:00:00</MobTimeAndDate>
  <IncidentNumber>1</IncidentNumber>
  <IncidentType>NATURE OF INCIDENT</IncidentType>
  <Address>High Street, New Romney</Address>
  <HouseNumber />
  <Street />
  <SubDistrict />
  <District />
  <Town />
  <County />
  <Postcode>TN28 8AZ</Postcode>
  <FirstAttendance>
    <Callsigns>
      <string>201</string>
    </Callsigns>
    <Address>High Street, New Romney</Address>
  </FirstAttendance>
  <SecondAttendance>
    <Callsigns />
    <Address />
  </SecondAttendance>
  <MapBook>EK200B6</MapBook>
  <MapRef>60661249</MapRef>
  <HowReceived>999</HowReceived>
  <TelNumber />
  <OperatorInfo>ADDITIONAL INFORMATION</OperatorInfo>
  <WaterInformation>F OPP SHIP HOTEL OS CINQUE PORT ARMS</WaterInformation>
  <LocationNotices>";
            testMsg6bodySecondAttendance = @"<MobilisationType>1</MobilisationType>
  <AssistanceMessage />
  <ResourcesList>
    <GD92Msg6Resource>
      <Callsign>201</Callsign>
      <Commentary>SOME COMMENTARY</Commentary>
    </GD92Msg6Resource>
  </ResourcesList>
  <AllResourcesList />
  <IncidentTimeAndDate>2001-02-08T12:18:54</IncidentTimeAndDate>
  <MobTimeAndDate>0001-01-01T00:00:00</MobTimeAndDate>
  <IncidentNumber>1</IncidentNumber>
  <IncidentType>NATURE OF INCIDENT</IncidentType>
  <Address>High Street, New Romney</Address>
  <HouseNumber />
  <Street />
  <SubDistrict />
  <District />
  <Town />
  <County />
  <Postcode>TN28 8AZ</Postcode>
  <FirstAttendance>
    <Callsigns />
    <Address>First Attendance, High Street, New Romney</Address>
  </FirstAttendance>
  <SecondAttendance>
    <Callsigns>
      <string>201</string>
    </Callsigns>
    <Address>Second Attendance, High Street, New Romney</Address>
  </SecondAttendance>
  <MapBook>EK200B6</MapBook>
  <MapRef>60661249</MapRef>
  <HowReceived>999</HowReceived>
  <TelNumber />
  <OperatorInfo>ADDITIONAL INFORMATION</OperatorInfo>
  <WaterInformation />
  <LocationNotices>";
            testMsg6rest = @"
Risk Card : 

Station Ground : 20
Caller Info :
High st, New Romney";
            testMsg6end = @"</LocationNotices>
  <AddText />
</GD92Msg6>";
            testMsg6 = testMsg6head + testMsg6body + testMsg6rest + testMsg6end;
            testMsg6SecondAttendance = testMsg6head + testMsg6bodySecondAttendance + testMsg6rest + testMsg6end;
		}
		
		#region MobiliseCommand Msg1

		[Test]
		public void MobiliseCommandTest()
		{
            var originalMessage = new GD92Msg1();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			Assert.IsInstanceOf(typeof(GD92Msg1), messageFromFrames);
		}
		[Test]
		public void MobiliseCommandSourceNameTest()
		{
            var originalMessage = new GD92Msg1();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.From, "because the frame has not passed through the Router");
		}
		[Test]
		public void MobiliseCommandSourceAddressTest()
		{
            var originalMessage = new GD92Msg1();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			var expected = (GD92Address)LocalNode.Address;
			Assert.AreEqual(expected, messageFromFrames.FromAddress);
		}
		[Test]
		public void MobiliseCommandDestinationNameTest()
		{
            var originalMessage = new GD92Msg1();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.Destination, "because the frame has not passed through the Router");
		}
		[Test]
		public void MobiliseCommandToPeripheralsPortTest()
		{
            var originalMessage = new GD92Msg1();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			var expected = new GD92Address(22, 456, GD92Header.PeripheralPortNumber); 
			Assert.AreEqual(expected, messageFromFrames.DestAddress, "because MobiliseCommand goes to Port 12");
		}
		[Test]
		public void MobiliseCommandToSpecificPortTest()
		{
            var originalMessage = new GD92Msg1();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = new GD92Address(22, 0, 14);
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			var expected = new GD92Address(22, 0, 14); 
			Assert.AreEqual(expected, messageFromFrames.DestAddress, "because the port in the destination node address is not 1");
		}

        [Test]
        public void MobiliseCommandManAckTest()
        {
            var originalMessage = new GD92Msg1();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = new GD92Address(22, 0, 14);
            originalMessage.ManualAck = true;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsTrue(messageFromFrames.ManualAck, "because it was set true originally");
        }
        
        #endregion

        #region MobiliseMessage msg2
        
        [Test]
        public void Mobilise2_InstanceTest()
        {
            var originalMessage = new GD92Msg2();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg2), messageFromFrames);
        }
        /// <summary>
        /// This writes the input and output messages to file and also compares XML in memory
        /// They differ in Destination, ReceivedAt and From fields as these are set during decode
        /// the time is current and Destination and From depend on what is in the router.
        /// The files show no line breaks in LocationNotices whereas the strings do (but do not compare)
        /// Write to file commented out when it has served its purpose (and git registers the date change)
        /// </summary>
        [Test]
        public void Mobilise2_EncodeDecodeTest()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg2>(testMsg2);
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            //originalMessage.ToXml(Path.Combine(TestOutputPath, "Msg2EncodeDecodeTest_Before.txt"));
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg2)messageFromFrames;
            string messageAsXml = message.ToXml();
            //message.ToXml(Path.Combine(TestOutputPath, "Msg2EncodeDecodeTest_After.txt"));
            Assert.IsTrue(messageAsXml.Contains(testMsg2body));
        }
        /// <summary>
        /// As above but with long LocationNotices to test multi-block
        /// Write to file commented out as it has served its purpose (and git registers the date change)
        /// </summary>
        [Test]
        public void Mobilise2_LongTextTest()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg2>(testMsg2);
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            // Converted apostrophes to ? to get this to work when testing Msg27
            originalMessage.Text = LongText;
            //originalMessage.ToXml(Path.Combine(TestOutputPath, "Msg2EncodeDecodeLong_Before.txt"));
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg2)messageFromFrames;
            //message.ToXml(Path.Combine(TestOutputPath, "Msg2EncodeDecodeLong_After.txt"));
            Assert.AreEqual(originalMessage.Text, message.Text);
        }
        [Test]
        public void Mobilise2_MapRefTest()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg2>(testMsg2);
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg2)messageFromFrames;
            Assert.AreEqual("TN28 8AZ", message.MapRef);
        }
        /// <summary>
        /// Set DestNodeType to encode in Storm format (no address elements). 
        /// FromAddress must resolve as Storm to get a compatible decoder.
        /// 1 Dec 2014 actual data from Storm is different from the sample supplied by Lee
        /// Not only does it include all the address elements, Storm uses them.
        /// </summary>
        [Test]
        public void Mobilise2_StormTest()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg2>(testMsg2);
            Node node = MockRouter.GetNodeByNameOrDefault("Storm");
            Assert.AreEqual(NodeType.Storm, node.GD92Type);
            originalMessage.Destination = node.Name;
            originalMessage.DestAddress = (GD92Address)node.Address;
            originalMessage.From = node.Name;
            originalMessage.FromAddress = (GD92Address)node.Address;
            originalMessage.DestNodeType = NodeType.Storm;
            originalMessage.Town = "Maidstone";
            //string expected = string.Empty;
            const string expected = "Maidstone";
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg2)messageFromFrames;
            Assert.AreEqual(expected, message.Town);
            Assert.AreEqual(originalMessage.FromAddress, message.FromAddress);
            Assert.AreEqual("Storm", message.From);
            Assert.AreEqual(originalMessage.DestAddress, message.DestAddress);
            Assert.AreEqual("Storm", message.Destination);
        }

        /// <summary>
        /// This tests that the encoder and decoder factories are providing the version that DOES deal with Address elements
        /// Set the destination name to something unknown
        /// </summary>
        [Test]
        public void Mobilise2_MOB1Test()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg2>(testMsg2);
            Node node = MockRouter.GetNodeByNameOrDefault("MOB1");
            Assert.AreEqual(NodeType.Mobs, node.GD92Type);
            originalMessage.From = node.Name;
            originalMessage.FromAddress = (GD92Address)node.Address;
            originalMessage.Destination = DestinationNode.Name;
            originalMessage.DestAddress = (GD92Address)node.Address;
            originalMessage.Town = "Maidstone";
            const string expected = "Maidstone";
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg2)messageFromFrames;
            Assert.AreEqual(expected, message.Town);
            Assert.AreEqual(originalMessage.FromAddress, message.FromAddress);
            Assert.AreEqual("MOB1", message.From);
            Assert.AreEqual(originalMessage.DestAddress, message.DestAddress);
            Assert.AreEqual("MOB1", message.Destination);
        }

        #endregion

        #region DutyStaffingUpdate Msg21 (not implemented)

        [Test]
        [ExpectedException("Thales.KentFire.GD92.GD92LibEncoderException")]
        public void DutyStaffingUpdateNotImplementedTest()
        {
            var originalMessage = new GD92Msg21();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg21), messageFromFrames);
        }
        
        #endregion

        #region ResourceStatusRequest Msg5

        [Test]
        public void ResourceStatusRequestTest()
        {
            var originalMessage = new GD92Msg5();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg5), messageFromFrames);
        }
        [Test]
        public void ResourceStatusRequestCallsignNotSetTest()
        {
            var originalMessage = new GD92Msg5();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg5)messageFromFrames;
            Assert.AreEqual(GD92Message.DefaultNotSet, message.Callsign, "because the callsign was not set");
        }

        #endregion

        #region Mobilise Msg6

        [Test]
        public void Mobilise6_InstanceTest()
        {
            var originalMessage = new GD92Msg6();
			originalMessage.FromAddress = (GD92Address)LocalNode.Address;
			originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
			originalMessage.IncidentTimeAndDate = DateTime.Now;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg6), messageFromFrames);
        }
        
        /// <summary>
        /// This writes the input and output messages to file and also compares XML in memory
        /// They differ in Destination, ReceivedAt and From fields as these are set during decode
        /// the time is current and Destination and From depend on what is in the router.
        /// The files show no line breaks in LocationNotices whereas the strings do (but do not compare)
        /// Write to file commented out as it has served its purpose (and git registers the date change)
        /// </summary>
        [Test]
        public void Mobilise6_EncodeDecodeTest()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg6>(testMsg6);
            //originalMessage.ToXml(Path.Combine(TestOutputPath, "Msg6EncodeDecodeTest_BeforeNewfield.txt"));
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg6)messageFromFrames;
            string messageAsXml = message.ToXml();
            //message.ToXml(Path.Combine(TestOutputPath, "Msg6EncodeDecodeTest_AfterNewfield.txt"));
            Assert.IsTrue(messageAsXml.Contains(testMsg6body));
            Assert.IsTrue(originalMessage.LocationNotices.Length == message.LocationNotices.Length);
            // TODO  These all fail - to do with line endings I think
            //Assert.AreSame(originalMessage.LocationNotices, message.LocationNotices);
            //Assert.AreSame(testMsg6rest, message.LocationNotices);
            //Assert.AreSame(testMsg6rest, originalMessage.LocationNotices);
        }

        /// <summary>
        /// Show that if you supply a first attendance address (but no callsign) it is ignored.
        /// </summary>
        [Test]
        public void Mobilise6_EncodeDecodeSecondAttendanceTest()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg6>(testMsg6SecondAttendance);
            Assert.AreEqual(0, originalMessage.FirstAttendance.Callsigns.Count);
            Assert.IsNotNullOrEmpty(originalMessage.FirstAttendance.Address);
            //originalMessage.ToXml(Path.Combine(TestOutputPath, "Msg6EncodeDecodeSecondAttendanceTest_Before.txt"));
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg6)messageFromFrames;
            string messageAsXml = message.ToXml();
            //message.ToXml(Path.Combine(TestOutputPath, "Msg6EncodeDecodeSecondAttendanceTest_After.txt"));
            Assert.AreEqual(0, message.FirstAttendance.Callsigns.Count);
            Assert.AreEqual(0, message.FirstAttendance.Address.Length);
        }

        /// <summary>
        /// As Mobilise6_EncodeDecodeTest but with long LocationNotices to test multi-block
        /// Write to file commented out as it has served its purpose (and git registers the date change)
        /// </summary>
        [Test]
        public void Mobilise6_LongTextTest()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg6>(testMsg6);
            // Converted apostrophes to ? to get this to work when testing Msg27
            originalMessage.LocationNotices = LongText;
            //originalMessage.ToXml(Path.Combine(TestOutputPath, "Msg6EncodeDecodeLong_BeforeNewfield.txt"));
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg6)messageFromFrames;
            string messageAsXml = message.ToXml();
            //message.ToXml(Path.Combine(TestOutputPath, "Msg6EncodeDecodeLong_AfterNewfield.txt"));
            Assert.IsTrue(messageAsXml.Contains(testMsg6body));
            Assert.AreEqual(originalMessage.LocationNotices, message.LocationNotices);
        }

        #endregion

		#region ResourceStatus Msg20

		[Test]
		public void ResourceStatusTest()
		{
			GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg20());
			Assert.IsInstanceOf(typeof(GD92Msg20), messageFromFrames);
		}
		[Test]
		public void ResourceStatusCallsignNotSetTest()
		{
            var originalMessage = new GD92Msg20();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			var message = (GD92Msg20)messageFromFrames;
			Assert.AreEqual(string.Empty, message.Callsign, "because the callsign was not set");
		}
		
        [Test]
        public void ResourceStatusCompressedStringTest()
        {
            var originalMessage = new GD92Msg20();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            const string expected = "This has more than three spaces                 and 111111111s";
            originalMessage.Remarks = expected;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg20)messageFromFrames;
            Assert.AreEqual(expected, message.Remarks);
        }
        
        [Test]
        public void ResourceStatusAvlTypeTest()
        {
            GD92Msg20 originalMessage = PopulateMessage20();
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg20)messageFromFrames;
            Assert.AreEqual(33, message.AvlType);
        }
        [Test]
        public void ResourceStatusAvlDataTest()
        {
            GD92Msg20 originalMessage = PopulateMessage20();
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg20)messageFromFrames;
            Assert.AreEqual("DATA", message.AvlData);
        }
        [Test]
        public void ResourceStatusStatusTest()
        {
            GD92Msg20 originalMessage = PopulateMessage20();
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg20)messageFromFrames;
            Assert.AreEqual(1, message.StatusCode);
       }
        [Test]
        public void ResourceStatusCallsignTest()
        {
            GD92Msg20 originalMessage = PopulateMessage20();
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg20)messageFromFrames;
            Assert.AreEqual("FJK60P1", message.Callsign);
        }
        [Test]
        public void ResourceStatusRemarksTest()
        {
            GD92Msg20 originalMessage = PopulateMessage20();
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg20)messageFromFrames;
            Assert.AreEqual("These are some remarks", message.Remarks);
        }
        private GD92Msg20 PopulateMessage20()
        {
            var originalMessage = new GD92Msg20();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            originalMessage.Callsign = "FJK60P1";
            originalMessage.StatusCode = 1;
            originalMessage.AvlType = 33;
            originalMessage.AvlData = "DATA";
            originalMessage.Remarks = "These are some remarks";
            return originalMessage;
        }

        #endregion

        #region LogUpdate Msg22

        [Test]
        public void LogUpdateTest()
        {
            GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg22());
            Assert.IsInstanceOf(typeof(GD92Msg22), messageFromFrames);
        }
        [Test]
        public void LogUpdateIncidentNumberTest()
        {
            GD92Message messageFromFrames = ConvertToBinaryAndBack(new GD92Msg22());
            var message = (GD92Msg22)messageFromFrames;
            Assert.AreEqual(FrameConverter.DefaultIncidentNumber.ToString(System.Globalization.CultureInfo.InvariantCulture),
                message.IncidentNumber);
        }
        [Test]
        public void LogUpdateCallsignNotSetTest()
        {
            var originalMessage = new GD92Msg22();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg22)messageFromFrames;
            Assert.AreEqual(string.Empty, message.Callsign, "because the callsign was not set");
        }
        
        [Test]
        public void LogUpdateCompressedStringTest()
        {
            var originalMessage = new GD92Msg22();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            const string expected = "This has more than three spaces                 and 111111111s";
            originalMessage.Update = expected;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg22)messageFromFrames;
            Assert.AreEqual(expected, message.Update);
        }

        #endregion

        #region Text Msg27

        [Test]
        public void Text27_InstanceTest()
        {
            var originalMessage = new GD92Msg27();
            originalMessage.Text = "Almost empty"; // Empty triggers assert length > 0
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg27), messageFromFrames);
        }
        [Test]
        public void Text27_TextTest()
        {
            var originalMessage = new GD92Msg27();
            originalMessage.Text = "Almost empty";
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg27)messageFromFrames;
            Assert.AreEqual(originalMessage.Text, message.Text);
        }
        [Test]
        public void Text27_LongTextTest()
        {
            var originalMessage = new GD92Msg27();
            // Converted apostrophes to ? to get this to work
            originalMessage.Text = LongText;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg27)messageFromFrames;
            Assert.AreEqual(originalMessage.Text, message.Text);
        }

        #endregion

        #region Alert Crew Msg40

		[Test]
		public void AlertCrewTest()
		{
            var originalMessage = new GD92Msg40();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
			Assert.IsInstanceOf(typeof(GD92Msg40), messageFromFrames);
		}
        [Test]
        [ExpectedException("Thales.KentFire.GD92.GD92LibEncoderException")]
        public void AlertCrewBadAlerterCodeTest()
        {
            var originalMessage = new GD92Msg40();
            originalMessage.AlertGroup = "Too long";
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg40), messageFromFrames);
        }
        [Test]
		public void AlertCrewToPeripheralsPortTest()
		{
            var originalMessage = new GD92Msg40();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var expected = new GD92Address(22, 456, GD92Header.AlerterPortNumber); 
			Assert.AreEqual(expected, messageFromFrames.DestAddress, "because AlertCrew goes to Port 13");
		}

		#endregion

        #region Ack Msg50

        [Test]
        public void AckTest()
        {
            var originalMessage = new GD92Msg50();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg50), messageFromFrames);
            Assert.IsFalse(messageFromFrames.ManualAck);
        }
        [Test]
        public void AckToFrameTest()
        {
            var message = new GD92Msg50();
            message.FromAddress = (GD92Address)LocalNode.Address;
            message.DestAddress = (GD92Address)DestinationNode.Address;
            IMessageEncoder encoder = Encoders.EncoderForMessageType(message);
			Frame[] frames = encoder.EncodeMessage(message);
			Assert.IsTrue(frames[0].AckRequired, "because original message is null");
            Assert.IsFalse(frames[0].ManAckRequired);
        }
        [Test]
        public void AckMsg6Test()
        {
            var originalMessage = GD92Message.ReadFromMemory<GD92Msg6>(testMsg6);
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.From = "Source";
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            originalMessage.Destination = "Destination";
            IMessageEncoder encoder = Encoders.EncoderForMessageType(originalMessage);
			Frame[] frames = encoder.EncodeMessage(originalMessage);
			Assert.AreEqual("Source", frames[0].SourceName);
			Assert.AreEqual("Destination", frames[0].DestinationName);
			ReceivedTestFrames.AddFramePendingReply(frames[0]);
            var message = new GD92Msg50(originalMessage);
            GD92Message messageFromFrames = ConvertToBinaryAndBack(message);
            Assert.IsInstanceOf(typeof(GD92Msg50), messageFromFrames);
            Assert.IsFalse(messageFromFrames.ManualAck);
            Assert.AreEqual(originalMessage.FromAddress, messageFromFrames.DestAddress);
            Assert.AreEqual(originalMessage.DestAddress.Node, messageFromFrames.FromAddress.Node);
            //Assert.AreEqual(originalMessage.From, messageFromFrames.Destination);
            Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.Destination);
            //Assert.AreEqual(originalMessage.Destination, messageFromFrames.From);
            Assert.AreEqual("MockNodeNameForAddress", messageFromFrames.From);
        }

        #endregion

        #region Nak Msg51

        [Test]
        public void NakTest()
        {
            var originalMessage = new GD92Msg51();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg51), messageFromFrames);
        }

        #endregion

        #region Printer Status Msg65

        [Test]
        public void PrinterStatusTest()
        {
            var originalMessage = new GD92Msg65();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            Assert.IsInstanceOf(typeof(GD92Msg65), messageFromFrames);
        }
        /// <summary>
        /// Show that an unexpected value survives the trip (no exception)
        /// </summary>
        [Test]
        public void PrinterStatusUnknownStatusTest()
        {
            var originalMessage = new GD92Msg65();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
            const int expected = 99;
            originalMessage.PrinterStatus = expected;
            GD92Message messageFromFrames = ConvertToBinaryAndBack(originalMessage);
            var message = (GD92Msg65)messageFromFrames;
            Assert.AreEqual(expected, message.PrinterStatus, "because that is what was set");
        }

        #endregion


        #region Raw Frame Msg255

        [Test]
        public void RawFrameTest()
        {
            var originalMessage = new RawFrameMsg();
            originalMessage.FromAddress = (GD92Address)LocalNode.Address;
            originalMessage.DestAddress = (GD92Address)DestinationNode.Address;
			originalMessage.LogOfFrameBytes = "01-16-AF-81-0F-C1-16-00-0A-11";
            IMessageEncoder encoder = Encoders.EncoderForMessageType(originalMessage);
			Frame[] frames = encoder.EncodeMessage(originalMessage);
			Assert.AreEqual(1, frames.Length);
			Assert.AreEqual(10, frames[0].FrameBytes[8]);
			Assert.AreEqual(10, frames[0].FrameBytes.Length);
        }

        #endregion

		private GD92Message ConvertToBinaryAndBack(GD92Message message)
		{
            IMessageEncoder encoder = Encoders.EncoderForMessageType(message);
			Frame[] frames = encoder.EncodeMessage(message);
			var newFrames = new Frame[frames.Length];
			for (int i = 0; i < frames.Length; i++)
			{
				newFrames[i] = FrameConverter.BuildFrame(frames[i].FrameBytes);
			}
			return FrameConverter.BuildMessageFromFrames(newFrames);
		}

        public const string DefaultPathToTestFolders = @"C:\git\kfrs\StormGateway\GD92Lib";
        public static string FolderFromAppConfigOrDefault(string key)
        {
            string folder = null;
            folder = ConfigurationManager.AppSettings[key];
            if (folder == null)
            {
                folder = Path.Combine(DefaultPathToTestFolders, key);
            }
            return folder;
        }
    }
}

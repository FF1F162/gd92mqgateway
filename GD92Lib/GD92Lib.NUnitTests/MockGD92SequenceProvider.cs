﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved


using System;
using Thales.KentFire.GD92;

namespace GD92Lib.NUnitTests
{
	/// <summary>
	/// Simplified IGD92SequenceProvider that provides sequence numbers without using the Registry
	/// The sequence number last allocated can be set by the tester
	/// </summary>
	public class MockGD92SequenceProvider : IGD92SequenceProvider
	{
		public int LastSequenceNumber { get; set; }

		public int GetSingleSequenceNumber()
        {
            var frameNumberArray = new int[1];
            this.FillArrayWithSequenceNumbers(frameNumberArray);
            return frameNumberArray[0];
        }
        public void FillArrayWithSequenceNumbers(int[] frameNumberArray)
        {
        	int last = ValidLastSequenceNumberOrZero();
        	for (int i = 0; i < frameNumberArray.Length; i++)
        	{
        		last = (last + 1) % (FrameConverter.MaxFrameSequenceNumber + 1);
        		frameNumberArray[i] = last;
        	}
         	LastSequenceNumber = last;
        }
        private int ValidLastSequenceNumberOrZero()
        {
        	int last = this.LastSequenceNumber;
        	if (last < 0 || last > FrameConverter.MaxFrameSequenceNumber)
        	{
        		last = 0;
        	}
        	return last;
        }
	}
}

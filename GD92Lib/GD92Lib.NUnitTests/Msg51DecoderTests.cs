﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    using Thales.KentFire.GD92.Decoders;
    using Thales.KentFire.LogLib;

    [TestFixture]
    public class Msg51DecoderTests
    {
        private readonly static IRouter MockRouter = new MockRouter();
        private static string TestInputPath;
        private static string TestOutputPath;
        private static string NakWaitAck;
        private static string NakSelf;
        private static string NakWaitAckBody;
        private static string NakSelfBody;

        [TestFixtureSetUp]
        public void Init()
        {
            TestInputPath = EncoderTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = EncoderTests.FolderFromAppConfigOrDefault("TestOutput");
            NakWaitAck = ReadHexDataFromFile("NakWaitAck");
            NakSelf = ReadHexDataFromFile("NakSelf");
            NakWaitAckBody = @"  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>22</Brigade>
    <Node>201</Node>
    <Port>13</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>2</Node>
    <Port>14</Port>
  </DestAddress>
  <FromNodeType>Rsc</FromNodeType>
  <DestNodeType>Rsc</DestNodeType>
  <OriginalDestination>
    <Brigade>22</Brigade>
    <Node>201</Node>
    <Port>13</Port>
  </OriginalDestination>
  <ReasonCodeSet>1</ReasonCodeSet>
  <ReasonCode>9</ReasonCode>
</GD92Msg51>";
            NakSelfBody = @"  <FromAddress>
    <Brigade>22</Brigade>
    <Node>201</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>201</Node>
    <Port>1</Port>
  </DestAddress>
  <FromNodeType>Rsc</FromNodeType>
  <DestNodeType>Rsc</DestNodeType>
  <OriginalDestination>
    <Brigade>22</Brigade>
    <Node>0</Node>
    <Port>14</Port>
  </OriginalDestination>
  <ReasonCodeSet>1</ReasonCodeSet>
  <ReasonCode>18</ReasonCode>
</GD92Msg51>";
        }
        /// <summary>
        /// Read hexadecimal data (as output by BitConverter) from text input path defined in this file
        /// </summary>
        /// <param name="dataName"></param>
        /// <returns></returns>
        private static string ReadHexDataFromFile(string dataName)
        {
            string fileName = Path.Combine(TestInputPath, dataName + ".txt");
            using (var file = new StreamReader(fileName))
            {
                return file.ReadToEnd();
            }
        }

        [Test]
        public void Msg51NakWaitAckTest()
        {
            string output = DecodeLogData(NakWaitAck);
            //WriteDataToFile(output, "NakWaitAck");
            Assert.IsTrue(output.EndsWith(NakWaitAckBody));
        }

        [Test]
        public void Msg51NakSelfTest()
        {
            string output = DecodeLogData(NakSelf);
            //WriteDataToFile(output, "NakSelf");
            Assert.IsTrue(output.EndsWith(NakSelfBody));
        }

        [Test]
        public void Msg51NakWaitAckHeaderTest()
        {
            GD92Message message = MessageFromFrames(FramesFromLogData(NakWaitAck));
            Assert.IsNotNull(message.Header);
        }

        private static void WriteDataToFile(string data, string dataName)
        {
            string fileName = Path.Combine(TestOutputPath, dataName + ".txt");
            using (var file = new StreamWriter(fileName))
            {
                file.Write(data);
            }
        }

        /// <summary>
        /// Convert log data for a single frame (as output by BitConverter) back to an array of bytes
        /// Use this to build an array of frames comprising the message (in this case just one frame)
        /// Decode and serialize the message to XML in a file (.txt)
        /// </summary>
        /// <param name="dataFromLog"></param>
        private static string DecodeLogData(string dataFromLog)
        {
            return DecodeFramesAndMessage(FramesFromLogData(dataFromLog));
        }

        private static Frame[] FramesFromLogData(string dataFromLog)
        {
            byte[] bytes = LogReader.ConvertToByteArray(dataFromLog);
            var frames = new Frame[1];
            frames[0] = new Frame(bytes);
            return frames;
        }

        private static string DecodeFramesAndMessage(Frame[] frames)
        {
            var message = MessageFromFrames(frames);
            var mobiliseMessage = message as GD92Msg51 ?? new GD92Msg51();
            return mobiliseMessage.ToXml();
        }

        private static GD92Message MessageFromFrames(Frame[] frames)
        {
           foreach (Frame frame in frames)
            {
                var frameDecoder = new FrameDecoder(frame, MockRouter);
                frameDecoder.DecodeFrame();
            }
            var decoder = new Msg51Decoder(frames);
            return decoder.DecodeMessage();
        }
    }
}

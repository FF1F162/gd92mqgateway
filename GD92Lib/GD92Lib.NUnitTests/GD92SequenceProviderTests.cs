﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    
    [TestFixture]
    public class GD92SequenceProviderTests
    {
        [Test]
        public void GetFrameNumbersFromFirstFrameTest()
        {
            int[] numbers = GD92SequenceProvider.GetFrameNumbers(30245, 1, 3);
            Assert.AreEqual(30245, numbers[0]);
            Assert.AreEqual(30246, numbers[1]);
            Assert.AreEqual(30247, numbers[2]);
        }

        [Test]
        public void GetFrameNumbersFromSecondFrameTest()
        {
            int[] numbers = GD92SequenceProvider.GetFrameNumbers(30246, 2, 3);
            Assert.AreEqual(30245, numbers[0]);
            Assert.AreEqual(30246, numbers[1]);
            Assert.AreEqual(30247, numbers[2]);
        }

        [Test]
        public void GetFrameNumbersFromThirdFrameTest()
        {
            int[] numbers = GD92SequenceProvider.GetFrameNumbers(30247, 3, 3);
            Assert.AreEqual(30245, numbers[0]);
            Assert.AreEqual(30246, numbers[1]);
            Assert.AreEqual(30247, numbers[2]);
        }
    
        [Test]
        public void GetFrameNumbersFromWrappedFirstFrameTest()
        {
            int[] numbers = GD92SequenceProvider.GetFrameNumbers(32767, 1, 3);
            Assert.AreEqual(32767, numbers[0]);
            Assert.AreEqual(0, numbers[1]);
            Assert.AreEqual(1, numbers[2]);
        }
    
        [Test]
        public void GetFrameNumbersFromWrappedSecondFrameTest()
        {
            int[] numbers = GD92SequenceProvider.GetFrameNumbers(0, 2, 3);
            Assert.AreEqual(32767, numbers[0]);
            Assert.AreEqual(0, numbers[1]);
            Assert.AreEqual(1, numbers[2]);
        }
    
        [Test]
        public void GetFrameNumbersFromWrappedThirdFrameTest()
        {
            int[] numbers = GD92SequenceProvider.GetFrameNumbers(1, 3, 3);
            Assert.AreEqual(32767, numbers[0]);
            Assert.AreEqual(0, numbers[1]);
            Assert.AreEqual(1, numbers[2]);
        }
    
//        /// <summary>
//        /// There is an assertion to prevent using GetFrameNumbers for a single block message
//        /// because it is not needed (not because it does not work).
//        /// </summary>
//        [Test]
//        public void GetFrameNumbersFromWrappedSingleFrameTest()
//        {
//            int[] numbers = GD92SequenceProvider.GetFrameNumbers(1, 1, 1);
//            Assert.AreEqual(1, numbers[0]);
//        }

        [Test]
        public void GetPreviousFrameNumberTest()
        {
            int supplied = 42;
            int expected = supplied - 1;
            int actual = GD92SequenceProvider.GetPreviousFrameNumber(supplied);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPreviousFrameNumberWrapTest()
        {
            int supplied = 0;
            int expected = FrameConverter.MaxFrameSequenceNumber;
            int actual = GD92SequenceProvider.GetPreviousFrameNumber(supplied);
            Assert.AreEqual(expected, actual);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using Thales.KentFire.GD92;

    /// <summary>
	/// Simplified IReceivedFrames that returns a frame with dummy data that can be used
    /// to construct a reply	
	/// </summary>
	internal class MockReceivedFrames : IReceivedFrames
	{
		private Frame FrameToAcknowledge;
		
		public Frame GetFrameToAcknowledge(int serialNumber)
		{
			var frame = this.FrameToAcknowledge;
			if (frame == null)
			{
				frame = new Frame();
				frame.Source = new PortAddress(33, 444, 1);
	            frame.Destination = new PortAddress(33, 555, 1);
	            frame.SequenceNumber = 999;
	            frame.Priority = 6;
	            frame.AckRequired = true;				
			}
			return frame;
		}
		public void AddFramePendingReply(Frame frame)
		{
			this.FrameToAcknowledge = frame;
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
    public class MockRouterTests
    {
        static IRouter MockRouter;

        [TestFixtureSetUp]
        public void Init()
        {
            MockRouter = new MockRouter();
        }

        [Test]
        public void GetNodeByNameNameTest()
        {
            Node node = MockRouter.GetNodeByNameOrDefault("Node801");
            Assert.AreEqual("MockNodeName", node.Name);
        }
        [Test]
        public void GetNodeByNameTypeTest()
        {
            Node node = MockRouter.GetNodeByNameOrDefault("Node801");
            Assert.AreEqual(NodeType.Rsc, node.GD92Type);
        }
        [Test]
        public void GetNodeByNameStormTest()
        {
            Node node = MockRouter.GetNodeByNameOrDefault("Storm");
            Assert.AreEqual(NodeType.Storm, node.GD92Type);
        }
        [Test]
        public void GetNodeByNodeAddressStormTest()
        {
            Node node = MockRouter.GetNodeByAddressOrDefault(new NodeAddress(22, 0));
            Assert.AreEqual(NodeType.Storm, node.GD92Type);
        }
        [Test]
        public void GetNodeByNodeAddressUnknownTest()
        {
            Node node = MockRouter.GetNodeByAddressOrDefault(new NodeAddress(22, 99));
            Assert.AreEqual(NodeType.Rsc, node.GD92Type);
        }
    }
}

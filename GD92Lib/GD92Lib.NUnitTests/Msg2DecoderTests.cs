﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;
    using Thales.KentFire.GD92.Decoders;
    using Thales.KentFire.LogLib;

	[TestFixture]
	public class Msg2DecoderTests
	{
        private readonly static IRouter MockRouter = new MockRouter();
        private static string TestInputPath;
        private static string TestOutputPath;
        private static string StormMobiliseSingle;
        private static string StormSingleBody;
        private static string StormMobilise1of2;
        private static string StormMobilise2of2;
        private static string StormTwoBlockBody;

        [TestFixtureSetUp]
		public void Init()
		{
            TestInputPath = EncoderTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = EncoderTests.FolderFromAppConfigOrDefault("TestOutput");
            StormMobiliseSingle = ReadHexDataFromFile("StormMobiliseSingle");
            StormMobilise1of2 = ReadHexDataFromFile("StormMobilise1of2");
            StormMobilise2of2 = ReadHexDataFromFile("StormMobilise2of2");
            StormSingleBody = @"  <From>Storm</From>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>22</Brigade>
    <Node>0</Node>
    <Port>10</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>703</Node>
    <Port>24</Port>
  </DestAddress>
  <FromNodeType>Storm</FromNodeType>
  <DestNodeType>Rsc</DestNodeType>
  <MobTimeAndDate>2014-11-28T15:22:08</MobTimeAndDate>
  <CallsignList>
    <string>FJK21P1</string>
  </CallsignList>
  <IncidentNumber>29</IncidentNumber>
  <MobilisationType>1</MobilisationType>
  <Address>ALEX FARM</Address>
  <HouseNumber />
  <Street>DUCK LN</Street>
  <SubDistrict />
  <District>SHADOXHURST</District>
  <Town>ASHFORD</Town>
  <County>KENT</County>
  <Postcode>TN261LT</Postcode>
  <MapRef>597024137541</MapRef>
  <TelNumber />
  <Text>Incident Time:  15:20:41 28/11/2014Incident Type:  FIRE CHIMNEY, FIRE CHIMNEYAssistance message:  All resouces list:  FJK11P1,FJK20P1,FJK11R1First Attendance:  FJK21P1 [:], ALEX FARM, DUCK LN, SHADOXHURST, ASHFORDMap book:  OSGR:  597024137541Tel Number:  Loc Info:  Location Notices:  Location Risk:  Location Comment:  </Text>
</GD92Msg2>";
            StormTwoBlockBody = @"  <From>Storm</From>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>22</Brigade>
    <Node>0</Node>
    <Port>10</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>22</Brigade>
    <Node>702</Node>
    <Port>24</Port>
  </DestAddress>
  <FromNodeType>Storm</FromNodeType>
  <DestNodeType>Rsc</DestNodeType>
  <MobTimeAndDate>2014-11-28T15:43:06</MobTimeAndDate>
  <CallsignList>
    <string>FJK11P1</string>
    <string>FJK11R1</string>
  </CallsignList>
  <IncidentNumber>32</IncidentNumber>
  <MobilisationType>1</MobilisationType>
  <Address>ALEX FARM</Address>
  <HouseNumber />
  <Street>DUCK LN</Street>
  <SubDistrict />
  <District>SHADOXHURST</District>
  <Town>ASHFORD</Town>
  <County>KENT</County>
  <Postcode>TN261LT</Postcode>
  <MapRef>597024137541</MapRef>
  <TelNumber />
  <Text>Incident Time:  15:38:47 28/11/2014Incident Type:  FIRE CHIMNEY, THIS IS ADDITIONAL TEXTAssistance message:  All resouces list:  First Attendance:  FJK11P1 [PL:],FJK11R1 [:], ALEX FARM, DUCK LN, SHADOXHURST, ASHFORDMap book:  UDF8 LOC RECOSGR:  597024137541Tel Number:  Loc Info:  Location Notices:  THIS IS AN OBJECT AND ITS TYPE IS RISK, THIS IS A LOCATION NOTICE WITH LOTS OF TEXT MMMMMMMMMMM FOR VERYONE TO VEIW . PART OF TESTING FOR JOHN AND PHIL ...............Location Risk:  Location Comment:   </Text>
</GD92Msg2>";
        }
        /// <summary>
        /// Read hexadecimal data (as output by BitConverter) from text input path defined in this file
        /// </summary>
        /// <param name="dataName"></param>
        /// <returns></returns>
		private static string ReadHexDataFromFile(string dataName)
		{
			string fileName = Path.Combine(TestInputPath, dataName + ".txt");
            using (var file = new StreamReader(fileName))
            {
            	return file.ReadToEnd();
            }
		}

        [Test]
        public void Msg2DecoderMessageInSingleBlockTest()
        {
            string output = DecodeLogData(StormMobiliseSingle);
            Assert.IsTrue(output.EndsWith(StormSingleBody));
        }

        [Test]
		public void Msg2DecoderMessageInTwoBlocksTest()
		{
			string output = DecodeMessageInTwoFrames(StormMobilise1of2, StormMobilise2of2);
            Assert.IsTrue(output.EndsWith(StormTwoBlockBody));
		}

		/// <summary>
		/// Convert log data for a single frame (as output by BitConverter) back to an array of bytes
		/// Use this to build an array of frames comprising the message (in this case just one frame)
		/// Decode and serialize the message to XML in a file (.txt)
		/// </summary>
		/// <param name="dataFromLog"></param>
		public static string DecodeLogData(string dataFromLog)
		{
			byte[] bytes = LogReader.ConvertToByteArray(dataFromLog);
			var frames = new Frame[1];
			frames[0] = new Frame(bytes);
			return DecodeFramesAndMessage(frames);
		}
		public static string DecodeMessageInTwoFrames(string firstFromLog, string second)
		{
			var frames = new Frame[2];
			frames[0] = new Frame(LogReader.ConvertToByteArray(firstFromLog));
			frames[1] = new Frame(LogReader.ConvertToByteArray(second));
			return DecodeFramesAndMessage(frames);
		}
		private static string DecodeFramesAndMessage(Frame[] frames)
		{
			foreach (Frame frame in frames)
			{
	        	var frameDecoder = new FrameDecoder(frame, MockRouter);
	        	frameDecoder.DecodeFrame();
			}
			var decoder = new Msg2Decoder(frames);
			GD92Message message = decoder.DecodeMessage();
			var mobiliseMessage = message as GD92Msg2 ?? new GD92Msg2();
			return mobiliseMessage.ToXml();
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92Lib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    /// <summary>
    /// These tests depend on settings in the Registry
    /// There is an export containing suitable settings in GD92Lib\TestInput\SystecaRegistryAux.txt
    /// </summary>
    [TestFixture]
    public class RouterTests
    {
        private static Router TestRouter;
        private static UserAgent TestUserAgent;
        
        /// <summary>
        /// Read configuration from the Registry. Tests verify that it is read correctly.
        /// </summary>
        [TestFixtureSetUp]
        public void Init()
        {
            TestRouter = Router.Instance;
            TestUserAgent = UserAgent.Instance;
            TestRouter.ReadMtas();
            TestRouter.ReadConnections();
            TestRouter.ReadNodes();
            TestUserAgent.ReadConfiguration();
        }

        /// <summary>
        /// Remove bearers and nodes from the static Router.
        /// That this has been done can be tested by running the tests again in the same runner.
        /// If any node remains there is an exception when adding it next time round.
        /// </summary>
        [TestFixtureTearDown]
        public void Cleanup()
        {
            TestUserAgent.Deactivate();
            TestRouter.DeactivateAndRemoveAllMtas();
            TestRouter.RemoveAllNodes();
        }

        /// <summary>
        /// Node 702 is valid if the UserAgent configuration shows this is a gateway.
        /// If the GatewayName is made empty in the Registry, this test fails.
        /// </summary>
        [Test]
        public void RouterIsValidAddress702Test()
        {
            var address = new PortAddress(22, 702, 0);
            Assert.IsTrue(TestRouter.IsValidAddress(address));
        }

        /// <summary>
        /// Node 199 is not valid as a destination (not in NodeData and lower than limit of 200
        /// for creating automatically).
        /// </summary>
        [Test]
        public void RouterIsValidAddress199Test()
        {
            var address = new PortAddress(22, 199, 0);
            Assert.IsFalse(TestRouter.IsValidAddress(address));
        }
        
        /// <summary>
        /// Node 100 is valid if the UserAgent configuration includes it (as expected).
        /// </summary>
        [Test]
        public void RouterIsValidAddress100Test()
        {
            var address = new PortAddress(22, 100, 0);
            Assert.IsTrue(TestRouter.IsValidAddress(address));
        }
        
        /// <summary>
        /// This test passes if Unknown is not in the Registry NodeData
        /// </summary>
        [Test]
        public void RouterGetNodeByNameDefaultForUnknownTest()
        {
            Node node = TestRouter.GetNodeByNameOrDefault("Unknown");
            Assert.IsEmpty(node.Name);
            Assert.AreEqual(0, node.Address.Brigade);
            Assert.AreEqual(0, node.Address.Node);
        }

        /// <summary>
        /// This test passes only if STORM is configured in the Registry NodeData
        /// </summary>
        [Test]
        public void RouterGetNodeByNameSTORMTest()
        {
            const string nodeName = "STORM";
            Node node = TestRouter.GetNodeByNameOrDefault(nodeName);
            Assert.AreEqual(nodeName, node.Name);
        }
        
        /// <summary>
        /// This test passes if node 999 is not in the Registry NodeData
        /// It checks the contents of the node created by default
        /// </summary>
        [Test]
        public void RouterGetNodeByAddressDefaultForUnknownTest()
        {
            Node node = TestRouter.GetNodeByAddressOrDefault(new NodeAddress(22, 999));
            Assert.IsEmpty(node.Name);
            Assert.AreEqual(new PortAddress(), node.Address);
            Assert.AreEqual(NodeType.Rsc, node.GD92Type);
        }

        /// <summary>
        /// This test passes only if node 100 is configured for STORM in the Registry NodeData.
        /// </summary>
        [Test]
        public void RouterGetNodeByAddress100Test()
        {
            const string nodeName = "STORM";
            Node node = TestRouter.GetNodeByAddressOrDefault(new NodeAddress(22, 100));
            Assert.AreEqual(nodeName, node.Name);
        }
        
        /// <summary>
        /// This requires privilege to set up IP sockets and firewall configuration for listener.
        /// Just verifies there is no exception when the code runs.
        /// </summary>
        [Test]
        public void RouterActivateMtaListenerTest()
        {
            TestRouter.ActivateMta("SGW1");
        }
        
        /// <summary>
        /// This requires privilege to set up IP sockets
        /// Just verifies there is no exception when the code runs.
        /// </summary>
        [Test]
        public void RouterActivateMtaTest()
        {
            TestRouter.ActivateMta("SLAN");
        }
        
        /// <summary>
        /// This requires privilege to set up IP sockets and firewall configuration for listener
        /// Just verifies there is no exception when the code runs.
        /// </summary>
        [Test]
        public void RouterActivateMtasTest()
        {
            TestRouter.ActivateMtas();
        }
        
        /// <summary>
        /// This test passes if this node name is the same as the gateway name 
        /// (i.e. User agent is configured as a gateway).
        /// If the GatewayName is made empty in the Registry, this test fails.
        /// Mystified as to why Routes is empty instead of referring to SGW1.
        /// </summary>
        [Test]
        public void RouterDivertToGatewayTest()
        {
            var frame = new Frame();
            Assert.IsEmpty(frame.Routes, "as this is the default");
            frame.Routes = null;
            Assert.IsNull(frame.Routes, "done to show there is a change");
            Router.DivertToGatewayIfAny(frame);
            Assert.IsNotNull(frame.Routes, "as this has been copied from the gateway node");
            string thisNodeName = UserAgent.Instance.NodeName;
            Assert.AreEqual("SGW1", thisNodeName, "as this should be what is in the Registry");
            Node gatewayNode = TestUserAgent.GetGatewayNode();
            Assert.AreEqual(thisNodeName, gatewayNode.Name, "as expected from the Registry");
            Assert.IsEmpty(gatewayNode.Routes);
            Assert.IsTrue(object.ReferenceEquals(gatewayNode.Routes, frame.Routes), "expected same!");
        }
    }
}

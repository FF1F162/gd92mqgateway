﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved


using System;
using NUnit.Framework;
using Thales.KentFire.GD92;

namespace GD92Lib.NUnitTests
{
	[TestFixture]
	public class MockGD92SequenceProviderTest
	{
		[Test]
		public void GetSingleSequenceNumberLast1Test()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			sequenceProvider.LastSequenceNumber = 1;
			int actual = sequenceProvider.GetSingleSequenceNumber();
			Assert.AreEqual(2, actual);
		}
		[Test]
		public void GetSingleSequenceNumberWrapAroundTest()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			sequenceProvider.LastSequenceNumber = FrameConverter.MaxFrameSequenceNumber;
			int actual = sequenceProvider.GetSingleSequenceNumber();
			Assert.AreEqual(0, actual);
		}
		/// <summary>
		/// Feeding a negative number gives an invalid sequence number
		/// but this is not possible from the Registry as the following two tests demonstrate
		/// </summary>
		[Test]
		public void GetSingleSequenceNumberNegativeTest()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			sequenceProvider.LastSequenceNumber = -20;
			int actual = sequenceProvider.GetSingleSequenceNumber();
			Assert.AreEqual(1, actual);
		}
		[Test]
		[ExpectedException("System.InvalidCastException")]
		public void GetSingleSequenceNumberDWordMaxTest()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			object value = 4294967295; // max DWORD as if read from the Registry
			sequenceProvider.LastSequenceNumber = (int)value;
			int actual = sequenceProvider.GetSingleSequenceNumber();
		}
		[Test]
		public void GetSingleSequenceNumberIntMaxTest()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			sequenceProvider.LastSequenceNumber = 2147483647;
			int actual = sequenceProvider.GetSingleSequenceNumber();
			Assert.AreEqual(1, actual);
		}
		/// <summary>
		/// The mock now returns 1 for any number greater than the frame max 
		/// or less than 0.
		/// TODO prove that with a unit test of the real provider
		/// </summary>
		[Test]
		public void GetSingleSequenceNumberBiggerThanFrameMaxWrapsAroundTest()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			sequenceProvider.LastSequenceNumber = FrameConverter.MaxFrameSequenceNumber + 20;
			int actual = sequenceProvider.GetSingleSequenceNumber();
			Assert.AreEqual(1, actual);
		}
		[Test]
		public void FillArrayWithSequenceNumbersTest()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			sequenceProvider.LastSequenceNumber = 20;
			var sequenceNumbers = new int[4];
			sequenceProvider.FillArrayWithSequenceNumbers(sequenceNumbers);
			Assert.AreEqual(24, sequenceProvider.LastSequenceNumber);
			var expected = new int[] { 21, 22, 23, 24 };
			Assert.AreEqual(expected, sequenceNumbers);
		}
		[Test]
		public void FillArrayWithSequenceNumbersWrapAroundTest()
		{
			var sequenceProvider = new MockGD92SequenceProvider();
			sequenceProvider.LastSequenceNumber = FrameConverter.MaxFrameSequenceNumber - 1;
			var sequenceNumbers = new int[4];
			sequenceProvider.FillArrayWithSequenceNumbers(sequenceNumbers);
			Assert.AreEqual(2, sequenceProvider.LastSequenceNumber);
			var expected = new int[] { 32767, 0, 1, 2 };
			Assert.AreEqual(expected, sequenceNumbers);
		}
	}
}

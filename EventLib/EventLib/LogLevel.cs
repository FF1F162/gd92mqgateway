﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;

    /// <summary>
    /// Type of log i.e. level, used to show the importance of a log (and to filter output)
    /// 0 is most important.
    /// Alert (default 0) is for messages at startup that always need to go in the log.
    /// Emergency is for exceptions that (mostly) stop the gateway.
    /// Warning is for configuration and data problems.
    /// Audit monitors traffic through the gateway.
    /// Filter logs are subject to configuration - traffic with a particular node etc.
    /// </summary>
    public enum LogLevel
    {
        Alert,
        Emergency,
        Warning,
        Audit,
        Filter,
        Notice,
        Info,
        Debug
    }
}

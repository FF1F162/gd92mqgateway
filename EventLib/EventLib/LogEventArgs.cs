﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;

    /// <summary>
    /// Class to hold log type (i.e. level) and log message in an event.
    /// </summary>
    public class LogEventArgs : EventArgs
    {
        private readonly LogLevel logType;
        private readonly string logMessage;

        /// <summary>
        /// Initialises a new instance of the <see cref="LogEventArgs"></see> class.
        /// </summary>
        /// <param name="logType">Level of the log.</param>
        /// <param name="logMessage">Contents of the log.</param>
        public LogEventArgs(LogLevel logType, string logMessage)
        {
            this.logType = logType;
            this.logMessage = logMessage;
        }
        
        /// <summary>
        /// Message to be written or displayed as part of the log.
        /// </summary>
        /// <value>Contents of log.</value>
        public string Message
        {
            get { return this.logMessage; }
        }
        
        /// <summary>
        /// Type of log i.e. level or importance.
        /// </summary>
        /// <value>Level of log.</value>
        public LogLevel TypeOfLog
        {
            get { return this.logType; }
        }
    }
}

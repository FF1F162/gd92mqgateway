﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;
    using System.Diagnostics;

    /// <summary>
    /// Holds configuation needed for writing to a particular event log.
    /// </summary>
    internal class ExternalEventLogger : IExternalEventLogger
    {
        private string source;

        /// <summary>
        /// Initialises a new instance of the <see cref="ExternalEventLogger"></see> class.
        /// When using the process name as source.
        /// </summary>
        internal ExternalEventLogger()
        {
            this.source = Process.GetCurrentProcess().ProcessName;
            if (!EventLog.SourceExists(this.source))
            {
                EventLog.CreateEventSource(this.source, "Application");
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ExternalEventLogger"></see> class.
        /// When using a pre-existing source.
        /// </summary>
        /// <param name="serviceName">Name of pre-existing source.</param>
        internal ExternalEventLogger(string serviceName)
        {
            Debug.Assert(EventLog.SourceExists(serviceName), "Windows service is expected to be registered automatically as an event log source");
            this.source = serviceName;
        }

        /// <summary>
        /// Write a warning in the Windows event log referred to by the source set up previously.
        /// </summary>
        /// <param name="eventText">Warning text.</param>
        public void WriteEvent(string eventText)
        {
            Debug.Assert(this.source != null, "Source must be set up previously");
            EventLog.WriteEntry(this.source, eventText, EventLogEntryType.Warning);
        }
    }
}

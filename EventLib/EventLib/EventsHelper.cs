﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

//Thread-safe event publishing
//from Lowy, J. Programming .NET Components 2nd Edition (O'Reilly, 2005)
// © 2005 IDesign Inc. All rights reserved 
//Questions? Comments? go to 
//http://www.idesign.net
//The code below does not include 'GenericEventHandler' and Asynchronous events
//as events from the GD92Component are published one by one on a dedicated thread

using System;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Thales.KentFire.EventLib
{
    public static class EventsHelper
    {
        static void InvokeDelegate(Delegate del, object[] args)
        {
            ISynchronizeInvoke synchronizer = del.Target as ISynchronizeInvoke;
            if (synchronizer != null)//Requires thread affinity
            {
                if (synchronizer.InvokeRequired)
                {
                    synchronizer.Invoke(del, args);
                    return;
                }
            }
            //Not requiring thread afinity or invoke is not required
            del.DynamicInvoke(args);
        }
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void UnsafeFire(Delegate del, params object[] args)
        {
            if (args.Length > 7)
            {
                Trace.TraceWarning("Too many parameters. Consider a structure to enable the use of the type-safe versions");
            }
            if (del == null)
            {
                return;
            }
            Delegate[] delegates = del.GetInvocationList();

            foreach (Delegate sink in delegates)
            {
                try
                {
                    InvokeDelegate(sink, args);
                }
                catch
                {
                    throw;
                }
            }
        }
        public static void Fire(EventHandler del, object sender, EventArgs e)
        {
            UnsafeFire(del, sender, e);
        }
        public static void Fire<T>(EventHandler<T> del, object sender, T t) where T : EventArgs
        {
            UnsafeFire(del, sender, t);
        }
    }
}

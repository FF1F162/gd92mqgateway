﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;

    /// <summary>
    /// Log an event (probably in the Windows event log).
    /// </summary>
    public interface IExternalEventLogger
    {
        void WriteEvent(string eventText);
    }
}

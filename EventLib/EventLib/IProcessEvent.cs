﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;

    /// <summary>
    /// Provides a method to process (i.e. publish) an event from the event queue.
    /// </summary>
    public interface IProcessEvent
    {
        void ProcessEvent(EventQueueItem item);
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;
    
    /// <summary>
    /// Enumenration used to label items in the event queue to show the type of event,
    /// which corresponds to a specific sub-class of EventArgs.
    /// </summary>
    public enum EventType
    {
        Null,
        Log,
        MessageReceived,
        ReplyReceived,
        NodeConnected,
        NodeConnectionLost
    }
}

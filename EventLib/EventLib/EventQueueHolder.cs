﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This is a singleton class that holds the queue of events.
    /// </summary>
    public static class EventQueueHolder
    {
        private static readonly object Lock = new object();
        private static readonly Queue<EventQueueItem> EventQueue = new Queue<EventQueueItem>();

        /// <summary>
        /// Lock the queue and add the event item to the end.
        /// </summary>
        /// <param name="item">Event item to add to the queue.</param>
        public static void QueueEvent(EventQueueItem item)
        {
            lock (Lock)
            {
                EventQueue.Enqueue(item);
            }
        }
        
        /// <summary>
        /// Lock the queue and remove the next event to be published.
        /// </summary>
        /// <returns>Event to publish or event of type Null.</returns>
        internal static EventQueueItem DequeueEvent()
        {
            EventQueueItem item = null;
            lock (Lock)
            {
                if (EventQueue.Count > 0)
                {
                    item = EventQueue.Dequeue();
                }
            }
            
            return item ?? new EventQueueItem(EventType.Null, null);
        }
        
        /// <summary>
        /// Lock and read the number of items queued.
        /// </summary>
        /// <returns>number of queued events.</returns>
        internal static int Count
        {
            get
            {
                int count;
                lock (Lock)
                {
                    count = EventQueue.Count;
                }
                
                return count;
            }
        }
    }
}

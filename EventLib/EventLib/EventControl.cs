﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;
    using System.Threading;

    /// <summary>
    /// This is a singleton class that controls events with the aim of firing them in the order
    /// in which event items are added to a single queue.
    /// It includes static methods for adding Log event items to the queue. GD92Component adds
    /// other sorts of event item for notifying the host application about message received,
    /// node connected etc.
    /// It includes a timer for processing the event queue and fires log events. It fires other
    /// sorts of events by calling the method in the IProcessEvent interface (if present).
    /// </summary>
    public sealed class EventControl : IDisposable
    {
        public const int EventQueueTimeSpan = 1000; // milliseconds changed from 500 to 1000 6 Nov 2014
        private const int MaxLogNumber = 999999;

        private static readonly EventControl Self = new EventControl();

        private static int logNumber;
        
        private IProcessEvent externalPublisher;

        private Timer eventQueueTimer;

        /// <summary>
        /// Prevents a default instance of the <see cref="EventControl"/> class from being created.
        /// </summary>
        private EventControl()
        {
        }

        /// <summary>
        /// Property that allows access to the single instance of this class.
        /// </summary>
        /// <value>Single instance of class.</value>
        public static EventControl Instance
        {
            get { return Self; }
        }

        #region Static methods

        /// <summary>
        /// Queue log event at Debug level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Debug(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Debug, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }
        
        /// <summary>
        /// Queue log event at Info level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Info(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Info, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }
        
        /// <summary>
        /// Queue log event at Notice level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Note(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Notice, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }
        
        /// <summary>
        /// Queue log event at Warning level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Warn(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Warning, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }

        /// <summary>
        /// Queue log event at Filter level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Filter(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Filter, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }

        /// <summary>
        /// Queue log event at Audit level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Audit(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Audit, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }
        
        /// <summary>
        /// Queue log event at Emergency level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Emergency(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Emergency, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }

        /// <summary>
        /// Queue log event at Alert level.
        /// </summary>
        /// <param name="logName">Logging name of object generating the log.</param>
        /// <param name="displayLine">Contents of the log.</param>
        public static void Alert(string logName, string displayLine)
        {
            QueueLogEvent(LogLevel.Alert, string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}", logName, displayLine));
        }

        /// <summary>
        /// Queue item for a log event if the type (i.e. level) of the log is as important
        /// or more important than the current logging level (0 most important, 7 least).
        /// Give the log a serial number so missing or out-of-order logs can be identified.
        /// Record the time of the log (the date is in the log file name) and the thread identity.
        /// </summary>
        /// <param name="logType">Logging name of object generating the log.</param>
        /// <param name="eventMessage">Contents of the log.</param>
        public static void QueueLogEvent(LogLevel logType, string eventMessage)
        {
            if (logType <= LogEventLevel)
            {
                string argMessage = string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "{0}-{1}-{2}-{3}-{4}.",
                    NextLogNumber(),
                    DateTime.Now.ToString("HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                    Thread.CurrentThread.ManagedThreadId.ToString("D3", System.Globalization.CultureInfo.InvariantCulture),
                    logType.ToString(), 
                    eventMessage);
                var args = new LogEventArgs(logType, argMessage);
                var item = new EventQueueItem(EventType.Log, args);
                EventQueueHolder.QueueEvent(item);
            }
        }
        
        #endregion

        public event EventHandler<LogEventArgs> Log;

        /// <summary>
        /// Level for filtering logs - discard logs if the level is numerically greater.
        /// </summary>
        /// <value>Logging level.</value>
        public static LogLevel LogEventLevel { get; set; }

        /// <summary>
        /// Set up the timer to process queued events.
        /// </summary>
        public void Activate()
        {
            if (this.eventQueueTimer == null)
            {
                this.eventQueueTimer = new Timer(this.ProcessEventQueue, null, 0, EventQueueTimeSpan);
            }
        }
        
        /// <summary>
        /// Set the reference to an external event publisher (e.g. GD92Component).
        /// Set up the timer to process queued events.
        /// </summary>
        /// <param name="eventPublisher">External event publisher.</param>
        public void Activate(IProcessEvent eventPublisher)
        {
            this.externalPublisher = eventPublisher;

            this.Activate();
        }
        
        /// <summary>
        /// Stop (dispose) the timer.
        /// </summary>
        public void Deactivate()
        {
            this.Dispose();
            
            this.externalPublisher = null;
        }
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        public void Dispose()
        {
            if (this.eventQueueTimer != null)
            {
                this.eventQueueTimer.Dispose();
                this.eventQueueTimer = null;
            }
        }

        /// <summary>
        /// Provide a sequence number for each log added to the queue
        /// so it is possible to tell if any are missing or out of order when published.
        /// </summary>
        /// <returns>Sequence number with leading zeros.</returns>
        private static string NextLogNumber()
        {
            logNumber++;
            if (logNumber > MaxLogNumber) 
            { 
                logNumber = 0; 
            }
            
            return logNumber.ToString("D6", System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Called by the timer (so more than one may be running concurrently).
        /// Continue to remove items until the queue is empty.
        /// Fire log from here. Also call the method to fire the event from elsewhere.
        /// </summary>
        /// <param name="state">State object as provided by timer.</param>
        private void ProcessEventQueue(object state)
        {
            while (EventQueueHolder.Count > 0)
            {
                EventQueueItem item = EventQueueHolder.DequeueEvent();
                switch (item.EventType)
                {
                    case EventType.Log:
                        this.FireLog(item.EventArgs as LogEventArgs);
                        break;
                    default:
                        break;
                }
                
                if (this.externalPublisher != null)
                {
                    this.externalPublisher.ProcessEvent(item);
                }
            }
        }
        
        /// <summary>
        /// Check event is not null then fire it.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        private void FireLog(LogEventArgs e)
        {
            if (this.Log != null)
            {
                this.Log(this, e);
            }
        }
    }
}

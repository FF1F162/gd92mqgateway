﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.EventLib
{
    using System;
    
    /// <summary>
    /// This class represents an event in an object that can be held in a queue.
    /// It contains the type and arguments for an event.
    /// </summary>
    public class EventQueueItem
    {
        private readonly EventType eventType;
        private readonly EventArgs eventArgs;

        /// <summary>
        /// Initialises a new instance of the <see cref="EventQueueItem"/> class.
        /// </summary>
        /// <param name="eventType">Type of event.</param>
        /// <param name="eventArgs">Arguments for the event.</param>
        public EventQueueItem(EventType eventType, EventArgs eventArgs)
        {
            this.eventType = eventType;
            this.eventArgs = eventArgs;
        }

        /// <summary>
        /// Event type.
        /// </summary>
        /// <value>Type of event.</value>
        public EventType EventType
        {
            get
            {
                return this.eventType;
            }
        }
        
        /// <summary>
        /// Arguments for the event (derived from EventArgs, depending on event type).
        /// </summary>
        /// <value>Arguments for the event.</value>
        public EventArgs EventArgs
        {
            get
            {
                return this.eventArgs;
            }
        }
    }
}

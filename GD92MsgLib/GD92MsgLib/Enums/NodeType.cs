﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Used in node configuraton to distinguish message handling requirements and encoding or decoding
    /// according to purpose (mobilising or mobilised) and differences in implementation of GD-92.
    /// </summary>
    public enum NodeType
    {
        None,
        Storm,
        Mobs,
        Gateway,
        Rsc
    }
}

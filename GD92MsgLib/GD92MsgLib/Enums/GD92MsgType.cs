﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Subset of Table B-1 from GD-92 B.1.7 page 87
    /// listing message types implemented by the RSC.
    /// RawFrame is used for test purposes.
    /// </summary>
    public enum GD92MsgType
    {
        NotDefined              = 0,
        MobiliseCommand         = 1,
        MobiliseMessage         = 2,
        PageOfficer             = 3,
        ResourceStatusRequest   = 5,
        MobiliseMessage6        = 6,
        ActivatePeripheral      = 7,
        PeripheralStatusRequest = 9,
        ResourceStatus          = 20,
        DutyStaffingUpdate      = 21,
        LogUpdate               = 22,
        Stop                    = 23,
        Makeup                  = 24,
        InterruptRequest        = 25,
        Text                    = 27,
        PeripheralStatus        = 28,
        AlertCrew               = 40,
        AlertStatus             = 42,
        Ack                     = 50,
        Nak                     = 51,
        PrinterStatus           = 65,
        RawFrame                = 255
    }
}

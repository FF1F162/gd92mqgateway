﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// GD-92 A.4.3 page 79-80
    /// Output peripherals are in reverse order numerically
    /// as bit[0] in the array is the leftmost (most significant) bit.
    /// </summary>
    [Flags]
    public enum OpPeripherals
    {
        Sounders          = 0x8000,
        Lights            = 0x4000,
        Spare2            = 0x2000,
        Spare3            = 0x1000,
        Spare4            = 0x0800,
        Doors             = 0x0400,
        StandbySounder    = 0x0200,
        Appliance9        = 0x0100,
        Appliance1        = 0x0080,
        Appliance2        = 0x0040,
        Appliance3        = 0x0020,
        Appliance4        = 0x0010,
        Appliance5        = 0x0008,
        Appliance6        = 0x0004,
        Appliance7        = 0x0002,
        Appliance8        = 0x0001,
        None              = 0x0000
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// GD-92 Appendix A.4.3 page 80.
    /// </summary>
    public enum GD92PagerType
    {
        None,
        Alphanumeric = 65, // ASCII A
        Numeric = 78, // ASCII N
        TonesOnly = 84  // ASCII T
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// GD-92 A.4.3 page 77-78
    /// Input peripherals are in reverse order numerically
    /// as bit[0] in the array is the leftmost (most significant) bit.
    /// </summary>
    [Flags]
    public enum IpPeripherals
    {
        ManualAcknowledgementPressed          = 0x8000,
        PowerFailedBatteriesOn                = 0x4000,
        PowerFailedStandbyGeneratorOn         = 0x2000,
        RepeatLastMessagePressed              = 0x1000,
        BatteriesLow                          = 0x0800,
        PaperLow                              = 0x0400,
        SpareIp1                              = 0x0200,
        SpareIp2                              = 0x0100,
        RunningCallTelephoneDoorOpened        = 0x0080,
        PaperOut                              = 0x0040,
        SpareIp3                              = 0x0020,
        SpareIp4                              = 0x0010,
        SpareIp5                              = 0x0008,
        SpareIp6                              = 0x0004,
        SpareIp7                              = 0x0002,
        SpareIp8                              = 0x0001,
        None                                  = 0x0000
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Used (by MOBS and RSC) to show purpose of Msg20.
    /// Set to 1 for unrestricted update for use by MDT
    /// Set to 3 for changes restricted to a list of valid status codes (for example BS,
    /// OF, DR) for use at IP Station Ends.
    /// </summary>
    public enum AvlType
    {
        None,
        UnrestrictedUpdate = 1,
        FromMobs           = 2,
        RestrictedUpdate   = 3,
        AvlOnly            = 4,
        NodeUpdate         = 5
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// GD-92 Appendix A.4.3 page 81.
    /// </summary>
    public enum GD92PrinterStatus
    {
        Offline,
        PaperOut,
        Online
    }
}

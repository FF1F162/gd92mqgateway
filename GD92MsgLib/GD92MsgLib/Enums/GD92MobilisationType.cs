﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// GD-92 A.4.3 page 79.
    /// </summary>
    public enum GD92MobilisationType
    {
        PreAlert,                     // not used by MOBS
        Incident,
        NonIncident,                  // not used by MOBS
        BatchMobiliseAddress,         // not used by MOBS
        Standby,
        Demobilise,                   // not used by MOBS
        Test,                         // not used by MOBS
        Makeup,                       // was Assist - Message 6 special
        HomeStation,                  // KFRS special
        Inform,                       // KFRS special
        Assign,                       // KFRS special
        FurtherInformation = 96,      // KFRS special
        ChangeOfNature = 97,          // KFRS special
        ChangeOfAddress = 98,         // KFRS special
        UpdateRequest = 99,           // KFRS special
        NotDefined = 255              // max allowed
    }
}

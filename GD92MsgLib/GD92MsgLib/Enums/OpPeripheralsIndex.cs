﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Index into the BitArray holding Output Peripherals settings GD-92 A.4.3 page 79
    /// bit 0 is the leftmost (most significant).
    /// </summary>
    internal enum OpPeripheralsIndex
    {
        Sounders,
        Lights,
        Spare2,
        Spare3,
        Spare4,
        Doors,
        StandbySounder,
        Appliance9,
        Appliance1,
        Appliance2,
        Appliance3,
        Appliance4,
        Appliance5,
        Appliance6,
        Appliance7,
        Appliance8        
    }
}

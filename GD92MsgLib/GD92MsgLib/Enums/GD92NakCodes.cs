﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// GD-92 Appendix A.4.3 page 81.
    /// </summary>
    public enum GD92ReasonCodeSet
    {
        Printer,
        General,
        Alerter,
        Peripheral,
        Parameter
    } 

    /// <summary>
    /// GD-92 Appendix D.4.
    /// </summary>
    public enum GD92PrinterReasonCode
    {
        None, // Not in GD-92
        NoResponseFromPrinter,
        Offline,
        NoPaper,
        LowPaper,
        NoPower,
        NoConnection
    } 

    /// <summary>
    /// GD-92 Appendix D.2 .
    /// </summary>
    public enum GD92GeneralReasonCode
    {
        None, // Not in GD-92
        NoBearer,
        InvalidParameter,
        InvalidMessage,
        AvlDataNotFound,
        StatusFail,
        TextType,
        InvalidCallsign,
        NotAccepted,
        WaitAcknowledgement,
        TestFail,
        InvalidTest,
        InvalidProtocol,
        CheckError,
        Abort,
        NoPort,
        DataFail,
        ProformaFail,
        TransmissionTimedOut // Not in GD-92
    } 

    /// <summary>
    /// GD-92 Appendix D.1.
    /// </summary>
    public enum GD92AlerterReasonCode
    {
        None, // Not in GD-92
        TransmitterAFailed,
        TransmitterBFailed,
        TransmissionFailed,
        GroupAFailed,
        GroupBFailed,
        GroupCFailed,
        EncodingUnitFailed,
        PowerFailed,
        OtherFailure,
        SystemBusy,
        NoFac,
        OtherProblem,
        WaitAcknowledgement,
        NotConnected
    } 

    /// <summary>
    /// GD-92 Appendix D.3.
    /// </summary>
    public enum GD92PeripheralReasonCode
    {
        None, // Not in GD-92
        NoDoor,
        NoSounder,
        NoLights,
        NoIndicator1,
        NoIndicator2,
        NoIndicator3,
        NoIndicator4,
        NoIndicator5,
        NoIndicator6,
        NoIndicator7,
        NoIndicator8,
        DoorFailed,
        SounderFailed,
        LightsFailed,
        Indicator1Failed,
        Indicator2Failed,
        Indicator3Failed,
        Indicator4Failed,
        Indicator5Failed,
        Indicator6Failed,
        Indicator7Failed,
        Indicator8Failed,
        OperationFailed,
        NoResponse
    }

    /// <summary>
    /// GD-92 Appendix D.5.
    /// </summary>
    public enum GD92ParameterReasonCode
    {
        None, // Not in GD-92
        NoModifyAccess,
        InvalidSyntax,
        InvalidValue,
        InvalidPassword,
        InvalidTable,
        InvalidParameter,
        InvalidField,
        InvalidEntry,
        NoAccess
    } 
}

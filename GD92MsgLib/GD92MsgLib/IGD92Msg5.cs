﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
namespace Thales.KFRS.GD92
{
    public interface IGD92Msg5 : IGD92Message
    {
        string Callsign { get; set; }
    }
}

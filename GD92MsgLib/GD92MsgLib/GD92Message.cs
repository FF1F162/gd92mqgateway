﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Xml.Serialization;

    /// <summary>
    /// Base class for GD92 messages
    /// Abstract because a message with no body is not valid.
    /// </summary>
    public abstract class GD92Message
    {
        #region Validation constants

        /// <summary>
        /// Constants used to validate message properties
        /// aiming to ensure they fall within GD-92 constraints.
        /// </summary>
        public const int AlertGroupLength = 2;
        public const int AlertStatusLength = 2;
        public const int MaxForByte = 255;
        public const int MaxBrigade = MaxForByte;
        public const int MaxNode = 1023;
        public const int MaxPort = 63;
        public const int MaxAddressLength = 120; // GD-92 120 MOBS 140+
        public const int MaxHouseNumberLength = 10;
        public const int MaxStreetLength = 40;
        public const int MaxSubDistrictLength = 30;
        public const int MaxDistrictLength = 30;
        public const int MaxTownLength = 30;
        public const int MaxCountyLength = 20;

        public const int MaxAddTextLength = 68;
        public const int MaxAlternateLabelLength = 1;
        public const int MaxAssistanceMessage = 90;
        public const int MaxAvlType = MaxForByte;
        public const int MaxAvlDataLength = 40;
        public const int MaxCommentaryLength = 146;
        public const int MaxHowReceivedLength = 50;
        public const int MaxIncidentTypeLength = 47;
        public const int MaxLocationLength = 2;
        public const int MaxMapBookLength = 16;
        public const int MaxMapRefLength = 16;
        public const int MaxPagerNumberLength = 16;
        public const int MaxPagerTextLength = 200;
        public const int MaxPostcodeLength = 10;
        public const int MaxOperatorInfoLength = 132;
        public const int MaxRemarksLength = 200;
        public const int MaxResourceUpdateError = 99;
        public const int MaxRiders = 9;
        public const int MaxTelNumberLength = 16;
        public const int MaxTravelTime = 86400;
        public const int MaxStatusCode = MaxForByte;
        public const int MaxUpdateLength = MaxForByte;
        public const int MaxWaterInformationLength = 180;
        public const int MinIncidentNumber = 1;
        public const int MinIncidentNumberLength = 1;
        public const int MaxIncidentNumberLength = 10;

        // callsigns are defined in MOBS according to ENH188
        public const int MinCallsignLength = 3;
        public const int MaxCallsignLength = 7;

        // GD-92 probably allows 2^32 - 1 This is what is currently allowed in an int
        public const int MaxIncidentNumber = 2147483647; // 2^31 - 1 (MAX_INT in limits.h)
        public const int MinEorN = 100000;
        public const int MaxEorN = 999999;
        public const int MinEorNTextLength = 1;
        public const int MaxEorNTextLength = 8; // allow for possible decimeter precision 123456.7

        // this is a bit less than the size of the buffer allowed by the RSC
        // ECN3016 increased from 8000
        public const int MaxTextLength = 65536;

        #endregion

        public const string DefaultNotSet = "NotSet";

        private int serialNumber;
        private GD92MsgType msgType;
        private string destination;
        private bool manualAck;
        private DateTime sentAt;
        private DateTime receivedAt;
        private string from;
        private GD92Address fromAddress;
        private GD92Address destAddress;
        
        private bool doNotChange;

        private GD92Message originalMessage;
        private GD92Message gatewayOriginal;

        #region Public properties

        /// <summary>
        /// Local serial number for the message allocated on send or receive by the UA
        /// This is NOT the same as the GD-92 seq_no, which refers to an individual frame.
        /// </summary>
        /// <value>Local serial number.</value>
        [XmlElement]
        public int SerialNumber
        {
            get
            {
                return this.serialNumber;
            }

            set
            {
                this.CheckReadOnly("SerialNumber");
                this.serialNumber = value;
            }
        }

        /// <summary>
        /// GD92 message type number.
        /// </summary>
        /// <value>Message type enumeration.</value>
        [XmlElement]
        public GD92MsgType MsgType
        {
            get
            {
                return this.msgType;
            }

            set
            {
                this.CheckReadOnly("MsgType");
                this.msgType = value;
            }
        }

        /// <summary>
        /// Information in GD-92 header that depends on message type.
        /// </summary>
        /// <value>GD92Header object.</value>
        [XmlElement]
        public GD92Header Header { get; set; }

        /// <summary>
        /// Name of the destination: Registry key under NodeData or a name in the form Node456 (with default brigade and port).
        /// </summary>
        /// <value>Destination name.</value>
        [XmlElement]
        public string Destination
        {
            get
            {
                return this.destination;
            }

            set
            {
                this.CheckReadOnly("Destination");
                this.destination = value;
            }
        }

        /// <summary>
        /// Time of sending message (expect 0 for received message).
        /// </summary>
        /// <value>Time when message was sent.</value>
        [XmlElement]
        public DateTime SentAt
        {
            get
            {
                return this.sentAt;
            }

            set
            {
                this.CheckReadOnly("SentAt");
                this.sentAt = value;
            }
        }

        /// <summary>
        /// Time message was received (expect 0 for message sent by the host application).
        /// </summary>
        /// <value>Time when message was received.</value>
        [XmlElement]
        public DateTime ReceivedAt
        {
            get
            {
                return this.receivedAt;
            }

            set
            {
                this.CheckReadOnly("ReceivedAt");
                this.receivedAt = value;
            }
        }

        /// <summary>
        /// Name of the source: Registry key under NodeData or a name in the form Node456 (with default brigade and port).
        /// </summary>
        /// <value>Name of source.</value>
        [XmlElement]
        public string From
        {
            get
            {
                return this.from;
            }

            set
            {
                this.CheckReadOnly("From");
                this.from = value;
            }
        }

        /// <summary>
        /// Some GD92 message types allow for manual acknowledgement.
        /// </summary>
        /// <value>True if manual acknowledgement is required.</value>
        [XmlElement]
        public bool ManualAck
        {
            get
            {
                return this.manualAck;
            }

            set
            {
                this.CheckReadOnly("ManualAck");
                this.manualAck = value;
            }
        }

        /// <summary>
        /// This is a feature designed to help development of the host application,
        /// to prevent the HAP making (inadvertent) changes to received messages
        /// It does not prevent changes to lists etc.
        /// </summary>
        /// <value>True if message is supposed to be read-only.</value>
        [XmlIgnore]
        public bool DoNotChange
        {
            get { return this.doNotChange; }
        }

        /// <summary>
        /// GD-92 Address of the source.
        /// </summary>
        /// <value>Source address.</value>
        [XmlElement]
        public GD92Address FromAddress
        {
            get
            {
                return this.fromAddress;
            }

            set
            {
                this.CheckReadOnly("FromAddress");
                this.fromAddress = value;
            }
        }

        /// <summary>
        /// GD-92 Address of the destination.
        /// </summary>
        /// <value>Destination address.</value>
        [XmlElement]
        public GD92Address DestAddress
        {
            get
            {
                return this.destAddress;
            }

            set
            {
                this.CheckReadOnly("DestAddress");
                this.destAddress = value;
            }
        }

        /// <summary>
        /// Type of node affects message decode and handling.
        /// </summary>
        /// <value>Node type enumeration.</value>
        [XmlElement]
        public NodeType FromNodeType { get; set; }

        /// <summary>
        /// Type of node affects message encode and handling.
        /// </summary>
        /// <value>Node type enumeration.</value>
        [XmlElement]
        public NodeType DestNodeType { get; set; }

        /// <summary>
        /// This property is used to expose the ack_req setting in a received message
        /// This is needed because acknowledgement of some messages (Msg20) is optional
        /// and the gateway must respect the wishes of the sender (Storm).
        /// </summary>
        /// <value>True if ack_req is set.</value>
        [XmlIgnore]
        public bool AckRequired { get; set; }

        /// <summary>
        /// This property is used to overrule the normal requirement for acknowledgement
        /// of an unsolicited message - affects ack_req field in frame sent.
        /// </summary>
        /// <value>True if ack_req is not to be set.</value>
        [XmlIgnore]
        public bool SendWithNoAckRequired { get; set; }

        /// <summary>
        /// This property is used to overrule normal behaviour, which is to add an unsolicited
        /// message to the transmitted queue pending acknowledgement or timeout (regardless
        /// of the AckRequired setting).
        /// </summary>
        /// <value>True if message should not be added to the transmitted queue pending acknowledgement.</value>
        [XmlIgnore]
        public bool SendWithNoReplyExpected { get; set; }

        /// <summary>
        /// Reference to the message that this message is a reply to.
        /// Relevant to certain types only: Ack, Nak, Resource Status (and maybe others that can be requested)
        /// Must be null if this is not a reply.
        /// </summary>
        /// <value>Reference to message being replied to.</value>
        [XmlIgnore]
        public GD92Message OriginalMessage 
        {
            get
            {
                return this.originalMessage;
            }

            set
            {
                Debug.Assert(this.originalMessage == null, "original message must be set once only");
                this.originalMessage = value;
            }
        }

        /// <summary>
        /// Reference to the message originally received and forwarded by the gateway.
        /// Relevant to certain types only: Ack, Nak, Resource Status (and maybe others that can be requested).
        /// </summary>
        /// <value>Reference to message received by gateway originally.</value>
        [XmlIgnore]
        public GD92Message GatewayOriginal
        {
            get
            {
                return this.gatewayOriginal;
            }

            set
            {
                Debug.Assert(this.gatewayOriginal == null, "gateway original must be set once only");
                this.gatewayOriginal = value;
            }
        }

        /// <summary>
        /// Added for logging purposes to allow the message to be traced easily elsewhere in the network
        /// This is the number of the last frame in the message, which is the number in the final acknowledgement.
        /// </summary>
        /// <value>GD92 sequence number in last frame of message.</value>
        [XmlIgnore]
        public int GD92Sequence { get; set; }

        #endregion

        #region Static Validation Methods

        /// <summary>
        /// Check input is within limits allowed by GD-92.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="brigade">Brigade number.</param>
        /// <returns>True if brigade is valid.</returns>
        public static short ValidateBrigade(string fieldName, int brigade)
        {
            ValidateByRange(fieldName, brigade, MaxBrigade);
            return (short)brigade;
        }

        /// <summary>
        /// Check input is within limits allowed by GD-92.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="node">Node number.</param>
        /// <returns>True if node is valid.</returns>
        public static short ValidateNode(string fieldName, int node)
        {
            ValidateByRange(fieldName, node, MaxNode);
            return (short)node;
        }

        /// <summary>
        /// Check input is within limits allowed by GD-92.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="port">Port number.</param>
        /// <returns>True if port is valid.</returns>
        public static short ValidatePort(string fieldName, int port)
        {
            ValidateByRange(fieldName, port, MaxPort);
            return (short)port;
        }

        /// <summary>
        /// Check the message has a manual_ack field.
        /// </summary>
        /// <param name="msgType">Message type enumeration.</param>
        /// <returns>True if message supports manual acknowledgement.</returns>
        public static bool MessageTypeSupportsManualAck(GD92MsgType msgType)
        {
            return msgType == GD92MsgType.AlertCrew ||
                msgType == GD92MsgType.MobiliseCommand ||
                msgType == GD92MsgType.MobiliseMessage ||
                msgType == GD92MsgType.MobiliseMessage6;
        }

        /// <summary>
        /// Check the number is within the range allowed.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        /// <param name="max">Maximum value.</param>
        public static void ValidateByRange(string fieldName, int inputValue, int max)
        {
            if (inputValue > max)
            {
                throw new ArgumentOutOfRangeException(fieldName, // ParamName
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, " Number {0} is more than {1} characters", inputValue, max));
            }

            if (inputValue < 0)
            {
                throw new ArgumentOutOfRangeException(fieldName, // ParamName
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, " Number {0} is less than zero", inputValue));
            }
        }

        /// <summary>
        /// Check the string is within the length allowed.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        /// <param name="maxLength">Maximum lentgth.</param>
        public static void ValidateByLength(string fieldName, string inputValue, int maxLength)
        {
            if (inputValue.Length > maxLength)
            {
                throw new ArgumentOutOfRangeException(fieldName, // ParamName
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, " Text is more than {0} characters", maxLength));
            }
        }

        /// <summary>
        /// Check the string length is within the bounds specified.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        /// <param name="maxLength">Maximum length.</param>
        /// <param name="minLength">Minimum length.</param>
        public static void ValidateString(string fieldName, string inputValue, int maxLength, int minLength)
        {
            ValidateByLength(fieldName, inputValue, maxLength);
            if (inputValue.Length < minLength)
            {
                throw new ArgumentOutOfRangeException(fieldName,
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, " Text is less than {0} characters", minLength));
            }
        }

        /// <summary>
        /// Convert the input string to a number. Throw exception if it is null or not a number.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        /// <returns>Number from input.</returns>
        public static int ParseNumber(string fieldName, string inputValue)
        {
            int number = 0;
            if (inputValue == null)
            {
                throw new MessageValidationException(fieldName, string.Empty, "the field is null");
            }

            try
            {
                number = int.Parse(inputValue, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (FormatException e)
            {
                throw new MessageValidationException(fieldName, inputValue, e);
            }

            return number;
        }

        /// <summary>
        /// Check that the string is not null (for which int.Parse gives confusing exception)
        /// then check the number.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        public static void ValidateIncidentNumber(string fieldName, string inputValue)
        {
            // protect int.Parse from null (gives confusing exception)
            ValidateString(fieldName, inputValue, MaxIncidentNumberLength, MinIncidentNumberLength);
            ValidateIncidentNumberAsNumber(fieldName, inputValue);
        }

        /// <summary>
        /// Check that the number is not zero and can be held in word32.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        public static void ValidateIncidentNumberAsNumber(string fieldName, string inputValue)
        {
            int incidentNumber = ParseNumber(fieldName, inputValue);
            if (incidentNumber < MinIncidentNumber || incidentNumber > MaxIncidentNumber)
            {
                throw new ArgumentOutOfRangeException(fieldName, // ParamName
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, " IncidentNumber {0} is not valid", inputValue));
            }
        }

        /// <summary>
        /// Check input is within limits allowed by GD-92.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        public static void ValidateEorN(string fieldName, string inputValue)
        {
            // protect int.Parse from null (gives confusing exception)
            ValidateString(fieldName, inputValue, MaxEorNTextLength, MinEorNTextLength); 
            int gridRef = ParseNumber(fieldName, inputValue);
            if (gridRef < MinEorN)
            {
                throw new ArgumentOutOfRangeException(fieldName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    " OS Grid Reference {0} is less than {1}",
                    inputValue, 
                    MinEorN.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }

            if (gridRef > MaxEorN)
            {
                throw new ArgumentOutOfRangeException(fieldName, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture, 
                    " OS Grid Reference {0} is more than {1}",
                    inputValue, 
                    MaxEorN.ToString(System.Globalization.CultureInfo.InvariantCulture)));
            }
        }

        /// <summary>
        /// Check input is within limits allowed by GD-92.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        public static void ValidateMobilisationType(string fieldName, int inputValue)
        {
            if (!Enum.IsDefined(typeof(GD92MobilisationType), inputValue))
            {
                throw new ArgumentOutOfRangeException(fieldName,
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, "Mobilisation type {0} is not defined", inputValue));
            }
        }

        /// <summary>
        /// Check input is within limits allowed by GD-92.
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        /// <param name="inputValue">Input to be validated.</param>
        public static void ValidateCallsign(string fieldName, string inputValue)
        {
            ValidateString(fieldName, inputValue, MaxCallsignLength, MinCallsignLength);
        }

        #endregion

        #region Serialization

        /// <summary>
        /// Serialize  to XML string. Suppress output of namespaces: they serve no useful purpose in this project and
        /// the order of output is not guaranteed (e.g. xsi and xsd are reversed on XP and Windows 7) and this interferes with tests
        /// that compare ouput with what was saved previously.
        /// </summary>
        /// <typeparam name="T">Type being saved.</typeparam>
        /// <param name="data">Object of that type.</param>
        /// <returns>String of XML representing the object.</returns>
        public static string SaveToMemory<T>(T data)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            var s = new XmlSerializer(typeof(T));
            using (var myWriter = new StringWriter(System.Globalization.CultureInfo.InvariantCulture))
            {
                s.Serialize(myWriter, data, ns);
                return myWriter.ToString();
            }
        }

        /// <summary>
        /// Deserialize XML string to object of the type specified.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="xml">String (in memory) containing XML to deserialize.</param>
        /// <returns>Object constructed using data from theXML.</returns>
        public static T ReadFromMemory<T>(string xml)
        {
            var d = new XmlSerializer(typeof(T));
            var s = new StringReader(xml);
            return (T)d.Deserialize(s);
        }

        /// <summary>
        /// Serialize as XML to file using the full path specified. Suppress output of namespaces to avoid interference with tests.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="data">Object to serialize.</param>
        /// <param name="fileName">Full path of file to write.</param>
        public static void SaveToFile<T>(T data, string fileName)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            var s = new XmlSerializer(typeof(T));
            using (TextWriter myWriter = new StreamWriter(fileName))
            {
                s.Serialize(myWriter, data, ns);
            }
        }

        /// <summary>
        /// Read XML string from file and deserialize to object of the type specified.
        /// </summary>
        /// <typeparam name="T">Type of object represented by XML.</typeparam>
        /// <param name="fileName">Full path of the file containing XML.</param>
        /// <returns>Object constructed using data from theXML.</returns>
        public static T ReadFromFile<T>(string fileName)
        {
            var d = new XmlSerializer(typeof(T));
            using (var f = new FileStream(fileName, FileMode.Open))
            {
                return (T)d.Deserialize(f);
            }
        }

        #endregion

        #region Validation

        /// <summary>
        /// TODO This should be checked on messages being sent
        /// In the old version of gateway mode (e.g. in Airwave gateway) this flag is set in all received messages.
        /// This does not happen in the Storm gateway.
        /// </summary>
        public void ValidateManualAck()
        {
            string fieldName = "ManualAck";
            if (this.ManualAck && !MessageTypeSupportsManualAck(this.MsgType))
            {
                throw new ArgumentOutOfRangeException(fieldName,
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, "Message type {0} does not support manual acknowledgement", this.MsgType));
            }
        }

        /// <summary>
        /// Check that source and destination addresses are within GD-92 limits.
        /// </summary>
        public void ValidateAddresses()
        {
            this.FromAddress.Validate();
            this.DestAddress.Validate();
        }

        #endregion

        /// <summary>
        /// The idea of this was to make message data read-only outside the GD92 namespace
        /// It is not effective within collections and classes (e.g. GD92Header).
        /// </summary>
        public void SetReadOnly()
        {
            this.doNotChange = true;
        }

        /// <summary>
        /// Copy required common fields from another instance FOR FORWARDING ACROSS THE GATEWAY
        /// Not copied: serial number and timing data (these are unique to the copy)
        /// The purpose is to make the gateway transparent to either end
        /// as if the message had simply passed through the router.
        /// </summary>
        /// <param name="original">Message being copied - becomes GatewayOriginal in the copy.</param>
        public void CopySourceDestManAckAndOriginalFrom(GD92Message original)
        {
            this.CopySourceDestManAckFrom(original);
            this.GatewayOriginal = original;
        }

        /// <summary>
        /// Copy fields from another instance
        /// Not copied: serial number and timing data (these are unique to the copy).
        /// </summary>
        /// <param name="original">Message being copied.</param>
        public void CopySourceDestManAckFrom(GD92Message original)
        {
            this.From = original.From;
            this.FromAddress = original.FromAddress;
            this.FromNodeType = original.FromNodeType;
            this.Destination = original.Destination;
            this.DestAddress = original.DestAddress;
            this.DestNodeType = original.DestNodeType;
            this.ManualAck = original.ManualAck;
        }

        /// <summary>
        /// Reset fields to the default state for a new message,
        /// leaving message type, Header that depends on message type and content unchanged.
        /// </summary>
        public void ResetCommonFields()
        {
            this.serialNumber = 0;
            this.receivedAt = DateTime.MinValue;
            this.sentAt = DateTime.MinValue;
            this.doNotChange = false;
            this.from = string.Empty;
            this.fromAddress = new GD92Address();
            this.FromNodeType = NodeType.Rsc;
            this.destination = string.Empty;
            this.destAddress = new GD92Address();
            this.DestNodeType = NodeType.Rsc;
            this.manualAck = false;
            this.originalMessage = null;
            this.gatewayOriginal = null;
        }

        /// <summary>
        /// Flag set by the user agent (in GD92Lib.TransmittedFrames.cs) when
        /// all frames of the message have been acknowledged.
        /// </summary>
        [XmlIgnore]
        internal bool ReplyEventRaised { get; set; }

        /// <summary>
        /// Provided for use when DoNotChange is true.
        /// </summary>
        /// <param name="serialNumber">Serial number.</param>
        internal void SetSerialNumber(int serialNumber)
        {
            this.serialNumber = serialNumber;
        }

        /// <summary>
        /// Throw message access exception if the message is read-only (DoNotChange is true).
        /// </summary>
        /// <param name="fieldName">Name of field.</param>
        protected void CheckReadOnly(string fieldName)
        {
            if (this.DoNotChange == true)
            {
                throw new MessageAccessException(fieldName);
            }
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 51 Nak.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg51 : GD92Message, IGD92Message
    {
        private GD92Address originalDestination;
        private int reasonCodeSet;
        private int reasonCode;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg51"/> class.
        /// </summary>
        public GD92Msg51()
        {
            this.MsgType = GD92MsgType.Nak;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg51"/> class.
        /// This constructor associates the Nak with the message it is acknowledging.
        /// </summary>
        /// <param name="acknowledgedMessage">Message being acknowleded.</param>
        public GD92Msg51(GD92Message acknowledgedMessage) : this()
        {
            this.OriginalMessage = acknowledgedMessage;
            if (acknowledgedMessage != null)
            {
                this.OriginalDestination = acknowledgedMessage.DestAddress;
            }
        }

        /// <summary>
        /// Destination of original message.
        /// </summary>
        /// <value>GD92 Address of destination of original message.</value>
        [XmlElement]
        public GD92Address OriginalDestination
        {
            get
            {
                return this.originalDestination;
            }

            set
            {
                this.CheckReadOnly("OriginalDestination");
                this.originalDestination = value;
            }
        }

        /// <summary>
        /// What the Nak applies to (General, Printers, Alerters etc).
        /// </summary>
        /// <value>Reason code set.</value>
        [XmlElement]
        public int ReasonCodeSet
        {
            get
            {
                return this.reasonCodeSet;
            }

            set
            {
                this.CheckReadOnly("ReasonCodeSet");
                this.reasonCodeSet = value;
            }
        }

        /// <summary>
        /// The code for the Nak reason.
        /// </summary>
        /// <value>Reason code.</value>
        [XmlElement]
        public int ReasonCode
        {
            get
            {
                return this.reasonCode;
            }

            set
            {
                this.CheckReadOnly("ReasonCode");
                this.reasonCode = value;
            }
        }
        
        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg51>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg51>(this);
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            this.ValidateReasonCodeSet();
            this.ValidateReasonCode();
        }

        /// <summary>
        /// Check that the reason code set is defined in GD-92.
        /// </summary>
        private void ValidateReasonCodeSet()
        {
            string paramName = "ReasonCodeSet";
            if (!Enum.IsDefined(typeof(GD92ReasonCodeSet), this.ReasonCodeSet))
            {
                throw new ArgumentOutOfRangeException(paramName,
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, "Reason code set {0} is not defined", this.ReasonCodeSet));
            }
        }

        /// <summary>
        /// Check that the reason code is listed in the reason code set.
        /// </summary>
        private void ValidateReasonCode()
        {
            string paramName = "ReasonCode";
            int reasonCode = 0; // reason codes in GD-92 start at 1
            switch ((GD92ReasonCodeSet)this.ReasonCodeSet)
            {
                case GD92ReasonCodeSet.Alerter:
                    if (Enum.IsDefined(typeof(GD92AlerterReasonCode), this.ReasonCode))
                    {
                        reasonCode = this.ReasonCode;
                    }

                    break;
                case GD92ReasonCodeSet.General:
                    if (Enum.IsDefined(typeof(GD92GeneralReasonCode), this.ReasonCode))
                    {
                        reasonCode = this.ReasonCode;
                    }

                    break;
                case GD92ReasonCodeSet.Parameter:
                    if (Enum.IsDefined(typeof(GD92ParameterReasonCode), this.ReasonCode))
                    {
                        reasonCode = this.ReasonCode;
                    }

                    break;
                case GD92ReasonCodeSet.Peripheral:
                    if (Enum.IsDefined(typeof(GD92PeripheralReasonCode), this.ReasonCode))
                    {
                        reasonCode = this.ReasonCode;
                    }

                    break;
                case GD92ReasonCodeSet.Printer:
                    if (Enum.IsDefined(typeof(GD92PrinterReasonCode), this.ReasonCode))
                    {
                        reasonCode = this.ReasonCode;
                    }

                    break;
                default:
                    break;
            }

            if (reasonCode == 0)
            {
                throw new ArgumentOutOfRangeException(
                    paramName,
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, " {0} is not defined in Reason Code Set {1}", this.ReasonCode, this.ReasonCodeSet));
            }
        }
        
        /// <summary>
        /// A string representation of the reason code.
        /// </summary>
        /// <value>Reason code enumeration to string.</value>
        [XmlIgnore]
        public string Reason
        {
            get
            {
                switch ((GD92ReasonCodeSet)this.ReasonCodeSet)
                {
                    case GD92ReasonCodeSet.Alerter:
                        var alerterReason = (GD92AlerterReasonCode)this.ReasonCode;
                        return alerterReason.ToString();
                    case GD92ReasonCodeSet.General:
                        var generalReason = (GD92GeneralReasonCode)this.ReasonCode;
                        return generalReason.ToString();
                    case GD92ReasonCodeSet.Parameter:
                        var parameterReason = (GD92ParameterReasonCode)this.ReasonCode;
                        return parameterReason.ToString();
                    case GD92ReasonCodeSet.Peripheral:
                        var peripheralReason = (GD92PeripheralReasonCode)this.ReasonCode;
                        return peripheralReason.ToString();
                    case GD92ReasonCodeSet.Printer:
                        var printerReason = (GD92PrinterReasonCode)this.ReasonCode;
                        return printerReason.ToString();
                    default:
                        return string.Empty;
                }
            }
        }
    }
}

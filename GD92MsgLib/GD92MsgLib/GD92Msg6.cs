﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.ObjectModel;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 6 Mobilisation Message (Kent, Hampshire).
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg6 : GD92Message, IGD92Message
    {
        private readonly Collection<GD92Msg6Resource> resourcesList;
        private readonly Collection<GD92Msg6Resource> allResourcesList;
        private int mobilisationType = (int)GD92MobilisationType.NotDefined;
        private string assistanceMessage = string.Empty;
        private DateTime incidentTimeAndDate;
        private DateTime mobTimeAndDate;
        private string incidentNumber = string.Empty;
        private string incidentType = string.Empty;
        private string address = string.Empty;
        private string houseNumber = string.Empty;
        private string street = string.Empty;
        private string subDistrict = string.Empty;
        private string district = string.Empty;
        private string town = string.Empty;
        private string county = string.Empty;
        private string postcode = string.Empty;
        private GD92Msg6Attendance firstAttendance;
        private GD92Msg6Attendance secondAttendance;
        private string mapBook = string.Empty;
        private string mapRef = string.Empty;
        private string howReceived = string.Empty;
        private string telNumber = string.Empty;
        private string operatorInfo = string.Empty;
        private string waterInformation = string.Empty;
        private string locationNotices = string.Empty;
        private string addText = string.Empty;

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg6"/> class.
        /// Initialize fields with empty strings or lists.
        /// </summary>
        public GD92Msg6()
        {
            this.MsgType = GD92MsgType.MobiliseMessage6;
            this.Header = new GD92Header(this.MsgType);
            this.resourcesList = new Collection<GD92Msg6Resource>();
            this.allResourcesList = new Collection<GD92Msg6Resource>();
            this.firstAttendance = new GD92Msg6Attendance();
            this.secondAttendance = new GD92Msg6Attendance();
        }

        /// <summary>
        /// Get or set MobilisationType.
        /// </summary>
        /// <value>MobilisationType field.</value>
        [XmlElement]
        public int MobilisationType
        {
            get
            {
                return this.mobilisationType;
            }

            set
            {
                this.CheckReadOnly("MobilisationType");
                this.mobilisationType = value;
            }
        }

        /// <summary>
        /// Get or set AssistanceMessage.
        /// </summary>
        /// <value>AssistanceMessage field.</value>
        [XmlElement]
        public string AssistanceMessage
        {
            get
            {
                return this.assistanceMessage;
            }

            set
            {
                this.CheckReadOnly("AssistanceMessage");
                this.assistanceMessage = value;
            }
        }

        /// <summary>
        /// Get ResourcesList.
        /// </summary>
        /// <value>ResourcesList field.</value>
        [XmlArray]
        public Collection<GD92Msg6Resource> ResourcesList
        {
            get { return this.resourcesList; }
        }

        /// <summary>
        /// Get AllResourcesList.
        /// </summary>
        /// <value>AllResourcesList field.</value>
        [XmlArray]
        public Collection<GD92Msg6Resource> AllResourcesList
        {
            get { return this.allResourcesList; }
        }

        /// <summary>
        /// Get or set IncidentTimeAndDate.
        /// </summary>
        /// <value>IncidentTimeAndDate field.</value>
        [XmlElement]
        public DateTime IncidentTimeAndDate
        {
            get
            {
                return this.incidentTimeAndDate;
            }

            set
            {
                this.CheckReadOnly("IncidentTimeAndDate");
                this.incidentTimeAndDate = value;
            }
        }

        /// <summary>
        /// Get or set MobTimeAndDate.
        /// </summary>
        /// <value>MobTimeAndDate field.</value>
        [XmlElement]
        public DateTime MobTimeAndDate
        {
            get
            {
                return this.mobTimeAndDate;
            }

            set
            {
                this.CheckReadOnly("MobTimeAndDate");
                this.mobTimeAndDate = value;
            }
        }

        /// <summary>
        /// Get or set IncidentNumber.
        /// </summary>
        /// <value>IncidentNumber field.</value>
        [XmlElement]
        public string IncidentNumber
        {
            get
            {
                return this.incidentNumber;
            }

            set
            {
                this.CheckReadOnly("IncidentNumber");
                this.incidentNumber = value;
            }
        }

        /// <summary>
        /// Get or set IncidentType.
        /// </summary>
        /// <value>IncidentType field.</value>
        [XmlElement]
        public string IncidentType
        {
            get
            {
                return this.incidentType;
            }

            set
            {
                this.CheckReadOnly("IncidentType");
                this.incidentType = value;
            }
        }

        /// <summary>
        /// Get or set Address.
        /// </summary>
        /// <value>Address field.</value>
        [XmlElement]
        public string Address
        {
            get
            {
                return this.address;
            }

            set
            {
                this.CheckReadOnly("Address");
                this.address = value;
            }
        }

        /// <summary>
        /// Get or set HouseNumber.
        /// </summary>
        /// <value>HouseNumber field.</value>
        [XmlElement]
        public string HouseNumber
        {
            get
            {
                return this.houseNumber;
            }

            set
            {
                this.CheckReadOnly("HouseNumber");
                this.houseNumber = value;
            }
        }

        /// <summary>
        /// Get or set Street.
        /// </summary>
        /// <value>Street field.</value>
        [XmlElement]
        public string Street
        {
            get
            {
                return this.street;
            }

            set
            {
                this.CheckReadOnly("Street");
                this.street = value;
            }
        }

        /// <summary>
        /// Get or set SubDistrict.
        /// </summary>
        /// <value>SubDistrict field.</value>
        [XmlElement]
        public string SubDistrict
        {
            get
            {
                return this.subDistrict;
            }

            set
            {
                this.CheckReadOnly("SubDistrict");
                this.subDistrict = value;
            }
        }

        /// <summary>
        /// Get or set Distric.
        /// </summary>
        /// <value>Distric field.</value>
        [XmlElement]
        public string District
        {
            get
            {
                return this.district;
            }

            set
            {
                this.CheckReadOnly("District");
                this.district = value;
            }
        }

        /// <summary>
        /// Get or set Town.
        /// </summary>
        /// <value>Town field.</value>
        [XmlElement]
        public string Town
        {
            get
            {
                return this.town;
            }

            set
            {
                this.CheckReadOnly("Town");
                this.town = value;
            }
        }

        /// <summary>
        /// Get or set County.
        /// </summary>
        /// <value>County field.</value>
        [XmlElement]
        public string County
        {
            get
            {
                return this.county;
            }

            set
            {
                this.CheckReadOnly("County");
                this.county = value;
            }
        }

        /// <summary>
        /// Get or set Postcode.
        /// </summary>
        /// <value>Postcode field.</value>
        [XmlElement]
        public string Postcode
        {
            get
            {
                return this.postcode;
            }

            set
            {
                this.CheckReadOnly("Postcode");
                this.postcode = value;
            }
        }

        /// <summary>
        /// Get or set FirstAttendance.
        /// </summary>
        /// <value>FirstAttendance field.</value>
        [XmlElement]
        public GD92Msg6Attendance FirstAttendance
        {
            get
            {
                return this.firstAttendance;
            }

            set
            {
                this.CheckReadOnly("FirstAttendance");
                this.firstAttendance = value;
            }
        }

        /// <summary>
        /// Get or set SecondAttendance.
        /// </summary>
        /// <value>SecondAttendance field.</value>
        [XmlElement]
        public GD92Msg6Attendance SecondAttendance
        {
            get
            {
                return this.secondAttendance;
            }

            set
            {
                this.CheckReadOnly("SecondAttendance");
                this.secondAttendance = value;
            }
        }

        /// <summary>
        /// Get or set MapBook.
        /// </summary>
        /// <value>MapBook field.</value>
        [XmlElement]
        public string MapBook
        {
            get
            {
                return this.mapBook;
            }

            set
            {
                this.CheckReadOnly("MapBook");
                this.mapBook = value;
            }
        }

        /// <summary>
        /// Get or set MapRef.
        /// </summary>
        /// <value>MapRef field.</value>
        [XmlElement]
        public string MapRef
        {
            get
            {
                return this.mapRef;
            }

            set
            {
                this.CheckReadOnly("MapRef");
                this.mapRef = value;
            }
        }

        /// <summary>
        /// Get or set HowReceived (not used with Storm).
        /// </summary>
        /// <value>HowReceived field.</value>
        [XmlElement]
        public string HowReceived
        {
            get
            {
                return this.howReceived;
            }

            set
            {
                this.CheckReadOnly("HowReceived");
                this.howReceived = value;
            }
        }

        /// <summary>
        /// Get or set TelNumber.
        /// </summary>
        /// <value>TelNumber field.</value>
        [XmlElement]
        public string TelNumber
        {
            get
            {
                return this.telNumber;
            }

            set
            {
                this.CheckReadOnly("TelNumber");
                this.telNumber = value;
            }
        }

        /// <summary>
        /// Get or set Operator information.
        /// </summary>
        /// <value>Operator information field.</value>
        [XmlElement]
        public string OperatorInfo
        {
            get
            {
                return this.operatorInfo;
            }

            set
            {
                this.CheckReadOnly("OperatorInfo");
                this.operatorInfo = value;
            }
        }

        /// <summary>
        /// Get or set water information (not used with Storm).
        /// </summary>
        /// <value>Water information field.</value>
        [XmlElement]
        public string WaterInformation
        {
            get
            {
                return this.waterInformation;
            }

            set
            {
                this.CheckReadOnly("WaterInformation");
                this.waterInformation = value;
            }
        }

        /// <summary>
        /// Get or set Location Notices. This is a long compressed string containing labelled data.
        /// </summary>
        /// <value>Location Notices field.</value>
        [XmlElement]
        public string LocationNotices
        {
            get
            {
                return this.locationNotices;
            }

            set
            {
                this.CheckReadOnly("LocationNotices");
                this.locationNotices = value;
            }
        }

        /// <summary>
        /// Get or set Additional text (not used).
        /// </summary>
        /// <value>Additional text field.</value>
        [XmlElement]
        public string AddText
        {
            get
            {
                return this.addText;
            }

            set
            {
                this.CheckReadOnly("AddText");
                this.addText = value;
            }
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg6>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg6>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg6 Copy()
        {
            var copy = new GD92Msg6();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.MobilisationType = this.MobilisationType;
            copy.AssistanceMessage = this.AssistanceMessage;
            foreach (GD92Msg6Resource resource in this.ResourcesList)
            {
                copy.ResourcesList.Add(resource.Clone());
            }

            foreach (GD92Msg6Resource resource in this.AllResourcesList)
            {
                copy.AllResourcesList.Add(resource.Clone());
            }

            copy.IncidentTimeAndDate = this.IncidentTimeAndDate;
            copy.MobTimeAndDate = this.MobTimeAndDate;
            copy.IncidentNumber = this.IncidentNumber;
            copy.IncidentType = this.IncidentType;
            copy.Address = this.Address;
            copy.HouseNumber = this.HouseNumber;
            copy.Street = this.Street;
            copy.SubDistrict = this.SubDistrict;
            copy.District = this.District;
            copy.Town = this.Town;
            copy.County = this.County;
            copy.Postcode = this.Postcode;
            copy.FirstAttendance = this.FirstAttendance.Clone();
            copy.SecondAttendance = this.SecondAttendance.Clone();
            copy.MapBook = this.MapBook;
            copy.MapRef = this.MapRef;
            copy.HowReceived = this.HowReceived;
            copy.TelNumber = this.TelNumber;
            copy.OperatorInfo = this.OperatorInfo;
            copy.WaterInformation = this.WaterInformation;
            copy.LocationNotices = this.LocationNotices;
            copy.AddText = this.AddText;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// Omits IncidentTimeAndDate and MobTimeAndDate.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            GD92Message.ValidateMobilisationType("MobilisationType", this.MobilisationType);
            GD92Message.ValidateByLength("AssistanceMessage", this.AssistanceMessage, GD92Message.MaxAssistanceMessage);
            ValidateResourcesList("ResourcesList", this.ResourcesList);
            ValidateResourcesList("AllResourcesList", this.AllResourcesList);
            GD92Message.ValidateIncidentNumber("IncidentNumber", this.IncidentNumber);
            GD92Message.ValidateByLength("IncidentType", this.IncidentType, GD92Message.MaxIncidentTypeLength);
            GD92Message.ValidateByLength("Address", this.Address, GD92Message.MaxAddressLength);
            GD92Message.ValidateByLength("HouseNumber", this.HouseNumber, GD92Message.MaxHouseNumberLength);
            GD92Message.ValidateByLength("Street", this.Street, GD92Message.MaxStreetLength);
            GD92Message.ValidateByLength("SubDistrict", this.SubDistrict, GD92Message.MaxSubDistrictLength);
            GD92Message.ValidateByLength("District", this.District, GD92Message.MaxDistrictLength);
            GD92Message.ValidateByLength("Town", this.Town, GD92Message.MaxTownLength);
            GD92Message.ValidateByLength("County", this.County, GD92Message.MaxCountyLength);
            GD92Message.ValidateByLength("Postcode", this.Postcode, GD92Message.MaxPostcodeLength);
            this.FirstAttendance.Validate("FirstAttendance");
            this.SecondAttendance.Validate("SecondAttendance");
            GD92Message.ValidateByLength("MapBook", this.MapBook, GD92Message.MaxMapBookLength);
            GD92Message.ValidateByLength("MapRef", this.MapRef, GD92Message.MaxMapRefLength);
            GD92Message.ValidateByLength("HowReceived", this.HowReceived, GD92Message.MaxHowReceivedLength);
            GD92Message.ValidateByLength("TelNumber", this.TelNumber, GD92Message.MaxTelNumberLength);
            GD92Message.ValidateByLength("OperatorInfo", this.OperatorInfo, GD92Message.MaxOperatorInfoLength);
            GD92Message.ValidateByLength("WaterInformation", this.WaterInformation, GD92Message.MaxWaterInformationLength);
            GD92Message.ValidateByLength("LocationNotices", this.LocationNotices, GD92Message.MaxTextLength);
            GD92Message.ValidateByLength("AddText", this.AddText, GD92Message.MaxAddTextLength);
        }

        /// <summary>
        /// Call the Validate method for all resources in the collection to check they are within GD92 limits.
        /// </summary>
        /// <param name="fieldName">Name of resource collection (for error reporting).</param>
        /// <param name="resourcesList">Collection  of resources.</param>
        private static void ValidateResourcesList(string fieldName, Collection<GD92Msg6Resource> resourcesList)
        {
            foreach (GD92Msg6Resource r in resourcesList)
            {
                r.Validate(fieldName);
            }
        }
    }
}

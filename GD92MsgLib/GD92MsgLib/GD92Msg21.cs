﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// This is not fully implemented, but is used for testing with unsupported message types.
    /// </summary>
    [Serializable]
    public class GD92Msg21 : GD92Message
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg21"/> class.
        /// </summary>
        public GD92Msg21()
        {
            this.MsgType = GD92MsgType.DutyStaffingUpdate;
        }
    }
}

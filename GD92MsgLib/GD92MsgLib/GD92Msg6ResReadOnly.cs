﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Xml.Serialization;

namespace Thales.KentFire.GD92
{
    [XmlRootAttribute()]
    public class GD92Msg6ResReadOnly : GD92Msg6Resource
    {
        private readonly string m_Callsign;
        private readonly string m_Commentary;

        /// <summary>
        /// Default constructor (added for XML serialization)
        /// </summary>
        public GD92Msg6ResReadOnly()
        {
        	
        }
        public GD92Msg6ResReadOnly(string callsign, string commentary)
        {
            m_Callsign = callsign;
            m_Commentary = commentary;
        }

        [XmlElement()]
        public override string Callsign
        {
            get { return m_Callsign; }
            set { throw new MessageAccessException("Callsign"); }
        }

        [XmlElement()]
        public override string Commentary
        {
            get { return m_Commentary; }
            set { throw new MessageAccessException("Commentary"); }
        }
    }
}

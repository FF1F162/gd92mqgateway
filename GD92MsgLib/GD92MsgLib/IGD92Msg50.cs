﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
namespace Thales.KFRS.GD92
{
    public interface IGD92Msg50 : IGD92Message
    {
        GD92Message AcknowledgedMessage { get; }
        GD92MsgType AcknowledgedMsgType { get; }
    }
}

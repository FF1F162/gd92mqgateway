﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 50 Ack.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg50 : GD92Message, IGD92Message
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg50"/> class.
        /// </summary>
        public GD92Msg50()
        {
            this.MsgType = GD92MsgType.Ack;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg50"/> class.
        /// This constructor associates the Ack with the message it is acknowledging.
        /// </summary>
        /// <param name="acknowledgedMessage">Message being acknowleded.</param>
        public GD92Msg50(GD92Message acknowledgedMessage) : this()
        {
            this.OriginalMessage = acknowledgedMessage;
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg50>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg50>(this);
        }

        /// <summary>
        /// Needed to conform with IGD92Message.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
        }
    }
}

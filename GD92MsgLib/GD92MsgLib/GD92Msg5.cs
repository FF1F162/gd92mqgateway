﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 5 Resource Status Request.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg5 : GD92Message, IGD92Message
    {
        private string callsign;

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg5"/> class.
        /// </summary>
        public GD92Msg5()
        {
            this.MsgType = GD92MsgType.ResourceStatusRequest;
            this.Header = new GD92Header(this.MsgType);
            this.Callsign = GD92Message.DefaultNotSet;
        }

        /// <summary>
        /// Get or set callsign.
        /// </summary>
        /// <value>Callsign field.</value>
        [XmlElement]
        public string Callsign
        {
            get
            {
                return this.callsign;
            }

            set
            {
                this.CheckReadOnly("Callsign");
                this.callsign = value;
            }
        }

        /// <summary>
        /// Set true by the HAP if it decides that Callsign actually refers to a station.
        /// </summary>
        /// <value>False for callsign request.</value>
        public bool StationRequest { get; set; }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg5>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg5>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg5 Copy()
        {
            var copy = new GD92Msg5();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.Callsign = this.Callsign;
            copy.StationRequest = this.StationRequest;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            GD92Message.ValidateString("Callsign", this.Callsign, GD92Message.MaxCallsignLength, GD92Message.MinCallsignLength);
        }
    }
}

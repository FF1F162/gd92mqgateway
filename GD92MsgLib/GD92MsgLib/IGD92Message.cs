﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;

    /// <summary>
    /// Methods and properties required for a GD92 message.
    /// Many of the properties were originally read-only but this is incompatible with serialization requirements.
    /// </summary>
    public interface IGD92Message
    {
        int SerialNumber { get; set; }

        GD92MsgType MsgType { get; set; }

        string Destination { get; set; }

        DateTime SentAt { get; set; }

        DateTime ReceivedAt { get; set; }

        string From { get; set; }

        bool ManualAck { get; set; }

        bool DoNotChange { get; } 

        void ToXml(string fileName);

        string ToXml();

        void Validate();
    }
}

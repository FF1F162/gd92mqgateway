﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Header details that depend on message type, to be shown as attributes when serialised.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Header
    {
        /// <summary>
        /// For Ack and Nak, priority and port depend on the message being acknowledged. Provide protocol as a constant.
        /// </summary>
        public const int NakProtocolVersion = 1;
        public const int AckProtocolVersion = 1;

        /// <summary>
        /// Except for MobiliseMessagePort these are port numbers customarily used with MOBS. Storm may use different numbers.
        /// </summary>
        public const short DefaultPortNumber = 1;
        public const short PeripheralPortNumber = 12;
        public const short AlerterPortNumber = 13;
        public const short PrinterPortNumber = 15;

        /// <summary>
        /// As a precaution, give the mobilisation message its own port to avoid it getting stuck in the TransmitQueue 
        /// if something goes wrong with other text traffic (as happened April 10 2015 now fixed).
        /// </summary>
        public const short MobiliseMessagePortNumber = 16;

        /// <summary>
        /// This is defined in GD-92 (other port numbers are to be 'defined by the contractor').
        /// </summary>
        public const short RouterPortNumber = 0;

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Header"/> class.
        /// Default constructor for XML (de)serialization.
        /// </summary>
        public GD92Header()
        {
        }

        /// <summary>
        ///  Initialises a new instance of the <see cref="GD92Header"/> class.
        ///  Tailor the header for the message type.
        /// </summary>
        /// <param name="messageType">GD92 message type enumeration.</param>
        public GD92Header(GD92MsgType messageType)
        {
            this.VersionAndPriorityFromMessageType(messageType);
            this.PortFromMessageType(messageType);
        }

        /// <summary>
        /// Message protocol version.
        /// </summary>
        /// <value>Protocol version number.</value>
        [XmlAttribute]
        public int ProtocolVersion { get; set; }

        /// <summary>
        /// Message priority.
        /// </summary>
        /// <value>Priority number.</value>
        [XmlAttribute]
        public int Priority { get; set; }

        /// <summary>
        /// Customary port number.
        /// </summary>
        /// <value>Port number.</value>
        [XmlAttribute]
        public int StandardPort { get; set; }

        /// <summary>
        /// Version is from 'Message envelope definition' near the beginning of the GD-92 specification.
        /// Priority is from the table of message definitions in Appendix B of the GD-92 specification.
        /// </summary>
        /// <param name="messageType">GD92 message type enumeration.</param>
        private void VersionAndPriorityFromMessageType(GD92MsgType messageType)
        {
            switch (messageType)
            {
                case GD92MsgType.PageOfficer:
                    this.ProtocolVersion = 2;
                    this.Priority = 1;
                    break;
                case GD92MsgType.MobiliseCommand:
                case GD92MsgType.MobiliseMessage:
                case GD92MsgType.MobiliseMessage6:
                case GD92MsgType.ActivatePeripheral:
                case GD92MsgType.Text:
                case GD92MsgType.AlertCrew:
                case GD92MsgType.Ack: // overwrite with priority of original message
                case GD92MsgType.Nak: // overwrite with priority of original message
                    this.ProtocolVersion = 1;
                    this.Priority = 1;
                    break;
                case GD92MsgType.Makeup:
                case GD92MsgType.InterruptRequest:
                    this.ProtocolVersion = 1;
                    this.Priority = 2;
                    break;
                case GD92MsgType.PeripheralStatusRequest:
                case GD92MsgType.ResourceStatus:
                case GD92MsgType.DutyStaffingUpdate:
                case GD92MsgType.PeripheralStatus:
                case GD92MsgType.AlertStatus:
                case GD92MsgType.PrinterStatus:
                    this.ProtocolVersion = 1;
                    this.Priority = 3;
                    break;
                case GD92MsgType.ResourceStatusRequest:
                case GD92MsgType.LogUpdate:
                case GD92MsgType.Stop:
                case GD92MsgType.RawFrame:
                    this.ProtocolVersion = 1;
                    this.Priority = 4;
                    break;
                case GD92MsgType.NotDefined:
                default:
                    throw new MessageValidationException("messageType", messageType, "it is not implemented");
            }
        }

        /// <summary>
        /// For a new message this provides the port customarily used for this message type.
        /// </summary>
        /// <param name="messageType">GD92 message type enumeration.</param>
        private void PortFromMessageType(GD92MsgType messageType)
        {
            int port = DefaultPortNumber;
            switch (messageType)
            {
                case GD92MsgType.MobiliseCommand:
                case GD92MsgType.ActivatePeripheral:
                    port = PeripheralPortNumber;
                    break;
                case GD92MsgType.MobiliseMessage:
                case GD92MsgType.MobiliseMessage6:
                    port = MobiliseMessagePortNumber;
                    break;
                case GD92MsgType.Text:
                    port = PrinterPortNumber;
                    break;
                case GD92MsgType.AlertCrew:
                    port = AlerterPortNumber;
                    break;
                default:
                    break;
            }

            this.StandardPort = port;
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Message access exception.
    /// </summary>
    [Serializable]
    public sealed class MessageValidationException : Exception
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MessageValidationException"/> class.
        /// </summary>
        public MessageValidationException()
            : base()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageValidationException"/> class.
        /// </summary>
        /// <param name="message">Message description.</param>
        public MessageValidationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageValidationException"/> class.
        /// </summary>
        /// <param name="message">Message description.</param>
        /// <param name="innerException">Inner exception.</param>
        public MessageValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageValidationException"/> class.
        /// </summary>
        /// <param name="fieldName">Name of problem field.</param>
        /// <param name="content">Content of field.</param>
        /// <param name="reason">Reason validation failed.</param>
        public MessageValidationException(string fieldName, object content, string reason)
            : base(string.Format(System.Globalization.CultureInfo.InvariantCulture, "Field {0} ({1}) is invalid because {2}", fieldName, content.ToString(), reason))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageValidationException"/> class.
        /// </summary>
        /// <param name="fieldName">Name of problem field.</param>
        /// <param name="content">Content of field.</param>
        /// <param name="innerException">Inner exception.</param>
        public MessageValidationException(string fieldName, object content, Exception innerException)
            : base(string.Format(System.Globalization.CultureInfo.InvariantCulture, "Field {0} ({1}) is invalid", fieldName, content.ToString()), innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageValidationException"/> class.
        /// Added to placate FxCop.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        private MessageValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

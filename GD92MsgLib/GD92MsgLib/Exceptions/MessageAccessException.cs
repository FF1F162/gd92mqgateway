﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Message access exception.
    /// </summary>
    [Serializable]
    public sealed class MessageAccessException : Exception
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MessageAccessException"/> class.
        /// Added to placate FxCop.
        /// </summary>
        public MessageAccessException()
            : base("Message is marked as read-only")
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageAccessException"/> class.
        /// </summary>
        /// <param name="fieldName">Name of field being changed.</param>
        public MessageAccessException(string fieldName)
            : base(string.Format(System.Globalization.CultureInfo.InvariantCulture, "Trying to set field {0} but message is marked as read-only", fieldName))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageAccessException"/> class.
        /// Added to placate FxCop.
        /// </summary>
        /// <param name="message">Message for display.</param>
        /// <param name="innerException">Inner exception.</param>
        public MessageAccessException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="MessageAccessException"/> class.
        /// Added to placate FxCop.
        /// </summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        private MessageAccessException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

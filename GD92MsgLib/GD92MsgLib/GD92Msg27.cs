﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 27 Text.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg27 : GD92Message, IGD92Message
    {
        private string messageText;

        public GD92Msg27()
        {
            this.MsgType = GD92MsgType.Text;
            this.Header = new GD92Header(this.MsgType);
            this.Text = string.Empty;
        }

        /// <summary>
        /// Get or set message text.
        /// </summary>
        /// <value>Text field.</value>
        [XmlElement]
        public string Text
        {
            get
            {
                return this.messageText;
            }

            set
            {
                this.CheckReadOnly("Text");
                this.messageText = value;
            }
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg27>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg27>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg27 Copy()
        {
            var copy = new GD92Msg27();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.Text = this.Text;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            GD92Message.ValidateByLength("Text", this.Text, GD92Message.MaxTextLength);
        }
    }
}

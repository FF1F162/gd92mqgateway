﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.ObjectModel;
    using System.Xml.Serialization;

    [XmlRootAttribute]
    public class GD92Msg6Attendance
    {
        private readonly Collection<string> callsigns;
 
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg6Attendance"/> class.
        /// </summary>
        public GD92Msg6Attendance()
        {
            this.callsigns = new Collection<string>();
            this.Address = string.Empty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg6Attendance"/> class.
        /// Constructor used to set the properties.
        /// </summary>
        /// <param name="callsigns">Collection of callsigns required to attend.</param>
        /// <param name="address">Attendance address.</param>
        public GD92Msg6Attendance(Collection<string> callsigns, string address)
        {
            this.callsigns = callsigns;
            this.Address = address;
        }

        /// <summary>
        /// Names of callsigns required to attend at the address specified.
        /// </summary>
        /// <value>Collection of callsigns required to attend.</value>
        [XmlArray]
        public Collection<string> Callsigns 
        {
            get { return this.callsigns; }
        }
        
        /// <summary>
        /// Address where attendance is required.
        /// </summary>
        /// <value>Attendance address.</value>
        [XmlElement]
        public string Address { get; set; }

        /// <summary>
        /// Populate a new instance with references to (immutable) strings in this instance.
        /// </summary>
        /// <returns>New instance with copied callsigns and address.</returns>
        public GD92Msg6Attendance Clone()
        {
            var clone = new GD92Msg6Attendance();
            foreach (string callsign in this.Callsigns)
            {
                clone.Callsigns.Add(callsign);
            }

            clone.Address = this.Address;
            return clone;
        }

        /// <summary>
        /// Check the length of fields against limits specified in GD-92.
        /// </summary>
        /// <param name="fieldName">Field name (used for error reporting).</param>
        public void Validate(string fieldName)
        {
            string paramCallsign = fieldName + " Callsign";
            for (int i = 0; i < this.Callsigns.Count; i++)
            {
                GD92Message.ValidateCallsign(paramCallsign, this.Callsigns[i]);
            }

            GD92Message.ValidateByLength(fieldName + " Address", this.Address, GD92Message.MaxAddressLength);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 65 Printer Status.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg65 : GD92Message, IGD92Message
    {
        private int printerStatus;

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg65"/> class.
        /// </summary>
        public GD92Msg65()
        {
            this.MsgType = GD92MsgType.PrinterStatus;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// Get or set printer status.
        /// </summary>
        /// <value>Printer status.</value>
        [XmlElement]
        public int PrinterStatus
        {
            get
            {
                return this.printerStatus;
            }

            set
            {
                this.CheckReadOnly("PrinterStatus");
                this.printerStatus = value;
            }
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg65>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg65>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg65 Copy()
        {
            var copy = new GD92Msg65();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.printerStatus = this.printerStatus;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            this.ValidatePrinterStatus();
        }

        /// <summary>
        /// Check printer status code is defined in GD-92.
        /// </summary>
        private void ValidatePrinterStatus()
        {
            const string ParamName = "PrinterStatus";
            if (!Enum.IsDefined(typeof(GD92PrinterStatus), this.printerStatus))
            {
                throw new ArgumentOutOfRangeException(ParamName,
                    string.Format(System.Globalization.CultureInfo.InvariantCulture, "Printer status {0} is not defined", this.printerStatus));
            }
        }
    }
}

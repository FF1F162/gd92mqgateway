﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
namespace Thales.KFRS.GD92
{
    public interface IGD92Msg1 : IGD92Message
    {
        bool Appliance1 { get; set; }
        bool Appliance2 { get; set; }
        bool Appliance3 { get; set; }
        bool Appliance4 { get; set; }
        bool Appliance5 { get; set; }
        bool Appliance6 { get; set; }
        bool Appliance7 { get; set; }
        bool Appliance8 { get; set; }
        bool Appliance9 { get; set; }
        bool Doors { get; set; }
        bool Lights { get; set; }
        bool Sounders { get; set; }
        bool Spare2 { get; set; }
        bool Spare3 { get; set; }
        bool Spare4 { get; set; }
        bool StandbySounder { get; set; }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Collections.Generic;
using System.Text;

namespace Thales.KFRS.GD92
{
    [Serializable]
    public class GD92Msg25 : GD92Message
    {
        public GD92Msg25()
        {
            m_Type = GD92MsgType.InterruptRequest;
        }
    }
}

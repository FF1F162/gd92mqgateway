﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 28 Peripheral Status inherits from Message 1.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg28 : GD92Msg1, IGD92Message
    {
        private IpPeripherals manualAcknowledgementPressed;
        private IpPeripherals powerFailedBatteriesOn;
        private IpPeripherals powerFailedStandbyGeneratorOn;
        private IpPeripherals repeatLastMessagePressed;
        private IpPeripherals batteriesLow;
        private IpPeripherals paperLow;
        private IpPeripherals spareIp1;
        private IpPeripherals spareIp2;
        private IpPeripherals runningCallTelephoneDoorOpened;
        private IpPeripherals paperOut;
        private IpPeripherals spareIp3;
        private IpPeripherals spareIp4;
        private IpPeripherals spareIp5;
        private IpPeripherals spareIp6;
        private IpPeripherals spareIp7;
        private IpPeripherals spareIp8;

        /// <summary>
        ///  Initialises a new instance of the <see cref="GD92Msg28"/> class.
        /// </summary>
        public GD92Msg28()
        {
            this.MsgType = GD92MsgType.PeripheralStatus;
            this.Header = new GD92Header(this.MsgType);
        }
        
        /// <summary>
        /// ManualAcknowledgementPressed flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool ManualAcknowledgementPressed
        {
            get
            {
                return this.manualAcknowledgementPressed > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("ManualAcknowledgementPressed");
                this.manualAcknowledgementPressed = value ? IpPeripherals.ManualAcknowledgementPressed : 0;
            }
        }

        /// <summary>
        /// PowerFailedBatteriesOn flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool PowerFailedBatteriesOn
        {
            get
            {
                return this.powerFailedBatteriesOn > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("PowerFailedBatteriesOn");
                this.powerFailedBatteriesOn = value ? IpPeripherals.PowerFailedBatteriesOn : 0;
            }
        }

        /// <summary>
        /// PowerFailedStandbyGeneratorOn flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool PowerFailedStandbyGeneratorOn
        {
            get
            {
                return this.powerFailedStandbyGeneratorOn > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("PowerFailedStandbyGeneratorOn");
                this.powerFailedStandbyGeneratorOn = value ? IpPeripherals.PowerFailedStandbyGeneratorOn : 0;
            }
        }

        /// <summary>
        /// RepeatLastMessagePressed flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool RepeatLastMessagePressed
        {
            get
            {
                return this.repeatLastMessagePressed > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("RepeatLastMessagePressed");
                this.repeatLastMessagePressed = value ? IpPeripherals.RepeatLastMessagePressed : 0;
            }
        }

        /// <summary>
        /// BatteriesLow flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool BatteriesLow
        {
            get
            {
                return this.batteriesLow > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("BatteriesLow");
                this.batteriesLow = value ? IpPeripherals.BatteriesLow : 0;
            }
        }

        /// <summary>
        /// PaperLow flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool PaperLow
        {
            get
            {
                return this.paperLow > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("PaperLow");
                this.paperLow = value ? IpPeripherals.PaperLow : 0;
            }
        }

        /// <summary>
        /// SpareIp1 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp1
        {
            get
            {
                return this.spareIp1 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp1");
                this.spareIp1 = value ? IpPeripherals.SpareIp1 : 0;
            }
        }

        /// <summary>
        /// SpareIp2 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp2
        {
            get
            {
                return this.spareIp2 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp2");
                this.spareIp2 = value ? IpPeripherals.SpareIp2 : 0;
            }
        }

        /// <summary>
        /// RunningCallTelephoneDoorOpened flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool RunningCallTelephoneDoorOpened
        {
            get
            {
                return this.runningCallTelephoneDoorOpened > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("RunningCallTelephoneDoorOpened");
                this.runningCallTelephoneDoorOpened = value ? IpPeripherals.RunningCallTelephoneDoorOpened : 0;
            }
        }

        /// <summary>
        /// PaperOut flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool PaperOut
        {
            get
            {
                return this.paperOut > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("PaperOut");
                this.paperOut = value ? IpPeripherals.PaperOut : 0;
            }
        }
        
        /// <summary>
        /// SpareIp3 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp3
        {
            get
            {
                return this.spareIp3 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp3");
                this.spareIp3 = value ? IpPeripherals.SpareIp3 : 0;
            }
        }
        
        /// <summary>
        /// SpareIp4 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp4
        {
            get
            {
                return this.spareIp4 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp4");
                this.spareIp4 = value ? IpPeripherals.SpareIp4 : 0;
            }
        }
        
        /// <summary>
        /// SpareIp5 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp5
        {
            get
            {
                return this.spareIp5 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp5");
                this.spareIp5 = value ? IpPeripherals.SpareIp5 : 0;
            }
        }
        
        /// <summary>
        /// SpareIp6 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp6
        {
            get
            {
                return this.spareIp6 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp6");
                this.spareIp6 = value ? IpPeripherals.SpareIp6 : 0;
            }
        }
        
        /// <summary>
        /// SpareIp7 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp7
        {
            get
            {
                return this.spareIp7 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp7");
                this.spareIp7 = value ? IpPeripherals.SpareIp7 : 0;
            }
        }
        
        /// <summary>
        /// SpareIp8 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool SpareIp8
        {
            get
            {
                return this.spareIp8 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("SpareIp8");
                this.spareIp8 = value ? IpPeripherals.SpareIp8 : 0;
            }
        }

        /// <summary>
        /// Input peripherals flags.
        /// </summary>
        /// <value>IpPeripherals enumeration.</value>
        [XmlIgnore]
        public IpPeripherals IpPeripherals
        {
            get
            {
                return
                    this.manualAcknowledgementPressed | this.powerFailedBatteriesOn | this.powerFailedStandbyGeneratorOn |
                    this.repeatLastMessagePressed | this.batteriesLow |
                    this.paperLow | this.spareIp1 | this.spareIp2 | this.runningCallTelephoneDoorOpened | this.paperOut |
                    this.spareIp3 | this.spareIp4 | this.spareIp5 | this.spareIp6 | this.spareIp7 | this.spareIp8;
            }

            set
            {
                IpPeripherals inputPeripherals = value;
                this.manualAcknowledgementPressed = inputPeripherals & IpPeripherals.ManualAcknowledgementPressed;
                this.powerFailedBatteriesOn = inputPeripherals & IpPeripherals.PowerFailedBatteriesOn;
                this.powerFailedStandbyGeneratorOn = inputPeripherals & IpPeripherals.PowerFailedStandbyGeneratorOn;
                this.repeatLastMessagePressed = inputPeripherals & IpPeripherals.RepeatLastMessagePressed;
                this.batteriesLow = inputPeripherals & IpPeripherals.BatteriesLow;
                this.paperLow = inputPeripherals & IpPeripherals.PaperLow;
                this.spareIp1 = inputPeripherals & IpPeripherals.SpareIp1;
                this.spareIp2 = inputPeripherals & IpPeripherals.SpareIp2;
                this.runningCallTelephoneDoorOpened = inputPeripherals & IpPeripherals.RunningCallTelephoneDoorOpened;
                this.paperOut = inputPeripherals & IpPeripherals.PaperOut;
                this.spareIp3 = inputPeripherals & IpPeripherals.SpareIp3;
                this.spareIp4 = inputPeripherals & IpPeripherals.SpareIp4;
                this.spareIp5 = inputPeripherals & IpPeripherals.SpareIp5;
                this.spareIp6 = inputPeripherals & IpPeripherals.SpareIp6;
                this.spareIp7 = inputPeripherals & IpPeripherals.SpareIp7;
                this.spareIp8 = inputPeripherals & IpPeripherals.SpareIp8;
            }
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public new GD92Msg28 Copy()
        {
            var copy = new GD92Msg28();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.IpPeripherals = this.IpPeripherals;
            copy.OpPeripherals = this.OpPeripherals;
            return copy;
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public override void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg28>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public override string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg28>(this);
        }
    }
}

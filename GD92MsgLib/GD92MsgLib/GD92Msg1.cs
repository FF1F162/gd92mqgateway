﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 1 Mobilise Command.
    /// Base class for Message 40 Alert Crew.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg1 : GD92Message, IGD92Message
    {
        private OpPeripherals sounders;
        private OpPeripherals lights;
        private OpPeripherals spare2;
        private OpPeripherals spare3;
        private OpPeripherals spare4;
        private OpPeripherals doors;
        private OpPeripherals standbySounder;
        private OpPeripherals appliance9;
        private OpPeripherals appliance1;
        private OpPeripherals appliance2;
        private OpPeripherals appliance3;
        private OpPeripherals appliance4;
        private OpPeripherals appliance5;
        private OpPeripherals appliance6;
        private OpPeripherals appliance7;
        private OpPeripherals appliance8;        

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg1"/> class.
        /// </summary>
        public GD92Msg1()
        {
            this.MsgType = GD92MsgType.MobiliseCommand;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// Sounders flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Sounders
        {
            get
            {
                return this.sounders > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Sounders");
                this.sounders = value ? OpPeripherals.Sounders : 0;
            }
        }

        /// <summary>
        /// Lights flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Lights
        {
            get
            {
                return this.lights > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Lights");
                this.lights = value ? OpPeripherals.Lights : 0;
            }
        }

        /// <summary>
        /// Spare flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Spare2
        {
            get
            {
                return this.spare2 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Spare2");
                this.spare2 = value ? OpPeripherals.Spare2 : 0;
            }
        }

        /// <summary>
        /// Spare flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Spare3
        {
            get
            {
                return this.spare3 > 0 ? true : false;
            }
            
            set
            {
                this.CheckReadOnly("Spare3");
                this.spare3 = value ? OpPeripherals.Spare3 : 0;
            }
        }

        /// <summary>
        /// Spare flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Spare4
        {
            get
            {
                return this.spare4 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Spare4");
                this.spare4 = value ? OpPeripherals.Spare4 : 0;
            }
        }

        /// <summary>
        /// Doors flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Doors
        {
            get
            {
                return this.doors > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Doors");
                this.doors = value ? OpPeripherals.Doors : 0;
            }
        }

        /// <summary>
        /// Standby Sounder flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool StandbySounder
        {
            get
            {
                return this.standbySounder > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("StandbySounder");
                this.standbySounder = value ? OpPeripherals.StandbySounder : 0;
            }
        }

        /// <summary>
        /// Appliance 9 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance9
        {
            get
            {
                return this.appliance9 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance9");
                this.appliance9 = value ? OpPeripherals.Appliance9 : 0;
            }
        }

        /// <summary>
        /// Appliance 1 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance1
        {
            get
            {
                return this.appliance1 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance1");
                this.appliance1 = value ? OpPeripherals.Appliance1 : 0;
            }
        }

        /// <summary>
        /// Appliance 2 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance2
        {
            get
            {
                return this.appliance2 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance2");
                this.appliance2 = value ? OpPeripherals.Appliance2 : 0;
            }
        }

        /// <summary>
        /// Appliance 3 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance3
        {
            get
            {
                return this.appliance3 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance3");
                this.appliance3 = value ? OpPeripherals.Appliance3 : 0;
            }
        }

        /// <summary>
        /// Appliance 4 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance4
        {
            get
            {
                return this.appliance4 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance4");
                this.appliance4 = value ? OpPeripherals.Appliance4 : 0;
            }
        }

        /// <summary>
        /// Appliance 5 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance5
        {
            get
            {
                return this.appliance5 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance5");
                this.appliance5 = value ? OpPeripherals.Appliance5 : 0;
            }
        }

        /// <summary>
        /// Appliance 6 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance6
        {
            get
            {
                return this.appliance6 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance6");
                this.appliance6 = value ? OpPeripherals.Appliance6 : 0;
            }
        }

        /// <summary>
        /// Appliance 7 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance7
        {
            get
            {
                return this.appliance7 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance7");
                this.appliance7 = value ? OpPeripherals.Appliance7 : 0;
            }
        }

        /// <summary>
        /// Appliance 8 flag.
        /// </summary>
        /// <value>True if flag is set.</value>
        [XmlElement]
        public bool Appliance8
        {
            get
            {
                return this.appliance8 > 0 ? true : false;
            }

            set
            {
                this.CheckReadOnly("Appliance8");
                this.appliance8 = value ? OpPeripherals.Appliance8 : 0;
            }
        }

        /// <summary>
        /// Output peripherals flags.
        /// </summary>
        /// <value>OpPeripherals enumeration.</value>
        [XmlIgnore]
        public OpPeripherals OpPeripherals
        {
            get
            {
                return
                    this.appliance1 | this.appliance2 | this.appliance3 | this.appliance4 | this.appliance5 |
                    this.appliance6 | this.appliance7 | this.appliance8 | this.appliance9 | this.doors |
                    this.lights | this.sounders | this.spare2 | this.spare3 | this.spare4 | this.standbySounder;
            }

            set
            {
                OpPeripherals outputPeripherals = value;
                this.appliance1 = outputPeripherals & OpPeripherals.Appliance1;
                this.appliance2 = outputPeripherals & OpPeripherals.Appliance2;
                this.appliance3 = outputPeripherals & OpPeripherals.Appliance3;
                this.appliance4 = outputPeripherals & OpPeripherals.Appliance4;
                this.appliance5 = outputPeripherals & OpPeripherals.Appliance5;
                this.appliance6 = outputPeripherals & OpPeripherals.Appliance6;
                this.appliance7 = outputPeripherals & OpPeripherals.Appliance7;
                this.appliance8 = outputPeripherals & OpPeripherals.Appliance8;
                this.appliance9 = outputPeripherals & OpPeripherals.Appliance9;
                this.doors = outputPeripherals & OpPeripherals.Doors;
                this.lights = outputPeripherals & OpPeripherals.Lights;
                this.sounders = outputPeripherals & OpPeripherals.Sounders;
                this.spare2 = outputPeripherals & OpPeripherals.Spare2;
                this.spare3 = outputPeripherals & OpPeripherals.Spare3;
                this.spare4 = outputPeripherals & OpPeripherals.Spare4;
                this.standbySounder = outputPeripherals & OpPeripherals.StandbySounder;
            }
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public virtual void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg1>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public virtual string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg1>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg1 Copy()
        {
            var copy = new GD92Msg1();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.OpPeripherals = this.OpPeripherals;
            return copy;
        }
        
        /// <summary>
        /// Needed to conform with IGD92Message.
        /// Inherited by GD92Msg40.
        /// </summary>
        public virtual void Validate()
        {
            this.ValidateAddresses();
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Thales.KentFire.GD92
{
    [XmlRootAttribute()]
    public class GD92Msg6AttReadOnly : GD92Msg6Attendance
    {
        private readonly Collection<string> m_Callsigns;
        private readonly string m_Address;

        /// <summary>
        /// Default constructor (added for XML serialization)
        /// </summary>
        public GD92Msg6AttReadOnly()
        {
        	
        }
        public GD92Msg6AttReadOnly(Collection<string> callsigns, string address)
        {
            m_Callsigns = callsigns;
            m_Address = address;
        }

        [XmlArray()]
        public override Collection<string> Callsigns
        {
            get { return m_Callsigns; }
            //set { throw new MessageAccessException("Callsigns"); }
        }
        [XmlElement()]
        public override string Address
        {
            get { return m_Address; }
            //set { throw new MessageAccessException("Address"); }
        }
        
    }
}

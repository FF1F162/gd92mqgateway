﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
namespace Thales.KFRS.GD92
{
    public interface IGD92Msg51 : IGD92Message
    {
        GD92Message AcknowledgedMessage { get; }
        GD92MsgType AcknowledgedMsgType { get; }
        GD92_REASON_CODE_SET ReasonCodeSet { get; set; }
        int ReasonCode { get; set; }
        string Reason { get; }
    }
}

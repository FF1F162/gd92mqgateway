﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// GD92 address equivalent to GD92Lib.PortAddress (which can be cast to GD92Address).
    /// Used to re-address messages across the gateway and for logging as XML. 
    /// XML serialization.deserialization requires settable properties.
    /// </summary>
    [XmlRootAttribute]
    public struct GD92Address : IEquatable<GD92Address> 
    {
        private int brigade;
        private int node;
        private int port;

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Address"/> struct.
        /// </summary>
        /// <param name="brigade">Brigade number.</param>
        /// <param name="node">Node number.</param>
        /// <param name="port">Port number.</param>
        public GD92Address(int brigade, int node, int port)
        {
            this.brigade = GD92Message.ValidateBrigade("Brigade", brigade);
            this.node = GD92Message.ValidateNode("Node", node);
            this.port = GD92Message.ValidatePort("Port", port);
        }

        /// <summary>
        /// Gets or sets Brigade number.
        /// </summary>
        /// <value>Brigade number.</value>
        [XmlElement]
        public int Brigade
        {
            get { return this.brigade; }
            set { this.brigade = GD92Message.ValidateBrigade("Brigade", value); }
       }

        /// <summary>
        /// Gets or sets Node number.
        /// </summary>
        /// <value>Node number.</value>
        [XmlElement]
        public int Node
        {
            get { return this.node; }
            set { this.node = GD92Message.ValidateNode("Node", value); }
        }

        /// <summary>
        /// Gets or sets Port number.
        /// </summary>
        /// <value>Port number.</value>
        [XmlElement]
        public int Port
        {
            get { return this.port; }
            set { this.port = GD92Message.ValidatePort("Port", value); }
        }

        /// <summary>
        /// Implement == operator to avoid boxing.
        /// </summary>
        /// <param name="first">GD92Address.</param>
        /// <param name="second">GD92Address to compare.</param>
        /// <returns>True if all fields are the same.</returns>
        public static bool operator ==(GD92Address first, GD92Address second)
        {
            return first.Equals(second);
        }

        /// <summary>
        /// Implement != operator to avoid boxing.
        /// </summary>
        /// <param name="first">GD92Address.</param>
        /// <param name="second">GD92Address to compare.</param>
        /// <returns>True if not all fields are the same.</returns>
        public static bool operator !=(GD92Address first, GD92Address second)
        {
            return !first.Equals(second);
        }

        /// <summary>
        /// Override Equals(object) as part of IEquatableT. See also PortAddress and NodeAddress.
        /// Check for null and different type then cast to type and call the type-specific Equals method.
        /// </summary>
        /// <param name="obj">Object to be compared with this.</param>
        /// <returns>True if the object equals this.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            var address = (GD92Address)obj;
            return this.Equals(address);
        }

        /// <summary>
        /// Override Equals(type) as part of IEquatableT. See also PortAddress and NodeAddress.
        /// Check all fields are the same.
        /// </summary>
        /// <param name="other">A GD92Address.</param>
        /// <returns>True if all fields are the same.</returns>
        public bool Equals(GD92Address other)
        {
            return (this.brigade == other.brigade) && (this.node == other.node) && (this.port == other.port);
        }

        /// <summary>
        /// Brigade is always the same and there is a small number of values for Port.
        /// There is no justification for anything more complicated.
        /// </summary>
        /// <returns>Value of Node field.</returns>
        public override int GetHashCode()
        {
            return this.Node;
        }

        /// <summary>
        /// Check the properties are within limits allowed by GD-92.
        /// </summary>
        public void Validate()
        {
            GD92Message.ValidateBrigade("Brigade", this.Brigade);
            GD92Message.ValidateNode("Node", this.Node);
            GD92Message.ValidatePort("Port", this.Port);
        }

        /// <summary>
        /// Brigade_Node_Port distinct from PortAddress and NodeAdress which use space separator.
        /// </summary>
        /// <returns>Brigade_Node_Port.</returns>
        public override string ToString()
        {
            return string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "{0}_{1}_{2}",
                this.brigade.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.node.ToString(System.Globalization.CultureInfo.InvariantCulture),
                this.port.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}

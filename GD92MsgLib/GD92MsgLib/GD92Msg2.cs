﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Collections.ObjectModel;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 2 Mobilise Message.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg2 : GD92Message, IGD92Message
    {
        private readonly Collection<string> callsignList = new Collection<string>();
        private DateTime mobTimeAndDate;
        private string incidentNumber = string.Empty;
        private int mobilisationType = (int)GD92MobilisationType.NotDefined;
        private string address = string.Empty;
        private string mapRef = string.Empty;
        private string telNumber = string.Empty;
        private string text = string.Empty;

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg2"/> class.
        /// </summary>
        public GD92Msg2()
        {
            this.MsgType = GD92MsgType.MobiliseMessage;
            this.Header = new GD92Header(this.MsgType);
            this.HouseNumber = string.Empty;
            this.Street = string.Empty;
            this.SubDistrict = string.Empty;
            this.District = string.Empty;
            this.Town = string.Empty;
            this.County = string.Empty;
            this.Postcode = string.Empty;
        }

        /// <summary>
        /// Get or set MobTimeAndDate.
        /// </summary>
        /// <value>MobTimeAndDate field.</value>
        [XmlElement]
        public DateTime MobTimeAndDate
        {
            get
            {
                return this.mobTimeAndDate;
            }

            set
            {
                this.CheckReadOnly("MobTimeAndDate");
                this.mobTimeAndDate = value;
            }
        }

        /// <summary>
        /// Get Callsign list.
        /// </summary>
        /// <value>Callsign list.</value>
        [XmlArray]
        public Collection<string> CallsignList
        {
            get { return this.callsignList; }
        }

        /// <summary>
        /// Get or set incident number.
        /// </summary>
        /// <value>Incident number.</value>
        [XmlElement]
        public string IncidentNumber
        {
            get
            {
                return this.incidentNumber;
            }

            set
            {
                this.CheckReadOnly("IncidentNumber");
                this.incidentNumber = value;
            }
        }

        /// <summary>
        /// Get or set mobilisation type.
        /// </summary>
        /// <value>Mobilisation type.</value>
        [XmlElement]
        public int MobilisationType
        {
            get
            {
                return this.mobilisationType;
            }

            set
            {
                this.CheckReadOnly("MobilisationType");
                this.mobilisationType = value;
            }
        }
        
        /// <summary>
        /// Get or set address. Storm seems to use this for organisation name etc.
        /// This corresponds to address_text in GD-92.
        /// Except for postcode the address element fields are never used in MOBS but there is a byte 0 for each 
        /// in the encoded message. Initially (Oct 2014) these fields were missing in Storm.
        /// </summary>
        /// <value>Address (first part).</value>
        [XmlElement]
        public string Address
        {
            get
            {
                return this.address;
            }

            set
            {
                this.CheckReadOnly("Address");
                this.address = value;
            }
        }

        /// <summary>
        /// Get or set House Number.
        /// This is always empty in MOBS but used in Storm.
        /// </summary>
        /// <value>House number element.</value>
        [XmlElement]
        public string HouseNumber { get; set; }

        /// <summary>
        /// Get or set Street.
        /// This is always empty in MOBS but used in Storm.
        /// </summary>
        /// <value>Street element.</value>
        [XmlElement]
        public string Street { get; set; }

        /// <summary>
        /// Get or set Sub District.
        /// This is always empty in MOBS but used in Storm.
        /// </summary>
        /// <value>Sub district element.</value>
        [XmlElement]
        public string SubDistrict { get; set; }

        /// <summary>
        /// Get or set District.
        /// This is always empty in MOBS but used in Storm.
        /// </summary>
        /// <value>District element.</value>
        [XmlElement]
        public string District { get; set; }

        /// <summary>
        /// Get or set Town.
        /// This is always empty in MOBS but used in Storm.
        /// </summary>
        /// <value>Town element.</value>
        [XmlElement]
        public string Town { get; set; }

        /// <summary>
        /// Get or set County.
        /// This is always empty in MOBS but used in Storm.
        /// </summary>
        /// <value>County element.</value>
        [XmlElement]
        public string County { get; set; }

        /// <summary>
        /// Get or set post code.
        /// </summary>
        /// <value>Post code element.</value>
        [XmlElement]
        public string Postcode { get; set; }

        /// <summary>
        /// This corresponds to map_ref in GD-92.
        /// </summary>
        /// <value>OS grid reference.</value>
        [XmlElement]
        public string MapRef
        {
            get
            {
                return this.mapRef;
            }

            set
            {
                this.CheckReadOnly("MapRef");
                this.mapRef = value;
            }
        }

        /// <summary>
        /// Get or set telephone number.
        /// </summary>
        /// <value>Telephone number.</value>
        [XmlElement]
        public string TelNumber
        {
            get
            {
                return this.telNumber;
            }

            set
            {
                this.CheckReadOnly("TelNumber");
                this.telNumber = value;
            }
        }

        /// <summary>
        /// Get or set Text. This is a 'long compressed text' field like Msg6 location notices.
        /// This contains Storm Prompts.
        /// </summary>
        /// <value>Text field.</value>
        [XmlElement]
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.CheckReadOnly("Text");
                this.text = value;
            }
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg2>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg2>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg2 Copy()
        {
            var copy = new GD92Msg2();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.Address = this.Address;
            copy.MobTimeAndDate = this.MobTimeAndDate;
            foreach (string callsign in this.CallsignList)
            {
                copy.CallsignList.Add(callsign);
            }

            copy.IncidentNumber = this.IncidentNumber;
            copy.MobilisationType = this.MobilisationType;
            copy.MapRef = this.MapRef;
            copy.TelNumber = this.TelNumber;
            copy.Text = this.Text;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92
        /// Validate callsign list locally to conform with CA1002.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            this.ValidateCallsignList();
            GD92Message.ValidateIncidentNumber("IncidentNumber", this.IncidentNumber);
            GD92Message.ValidateMobilisationType("MobilisationType", this.MobilisationType);
            GD92Message.ValidateByLength("Address", this.Address, GD92Message.MaxAddressLength);
            GD92Message.ValidateByLength("MapRef", this.MapRef, GD92Message.MaxMapRefLength);
            GD92Message.ValidateByLength("TelNumber", this.TelNumber, GD92Message.MaxTelNumberLength);
            GD92Message.ValidateByLength("Text", this.Text, GD92Message.MaxTextLength);
        }

        /// <summary>
        /// Check each callsign is within GD-92 limits.
        /// </summary>
        private void ValidateCallsignList()
        {
            foreach (string callsign in this.CallsignList)
            {
                GD92Message.ValidateCallsign("CallsignList", callsign);
            }
        }
    }
}

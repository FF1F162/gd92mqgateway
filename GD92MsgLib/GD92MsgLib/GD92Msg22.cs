﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 22 Log Update.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg22 : GD92Message, IGD92Message
    {
        private string callsign = string.Empty;
        private string incidentNumber = string.Empty;
        private string update = string.Empty;

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg22"/> class.
        /// </summary>
        public GD92Msg22()
        {
            this.MsgType = GD92MsgType.LogUpdate;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// Get or set Callsign.
        /// </summary>
        /// <value>Callsign field.</value>
        [XmlElement]
        public string Callsign
        {
            get
            {
                return this.callsign;
            }

            set
            {
                this.CheckReadOnly("Callsign");
                this.callsign = value;
            }
        }

        /// <summary>
        /// Get or set Incident Number.
        /// </summary>
        /// <value>Incident Number field.</value>
        [XmlElement]
        public string IncidentNumber
        {
            get
            {
                return this.incidentNumber;
            }

            set
            {
                this.CheckReadOnly("IncidentNumber");
                this.incidentNumber = value;
            }
        }

        /// <summary>
        /// Get or set Update message.
        /// </summary>
        /// <value>Update field.</value>
        [XmlElement]
        public string Update
        {
            get
            {
                return this.update;
            }

            set
            {
                this.CheckReadOnly("Update");
                this.update = value;
            }
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg22>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg22>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg22 Copy()
        {
            var copy = new GD92Msg22();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.Callsign = this.Callsign;
            copy.IncidentNumber = this.IncidentNumber;
            copy.Update = this.Update;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            GD92Message.ValidateCallsign("Callsign", this.Callsign);
            GD92Message.ValidateIncidentNumber("IncidentNumber", this.IncidentNumber);
            GD92Message.ValidateByLength("Update", this.Update, GD92Message.MaxUpdateLength);
        }
    }
}

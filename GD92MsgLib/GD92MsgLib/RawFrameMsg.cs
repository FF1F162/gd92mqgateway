﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 255 RawFrameMsg.
    /// This is used for test purposes to send a raw frame, possibly with
    /// incorrect address information etc.
    /// </summary>
    [XmlRootAttribute]
    public class RawFrameMsg : GD92Message, IGD92Message
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="RawFrameMsg"/> class.
        /// </summary>
        public RawFrameMsg()
        {
            this.MsgType = GD92MsgType.RawFrame;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// This is the entire binary block: SOH header message bcc EOT
        /// as logged in BitConverter format (01-16-AF-...-04).
        /// </summary>
        /// <value>String representing byte array for entire frame.</value>
        [XmlElement]
        public string LogOfFrameBytes { get; set; }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<RawFrameMsg>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<RawFrameMsg>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public RawFrameMsg Copy()
        {
            var copy = new RawFrameMsg();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.LogOfFrameBytes = this.LogOfFrameBytes;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
        }
    }
}

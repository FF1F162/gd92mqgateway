﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 40 Alert Crew inherits from Message 1.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg40 : GD92Msg1, IGD92Message
    {
        /// <summary>
        /// This is encoded as a fixed length string
        /// Initialise with a string of the correct length (spaces serialize as empty).
        /// </summary>
        private string alertGroup = "xx";

        /// <summary>
        ///  Initialises a new instance of the <see cref="GD92Msg40"/> class.
        /// </summary>
        public GD92Msg40()
        {
            this.MsgType = GD92MsgType.AlertCrew;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// Get or set alert group.
        /// </summary>
        /// <value>Alert group field.</value>
        [XmlElement]
        public string AlertGroup
        {
            get 
            { 
                return this.alertGroup; 
            }

            set
            {
                this.CheckReadOnly("AlertGroup");
                this.alertGroup = value;
            }
        }

        /// <summary>
        /// Check Alert Group is within limits set by GD-92.
        /// </summary>
        /// <param name="alertGroup">Alert group.</param>
        public static void ValidateAlertGroup(string alertGroup)
        {
            if (alertGroup.Length != GD92Message.AlertGroupLength)
            {
                throw new ArgumentOutOfRangeException("alertGroup",
                    "AlertGroup - 2 characters expected");
            }

            char[] chars = alertGroup.ToCharArray();
            if (chars[0] != 'F' && chars[0] != 'T' && chars[0] != ' ')
            {
                throw new ArgumentOutOfRangeException("alertGroup",
                    "AlertGroup - first character not F or T or space");
            }

            if (chars[1] != 'A' && chars[1] != 'B' && chars[1] != 'C' && chars[1] != 'D' &&
                chars[1] != 'E' && chars[1] != 'F' && chars[1] != 'G' && chars[1] != ' ')
            {
                throw new ArgumentOutOfRangeException("alertGroup",
                    "AlertGroup - second character not A, B, C, D, E, F, G or space");
            }
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public new GD92Msg40 Copy()
        {
            var copy = new GD92Msg40();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.OpPeripherals = this.OpPeripherals;
            copy.AlertGroup = this.AlertGroup;
            return copy;
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public override void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg40>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public override string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg40>(this);
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public override void Validate()
        {
            base.Validate();
            ValidateAlertGroup(this.AlertGroup);
        }
    }
}

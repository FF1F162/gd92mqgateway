﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 20 Resource Status.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg20 : GD92Message, IGD92Message
    {
        private string callsign = string.Empty;
        private string avlData = string.Empty;
        private string remarks = string.Empty;
        private int avlType = 1;                              // default is appropriate for MDT
        private int statusCode;                               // 0 means 'No status data'

        private int error;                                    // set in message from MOBS
        private int riders;
        private string easting = "0";
        private string northing = "0";
        private string incident = string.Empty;               // set in message from MOBS
        private string location = string.Empty;
        private string homeStation = string.Empty;            // set in message from MOBS
        private string alternateLabel = string.Empty;
        private bool confirm;                                 // false in message from MOBS
        private int travelTime;
        private string destE = "0";
        private string destN = "0";

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg20"/> class.
        /// </summary>
        public GD92Msg20()
        {
            this.MsgType = GD92MsgType.ResourceStatus;
            this.Header = new GD92Header(this.MsgType);
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg20"/> class.
        /// Constructor used when this message is sent in reply to Message 5 (or to Msg20 in Storm emulator).
        /// </summary>
        /// <param name="requestMessage">Reference to Msg5.</param>
        public GD92Msg20(GD92Message requestMessage) : this()
        {
            var test = requestMessage as GD92Msg5;
            var test2 = requestMessage as GD92Msg20;
            System.Diagnostics.Debug.Assert(test != null || test2 != null, "The original message must be Type 5 or Type 20.");
            this.OriginalMessage = requestMessage;
        }

        #region Message elements

        /// <summary>
        /// Get or set AVL type.
        /// </summary>
        /// <value>AVL type field.</value>
        [XmlElement]
        public int AvlType
        {
            get
            {
                return this.avlType;
            }

            set
            {
                this.CheckReadOnly("AvlType");
                this.avlType = value;
            }
        }

        /// <summary>
        /// Get or set status code.
        /// </summary>
        /// <value>Status code field.</value>
        [XmlElement]
        public int StatusCode
        {
            get
            {
                return this.statusCode;
            }

            set
            {
                this.CheckReadOnly("StatusCode");
                this.statusCode = value;
            }
        }

        /// <summary>
        /// Get or set callsign.
        /// </summary>
        /// <value>Callsign field.</value>
        [XmlElement]
        public string Callsign 
        {
            get
            {
                return this.callsign;
            }

            set
            {
                this.CheckReadOnly("Callsign");
                this.callsign = value;
            }
        }

        /// <summary>
        /// Get or set AVL data.
        /// </summary>
        /// <value>AVL data field.</value>
        [XmlElement]
        public string AvlData
        {
            get
            {
                return this.avlData;
            }

            set
            {
                this.CheckReadOnly("AvlData");
                this.avlData = value;
            }
        }

        /// <summary>
        /// Get or set remarks.
        /// </summary>
        /// <value>Remarks field.</value>
        /// [XmlElement]
        public string Remarks
        {
            get
            {
                return this.remarks;
            }

            set
            {
                this.CheckReadOnly("Remarks");
                this.remarks = value;
            }
        }

        #endregion

        #region Data in Remarks or AvlData field

        /// <summary>
        /// Get or set number of riders.
        /// </summary>
        /// <value>Riders data.</value>
        [XmlIgnore]
        public int Riders
        {
            get
            {
                return this.riders;
            }

            set
            {
                this.CheckReadOnly("Riders");
                this.riders = value;
            }
        }

        /// <summary>
        /// Get or set error code.
        /// </summary>
        /// <value>Error data.</value>
        [XmlIgnore]
        public int Error
        {
            get
            {
                return this.error;
            }

            set
            {
                this.CheckReadOnly("Error");
                this.error = value;
            }
        }

        /// <summary>
        /// Get or set Easting.
        /// </summary>
        /// <value>Easting data.</value>
        [XmlIgnore]
        public string Easting
        {
            get
            {
                return this.easting;
            }

            set
            {
                this.CheckReadOnly("Easting");
                this.easting = value;
            }
        }

        /// <summary>
        /// Get or set Northing.
        /// </summary>
        /// <value>Northing data.</value>
        [XmlIgnore]
        public string Northing
        {
            get
            {
                return this.northing;
            }

            set
            {
                this.CheckReadOnly("Northing");
                this.northing = value;
            }
        }

        /// <summary>
        /// Get or set incident number.
        /// </summary>
        /// <value>Incident number data.</value>
        [XmlIgnore]
        public string Incident
        {
            get
            {
                return this.incident;
            }

            set
            {
                this.CheckReadOnly("Incident");
                this.incident = value;
            }
        }

        /// <summary>
        /// Get or set location.
        /// </summary>
        /// <value>Location data.</value>
        [XmlIgnore]
        public string Location
        {
            get
            {
                return this.location;
            }

            set
            {
                this.CheckReadOnly("Location");
                this.location = value;
            }
        }

        /// <summary>
        /// Get or set Home Station.
        /// </summary>
        /// <value>Mone station data.</value>
        [XmlIgnore]
        public string HomeStation
        {
            get
            {
                return this.homeStation;
            }

            set
            {
                this.CheckReadOnly("HomeStation");
                this.homeStation = value;
            }
        }

        /// <summary>
        /// Confirm indication.
        /// </summary>
        /// <value>Confirm data.</value>
        [XmlIgnore]
        public bool Confirm
        {
            get
            {
                return this.confirm;
            }

            set
            {
                this.CheckReadOnly("Confirm");
                this.confirm = value;
            }
        }

        /// <summary>
        /// Alternate label.
        /// </summary>
        /// <value>Alternate data.</value>
        [XmlIgnore]
        public string AlternateLabel
        {
            get
            {
                return this.alternateLabel;
            }

            set
            {
                this.CheckReadOnly("AlternateLabel");
                this.alternateLabel = value;
            }
        }

        /// <summary>
        /// Travel time.
        /// </summary>
        /// <value>Travel time data.</value>
        [XmlIgnore]
        public int TravelTime
        {
            get
            {
                return this.travelTime;
            }

            set
            {
                this.CheckReadOnly("TravelTime");
                this.travelTime = value;
            }
        }

        /// <summary>
        /// Destination easting.
        /// </summary>
        /// <value>Destination east data.</value>
        [XmlIgnore]
        public string DestE
        {
            get
            {
                return this.destE;
            }

            set
            {
                this.CheckReadOnly("DestE");
                this.destE = value;
            }
        }

        /// <summary>
        /// Destination northing.
        /// </summary>
        /// <value>Destination north data.</value>
        [XmlIgnore]
        public string DestN
        {
            get
            {
                return this.destN;
            }

            set
            {
                this.CheckReadOnly("DestN");
                this.destN = value;
            }
        }

        #endregion

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg20>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg20>(this);
        }

        /// <summary>
        /// Copy fields to a new instance (when this is not the original message received).
        /// </summary>
        /// <param name="gatewayOriginal">Needed because this message being copied is not the one originally received.</param>
        /// <returns>New instance with copied fields and GatewayOriginal as specified.</returns>
        public GD92Msg20 CopyAndSetGatewayOriginal(GD92Message gatewayOriginal)
        {
            var copy = this.CopyWithoutSettingGatewayOriginal();
            copy.GatewayOriginal = gatewayOriginal;
            return copy;
        }

        /// <summary>
        /// Copy fields to a new instance (when this is the original message received - not a copy).
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg20 Copy()
        {
            var copy = this.CopyWithoutSettingGatewayOriginal();
            copy.GatewayOriginal = this;
            return copy;
        }

        /// <summary>
        /// Copy fields to a new instance (but do not set the GatewayOriginal field).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg20 CopyWithoutSettingGatewayOriginal()
        {
            var copy = new GD92Msg20();
            copy.CopySourceDestManAckFrom(this);
            copy.Callsign = this.Callsign;
            copy.AvlType = this.AvlType;
            copy.AvlData = this.AvlData;
            copy.Remarks = this.Remarks;
            copy.StatusCode = this.StatusCode;
            return copy;
        }

        /// <summary>
        /// Copy message fields (when the gateway is forwarding a reply to Msg5).
        /// </summary>
        /// <param name="target">Instance to copy fields to.</param>
        public void CopyFieldsTo(GD92Msg20 target)
        {
            target.Callsign = this.Callsign;
            target.AvlType = this.AvlType;
            target.AvlData = this.AvlData;
            target.Remarks = this.Remarks;
            target.StatusCode = this.StatusCode;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// There is no validation for Error, Incident, HomeStation, Confirm.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            GD92Message.ValidateByRange("AvlType", this.AvlType, GD92Message.MaxAvlType);
            GD92Message.ValidateByRange("StatusCode", this.StatusCode, GD92Message.MaxStatusCode);
            GD92Message.ValidateCallsign("Callsign", this.Callsign);
            GD92Message.ValidateByLength("AvlData", this.AvlData, GD92Message.MaxAvlDataLength);
            GD92Message.ValidateByLength("Remarks", this.Remarks, GD92Message.MaxRemarksLength);

            GD92Message.ValidateByRange("Riders", this.Riders, GD92Message.MaxRiders);
            GD92Message.ValidateEorN("Easting", this.Easting);
            GD92Message.ValidateEorN("Northing", this.Northing);
            GD92Message.ValidateByLength("Location", this.Location, GD92Message.MaxLocationLength);
            GD92Message.ValidateByLength("AlternateLabel", this.AlternateLabel, GD92Message.MaxAlternateLabelLength);
            GD92Message.ValidateByRange("TravelTime", this.TravelTime, GD92Message.MaxTravelTime);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
namespace Thales.KFRS.GD92
{
    public interface IGD92Msg20 : IGD92Message
    {
        string AvlData { get; set; }
        int AvlType { get; set; }
        string Callsign { get; set; }
        string Remarks { get; set; }
        int StatusCode { get; set; }
    }
}

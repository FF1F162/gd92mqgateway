﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Message 42 Alert Status.
    /// </summary>
    [XmlRootAttribute]
    public class GD92Msg42 : GD92Message, IGD92Message
    {
        /// <summary>
        /// This is encoded as a fixed length string
        /// Initialise with a string of the correct length (spaces serialize as empty).
        /// </summary>
        private string alerterStatus = "xx";

        public GD92Msg42()
        {
            this.MsgType = GD92MsgType.AlertStatus;
            this.Header = new GD92Header(this.MsgType);
        }
        
        /// <summary>
        /// Get or set alerter status.
        /// </summary>
        /// <value>Alerter status.</value>
        [XmlElement]
        public string AlerterStatus
        {
            get
            {
                return this.alerterStatus;
            }

            set
            {
                this.CheckReadOnly("Status");
                this.alerterStatus = value;
            }
        }

        /// <summary>
        /// Check Alerter Status is within limits set by GD-92.
        /// MG-4 Reverse protocol messages (that could presumably appear as status)
        /// are all lower case. But just pass what is provided unless there is a reason to filter
        /// (supposing validation is ever used).
        /// </summary>
        /// <param name="alerterStatus">Alerter status.</param>
        public static void ValidateAlerterStatus(string alerterStatus)
        {
            if (alerterStatus.Length != GD92Message.AlertStatusLength)
            {
                throw new ArgumentOutOfRangeException("alerterStatus",
                    "Alerter status - 2 characters expected");
            }
        }

        /// <summary>
        /// Serialize this message to file.
        /// </summary>
        /// <param name="fileName">Path to file.</param>
        public void ToXml(string fileName)
        {
            GD92Message.SaveToFile<GD92Msg42>(this, fileName);
        }

        /// <summary>
        /// Serialize this message to string.
        /// </summary>
        /// <returns>XML representation of the message.</returns>
        public string ToXml()
        {
            return GD92Message.SaveToMemory<GD92Msg42>(this);
        }

        /// <summary>
        /// Copy fields to a new instance
        /// (Message handling data - serial number and times - are not copied).
        /// </summary>
        /// <returns>New instance with copied fields.</returns>
        public GD92Msg42 Copy()
        {
            var copy = new GD92Msg42();
            copy.CopySourceDestManAckAndOriginalFrom(this);
            copy.alerterStatus = this.alerterStatus;
            return copy;
        }

        /// <summary>
        /// Check message data are within limits set by GD-92.
        /// </summary>
        public void Validate()
        {
            this.ValidateAddresses();
            ValidateAlerterStatus(this.AlerterStatus);
        }
    }
}

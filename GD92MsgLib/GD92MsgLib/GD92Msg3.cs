﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Collections.Generic;
using System.Text;

namespace Thales.KFRS.GD92
{
    [Serializable]
    public class GD92Msg3 : GD92Message
    {
        private string m_PagerNumber = string.Empty;
        private string m_PagerText = string.Empty;
        private GD92PagerType m_PagerType = GD92PagerType.Alphanumeric;

        public GD92Msg3()
        {
            m_Type = GD92MsgType.PageOfficer;
        }

        public string PagerNumber
        {
            get { return m_PagerNumber; }
            set
            {
                CheckReadOnly("PagerNumber");
                m_PagerNumber = ValidateString("PagerNumber", value, MaxPagerNumberLength);
            }
        }
        public string PagerText
        {
            get { return m_PagerText; }
            set
            {
                CheckReadOnly("PagerText");
                m_PagerText = ValidateString("PagerText", value, MaxPagerTextLength);
            }
        }
        public GD92PagerType PagerType
        {
            get { return m_PagerType; }
            set
            {
                CheckReadOnly("PagerType");
                m_PagerType = value;
            }
        }
    }
}

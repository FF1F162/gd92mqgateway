﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.GD92
{
    using System;
    using System.Xml.Serialization;

    [XmlRootAttribute]
    public class GD92Msg6Resource
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg6Resource"/> class.
        /// </summary>
        public GD92Msg6Resource()
        {
            this.Callsign = string.Empty;
            this.Commentary = string.Empty;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="GD92Msg6Resource"/> class.
        /// Create instance of <see cref="GD92Msg6Resource"/> with data supplied
        /// The original intention was to make the fields read-only but then they do not serialize.
        /// </summary>
        /// <param name="callsign">Callsign name.</param>
        /// <param name="commentary">Commentary e.g. associated resources.</param>
        public GD92Msg6Resource(string callsign, string commentary)
        {
            this.Callsign = callsign;
            this.Commentary = commentary;
        }

        /// <summary>
        /// Get or set callsign being mobilised.
        /// </summary>
        /// <value>Callsign name.</value>
        [XmlElement]
        public string Callsign { get; set; }

        /// <summary>
        /// Commentary from the operator doing the mobilisation or resources associated.
        /// </summary>
        /// <value>Resource description or message associated with the callsign.</value>
        [XmlElement]
        public string Commentary { get; set; }

        /// <summary>
        /// Populate a new instance with references to (immutable) strings in this instance.
        /// </summary>
        /// <returns>New instance with same fields as this.</returns>
        public GD92Msg6Resource Clone()
        {
            return new GD92Msg6Resource(this.Callsign, this.Commentary);
        }

        /// <summary>
        /// Check the length of the fields.
        /// </summary>
        /// <param name="fieldName">Name of the collection field containing the object.</param>
        public void Validate(string fieldName)
        {
            GD92Message.ValidateCallsign(fieldName + " Callsign", this.Callsign);
            GD92Message.ValidateByLength(fieldName + " Commentary", this.Commentary, GD92Message.MaxCommentaryLength);
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg51Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg51XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg51XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg51>
  ";
   			EmptyMessageAsXml = Msg51XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>Nak</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""1"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <OriginalDestination>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </OriginalDestination>
  <ReasonCodeSet>0</ReasonCodeSet>
  <ReasonCode>0</ReasonCode>
</GD92Msg51>";

		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg51ConstructorTest()
		{
			var msg = new GD92Msg51();
			Assert.AreEqual(GD92MsgType.Nak, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg51_ToXml_Empty_Test()
		{
			var msg = new GD92Msg51();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg51_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Set ReadOnly and check serialization still works
		/// </summary>
		[Test]
		public void GD92Msg51_ToXml_ReadOnly_Test()
		{
			var msg = new GD92Msg51();
			msg.SetReadOnly();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg51_readonly.txt"));
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg51_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg51 msg = GD92Message.ReadFromFile<GD92Msg51>(Path.Combine(TestOutputPath, "Msg51_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
	}
}

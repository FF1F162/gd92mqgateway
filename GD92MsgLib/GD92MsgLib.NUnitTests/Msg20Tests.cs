﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg20Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg20XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg20XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg20>
  ";
  			EmptyMessageAsXml = Msg20XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>ResourceStatus</MsgType>
  <Header ProtocolVersion=""1"" Priority=""3"" StandardPort=""1"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <AvlType>1</AvlType>
  <StatusCode>0</StatusCode>
  <Callsign />
  <AvlData />
  <Remarks />
</GD92Msg20>";
  
		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg20ConstructorTest()
		{
			var msg = new GD92Msg20();
			Assert.AreEqual(GD92MsgType.ResourceStatus, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg20_ToXml_Empty_Test()
		{
			var msg = new GD92Msg20();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg20_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg20_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg20 msg = GD92Message.ReadFromFile<GD92Msg20>(Path.Combine(TestOutputPath, "Msg20_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of a message sent from kentres and logged (original RSC test data)
		/// </summary>
		[Test]
		public void GD92Msg20_ReadFromFile_toMDG1_Test()
		{
			GD92Msg20 msg = GD92Message.ReadFromFile<GD92Msg20>(Path.Combine(TestInputPath, "toMDG1_20.txt"));
		}
	}
}

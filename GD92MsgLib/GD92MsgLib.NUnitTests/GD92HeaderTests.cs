﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.Configuration;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
    public class GD92HeaderTests
    {
        public string TestInputPath;
        public string TestOutputPath;
        public string GD92HeaderXmlHeader;

        [SetUp]
        public void init()
        {
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            GD92HeaderXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Header";
        }

        /// <summary>
        /// Test that the constructor sets the message type
        /// </summary>
        [Test]
        public void GD92HeaderConstructorTest()
        {
            var msg = new GD92Header();
            Assert.AreEqual(0, msg.Priority);
            Assert.AreEqual(0, msg.ProtocolVersion);
            Assert.AreEqual(0, msg.StandardPort);
        }
        /// <summary>
        /// Examine the content of an empty message
        /// </summary>
        [Test]
        public void GD92Header_SaveToString_Empty_Test()
        {
            var msg = new GD92Header();
            GD92Message.SaveToFile<GD92Header>(msg, Path.Combine(TestOutputPath, "GD92Header_empty.txt"));
            string expected = GD92HeaderXmlHeader + @" ProtocolVersion=""0"" Priority=""0"" StandardPort=""0"" />";
            string actual = GD92Message.SaveToMemory<GD92Header>(msg);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Examine the content of resource list
        /// </summary>
        [Test]
        public void GD92Header_SaveToString_Data_Test()
        {
            var msg = new GD92Header();
            msg.Priority = 1;
            msg.ProtocolVersion = 3;
            msg.StandardPort = 12;
            GD92Message.SaveToFile<GD92Header>(msg, Path.Combine(TestOutputPath, "GD92Header_data.txt"));
            string expected = GD92HeaderXmlHeader + @" ProtocolVersion=""3"" Priority=""1"" StandardPort=""12"" />";
            string actual = GD92Message.SaveToMemory<GD92Header>(msg);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Demonstrate correct details for type
        /// </summary>
        [Test]
        public void GD92HeaderForMsg1Test()
        {
            var msg = new GD92Header(GD92MsgType.MobiliseCommand);
            Assert.AreEqual(1, msg.ProtocolVersion);
            Assert.AreEqual(1, msg.Priority);
            Assert.AreEqual(12, msg.StandardPort);
        }
        /// <summary>
        /// Demonstrate correct details for type
        /// </summary>
        [Test]
        public void GD92HeaderForMsg40Test()
        {
            var msg = new GD92Header(GD92MsgType.AlertCrew);
            Assert.AreEqual(1, msg.ProtocolVersion);
            Assert.AreEqual(1, msg.Priority);
            Assert.AreEqual(13, msg.StandardPort);
        }
        /// <summary>
        /// Demonstrate correct details for type
        /// </summary>
        [Test]
        [ExpectedException("Thales.KentFire.GD92.MessageValidationException")]
        public void GD92HeaderForUnknownMessageTypeTest()
        {
            var msg = new GD92Header((GD92MsgType)99);
        }
    }
}

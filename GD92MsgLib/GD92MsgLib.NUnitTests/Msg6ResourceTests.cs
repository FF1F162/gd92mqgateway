﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.Configuration;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg6ResourceTests
	{
		public string TestInputPath;
        public string TestOutputPath;
        public string Msg6ResourceXmlHeader;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg6ResourceXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6Resource>
  ";
		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg6ResourceConstructorTest()
		{
			var msg = new GD92Msg6Resource();
			Assert.AreEqual(string.Empty, msg.Callsign);
            Assert.AreEqual(string.Empty, msg.Commentary);
        }
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg6Resource_SaveToString_Empty_Test()
		{
			var msg = new GD92Msg6Resource();
			GD92Message.SaveToFile<GD92Msg6Resource>(msg, Path.Combine(TestOutputPath, "Msg6Resource_empty.txt"));
            string expected = Msg6ResourceXmlHeader + @"<Callsign />
  <Commentary />
</GD92Msg6Resource>"; ;
			string actual = GD92Message.SaveToMemory<GD92Msg6Resource>(msg);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of resource list
		/// </summary>
		[Test]
		public void GD92Msg6Resource_SaveToString_Data_Test()
		{
			var msg = new GD92Msg6Resource();
			msg.Callsign = "FJK60P1";
			msg.Commentary = "Test commentary";
			GD92Message.SaveToFile<GD92Msg6Resource>(msg, Path.Combine(TestOutputPath, "Msg6Resource_data.txt"));
			string expected = Msg6ResourceXmlHeader + @"<Callsign>FJK60P1</Callsign>
  <Commentary>Test commentary</Commentary>
</GD92Msg6Resource>";
			string actual = GD92Message.SaveToMemory<GD92Msg6Resource>(msg);
			Assert.AreEqual(expected, actual);		
		}
	}
}

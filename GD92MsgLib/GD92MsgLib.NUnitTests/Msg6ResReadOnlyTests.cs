﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Configuration;
using System.IO;
using NUnit.Framework;
using Thales.KentFire.GD92;

namespace GD92MsgLib.NUnitTests
{
	[TestFixture]
	public class Msg6ResReadOnlyTests
	{
		public string TestOutputPath;
		public string Msg6ResourceXmlHeader;
		public string Msg6ResourceXmlHeaderEmpty;
		public string Msg6ResReadOnlyXmlHeader;
		public string Msg6ResReadOnlyXmlHeaderEmpty;
		
		[SetUp]
		public void init()
		{
		    TestOutputPath = ConfigurationManager.AppSettings["TestOutputFolder"];
		    Msg6ResourceXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6Resource xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  ";
  		    Msg6ResourceXmlHeaderEmpty = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6Resource xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" />";
		    Msg6ResReadOnlyXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6ResReadOnly xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  ";
  		    Msg6ResReadOnlyXmlHeaderEmpty = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6ResReadOnly xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" />";

		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg6ResReadOnlyConstructorTest()
		{
			var msg = new GD92Msg6ResReadOnly();
			Assert.IsNull(msg.Callsign);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg6ResReadOnly_SaveToString_Empty_Test()
		{
			var msg = new GD92Msg6ResReadOnly(null, null);
			GD92Message.SaveToFile<GD92Msg6ResReadOnly>(msg, Path.Combine(TestOutputPath, "Msg6ResReadOnly_empty.txt"));
			string expected = Msg6ResReadOnlyXmlHeaderEmpty;
			string actual = GD92Message.SaveToMemory<GD92Msg6ResReadOnly>(msg);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of a resource list
		/// </summary>
		[Test]
		public void GD92Msg6ResReadOnly_SaveToString_Data_Test()
		{
			var msg = new GD92Msg6ResReadOnly("FJK60P1", "Test commentary");
			GD92Message.SaveToFile<GD92Msg6ResReadOnly>(msg, Path.Combine(TestOutputPath, "Msg6ResReadOnly_data.txt"));
			string expected = Msg6ResReadOnlyXmlHeader + @"<Callsign>FJK60P1</Callsign>
  <Commentary>Test commentary</Commentary>
</GD92Msg6ResReadOnly>";
			string actual = GD92Message.SaveToMemory<GD92Msg6ResReadOnly>(msg);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Try to set the read-only Callsign field
		/// </summary>
		[Test]
		[ExpectedException("Thales.KentFire.GD92.MessageAccessException")]
		public void GD92Msg6ResReadOnly_CallsignFieldIsReadOnly_Test()
		{
			var msg = new GD92Msg6ResReadOnly("FJK60P1", "Test commentary");
			msg.Callsign = "Test";
		}
		/// <summary>
		/// Try to set the read-only Commentary field
		/// </summary>
		[Test]
		[ExpectedException("Thales.KentFire.GD92.MessageAccessException")]
		public void GD92Msg6ResReadOnly_CommentaryFieldIsReadOnly_Test()
		{
			var msg = new GD92Msg6ResReadOnly("FJK60P1", "");
			msg.Commentary = "Test";
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg50Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg50XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg50XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg50>
  ";
   			EmptyMessageAsXml = Msg50XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>Ack</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""1"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
</GD92Msg50>";

		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg50ConstructorTest()
		{
			var msg = new GD92Msg50();
			Assert.AreEqual(GD92MsgType.Ack, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg50_ToXml_Empty_Test()
		{
			var msg = new GD92Msg50();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg50_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Set ReadOnly and check serialization still works
		/// </summary>
		[Test]
		public void GD92Msg50_ToXml_ReadOnly_Test()
		{
			var msg = new GD92Msg50();
			msg.SetReadOnly();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg50_readonly.txt"));
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg50_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg50 msg = GD92Message.ReadFromFile<GD92Msg50>(Path.Combine(TestOutputPath, "Msg50_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
	}
}

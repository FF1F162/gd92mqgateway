﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
	using System;
	using NUnit.Framework;
	using Thales.KentFire.GD92;

	[TestFixture]
	public class GD92AddressTests
	{
		[Test]
		public void GD92AddressEmptyEqualityTest()
		{
			var empty = new GD92Address();
			var zero = new GD92Address(0, 0, 0);
			Assert.IsTrue(empty == zero);
		}
		[Test]
		public void GD92AddressEmptyEqualsTest()
		{
			var empty = new GD92Address();
			var zero = new GD92Address(0, 0, 0);
			Assert.IsTrue(empty.Equals(zero));
		}
		[Test]
		public void GD92AddressZeroEqualsTest()
		{
			var empty = new GD92Address();
			var zero = new GD92Address(0, 0, 0);
			Assert.IsTrue(zero.Equals(empty));
		}
		[Test]
		public void GD92AddressNormalEqualsTest()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 1;
			second.Port = 12;
			Assert.IsTrue(first.Equals(second));
		}
		[Test]
		public void GD92AddressMaxEqualsTest()
		{
			var first = new GD92Address(255, 1023, 63);
			var second = new GD92Address();
			second.Brigade = 255;
			second.Node = 1023;
			second.Port = 63;
			Assert.IsTrue(first.Equals(second));
		}
		
		[Test]
		public void GD92AddressPortInequalityTest1()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 1;
			second.Port = 13;
			Assert.IsFalse(first.Equals(second));
		}
		[Test]
		public void GD92AddressPortInequalityTest2()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 1;
			second.Port = 13;
			Assert.IsFalse(first == second);
		}
		[Test]
		public void GD92AddressPortInequalityTest3()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 1;
			second.Port = 13;
			Assert.IsFalse(second == first);
		}
		[Test]
		public void GD92AddressPortInequalityTest4()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 1;
			second.Port = 13;
			Assert.IsTrue(second != first);
		}
		[Test]
		public void GD92AddressPortInequalityTest5()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 1;
			second.Port = 13;
			Assert.IsTrue(first != second);
		}
		
		[Test]
		public void GD92AddressNodeInequalityTest1()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 3;
			second.Port = 12;
			Assert.IsFalse(first.Equals(second));
		}
		[Test]
		public void GD92AddressNodeInequalityTest2()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 0;
			second.Port = 12;
			Assert.IsFalse(first == second);
		}
		[Test]
		public void GD92AddressNodeInequalityTest3()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 999;
			second.Port = 12;
			Assert.IsFalse(second == first);
		}
		[Test]
		public void GD92AddressNodeInequalityTest4()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 405;
			second.Port = 12;
			Assert.IsTrue(second != first);
		}
		[Test]
		public void GD92AddressNodeInequalityTest5()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 405;
			second.Port = 12;
			Assert.IsTrue(first != second);
		}
		
		[Test]
		public void GD92AddressBrigadeInequalityTest()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 0;
			second.Node = 1;
			second.Port = 12;
			Assert.IsTrue(first != second);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void GD92AddressBrigadeIsInvalidTest1()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = -1;
			second.Node = 1;
			second.Port = 12;
			Assert.IsTrue(first != second);
		}
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void GD92AddressBrigadeIsInvalidTest2()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 256;
			second.Node = 1;
			second.Port = 12;
			Assert.IsTrue(first != second);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void GD92AddressNodeIsInvalidTest1()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = -1;
			second.Port = 12;
			Assert.IsTrue(first != second);
		}
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void GD92AddressNodeIsInvalidTest2()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 1024;
			second.Port = 12;
			Assert.IsTrue(first != second);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void GD92AddressPortIsInvalidTest1()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 2;
			second.Port = -1;
			Assert.IsTrue(first != second);
		}
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void GD92AddressPortIsInvalidTest2()
		{
			var first = new GD92Address(22, 1, 12);
			var second = new GD92Address();
			second.Brigade = 22;
			second.Node = 2;
			second.Port = 64;
			Assert.IsTrue(first != second);
		}

		[Test]
		public void GD92AddressValidateTest1()
		{
			var first = new GD92Address();
			first.Validate();
			Assert.IsTrue(true);
		}
		[Test]
		public void GD92AddressValidateTest2()
		{
			var first = new GD92Address(22, 1, 12);
			first.Validate();
			Assert.IsTrue(true);
		}
		[Test]
		public void GD92AddressToStringTest1()
		{
			var first = new GD92Address();
			string expected = "0_0_0";
			string actual = first.ToString();
			Assert.IsTrue(expected == actual);
		}
		[Test]
		public void GD92AddressToStringTest2()
		{
			var first = new GD92Address(22, 1, 12);
			string expected = "22_1_12";
			string actual = first.ToString();
			Assert.IsTrue(expected == actual);
		}
	}
}

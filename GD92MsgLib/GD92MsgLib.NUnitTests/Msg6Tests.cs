﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
	using System;
	using System.Collections.ObjectModel;
	using System.IO;
	using NUnit.Framework;
	using Thales.KentFire.GD92;
	
	[TestFixture]
	public class Msg6Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg6XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg6XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6>
  ";
   			EmptyMessageAsXml = Msg6XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>MobiliseMessage6</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""16"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <MobilisationType>255</MobilisationType>
  <AssistanceMessage />
  <ResourcesList />
  <AllResourcesList />
  <IncidentTimeAndDate>0001-01-01T00:00:00</IncidentTimeAndDate>
  <MobTimeAndDate>0001-01-01T00:00:00</MobTimeAndDate>
  <IncidentNumber />
  <IncidentType />
  <Address />
  <HouseNumber />
  <Street />
  <SubDistrict />
  <District />
  <Town />
  <County />
  <Postcode />
  <FirstAttendance>
    <Callsigns />
    <Address />
  </FirstAttendance>
  <SecondAttendance>
    <Callsigns />
    <Address />
  </SecondAttendance>
  <MapBook />
  <MapRef />
  <HowReceived />
  <TelNumber />
  <OperatorInfo />
  <WaterInformation />
  <LocationNotices />
  <AddText />
</GD92Msg6>";

		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg6ConstructorTest()
		{
			var msg = new GD92Msg6();
			Assert.AreEqual(GD92MsgType.MobiliseMessage6, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg6_ToXml_Empty_Test()
		{
			var msg = new GD92Msg6();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg6_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Set ReadOnly and check serialization still works
		/// </summary>
		[Test]
		public void GD92Msg6_ToXml_ReadOnly_Test()
		{
			var msg = new GD92Msg6();
			msg.SetReadOnly();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg6_readonly.txt"));
		}
		/// <summary>
		/// Set ReadOnly data and check serialization still works
		/// </summary>
		[Test]
		public void GD92Msg6_ToXml_ReadOnlyData_Test()
		{
			var callsignList = new Collection<string>();
			callsignList.Add("FJK60P1");
			var attendance = new GD92Msg6Attendance(callsignList, "Test address");
			var resource1 = new GD92Msg6Resource("FJK60P1", "Test commentary");
			var resource2 = new GD92Msg6Resource("FJK60R1", "");
			var msg = new GD92Msg6();
			msg.ResourcesList.Add(resource1);
			msg.AllResourcesList.Add(resource1);
			msg.AllResourcesList.Add(resource2);
			msg.FirstAttendance = attendance;
			msg.IncidentNumber = "99";
			msg.SetReadOnly();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg6_readonlydata.txt"));
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg6_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg6 msg = GD92Message.ReadFromFile<GD92Msg6>(Path.Combine(TestOutputPath, "Msg6_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of a message sent from kentres and logged (original RSC test data)
		/// </summary>
		[Test]
		public void GD92Msg6_ReadFromFile_toMDG1_Test()
		{
			GD92Msg6 msg = GD92Message.ReadFromFile<GD92Msg6>(Path.Combine(TestInputPath, "toMDG1_6.txt"));
			Assert.AreEqual("SOME COMMENTARY", msg.ResourcesList[0].Commentary);
		}
	}
}

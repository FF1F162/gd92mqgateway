﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
	using System;
	using System.Collections.ObjectModel;
	using System.Configuration;
	using System.IO;
	using NUnit.Framework;
	using Thales.KentFire.GD92;
	
	[TestFixture]
	public class Msg6AttReadOnlyTests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg6AttendanceXmlHeader;
		public string Msg6AttendanceXmlHeaderEmpty;
		public string Msg6AttReadOnlyXmlHeader;
		public string Msg6AttReadOnlyXmlHeaderEmpty;
		
		[SetUp]
		public void init()
		{
		    TestInputPath = ConfigurationManager.AppSettings["TestInputFolder"];
		    TestOutputPath = ConfigurationManager.AppSettings["TestOutputFolder"];
		    Msg6AttendanceXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6Attendance xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  ";
  		    Msg6AttendanceXmlHeaderEmpty = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6Attendance xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" />";
		    Msg6AttReadOnlyXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6AttReadOnly xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  ";
  		    Msg6AttReadOnlyXmlHeaderEmpty = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6AttReadOnly xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" />";

		}

		/// <summary>
		/// Test that the constructor exists
		/// </summary>
		[Test]
		public void Msg6AttReadOnlyDefaultConstructorTest()
		{
			var msg = new GD92Msg6AttReadOnly();
			Assert.IsNull(msg.Address);
			Assert.IsNull(msg.Callsigns);
		}
		/// <summary>
		/// Test that the constructor sets the adddress in the argument
		/// </summary>
		[Test]
		public void Msg6AttReadOnlyConstructorTest()
		{
			var callsigns = new Collection<string>( );
			string expected = String.Empty;
			var msg = new GD92Msg6AttReadOnly(callsigns, expected);
			string actual = msg.Address;		
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of an empty attendance list
		/// </summary>
		[Test]
		public void GD92Msg6AttReadOnly_SaveToString_Empty_Test()
		{
			var msg = new GD92Msg6AttReadOnly();
			GD92Message.SaveToFile<GD92Msg6AttReadOnly>(msg, Path.Combine(TestOutputPath, "Msg6AttReadOnly_empty.txt"));
			string expected = Msg6AttReadOnlyXmlHeaderEmpty;
			string actual = GD92Message.SaveToMemory<GD92Msg6AttReadOnly>(msg);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of an attendance list
		/// </summary>
		[Test]
		public void GD92Msg6AttReadOnly_SaveToString_Data_Test()
		{
			var callsigns = new Collection<string>( );
			callsigns.Add("FJK60P1");
			var msg = new GD92Msg6AttReadOnly(callsigns, "Test Address");
			GD92Message.SaveToFile<GD92Msg6AttReadOnly>(msg, Path.Combine(TestOutputPath, "Msg6AttReadOnly_data.txt"));
			string expected = Msg6AttReadOnlyXmlHeader + @"<Callsigns>
    <string>FJK60P1</string>
  </Callsigns>
  <Address>Test Address</Address>
</GD92Msg6AttReadOnly>";
			string actual = GD92Message.SaveToMemory<GD92Msg6AttReadOnly>(msg);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Try to set the read-only Callsigns field
		/// There is no setter - the assignment fails silently
		/// </summary>
		[Test]
		public void GD92Msg6AttReadOnly_CallsignsFieldIsReadOnly_Test()
		{
			var callsigns = new Collection<string>( );
			string expected = "FJK60P1";
			callsigns.Add(expected);
			var msg = new GD92Msg6AttReadOnly(callsigns, "Test Address");
			msg.Callsigns = new Collection<string>( );
			Assert.AreEqual(expected, msg.Callsigns[0]);
		}
		/// <summary>
		/// Unfortunately it is still possible to change a callsign
		/// </summary>
		[Test]
		public void GD92Msg6AttReadOnly_CallsignIsNOTReadOnly_Test()
		{
			var callsigns = new Collection<string>( );
			callsigns.Add("FJK60P1");
			var msg = new GD92Msg6AttReadOnly(callsigns, "Test Address");
			msg.Callsigns[0] = "Test";
			callsigns.Add("FJK60R1");
			Assert.AreEqual("Test", msg.Callsigns[0]);
			Assert.AreEqual("FJK60R1", msg.Callsigns[1]);
		}
		/// <summary>
		/// Try to set the read-only Address field
		/// There is no setter - the assignment fails silently
		/// XMLSerializer usually ignores a field with no setter, but Collection is an exception
		/// </summary>
		[Test]
		public void GD92Msg6AttReadOnly_AddressFieldIsReadOnly_Test()
		{
			var callsigns = new Collection<string>( );
			callsigns.Add("FJK60P1");
			var msg = new GD92Msg6AttReadOnly(callsigns, "Test Address");
			msg.Address = "Test";
			Assert.AreEqual("Test Address", msg.Address);
		}
		/// <summary>
		/// Downcast to XML-friendly version before saving
		/// The hope is that the output can be de-serialized
		/// This worked after adding XmlInclude attribute to the base class
		/// There is a reference to the derived class so deserialization may still be a problem
		/// </summary>
		[Test]
		public void GD92Msg6AttReadOnly_DowncastSaveToString_Test()
		{
			var callsigns = new Collection<string>( );
			callsigns.Add("FJK60P1");
			var msg = new GD92Msg6AttReadOnly(callsigns, "Test Address");
			var downcast = msg as GD92Msg6Attendance;
			GD92Message.SaveToFile<GD92Msg6Attendance>(downcast, Path.Combine(TestOutputPath, "Msg6AttReadOnly_downcast.txt"));
			string expected = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6Attendance xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xsi:type=""GD92Msg6AttReadOnly"">
  <Callsigns>
    <string>FJK60P1</string>
  </Callsigns>
  <Address>Test Address</Address>
</GD92Msg6Attendance>";
			string actual = GD92Message.SaveToMemory<GD92Msg6Attendance>(downcast);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of the downcast message 
		/// </summary>
		/// <description>
		/// As expected this fails becausethe xsi identifies it as the read only class
		/// That is not a problem as we do not need a method for saving messages for reload
		/// directly. Instead, log to file and extract the data from there (using notepad etc)
		/// Strip out the problematic xsi and utf instructions.
		/// Use notepad etc to prepare the data for test messages then get the test program
        /// to read from disk and deserialize for sending test message.
		/// </description>
		[Test]
		[ExpectedException("System.InvalidOperationException")]
		public void GD92Msg6AttReadOnly_Downcast_ReadFromFile_Test()
		{
			GD92Msg6Attendance msg = GD92Message.ReadFromFile<GD92Msg6Attendance>(Path.Combine(TestOutputPath, "Msg6AttReadOnly_downcast.txt"));
		}
		/// <summary>
		/// Test deserialization of the downcast message with xsi stripped out manually
		/// </summary>
		[Test]
		public void GD92Msg6AttReadOnly_Downcast_ReadFromEditedFile_Test()
		{
			GD92Msg6Attendance msg = GD92Message.ReadFromFile<GD92Msg6Attendance>(Path.Combine(TestOutputPath, "Msg6AttReadOnly_downcastedited.txt"));
			string expected = Msg6AttendanceXmlHeader + @"<Callsigns>
    <string>FJK60P1</string>
  </Callsigns>
  <Address>Test Address</Address>
</GD92Msg6Attendance>";
			string actual = GD92Message.SaveToMemory<GD92Msg6Attendance>(msg);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// 
		/// </summary>
		[Test]
		[ExpectedException("System.InvalidOperationException")]
		public void GD92Msg6AttReadOnly_ReadFromFile_Test()
		{
			GD92Msg6AttReadOnly msg = GD92Message.ReadFromFile<GD92Msg6AttReadOnly>(Path.Combine(TestInputPath, "Msg6AttReadOnly_manual.txt"));
			Assert.AreEqual("Test Address", msg.Address);
			Assert.AreEqual("FJK60P1", msg.Callsigns[0]);
		}
	}
}

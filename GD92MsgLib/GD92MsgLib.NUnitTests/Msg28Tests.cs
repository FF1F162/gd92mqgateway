﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
    public class Msg28Tests
    {
        public string TestInputPath;
        public string TestOutputPath;
        public string Msg28XmlHeader;
        public string EmptyMessageAsXml;

        [SetUp]
        public void init()
        {
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg28XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg28>
  ";
            EmptyMessageAsXml = Msg28XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>PeripheralStatus</MsgType>
  <Header ProtocolVersion=""1"" Priority=""3"" StandardPort=""1"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <Sounders>false</Sounders>
  <Lights>false</Lights>
  <Spare2>false</Spare2>
  <Spare3>false</Spare3>
  <Spare4>false</Spare4>
  <Doors>false</Doors>
  <StandbySounder>false</StandbySounder>
  <Appliance9>false</Appliance9>
  <Appliance1>false</Appliance1>
  <Appliance2>false</Appliance2>
  <Appliance3>false</Appliance3>
  <Appliance4>false</Appliance4>
  <Appliance5>false</Appliance5>
  <Appliance6>false</Appliance6>
  <Appliance7>false</Appliance7>
  <Appliance8>false</Appliance8>
  <ManualAcknowledgementPressed>false</ManualAcknowledgementPressed>
  <PowerFailedBatteriesOn>false</PowerFailedBatteriesOn>
  <PowerFailedStandbyGeneratorOn>false</PowerFailedStandbyGeneratorOn>
  <RepeatLastMessagePressed>false</RepeatLastMessagePressed>
  <BatteriesLow>false</BatteriesLow>
  <PaperLow>false</PaperLow>
  <SpareIp1>false</SpareIp1>
  <SpareIp2>false</SpareIp2>
  <RunningCallTelephoneDoorOpened>false</RunningCallTelephoneDoorOpened>
  <PaperOut>false</PaperOut>
  <SpareIp3>false</SpareIp3>
  <SpareIp4>false</SpareIp4>
  <SpareIp5>false</SpareIp5>
  <SpareIp6>false</SpareIp6>
  <SpareIp7>false</SpareIp7>
  <SpareIp8>false</SpareIp8>
</GD92Msg28>";
        }

        /// <summary>
        /// Test that the constructor sets the message type
        /// </summary>
        [Test]
        public void Msg28ConstructorTest()
        {
            var msg = new GD92Msg28();
            Assert.AreEqual(GD92MsgType.PeripheralStatus, msg.MsgType);
        }

        /// <summary>
        /// Examine the content of an empty message
        /// </summary>
        [Test]
        public void GD92Msg28_ToXml_Empty_Test()
        {
            var msg = new GD92Msg28();
            msg.ToXml(Path.Combine(TestOutputPath, "Msg28_empty.txt"));
            string expected = EmptyMessageAsXml;
            string actual = msg.ToXml();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Test deserialization of an empty message
        /// </summary>
        [Test]
        public void GD92Msg28_ReadFromFile_Empty_Test()
        {
            string expected = EmptyMessageAsXml;
            GD92Msg28 msg = GD92Message.ReadFromFile<GD92Msg28>(Path.Combine(TestOutputPath, "Msg28_empty.txt"));
            string actual = msg.ToXml();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Test deserialization of a message sent from kentres and logged (original RSC test data)
        /// XML is output of TestGateway ToXML edited for name of source and destination and utf-16 > 8. 
        /// </summary>
        [Test]
        public void GD92Msg28_ReadFromFile_toMDG1_Test()
        {
            GD92Msg28 msg = GD92Message.ReadFromFile<GD92Msg28>(Path.Combine(TestInputPath, "toMDG1_28.txt"));
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved


namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.Configuration;
    using System.IO;
    using NUnit.Framework;

    [TestFixture]
	public class GD92MessageTests
	{
		[Test]
		public void TestMethod()
		{
			// TODO: Add your test.
		}
        public const string DefaultPathToTestFolders = @"C:\git\kfrs\StormGateway\GD92msgLib";
        public static string FolderFromAppConfigOrDefault(string key)
        {
            string folder = ConfigurationManager.AppSettings[key];
            if (folder == null)
            {
                folder = Path.Combine(DefaultPathToTestFolders, key);
            }
            return folder;
        }
    }
}

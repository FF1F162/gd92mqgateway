﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved


namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    /// <summary>
	/// Tests cover base class and Msg 27 in particular
	/// <description>
	/// Serialize object to file initially and copy the output for use as 'expected' string
	/// Properties that are null or read-only (no setter) do not show up in the XML
	/// Re-worked the original tests after converting to automatic properties
	/// </description>
	/// </summary>
	[TestFixture]
	public class Msg27Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg27XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            // this was copied from a file initially 
		    // changed utf-8 to utf-16 for testing serialization in memory
		    Msg27XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg27>
  ";
   			EmptyMessageAsXml = Msg27XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>Text</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""15"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <Text />
</GD92Msg27>";

		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg27ConstructorTest()
		{
			var msg = new GD92Msg27();
			Assert.AreEqual(GD92MsgType.Text, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg27_ToXml_Empty_Test()
		{
			var msg = new GD92Msg27();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg27_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of message with some text
		/// </summary>
		[Test]
		public void GD92Msg27_ToXml_Text_Test()
		{
			var msg = new GD92Msg27();
			msg.Text = "This is some text";
			msg.ToXml(Path.Combine(TestOutputPath, "Msg27_text.txt"));
			string expected = Msg27XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>Text</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""15"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <Text>This is some text</Text>
</GD92Msg27>";
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of message with empty text
		/// </summary>
		[Test]
		public void GD92Msg27_ToXml_EmptyText_Test()
		{
			var msg = new GD92Msg27();
			msg.Text = string.Empty;
			msg.ToXml(Path.Combine(TestOutputPath, "Msg27_emptytext.txt"));
			string expected = Msg27XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>Text</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""15"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <Text />
</GD92Msg27>";
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of message with some text
		/// </summary>
		[Test]
		public void GD92Msg27_ToXml_Cast_Test()
		{
			var msg = new GD92Msg27();
			msg.Text = "This is some text";
			var iMsg = msg as IGD92Message;
			string expected = Msg27XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>Text</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""15"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <Text>This is some text</Text>
</GD92Msg27>";
			string actual = iMsg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg27_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg27 msg = GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestOutputPath, "Msg27_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of a message sent from kentres and logged (original RSC test data)
		/// </summary>
		[Test]
		public void GD92Msg27_ReadFromFile_toMDG1_Test()
		{
			GD92Msg27 msg = GD92Message.ReadFromFile<GD92Msg27>(Path.Combine(TestInputPath, "toMDG1_27.txt"));
		}
	}
}

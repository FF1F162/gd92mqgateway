﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg1Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg1XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
		    TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
		    Msg1XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg1>
  ";
  			EmptyMessageAsXml = Msg1XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>MobiliseCommand</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""12"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <Sounders>false</Sounders>
  <Lights>false</Lights>
  <Spare2>false</Spare2>
  <Spare3>false</Spare3>
  <Spare4>false</Spare4>
  <Doors>false</Doors>
  <StandbySounder>false</StandbySounder>
  <Appliance9>false</Appliance9>
  <Appliance1>false</Appliance1>
  <Appliance2>false</Appliance2>
  <Appliance3>false</Appliance3>
  <Appliance4>false</Appliance4>
  <Appliance5>false</Appliance5>
  <Appliance6>false</Appliance6>
  <Appliance7>false</Appliance7>
  <Appliance8>false</Appliance8>
</GD92Msg1>";
		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg1ConstructorTest()
		{
			var msg = new GD92Msg1();
			Assert.AreEqual(GD92MsgType.MobiliseCommand, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg1_ToXml_Empty_Test()
		{
			var msg = new GD92Msg1();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg1_empty.txt"));

			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg1_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg1 msg = GD92Message.ReadFromFile<GD92Msg1>(Path.Combine(TestOutputPath, "Msg1_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of a message sent from kentres and logged (original RSC test data)
		/// Leave ReadOnly flag set
		/// </summary>
		[Test]
		public void GD92Msg1_ReadFromFile_toMDG1_readonly_Test()
		{
			GD92Msg1 msg = GD92Message.ReadFromFile<GD92Msg1>(Path.Combine(TestInputPath, "toMDG1_1_readonly.txt"));
		}
		/// <summary>
		/// Test deserialization of a message sent from kentres and logged (original RSC test data)
		/// </summary>
		[Test]
		public void GD92Msg1_ReadFromFile_toMDG1_Test()
		{
			GD92Msg1 msg = GD92Message.ReadFromFile<GD92Msg1>(Path.Combine(TestInputPath, "toMDG1_1.txt"));
		}
		/// <summary>
		/// Set addresses in an empty message
		/// </summary>
		[Test]
		public void GD92Msg1_ToXml_Addressed_Test()
		{
			var msg = new GD92Msg1();
			msg.FromAddress = new GD92Address(22, 0, 10);
			msg.DestAddress = new GD92Address(22, 300, 0);
			msg.ToXml(Path.Combine(TestOutputPath, "Msg1_addressed.txt"));
		}
	}
}

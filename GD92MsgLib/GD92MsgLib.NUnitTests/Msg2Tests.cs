﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg2Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg2XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg2XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg2>
  ";
   			EmptyMessageAsXml = Msg2XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>MobiliseMessage</MsgType>
  <Header ProtocolVersion=""1"" Priority=""1"" StandardPort=""16"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <MobTimeAndDate>0001-01-01T00:00:00</MobTimeAndDate>
  <CallsignList />
  <IncidentNumber />
  <MobilisationType>255</MobilisationType>
  <Address />
  <HouseNumber />
  <Street />
  <SubDistrict />
  <District />
  <Town />
  <County />
  <Postcode />
  <MapRef />
  <TelNumber />
  <Text />
</GD92Msg2>";
  
		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg2ConstructorTest()
		{
			var msg = new GD92Msg2();
			Assert.AreEqual(GD92MsgType.MobiliseMessage, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg2_ToXml_Empty_Test()
		{
			var msg = new GD92Msg2();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg2_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg2_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg2 msg = GD92Message.ReadFromFile<GD92Msg2>(Path.Combine(TestOutputPath, "Msg2_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of a message sent from kentres and logged (original RSC test data)
		/// </summary>
		[Test]
		public void GD92Msg2_ReadFromFile_toMDG1_Test()
		{
			GD92Msg2 msg = GD92Message.ReadFromFile<GD92Msg2>(Path.Combine(TestInputPath, "toMDG1_2.txt"));
		}
	}
}

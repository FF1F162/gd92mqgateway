﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg6AttendanceTests
	{
		public string TestOutputPath;
        public string TestInputPath;
        public string Msg6AttendanceXmlHeader;
		public string Msg6AttendanceXmlHeaderEmpty;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg6AttendanceXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg6Attendance>
  ";
		}

		/// <summary>
		/// Test that the constructor sets the defaults
		/// </summary>
		[Test]
		public void Msg6AttendanceConstructorTest()
		{
			var msg = new GD92Msg6Attendance();
			Assert.AreEqual(string.Empty, msg.Address);
            Assert.AreEqual(0, msg.Callsigns.Count);
		}
		/// <summary>
		/// Examine the content of an empty attendance list
		/// </summary>
		[Test]
		public void GD92Msg6Attendance_SaveToString_Empty_Test()
		{
			var msg = new GD92Msg6Attendance();
			GD92Message.SaveToFile<GD92Msg6Attendance>(msg, Path.Combine(TestOutputPath, "Msg6Attendance_empty.txt"));
			string expected = Msg6AttendanceXmlHeader + @"<Callsigns />
  <Address />
</GD92Msg6Attendance>";
			string actual = GD92Message.SaveToMemory<GD92Msg6Attendance>(msg);
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Examine the content of an attendance list
		/// </summary>
		[Test]
		public void GD92Msg6Attendance_SaveToString_Data_Test()
		{
			var callsigns = new Collection<string>( );
			callsigns.Add("FJK60P1");
			var msg = new GD92Msg6Attendance(callsigns, "Test Address");
			GD92Message.SaveToFile<GD92Msg6Attendance>(msg, Path.Combine(TestOutputPath, "Msg6Attendance_data.txt"));
			string expected = Msg6AttendanceXmlHeader + @"<Callsigns>
    <string>FJK60P1</string>
  </Callsigns>
  <Address>Test Address</Address>
</GD92Msg6Attendance>";
			string actual = GD92Message.SaveToMemory<GD92Msg6Attendance>(msg);
			Assert.AreEqual(expected, actual);		
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg5Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg5XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg5XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg5>
  ";
   			EmptyMessageAsXml = Msg5XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>ResourceStatusRequest</MsgType>
  <Header ProtocolVersion=""1"" Priority=""4"" StandardPort=""1"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <Callsign>NotSet</Callsign>
  <StationRequest>false</StationRequest>
</GD92Msg5>";

		}

		/// <summary>
		/// Test that the constructor sets the message type
		/// </summary>
		[Test]
		public void Msg5ConstructorTest()
		{
			var msg = new GD92Msg5();
			Assert.AreEqual(GD92MsgType.ResourceStatusRequest, msg.MsgType);		
		}
		/// <summary>
		/// Examine the content of an empty message
		/// </summary>
		[Test]
		public void GD92Msg5_ToXml_Empty_Test()
		{
			var msg = new GD92Msg5();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg5_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Set ReadOnly and check serialization still works
		/// </summary>
		[Test]
		public void GD92Msg5_ToXml_ReadOnly_Test()
		{
			var msg = new GD92Msg5();
			msg.Callsign = "FJK11P1";
			msg.SetReadOnly();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg5_readonly.txt"));
		}
		/// <summary>
		/// Test deserialization of an empty message
		/// </summary>
		[Test]
		public void GD92Msg5_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg5 msg = GD92Message.ReadFromFile<GD92Msg5>(Path.Combine(TestOutputPath, "Msg5_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		/// <summary>
		/// Test deserialization of a message sent from kentres and logged (original RSC test data)
		/// </summary>
		[Test]
		public void GD92Msg5_ReadFromFile_toMDG1_Test()
		{
			GD92Msg5 msg = GD92Message.ReadFromFile<GD92Msg5>(Path.Combine(TestInputPath, "toMDG1_5.txt"));
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class Msg42Tests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string Msg42XmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            Msg42XmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<GD92Msg42>
  ";
   			EmptyMessageAsXml = Msg42XmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>AlertStatus</MsgType>
  <Header ProtocolVersion=""1"" Priority=""3"" StandardPort=""1"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
  <AlerterStatus>xx</AlerterStatus>
</GD92Msg42>";

		}

		/// <summary>
		/// Test that the constructor sets the message type.
		/// </summary>
		[Test]
		public void Msg42ConstructorTest()
		{
			var msg = new GD92Msg42();
			Assert.AreEqual(GD92MsgType.AlertStatus, msg.MsgType);		
		}
		
		/// <summary>
		/// Examine the content of an empty message.
		/// </summary>
		[Test]
		public void GD92Msg42_ToXml_Empty_Test()
		{
			var msg = new GD92Msg42();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg42_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
		
		/// <summary>
		/// Set ReadOnly and check serialization still works.
		/// </summary>
		[Test]
		public void GD92Msg42_ToXml_ReadOnly_Test()
		{
			var msg = new GD92Msg42();
			msg.SetReadOnly();
			msg.ToXml(Path.Combine(TestOutputPath, "Msg42_readonly.txt"));
		}
		
		/// <summary>
		/// Test deserialization of an empty message.
		/// </summary>
		[Test]
		public void GD92Msg42_ReadFromFile_Empty_Test()
		{
			string expected = EmptyMessageAsXml;
			GD92Msg42 msg = GD92Message.ReadFromFile<GD92Msg42>(Path.Combine(TestOutputPath, "Msg42_empty.txt"));
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}

        /// <summary>
        /// Test deserialization of a message sent from kentres and logged (original RSC test data).
        /// XML is output of TestGateway ToXML edited for name of source and destination and utf-16 > 8. 
        /// </summary>
        [Test]
        public void GD92Msg42_ReadFromFile_toMDG1_Test()
        {
            GD92Msg42 msg = GD92Message.ReadFromFile<GD92Msg42>(Path.Combine(TestInputPath, "toMDG1_42.txt"));
        }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace GD92MsgLib.NUnitTests
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.GD92;

    [TestFixture]
	public class RawFrameMsgTests
	{
		public string TestInputPath;
		public string TestOutputPath;
		public string RawFrameMsgXmlHeader;
		public string EmptyMessageAsXml;
		
		[SetUp]
		public void init()
		{
            TestInputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = GD92MessageTests.FolderFromAppConfigOrDefault("TestOutput");
            RawFrameMsgXmlHeader = @"<?xml version=""1.0"" encoding=""utf-16""?>
<RawFrameMsg>
  ";
   			EmptyMessageAsXml = RawFrameMsgXmlHeader + @"<SerialNumber>0</SerialNumber>
  <MsgType>RawFrame</MsgType>
  <Header ProtocolVersion=""1"" Priority=""4"" StandardPort=""1"" />
  <SentAt>0001-01-01T00:00:00</SentAt>
  <ReceivedAt>0001-01-01T00:00:00</ReceivedAt>
  <ManualAck>false</ManualAck>
  <FromAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </FromAddress>
  <DestAddress>
    <Brigade>0</Brigade>
    <Node>0</Node>
    <Port>0</Port>
  </DestAddress>
  <FromNodeType>None</FromNodeType>
  <DestNodeType>None</DestNodeType>
</RawFrameMsg>";

		}

		/// <summary>
		/// Test that the constructor sets the message type.
		/// </summary>
		[Test]
		public void RawFrameMsgConstructorTest()
		{
			var msg = new RawFrameMsg();
			Assert.AreEqual(GD92MsgType.RawFrame, msg.MsgType);		
		}
		
		/// <summary>
		/// Examine the content of an empty message.
		/// </summary>
		[Test]
		public void RawFrameMsg_ToXml_Empty_Test()
		{
			var msg = new RawFrameMsg();
			msg.ToXml(Path.Combine(TestOutputPath, "RawFrameMsg_empty.txt"));
			string expected = EmptyMessageAsXml;
			string actual = msg.ToXml();
			Assert.AreEqual(expected, actual);		
		}
	}
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace LogLib.NUnitTests
{
    using System;
    using System.Text;
    using NUnit.Framework;
    
    [TestFixture]
    public class LogControlTests
    {
        /// <summary>
        /// Show that the Length property shows the length of the current contents.
        /// </summary>
        [Test]
        public void LogControlStringBuilderLengthTest()
        {
            var s = new StringBuilder();
            Assert.AreEqual(0, s.Length);
            s.Append("12345");
            Assert.AreEqual(5, s.Length);
            s.Remove(0, s.Length);
            Assert.AreEqual(0, s.Length);
        }
    }
}

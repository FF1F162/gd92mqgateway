﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace LogLib.NUnitTests
{
    using System;
    using System.Configuration;
    using System.IO;
    using NUnit.Framework;
    using Thales.KentFire.LogLib;

    /// <summary>
    /// Convert output of BitConverter (captured from logs taken from the TestGateway on site)
    /// to a byte array then use that to test ByteLogger. There must be no trailing characters
    /// or lines in the input.
    /// Write to file for visual inspection.
    /// </summary>
	[TestFixture]
	public class ByteLoggerTests
	{
		private string ResourceStatusFJK44P1;
		private string ResourceStatusFJK60P1;
		private string RscTextCrewRequest;
		private string StormMobilise459;
		private string StormMobilise612;
		private string StormCompressedText;
        private string Mob150120_124410;
        private string LogUpdate1;
        private string LogUpdate2;
        private string LogUpdate3;
        public const string DefaultPathToTestFolders = @"C:\git\kfrs\StormGateway\LogLib";
		public static string TestInputPath;
		public static string TestOutputPath;
		
		[TestFixtureSetUp]
		public void Init()
		{
		    TestInputPath = FolderFromAppConfigOrDefault("TestInput");
            TestOutputPath = FolderFromAppConfigOrDefault("TestOutput");
            RscTextCrewRequest = ReadHexDataFromFile("RscTextCrewRequest");
            ResourceStatusFJK44P1 = ReadHexDataFromFile("ResourceStatusFJK44P1");
            ResourceStatusFJK60P1 = ReadHexDataFromFile("ResourceStatusFJK60P1");
            StormMobilise459 = ReadHexDataFromFile("StormMobilise459");
            StormMobilise612 = ReadHexDataFromFile("StormMobilise612");
            StormCompressedText = ReadHexDataFromFile("StormCompressedText");
            Mob150120_124410 = ReadHexDataFromFile("Mob150120_124410");
            LogUpdate1 = ReadHexDataFromFile("LogUpdate1");
            LogUpdate2 = ReadHexDataFromFile("LogUpdate2");
            LogUpdate3 = ReadHexDataFromFile("LogUpdate3");
		}
        /// <summary>
        /// If app.config is not available use the default path
        /// Check that the path in app.config (or the default) exists.
        /// Return the default path even if it does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string FolderFromAppConfigOrDefault(string key)
        {
            string folder = ConfigurationManager.AppSettings[key];
            if (folder != null)
            {
                folder = FoldeExistsOrNull(folder);
            }
            return folder ?? Path.Combine(DefaultPathToTestFolders, key) ;
        }
        private static string FoldeExistsOrNull(string folder)
        {
            var logDirectory = new DirectoryInfo(folder);
            return (logDirectory.Exists) ? folder : null;
        }
        /// <summary>
        /// Read hexadecimal data (as output by BitConverter)
        /// </summary>
        /// <param name="dataName"></param>
        /// <returns></returns>
		private static string ReadHexDataFromFile(string dataName)
		{
			string fileName = Path.Combine(TestInputPath, dataName + ".txt");
            using (var file = new StreamReader(fileName))
            {
            	return file.ReadToEnd();
            }
		}
        
		[Test]
		public void EmptyLogTest()
		{
			var data = new byte[0];
			var logger = new ByteLogger(data);
			string log = logger.PrintLog(0, data.Length);
			Assert.AreEqual(0, log.Length);
		}
        
		[Test]
		public void AlmostEmptyLog_LineMadeUpWithSpacesTest()
		{
			byte[] data = LogReader.ConvertToByteArray("01");
			var logger = new ByteLogger(data);
			string log = logger.PrintLog(0, data.Length);
			Assert.AreEqual(ByteLogger.OutputLineLength, log.Length);
		}
        
		[Test]
		public void PrintStormResourceStatusFJK60P1()
		{
			PrintLogToFile(ResourceStatusFJK60P1, "ResourceStatusFJK60P1");
		}
		[Test]
		public void PrintStormResourceStatusFJK44P1()
		{
			PrintLogToFile(ResourceStatusFJK44P1, "ResourceStatusFJK44P1");
		}
		[Test]
		public void PrintStormMobilise459Test()
		{
			PrintLogToFile(StormMobilise459, "StormMobilise459");
		}
		[Test]
		public void PrintStormMobilise612Test()
		{
			PrintLogToFile(StormMobilise612, "StormMobilise612");
		}
		[Test]
		public void PrintRscCrewRequestTest()
		{
			PrintLogToFile(RscTextCrewRequest, "RscTextCrewRequest");
		}
		[Test]
		public void PrintStormCompressedTextTest()
		{
			PrintLogToFile(StormCompressedText, "StormCompressedText");
		}
        [Test]
        public void PrintMob150120_124410Test()
        {
            PrintLogToFile(Mob150120_124410, "Mob150120_124410");
        }
        [Test]
        public void PrintLogUpdate1Test()
        {
            PrintLogToFile(LogUpdate1, "LogUpdate1");
        }
        [Test]
        public void PrintLogUpdate2Test()
        {
            PrintLogToFile(LogUpdate2, "LogUpdate2");
        }
        [Test]
        public void PrintLogUpdate3Test()
        {
            PrintLogToFile(LogUpdate3, "LogUpdate3");
        }
        public static void PrintLogToFile(string dataFromLog, string dataName)
		{
			byte[] data = LogReader.ConvertToByteArray(dataFromLog);
			var logger = new ByteLogger(data);
			string log = logger.PrintLog(0, data.Length);
			string fileName = Path.Combine(TestOutputPath, dataName + ".txt");
            using (var file = new StreamWriter(fileName))
            {
                file.WriteLine(log);
            }
		}
	}
}

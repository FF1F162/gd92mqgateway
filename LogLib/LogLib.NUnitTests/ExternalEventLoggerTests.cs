﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace LogLib.NUnitTests
{
    using System;
    using NUnit.Framework;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Test logging to the Windows event log - now moved to EventLib
    /// Normally exclude by commenting out [Test] since these need to be run as Administrator 
    /// </summary>
    //[TestFixture]
    public class ExternalEventLoggerTests
    {
        /// <summary>
        /// Unexpectedly this works even though the source has not been created.
        /// </summary>
        //[Test]
        public void ExternalEventLogger_ServiceNameTest()
        {
            var logger = new ExternalEventLogger("PretendService");
            logger.WriteEvent("This is from ExternalEventLogger_ServiceNameTest");
        }

        /// <summary>
        /// Create a source and present it in the same way as for a service.
        /// </summary>
        //[Test]
        public void ExternalEventLogger_SimulateServiceTest()
        {
            string source = "TestApplication";
            if (!System.Diagnostics.EventLog.SourceExists(source))
            {
                System.Diagnostics.EventLog.CreateEventSource(source, "Application");
            }
            var logger = new ExternalEventLogger(source);
            logger.WriteEvent("This is from ExternalEventLogger_SimulateServiceTest");
        }

        /// <summary>
        /// Create a source and present it in the same way as for a service.
        /// </summary>
        //[Test]
        public void ExternalEventLogger_ApplicationTest()
        {
            var logger = new ExternalEventLogger();
            logger.WriteEvent("This is from ExternalEventLogger_ApplicationTest");
        }
    }
}

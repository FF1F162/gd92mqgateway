﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace LogLib.NUnitTests
{
	using System;
	using NUnit.Framework;
	using Thales.KentFire.LogLib;

	[TestFixture]
	public class LogReaderTests
	{
		[Test]
		[ExpectedException("System.FormatException")]
		public void ConvertToByteArrayBadSeparatorTest()
		{
			const string input = "01 01";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(10, output.Length);
		}
		
		[Test]
		[ExpectedException("System.FormatException")]
		public void ConvertToByteArrayNotHexTest()
		{
			const string input = "QQ";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(10, output.Length);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void ConvertToByteArrayEmptyTest()
		{
			const string input = "";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(10, output.Length);
		}
		
		[Test]
		public void ConvertToByteArrayLengthTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(10, output.Length);
		}
		
		[Test]
		public void ConvertToByteArrayValueTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(10, output[8]);
		}
		
		[Test]
		public void ConvertToByteArrayFirstValueTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(1, output[0]);
		}

		[Test]
		[ExpectedException("System.FormatException")]
		public void ConvertToByteArrayEndsInDoubleSpaceTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11-  ";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(17, output[9]);
		}

		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void ConvertToByteArrayEndsInSingleSpaceTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11- ";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(17, output[9]);
		}
		
		[Test]
		[ExpectedException("System.ArgumentOutOfRangeException")]
		public void ConvertToByteArrayEmptyFieldTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11-";
			byte[] output = LogReader.ConvertToByteArray(input);
			Assert.AreEqual(17, output[9]);
		}
		
		[Test]
		public void TerminateAtStopTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11.-  ";
			string output = LogReader.TerminateAtStop(input);
			Assert.AreEqual(29, output.Length);
		}
		
		[Test]
		public void TerminateAtStopFirstCharacterTest()
		{
			const string input = ".01-16-AF-81-0F-C1-16-00-0A-11.-  ";
			string output = LogReader.TerminateAtStop(input);
			Assert.AreEqual(0, output.Length);
		}
		
		[Test]
		public void TerminateAtStopEmptyStringTest()
		{
			const string input = "";
			string output = LogReader.TerminateAtStop(input);
			Assert.AreEqual(0, output.Length);
		}
		
		[Test]
		public void TerminateAtStopNoStopTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11";
			string output = LogReader.TerminateAtStop(input);
			Assert.AreEqual(29, output.Length);
		}
		
		[Test]
		public void ConvertToBitConverterFormatNoConversionTest()
		{
			const string input = "01-16-AF-81-0F-C1-16-00-0A-11";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(29, output.Length);
		}
		
		[Test]
		public void ConvertToBitConverterFormatTest()
		{
			const string input = "0116AF810FC116000A11";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(29, output.Length);
		}
		
		[Test]
		public void ConvertToBitConverterFormatEmptyStringTest()
		{
			const string input = "";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(0, output.Length);
		}
		
		[Test]
		public void ConvertToBitConverterFormatOneDigitTest()
		{
			const string input = "8";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(1, output.Length);
		}
		
		[Test]
		public void ConvertToBitConverterFormatTwoDigitTest()
		{
			const string input = "22";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(2, output.Length);
		}
		
		[Test]
		public void ConvertToBitConverterFormatThreeDigitTest()
		{
			const string input = "223";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(3, output.Length);
			Assert.AreEqual("22-", output);
		}
		
		[Test]
		public void ConvertToBitConverterFormatFourDigitTest()
		{
			const string input = "2233";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(5, output.Length);
			Assert.AreEqual("22-33", output);
		}
		
		[Test]
		public void ConvertToBitConverterFormatFourSpaceTest()
		{
			const string input = "    ";
			string output = LogReader.ConvertToBitConverterFormat(input);
			Assert.AreEqual(5, output.Length);
			Assert.AreEqual("  -  ", output);
		}
	}
}

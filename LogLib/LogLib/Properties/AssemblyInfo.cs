﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LogLib")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Thales")]
[assembly: AssemblyProduct("LogLib")]
[assembly: AssemblyCopyright("Copyright © Thales 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1a18bbaa-b934-42ac-98e1-d0ad58c4cdbc")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.41.0")]
[assembly: AssemblyFileVersion("1.0.41.0")]

[assembly: InternalsVisibleTo("LogLib.NUnitTests")]
[assembly: InternalsVisibleTo("StormGway")]
[assembly: InternalsVisibleTo("TestGateway")]

[assembly: System.CLSCompliant(true)]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]

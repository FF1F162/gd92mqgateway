﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.LogLib
{
    using System;
    using System.Text;

    /// <summary>
    /// Static class holding methods to read elements of the log.
    /// </summary>
    public static class LogReader
    {
        public const char ByteSeparatorCharacter = '-';
        public const char StopCharacter = '.';
        public const int ByteLength = 2;
        
        /// <summary>
        /// Insert delimiters if input appears exclusively numeric (e.g. from Storm log).
        /// </summary>
        /// <param name="testData">Data as hexadecimal bytes.</param>
        /// <returns>Input string with separators (inserted if necessary) between two-character bytes.</returns>
        public static string ConvertToBitConverterFormat(string testData)
        {
            string returnString = TerminateAtStop(testData);
            if (returnString.Length > ByteLength && returnString[ByteLength] != ByteSeparatorCharacter)
            {
                returnString = InsertSeparators(testData);
            }
            
            return returnString;
        }
        
        /// <summary>
        /// Terminate input at stop character.
        /// </summary>
        /// <param name="testData">Data as hexadecimal bytes (delimiter optional).</param>
        /// <returns>Original string (if no stop) or string terminated at Stop character.</returns>
        public static string TerminateAtStop(string testData)
        {
            string returnString = testData;
            int indexOfStop = testData.IndexOf(StopCharacter);
            if (indexOfStop >= 0)
            {
                returnString = testData.Substring(0, indexOfStop);
            }
            
            return returnString;
        }
        
        /// <summary>
        /// Read the output of BitConverter.ToString back into a byte array.
        /// The string must be exclusively 2-digit hexadecimal separated by hyphen.
        /// </summary>
        /// <param name="testData">Data as output by BitConverter.</param>
        /// <returns>Array of bytes.</returns>
        public static byte[] ConvertToByteArray(string testData)
        {
            var output = new byte[CalculateOutputLength(testData)];
            char[] separators = { ByteSeparatorCharacter };
            string[] splitData = testData.Split(separators);
            if (output.Length != splitData.Length)
            {
                throw new System.FormatException("string must be exclusively 2-digit hexadecimal");
            }
            
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = ConvertTwoHexDigitsToByte(splitData[i]);
            }
            
            return output;
        }
        
        /// <summary>
        /// Each byte in the output is represented in the input by a separator and two digits.
        /// </summary>
        /// <param name="testData">Data as output by BitConverter.</param>
        /// <returns>Expected length of output.</returns>
        private static int CalculateOutputLength(string testData)
        {
            return (testData.Length / 3) + 1;
        }
        
        /// <summary>
        /// Validate input and use Convert.
        /// </summary>
        /// <param name="digits">Two hexadecimal digits.</param>
        /// <returns>Byte represented by the input.</returns>
        private static byte ConvertTwoHexDigitsToByte(string digits)
        {
            if (digits.Length != 2)
            {
                throw new ArgumentOutOfRangeException("digits", digits, "two-character hexadecimal expected");
            }
            
            const int BaseSixteen = 16;
            return Convert.ToByte(digits, BaseSixteen);
        }
        
        /// <summary>
        /// Insert a separator after each 2-character byte.
        /// Remove the trailing separator (or odd character).
        /// Result appears the same as if ouput by BitConverter.
        /// </summary>
        /// <param name="testData">Byte data with no separators.</param>
        /// <returns></returns>
        private static string InsertSeparators(string testData)
        {
            var sb = new StringBuilder();
            bool appendSeparator = false;
            for (int testIndex = 0; testIndex < testData.Length; testIndex++)
            {
                sb.Append(testData[testIndex], 1);
                if (appendSeparator)
                {
                    sb.Append(ByteSeparatorCharacter);
                    appendSeparator = false;
                }
                else
                {
                    appendSeparator = true;
                }
            }
            
            sb.Remove(sb.Length - 1, 1); // remove last character (expected to be a trailing separator)
            return sb.ToString();
        }
    }
}

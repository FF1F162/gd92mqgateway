﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.LogLib
{
    using System;
    using System.Linq;
    using System.Windows.Forms;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Form that displays logs as they are generated.
    /// When the 'current' text box becomes full, the contents are displayed temporarily
    /// in the 'previous' text box.
    /// The form includes a combo box to display and change the current logging level.
    /// </summary>
    public partial class LoggingForm : Form
    {
        private const int MaxLogTextLength = 10000;

        public LoggingForm()
        {
            this.InitializeComponent();

            // This raises comboBox1_SelectedIndexChanged so must be done
            // after setting m_EventControl
            comboBox1.Items.AddRange(LogLevel.GetNames(typeof(LogLevel)));
            this.SetDisplayedLogLevel(EventControl.LogEventLevel.ToString());

            this.Text = LogControl.Instance.LoggingName + " Log";
        }

        private delegate void SetTextCallback(string level);
        
        public void SetDisplayedLogLevel(string level)
        {
            if (comboBox1.InvokeRequired)
            {
                var s1 = new SetTextCallback(this.SetDisplayedLogLevel);
                this.Invoke(s1, new object[] { level });
            }
            else
            {
                comboBox1.Text = level;
            }
        }

        public void SetText(string text)
        {
            // taken from GD92Rad.Form1.cs (box numbers are reversed)
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (richTextBox2.InvokeRequired)
            {
                var s1 = new SetTextCallback(this.SetText);
                this.Invoke(s1, new object[] { text });
            }
            else
            {
                if (richTextBox2.TextLength > MaxLogTextLength)
                {
                    richTextBox1.Text = richTextBox2.Text;
                    richTextBox2.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, " . . . continued{0}", Environment.NewLine);
                }
                
                richTextBox2.AppendText(text + Environment.NewLine); 
                richTextBox2.SelectionStart = richTextBox2.TextLength; // TextBox cannot do this
                richTextBox2.ScrollToCaret();
            }
        }
        
        private void Button1_Click(object sender, EventArgs e)
        {
            // Clear previous log
            richTextBox1.Text = string.Empty;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            // Clear current log
            richTextBox2.Text = string.Empty;
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            EventControl.LogEventLevel = (LogLevel)Enum.Parse(typeof(LogLevel), comboBox1.SelectedItem.ToString(), false);
        }
    }
}

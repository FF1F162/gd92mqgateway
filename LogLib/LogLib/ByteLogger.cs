﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.LogLib
{
    using System;
    using System.Text;

    /// <summary>
    /// Represent an array of bytes as hexadecimal and ascii characters.
    /// </summary>
    public class ByteLogger
    {
        public const int OutputLineLength = (LineLength * 5) + (4 * 3) + 1;
        private const int LineLength = 10;
        private const string ThreeSpaces = "   ";
        private const string FourSpaces = "    ";
        private readonly char[] lineChars;
        private readonly Encoding ascii;
        private readonly byte[] buffer;
        private int logLength;
        private StringBuilder logBuilder;
        private int bufferIndex;
        
        /// <summary>
        /// Initialises a new instance of the <see cref="ByteLogger"/> class.
        /// </summary>
        /// <param name="buffer">Array of bytes to be logged.</param>
        public ByteLogger(byte[] buffer)
        {
            this.buffer = buffer;
            this.lineChars = new char[LineLength];
            this.ascii = Encoding.ASCII;
        }
        
        /// <summary>
        /// Represent the byte array in a string. Output comprises lines showing up to 10 bytes
        /// in hexadecimal and ascii.
        /// </summary>
        /// <param name="startIndex">Index into the array of bytes.</param>
        /// <param name="length">Number of bytes to log.</param>
        /// <returns>Log output.</returns>
        public string PrintLog(int startIndex, int length)
        {
            this.ValidateParameters(startIndex, length);
            this.logBuilder = new StringBuilder();
            this.bufferIndex = startIndex;
            this.logLength = length;
            int fullLines = this.logLength / LineLength;
            int remainder = this.logLength - (fullLines * LineLength);
            for (int i = 0; i < fullLines; i++)
            {
                this.AppendLine(LineLength);
                this.bufferIndex += LineLength;
            }
            
            if (remainder > 0)
            {
                this.AppendLine(remainder);
            }
            
            return this.logBuilder.ToString();
        }
        
        /// <summary>
        /// Check that the start and end of the array for conversion are within the buffer.
        /// </summary>
        /// <param name="startIndex">Start index within the buffer.</param>
        /// <param name="length">Length of array.</param>
        private void ValidateParameters(int startIndex, int length)
        {
            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException("startIndex");
            }
            
            if (startIndex > this.buffer.Length)
            {
                throw new ArgumentOutOfRangeException("startIndex");
            }
            
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException("length");
            }
            
            if (startIndex + length > this.buffer.Length)
            {
                throw new ArgumentOutOfRangeException("length");
            }
        }
        
        /// <summary>
        /// Add a line to the output comprising
        /// index of first byte represented in the line
        /// up to ten bytes hexadecimal
        /// same bytes as ascii characters (or . if unprintable).
        /// </summary>
        /// <param name="length">Number of bytes represented.</param>
        private void AppendLine(int length)
        {
            this.logBuilder.AppendFormat(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0,4}",
                this.bufferIndex.ToString(System.Globalization.CultureInfo.InvariantCulture));
            this.logBuilder.Append(FourSpaces);
            this.AppendLineInHexadecimal(length);
            this.logBuilder.Append(FourSpaces);
            this.AppendLineInCharacters(length);
            this.logBuilder.Append(Environment.NewLine);
        }
        
        /// <summary>
        /// Use BitConverter for hexadecimal representation.
        /// Pad with spaces if representing less than ten bytes.
        /// </summary>
        /// <param name="length">Number of bytes represented.</param>
        private void AppendLineInHexadecimal(int length)
        {
            this.logBuilder.Append(BitConverter.ToString(this.buffer, this.bufferIndex, length));
            for (int i = length; i < LineLength; i++)
            {
                this.logBuilder.Append(ThreeSpaces);
            }
        }
        
        /// <summary>
        /// Use Encoding to convert array from bytes to ascii characters.
        /// Add each printable character to the output; substitute . it not printable.
        /// </summary>
        /// <param name="length">Number of bytes represented.</param>
        private void AppendLineInCharacters(int length)
        {
            this.ascii.GetChars(this.buffer, this.bufferIndex, length, this.lineChars, 0);
            for (int i = 0; i < LineLength; i++)
            {
                char c = ' ';
                if (i < length)
                {
                    c = this.lineChars[i];
                }
                
                if (!(char.IsLetterOrDigit(c) || char.IsPunctuation(c) || ' ' == c))
                {
                    c = '.';
                }
                
                this.logBuilder.AppendFormat("{0} ", c);
            }
        }
    }
}

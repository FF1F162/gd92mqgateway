﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.LogLib
{
    using System;
    using System.IO;
    using System.Security.AccessControl;
    using System.Text;
    using System.Windows.Forms;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// Singleton that subscribes to log event and controls output of logs to the logging window and/or log file.
    /// Note that the LogEventLevel property is in EventControl, which applies the filter based on log level.
    /// </summary>
    public sealed class LogControl : IDisposable
    {
        private const string DefaultFolderName = "log";
        private const string DefaultLogFileName = "Logs";
        private const string DefaultUserGroupName = "Users";
        private const int MaxLogCount = 999;
        private const int SavedLogInitialSize = 2000; // characters
        private const int SavedLogWriteTimeSpan = 5000; // milliseconds

        private static readonly LogControl Self = new LogControl();

        /// <summary>
        /// Used to accumulate logs in between writes by the timer.
        /// </summary>
        private readonly StringBuilder savedLogs;

        /// <summary>
        /// For thread-safe access to log display and saved logs.
        /// </summary>
        private readonly object logLock;

        /// <summary>
        /// Used to hold header (maybe) and string of accumulated logs prior to writing.
        /// Emptied after writing or after failed attempt to write.
        /// </summary>
        private string log;

        /// <summary>
        /// Timer to write logs every few seconds. The file is closed in between, allowing access
        /// for copy and monitoring and easy creation of a new file based on date (or datetime).
        /// </summary>
        private System.Threading.Timer savedLogWriteTimer;
        
        /// <summary>
        /// LogFolder + LogFileName (add date.txt for individual log file).
        /// </summary>
        private string baseName = string.Empty;

        /// <summary>
        /// Individual log file = baseName + yymmdd + .txt.
        /// </summary>
        private string logFilePath = "none";

        /// <summary>
        /// Set when logs are written successfully. This is the path used to flush logs when the service stops.
        /// </summary>
        private string currentLogFile = string.Empty;

        private bool logDisplayed;
        private LoggingForm logForm;
        
        private int logCount;
        private int exceptionCount;

        /// <summary>
        /// Prevents a default instance of the <see cref="LogControl"/> class from being created.
        /// Subscribe to log event and set up default parameters for log to file
        /// Log in the same folder as the assembly by default.
        /// </summary>
        private LogControl()
        {
            EventControl.Instance.Log += new EventHandler<LogEventArgs>(this.EventControl_Log);
            var fileInfo = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);
            this.LogFolder = Path.Combine(fileInfo.DirectoryName, DefaultFolderName);
            this.LoggingName = DefaultLogFileName;
            this.UserGroupName = DefaultUserGroupName;
            this.HeaderInformation = string.Empty;
            this.savedLogs = new StringBuilder(SavedLogInitialSize);
            this.log = string.Empty;
            this.logLock = new object();
            this.savedLogWriteTimer = new System.Threading.Timer(this.WriteSavedLogs, null, 0, SavedLogWriteTimeSpan);
        }

        /// <summary>
        /// Provide access to non-static properties and methods of the singleton.
        /// </summary>
        /// <value>Single instance of class.</value>
        public static LogControl Instance
        {
            get
            {
                return Self;
            }
        }

        /// <summary>
        /// Set true for log to file.
        /// </summary>
        /// <value>True for log to file.</value>
        public bool LogToFile { get; set; }

        /// <summary>
        /// Core name for the logging file.
        /// </summary>
        /// <value>Log file name (omits path and date).</value>
        public string LoggingName { get; set; }

        /// <summary>
        /// Default is folder 'log' in the same folder as the assembly.
        /// </summary>
        /// <value>Path of folder that holds the log files.</value>
        public string LogFolder { get; set; }

        /// <summary>
        /// Default is 'Users' - used to set permissions for new log file.
        /// </summary>
        /// <value>User group name.</value>
        public string UserGroupName { get; set; }

        /// <summary>
        /// Default is 'Users' - used to set permissions for new log file.
        /// </summary>
        /// <value>Header information.</value>
        public string HeaderInformation { get; set; }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        public void Dispose()
        {
            if (this.logForm != null)
            {
                this.logForm.Dispose();
                this.logForm = null;
            }
            
            if (this.savedLogWriteTimer != null)
            {
                this.savedLogWriteTimer.Dispose();
                this.savedLogWriteTimer = null;
            }
        }
        
        /// <summary>
        /// Update the logging level in EventControl and refresh the displayed level (if there is a logging window).
        /// </summary>
        /// <param name="level">Logging level.</param>
        public void SetLoggingLevel(LogLevel level)
        {
            EventControl.LogEventLevel = level;
            if (this.logForm != null)
            {
                this.logForm.SetDisplayedLogLevel(level.ToString());
            }
        }

        /// <summary>
        /// Flush the logs (provided for when the process stops at shutdown).
        /// Lock for thread-safe access to the saved logs.
        /// Actually this probably never finds any saved logs now there is a sleep between
        /// catching and throwing exceptions during shutdown (which allows time 1. for processing
        /// the event queue and 2. writing the next batch of logs as normal).
        /// </summary>
        public void FlushSavedLogs()
        {
            lock (this.logLock)
            {
                if (this.currentLogFile.Length > 0)
                {
                    this.savedLogs.Append(" END OF LOG"); // mark of success.
                    this.savedLogs.AppendLine();
                    this.log = this.savedLogs.ToString();
                    this.WriteLogsToFileAndCleanUp(this.currentLogFile);
                }
            }
        }

        #region LoggingForm

        /// <summary>
        /// Open or close logging form (toggle action depending on previous state).
        /// </summary>
        public void OpenLoggingForm()
        {
            this.logDisplayed = !this.logDisplayed; // toggle between minimized and displayed
            this.OpenLoggingForm(this.logDisplayed);
        }
        
        /// <summary>
        /// Open logging form and display it if show is true.
        /// </summary>
        /// <param name="show">Set true to display the logging form.</param>
        public void OpenLoggingForm(bool show)
        {
            try
            {
                // make sure only one instance of the form is displayed
                // the Closed event handler sets it back to null
                if (this.logForm == null)
                {
                    this.logForm = new LoggingForm();
                    this.logForm.ShowInTaskbar = false;
                    if (!show)
                    {
                        this.logForm.WindowState = FormWindowState.Minimized;
                        this.logDisplayed = false;
                    }
                    
                    this.logForm.Show();
                    this.logForm.Closed += new System.EventHandler(this.LoggingFormClosed);
                }
                else
                {
                    if (show)
                    {
                        this.logForm.WindowState = FormWindowState.Normal;
                        this.logForm.BringToFront();
                    }
                    else
                    {
                        this.logForm.WindowState = FormWindowState.Minimized;
                    }
                }
            }
            catch (Exception)
            {
                this.logForm = null;
                throw;
            }
        }
        
        /// <summary>
        /// Close the form if there is a reference to it.
        /// </summary>
        public void CloseLoggingForm()
        {
            if (this.logForm != null)
            {
                this.logForm.Close();
            }
        }
        
        /// <summary>
        /// Event handler to set the reference to null when the form is closed.
        /// </summary>
        /// <param name="sender">Event publisher (not used).</param>
        /// <param name="e">Event arguments (not used).</param>
        private void LoggingFormClosed(object sender, EventArgs e)
        {
            this.logForm = null;
        }

        #endregion

        /// <summary>
        /// Handle a log event: 
        /// Set lock - as it is possible for more than one timer thread to be serving the event queue
        /// and (otherwise) calling this method at the same time. Also there is a separate timer 
        /// for writing the accumulated saved logs to file.
        /// Write to the logging window (if any) and save the log (if log to file is configured).
        /// </summary>
        /// <param name="sender">Event publisher (not used).</param>
        /// <param name="eventArguments">Event arguments (contents of log).</param>
        private void EventControl_Log(object sender, LogEventArgs eventArguments)
        {
            lock (this.logLock)
            {
                if (this.logForm != null)
                {
                    this.logForm.SetText(eventArguments.Message);
                }
                
                if (this.LogToFile)
                {
                    this.SaveLogAndCounts(eventArguments.Message);
                }
            }
        }
        
        /// <summary>
        /// Save the log to be written later. Keep a count of logs. This is for checking (manually)
        /// against the log number to make sure none are missing. Print the count and reset regularly
        /// together with a count of exceptions (caught when file is unavailable).
        /// </summary>
        /// <param name="logMessage">Log message from event.</param>
        private void SaveLogAndCounts(string logMessage)
        {
            this.savedLogs.AppendLine(logMessage);
            this.logCount++;
            if (this.logCount > MaxLogCount)
            {
                this.savedLogs.AppendFormat(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "Log count {0} exception count {1}{2}",
                    this.logCount,
                    this.exceptionCount,
                    Environment.NewLine);
                this.logCount = 0;
                this.exceptionCount = 0;
            }
        }
        
        /// <summary>
        /// Called by timer to write logs.
        /// Lock for thread-safe access to the saved logs.
        /// </summary>
        /// <param name="state">State object as provided by timer.</param>
        private void WriteSavedLogs(object state)
        {
            lock (this.logLock)
            {
                this.WriteSavedLogs();
            }
        }

        /// <summary>
        /// Check the length to prevent writing an empty file at start up.
        /// Work out the file path (changes at midnight). Open existing or new file and
        /// write the saved logs. If successful clear the StringBuilder otherwise (if there
        /// is an IOException) leave the saved logs for another attempt later.
        /// Update the file path if it has changed.
        /// </summary>
        private void WriteSavedLogs()
        {
            try
            {
                if (this.savedLogs.Length > 0)
                {
                    string filePath = this.NewLogFilePath();
                    this.PrepareLogForWriting(filePath);
                    this.WriteLogsToFileAndCleanUp(filePath);
                    this.UpdateLogFilePath(filePath);
                }
            }
            catch (IOException)
            {
                this.exceptionCount++;
                this.log = string.Empty;
            }
        }
        
        /// <summary>
        /// Use File.Exists to check for a new log file.
        /// Begin a new log file with a header showing process name, version and current date.
        /// Append saved logs to header or empty string.
        /// </summary>
        /// <param name="filePath">Path of log file.</param>
        private void PrepareLogForWriting(string filePath)
        {
            System.Diagnostics.Debug.Assert(this.log.Length == 0, "this.log should be cleared after previous write (successful or otherwise).");
            if (!File.Exists(filePath))
            {
                this.PrepareLogHeader();
            }
            
            this.log += this.savedLogs.ToString();
        }
        
        /// <summary>
        /// Prepare header to write at the start of the log file. 
        /// Use local time.
        /// </summary>
        private void PrepareLogHeader()
        {
            var s = new StringBuilder("Thales ");
            s.Append(System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            s.Append(" log for ");
            s.Append(DateTime.Now.ToLongDateString());
            s.AppendLine();
            s.AppendLine(this.HeaderInformation);
            this.log = s.ToString();
        }

        /// <summary>
        /// Open file and write accumulated logs.
        /// If successful, clear the written logs ready for the next lot.
        /// </summary>
        /// <param name="filePath">Path to existing or new log file.</param>
        private void WriteLogsToFileAndCleanUp(string filePath)
        {
            using (var sw = new StreamWriter(filePath, true))
            {
                sw.Write(this.log);
                this.ClearSavedLogs();
                this.currentLogFile = filePath;
            }
        }

        /// <summary>
        /// Empty all text from the StringBuilder and log (now it has been written to file).
        /// </summary>
        private void ClearSavedLogs()
        {
             this.savedLogs.Remove(0, this.savedLogs.Length);
             this.log = string.Empty;
        }

        /// <summary>
        /// Check if properties have changed to alter the base log name and create a new name based on date
        /// Most of the time this is the same as the old file path (except for the first log after midnight).
        /// Use local time.
        /// </summary>
        /// <returns>Log file path for today.</returns>
        private string NewLogFilePath()
        {
            this.UpdateBaseName();
            string filePath = string.Format(
                System.Globalization.CultureInfo.InvariantCulture,
                "{0}{1}.txt", 
                this.baseName, 
                DateTime.Now.ToString("yyMMdd", System.Globalization.CultureInfo.InvariantCulture));
            return filePath;
        }

        /// <summary>
        /// Check if the base name is up to date
        /// It changes when logging first begins and if the log folder or name property changes
        /// Exception expected if the user has insufficient privilege to write log.
        /// </summary>
        private void UpdateBaseName()
        {
            string basePath = Path.Combine(this.LogFolder, this.LoggingName);
            if (0 != string.Compare(this.baseName, basePath, System.StringComparison.Ordinal))
            {
                this.CheckLogFolder(); // create the folder if it does not exist
                this.baseName = basePath;
            }
        }

        /// <summary>
        /// Create the log folder if it does not exist.
        /// </summary>
        private void CheckLogFolder()
        {
            var logDirectory = new DirectoryInfo(this.LogFolder);
            if (!logDirectory.Exists)
            {
                // This is expected only if the installation is by 'xcopy'
                // in which case the user probably has full permissions. 
                // Normally the folder is created and permissions granted in advance by Setup
                // Tested by deleting the log folder and double-clicking the .exe to start
                // (using the Program Menu results in restoration of the log folder together
                // with display of message boxes from the installer)
                // For an ordinary user this fails silently if there is insufficient privilege
                logDirectory.Create(); // without the folder StreamWriter fails silently
            }
        }

        /// <summary>
        /// The file name is new initially and changes after date change or property change.
        /// StreamWriter creates the file with default permissions.
        /// Make sure other users can add to the log.
        /// This fails silently if the user group name is not valid.
        /// Requires administrator privileges (so for debug under Windows 7 run VS2010 as administrator).
        /// </summary>
        /// <param name="filePath">Path to the new log file.</param>
        private void UpdateLogFilePath(string filePath)
        {
            if (0 != string.Compare(filePath, this.logFilePath, System.StringComparison.Ordinal))
            {
                if (this.UserGroupName.Length > 0)
                {
                    FileSecurity security = File.GetAccessControl(filePath);
                    security.AddAccessRule(new FileSystemAccessRule(this.UserGroupName, FileSystemRights.Modify, AccessControlType.Allow));
                    File.SetAccessControl(filePath, security);
                }

                this.logFilePath = filePath;
            }
        }
    }
}

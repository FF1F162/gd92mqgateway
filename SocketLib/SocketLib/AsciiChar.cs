﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.SocketLib
{
    using System;

    /// <summary>
    /// Enumeration representing ASCII control characters and certain others
    /// that are used when interpreting bytes.
    /// The characters at the end of the list were used to interpret XML from CallTouch
    /// in the Tetra Airwave gateway. 
    /// </summary>
    public enum AsciiChar
    {
        NUL = 0x00,    /* The NULL or PAD character */
        SOH = 0x01,    /* Start of Header */
        STX = 0x02,    /* Start of Text */
        ETX = 0x03,    /* End of Text */
        EOT = 0x04,    /* End of Transmission */
        ENQ = 0x05,    /* Enquiry */
        ACK = 0x06,    /* Positive Acknowledge */
        BEL = 0x07,    /* Bell */
        BS = 0x08,    /* BackSpace */
        HT = 0x09,    /* Horizontal Tab */
        LF = 0x0A,    /* Line Feed */
        VT = 0x0B,    /* Vertical Tab */
        FF = 0x0C,    /* Form Feed */
        CR = 0x0D,    /* Carridge Return */
        SO = 0x0E,    /* Shift Out */
        SI = 0x0F,    /* Shift In */
        DLE = 0x10,    /* Data Link Escape */
        DC1 = 0x11,    /* Device Control Normally XON */
        DC2 = 0x12,    /* Device Control */
        DC3 = 0x13,    /* Device Control Normally XOFF */
        DC4 = 0x14,    /* Device Control */
        NAK = 0x15,    /* Negative Acknowledge */
        SYN = 0x16,    /* Synchronous Idle */
        ETB = 0x17,    /* End Of Transmission Block */
        CAN = 0x18,    /* Cancel */
        EM = 0x19,    /* End Of Medium */
        SUB = 0x1A,    /* Substitute */
        ESC = 0x1B,    /* Escape */
        FS = 0x1C,    /* File Separator */
        GS = 0x1D,    /* Group Separator */
        RS = 0x1E,    /* Record Separator */
        US = 0x1F,    /* Unit Separator */
        SP = 0x20,     /* Space */
        FSL = 0x2F,    /* Foreward SLash - start of XML */
        LAR = 0x3C,    /* Left ARrow - needed for XML */
        RAR = 0x3E,    /* Right ARrow - needed for XML */
        QM = 0x3F,    /* Question Mark - needed for XML */
        BSL = 0x5C,    /* Back SLash - may be needed for XML */
        e = 0x65,       /* e - needed for XML */
        g = 0x67,       /* g - needed for XML */
        DEL = 0x7F     /* Delete */
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.SocketLib
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Class incorporating IP address and maximum number of connections allowed from it
    /// </summary>
    /// <description>
    /// Originally used for configuring the RAD server to restrict the number of clients
    /// that are allowed to connect from the same machine
    /// </description>
    [XmlRootAttribute()]
    public class IPConnectData
    {
        [XmlElementAttribute()]
        public IPAddressData IPAddress { get; set; }

        [XmlElementAttribute()]
        public int MaxConnections { get; set; }
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.SocketLib
{
    using System;

    public interface ISocketHandler
    {
        void NotifyConnected(SocketEnd s);
        void NotifyDisconnected(SocketEnd s);
        void RecordReceived(string remoteEnd, Object record);
        void RecordAcknowledged(string remoteEnd, AsciiChar ackOrNak);
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.SocketLib
{
    using System;
    using System.Text;
    using System.Xml.Serialization;
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// Class representing name and IP address (excludes port)
    /// </summary>
    /// <description>
    /// Used in configuration of RAD and Airwave Gateway
    /// </description>
    [XmlRootAttribute()]
    public class IPAddressData : IEquatable<IPAddressData>
    {
        private string m_Name;
        private int m_IPAddress1;
        private int m_IPAddress2;
        private int m_IPAddress3;
        private int m_IPAddress4;
        private IPAddress m_IPAddr;

        public IPAddressData()
        {
            m_Name = "None";
        }
        public IPAddressData(string name, IPAddress ipAddress)
        {
        	if (ipAddress.AddressFamily != AddressFamily.InterNetwork){
        		throw new ApplicationException("This is designed to handle IPV4 only");
        	}
        	m_Name = name;
        	string ipString = ipAddress.ToString();
        	string[] ipArray = ipString.Split('.');
        	int.TryParse(ipArray[0], out m_IPAddress1);
        	int.TryParse(ipArray[1], out m_IPAddress2);
        	int.TryParse(ipArray[2], out m_IPAddress3);
        	int.TryParse(ipArray[3], out m_IPAddress4);
        	SetIPAddr();
        }
        public IPAddressData(string name, int ad1, int ad2, int ad3, int ad4)
        {
            SetFields(name, ad1, ad2, ad3, ad4);
        }
        public IPAddressData(IPAddressData a)
        {
            SetFields(a.Name, a.m_IPAddress1, a.m_IPAddress2, a.m_IPAddress3, a.m_IPAddress4);
        }
        private void SetFields(string name, int ad1, int ad2, int ad3, int ad4)
        {
            m_Name = name;
            m_IPAddress1 = ad1;
            m_IPAddress2 = ad2;
            m_IPAddress3 = ad3;
            m_IPAddress4 = ad4;
            SetIPAddr();
        }

        #region Properties

        [XmlElementAttribute()]
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        [XmlElementAttribute()]
        public int IPAddress1
        {
            get { return m_IPAddress1; }
            set { m_IPAddress1 = value; }
        }
        [XmlElementAttribute()]
        public int IPAddress2
        {
            get { return m_IPAddress2; }
            set { m_IPAddress2 = value; }
        }
        [XmlElementAttribute()]
        public int IPAddress3
        {
            get { return m_IPAddress3; }
            set { m_IPAddress3 = value; }
        }
        [XmlElementAttribute()]
        public int IPAddress4
        {
            get { return m_IPAddress4; }
            set { m_IPAddress4 = value; }
        }
        [XmlIgnore()]
        public System.Net.IPAddress IPAddr
        {
            get
            {
                if (m_IPAddr == null)
                {
                    SetIPAddr();
                }
                return m_IPAddr;
            }
        }

        #endregion

        public System.Net.IPAddress NewIPAddr()
        {
            byte[] addressBytes = GetByteArray();
            return new System.Net.IPAddress(addressBytes);
        }

        private void SetIPAddr()
        {
            byte[] addressBytes = GetByteArray();
            m_IPAddr = new System.Net.IPAddress(addressBytes);
        }
        private byte[] GetByteArray()
        {
            byte[] addressBytes = new byte[4];
            addressBytes[0] = (byte)m_IPAddress1;
            addressBytes[1] = (byte)m_IPAddress2;
            addressBytes[2] = (byte)m_IPAddress3;
            addressBytes[3] = (byte)m_IPAddress4;
            return addressBytes;
        }

        #region IEquatable Methods

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (this.GetType() != obj.GetType()) return false;
            IPAddressData a = obj as IPAddressData;
            return Equals(a);
        }
        public bool Equals(IPAddressData a)
        {
            return (m_Name.Equals(a.Name)
                && m_IPAddress1 == a.IPAddress1) && (m_IPAddress2 == a.IPAddress2) 
                && (m_IPAddress3 == a.IPAddress3) && (m_IPAddress4 == a.IPAddress4);
        }
        public static bool operator ==(IPAddressData a, IPAddressData b)
        {
        	// as this is a reference type it can be null (not like NodeAddress - value type)
        	if (object.Equals(a, null)) return object.Equals(b, null);
            // a is not null but b could be
            if (object.Equals(b, null)) return false;
            // neither a nor b are null so this should work
            return a.Equals(b);
        }
        public static bool operator !=(IPAddressData a, IPAddressData b)
        {
        	if (object.Equals(a, null)) return !object.Equals(b, null);
            // a is not null but b could be
            if (object.Equals(b, null)) return true;
            // neither a nor b are null so this should work
            return !a.Equals(b);
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        public override string ToString()
        {
            return String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} {1}.{2}.{3}.{4}",
                m_Name, m_IPAddress1.ToString(), m_IPAddress2.ToString(), m_IPAddress3.ToString(), m_IPAddress4.ToString());
        }

        #endregion
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.SocketLib
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using Thales.KentFire.EventLib;

    /// <summary>
    /// SocketEnd includes send, receive and close functionality.
    /// This was extracted from the original Bearer.cs and is used as the base class for Bearer (and so UdpBearer).
    /// </summary>
    public abstract class SocketEnd
    {
        public const int DefaultDebugBytes = 20;
        public const int MaxDebugBytes = 1000;
        public const int MaxForByte = 255;
        public const int MaxForWord = 65535;

        protected const int BufferSize = 1024 * 2; // ample for two datagrams

        private readonly byte[] readBuffer;

        private int debugBytes = DefaultDebugBytes;
        private int bytesInBuffer;
        private int readOffset;

        private delegate void ReceivedFrameDelegate(byte[] frameBytes);

        /// <summary>
        /// Initialises a new instance of the <see cref="SocketEnd"/> class.
        /// </summary>
        protected SocketEnd()
        {
            this.Lock = new object();
            this.Name = "SocketEnd"; // should be replaced by constructor in derived class
            this.readBuffer = new byte[BufferSize];
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="SocketEnd"/> class.
        /// Constructor used by listener.
        /// </summary>
        /// <param name="socket">Socket just established.</param>
        protected SocketEnd(Socket socket) 
            : this()
        {
            this.Socket = socket;
            this.SocketConnected = true;
            var remoteEndPoint = socket.RemoteEndPoint as IPEndPoint;
            if (remoteEndPoint != null)
            {
                this.IPAddress = remoteEndPoint.Address;
                this.IPort = remoteEndPoint.Port;
            }
        }

        /// <summary>
        /// Public read-only access to the name given to the socket connection.
        /// </summary>
        /// <value>Name of connection.</value>
        public string ConnectionName
        {
            get { return this.Name; }
        }

        /// <summary>
        /// Public read-only access to the remote address of the socket connection.
        /// </summary>
        /// <value>Remote address.</value>
        public IPAddress ConnectionIPAddress
        {
            get { return this.IPAddress; }
        }

        /// <summary>
        /// Public read-only access to the remote port of the socket connection.
        /// </summary>
        /// <value>Remote port.</value>
        public int ConnectionPort
        {
            get { return this.IPort; }
        }

        /// <summary>
        /// For thread-safe access to the socket.
        /// </summary>
        /// <value>Lock object.</value>
        protected object Lock { get; set; }

        /// <summary>
        /// Reference to the socket.
        /// </summary>
        /// <value>Socket reference.</value>
        protected Socket Socket { get; set; }

        protected string Name { get; set; }

        /// <summary>
        /// Remote address of the socket connection.
        /// </summary>
        /// <value>Remote address.</value>
        protected IPAddress IPAddress { get; set; } 

        /// <summary>
        /// Remote port of the socket connection.
        /// </summary>
        /// <value>Remote port.</value>
        protected int IPort { get; set; }           

        /// <summary>
        /// Connection state of the socket.
        /// </summary>
        /// <value>True if connected.</value>
        protected bool SocketConnected { get; set; }

        public void SetIPAddress(IPAddress remoteIPAddress)
        {
            this.IPAddress = remoteIPAddress;
        }

        public void SetIPort(int remotePort)
        {
            this.IPort = remotePort;
        }

        /// <summary>
        /// This can be used by derived classes to set up a reference to the read buffer.
        /// </summary>
        /// <returns>Read buffer.</returns>
        protected byte[] GetReadBuffer()
        {
            return this.readBuffer;
        }

        #region Connect

        /// <summary>
        /// Set lock before calling this.
        /// Creates IP end point and calls BeginConnect.
        /// </summary>
        protected virtual void BeginToConnect()
        {
            // lock set in CTSocket.CTConnect and SgcSocket.Connect
            var remoteIPEndpoint = new IPEndPoint(this.IPAddress, this.IPort);
            this.Socket = new Socket(
                AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.Socket.BeginConnect(
                remoteIPEndpoint, new AsyncCallback(this.ConnectCallback), this.Socket);
        }

        /// <summary>
        /// Extract socket from IAsyncResult data.
        /// Complete set-up of the socket end and (maybe) send ENQ. Begin to receive data.
        /// </summary>
        /// <param name="ar">Data including reference to the new socket.</param>
        protected void ConnectCallback(IAsyncResult ar)
        {
            lock (this.Lock)
            {
                EventControl.Debug(this.Name, "Locked by ConnectCallback");
                var connectSocket = ar.AsyncState as Socket;
                if (connectSocket == null)
                {
                    EventControl.Emergency(this.Name, "ConnectCallback - null socket");
                }
                else
                {
                    this.CompleteConnection(ar, connectSocket);
                }
            }
        }

        /// <summary>
        /// Complete set-up of the socket end and (maybe) send ENQ. Begin to receive data.
        /// </summary>
        /// <param name="ar">Data including reference to the new socket.</param>
        /// <param name="connectSocket">Socket reference.</param>
        protected void CompleteConnection(IAsyncResult ar, Socket connectSocket)
        {
            try
            {
                connectSocket.EndConnect(ar);
                this.Socket = connectSocket;
                this.SocketConnected = true;
                this.SendOnConnect();
                this.Receive(0);
            }
            catch (SocketException e)
            {
                EventControl.Warn(this.Name, string.Format(
                    System.Globalization.CultureInfo.InvariantCulture,
                    "ConnectCallback - {0}",
                    e.Message));
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Override this to send something else or nothing if that is required.
        /// </summary>
        protected virtual void SendOnConnect()
        {
            this.Send(AsciiChar.ENQ);
        }
        
        #endregion

        #region Close

        /// <summary>
        /// Override to do other cleanup e.g. remove from listener collection of sockets
        /// make sure the lock is set.
        /// </summary>
        protected virtual void CloseConnection()
        {
            this.CloseSocket();
        }

        /// <summary>
        /// Protected to allow a call in Bearer.CloseConnection.
        /// Make sure the lock is set.
        /// </summary>
        protected void CloseSocket()
        {
            this.SocketConnected = false;
            if (this.Socket != null)
            {
                this.Socket.Close();
                this.Socket = null;
            }
        }

        #endregion

        #region Receive

        /// <summary>
        /// Begin to receive data. Offset is non-zero if part of a message is in the buffer already.
        /// Note that any part-message left at the end of previous processing will have been moved to the start of the buffer.
        /// </summary>
        /// <param name="offset">Offset into read buffer.</param>
        protected virtual void Receive(int offset)
        {
            Debug.Assert(this.Socket != null, "Socket must not be null");
            if (this.Socket != null)
            {
                this.readOffset = offset;
                this.BeginReceive(offset);
            }
        }
        
        /// <summary>
        /// Overridden by UdpBearer.
        /// Check room in buffer and use Async pattern to receive data.
        /// </summary>
        /// <param name="offset">Offset into read buffer.</param>
        protected virtual void BeginReceive(int offset)
        {
            Debug.Assert(BufferSize > offset, "Offset must not be larger than the buffer");
            int maximumToRead = BufferSize - offset;
            if (maximumToRead > 0)
            {
                this.Socket.BeginReceive(this.readBuffer, offset, maximumToRead, SocketFlags.None, new AsyncCallback(this.ReceiveCallback), this.Socket);
            }

            // TODO consider raising exception as this breaks the cycle of async calls
        }

        /// <summary>
        /// Overridden by UdpBearer.
        /// </summary>
        /// <param name="ar">IAsyncResult data.</param>
        /// <returns>Number of bytes read.</returns>
        protected virtual int EndReceive(IAsyncResult ar)
        {
            int bytesRead = this.Socket.EndReceive(ar);
            EventControl.Debug(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "EndReceive {0} bytes added to read buffer", 
                bytesRead));
            return bytesRead;
        }

        /// <summary>
        /// Call EndReceive and process the data.
        /// </summary>
        /// <param name="ar">IAsyncResult data.</param>
        protected void ReceiveCallback(IAsyncResult ar)
        {
            lock (this.Lock)
            {
                try
                {
                    EventControl.Debug(this.Name, "Locked by ReceiveCallback");
                    var passedSocket = ar.AsyncState as Socket;
                    if (object.ReferenceEquals(this.Socket, passedSocket))
                    {
                        int bytesRead = this.EndReceive(ar);
                        this.bytesInBuffer = bytesRead + this.readOffset;
                        if (bytesRead > 0)
                        {
                            this.DoDataReceivedProcessing();

                            // when processing is complete this calls Receive
                            this.ParseData(bytesRead, 0);
                        }
                        else
                        {
                            EventControl.Info(this.Name, "No bytes to receive - closing connection");
                            this.CloseConnection();
                        }
                    }
                    else
                    {
                        EventControl.Debug(this.Name, "ReceiveCallback found socket closed or replaced");
                    }
                }
                catch (SocketException e)
                {
                    EventControl.Note(this.Name, string.Format(System.Globalization.CultureInfo.InvariantCulture, 
                        "ReceiveCallback closing connection - {0}",
                        e.Message));
                    this.CloseConnection();
                }
                catch (ObjectDisposedException)
                {
                    // whatever disposed of the socket did not set the member to null
                    // seen 3 times out of 4 when closing GIS listener
                }
                catch (Exception e)
                {
                    EventControl.Emergency(this.Name, e.ToString());
                    throw;
                }
            }
        }

        /// <summary>
        /// Parse control characters and messages in the read buffer.
        /// This may be called recursively to process the data. 
        /// BytesRead is non-zero for the first call and zero thereafter.
        /// Processing starts at firstIndex, which is zero for the first call.
        /// </summary>
        /// <param name="bytesRead">Number of bytes just added to read buffer.</param>
        /// <param name="firstIndex">Index into read buffer.</param>
        protected void ParseData(int bytesRead, int firstIndex)
        {
            // assume lock set
            // assume buffer may contain more than one control character or frame
            // and the last frame may not be complete
            // This is much more likely now MOBS sends text message frames requiring no
            // acknowldegement except for the last

            // firstByteIndex is 0 when more data has been added to the buffer
            // if it is not 0 this is a recursive call - the buffer includes more than one 
            // control character or frame

            // bytesRead is as returned by EndReceive (or 0 for a recursive call)
            // there may be more than this in the read buffer if the previous read ended
            // with part of a frame and this was copied to the front of the buffer

            // this.readOffset shows the point in the buffer where more data is to be added
            // it is non-zero when there is a remainder of part of a frame when 
            // the rest of the buffer has been processed by recursive calls
            // Before calling Receive the remainder is copied to the front of the buffer
            // it seemed to work quite well without doing this until AVL report
            // managed to fill the buffer (ECN3016)
            // Receive records the offset in m_this.readOffset
            int bytesUnprocessed;
            if (firstIndex == 0)
            {
                Debug.Assert(bytesRead > 0, "if firstIndex is 0 bytesRead must be greater than zero");

                // there has just been a read - all bytes in the buffer need processing
                bytesUnprocessed = this.bytesInBuffer;
            }
            else
            {
                Debug.Assert(bytesRead == 0, "bytesRead should be 0 if firstIndex is greater than 0 i.e. this is a recursive call after processing control char or frame earlier in buffer");
                bytesUnprocessed = this.bytesInBuffer - firstIndex;
                Debug.Assert(bytesUnprocessed > 0, "bytesInBuffer less firstIndex must be positive");
            }

            int bytesToLog = bytesUnprocessed;
            string more = string.Empty;
            if (bytesUnprocessed > this.debugBytes)
            {
                bytesToLog = this.debugBytes;
                more = " . . .";
            }

            EventControl.Debug(this.Name, string.Format(
                System.Globalization.CultureInfo.InvariantCulture, 
                "Read {0} bytes. firstByteIndex {1} bytes in buffer {2} unprocessed {3}{4}{5}{6}",
                bytesRead, 
                firstIndex, 
                this.bytesInBuffer, 
                bytesUnprocessed, 
                Environment.NewLine,
                BitConverter.ToString(this.readBuffer, firstIndex, bytesToLog), 
                more));

            int parseReadOffset = 0;

            AsciiChar firstByte = this.InterpretFirstByte(this.readBuffer[firstIndex]);
            switch (firstByte)
            {
                case AsciiChar.ENQ:
                    EventControl.Info(this.Name, "ENQ received");
                    this.Send(AsciiChar.ACK);
                    firstIndex++;
                    break;
                case AsciiChar.ACK:
                    this.DoAckReceivedProcessing();
                    EventControl.Info(this.Name, "ACK received");
                    firstIndex++;
                    break;
                case AsciiChar.NAK:
                    this.DoNakReceivedProcessing();
                    EventControl.Info(this.Name, "NAK received");
                    firstIndex++;
                    break;
                case AsciiChar.SOH:
                    this.DoStartOfHeaderReceivedProcessing();

                    // DecodeLength returns 0 if there are too few bytes so the length is missing
                    int frameLength = this.DecodeLength(this.readBuffer, firstIndex, bytesUnprocessed);
                    if (bytesUnprocessed < frameLength || 0 == frameLength)
                    {
                        // this is expected when read fills the buffer - the last frame is incomplete
                        // move the fragment to the front of the buffer and do another read
                        EventControl.Debug(this.Name, "SOH received and part of message");
                        if (firstIndex > 0)
                        {
                            // copy unprocessed data to the front of the buffer 
                            var remainder = new byte[bytesUnprocessed];
                            Buffer.BlockCopy(this.readBuffer, firstIndex, remainder, 0, bytesUnprocessed);
                            Buffer.BlockCopy(remainder, 0, this.readBuffer, 0, bytesUnprocessed);
                            this.bytesInBuffer = bytesUnprocessed;
                        }
                        else
                        {
                            // this does not seem likely - frames fit in a datagram so expect
                            // to read a whole frame when the buffer is empty (firstByteIndex == 0)
                            Debug.Assert(this.bytesInBuffer == bytesUnprocessed, "Check that whole buffer is unprocessed");
                        }

                        parseReadOffset = bytesUnprocessed;
                    }
                    else
                    {
                        var frameBytes = new byte[frameLength];
                        Buffer.BlockCopy(this.readBuffer, firstIndex, frameBytes, 0, frameLength);

                        // Begin a new thread to process the received data
                        ReceivedFrameDelegate rfd = HandleReceivedFrame;
                        rfd.BeginInvoke(frameBytes, HandleReceivedFrameCallback, rfd);

                        // Meanwhile do low-level (frame) acknowledgement and go on to
                        // begin the next asynchronous read and release the lock
                        this.DoSendFrameAckProcessing();
                        firstIndex += frameLength;
                    }

                    break;
                case AsciiChar.BEL:
                    this.DoBellReceivedProcessing();
                    EventControl.Info(this.Name, "BEL received");
                    firstIndex++;
                    break;
                case AsciiChar.CR:
                    // CallTouch sends this in between messages that are sent together
                    // As this (or any other default) has never been seen in any of the other interfaces
                    // it is probably OK for this to apply in all cases (like BEL, which should really be
                    // treated as an unexpected character for the purposes of the CallTouch interface).
                    EventControl.Info(this.Name, "CR received and ignored");
                    firstIndex++;
                    break;
                case AsciiChar.LF:
                    // Following changes to CTSocket LF started to appear (when using test CallTouch)
                    // added this case as a precaution
                    EventControl.Info(this.Name, "LF received and ignored");
                    firstIndex++;
                    break;
                default:
                    EventControl.Warn(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "Unexpected character {0} - data discarded",
                        firstByte));
                    parseReadOffset = 0; // not needed? only set when processing SOH
                    firstIndex += this.bytesInBuffer;
                    break;
            }

            // this.readOffset is non-zero only if there is just part of a frame 
            // left in the buffer - in which case Receive is needed
            // (this puts more data in the buffer, starting at the offset)

            // if this.readOffset is 0 and there are bytes left in the buffer
            // call ParseData recursively to process them
            if (parseReadOffset == 0 && this.bytesInBuffer - firstIndex > 0)
            {
                this.ParseData(0, firstIndex);
            }
            else
            {
                this.Receive(parseReadOffset);
            }
        }

        /// <summary>
        /// Override this if the protocol uses different control characters e.g. for CallTouch interpret LAR as SOH
        /// Default behaviour is to return the same byte (no substitution).
        /// </summary>
        /// <param name="first">Byte to be substituted.</param>
        /// <returns>Substitute byte.</returns>
        protected virtual AsciiChar InterpretFirstByte(byte first)
        {
            return (AsciiChar)first;
        }

        /// <summary>
        /// Override for actions when data is received (e.g. mark connection as open).
        /// </summary>
        protected virtual void DoDataReceivedProcessing()
        {
        }

        /// <summary>
        /// Override for actions when ACK is received.
        /// </summary>
        protected virtual void DoAckReceivedProcessing()
        {
        }

        /// <summary>
        /// Override for actions when NAK is received.
        /// </summary>
        protected virtual void DoNakReceivedProcessing()
        {
        }

        /// <summary>
        /// Override for actions when BEL is received.
        /// </summary>
        protected virtual void DoBellReceivedProcessing()
        {
        }

        /// <summary>
        /// Override for actions when SOH is received.
        /// </summary>
        protected virtual void DoStartOfHeaderReceivedProcessing()
        {
        }

        /// <summary>
        /// Override for actions needed to acknowledge a frame. 
        /// </summary>
        protected virtual void DoFrameAcknowledgedProcessing()
        {
        }

        /// <summary>
        /// Override to send the correct frame acknowledgement character. 
        /// </summary>
        protected virtual void DoSendFrameAckProcessing()
        {
        }

        /// <summary>
        /// Override to extract the message length from data at the beginning of the message (or frame).
        /// </summary>
        /// <param name="buffer">Read buffer.</param>
        /// <param name="offset">Offset into read buffer.</param>
        /// <param name="bytesRead">Number of bytes available.</param>
        /// <returns>Length of frame or 0.</returns>
        protected virtual int DecodeLength(byte[] buffer, int offset, int bytesRead)
        {
            Debug.Assert(false, "this needs overriding if it is called");
            return 0; // return 0 if there are not enough bytes to work with
        }

        /// <summary>
        /// Override to proces a frame.
        /// </summary>
        /// <param name="value">Byte array comprising the frame.</param>
        protected virtual void HandleReceivedFrame(byte[] value)
        {
            Debug.Assert(false, "this needs overriding if it is called");
        }

        /// <summary>
        /// Call the received frame handler asynchronously (in a new thread from the thread pool).
        /// </summary>
        /// <param name="ar">IAsyncResult data - reference to the delegate.</param>
        protected void HandleReceivedFrameCallback(IAsyncResult ar)
        {
            try
            {
                var rfd = ar.AsyncState as ReceivedFrameDelegate;
                if (rfd != null)
                {
                    rfd.EndInvoke(ar);
                }
            }
            catch (Exception e)
            {
                EventControl.Emergency(this.Name, e.ToString());
                throw;
            }
        }
        
        #endregion

        #region Send

        /// <summary>
        /// Lock before calling this.
        /// Begin to send a single character asynchronously.
        /// </summary>
        /// <param name="asciiChar">ASCII character enumeration.</param>
        protected void Send(AsciiChar asciiChar)
        {
            // lock is needed
            var byteData = new byte[1];
            byteData[0] = (byte)asciiChar;
            this.Send(byteData, 1);
        }

        /// <summary>
        /// Lock before calling this.
        /// Begin to send an array of bytes (control character or frame) asynchronously.
        /// </summary>
        /// <param name="byteData">Byte array.</param>
        /// <param name="numberOfBytes">Number of bytes to be sent.</param>
        protected void Send(byte[] byteData, int numberOfBytes)
        {
            if (this.Socket != null && this.SocketConnected == true)
            {
                if (numberOfBytes == 1)
                {
                    var controlCharacter = (AsciiChar)byteData[0];
                    EventControl.Info(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "Sending {0}", 
                        controlCharacter.ToString()));
                }
                else
                {
                    EventControl.Info(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "Sending {0} bytes",
                        numberOfBytes));
                }

                this.BeginSend(byteData, numberOfBytes);
            }
        }

        /// <summary>
        /// Call with lock set.
        /// Begin asynchronous send.
        /// </summary>
        /// <param name="byteData">Array of bytes.</param>
        /// <param name="numberOfBytes">Number of bytes to send.</param>
        protected virtual void BeginSend(byte[] byteData, int numberOfBytes)
        {
            this.Socket.BeginSend(byteData, 0, numberOfBytes, 0, new AsyncCallback(this.SendCallback), this.Socket);
        }

        /// <summary>
        /// Call with lock set.
        /// End asynchronous send.
        /// Overridden by UdpBearer.
        /// </summary>
        /// <param name="ar">IAsyncResult data.</param>
        /// <returns>Number of bytes sent.</returns>
        protected virtual int EndSend(IAsyncResult ar)
        {
            int bytesSent = this.Socket.EndSend(ar);
            return bytesSent;
        }

        /// <summary>
        /// Call EndSend and clean up.
        /// </summary>
        /// <param name="ar">IAsyncResult data.</param>
        protected void SendCallback(IAsyncResult ar)
        {
            lock (this.Lock)
            {
                try
                {
                    var passedSocket = ar.AsyncState as Socket;
                    EventControl.Debug(this.Name, "Locked by SendCallback");
                    if (object.ReferenceEquals(this.Socket, passedSocket))
                    {
                        int bytesSent = this.EndSend(ar);
                        EventControl.Debug(this.Name, string.Format(
                            System.Globalization.CultureInfo.InvariantCulture, 
                            "Sent {0} bytes", 
                            bytesSent));
                    }
                }
                catch (SocketException e)
                {
                    EventControl.Note(this.Name, string.Format(
                        System.Globalization.CultureInfo.InvariantCulture, 
                        "SendCallback closing connection - {0}",
                        e.Message));
                    this.CloseConnection();
                }
                catch (Exception e)
                {
                    EventControl.Emergency(this.Name, e.ToString());
                    throw;
                }
            }
        }

        #endregion
    }
}

﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

namespace Thales.KentFire.SocketLib
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net;
	using System.Net.Sockets;
	using Thales.KentFire.EventLib;

	/// <summary>
	/// SocketListener listens for TCP/IP connection on a single port
    /// IMPORTANT implement as a single static instance
	/// </summary>
	/// <description>
    /// based on GisListener via RadListener which is a simplified version of GD92Component.Mta
    /// It validates connection requests against a list of allowed IP addresses
    /// and number of connections allowed from each
    /// It maintains a dictionary holding a reference to each connection
	/// </description>
	public abstract class SocketListener
	{
		private const int MaxPendingConnections = 100;
		
        protected Object m_Lock;

        private IPEndPoint m_ListeningEndPoint;
        private Socket m_ListeningSocket;
        private IAsyncResult m_CurrentAsyncResult;

        protected ListenerConfig m_Config;
        protected string m_Name;

        // the container that instantiates this class must provide a method to create a new SocketEnd
        // derived type (in the listener we just deal with the base SocketEnd class)
        protected ISocketListener m_Container; // set by Activate

        protected ISocketHandler m_Handler; // optionally set by Activate (to allow NotifyConnected)

        protected Dictionary<string, SocketEnd> m_SocketsByRemoteEnd;

        protected SocketListener()
		{
            m_Lock = new Object();
            m_Name = "SocketListener";
            m_SocketsByRemoteEnd = new Dictionary<string, SocketEnd>();
		}

        #region Properties
        
        public string Name
        {
            get { return m_Name; }
        }
        public bool Activated
        {
        	get { return (m_ListeningSocket != null); }
        }
        public int ListeningPort
        {
            get { return (m_Config == null) ? 0 : m_Config.ListeningPort; }
        }
        public ListenerConfig Config
        {
            get { return m_Config; }
        }

        #endregion

        #region Deactivate and Activate
        
        public virtual void Deactivate()
        {
            lock (m_Lock)
            {
                foreach (KeyValuePair<string, SocketEnd> kvp in m_SocketsByRemoteEnd)
                {
                	kvp.Value.LockAndCloseSocket();
                }
                m_SocketsByRemoteEnd.Clear();

                m_ListeningEndPoint = null;
                if (m_ListeningSocket != null)
                {
                    m_ListeningSocket.Close();
                    m_ListeningSocket = null;
                }
            }
        }
        protected void Activate(ISocketListener container)
        {
            // Actually this need not be the container - it can be the listener
            // itself e.g. CTListener extends SocketListener and provides ISocketListener
            m_Container = container;

            if (ListeningPort == 0)
            {
                EventControl.QueueLogEvent(LogLevel.Warning, String.Format(System.Globalization.CultureInfo.InvariantCulture, 
                "{0} cannot activate - listening port is not configured",
                Name));
            }
            else if (m_ListeningSocket == null)
        	{
	        	lock(m_Lock)
	        	{
		            m_ListeningEndPoint = new IPEndPoint(0, ListeningPort);
		            m_ListeningSocket = new Socket(AddressFamily.InterNetwork,
		                SocketType.Stream, ProtocolType.Tcp);
		            m_ListeningSocket.Bind(m_ListeningEndPoint);
		            m_ListeningSocket.Listen(MaxPendingConnections);
		            Listen();
	        	}
        	}
        	else 
        	{
                // no good trying to create a socket if there is one in place using the same port
                EventControl.QueueLogEvent(LogLevel.Warning, String.Format(System.Globalization.CultureInfo.InvariantCulture, 
                "{0} cannot activate - previous listener has not been deactivated", 
                Name));
        	}
        }
#endregion
#region Listen and Accept
        private void Listen()
        {
            try
            {
                m_CurrentAsyncResult = m_ListeningSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
            }
            catch (Exception e)
            {
            	m_ListeningEndPoint = null;
            	m_ListeningSocket = null;
                EventControl.QueueLogEvent(LogLevel.Debug, String.Format(System.Globalization.CultureInfo.InvariantCulture, 
                    "{0} Listen socket closed - {1}", m_Name, e.Message));
            }
        }
        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                lock (m_Lock)
                {
                    if (ar == m_CurrentAsyncResult)
                    {
                        if (m_ListeningSocket != null)
                        {
                            Socket connectingSocket = m_ListeningSocket.EndAccept(ar);
                            SetUpEndPoint(connectingSocket);
                            Listen();
                        }
                        else
                        {
                            EventControl.QueueLogEvent(LogLevel.Debug, String.Format(System.Globalization.CultureInfo.InvariantCulture, 
                                "{0} AcceptCallback socket closed.", m_Name));
                        }
                    }
                    else
                    {
                        //ignore calls that do not match - seen for GIS listener before this fix
                        //if the test is made outside the lock the exception still occurs
                        //possibly MOBS is trying to connect but socket is not yet in a fit state
                        //  fix is not vital as the next call to AcceptCallback seems to succeed
                    }
                }
            }
            catch (Exception e)
            {
                EventControl.QueueLogEvent(LogLevel.Critical, String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} AcceptCallback - {1}",
                    m_Name, e.ToString()));
            }
        }
        private void SetUpEndPoint(Socket connectingSocket)
        {
            var remoteEndPoint = connectingSocket.RemoteEndPoint as IPEndPoint;
            IPAddress address = remoteEndPoint.Address;
            bool validAddress = false;
            IPConnectData validConnection = null;
            if (m_Config.ConnectData == null)
            {
                // allow unlimited connection
                validAddress = true;
            }
            else
            {
                foreach (IPConnectData connection in m_Config.ConnectData)
                {
                    if (connection.IPAddress.IPAddr.Equals(address))
                    {
                        validConnection = connection;
                        validAddress = true;
                        break;
                    }
                }
            }
            if (validAddress)
            {
            	SocketEnd socketEnd = null;
                lock (m_Lock)
                {
                    // For CallTouch emulator we need to restrict the number of connections to one
                    // from each valid IP address
                    // Count number of connection already in place. Warn if this is at the maximum
                    // (and refuse the connection)
                    // There is no restriction if the array of valid addresses is null
                    // Connection from an address can be prevented by setting MaxConnections to 0
                    //
                    if (m_SocketsByRemoteEnd.Count > 0)
                    {
                        int addressConnections = 0;
                        int totalConnections = 0;
                        foreach (KeyValuePair<string, SocketEnd> kvp in m_SocketsByRemoteEnd)
                        {
                            EventControl.QueueLogEvent(LogLevel.Debug, String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} is already connected to {1}",
                                m_Name, kvp.Value.RemoteEnd));
                            if (kvp.Value.IPAddress.Equals(address))
                            {
                                addressConnections++;
                            }
                            totalConnections++;
                        }
                        EventControl.QueueLogEvent(LogLevel.Info, String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} already has {1} connections including {2} to {3}",
                            m_Name, totalConnections, addressConnections, address.ToString()));
                        if (validConnection != null && addressConnections >= validConnection.MaxConnections)
                        {
                            EventControl.QueueLogEvent(LogLevel.Warning, String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} no more than {1} connections allowed to {2}",
                            m_Name, addressConnections, address.ToString()));
                            validAddress = false;
                        }
                    }
                    if (validAddress)
                    {
                        socketEnd = m_Container.NewSocketEnd(connectingSocket);
                        m_SocketsByRemoteEnd.Add(socketEnd.RemoteEnd, socketEnd);
                        EventControl.QueueLogEvent(LogLevel.Notice, String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} allowing connection request from {1}",
                            m_Name, socketEnd.RemoteEnd));
                    }
                }
                // release the lock
                // need to apply it again to send xml message to synchronize client configuration
                if (m_Handler != null && socketEnd != null)
                {
                	m_Handler.NotifyConnected(socketEnd);
                }
            }

            if (!validAddress)
            {
                EventControl.QueueLogEvent(LogLevel.Warning, String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} Unauthorised connection request from {1}",
                    m_Name, address.ToString()));
                // not closing avoids all the warnings - MOBS just carries on sending Init request
                // the socket does not get closed until the entire application shuts down
                connectingSocket.Close();
            }
        }

        #endregion

        #region Methods Applying to Socket Collection
        
        public void RemoveSocket(string remoteEnd)
        {
            lock (m_Lock)
            {
                m_SocketsByRemoteEnd.Remove(remoteEnd);
            }
        }
        public void SendToAll(Object record)
        {
            lock (m_Lock)
            {
                SendRecordToAll(record);
            }
        }
        protected void SendRecordToAll(Object record)
        {
            // assume lock is set
            // can be used where there is more than one record to send to each client
            foreach (KeyValuePair<string, SocketEnd> kvp in m_SocketsByRemoteEnd)
            {
                m_Container.SendRecord(kvp.Value, record);
            }
        }

        #endregion
	}
}

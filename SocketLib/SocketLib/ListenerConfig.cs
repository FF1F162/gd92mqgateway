﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Thales.KentFire.SocketLib
{
    /// <summary>
    /// Class representing connection information for a socket listener
    /// </summary>
    /// <description>
    /// based on Thales.KFRS.Rad.IPAddressData
    /// 
    /// </description>
    public class ListenerConfig
    {
        private int m_ListeningPort;
        private IPConnectData[] m_ConnectData;

#region Properties
        [XmlElementAttribute()]
        public int ListeningPort
        {
            get { return m_ListeningPort; }
            set { m_ListeningPort = value; }
        }
        [XmlArray()]
        public IPConnectData[] ConnectData
        {
            get { return m_ConnectData; }
            set { m_ConnectData = value; }
        }
#endregion
    }
}

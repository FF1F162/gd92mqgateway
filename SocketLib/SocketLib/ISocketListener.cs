﻿// Copyright (c) Thales UK Limited 2015 All Rights Reserved

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Thales.KentFire.SocketLib
{
    public interface ISocketListener
    {
        SocketEnd NewSocketEnd(Socket connectingSocket);
        void SendRecord(SocketEnd socketEnd, Object record);
    }
}
